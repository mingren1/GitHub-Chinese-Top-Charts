<div align="center">
    <img src="https://i.v2ex.co/wF4Dx0vO.png">
    <a href="https://github.com/kon9chunkit/GitHub-Chinese-Top-Charts" target="_blank">GitHub版本</a> | 码云版本
</div>

## 设立目的

- :cn: GitHub中文排行榜，帮助你发现高分优秀中文项目；
- 各位伙伴可以无语言障碍、更高效地吸收国人的优秀经验、成果；
- 中文项目只能满足阶段性的需求，想要有进一步提升，还请多花时间学习高分神级英文项目；

## 设立范围

- 设立1个总榜（所有语言项目汇总排名）、26个分榜（单个语言项目排名）；

## 入选规则

- 包含有中文文档：项目的 Description、README.md 包含中文说明；
- 更新越持续越好：最近半年内有更新过的项目才有机会入选（拥抱活跃，远离僵尸）；
- Stars越多越好：在满足持续更新的前提条件下，各榜根据 Stars 对项目进行排序；

## 更新情况

- 项目更新频率：每周更新；
- 最近更新时间：2021年12月1日；
- 近期更新内容：
  - [x] 移除了违规、敏感项目，如果还有遗漏请大家反馈给我们；
  - [x] 新增了8个分榜：TypeScript、Kotlin、Rust、MATLAB、Lua、Ruby、Scala、Perl；
  - [x] 项目同步更新到码云，方便喜欢码云的伙伴查看；
- 即将更新内容：
  - [ ] 推出「排除中文项目排行榜」，高分神级英文项目是进阶的不二之选；
  - [ ] 将「资料类项目」分离出「技术类项目」的榜单，在大家的长期鞭策下终于要来了；

## License

- 本仓库内容的定义、创建、更新维护均由本人发起与推进，在您引用本仓库内容、转载文章时，请在开头明显处标明作者及页面地址，谢谢；

---

## 内容目录

- 总榜
  - [All Language](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#all-language)

- 分榜
  - [Java](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#java)
  - [JavaScript](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#javascript)
  - [Python](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#python)
  - [Go](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#go)
  - [PHP](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#php)
  - [C++](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#c)
  - [C#](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#c-1)
  - [HTML](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#html)
  - [TypeScript](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#typescript)
  - [Vue](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#vue)
  - [Kotlin](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#kotlin)
  - [Swift](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#swift)
  - [Objective-C](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#objective-c)
  - [C](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#c-2)
  - [Shell](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#shell)
  - [Jupyter Notebook](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#jupyter-notebook)
  - [CSS](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#css)
  - [Dart](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#dart)
  - [TeX](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#tex)
  - [Lua](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#lua)
  - [Rust](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#rust)
  - [MATLAB](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#matlab)
  - [Ruby](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#ruby)
  - [Vim Script](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#vim-script)
  - [Perl](https://gitee.com/kon9chunkit/GitHub-Chinese-Top-Charts#perl)

---

## All Language

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[CyC2018/CS-Notes](https://github.com/CyC2018/CS-Notes)|:books: 技术面试必备基础知识、Leetcode、计算机操作系统、计算机网络、系统设计|142.5k|Java|2021/11/25|
|2|[Snailclimb/JavaGuide](https://github.com/Snailclimb/JavaGuide)|「Java学习+面试指南」一份涵盖大部分 Java 程序员所需要掌握的核心知识。准备 Java 面试，首选 JavaGuide！|114.0k|Java|2021/11/29|
|3|[labuladong/fucking-algorithm](https://github.com/labuladong/fucking-algorithm)|刷算法全靠套路，认准 labuladong 就够了！English version supported! Crack LeetCode, not only how, but also why. |99.0k|-|2021/11/29|
|4|[justjavac/free-programming-books-zh_CN](https://github.com/justjavac/free-programming-books-zh_CN)|:books: 免费的计算机编程类中文书籍，欢迎投稿|84.6k|-|2021/09/08|
|5|[MisterBooo/LeetCodeAnimation](https://github.com/MisterBooo/LeetCodeAnimation)|Demonstrate all the questions on LeetCode in the form of animation.（用动画的形式呈现解LeetCode题目的思路）|67.9k|Java|2021/06/24|
|6|[doocs/advanced-java](https://github.com/doocs/advanced-java)|😮 Core Interview Questions & Answers For Experienced Java(Backend) Developers   互联网 Java 工程师进阶知识完全扫盲：涵盖高并发、分布式、高可用、微服务、海量数据处理等领域知识|58.8k|Java|2021/11/27|
|7|[macrozheng/mall](https://github.com/macrozheng/mall)|mall项目是一套电商系统，包括前台商城系统及后台管理系统，基于SpringBoot+MyBatis实现，采用Docker容器化部署。 前台商城系统包含首页门户、商品推荐、商品搜索、商品展示、购物车、订单流程、会员中心、客户服务、帮助中心等模块。 后台管理系统包含商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等模块。|54.4k|Java|2021/11/30|
|8|[521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub)|:octocat: 分享 GitHub 上有趣、入门级的开源项目。Share interesting, entry-level open source projects on GitHub.|49.0k|Python|2021/11/26|
|9|[bailicangdu/vue2-elm](https://github.com/bailicangdu/vue2-elm)|基于 vue2 + vuex 构建一个具有 45 个页面的大型单页面应用|37.2k|Vue|2021/09/13|
|10|[chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry)|The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库,  唐宋两朝近一万四千古诗人,  接近5.5万首唐诗加26万宋诗.  两宋时期1564位词人，21050首词。|34.7k|JavaScript|2021/11/27|
|11|[dcloudio/uni-app](https://github.com/dcloudio/uni-app)|uni-app 是使用 Vue 语法开发小程序、H5、App的统一框架|34.6k|JavaScript|2021/11/30|
|12|[0voice/interview_internal_reference](https://github.com/0voice/interview_internal_reference)|2021年最新总结，阿里，腾讯，百度，美团，头条等技术面试题目，以及答案，专家出题人分析汇总。|32.9k|Python|2021/08/25|
|13|[apachecn/AiLearning](https://github.com/apachecn/AiLearning)|AiLearning: 机器学习 - MachineLearning - ML、深度学习 - DeepLearning - DL、自然语言处理 NLP|32.1k|Python|2021/09/07|
|14|[testerSunshine/12306](https://github.com/testerSunshine/12306)|12306智能刷票，订票|30.3k|Python|2021/08/25|
|15|[NervJS/taro](https://github.com/NervJS/taro)|开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。  https://taro.zone/|30.1k|JavaScript|2021/12/01|
|16|[AobingJava/JavaFamily](https://github.com/AobingJava/JavaFamily)|【Java面试+Java学习指南】 一份涵盖大部分Java程序员所需要掌握的核心知识。|28.5k|-|2021/11/25|
|17|[d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh)|《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被55个国家的300所大学用于教学。|28.3k|Python|2021/12/01|
|18|[unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN)|《The Way to Go》中文译本，中文正式名《Go 入门指南》|28.2k|Go|2021/10/25|
|19|[formulahendry/955.WLB](https://github.com/formulahendry/955.WLB)|955 不加班的公司名单 - 工作 955，work–life balance (工作与生活的平衡)|28.0k|-|2021/11/22|
|20|[ityouknow/spring-boot-examples](https://github.com/ityouknow/spring-boot-examples)|about learning Spring Boot via examples. Spring Boot 教程、技术栈示例代码，快速简单上手教程。 |26.7k|Java|2021/11/29|
|21|[sentsin/layui](https://github.com/sentsin/layui)|采用自身模块规范编写的前端 UI 框架，遵循原生 HTML/CSS/JS 的书写形式，极低门槛，拿来即用。|25.4k|JavaScript|2021/10/26|
|22|[proxyee-down-org/proxyee-down](https://github.com/proxyee-down-org/proxyee-down)|http下载工具，基于http代理，支持多连接分块下载|25.3k|Java|2021/06/05|
|23|[mqyqingfeng/Blog](https://github.com/mqyqingfeng/Blog)|冴羽写博客的地方，预计写四个系列：JavaScript深入系列、JavaScript专题系列、ES6系列、React系列。|25.0k|-|2021/11/30|
|24|[lib-pku/libpku](https://github.com/lib-pku/libpku)|贵校课程资料民间整理|24.9k|TeX|2021/11/01|
|25|[hankcs/HanLP](https://github.com/hankcs/HanLP)|中文分词 词性标注 命名实体识别 依存句法分析 成分句法分析 语义依存分析 语义角色标注 指代消解 风格转换 语义相似度 新词发现 关键词短语提取 自动摘要 文本分类聚类 拼音简繁转换 自然语言处理|24.5k|Python|2021/11/25|
|26|[QSCTech/zju-icicles](https://github.com/QSCTech/zju-icicles)|浙江大学课程攻略共享计划|23.5k|HTML|2021/11/29|
|27|[fengdu78/Coursera-ML-AndrewNg-Notes](https://github.com/fengdu78/Coursera-ML-AndrewNg-Notes)|吴恩达老师的机器学习课程个人笔记|23.5k|HTML|2021/07/24|
|28|[jobbole/awesome-python-cn](https://github.com/jobbole/awesome-python-cn)|Python资源大全中文版，包括：Web框架、网络爬虫、模板引擎、数据库、数据可视化、图片处理等，由「开源前哨」和「Python开发者」微信公号团队维护更新。|23.3k|Makefile|2021/11/24|
|29|[scwang90/SmartRefreshLayout](https://github.com/scwang90/SmartRefreshLayout)|🔥下拉刷新、上拉加载、二级刷新、淘宝二楼、RefreshLayout、OverScroll，Android智能下拉刷新框架，支持越界回弹、越界拖动，具有极强的扩展性，集成了几十种炫酷的Header和 Footer。|23.1k|Java|2021/11/27|
|30|[YMFE/yapi](https://github.com/YMFE/yapi)|YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台|23.1k|JavaScript|2021/11/30|
|31|[lenve/vhr](https://github.com/lenve/vhr)|微人事是一个前后端分离的人力资源管理系统，项目采用SpringBoot+Vue开发。|22.8k|Java|2021/11/02|
|32|[hollischuang/toBeTopJavaer](https://github.com/hollischuang/toBeTopJavaer)|To Be Top Javaer - Java工程师成神之路|22.0k|Java|2021/10/27|
|33|[alibaba/easyexcel](https://github.com/alibaba/easyexcel)|快速、简单避免OOM的java处理Excel工具|21.9k|Java|2021/11/18|
|34|[Tencent/wepy](https://github.com/Tencent/wepy)|小程序组件化开发框架|21.6k|JavaScript|2021/11/16|
|35|[huihut/interview](https://github.com/huihut/interview)|📚 C/C++ 技术面试基础知识总结，包括语言、程序库、数据结构、算法、系统、网络、链接装载库等知识及面试经验、招聘、内推等信息。This repository is a summary of the basic knowledge of recruiting job seekers and beginners in the direction of C/C++ technology, including language, program library, data structure, algorithm, system, network, link loading library, in ...|21.4k|C++|2021/10/06|
|36|[googlehosts/hosts](https://github.com/googlehosts/hosts)|镜像：https://scaffrey.coding.net/p/hosts/git / https://git.qvq.network/googlehosts/hosts|20.8k|-|2021/06/20|
|37|[komeiji-satori/Dress](https://github.com/komeiji-satori/Dress)|好耶  是女装|20.6k|Standard ML|2021/11/28|
|38|[Meituan-Dianping/mpvue](https://github.com/Meituan-Dianping/mpvue)|基于 Vue.js 的小程序开发框架，从底层支持 Vue.js 语法和构建工具体系。|20.4k|JavaScript|2021/08/11|
|39|[halo-dev/halo](https://github.com/halo-dev/halo)|✍ 一款优秀的开源博客发布应用。|20.4k|Java|2021/11/30|
|40|[SwiftGGTeam/the-swift-programming-language-in-chinese](https://github.com/SwiftGGTeam/the-swift-programming-language-in-chinese)|中文版 Apple 官方 Swift 教程《The Swift Programming Language》|20.4k|CSS|2021/11/24|
|41|[julycoding/The-Art-Of-Programming-By-July](https://github.com/julycoding/The-Art-Of-Programming-By-July)|本项目曾冲到全球第一，干货集锦见本页面最底部，另完整精致的纸质版《编程之法：面试和算法心得》已在京东/当当上销售|20.2k|C|2021/07/03|
|42|[xuxueli/xxl-job](https://github.com/xuxueli/xxl-job)|A distributed task scheduling framework.（分布式任务调度平台XXL-JOB）|20.0k|Java|2021/11/24|
|43|[wangzheng0822/algo](https://github.com/wangzheng0822/algo)|数据结构和算法必知必会的50个代码实现|20.0k|Python|2021/11/15|
|44|[ruanyf/weekly](https://github.com/ruanyf/weekly)|科技爱好者周刊，每周五发布|19.9k|-|2021/11/26|
|45|[ruanyf/es6tutorial](https://github.com/ruanyf/es6tutorial)|《ECMAScript 6入门》是一本开源的 JavaScript 语言教程，全面介绍 ECMAScript 6 新增的语法特性。|19.7k|JavaScript|2021/11/30|
|46|[Light-City/CPlusPlusThings](https://github.com/Light-City/CPlusPlusThings)|C++那些事|16.9k|C++|2021/11/29|
|47|[CarGuo/GSYVideoPlayer](https://github.com/CarGuo/GSYVideoPlayer)|视频播放器（IJKplayer、ExoPlayer、MediaPlayer），HTTPS，支持弹幕，外挂字幕，支持滤镜、水印、gif截图，片头广告、中间广告，多个同时播放，支持基本的拖动，声音、亮度调节，支持边播边缓存，支持视频自带rotation的旋转（90,270之类），重力旋转与手动旋转的同步支持，支持列表播放 ，列表全屏动画，视频加载速度，列表小窗口支持拖动，动画效果，调整比例，多分辨率切换，支持切换播放器，进度条小窗口预览，列表切换详情页面无缝播放，rtsp、concat、mpeg。 |16.9k|Java|2021/11/29|
|48|[vnpy/vnpy](https://github.com/vnpy/vnpy)|基于Python的开源量化交易平台开发框架|16.9k|C++|2021/11/28|
|49|[ascoders/weekly](https://github.com/ascoders/weekly)|前端精读周刊。帮你理解最前沿、实用的技术。|16.8k|JavaScript|2021/11/29|
|50|[elunez/eladmin](https://github.com/elunez/eladmin)|项目基于 Spring Boot 2.1.0 、 Jpa、 Spring Security、redis、Vue的前后端分离的后台管理系统，项目采用分模块开发方式， 权限控制采用 RBAC，支持数据字典与数据权限管理，支持一键生成前后端代码，支持动态路由|16.8k|Java|2021/11/29|
|51|[MLEveryday/100-Days-Of-ML-Code](https://github.com/MLEveryday/100-Days-Of-ML-Code)|100-Days-Of-ML-Code中文版|16.7k|Jupyter Notebook|2021/08/11|
|52|[linlinjava/litemall](https://github.com/linlinjava/litemall)|又一个小商城。litemall = Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端|16.6k|Java|2021/10/07|
|53|[LingCoder/OnJava8](https://github.com/LingCoder/OnJava8)|《On Java 8》中文版 |16.5k|-|2021/09/25|
|54|[facert/awesome-spider](https://github.com/facert/awesome-spider)|爬虫集合|16.5k|-|2021/09/02|
|55|[chai2010/advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book)|:books: 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题(完稿)|16.3k|Go|2021/11/25|
|56|[dianping/cat](https://github.com/dianping/cat)|CAT 作为服务端项目基础组件，提供了 Java, C/C++, Node.js, Python, Go 等多语言客户端，已经在美团点评的基础架构中间件框架（MVC框架，RPC框架，数据库框架，缓存框架等，消息队列，配置系统等）深度集成，为美团点评各业务线提供系统丰富的性能指标、健康状况、实时告警等。|16.2k|Java|2021/11/22|
|57|[ZhongFuCheng3y/athena](https://github.com/ZhongFuCheng3y/athena)|:notebook:Java后端知识图谱|15.7k|-|2021/11/27|
|58|[jaywcjlove/linux-command](https://github.com/jaywcjlove/linux-command)|Linux命令大全搜索工具，内容包含Linux命令手册、详解、学习、搜集。https://git.io/linux|17.3k|Markdown|2021/12/01|
|59|[1c7/chinese-independent-developer](https://github.com/1c7/chinese-independent-developer)|👩🏿‍💻👨🏾‍💻👩🏼‍💻👨🏽‍💻👩🏻‍💻中国独立开发者项目列表 -- 分享大家都在做什么|17.2k|-|2021/11/24|
|60|[PaddlePaddle/Paddle](https://github.com/PaddlePaddle/Paddle)|PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice （『飞桨』核心框架，深度学习&机器学习高性能单机、分布式训练和跨平台部署）|17.1k|Python|2021/12/01|
|61|[alibaba/ice](https://github.com/alibaba/ice)|🚀 The Progressive Framework Based On React（基于 React 的渐进式研发框架）|16.9k|TypeScript|2021/12/01|
|62|[youzan/vant-weapp](https://github.com/youzan/vant-weapp)|轻量、可靠的小程序 UI 组件库|15.4k|JavaScript|2021/12/01|
|63|[EastWorld/wechat-app-mall](https://github.com/EastWorld/wechat-app-mall)|微信小程序商城，微信小程序微店|15.3k|JavaScript|2021/11/30|
|64|[233boy/v2ray](https://github.com/233boy/v2ray)|最好用的 V2Ray 一键安装脚本 & 管理脚本|15.0k|Shell|2021/08/24|
|65|[nndl/nndl.github.io](https://github.com/nndl/nndl.github.io)|《神经网络与深度学习》 邱锡鹏著 Neural Network and Deep Learning |15.0k|HTML|2021/05/18|
|66|[fengdu78/lihang-code](https://github.com/fengdu78/lihang-code)|《统计学习方法》的代码实现|14.9k|Jupyter Notebook|2021/05/31|
|67|[zhongyang219/TrafficMonitor](https://github.com/zhongyang219/TrafficMonitor)|这是一个用于显示当前网速、CPU及内存利用率的桌面悬浮窗软件，并支持任务栏显示，支持更换皮肤。|14.7k|C++|2021/11/13|
|68|[shimohq/chinese-programmer-wrong-pronunciation](https://github.com/shimohq/chinese-programmer-wrong-pronunciation)|中国程序员容易发音错误的单词|14.7k|Python|2021/07/29|
|69|[JeffLi1993/springboot-learning-example](https://github.com/JeffLi1993/springboot-learning-example)|spring boot 实践学习案例，是 spring boot 初学者及核心技术巩固的最佳实践。另外写博客，用 OpenWrite。|14.6k|Java|2021/11/01|
|70|[521xueweihan/git-tips](https://github.com/521xueweihan/git-tips)|:trollface:Git的奇技淫巧|13.5k|-|2021/11/02|
|71|[Bigkoo/Android-PickerView](https://github.com/Bigkoo/Android-PickerView)|This is a picker view for android , support linkage effect, timepicker and optionspicker.（时间选择器、省市区三级联动）|13.0k|Java|2021/06/28|
|72|[greyireland/algorithm-pattern](https://github.com/greyireland/algorithm-pattern)|算法模板，最科学的刷题方式，最快速的刷题路径，你值得拥有~|12.9k|Go|2021/05/14|
|73|[CarGuo/gsy_github_app_flutter](https://github.com/CarGuo/gsy_github_app_flutter)|Flutter 超完整的开源项目，功能丰富，适合学习和日常使用。GSYGithubApp系列的优势：我们目前已经拥有Flutter、Weex、ReactNative、kotlin 四个版本。 功能齐全，项目框架内技术涉及面广，完成度高，持续维护，配套文章，适合全面学习，对比参考。跨平台的开源Github客户端App，更好的体验，更丰富的功能，旨在更好的日常管理和维护个人Github，提供更好更方便的驾车体验Σ(￣。￣ﾉ)ﾉ。同款Weex版本 ： https://github.com/CarGuo/GSYGithubAppWeex    、同款React Native版本 ： https://g ...|12.9k|Dart|2021/11/28|
|74|[babysor/MockingBird](https://github.com/babysor/MockingBird)|🚀AI拟声: 5秒内克隆您的声音并生成任意语音内容 Clone a voice in 5 seconds to generate arbitrary speech in real-time|12.9k|JavaScript|2021/11/29|
|75|[Qv2ray/Qv2ray](https://github.com/Qv2ray/Qv2ray)|:star: Linux / Windows / macOS 跨平台 V2Ray 客户端   支持 VMess / VLESS / SSR / Trojan / Trojan-Go / NaiveProxy / HTTP / HTTPS / SOCKS5   使用 C++ / Qt 开发   可拓展插件式设计 :star:|12.8k|C++|2021/08/17|
|76|[Curzibn/Luban](https://github.com/Curzibn/Luban)|Luban(鲁班)—Image compression with efficiency very close to WeChat Moments/可能是最接近微信朋友圈的图片压缩算法|12.8k|Java|2021/11/06|
|77|[wangeditor-team/wangEditor](https://github.com/wangeditor-team/wangEditor)|wangEditor —— 轻量级web富文本框|12.7k|TypeScript|2021/12/01|
|78|[YunaiV/SpringBoot-Labs](https://github.com/YunaiV/SpringBoot-Labs)|一个涵盖六个专栏：Spring Boot 2.X、Spring Cloud、Spring Cloud Alibaba、Dubbo、分布式消息队列、分布式事务的仓库。希望胖友小手一抖，右上角来个 Star，感恩 1024|12.6k|Java|2021/09/21|
|79|[eip-work/kuboard-press](https://github.com/eip-work/kuboard-press)|Kuboard 是基于 Kubernetes 的微服务管理界面。同时提供 Kubernetes 免费中文教程，入门教程，最新版本的 Kubernetes v1.20 安装手册，(k8s install) 在线答疑，持续更新。|12.4k|JavaScript|2021/11/30|
|80|[huiyadanli/RevokeMsgPatcher](https://github.com/huiyadanli/RevokeMsgPatcher)|:trollface: A hex editor for WeChat/QQ/TIM - PC版微信/QQ/TIM防撤回补丁（我已经看到了，撤回也没用了）|12.3k|C#|2021/09/17|
|81|[cloudreve/Cloudreve](https://github.com/cloudreve/Cloudreve)|🌩支持多家云存储的云盘系统 (Self-deployed file management and sharing system, supports multiple storage providers)|12.1k|Go|2021/11/30|
|82|[dragen1860/Deep-Learning-with-TensorFlow-book](https://github.com/dragen1860/Deep-Learning-with-TensorFlow-book)|深度学习入门开源书，基于TensorFlow 2.0案例实战。Open source Deep Learning book, based on TensorFlow 2.0 framework.|12.1k|Jupyter Notebook|2021/08/30|
|83|[JessYanCoding/AndroidAutoSize](https://github.com/JessYanCoding/AndroidAutoSize)|🔥 A low-cost Android screen adaptation solution (今日头条屏幕适配方案终极版，一个极低成本的 Android 屏幕适配方案).|12.0k|Java|2021/06/24|
|84|[leisurelicht/wtfpython-cn](https://github.com/leisurelicht/wtfpython-cn)|wtfpython的中文翻译/施工结束/ 能力有限，欢迎帮我改进翻译|12.0k|Python|2021/07/08|
|85|[itdevbooks/pdf](https://github.com/itdevbooks/pdf)|编程电子书，电子书，编程书籍，包括C，C#，Docker，Elasticsearch，Git，Hadoop，HeadFirst，Java，Javascript，jvm，Kafka，Linux，Maven，MongoDB，MyBatis，MySQL，Netty，Nginx，Python，RabbitMQ，Redis，Scala，Solr，Spark，Spring，SpringBoot，SpringCloud，TCPIP，Tomcat，Zookeeper，人工智能，大数据类，并发编程，数据库类，数据挖掘，新面试题，架构设计，算法系列，计算机类，设计模式，软件测试，重构优化，等更多分类|11.7k|-|2021/11/24|
|86|[youth5201314/banner](https://github.com/youth5201314/banner)|🔥🔥🔥Banner 2.0 来了！Android广告图片轮播控件，内部基于ViewPager2实现，Indicator和UI都可以自定义。|11.6k|Java|2021/07/11|
|87|[modood/Administrative-divisions-of-China](https://github.com/modood/Administrative-divisions-of-China)|中华人民共和国行政区划：省级（省份直辖市自治区）、 地级（城市）、 县级（区县）、 乡级（乡镇街道）、 村级（村委会居委会） ，中国省市区镇村二级三级四级五级联动地址数据。|11.6k|JavaScript|2021/07/15|
|88|[Tamsiree/RxTool](https://github.com/Tamsiree/RxTool)|Android开发人员不得不收集的工具类集合   支付宝支付   微信支付（统一下单）   微信分享   Zip4j压缩（支持分卷压缩与加密）   一键集成UCrop选择圆形头像   一键集成二维码和条形码的扫描与生成   常用Dialog   WebView的封装可播放视频   仿斗鱼滑动验证码   Toast封装   震动   GPS   Location定位   图片缩放   Exif 图片添加地理位置信息（经纬度）   蛛网等级   颜色选择器   ArcGis   VTPK   编译运行一下说不定会找到惊喜|11.6k|Kotlin|2021/06/05|
|89|[chokcoco/iCSS](https://github.com/chokcoco/iCSS)|不止于 CSS|11.5k|CSS|2021/11/29|
|90|[pjialin/py12306](https://github.com/pjialin/py12306)|🚂 12306 购票助手，支持集群，多账号，多任务购票以及 Web 页面管理 |11.5k|Python|2021/10/21|
|91|[answershuto/learnVue](https://github.com/answershuto/learnVue)|:octocat:Vue.js 源码解析|11.5k|JavaScript|2021/07/26|
|92|[bailicangdu/vue2-manage](https://github.com/bailicangdu/vue2-manage)|基于 vue + element-ui 的后台管理系统|11.3k|Vue|2021/07/15|
|93|[zhisheng17/flink-learning](https://github.com/zhisheng17/flink-learning)|flink learning blog. http://www.54tianzhisheng.cn/  含 Flink 入门、概念、原理、实战、性能调优、源码解析等内容。涉及 Flink Connector、Metrics、Library、DataStream API、Table API & SQL 等内容的学习案例，还有 Flink 落地应用的大型项目案例（PVUV、日志存储、百亿数据实时去重、监控告警）分享。欢迎大家支持我的专栏《大数据实时计算引擎 Flink 实战与性能优化》|11.3k|Java|2021/10/08|
|94|[LuckSiege/PictureSelector](https://github.com/LuckSiege/PictureSelector)|Picture Selector Library for Android  or 图片选择器|11.0k|Java|2021/11/29|
|95|[meolu/walle-web](https://github.com/meolu/walle-web)|walle - 瓦力 Devops开源项目代码部署平台|11.3k|Python|2021/05/09|
|96|[snail007/goproxy](https://github.com/snail007/goproxy)|🔥  Proxy is a high performance HTTP(S) proxies, SOCKS5 proxies,WEBSOCKET, TCP, UDP proxy server implemented by golang. Now, it supports chain-style proxies,nat forwarding in different lan,TCP/UDP port forwarding, SSH forwarding.Proxy是golang实现的高性能http,https,websocket,tcp,socks5代理服务器,支持内网穿透,链式代理,通讯加密, ...|11.2k|Go|2021/11/29|
|97|[seaswalker/spring-analysis](https://github.com/seaswalker/spring-analysis)|Spring源码阅读|11.2k|Java|2021/06/05|
|98|[bailicangdu/node-elm](https://github.com/bailicangdu/node-elm)|基于 node.js + Mongodb 构建的后台系统|11.2k|JavaScript|2021/09/01|
|99|[fangzesheng/free-api](https://github.com/fangzesheng/free-api)|收集免费的接口服务,做一个api的搬运工|10.9k|-|2021/11/09|
|100|[weilanwl/ColorUI](https://github.com/weilanwl/ColorUI)|鲜亮的高饱和色彩，专注视觉的小程序组件库|10.9k|Vue|2021/11/17|
|101|[heibaiying/BigData-Notes](https://github.com/heibaiying/BigData-Notes)|大数据入门指南  :star:|10.9k|Java|2021/05/12|
|102|[Tim9Liu9/TimLiu-iOS](https://github.com/Tim9Liu9/TimLiu-iOS)|iOS开发常用三方库、插件、知名博客等等|10.9k|-|2021/10/21|
|103|[chuzhixin/vue-admin-beautiful-pro](https://github.com/chuzhixin/vue-admin-beautiful-pro)|🚀🚀🚀vue3 admin,vue3.0 admin,vue后台管理,vue-admin,vue3.0-admin,admin,vue-admin,vue-element-admin,ant-design，vue-admin-beautiful-pro,vab admin pro,vab admin plus主线版本基于element-plus、element-ui、ant-design-vue三者并行开发维护，同时支持电脑，手机，平板，切换分支查看不同的vue版本，element-plus版本已发布(vue3,vue3.0,vue,vue3.x,vue.js)|10.8k|Vue|2021/11/26|
|104|[imarvinle/awesome-cs-books](https://github.com/imarvinle/awesome-cs-books)|经典编程书籍大全，涵盖：计算机系统与网络、系统架构、算法与数据结构、前端开发、后端开发、移动开发、数据库、测试、项目与团队、程序员职业修炼、求职面试等|10.7k|-|2021/11/21|
|105|[opendigg/awesome-github-vue](https://github.com/opendigg/awesome-github-vue)|Vue相关开源项目库汇总|10.7k|-|2021/11/16|
|106|[hyb1996/Auto.js](https://github.com/hyb1996/Auto.js)|A UiAutomator on android, does not need root access(安卓平台上的JavaScript自动化工具)|10.8k|Java|2021/11/16|
|107|[pagehelper/Mybatis-PageHelper](https://github.com/pagehelper/Mybatis-PageHelper)|Mybatis通用分页插件|10.8k|Java|2021/11/03|
|108|[NLP-LOVE/ML-NLP](https://github.com/NLP-LOVE/ML-NLP)|此项目是机器学习(Machine Learning)、深度学习(Deep Learning)、NLP面试中常考到的知识点和代码实现，也是作为一个算法工程师必会的理论基础知识。|10.7k|Jupyter Notebook|2021/06/26|
|109|[sparanoid/chinese-copywriting-guidelines](https://github.com/sparanoid/chinese-copywriting-guidelines)|Chinese copywriting guidelines for better written communication／中文文案排版指北|10.6k|-|2021/11/25|
|110|[apachecn/apachecn-algo-zh](https://github.com/apachecn/apachecn-algo-zh)|ApacheCN 数据结构与算法译文集|10.5k|JavaScript|2021/05/09|
|111|[doocs/leetcode](https://github.com/doocs/leetcode)|😏 LeetCode solutions in any programming language   多种编程语言实现 LeetCode、《剑指 Offer（第 2 版）》、《程序员面试金典（第 6 版）》题解|7.7k|Java|2021/11/30|
|112|[hs-web/hsweb-framework](https://github.com/hs-web/hsweb-framework)|hsweb (haʊs wɛb) 是一个基于spring-boot 2.x开发 ,首个使用全响应式编程的企业级后台管理系统基础项目。|7.7k|Java|2021/11/26|
|113|[doocs/jvm](https://github.com/doocs/jvm)|🤗 JVM 底层原理最全知识总结|7.7k|-|2021/11/10|
|114|[dromara/Sa-Token](https://github.com/dromara/Sa-Token)|这可能是史上功能最全的Java权限认证框架！目前已集成——登录认证、权限认证、分布式Session会话、微服务网关鉴权、单点登录、OAuth2.0、踢人下线、Redis集成、前后台分离、记住我模式、模拟他人账号、临时身份切换、账号封禁、多账号认证体系、注解式鉴权、路由拦截式鉴权、花式token生成、自动续签、同端互斥登录、会话治理、密码加密、jwt集成、Spring集成、WebFlux集成...|7.3k|Java|2021/11/30|
|115|[thx/rap2-delos](https://github.com/thx/rap2-delos)|阿里妈妈前端团队出品的开源接口管理工具RAP第二代|7.2k|TypeScript|2021/11/26|
|116|[daniulive/SmarterStreaming](https://github.com/daniulive/SmarterStreaming)|业内为数不多致力于极致体验的超强全自研跨平台(windows/android/iOS)流媒体内核，通过模块化自由组合，支持实时RTMP推流、RTSP推流、RTMP播放器、RTSP播放器、录像、多路流媒体转发、音视频导播、动态视频合成、音频混音、直播互动、内置轻量级RTSP服务等，比快更快，业界真正靠谱的超低延迟直播SDK(1秒内，低延迟模式下200~400ms)。|9.8k|Java|2021/11/11|
|117|[the1812/Bilibili-Evolved](https://github.com/the1812/Bilibili-Evolved)|强大的哔哩哔哩增强脚本: 下载视频, 音乐, 封面, 弹幕 / 简化直播间, 评论区, 首页 / 自定义顶栏, 删除广告, 夜间模式 / 触屏设备支持|9.7k|TypeScript|2021/12/01|
|118|[w7corp/easywechat](https://github.com/w7corp/easywechat)|:package: 一个 PHP 微信 SDK|9.7k|PHP|2021/11/29|
|119|[yifeikong/reverse-interview-zh](https://github.com/yifeikong/reverse-interview-zh)|技术面试最后反问面试官的话|9.6k|-|2021/10/13|
|120|[XIU2/TrackersListCollection](https://github.com/XIU2/TrackersListCollection)|🎈 Updated daily! A list of popular BitTorrent Trackers! / 每天更新！全网热门 BT Tracker 列表！|9.5k|-|2021/12/01|
|121|[nusr/hacker-laws-zh](https://github.com/nusr/hacker-laws-zh)|💻📖对开发人员有用的定律、理论、原则和模式。(Laws, Theories, Principles and Patterns that developers will find useful.)|9.4k|-|2021/06/19|
|122|[zhaoolee/ChineseBQB](https://github.com/zhaoolee/ChineseBQB)|🇨🇳 Chinese sticker pack,More joy / 表情包的博物馆, Github最有毒的仓库, 中国表情包大集合, 聚欢乐~|9.4k|JavaScript|2021/10/23|
|123|[chefyuan/algorithm-base](https://github.com/chefyuan/algorithm-base)|专门为刚开始刷题的同学准备的算法基地，没有最细只有更细，立志用动画将晦涩难懂的算法说的通俗易懂！|9.4k|Java|2021/11/07|
|124|[hackstoic/golang-open-source-projects](https://github.com/hackstoic/golang-open-source-projects)|为互联网IT人打造的中文版awesome-go|9.3k|-|2021/05/13|
|125|[Tencent/secguide](https://github.com/Tencent/secguide)|面向开发人员梳理的代码安全指南|9.3k|-|2021/10/06|
|126|[AlloyTeam/Mars](https://github.com/AlloyTeam/Mars)|腾讯移动 Web 前端知识库|9.2k|-|2021/09/03|
|127|[cnodejs/nodeclub](https://github.com/cnodejs/nodeclub)|:baby_chick:Nodeclub 是使用 Node.js 和 MongoDB 开发的社区系统|9.2k|JavaScript|2021/11/13|
|128|[OI-wiki/OI-wiki](https://github.com/OI-wiki/OI-wiki)|:star2: Wiki of OI / ICPC for everyone. （某大型游戏线上攻略，内含炫酷算术魔法）|9.1k|-|2021/12/01|
|129|[crossoverJie/cim](https://github.com/crossoverJie/cim)|📲cim(cross IM) 适用于开发者的分布式即时通讯系统|8.1k|Java|2021/10/12|
|130|[extreme-assistant/CVPR2021-Paper-Code-Interpretation](https://github.com/extreme-assistant/CVPR2021-Paper-Code-Interpretation)|cvpr2021/cvpr2020/cvpr2019/cvpr2018/cvpr2017 论文/代码/解读/直播合集，极市团队整理|8.0k|-|2021/08/23|
|131|[mamoe/mirai](https://github.com/mamoe/mirai)|高效率 QQ 机器人支持库|7.9k|Kotlin|2021/11/30|
|132|[getgridea/gridea](https://github.com/getgridea/gridea)|✍️ A static blog writing client (一个静态博客写作客户端)|7.9k|Less|2021/11/02|
|133|[hoochanlon/w3-goto-world](https://github.com/hoochanlon/w3-goto-world)|🍅 Git/AWS/Google 镜像 ,SS/SSR/VMESS节点,WireGuard,IPFS, DeepWeb,Capitalism 、行业研究报告的知识储备库|7.9k|Python|2021/09/23|
|134|[skywind3000/awesome-cheatsheets](https://github.com/skywind3000/awesome-cheatsheets)|超级速查表 - 编程语言、框架和开发工具的速查表，单个文件包含一切你需要知道的东西 :zap:|7.9k|Shell|2021/10/30|
|135|[evil-huawei/evil-huawei](https://github.com/evil-huawei/evil-huawei)|Evil Huawei - 华为作过的恶|7.8k|JavaScript|2021/07/02|
|136|[macrozheng/mall-swarm](https://github.com/macrozheng/mall-swarm)|mall-swarm是一套微服务商城系统，采用了 Spring Cloud Hoxton & Alibaba、Spring Boot 2.3、Oauth2、MyBatis、Docker、Elasticsearch、Kubernetes等核心技术，同时提供了基于Vue的管理后台方便快速搭建系统。mall-swarm在电商业务的基础集成了注册中心、配置中心、监控中心、网关等系统功能。文档齐全，附带全套Spring Cloud教程。|7.8k|Java|2021/08/23|
|137|[banchichen/TZImagePickerController](https://github.com/banchichen/TZImagePickerController)|一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。  A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+|7.7k|Objective-C|2021/11/06|
|138|[DayBreak-u/chineseocr_lite](https://github.com/DayBreak-u/chineseocr_lite)|超轻量级中文ocr，支持竖排文字识别, 支持ncnn、mnn、tnn推理 ( dbnet(1.8M) + crnn(2.5M) + anglenet(378KB)) 总模型仅4.7M |8.2k|C++|2021/11/24|
|139|[sentsin/layer](https://github.com/sentsin/layer)|丰富多样的 Web 弹出层组件，可轻松实现 Alert/Confirm/Prompt/ 普通提示/页面区块/iframe/tips等等几乎所有的弹出交互。目前已成为最多人使用的弹层解决方案|8.2k|JavaScript|2021/05/22|
|140|[newbee-ltd/newbee-mall](https://github.com/newbee-ltd/newbee-mall)|🔥 🎉newbee-mall 项目（新蜂商城）是一套电商系统，包括 newbee-mall 商城系统及 newbee-mall-admin 商城后台管理系统，基于 Spring Boot 2.X 及相关技术栈开发。 前台商城系统包含首页门户、商品分类、新品上线、首页轮播、商品推荐、商品搜索、商品展示、购物车、订单结算、订单流程、个人订单管理、会员中心、帮助中心等模块。 后台管理系统包含数据面板、轮播图管理、商品管理、订单管理、会员管理、分类管理、设置等模块。|8.2k|Java|2021/11/26|
|141|[guyueyingmu/avbook](https://github.com/guyueyingmu/avbook)|AV 电影管理系统， avmoo , javbus , javlibrary 爬虫，线上 AV 影片图书馆，AV 磁力链接数据库，Japanese Adult Video Library,Adult Video Magnet Links - Japanese Adult Video Database|8.1k|PHP|2021/10/06|
|142|[521xueweihan/GitHub520](https://github.com/521xueweihan/GitHub520)|:kissing_heart: 让你“爱”上 GitHub，解决访问时图裂、加载慢的问题。（无需安装）|8.4k|Python|2021/12/01|
|143|[Dod-o/Statistical-Learning-Method_Code](https://github.com/Dod-o/Statistical-Learning-Method_Code)|手写实现李航《统计学习方法》书中全部算法|8.4k|Python|2021/09/13|
|144|[wistbean/learn_python3_spider](https://github.com/wistbean/learn_python3_spider)|python爬虫教程系列、从0到1学习python爬虫，包括浏览器抓包，手机APP抓包，如 fiddler、mitmproxy，各种爬虫涉及的模块的使用，如：requests、beautifulSoup、selenium、appium、scrapy等，以及IP代理，验证码识别，Mysql，MongoDB数据库的python使用，多线程多进程爬虫的使用，css 爬虫加密逆向破解，JS爬虫逆向，分布式爬虫，爬虫项目实战实例等|8.4k|Python|2021/11/16|
|145|[crawlab-team/crawlab](https://github.com/crawlab-team/crawlab)|Distributed web crawler admin platform for spiders management regardless of languages and frameworks. 分布式爬虫管理平台，支持任何语言和框架|8.3k|Go|2021/11/22|
|146|[xcatliu/typescript-tutorial](https://github.com/xcatliu/typescript-tutorial)|TypeScript 入门教程|8.3k|TypeScript|2021/09/10|
|147|[litten/hexo-theme-yilia](https://github.com/litten/hexo-theme-yilia)|一个简洁优雅的hexo主题  A simple and elegant theme for hexo.|8.2k|JavaScript|2021/06/05|
|148|[doocs/source-code-hunter](https://github.com/doocs/source-code-hunter)|😱 从源码层面，剖析挖掘互联网行业主流技术的底层实现原理，为广大开发者 “提升技术深度” 提供便利。目前开放 Spring 全家桶，Mybatis、Netty、Dubbo 框架，及 Redis、Tomcat 中间件等|7.3k|Java|2021/09/13|
|149|[injetlee/Python](https://github.com/injetlee/Python)|Python脚本。模拟登录知乎， 爬虫，操作excel，微信公众号，远程开机|7.3k|Python|2021/10/13|
|150|[xirong/my-git](https://github.com/xirong/my-git)|Individual collecting material of learning git（有关 git 的学习资料）|7.1k|-|2021/08/24|
|151|[gedoor/MyBookshelf](https://github.com/gedoor/MyBookshelf)|阅读是一款可以自定义来源阅读网络内容的工具，为广大网络文学爱好者提供一种方便、快捷舒适的试读体验。|7.1k|Java|2021/10/26|
|152|[JeffreySu/WeiXinMPSDK](https://github.com/JeffreySu/WeiXinMPSDK)|微信全平台 SDK Senparc.Weixin for C#，支持 .NET Framework 及 .NET Core、.NET 6.0。已支持微信公众号、小程序、小游戏、企业号、企业微信、开放平台、微信支付、JSSDK、微信周边等全平台。 WeChat SDK for C#.|7.1k|C#|2021/11/30|
|153|[panjf2000/ants](https://github.com/panjf2000/ants)|🐜🐜🐜 ants is a high-performance and low-cost goroutine pool in Go, inspired by fasthttp./ ants 是一个高性能且低损耗的 goroutine 池。|7.0k|Go|2021/11/30|
|154|[amusi/CVPR2021-Papers-with-Code](https://github.com/amusi/CVPR2021-Papers-with-Code)|CVPR 2021 论文和开源项目合集|7.0k|-|2021/11/15|
|155|[zhoutaoo/SpringCloud](https://github.com/zhoutaoo/SpringCloud)|基于SpringCloud2.1的微服务开发脚手架，整合了spring-security-oauth2、nacos、feign、sentinel、springcloud-gateway等。服务治理方面引入elasticsearch、skywalking、springboot-admin、zipkin等，让项目开发快速进入业务开发，而不需过多时间花费在架构搭建上。持续更新中|7.0k|Java|2021/09/04|
|156|[wulabing/Xray_onekey](https://github.com/wulabing/Xray_onekey)|Xray 基于 Nginx 的 VLESS + XTLS 一键安装脚本 |7.0k|Shell|2021/11/09|
|157|[ACL4SSR/ACL4SSR](https://github.com/ACL4SSR/ACL4SSR)|SSR 去广告ACL规则/SS完整GFWList规则/Clash规则碎片，Telegram频道订阅地址|6.9k|-|2021/11/28|
|158|[KunMinX/Jetpack-MVVM-Best-Practice](https://github.com/KunMinX/Jetpack-MVVM-Best-Practice)|是 难得一见 的 Jetpack MVVM 最佳实践！在 以简驭繁 的代码中，对 视图控制器 乃至 标准化开发模式 形成正确、深入的理解！|6.9k|Java|2021/11/27|
|159|[taowen/awesome-lowcode](https://github.com/taowen/awesome-lowcode)|国内低代码平台从业者交流|6.9k|-|2021/12/01|
|160|[goldze/MVVMHabit](https://github.com/goldze/MVVMHabit)|👕基于谷歌最新AAC架构，MVVM设计模式的一套快速开发库，整合Okhttp+RxJava+Retrofit+Glide等主流模块，满足日常开发需求。使用该框架可以快速开发一个高质量、易维护的Android应用。|6.8k|Java|2021/11/27|
|161|[kaina404/FlutterDouBan](https://github.com/kaina404/FlutterDouBan)|🔥🔥🔥Flutter豆瓣客户端,Awesome Flutter Project,全网最100%还原豆瓣客户端。首页、书影音、小组、市集及个人中心，一个不拉。（ https://img.xuvip.top/douyademo.mp4）|6.7k|Dart|2021/08/01|
|162|[openspug/spug](https://github.com/openspug/spug)|开源运维平台：面向中小型企业设计的轻量级无Agent的自动化运维平台，整合了主机管理、主机批量执行、主机在线终端、文件在线上传下载、应用发布部署、在线任务计划、配置中心、监控、报警等一系列功能。|6.7k|JavaScript|2021/11/30|
|163|[renzifeng/ZFPlayer](https://github.com/renzifeng/ZFPlayer)|Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)|6.7k|Objective-C|2021/11/04|
|164|[Mikoto10032/DeepLearning](https://github.com/Mikoto10032/DeepLearning)|深度学习入门教程, 优秀文章, Deep Learning Tutorial|6.7k|Jupyter Notebook|2021/10/21|
|165|[opsnull/follow-me-install-kubernetes-cluster](https://github.com/opsnull/follow-me-install-kubernetes-cluster)|和我一步步部署 kubernetes 集群|6.6k|Shell|2021/06/04|
|166|[ustbhuangyi/vue-analysis](https://github.com/ustbhuangyi/vue-analysis)|:thumbsup: Vue.js 源码分析|6.6k|JavaScript|2021/10/06|
|167|[ChenYilong/CYLTabBarController](https://github.com/ChenYilong/CYLTabBarController)|[EN]It is an iOS UI module library for adding animation to iOS tabbar items and icons with Lottie, and adding a bigger center UITabBar Item.  [CN]【中国特色 TabBar】一行代码实现 Lottie 动画TabBar，支持中间带+号的TabBar样式，自带红点角标，支持动态刷新。【iOS13 & Dark Mode  & iPhone XS MAX supported】|6.6k|Objective-C|2021/08/30|
|168|[skyline75489/what-happens-when-zh_CN](https://github.com/skyline75489/what-happens-when-zh_CN)|What-happens-when 的中文翻译，原仓库 https://github.com/alex/what-happens-when|6.6k|-|2021/07/01|
|169|[fuzhengwei/CodeGuide](https://github.com/fuzhengwei/CodeGuide)|:books: 本代码库是作者小傅哥多年从事一线互联网 Java 开发的学习历程技术汇总，旨在为大家提供一个清晰详细的学习教程，侧重点更倾向编写Java核心内容。如果本仓库能为您提供帮助，请给予支持(关注、点赞、分享)！|6.6k|-|2021/11/29|
|170|[datawhalechina/leeml-notes](https://github.com/datawhalechina/leeml-notes)|李宏毅《机器学习》笔记，在线阅读地址：https://datawhalechina.github.io/leeml-notes|6.6k|-|2021/09/16|
|171|[lenve/VBlog](https://github.com/lenve/VBlog)|V部落，Vue+SpringBoot实现的多用户博客管理平台!|6.3k|Java|2021/11/30|
|172|[PaddlePaddle/models](https://github.com/PaddlePaddle/models)|Pre-trained and Reproduced Deep Learning Models （『飞桨』官方模型库，包含多种学术前沿和工业场景验证的深度学习模型）|6.3k|Python|2021/12/01|
|173|[Meituan-Dianping/walle](https://github.com/Meituan-Dianping/walle)|Android Signature V2 Scheme签名下的新一代渠道包打包神器|6.3k|Java|2021/09/07|
|174|[ymcui/Chinese-BERT-wwm](https://github.com/ymcui/Chinese-BERT-wwm)|Pre-Training with Whole Word Masking for Chinese BERT（中文BERT-wwm系列模型）|6.3k|Python|2021/11/04|
|175|[Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB](https://github.com/Linzaer/Ultra-Light-Fast-Generic-Face-Detector-1MB)| 💎1MB lightweight face detection model  (1MB轻量级人脸检测模型)|6.2k|Python|2021/11/26|
|176|[chaitin/xray](https://github.com/chaitin/xray)|一款完善的安全评估工具，支持常见 web 安全问题扫描和自定义 poc   使用之前务必先阅读文档|6.1k|Vue|2021/11/29|
|177|[nswbmw/node-in-debugging](https://github.com/nswbmw/node-in-debugging)|《Node.js 调试指南》|6.1k|-|2021/09/03|
|178|[nonstriater/Learn-Algorithms](https://github.com/nonstriater/Learn-Algorithms)|算法学习笔记|6.1k|C|2021/11/30|
|179|[selierlin/Share-SSR-V2ray](https://github.com/selierlin/Share-SSR-V2ray)|🃏 Free SS/SSR/V2ray 免费分享节点账号信息网站|5.9k|-|2021/11/20|
|180|[wangzhiwubigdata/God-Of-BigData](https://github.com/wangzhiwubigdata/God-Of-BigData)|专注大数据学习面试，大数据成神之路开启。Flink/Spark/Hadoop/Hbase/Hive...|5.9k|-|2021/11/20|
|181|[metersphere/metersphere](https://github.com/metersphere/metersphere)|MeterSphere 是一站式开源持续测试平台，覆盖测试管理、接口测试、性能测试等。搞测试，就选 MeterSphere！|5.9k|Java|2021/12/01|
|182|[ityouknow/spring-cloud-examples](https://github.com/ityouknow/spring-cloud-examples)|Spring Cloud 学习案例，服务发现、服务治理、链路追踪、服务监控等|5.8k|Java|2021/08/21|
|183|[senghoo/golang-design-pattern](https://github.com/senghoo/golang-design-pattern)|设计模式 Golang实现－《研磨设计模式》读书笔记|5.8k|Go|2021/11/10|
|184|[DataV-Team/DataV](https://github.com/DataV-Team/DataV)|Vue数据可视化组件库（类似阿里DataV，大屏数据展示），提供SVG的边框及装饰、图表、水位图、飞线图等组件，简单易用，长期更新(React版已发布)|5.8k|Vue|2021/11/29|
|185|[PaddlePaddle/Paddle-Lite](https://github.com/PaddlePaddle/Paddle-Lite)|Multi-platform high performance  deep learning inference engine (『飞桨』多平台高性能深度学习预测引擎）|5.8k|C++|2021/12/01|
|186|[guodongxiaren/README](https://github.com/guodongxiaren/README)|README文件语法解读，即Github Flavored Markdown语法介绍|5.8k|-|2021/10/16|
|187|[xiaolai/everyone-can-use-english](https://github.com/xiaolai/everyone-can-use-english)|人人都能用英语|5.8k|-|2021/10/28|
|188|[jobbole/awesome-cpp-cn](https://github.com/jobbole/awesome-cpp-cn)|C++ 资源大全中文版，标准库、Web应用框架、人工智能、数据库、图片处理、机器学习、日志、代码分析等。由「开源前哨」和「CPP开发者」微信公号团队维护更新。|5.8k|-|2021/10/15|
|189|[MrXujiang/h5-Dooring](https://github.com/MrXujiang/h5-Dooring)|H5 Page Maker, H5 Editor, LowCode. Make H5 as easy as building blocks.   让H5制作像搭积木一样简单, 轻松搭建H5页面, H5网站, PC端网站,LowCode平台.|5.7k|JavaScript|2021/11/14|
|190|[HcySunYang/vue-design](https://github.com/HcySunYang/vue-design)|📖 master分支：《渲染器》|5.7k|JavaScript|2021/10/11|
|191|[sparklemotion/nokogiri](https://github.com/sparklemotion/nokogiri)|Nokogiri (鋸) makes it easy and painless to work with XML and HTML from Ruby.|5.7k|C|2021/11/29|
|192|[shengcaishizhan/kkndme_tianya](https://github.com/shengcaishizhan/kkndme_tianya)|天涯 kkndme 神贴聊房价|5.7k|-|2021/11/13|
|193|[lining0806/PythonSpiderNotes](https://github.com/lining0806/PythonSpiderNotes)|Python入门网络爬虫之精华版|5.6k|Python|2021/06/21|
|194|[me115/design_patterns](https://github.com/me115/design_patterns)|图说设计模式|5.6k|C++|2021/08/10|
|195|[We5ter/Scanners-Box](https://github.com/We5ter/Scanners-Box)|A powerful hacker toolkit collected more than 10 categories of open source scanners from Github  - 安全行业从业者自研开源扫描器合辑|5.6k|-|2021/11/18|
|196|[javascript-tutorial/zh.javascript.info](https://github.com/javascript-tutorial/zh.javascript.info)|现代 JavaScript 教程（The Modern JavaScript Tutorial）|5.5k|HTML|2021/11/29|
|197|[pujiaxin33/JXCategoryView](https://github.com/pujiaxin33/JXCategoryView)|A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)|5.5k|Objective-C|2021/10/25|
|198|[liyifeng1994/ssm](https://github.com/liyifeng1994/ssm)|手把手教你整合最优雅SSM框架：SpringMVC + Spring + MyBatis|5.5k|Java|2021/07/15|
|199|[jpush/aurora-imui](https://github.com/jpush/aurora-imui)|General IM UI components. Android/iOS/RectNative ready.  通用 IM 聊天 UI 组件，已经同时支持 Android/iOS/RN。|5.5k|Java|2021/09/21|
|200|[JakHuang/form-generator](https://github.com/JakHuang/form-generator)|:sparkles:Element UI表单设计及代码生成器|5.4k|Vue|2021/11/21|

⬆ [回到目录](#内容目录)

<br/>

## Java

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[CyC2018/CS-Notes](https://github.com/CyC2018/CS-Notes)|:books: 技术面试必备基础知识、Leetcode、计算机操作系统、计算机网络、系统设计|142.5k|Java|2021/11/25|
|2|[Snailclimb/JavaGuide](https://github.com/Snailclimb/JavaGuide)|「Java学习+面试指南」一份涵盖大部分 Java 程序员所需要掌握的核心知识。准备 Java 面试，首选 JavaGuide！|114.0k|Java|2021/11/29|
|3|[MisterBooo/LeetCodeAnimation](https://github.com/MisterBooo/LeetCodeAnimation)|Demonstrate all the questions on LeetCode in the form of animation.（用动画的形式呈现解LeetCode题目的思路）|67.9k|Java|2021/06/24|
|4|[kon9chunkit/GitHub-Chinese-Top-Charts](https://github.com/kon9chunkit/GitHub-Chinese-Top-Charts)|:cn: GitHub中文排行榜，帮助你发现高分优秀中文项目、更高效地吸收国人的优秀经验成果；榜单每周更新一次，敬请关注！|40.8k|Java|2021/11/27|
|5|[geekxh/hello-algorithm](https://github.com/geekxh/hello-algorithm)|🌍 针对小白的算法训练   包括四部分：①.算法基础 ②.力扣图解 ③.大厂面经 ④.CS_汇总   附：1、千本开源电子书  2、百张技术思维导图（项目花了上百小时，希望可以点 star 支持，🌹感谢~）|29.3k|Java|2021/10/11|
|6|[alibaba/arthas](https://github.com/alibaba/arthas)|Alibaba Java Diagnostic Tool Arthas/Alibaba Java诊断利器Arthas|27.8k|Java|2021/11/30|
|7|[ityouknow/spring-boot-examples](https://github.com/ityouknow/spring-boot-examples)|about learning Spring Boot via examples. Spring Boot 教程、技术栈示例代码，快速简单上手教程。 |26.7k|Java|2021/11/29|
|8|[jeecgboot/jeecg-boot](https://github.com/jeecgboot/jeecg-boot)|「企业级低代码平台」前后端分离架构SpringBoot 2.x，SpringCloud，Ant Design&Vue，Mybatis-plus，Shiro，JWT。强大的代码生成器让前后端代码一键生成，无需写任何代码! 引领新的开发模式OnlineCoding->代码生成->手工MERGE，帮助Java项目解决70%重复工作，让开发更关注业务，既能快速提高效率，帮助公司节省成本，同时又不失灵活性。|26.1k|Java|2021/11/22|
|9|[proxyee-down-org/proxyee-down](https://github.com/proxyee-down-org/proxyee-down)|http下载工具，基于http代理，支持多连接分块下载|25.3k|Java|2021/06/05|
|10|[alibaba/druid](https://github.com/alibaba/druid)|阿里云计算平台DataWorks(https://help.aliyun.com/document_detail/137663.html) 团队出品，为监控而生的数据库连接池|24.8k|Java|2021/11/29|
|11|[xkcoding/spring-boot-demo](https://github.com/xkcoding/spring-boot-demo)|该项目已成功集成 actuator(监控)、admin(可视化监控)、logback(日志)、aopLog(通过AOP记录web请求日志)、统一异常处理(json级别和页面级别)、freemarker(模板引擎)、thymeleaf(模板引擎)、Beetl(模板引擎)、Enjoy(模板引擎)、JdbcTemplate(通用JDBC操作数据库)、JPA(强大的ORM框架)、mybatis(强大的ORM框架)、通用Mapper(快速操作Mybatis)、PageHelper(通用的Mybatis分页插件)、mybatis-plus(快速操作Mybatis)、BeetlSQL(强大的ORM框架)、u ...|24.1k|Java|2021/11/07|
|12|[scwang90/SmartRefreshLayout](https://github.com/scwang90/SmartRefreshLayout)|🔥下拉刷新、上拉加载、二级刷新、淘宝二楼、RefreshLayout、OverScroll，Android智能下拉刷新框架，支持越界回弹、越界拖动，具有极强的扩展性，集成了几十种炫酷的Header和 Footer。|23.1k|Java|2021/11/27|
|13|[qiurunze123/miaosha](https://github.com/qiurunze123/miaosha)|⭐⭐⭐⭐秒杀系统设计与实现.互联网工程师进阶与分析🙋🐓|23.1k|Java|2021/06/04|
|14|[hollischuang/toBeTopJavaer](https://github.com/hollischuang/toBeTopJavaer)|To Be Top Javaer - Java工程师成神之路|22.0k|Java|2021/10/27|
|15|[alibaba/easyexcel](https://github.com/alibaba/easyexcel)|快速、简单避免OOM的java处理Excel工具|21.9k|Java|2021/11/18|
|16|[halo-dev/halo](https://github.com/halo-dev/halo)|✍ 一款优秀的开源博客发布应用。|20.4k|Java|2021/11/30|
|17|[xuxueli/xxl-job](https://github.com/xuxueli/xxl-job)|A distributed task scheduling framework.（分布式任务调度平台XXL-JOB）|20.0k|Java|2021/11/24|
|18|[didi/DoraemonKit](https://github.com/didi/DoraemonKit)|一款面向泛前端产品研发全生命周期的效率平台。|18.2k|Java|2021/11/30|
|19|[alibaba/Sentinel](https://github.com/alibaba/Sentinel)|A powerful flow control component enabling reliability, resilience and monitoring for microservices. (面向云原生微服务的高可用流控防护组件)|17.9k|Java|2021/12/01|
|20|[CarGuo/GSYVideoPlayer](https://github.com/CarGuo/GSYVideoPlayer)|视频播放器（IJKplayer、ExoPlayer、MediaPlayer），HTTPS，支持弹幕，外挂字幕，支持滤镜、水印、gif截图，片头广告、中间广告，多个同时播放，支持基本的拖动，声音、亮度调节，支持边播边缓存，支持视频自带rotation的旋转（90,270之类），重力旋转与手动旋转的同步支持，支持列表播放 ，列表全屏动画，视频加载速度，列表小窗口支持拖动，动画效果，调整比例，多分辨率切换，支持切换播放器，进度条小窗口预览，列表切换详情页面无缝播放，rtsp、concat、mpeg。 |16.9k|Java|2021/11/29|
|21|[linlinjava/litemall](https://github.com/linlinjava/litemall)|又一个小商城。litemall = Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端|16.6k|Java|2021/10/07|
|22|[dianping/cat](https://github.com/dianping/cat)|CAT 作为服务端项目基础组件，提供了 Java, C/C++, Node.js, Python, Go 等多语言客户端，已经在美团点评的基础架构中间件框架（MVC框架，RPC框架，数据库框架，缓存框架等，消息队列，配置系统等）深度集成，为美团点评各业务线提供系统丰富的性能指标、健康状况、实时告警等。|16.2k|Java|2021/11/22|
|23|[shuzheng/zheng](https://github.com/shuzheng/zheng)|基于Spring+SpringMVC+Mybatis分布式敏捷开发系统架构，提供整套公共微服务服务模块：集中权限管理（单点登录）、内容管理、支付中心、用户管理（支持第三方登录）、微信平台、存储系统、配置中心、日志分析、任务和通知等，支持服务治理、监控和追踪，努力为中小型企业打造全方位J2EE企业级开发解决方案。|16.1k|Java|2021/09/21|
|24|[JeffLi1993/springboot-learning-example](https://github.com/JeffLi1993/springboot-learning-example)|spring boot 实践学习案例，是 spring boot 初学者及核心技术巩固的最佳实践。另外写博客，用 OpenWrite。|14.6k|Java|2021/11/01|
|25|[dyc87112/SpringBoot-Learning](https://github.com/dyc87112/SpringBoot-Learning)|《Spring Boot基础教程》，2.x版本持续连载中！点击下方链接直达教程目录！|13.8k|Java|2021/11/09|
|26|[alibaba/ARouter](https://github.com/alibaba/ARouter)|💪 A framework for assisting in the renovation of Android componentization (帮助 Android App 进行组件化改造的路由框架)|13.6k|Java|2021/11/27|
|27|[Tencent/QMUI_Android](https://github.com/Tencent/QMUI_Android)|提高 Android UI 开发效率的 UI 库|13.3k|Java|2021/11/29|
|28|[Bigkoo/Android-PickerView](https://github.com/Bigkoo/Android-PickerView)|This is a picker view for android , support linkage effect, timepicker and optionspicker.（时间选择器、省市区三级联动）|13.0k|Java|2021/06/28|
|29|[Curzibn/Luban](https://github.com/Curzibn/Luban)|Luban(鲁班)—Image compression with efficiency very close to WeChat Moments/可能是最接近微信朋友圈的图片压缩算法|12.8k|Java|2021/11/06|
|30|[YunaiV/SpringBoot-Labs](https://github.com/YunaiV/SpringBoot-Labs)|一个涵盖六个专栏：Spring Boot 2.X、Spring Cloud、Spring Cloud Alibaba、Dubbo、分布式消息队列、分布式事务的仓库。希望胖友小手一抖，右上角来个 Star，感恩 1024|12.6k|Java|2021/09/21|
|31|[justauth/JustAuth](https://github.com/justauth/JustAuth)|🏆Gitee 最有价值开源项目 🚀:100: 小而全而美的第三方登录开源组件。目前已支持Github、Gitee、微博、钉钉、百度、Coding、腾讯云开发者平台、OSChina、支付宝、QQ、微信、淘宝、Google、Facebook、抖音、领英、小米、微软、今日头条、Teambition、StackOverflow、Pinterest、人人、华为、企业微信、酷家乐、Gitlab、美团、饿了么、推特、飞书、京东、阿里云、喜马拉雅、Amazon、Slack和 Line 等第三方平台的授权登录。 Login, so easy!|12.5k|Java|2021/10/18|
|32|[Tencent/APIJSON](https://github.com/Tencent/APIJSON)|🚀 零代码、热更新、全自动 ORM 库，后端接口和文档零代码，前端(客户端) 定制返回 JSON 的数据和结构。 🚀 A JSON Transmission Protocol and an ORM Library for automatically providing APIs and Docs.|12.5k|Java|2021/12/01|
|33|[JessYanCoding/AndroidAutoSize](https://github.com/JessYanCoding/AndroidAutoSize)|🔥 A low-cost Android screen adaptation solution (今日头条屏幕适配方案终极版，一个极低成本的 Android 屏幕适配方案).|12.0k|Java|2021/06/24|
|34|[youth5201314/banner](https://github.com/youth5201314/banner)|🔥🔥🔥Banner 2.0 来了！Android广告图片轮播控件，内部基于ViewPager2实现，Indicator和UI都可以自定义。|11.6k|Java|2021/07/11|
|35|[zhisheng17/flink-learning](https://github.com/zhisheng17/flink-learning)|flink learning blog. http://www.54tianzhisheng.cn/  含 Flink 入门、概念、原理、实战、性能调优、源码解析等内容。涉及 Flink Connector、Metrics、Library、DataStream API、Table API & SQL 等内容的学习案例，还有 Flink 落地应用的大型项目案例（PVUV、日志存储、百亿数据实时去重、监控告警）分享。欢迎大家支持我的专栏《大数据实时计算引擎 Flink 实战与性能优化》|11.3k|Java|2021/10/08|
|36|[seaswalker/spring-analysis](https://github.com/seaswalker/spring-analysis)|Spring源码阅读|11.2k|Java|2021/06/05|
|37|[LuckSiege/PictureSelector](https://github.com/LuckSiege/PictureSelector)|Picture Selector Library for Android  or 图片选择器|11.0k|Java|2021/11/29|
|38|[heibaiying/BigData-Notes](https://github.com/heibaiying/BigData-Notes)|大数据入门指南  :star:|10.9k|Java|2021/05/12|
|39|[hyb1996/Auto.js](https://github.com/hyb1996/Auto.js)|A UiAutomator on android, does not need root access(安卓平台上的JavaScript自动化工具)|10.8k|Java|2021/11/16|
|40|[pagehelper/Mybatis-PageHelper](https://github.com/pagehelper/Mybatis-PageHelper)|Mybatis通用分页插件|10.8k|Java|2021/11/03|
|41|[frank-lam/fullstack-tutorial](https://github.com/frank-lam/fullstack-tutorial)|🚀 fullstack tutorial 2021，后台技术栈/架构师之路/全栈开发社区，春招/秋招/校招/面试|10.2k|Java|2021/09/30|
|42|[JessYanCoding/MVPArms](https://github.com/JessYanCoding/MVPArms)|⚔️ A common architecture for Android applications developing based on MVP, integrates many open source projects, to make your developing quicker and easier (一个整合了大量主流开源项目高度可配置化的 Android MVP 快速集成框架). |10.1k|Java|2021/11/19|
|43|[macrozheng/mall-learning](https://github.com/macrozheng/mall-learning)|mall学习教程，架构、业务、技术要点全方位解析。mall项目（40k+star）是一套电商系统，使用现阶段主流技术实现。涵盖了SpringBoot 2.3.0、MyBatis 3.4.6、Elasticsearch 7.6.2、RabbitMQ 3.7.15、Redis 5.0、MongoDB 4.2.5、Mysql5.7等技术，采用Docker容器化部署。|10.1k|Java|2021/11/09|
|44|[alibaba/DataX](https://github.com/alibaba/DataX)|DataX是阿里云DataWorks数据集成的开源版本。|10.0k|Java|2021/11/30|
|45|[gyf-dev/ImmersionBar](https://github.com/gyf-dev/ImmersionBar)|android 4.4以上沉浸式状态栏和沉浸式导航栏管理，适配横竖屏切换、刘海屏、软键盘弹出等问题，可以修改状态栏字体颜色和导航栏图标颜色，以及不可修改字体颜色手机的适配，适用于Activity、Fragment、DialogFragment、Dialog，PopupWindow，一句代码轻松实现，以及对bar的其他设置，详见README。简书请参考：http://www.jianshu.com/p/2a884e211a62|10.0k|Java|2021/11/26|
|46|[daniulive/SmarterStreaming](https://github.com/daniulive/SmarterStreaming)|业内为数不多致力于极致体验的超强全自研跨平台(windows/android/iOS)流媒体内核，通过模块化自由组合，支持实时RTMP推流、RTSP推流、RTMP播放器、RTSP播放器、录像、多路流媒体转发、音视频导播、动态视频合成、音频混音、直播互动、内置轻量级RTSP服务等，比快更快，业界真正靠谱的超低延迟直播SDK(1秒内，低延迟模式下200~400ms)。|9.8k|Java|2021/11/11|
|47|[chefyuan/algorithm-base](https://github.com/chefyuan/algorithm-base)|专门为刚开始刷题的同学准备的算法基地，没有最细只有更细，立志用动画将晦涩难懂的算法说的通俗易懂！|9.4k|Java|2021/11/07|
|48|[DuGuQiuBai/Java](https://github.com/DuGuQiuBai/Java)|27天成为Java大神|9.0k|Java|2021/05/20|
|49|[lihengming/spring-boot-api-project-seed](https://github.com/lihengming/spring-boot-api-project-seed)|:seedling::rocket:一个基于Spring Boot & MyBatis的种子项目，用于快速构建中小型API、RESTful API项目~|9.0k|Java|2021/07/21|
|50|[hackware1993/MagicIndicator](https://github.com/hackware1993/MagicIndicator)|A powerful, customizable and extensible ViewPager indicator framework. As the best alternative of ViewPagerIndicator, TabLayout and PagerSlidingTabStrip   ——   强大、可定制、易扩展的 ViewPager 指示器框架。是ViewPagerIndicator、TabLayout、PagerSlidingTabStrip的最佳替代品。支持角标，更支持在非ViewPager场景下使用（使用hide()、show()切换Fragment或使用se ...|8.9k|Java|2021/06/20|
|51|[GcsSloop/AndroidNote](https://github.com/GcsSloop/AndroidNote)|安卓学习笔记|8.5k|Java|2021/05/25|
|52|[crossoverJie/cim](https://github.com/crossoverJie/cim)|📲cim(cross IM) 适用于开发者的分布式即时通讯系统|8.1k|Java|2021/10/12|
|53|[huanghaibin-dev/CalendarView](https://github.com/huanghaibin-dev/CalendarView)|Android上一个优雅、万能自定义UI、仿iOS、支持垂直、水平方向切换、支持周视图、自定义周起始、性能高效的日历控件，支持热插拔实现的UI定制！支持标记、自定义颜色、农历、自定义月视图各种显示模式等。Canvas绘制，速度快、占用内存低，你真的想不到日历居然还可以如此优雅！An elegant, highly customized and high-performance Calendar Widget on Android.|8.0k|Java|2021/10/11|
|54|[doocs/source-code-hunter](https://github.com/doocs/source-code-hunter)|😱 从源码层面，剖析挖掘互联网行业主流技术的底层实现原理，为广大开发者 “提升技术深度” 提供便利。目前开放 Spring 全家桶，Mybatis、Netty、Dubbo 框架，及 Redis、Tomcat 中间件等|7.3k|Java|2021/09/13|
|55|[dromara/Sa-Token](https://github.com/dromara/Sa-Token)|这可能是史上功能最全的Java权限认证框架！目前已集成——登录认证、权限认证、分布式Session会话、微服务网关鉴权、单点登录、OAuth2.0、踢人下线、Redis集成、前后台分离、记住我模式、模拟他人账号、临时身份切换、账号封禁、多账号认证体系、注解式鉴权、路由拦截式鉴权、花式token生成、自动续签、同端互斥登录、会话治理、密码加密、jwt集成、Spring集成、WebFlux集成...|7.3k|Java|2021/11/30|
|56|[gedoor/MyBookshelf](https://github.com/gedoor/MyBookshelf)|阅读是一款可以自定义来源阅读网络内容的工具，为广大网络文学爱好者提供一种方便、快捷舒适的试读体验。|7.1k|Java|2021/10/26|
|57|[zhoutaoo/SpringCloud](https://github.com/zhoutaoo/SpringCloud)|基于SpringCloud2.1的微服务开发脚手架，整合了spring-security-oauth2、nacos、feign、sentinel、springcloud-gateway等。服务治理方面引入elasticsearch、skywalking、springboot-admin、zipkin等，让项目开发快速进入业务开发，而不需过多时间花费在架构搭建上。持续更新中|7.0k|Java|2021/09/04|
|58|[hongyangAndroid/okhttputils](https://github.com/hongyangAndroid/okhttputils)|[停止维护]okhttp的辅助类|6.9k|Java|2021/11/17|
|59|[KunMinX/Jetpack-MVVM-Best-Practice](https://github.com/KunMinX/Jetpack-MVVM-Best-Practice)|是 难得一见 的 Jetpack MVVM 最佳实践！在 以简驭繁 的代码中，对 视图控制器 乃至 标准化开发模式 形成正确、深入的理解！|6.9k|Java|2021/11/27|
|60|[goldze/MVVMHabit](https://github.com/goldze/MVVMHabit)|👕基于谷歌最新AAC架构，MVVM设计模式的一套快速开发库，整合Okhttp+RxJava+Retrofit+Glide等主流模块，满足日常开发需求。使用该框架可以快速开发一个高质量、易维护的Android应用。|6.8k|Java|2021/11/27|
|61|[Exrick/xmall](https://github.com/Exrick/xmall)|基于SOA架构的分布式电商购物商城 前后端分离 前台商城:Vue全家桶 后台管理系统:Dubbo/SSM/Elasticsearch/Redis/MySQL/ActiveMQ/Shiro/Zookeeper等|6.5k|Java|2021/07/28|
|62|[wildfirechat/server](https://github.com/wildfirechat/server)|即时通讯(IM)系统|6.3k|Java|2021/12/01|
|63|[kekingcn/kkFileView](https://github.com/kekingcn/kkFileView)|spring-boot打造文件文档在线预览项目|6.3k|Java|2021/11/30|
|64|[lenve/VBlog](https://github.com/lenve/VBlog)|V部落，Vue+SpringBoot实现的多用户博客管理平台!|6.3k|Java|2021/11/30|
|65|[Meituan-Dianping/walle](https://github.com/Meituan-Dianping/walle)|Android Signature V2 Scheme签名下的新一代渠道包打包神器|6.3k|Java|2021/09/07|
|66|[li-xiaojun/XPopup](https://github.com/li-xiaojun/XPopup)|🔥XPopup2.0版本重磅来袭，2倍以上性能提升，带来可观的动画性能优化和交互细节的提升！！！功能强大，交互优雅，动画丝滑的通用弹窗！可以替代Dialog，PopupWindow，PopupMenu，BottomSheet，DrawerLayout，Spinner等组件，自带十几种效果良好的动画， 支持完全的UI和动画自定义！(Powerful and Beautiful Popup for Android，can absolutely replace Dialog，PopupWindow，PopupMenu，BottomSheet，DrawerLayout，Spinner. With bu ...|6.1k|Java|2021/12/01|
|67|[NLPchina/ansj_seg](https://github.com/NLPchina/ansj_seg)|ansj分词.ict的真正java实现.分词效果速度都超过开源版的ict. 中文分词,人名识别,词性标注,用户自定义词典|6.0k|Java|2021/08/15|
|68|[zouzg/mybatis-generator-gui](https://github.com/zouzg/mybatis-generator-gui)|mybatis-generator界面工具，让你生成代码更简单更快捷|6.0k|Java|2021/07/05|
|69|[metersphere/metersphere](https://github.com/metersphere/metersphere)|MeterSphere 是一站式开源持续测试平台，覆盖测试管理、接口测试、性能测试等。搞测试，就选 MeterSphere！|5.9k|Java|2021/12/01|
|70|[ityouknow/spring-cloud-examples](https://github.com/ityouknow/spring-cloud-examples)|Spring Cloud 学习案例，服务发现、服务治理、链路追踪、服务监控等|5.8k|Java|2021/08/21|
|71|[sohutv/cachecloud](https://github.com/sohutv/cachecloud)|搜狐视频(sohu tv)Redis私有云平台|5.7k|Java|2021/06/08|
|72|[febsteam/FEBS-Shiro](https://github.com/febsteam/FEBS-Shiro)|Spring Boot 2.4.2，Shiro1.6.0 & Layui 2.5.6 权限管理系统。预览地址：http://47.104.70.138:8080/login|5.6k|Java|2021/08/24|
|73|[liyifeng1994/ssm](https://github.com/liyifeng1994/ssm)|手把手教你整合最优雅SSM框架：SpringMVC + Spring + MyBatis|5.5k|Java|2021/07/15|
|74|[jpush/aurora-imui](https://github.com/jpush/aurora-imui)|General IM UI components. Android/iOS/RectNative ready.  通用 IM 聊天 UI 组件，已经同时支持 Android/iOS/RN。|5.5k|Java|2021/09/21|
|75|[h2pl/Java-Tutorial](https://github.com/h2pl/Java-Tutorial)|【Java工程师面试复习指南】本仓库涵盖大部分Java程序员所需要掌握的核心知识，整合了互联网上的很多优质Java技术文章，力求打造为最完整最实用的Java开发者学习指南，如果对你有帮助，给个star告诉我吧，谢谢！|5.3k|Java|2021/05/14|
|76|[gzu-liyujiang/AndroidPicker](https://github.com/gzu-liyujiang/AndroidPicker)|安卓选择器类库，包括日期及时间选择器（可用于出生日期、营业时间等）、单项选择器（可用于性别、民族、职业、学历、星座等）、二三级联动选择器（可用于车牌号、基金定投日期等）、城市地址选择器（分省级、地市级及区县级）、数字选择器（可用于年龄、身高、体重、温度等）、日历选日期择器（可用于酒店及机票预定日期）、颜色选择器、文件及目录选择器、图片选择器等……WheelPicker/DatePicker/TimePicker/OptionPicker/NumberPicker/LinkagePicker/AddressPicker/CarPlatePicker/CalendarPicker/ColorPic ...|5.3k|Java|2021/11/16|
|77|[changmingxie/tcc-transaction](https://github.com/changmingxie/tcc-transaction)|tcc-transaction是TCC型事务java实现|5.3k|Java|2021/11/04|
|78|[527515025/springBoot](https://github.com/527515025/springBoot)|springboot 框架与其它组件结合如 jpa、mybatis、websocket、security、shiro、cache等|5.3k|Java|2021/06/05|
|79|[YunaiV/onemall](https://github.com/YunaiV/onemall)|芋道 mall 商城，基于微服务的思想，构建在 B2C 电商场景下的项目实战。核心技术栈，是 Spring Boot + Dubbo 。未来，会重构成 Spring Cloud Alibaba 。|5.3k|Java|2021/10/13|
|80|[yipianfengye/android-zxingLibrary](https://github.com/yipianfengye/android-zxingLibrary)|几行代码快速集成二维码扫描功能|4.9k|Java|2021/08/21|
|81|[ffay/lanproxy](https://github.com/ffay/lanproxy)|lanproxy是一个将局域网个人电脑、服务器代理到公网的内网穿透工具，支持tcp流量转发，可支持任何tcp上层协议（访问内网网站、本地支付接口调试、ssh访问、远程桌面、http代理、https代理、socks5代理...）。技术交流QQ群 678776401|4.8k|Java|2021/09/05|
|82|[AriaLyy/Aria](https://github.com/AriaLyy/Aria)|下载可以很简单|4.8k|Java|2021/09/05|
|83|[hongyangAndroid/baseAdapter](https://github.com/hongyangAndroid/baseAdapter)|Android 万能的Adapter for ListView,RecyclerView,GridView等，支持多种Item类型的情况。|4.7k|Java|2021/10/13|
|84|[Nepxion/Discovery](https://github.com/Nepxion/Discovery)|☀️ Nepxion Discovery is a solution for Spring Cloud with blue green, gray, route, limitation, circuit breaker, degrade, isolation, tracing, dye, failover 蓝绿、灰度、路由、限流、熔断、降级、隔离、追踪、流量染色、故障转移|4.6k|Java|2021/10/06|
|85|[youlookwhat/CloudReader](https://github.com/youlookwhat/CloudReader)|🗡️  云阅：一款基于网易云音乐UI，使用玩Android Api，Retrofit2 + RxJava2 + Room + MVVM-databinding架构开发的Android客户端|4.6k|Java|2021/11/09|
|86|[razerdp/BasePopup](https://github.com/razerdp/BasePopup)| Android下打造通用便捷的PopupWindow弹窗库|4.6k|Java|2021/11/01|
|87|[chentao0707/SimplifyReader](https://github.com/chentao0707/SimplifyReader)|一款基于Google Material Design设计开发的Android客户端，包括新闻简读，图片浏览，视频爽看 ，音乐轻听以及二维码扫描五个子模块。项目采取的是MVP架构开发，由于还是摸索阶段，可能不是很规范。但基本上应该是这么个套路，至少我个人认为是这样的~恩，就是这样的！|4.6k|Java|2021/09/17|
|88|[SplashCodes/JAViewer](https://github.com/SplashCodes/JAViewer)|更优雅的驾车体验|4.6k|Java|2021/08/22|
|89|[981011512/--](https://github.com/981011512/--)|停车场系统源码，停车场小程序，智能停车，Parking system，【功能介绍】：①兼容市面上主流的多家相机，理论上兼容所有硬件，可灵活扩展，②相机识别后数据自动上传到云端并记录，校验相机唯一id和硬件序列号，防止非法数据录入，③用户手机查询停车记录详情可自主缴费(支持微信，支付宝，银行接口支付，支持每个停车场指定不同的商户进行收款)，支付后出场在免费时间内会自动抬杆。④支持app上查询附近停车场(导航，可用车位数，停车场费用，优惠券，评分，评论等)，可预约车位。⑤断电断网支持岗亭人员使用app可接管硬件进行停车记录的录入。 【技术架构】：后端开发语言java，框架oauth2+spring ...|4.5k|Java|2021/11/09|
|90|[zhanghai/Douya](https://github.com/zhanghai/Douya)|开源的 Material Design 豆瓣客户端（A Material Design app for douban.com）|4.5k|Java|2021/06/15|
|91|[pili-engineering/PLDroidPlayer](https://github.com/pili-engineering/PLDroidPlayer)|PLDroidPlayer 是七牛推出的一款免费的适用于 Android 平台的播放器 SDK，采用全自研的跨平台播放内核，拥有丰富的功能和优异的性能，可高度定制化和二次开发。|4.5k|Java|2021/10/29|
|92|[getActivity/AndroidProject](https://github.com/getActivity/AndroidProject)|Android 技术中台，但愿人长久，搬砖不再有|4.3k|Java|2021/10/11|
|93|[macrozheng/springcloud-learning](https://github.com/macrozheng/springcloud-learning)|一套涵盖大部分核心组件使用的Spring Cloud教程，包括Spring Cloud Alibaba及分布式事务Seata，基于Spring Cloud Greenwich及SpringBoot 2.1.7。22篇文章，篇篇精华，32个Demo，涵盖大部分应用场景。|4.3k|Java|2021/08/23|
|94|[zhaojun1998/zfile](https://github.com/zhaojun1998/zfile)|在线云盘、网盘、OneDrive、云存储、私有云、对象存储、h5ai|4.2k|Java|2021/10/30|
|95|[2227324689/gpmall](https://github.com/2227324689/gpmall)|【咕泡学院实战项目】-基于SpringBoot+Dubbo构建的电商平台-微服务架构、商城、电商、微服务、高并发、kafka、Elasticsearch|4.2k|Java|2021/10/24|
|96|[hansonwang99/Spring-Boot-In-Action](https://github.com/hansonwang99/Spring-Boot-In-Action)|Spring Boot 系列实战合集|4.1k|Java|2021/06/08|
|97|[fuzhengwei/itstack-demo-design](https://github.com/fuzhengwei/itstack-demo-design)|:art: 《重学Java设计模式》是一本互联网真实案例实践书籍。以落地解决方案为核心，从实际业务中抽离出，交易、营销、秒杀、中间件、源码等22个真实场景，来学习设计模式的运用。欢迎关注小傅哥，微信(fustack)，公众号：bugstack虫洞栈，博客：https://bugstack.cn|4.1k|Java|2021/11/10|
|98|[zhanglei-workspace/shopping-management-system](https://github.com/zhanglei-workspace/shopping-management-system)|该项目为多个小项目的集合（持续更新中...）。内容类似淘宝、京东等网购管理系统以及图书管理、超市管理等系统。目的在于便于Java初级爱好者在学习完某一部分Java知识后有一个合适的项目锻炼、运用所学知识，完善知识体系。适用人群：Java基础到入门的爱好者。|4.1k|Java|2021/08/07|
|99|[CodingDocs/springboot-guide](https://github.com/CodingDocs/springboot-guide)|SpringBoot2.0+从入门到实战！|4.1k|Java|2021/09/09|
|100|[zuihou/lamp-cloud](https://github.com/zuihou/lamp-cloud)|lamp-cloud 基于Jdk11 + SpringCloud + SpringBoot的微服务快速开发平台，其中的可配置的SaaS功能尤其闪耀， 具备RBAC功能、网关统一鉴权、Xss防跨站攻击、自动代码生成、多种存储系统、分布式事务、分布式定时任务等多个模块，支持多业务系统并行开发， 支持多服务并行开发，可以作为后端服务的开发脚手架。代码简洁，注释齐全，架构清晰，非常适合学习和企业作为基础框架使用。|4.1k|Java|2021/11/29|
|101|[ZHENFENG13/spring-boot-projects](https://github.com/ZHENFENG13/spring-boot-projects)|该仓库中主要是 Spring Boot 的入门学习教程以及一些常用的 Spring Boot 实战项目教程，包括 Spring Boot 使用的各种示例代码，同时也包括一些实战项目的项目源码和效果展示，实战项目包括基本的 web 开发以及目前大家普遍使用的线上博客项目/企业大型商城系统/前后端分离实践项目等，摆脱各种 hello world 入门案例的束缚，真正的掌握 Spring Boot 开发。|4.0k|Java|2021/10/21|
|102|[yhaolpz/FloatWindow](https://github.com/yhaolpz/FloatWindow)|Andorid 任意界面悬浮窗，实现悬浮窗如此简单|3.9k|Java|2021/10/13|
|103|[dataease/dataease](https://github.com/dataease/dataease)|人人可用的开源数据可视化分析工具。|3.9k|Java|2021/12/01|
|104|[Heeexy/SpringBoot-Shiro-Vue](https://github.com/Heeexy/SpringBoot-Shiro-Vue)|提供一套基于Spring Boot-Shiro-Vue的权限管理思路.前后端都加以控制,做到按钮/接口级别的权限。（当前新版本已移除shiro依赖，简化了配置）|3.9k|Java|2021/05/27|
|105|[luckybilly/CC](https://github.com/luckybilly/CC)|业界首个支持渐进式组件化改造的Android组件化开源框架，支持跨进程调用。Componentize your android project gradually.|3.9k|Java|2021/05/14|
|106|[bjmashibing/InternetArchitect](https://github.com/bjmashibing/InternetArchitect)|年薪百万互联网架构师课程文档及源码(公开部分)|3.9k|Java|2021/09/09|
|107|[oldmanpushcart/greys-anatomy](https://github.com/oldmanpushcart/greys-anatomy)|Java诊断工具|3.8k|Java|2021/06/08|
|108|[Doikki/DKVideoPlayer](https://github.com/Doikki/DKVideoPlayer)|Android Video Player. 安卓视频播放器，封装MediaPlayer、ExoPlayer、IjkPlayer。模仿抖音并实现预加载，列表播放，悬浮播放，广告播放，弹幕|3.8k|Java|2021/08/07|
|109|[WeiYe-Jing/datax-web](https://github.com/WeiYe-Jing/datax-web)|DataX集成可视化页面，选择数据源即可一键生成数据同步任务，支持RDBMS、Hive、HBase、ClickHouse、MongoDB等数据源，批量创建RDBMS数据同步任务，集成开源调度系统，支持分布式、增量同步数据、实时查看运行日志、监控执行器资源、KILL运行进程、数据源信息加密等。|3.3k|Java|2021/11/24|
|110|[java-aodeng/hope-boot](https://github.com/java-aodeng/hope-boot)|🌱 Hope-Boot 一款现代化的脚手架项目|3.2k|Java|2021/08/25|
|111|[didi/LogiKM](https://github.com/didi/LogiKM)|一站式Apache Kafka集群指标监控与运维管控平台|3.2k|Java|2021/12/01|
|112|[JeremyLiao/LiveEventBus](https://github.com/JeremyLiao/LiveEventBus)|:mailbox_with_mail:EventBus for Android，消息总线，基于LiveData，具有生命周期感知能力，支持Sticky，支持AndroidX，支持跨进程，支持跨APP|3.2k|Java|2021/09/03|
|113|[qunarcorp/bistoury](https://github.com/qunarcorp/bistoury)|Bistoury是去哪儿网的java应用生产问题诊断工具，提供了一站式的问题诊断方案|3.2k|Java|2021/06/17|
|114|[Skykai521/StickerCamera](https://github.com/Skykai521/StickerCamera)|This is an Android application with camera,picture cropping,collage sticking and tagging.贴纸标签相机,功能:拍照,相片裁剪,给图片贴贴纸,打标签。|3.1k|Java|2021/11/06|
|115|[CheckChe0803/flink-recommandSystem-demo](https://github.com/CheckChe0803/flink-recommandSystem-demo)|:helicopter::rocket:基于Flink实现的商品实时推荐系统。flink统计商品热度，放入redis缓存，分析日志信息，将画像标签和实时记录放入Hbase。在用户发起推荐请求后，根据用户画像重排序热度榜，并结合协同过滤和标签两个推荐模块为新生成的榜单的每一个产品添加关联产品，最后返回新的用户列表。|3.1k|Java|2021/07/02|
|116|[NotFound9/interviewGuide](https://github.com/NotFound9/interviewGuide)|《大厂面试指北》——包括Java基础、JVM、数据库、mysql、redis、计算机网络、算法、数据结构、操作系统、设计模式、系统设计、框架原理。最佳阅读地址：http://notfound9.github.io/interviewGuide/|3.1k|Java|2021/10/30|
|117|[baomidou/dynamic-datasource-spring-boot-starter](https://github.com/baomidou/dynamic-datasource-spring-boot-starter)|dynamic datasource for springboot 多数据源 动态数据源 主从分离 读写分离 分布式事务 |3.1k|Java|2021/11/29|
|118|[crazyandcoder/citypicker](https://github.com/crazyandcoder/citypicker)|citypicker城市选择器，详细的省市区地址信息，支持仿iOS滚轮实现，仿京东样式，一级或者三级列表展示方式。|3.0k|Java|2021/07/12|
|119|[woxingxiao/BubbleSeekBar](https://github.com/woxingxiao/BubbleSeekBar)|A beautiful Android custom seekbar, which has a bubble view with progress appearing upon when seeking. 自定义SeekBar，进度变化更以可视化气泡样式呈现|3.0k|Java|2021/05/28|
|120|[lenve/JavaEETest](https://github.com/lenve/JavaEETest)|Spring、SpringMVC、MyBatis、Spring Boot案例|3.0k|Java|2021/06/08|
|121|[zaaach/CityPicker](https://github.com/zaaach/CityPicker)|:fire::fire::fire:城市选择、定位、搜索及右侧字母导航，类似美团 百度糯米 饿了么等APP选择城市功能|3.0k|Java|2021/07/07|
|122|[KunMinX/Linkage-RecyclerView](https://github.com/KunMinX/Linkage-RecyclerView)|即使不用饿了么订餐，也请务必收藏好该库！🔥  一行代码即可接入，二级联动订餐列表。|3.0k|Java|2021/10/13|
|123|[mxdldev/android-mvp-mvvm-flytour](https://github.com/mxdldev/android-mvp-mvvm-flytour)|🔥🔥🔥 FlyTour是Android MVVM+MVP+Dagger2+Retrofit+RxJava+组件化+插件组成的双编码架构+双工程架构+双语言Android应用开发框架，通过不断的升级迭代该框架已经有了十个不同的版本，5.0之前工程架构采用gradle配置实现组件化，5.0之后的工程架构采用VirtualAPK实现了插件化，5.0之前采用Java编码实现，5.0之后采用Kotlin编码实现，编码架构由MVVM和MVP组成，工程架构和编码架构及编码语言开发者可根据自己具体的项目实际需求去决定选择使用，该框架是Android组件化、Android插件化、Android MVP架构、An ...|2.9k|Java|2021/06/03|
|124|[getActivity/XXPermissions](https://github.com/getActivity/XXPermissions)|Android 权限请求框架，已适配 Android 12|2.9k|Java|2021/11/15|
|125|[FinalTeam/RxGalleryFinal](https://github.com/FinalTeam/RxGalleryFinal)|图片选择库，单选/多选、拍照、裁剪、压缩，自定义。包括视频选择和录制。|2.8k|Java|2021/06/22|
|126|[JessYanCoding/ProgressManager](https://github.com/JessYanCoding/ProgressManager)|⏳ Listen the progress of  downloading and uploading in Okhttp, compatible Retrofit and Glide (一行代码即可监听 App 中所有网络链接的上传以及下载进度, 包括 Glide 的图片加载进度).|2.5k|Java|2021/10/31|
|127|[xiaojinzi123/Component](https://github.com/xiaojinzi123/Component)|🔥🔥🔥A powerful componentized framework.一个强大、100% 兼容、支持 AndroidX、支持 Kotlin并且灵活的组件化框架|2.4k|Java|2021/10/13|
|128|[qunarcorp/qmq](https://github.com/qunarcorp/qmq)| QMQ是去哪儿网内部广泛使用的消息中间件，自2012年诞生以来在去哪儿网所有业务场景中广泛的应用，包括跟交易息息相关的订单场景； 也包括报价搜索等高吞吐量场景。|2.4k|Java|2021/09/26|
|129|[liyiorg/weixin-popular](https://github.com/liyiorg/weixin-popular)|微信SDK  JAVA  (公众平台、开放平台、 商户平台、 服务商平台)|2.4k|Java|2021/11/25|
|130|[ZHENFENG13/My-Blog](https://github.com/ZHENFENG13/My-Blog)|:palm_tree::octocat:A simple & beautiful blogging system implemented with spring-boot & thymeleaf & mybatis My Blog 是由 SpringBoot + Mybatis + Thymeleaf 等技术实现的 Java 博客系统，页面美观、功能齐全、部署简单及完善的代码，一定会给使用者无与伦比的体验|2.4k|Java|2021/08/11|
|131|[jetlinks/jetlinks-community](https://github.com/jetlinks/jetlinks-community)|JetLinks  基于Java8,Spring Boot 2.x ,WebFlux,Netty,Vert.x,Reactor等开发, 是一个全响应式的企业级物联网平台。支持统一物模型管理,多种设备,多种厂家,统一管理。统一设备连接管理,多协议适配(TCP,MQTT,UDP,CoAP,HTTP等),屏蔽网络编程复杂性,灵活接入不同厂家不同协议等设备。实时数据处理,设备告警,消息通知,数据转发。地理位置,数据可视化等。能帮助你快速建立物联网相关业务系统。|2.4k|Java|2021/11/30|
|132|[mqzhangw/JIMU](https://github.com/mqzhangw/JIMU)|一种简单有效的android组件化方案，支持组件的代码资源隔离、单独调试、集成调试、组件交互、UI跳转、生命周期等完整功能。|2.4k|Java|2021/06/13|
|133|[HuanTanSheng/EasyPhotos](https://github.com/HuanTanSheng/EasyPhotos)|兼容android11、android 10，相机拍照，相册选择(单选/多选)，文件夹图片选择(单选/多选)，视频选择，视频图片多类型复杂选择，各界面根据状态栏颜色智能适配状态栏字体颜色变色为深色或浅色，根据使用场景智能适配沉浸式状态栏，内部处理运行时权限，支持Glide/Picasso/Imageloader等所有图片加载框架库的带默认勾选选中图片的能填充自定义广告的自定义Ui相机相册图片浏览选择器；更有拼图/文字贴纸/贴图/图片缩放/Bitmap图片添加水印/媒体文件更新到媒体库等众多Bitmap图片编辑功能的android Bitmap图片处理工具框架库。|2.3k|Java|2021/11/02|
|134|[JPressProjects/jpress](https://github.com/JPressProjects/jpress)|JPress，一个使用 Java 开发的建站神器，目前已经有 10w+ 网站使用 JPress 进行驱动，其中包括多个政府机构，200+上市公司，中科院、红+字会等。|2.3k|Java|2021/11/29|
|135|[lihangleo2/ShadowLayout](https://github.com/lihangleo2/ShadowLayout)|可定制化阴影的万能阴影布局ShadowLayout 3.0 震撼上线。效果赶超CardView。阴影支持x,y轴偏移，支持阴影扩散程度，支持阴影圆角，支持单边或多边不显示阴影；控件支持动态设置shape和selector（项目里再也不用画shape了）；支持随意更改颜色值，支持随意更改颜色值，支持随意更改颜色值。重要的事情说三遍|2.3k|Java|2021/11/18|
|136|[fuzhengwei/small-spring](https://github.com/fuzhengwei/small-spring)|🌱《 Spring 手撸专栏》，本专栏以 Spring 源码学习为目的，通过手写简化版 Spring 框架，了解 Spring 核心原理。在手写的过程中会简化 Spring 源码，摘取整体框架中的核心逻辑，简化代码实现过程，保留核心功能，例如：IOC、AOP、Bean生命周期、上下文、作用域、资源处理等内容实现。|2.3k|Java|2021/11/17|
|137|[xtuhcy/gecco](https://github.com/xtuhcy/gecco)|Easy to use lightweight web crawler（易用的轻量化网络爬虫）|2.3k|Java|2021/08/24|
|138|[IoT-Technology/IoT-Technical-Guide](https://github.com/IoT-Technology/IoT-Technical-Guide)|:honeybee: IoT Technical Guide --- 从零搭建高性能物联网平台及物联网解决方案和Thingsboard源码分析 :sparkles: :sparkles: :sparkles: (IoT Platform, SaaS, MQTT, CoAP, HTTP, Modbus, OPC, WebSocket, 物模型，Protobuf, PostgreSQL, MongoDB, Spring Security, OAuth2, RuleEngine, Kafka, Docker)|2.3k|Java|2021/11/02|
|139|[bingoogolapple/BGASwipeBackLayout-Android](https://github.com/bingoogolapple/BGASwipeBackLayout-Android)|Android Activity 滑动返回。支持微信滑动返回样式、横屏滑动返回、全屏滑动返回|2.2k|Java|2021/07/18|
|140|[201206030/novel-plus](https://github.com/201206030/novel-plus)|小说精品屋-plus是一个多端（PC、WAP）阅读、功能完善的原创文学CMS系统，由前台门户系统、作家后台管理系统、平台后台管理系统、爬虫管理系统等多个子系统构成，支持多模版、会员充值、订阅模式、新闻发布和实时统计报表等功能，新书自动入库，老书自动更新。|2.2k|Java|2021/11/11|
|141|[alibaba/yugong](https://github.com/alibaba/yugong)|阿里巴巴去Oracle数据迁移同步工具(全量+增量,目标支持MySQL/DRDS)|2.2k|Java|2021/06/08|
|142|[Pay-Group/best-pay-sdk](https://github.com/Pay-Group/best-pay-sdk)|可能是最好的支付SDK|2.6k|Java|2021/09/03|
|143|[tianshiyeben/wgcloud](https://github.com/tianshiyeben/wgcloud)|linux运维监控工具，支持系统信息，内存，cpu，温度，磁盘空间及IO，硬盘smart，系统负载，网络流量等监控，API接口，大屏展示，拓扑图，进程监控，端口监控，docker监控，文件防篡改，日志监控，数据可视化，web ssh，堡垒机，指令下发批量执行，linux面板，探针，故障告警|2.6k|Java|2021/11/29|
|144|[ngbdf/redis-manager](https://github.com/ngbdf/redis-manager)|Redis 一站式管理平台，支持集群的监控、安装、管理、告警以及基本的数据操作|2.6k|Java|2021/08/17|
|145|[zxbu/webdav-aliyundriver](https://github.com/zxbu/webdav-aliyundriver)|阿里云盘(https://www.aliyundrive.com/) 的webdav协议开源实现|2.6k|Java|2021/11/10|
|146|[yangchong211/LifeHelper](https://github.com/yangchong211/LifeHelper)|【停止维护】组件化综合案例，包含微信新闻，头条视频，美女图片，百度音乐，干活集中营，玩Android，豆瓣读书电影，知乎日报等等模块。架构模式：组件化+MVP+Rx+Retrofit+Desgin+Dagger2+阿里VLayout+腾讯X5+腾讯bugly。融合开发中需要的各种小案例！|2.6k|Java|2021/11/19|
|147|[rememberber/WePush](https://github.com/rememberber/WePush)|专注批量推送的小而美的工具，目前支持：模板消息-公众号、模板消息-小程序、微信客服消息、微信企业号/企业微信消息、阿里云短信、阿里大于模板短信 、腾讯云短信、云片网短信、E-Mail、HTTP请求、钉钉、华为云短信、百度云短信、又拍云短信、七牛云短信|2.6k|Java|2021/07/17|
|148|[zhpanvip/BannerViewPager](https://github.com/zhpanvip/BannerViewPager)|🚀  An awesome banner view  for Android，Based on ViewPager2. 这可能是全网最好用的ViewPager轮播图。简单、高效，一行代码实现循环轮播，一屏三页任意变，指示器样式任你挑。|2.6k|Java|2021/10/26|
|149|[bm-x/PhotoView](https://github.com/bm-x/PhotoView)|图片浏览缩放控件|2.5k|Java|2021/10/15|
|150|[JsonChao/Awesome-WanAndroid](https://github.com/JsonChao/Awesome-WanAndroid)|:zap:致力于打造一款极致体验的 http://www.wanandroid.com/ 客户端，知识和美是可以并存的哦QAQn(*≧▽≦*)n|2.5k|Java|2021/08/17|
|151|[ainilili/ratel](https://github.com/ainilili/ratel)|命令行斗地主!|1.7k|Java|2021/11/08|
|152|[decaywood/XueQiuSuperSpider](https://github.com/decaywood/XueQiuSuperSpider)|雪球股票信息超级爬虫|1.7k|Java|2021/08/24|
|153|[xuexiangjys/XUpdate](https://github.com/xuexiangjys/XUpdate)|🚀A lightweight, high availability Android version update framework.(一个轻量级、高可用性的Android版本更新框架)|1.7k|Java|2021/11/29|
|154|[DTStack/flinkStreamSQL](https://github.com/DTStack/flinkStreamSQL)|基于开源的flink，对其实时sql进行扩展；主要实现了流与维表的join，支持原生flink SQL所有的语法|1.7k|Java|2021/11/10|
|155|[doublechaintech/scm-biz-suite](https://github.com/doublechaintech/scm-biz-suite)|供应链中台系统基础版，集成零售管理, 电子商务, 供应链管理,  财务管理, 车队管理, 仓库管理, 人员管理, 产品管理, 订单管理, 会员管理, 连锁店管理, 加盟管理, 前端React/Ant Design, 后端Java Spring+自有开源框架，全面支持MySQL, PostgreSQL, 全面支持国产数据库南大通用GBase 8s,通过REST接口调用，前后端完全分离。|1.7k|Java|2021/11/21|
|156|[CarpenterLee/JavaLambdaInternals](https://github.com/CarpenterLee/JavaLambdaInternals)|深入理解Java函数式编程和Streams API|1.7k|Java|2021/10/20|
|157|[xiaojieonly/Ehviewer_CN_SXJ](https://github.com/xiaojieonly/Ehviewer_CN_SXJ)|ehviewer，用爱发电，快乐前行|1.6k|Java|2021/11/07|
|158|[zq2599/blog_demos](https://github.com/zq2599/blog_demos)|CSDN博客专家程序员欣宸的github，这里有五百多篇原创文章的详细分类和汇总，以及对应的源码，内容涉及Java、Docker、Kubernetes、DevOPS等方面|1.6k|Java|2021/12/01|
|159|[wkeyuan/DWSurvey](https://github.com/wkeyuan/DWSurvey)|Survey System. 最好用的开源问卷调查系统、表单系统。|1.6k|Java|2021/11/30|
|160|[limpoxe/Android-Plugin-Framework](https://github.com/limpoxe/Android-Plugin-Framework)|Android插件框架，免安装运行插件APK ，支持独立插件和非独立插件|1.6k|Java|2021/11/30|
|161|[reactnativecn/react-native-pushy](https://github.com/reactnativecn/react-native-pushy)|React Native 极速热更新服务|1.6k|Java|2021/11/22|
|162|[SherlockGougou/BigImageViewPager](https://github.com/SherlockGougou/BigImageViewPager)|🔥🔥🔥 BigImage ImageView ViewPager 支持超长图、超大图的图片浏览器，优化内存，支持手势放大、下拉关闭、查看原图、加载百分比、保存图片等功能。现已支持androidx。|1.6k|Java|2021/11/25|
|163|[peng-zhihui/DeepVision](https://github.com/peng-zhihui/DeepVision)|在我很多项目中用到的CV算法推理框架应用。|1.6k|Java|2021/11/09|
|164|[dairongpeng/algorithm-note](https://github.com/dairongpeng/algorithm-note)|该系列包括数组，链表，树，图，递归，DP，有序表等相关数据结构与算法的讲解及代码实现。|1.6k|Java|2021/08/09|
|165|[CeuiLiSA/Pixiv-Shaft](https://github.com/CeuiLiSA/Pixiv-Shaft)|Pixiv第三方Android客户端|1.6k|Java|2021/11/23|
|166|[spring2go/staffjoy](https://github.com/spring2go/staffjoy)|微服务(Microservices)和云原生架构教学案例项目，基于Spring Boot和Kubernetes技术栈|1.6k|Java|2021/10/06|
|167|[xuhuisheng/lemon](https://github.com/xuhuisheng/lemon)|开源OA|1.6k|Java|2021/08/19|
|168|[febsteam/FEBS-Cloud](https://github.com/febsteam/FEBS-Cloud)|基于Spring Cloud Hoxton.RELEASE、Spring Cloud OAuth2 & Spring Cloud Alibaba & Element 微服务权限系统，开箱即用。预览地址：https://cloud.mrbird.cn|1.6k|Java|2021/10/11|
|169|[threedr3am/learnjavabug](https://github.com/threedr3am/learnjavabug)|Java安全相关的漏洞和技术demo，原生Java、Fastjson、Jackson、Hessian2、XML反序列化漏洞利用和Spring、Dubbo、Shiro、CAS、Tomcat、RMI、Nexus等框架\中间件\功能的exploits以及Java Security Manager绕过、Dubbo-Hessian2安全加固等等实践代码。|1.6k|Java|2021/10/27|
|170|[jayZheng87/Thusy](https://github.com/jayZheng87/Thusy)|Java 工具类库；在全面集成 Hutool 上进行工具类二次收集的一个类库|1.6k|Java|2021/08/25|
|171|[panpf/sketch](https://github.com/panpf/sketch)|Sketch 是 Android 上一个强大且全面的图片加载器，支持 GIF，手势缩放以及分块显示超大图片。Sketch is a powerful and comprehensive image loader on Android, with support for GIF, gesture zooming, block display super large image|1.6k|Java|2021/11/16|
|172|[GoogleLLP/SuperMarket](https://github.com/GoogleLLP/SuperMarket)|设计精良的网上商城系统，包括前端、后端、数据库、负载均衡、数据库缓存、分库分表、读写分离、全文检索、消息队列等，使用SpringCloud框架，基于Java开发。该项目可部署到服务器上，不断完善中……|1.6k|Java|2021/08/10|
|173|[YummyLau/PanelSwitchHelper](https://github.com/YummyLau/PanelSwitchHelper)|:heavy_check_mark:  A framework that helps the keyboard smoothly transition to the function panel    一个帮助键盘平稳过渡到功能面板的框架，支持动画无缝衔接，支持 activity/fragment/dialog/dialogFragment/popupWindow 容器，支持IM/直播/视频播放/信息流评论等场景，支持全屏模式。|1.9k|Java|2021/11/05|
|174|[codedrinker/community](https://github.com/codedrinker/community)|开源论坛、问答系统，现有功能提问、回复、通知、最新、最热、消除零回复功能。功能持续更新中…… 技术栈 Spring、Spring Boot、MyBatis、MySQL/H2、Bootstrap|1.9k|Java|2021/10/25|
|175|[Snailclimb/guide-rpc-framework](https://github.com/Snailclimb/guide-rpc-framework)|A custom RPC framework implemented by Netty+Kyro+Zookeeper.（一款基于 Netty+Kyro+Zookeeper 实现的自定义 RPC 框架-附详细实现过程和相关教程。）|1.9k|Java|2021/08/06|
|176|[MarkerHub/vueblog](https://github.com/MarkerHub/vueblog)|一个前后端分离的简单博客案例，适合刚入门vue，学前后端分离的童鞋！|1.9k|Java|2021/10/07|
|177|[HelloWorld521/Java](https://github.com/HelloWorld521/Java)|java项目实战练习|1.9k|Java|2021/10/08|
|178|[liuyubobobo/Play-with-Data-Structures](https://github.com/liuyubobobo/Play-with-Data-Structures)|Codes of my MOOC Course <Play Data Structures in Java>. Updated contents and practices are also included. 我在慕课网上的课程《Java语言玩转数据结构》示例代码。课程的更多更新内容及辅助练习也将逐步添加进这个代码仓。|1.9k|Java|2021/06/18|
|179|[xwjie/PLMCodeTemplate](https://github.com/xwjie/PLMCodeTemplate)|给部门制定的代码框架模板|1.9k|Java|2021/08/25|
|180|[yuzhiqiang1993/zxing](https://github.com/yuzhiqiang1993/zxing)|基于zxing的扫一扫，优化了扫描二维码速度，集成最新版本的jar包（zxing-core.jar 3.3.3），集成简单，速度快，可配置颜色，还有闪光灯，解析二维码图片，生成二维码等功能|1.9k|Java|2021/08/05|
|181|[yangchong211/YCVideoPlayer](https://github.com/yangchong211/YCVideoPlayer)|基础封装视频播放器player，可以在ExoPlayer、MediaPlayer原生MediaPlayer可以自由切换内核；该播放器整体架构：播放器内核(自由切换) + 视频播放器 + 边播边缓存 + 高度定制播放器UI视图层。支持视频简单播放，列表播放，仿抖音滑动播放，自动切换播放，使用案例丰富，拓展性强。|1.9k|Java|2021/11/03|
|182|[pig-mesh/pig](https://github.com/pig-mesh/pig)|↥ ↥ ↥ 点击关注更新，基于 Spring Cloud 2020 、Spring Boot 2.5、 OAuth2 的 RBAC 权限管理系统|1.9k|Java|2021/11/18|
|183|[Skykai521/ECTranslation](https://github.com/Skykai521/ECTranslation)|Android Studio Plugin,Translate English to Chinese. Android Studio 翻译插件,可以将英文翻译为中文.|1.8k|Java|2021/11/06|
|184|[scalad/Note](https://github.com/scalad/Note)|常规Java工具，算法，加密，数据库，面试题，源代码分析，解决方案|1.8k|Java|2021/05/12|
|185|[pppscn/SmsForwarder](https://github.com/pppscn/SmsForwarder)|短信转发器——监控Android手机短信、来电、APP通知，并根据指定规则转发到其他手机：钉钉机器人、企业微信群机器人、飞书机器人、企业微信应用消息、邮箱、bark、webhook、Telegram机器人、Server酱、PushPlus、手机短信等。PS.这个APK主要是学习与自用，如有BUG请提ISSUE，同时欢迎大家提PR指正|1.8k|Java|2021/11/26|
|186|[SpringCloud/spring-cloud-code](https://github.com/SpringCloud/spring-cloud-code)|🔥《重新定义Spring Cloud实战》实体书对应源码，欢迎大家Star点赞收藏|1.8k|Java|2021/06/05|
|187|[wuyr/PathLayoutManager](https://github.com/wuyr/PathLayoutManager)|RecyclerView的LayoutManager，轻松实现各种炫酷、特殊效果，再也不怕产品经理为难！ |1.8k|Java|2021/10/30|
|188|[Antabot/White-Jotter](https://github.com/Antabot/White-Jotter)|白卷是一款使用 Vue+Spring Boot 开发的前后端分离项目，附带全套开发教程。（A simple CMS developed by Spring Boot and Vue.js with development tutorials）|1.8k|Java|2021/11/30|
|189|[zhegexiaohuozi/SeimiCrawler](https://github.com/zhegexiaohuozi/SeimiCrawler)|一个简单、敏捷、分布式的支持SpringBoot的Java爬虫框架;An agile, distributed crawler framework.|1.8k|Java|2021/06/24|
|190|[vector4wang/spring-boot-quick](https://github.com/vector4wang/spring-boot-quick)|:herb: 基于springboot的快速学习示例,整合自己遇到的开源框架,如：rabbitmq(延迟队列)、Kafka、jpa、redies、oauth2、swagger、jsp、docker、spring-batch、异常处理、日志输出、多模块开发、多环境打包、缓存cache、爬虫、jwt、GraphQL、dubbo、zookeeper和Async等等:pushpin:|1.8k|Java|2021/08/25|
|191|[donkingliang/ConsecutiveScroller](https://github.com/donkingliang/ConsecutiveScroller)|ConsecutiveScrollerLayout是Android下支持多个滑动布局(RecyclerView、WebView、ScrollView等)和普通控件(TextView、ImageView、LinearLayou、自定义View等)持续连贯滑动的容器,它使所有的子View像一个整体一样连续顺畅滑动。并且支持布局吸顶功能。|1.8k|Java|2021/08/11|
|192|[lealone/Lealone](https://github.com/lealone/Lealone)|极具创新的面向微服务和 OLTP/OLAP 场景的单机与分布式关系数据库|1.8k|Java|2021/11/24|
|193|[qiujiayu/AutoLoadCache](https://github.com/qiujiayu/AutoLoadCache)|  AutoLoadCache 是基于AOP+Annotation等技术实现的高效的缓存管理解决方案，实现缓存与业务逻辑的解耦，并增加异步刷新及“拿来主义机制”，以适应高并发环境下的使用。|1.8k|Java|2021/08/27|
|194|[kanwangzjm/funiture](https://github.com/kanwangzjm/funiture)|慕课网课程推荐 Java并发编程与高并发解决方案：http://coding.imooc.com/class/195.html Java开发企业级权限管理系统：http://coding.imooc.com/class/149.html github: https://github.com/kanwangzjm/funiture, spring项目，权限管理、系统监控、定时任务动态调整、qps限制、sql监控(邮件)、验证码服务、短链接服务、动态配置等|1.8k|Java|2021/08/24|
|195|[dromara/raincat](https://github.com/dromara/raincat)|强一致分布式事务框架|1.8k|Java|2021/06/04|
|196|[a466350665/smart-sso](https://github.com/a466350665/smart-sso)|springboot SSO 单点登录，OAuth2实现，支持App登录，支持分布式|1.8k|Java|2021/10/16|
|197|[baichengzhou/SpringMVC-Mybatis-Shiro-redis-0.2](https://github.com/baichengzhou/SpringMVC-Mybatis-Shiro-redis-0.2)|基于SpringMVC、Mybatis、Redis、Freemarker的Shiro管理Demo源码的升级版。 |1.8k|Java|2021/09/21|
|198|[sunfusheng/GlideImageView](https://github.com/sunfusheng/GlideImageView)|基于Glide V4.9.0封装的图片加载库，可以监听加载图片时的进度|1.7k|Java|2021/07/15|
|199|[alidili/Demos](https://github.com/alidili/Demos)|🔥折线图、Retrofit、RxJava、RxLifecycle、DataBinding、MVP、MVVM、自动化测试工具UiAutomator、自定义控件、RecyclerView扩展组件、NDK开发、Design Support Library、蓝牙BLE开发、正则表达式|1.6k|Java|2021/11/18|
|200|[xuxueli/xxl-sso](https://github.com/xuxueli/xxl-sso)|A distributed single-sign-on framework.（分布式单点登录框架XXL-SSO）|1.6k|Java|2021/06/04|

⬆ [回到目录](#内容目录)

<br/>

## JavaScript

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[scutan90/DeepLearning-500-questions](https://github.com/scutan90/DeepLearning-500-questions)|深度学习500问，以问答形式对常用的概率知识、线性代数、机器学习、深度学习、计算机视觉等热点问题进行阐述，以帮助自己及有需要的读者。 全书分为18个章节，50余万字。由于水平有限，书中不妥之处恳请广大读者批评指正。   未完待续............ 如有意合作，联系scutjy2015@163.com                     版权所有，违权必究       Tan 2018.06|46.6k|JavaScript|2021/10/14|
|2|[azl397985856/leetcode](https://github.com/azl397985856/leetcode)| LeetCode Solutions: A Record of My Problem Solving Journey.( leetcode题解，记录自己的leetcode解题之路。)|45.4k|JavaScript|2021/11/09|
|3|[chinese-poetry/chinese-poetry](https://github.com/chinese-poetry/chinese-poetry)|The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库,  唐宋两朝近一万四千古诗人,  接近5.5万首唐诗加26万宋诗.  两宋时期1564位词人，21050首词。|34.7k|JavaScript|2021/11/27|
|4|[dcloudio/uni-app](https://github.com/dcloudio/uni-app)|uni-app 是使用 Vue 语法开发小程序、H5、App的统一框架|34.6k|JavaScript|2021/11/30|
|5|[NervJS/taro](https://github.com/NervJS/taro)|开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。  https://taro.zone/|30.1k|JavaScript|2021/12/01|
|6|[YMFE/yapi](https://github.com/YMFE/yapi)|YApi 是一个可本地部署的、打通前后端及QA的、可视化的接口管理平台|23.1k|JavaScript|2021/11/30|
|7|[Binaryify/NeteaseCloudMusicApi](https://github.com/Binaryify/NeteaseCloudMusicApi)|网易云音乐 Node.js API service|22.1k|JavaScript|2021/11/30|
|8|[Tencent/wepy](https://github.com/Tencent/wepy)|小程序组件化开发框架|21.6k|JavaScript|2021/11/16|
|9|[Meituan-Dianping/mpvue](https://github.com/Meituan-Dianping/mpvue)|基于 Vue.js 的小程序开发框架，从底层支持 Vue.js 语法和构建工具体系。|20.4k|JavaScript|2021/08/11|
|10|[ruanyf/es6tutorial](https://github.com/ruanyf/es6tutorial)|《ECMAScript 6入门》是一本开源的 JavaScript 语言教程，全面介绍 ECMAScript 6 新增的语法特性。|19.7k|JavaScript|2021/11/30|
|11|[haizlin/fe-interview](https://github.com/haizlin/fe-interview)|前端面试每日 3+1，以面试题来驱动学习，提倡每日学习与思考，每天进步一点！每天早上5点纯手工发布面试题（死磕自己，愉悦大家），5000+道前端面试题全面覆盖，HTML/CSS/JavaScript/Vue/React/Nodejs/TypeScript/ECMAScritpt/Webpack/Jquery/小程序/软技能……|19.5k|JavaScript|2021/12/01|
|12|[zhaoolee/ChromeAppHeroes](https://github.com/zhaoolee/ChromeAppHeroes)|🌈谷粒-Chrome插件英雄榜, 为优秀的Chrome插件写一本中文说明书, 让Chrome插件英雄们造福人类~  ChromePluginHeroes, Write a Chinese manual for the excellent Chrome plugin, let the Chrome plugin heroes benefit the human~ 公众号「0加1」同步更新|19.0k|JavaScript|2021/10/03|
|13|[ryanhanwu/How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way)|本文原文由知名 Hacker Eric S. Raymond 所撰寫，教你如何正確的提出技術問題並獲得你滿意的答案。|18.2k|JavaScript|2021/10/13|
|14|[qianguyihao/Web](https://github.com/qianguyihao/Web)|千古前端图文教程，超详细的前端入门到进阶学习笔记。从零开始学前端，做一名精致优雅的前端工程师。公众号「千古壹号」作者。|18.2k|JavaScript|2021/11/17|
|15|[ascoders/weekly](https://github.com/ascoders/weekly)|前端精读周刊。帮你理解最前沿、实用的技术。|16.8k|JavaScript|2021/11/29|
|16|[youzan/vant-weapp](https://github.com/youzan/vant-weapp)|轻量、可靠的小程序 UI 组件库|15.4k|JavaScript|2021/12/01|
|17|[EastWorld/wechat-app-mall](https://github.com/EastWorld/wechat-app-mall)|微信小程序商城，微信小程序微店|15.3k|JavaScript|2021/11/30|
|18|[lyswhut/lx-music-desktop](https://github.com/lyswhut/lx-music-desktop)|一个基于 electron 的音乐软件|14.5k|JavaScript|2021/11/21|
|19|[dcloudio/mui](https://github.com/dcloudio/mui)|最接近原生APP体验的高性能框架|13.3k|JavaScript|2021/07/07|
|20|[qier222/YesPlayMusic](https://github.com/qier222/YesPlayMusic)|高颜值的第三方网易云播放器，支持 Windows / macOS / Linux :electron: |12.7k|JavaScript|2021/11/29|
|21|[xiandanin/magnetW](https://github.com/xiandanin/magnetW)|磁力链接聚合搜索|12.6k|JavaScript|2021/06/29|
|22|[stephentian/33-js-concepts](https://github.com/stephentian/33-js-concepts)|:scroll: 每个 JavaScript 工程师都应懂的33个概念 @leonardomso|12.5k|JavaScript|2021/08/12|
|23|[eip-work/kuboard-press](https://github.com/eip-work/kuboard-press)|Kuboard 是基于 Kubernetes 的微服务管理界面。同时提供 Kubernetes 免费中文教程，入门教程，最新版本的 Kubernetes v1.20 安装手册，(k8s install) 在线答疑，持续更新。|12.4k|JavaScript|2021/11/30|
|24|[Tencent/omi](https://github.com/Tencent/omi)| Front End Cross-Frameworks Framework - 前端跨框架跨平台框架|12.1k|JavaScript|2021/11/15|
|25|[modood/Administrative-divisions-of-China](https://github.com/modood/Administrative-divisions-of-China)|中华人民共和国行政区划：省级（省份直辖市自治区）、 地级（城市）、 县级（区县）、 乡级（乡镇街道）、 村级（村委会居委会） ，中国省市区镇村二级三级四级五级联动地址数据。|11.6k|JavaScript|2021/07/15|
|26|[answershuto/learnVue](https://github.com/answershuto/learnVue)|:octocat:Vue.js 源码解析|11.5k|JavaScript|2021/07/26|
|27|[apachecn/apachecn-algo-zh](https://github.com/apachecn/apachecn-algo-zh)|ApacheCN 数据结构与算法译文集|10.5k|JavaScript|2021/05/09|
|28|[didi/chameleon](https://github.com/didi/chameleon)|🦎 一套代码运行多端，一端所见即多端所见|8.6k|JavaScript|2021/10/07|
|29|[sentsin/layer](https://github.com/sentsin/layer)|丰富多样的 Web 弹出层组件，可轻松实现 Alert/Confirm/Prompt/ 普通提示/页面区块/iframe/tips等等几乎所有的弹出交互。目前已成为最多人使用的弹层解决方案|8.2k|JavaScript|2021/05/22|
|30|[evil-huawei/evil-huawei](https://github.com/evil-huawei/evil-huawei)|Evil Huawei - 华为作过的恶|7.8k|JavaScript|2021/07/02|
|31|[timqian/chinese-independent-blogs](https://github.com/timqian/chinese-independent-blogs)|中文独立博客列表|7.7k|JavaScript|2021/12/01|
|32|[litten/hexo-theme-yilia](https://github.com/litten/hexo-theme-yilia)|一个简洁优雅的hexo主题  A simple and elegant theme for hexo.|8.2k|JavaScript|2021/06/05|
|33|[Hackl0us/SS-Rule-Snippet](https://github.com/Hackl0us/SS-Rule-Snippet)|搜集、整理、维护 Surge / Quantumult (X) / Shadowrocket / Surfboard / clash (Premium) 实用规则。|7.2k|JavaScript|2021/11/19|
|34|[ljianshu/Blog](https://github.com/ljianshu/Blog)|关注基础知识，打造优质前端博客，公众号[前端工匠]的作者|6.9k|JavaScript|2021/10/06|
|35|[openspug/spug](https://github.com/openspug/spug)|开源运维平台：面向中小型企业设计的轻量级无Agent的自动化运维平台，整合了主机管理、主机批量执行、主机在线终端、文件在线上传下载、应用发布部署、在线任务计划、配置中心、监控、报警等一系列功能。|6.7k|JavaScript|2021/11/30|
|36|[ustbhuangyi/vue-analysis](https://github.com/ustbhuangyi/vue-analysis)|:thumbsup: Vue.js 源码分析|6.6k|JavaScript|2021/10/06|
|37|[wuchangming/spy-debugger](https://github.com/wuchangming/spy-debugger)|微信调试，各种WebView样式调试、手机浏览器的页面真机调试。便捷的远程调试手机页面、抓包工具，支持：HTTP/HTTPS，无需USB连接设备。|6.4k|JavaScript|2021/09/22|
|38|[hotoo/pinyin](https://github.com/hotoo/pinyin)|:cn: 汉字拼音 ➜ hàn zì pīn yīn|6.0k|JavaScript|2021/11/24|
|39|[Tencent/kbone](https://github.com/Tencent/kbone)|一个致力于微信小程序和 Web 端同构的解决方案|4.1k|JavaScript|2021/12/01|
|40|[KieSun/all-of-frontend](https://github.com/KieSun/all-of-frontend)|你想知道的前端内容都在这|4.0k|JavaScript|2021/10/20|
|41|[cteamx/Thief](https://github.com/cteamx/Thief)|一款创新跨平台摸鱼神器，支持小说、股票、网页、视频、直播、PDF、游戏等摸鱼模式，为上班族打造的上班必备神器，使用此软件可以让上班倍感轻松，远离 ICU。|3.9k|JavaScript|2021/09/10|
|42|[Tencent/westore](https://github.com/Tencent/westore)|更好的小程序项目架构|3.9k|JavaScript|2021/09/28|
|43|[margox/braft-editor](https://github.com/margox/braft-editor)|美观易用的React富文本编辑器，基于draft-js开发|4.3k|JavaScript|2021/11/02|
|44|[a597873885/webfunny_monitor](https://github.com/a597873885/webfunny_monitor)|webfunny是一款轻量级的前端监控系统，webfunny也是一款前端性能监控系统，无埋点监控前端日志，实时分析前端健康状态。webfunny is a lightweight front-end monitoring system and webfunny is also a front-end performance monitoring system. It monitors front-end logs and analyzes front-end health status in real time.|3.4k|JavaScript|2021/12/01|
|45|[apachecn/pytorch-doc-zh](https://github.com/apachecn/pytorch-doc-zh)|Pytorch 中文文档|3.3k|JavaScript|2021/10/19|
|46|[mdnice/markdown-nice](https://github.com/mdnice/markdown-nice)|支持主题设计的 Markdown 编辑器，让排版变 Nice|3.3k|JavaScript|2021/10/14|
|47|[zhongshaofa/layuimini](https://github.com/zhongshaofa/layuimini)|后台admin前端模板，基于 layui 编写的最简洁、易用的后台框架模板。只需提供一个接口就直接初始化整个框架，无需复杂操作。|3.2k|JavaScript|2021/09/17|
|48|[dyq086/wepy-mall](https://github.com/dyq086/wepy-mall)|微信小程序--基于wepy 商城(微店)微信小程序 欢迎学习交流|3.2k|JavaScript|2021/05/18|
|49|[doramart/DoraCMS](https://github.com/doramart/DoraCMS)|DoraCMS是基于Nodejs+eggjs+mongodb编写的一套内容管理系统，结构简单，较目前一些开源的cms，doracms易于拓展，特别适合前端开发工程师做二次开发。|3.2k|JavaScript|2021/10/27|
|50|[Kujiale-Mobile/Painter](https://github.com/Kujiale-Mobile/Painter)|小程序生成图片库，轻松通过 json 方式绘制一张可以发到朋友圈的图片|3.2k|JavaScript|2021/11/05|
|51|[OBKoro1/koro1FileHeader](https://github.com/OBKoro1/koro1FileHeader)|VSCode插件：自动生成，自动更新VSCode文件头部注释, 自动生成函数注释并支持提取函数参数，支持所有主流语言，文档齐全，使用简单，配置灵活方便，持续维护多年。|3.1k|JavaScript|2021/11/29|
|52|[sunoj/jjb](https://github.com/sunoj/jjb)|一个帮助你自动申请京东价格保护的chrome拓展|3.1k|JavaScript|2021/10/25|
|53|[xiangyuecn/AreaCity-JsSpider-StatsGov](https://github.com/xiangyuecn/AreaCity-JsSpider-StatsGov)|省市区县乡镇三级或四级城市数据，带拼音标注、坐标、行政区域边界范围；2021年10月02日最新采集，提供csv格式文件，支持在线转成多级联动js代码、通用json格式，提供软件转成shp、geojson、sql、导入数据库；带浏览器里面运行的js采集源码，综合了中华人民共和国民政部、国家统计局、高德地图、腾讯地图行政区划数据|3.0k|JavaScript|2021/10/02|
|54|[o2oa/o2oa](https://github.com/o2oa/o2oa)|开源OA系统 - 码云GVP Java开源oa 企业OA办公平台 企业OA 协同办公OA 流程平台OA O2OA OA，支持国产麒麟操作系统和国产数据库（达梦、人大金仓），政务OA，军工信息化OA|3.0k|JavaScript|2021/12/01|
|55|[Ice-Hazymoon/MikuTools](https://github.com/Ice-Hazymoon/MikuTools)|一个轻量的工具集合|3.0k|JavaScript|2021/05/28|
|56|[iammapping/wedding](https://github.com/iammapping/wedding)|婚礼大屏互动，微信请柬一站式解决方案|2.9k|JavaScript|2021/05/07|
|57|[didi/mpx](https://github.com/didi/mpx)|Mpx，一款具有优秀开发体验和深度性能优化的增强型跨端小程序框架|2.9k|JavaScript|2021/11/30|
|58|[xiangyuecn/Recorder](https://github.com/xiangyuecn/Recorder)|html5 js 录音 mp3 wav ogg webm amr 格式，支持pc和Android、ios部分浏览器、和Hybrid App（提供Android IOS App源码），微信也是支持的，提供H5版语音通话聊天示例 和DTMF编解码|2.9k|JavaScript|2021/10/25|
|59|[blackmatrix7/ios_rule_script](https://github.com/blackmatrix7/ios_rule_script)|各平台的分流规则、复写规则及自动化脚本。|2.8k|JavaScript|2021/12/01|
|60|[feelschaotic/AndroidKnowledgeSystem](https://github.com/feelschaotic/AndroidKnowledgeSystem)|The most complete Android advanced route knowledge map ⭐️你想要的最全 Android 进阶路线知识图谱+干货资料收集🚀 |2.8k|JavaScript|2021/07/05|
|61|[renrenio/renren-fast-vue](https://github.com/renrenio/renren-fast-vue)|renren-fast-vue基于vue、element-ui构建开发，实现renren-fast后台管理前端功能，提供一套更优的前端解决方案。|2.8k|JavaScript|2021/05/07|
|62|[ronggang/PT-Plugin-Plus](https://github.com/ronggang/PT-Plugin-Plus)|PT 助手 Plus，为 Google Chrome 和 Firefox 浏览器插件（Web Extensions），主要用于辅助下载 PT 站的种子。|2.8k|JavaScript|2021/11/21|
|63|[toolgood/ToolGood.Words](https://github.com/toolgood/ToolGood.Words)|一款高性能敏感词(非法词/脏字)检测过滤组件，附带繁体简体互换，支持全角半角互换，汉字转拼音，模糊搜索等功能。|2.8k|JavaScript|2021/10/19|
|64|[JAVClub/core](https://github.com/JAVClub/core)|🔞 JAVClub - 让你的大姐姐不再走丢|2.7k|JavaScript|2021/06/14|
|65|[nashaofu/dingtalk](https://github.com/nashaofu/dingtalk)|钉钉桌面版，基于electron和钉钉网页版开发，支持Windows、Linux和macOS|2.7k|JavaScript|2021/09/29|
|66|[jasondu/wxa-plugin-canvas](https://github.com/jasondu/wxa-plugin-canvas)|小程序海报组件-生成朋友圈分享海报并生成图片|2.7k|JavaScript|2021/08/11|
|67|[wetools/wept](https://github.com/wetools/wept)|微信小程序多端实时运行工具|2.7k|JavaScript|2021/08/13|
|68|[huangwei9527/quark-h5](https://github.com/huangwei9527/quark-h5)|基于vue2 + koa2的 H5制作工具。让不会写代码的人也能轻松快速上手制作H5页面。类似易企秀、百度H5等H5制作、建站工具|2.6k|JavaScript|2021/06/18|
|69|[staven630/vue-cli4-config](https://github.com/staven630/vue-cli4-config)|vue-cli4配置vue.config.js持续更新|2.5k|JavaScript|2021/10/06|
|70|[hyj1991/easy-monitor](https://github.com/hyj1991/easy-monitor)|企业级 Node.js 应用性能监控与线上故障定位解决方案|2.4k|JavaScript|2021/08/01|
|71|[js-newbee/taro-yanxuan](https://github.com/js-newbee/taro-yanxuan)|首个 Taro 多端统一实例 - 网易严选（小程序 + H5 + React Native） - By 趣店 FED|2.4k|JavaScript|2021/10/06|
|72|[mumuy/data_location](https://github.com/mumuy/data_location)|中华人民共和国行政区划数据【省、市、区县、乡镇街道】中国省市区镇三级四级联动地址数据（GB/T 2260）|2.4k|JavaScript|2021/09/17|
|73|[justjavac/ReplaceGoogleCDN](https://github.com/justjavac/ReplaceGoogleCDN)|:cancer: 一个 Chrome 插件：将 Google CDN 替换为国内的。|2.4k|JavaScript|2021/09/10|
|74|[Tomotoes/scrcpy-gui](https://github.com/Tomotoes/scrcpy-gui)|👻 A simple & beautiful GUI application for scrcpy. QQ群:734330215|2.4k|JavaScript|2021/05/27|
|75|[aliyun/oss-browser](https://github.com/aliyun/oss-browser)|OSS Browser 提供类似windows资源管理器功能。用户可以很方便的浏览文件，上传下载文件，支持断点续传等。|2.4k|JavaScript|2021/07/28|
|76|[kgco/RateMySupervisor](https://github.com/kgco/RateMySupervisor)|永久免费开源的导师评价数据、数据爬虫、无需编程基础的展示网页以及新信息补充平台|2.4k|JavaScript|2021/11/01|
|77|[TaleLin/lin-cms-vue](https://github.com/TaleLin/lin-cms-vue)| 🔆 Vue+ElementPlus构建的CMS开发框架|2.3k|JavaScript|2021/11/24|
|78|[dushixiang/next-terminal](https://github.com/dushixiang/next-terminal)|Next Terminal是一个轻量级堡垒机系统，易安装，易使用，支持RDP、SSH、VNC、Telnet、Kubernetes协议。|2.3k|JavaScript|2021/11/25|
|79|[sxei/pinyinjs](https://github.com/sxei/pinyinjs)|一个实现汉字与拼音互转的小巧web工具库，演示地址：|2.3k|JavaScript|2021/05/14|
|80|[coffe1891/frontend-hard-mode-interview](https://github.com/coffe1891/frontend-hard-mode-interview)|《前端内参》，有关于JavaScript、编程范式、设计模式、软件开发的艺术等大前端范畴内的知识分享，旨在帮助前端工程师们夯实技术基础以通过一线互联网企业技术面试。|2.3k|JavaScript|2021/11/04|
|81|[rubickCenter/rubick](https://github.com/rubickCenter/rubick)|🔧  Electron based open source toolbox, free integration of rich plug-ins. 基于 electron 的开源工具箱，自由集成丰富插件。|2.3k|JavaScript|2021/11/22|
|82|[imfly/bitcoin-on-nodejs](https://github.com/imfly/bitcoin-on-nodejs)|《Node.js区块链开发》，注：新版代码已开源！请star支持哦-^-：|2.3k|JavaScript|2021/07/09|
|83|[lazy-luo/smarGate](https://github.com/lazy-luo/smarGate)|内网穿透，c++实现，无需公网IP，小巧，易用，快速，安全，最好的多链路聚合（p2p+proxy）模式，不做之一...这才是你真正想要的内网穿透工具！|2.3k|JavaScript|2021/11/24|
|84|[alibaba/butterfly](https://github.com/alibaba/butterfly)|🦋Butterfly，A JavaScript/React/Vue2 Diagramming library which concentrate on flow layout field.    (基于JavaScript/React/Vue2的流程图组件)|2.3k|JavaScript|2021/11/29|
|85|[w37fhy/QuantumultX](https://github.com/w37fhy/QuantumultX)|同步和更新大佬脚本库，更新懒人配置|2.2k|JavaScript|2021/10/04|
|86|[CarGuo/GSYGithubAPP](https://github.com/CarGuo/GSYGithubAPP)|超完整的React Native项目，功能丰富，适合学习和日常使用。GSYGithubApp系列的优势：我们目前已经拥有Flutter、Weex、ReactNative、kotlin 四个版本。 功能齐全，项目框架内技术涉及面广，完成度高，配套文章，适合全面学习，对比参考。开源Github客户端App，更好的体验，更丰富的功能，旨在更好的日常管理和维护个人Github，提供更好更方便的驾车体验Σ(￣。￣ﾉ)ﾉ。同款Weex版本 ： https://github.com/CarGuo/GSYGithubAppWeex 、同款Flutter版本 ： https://github.com/CarGu ...|2.2k|JavaScript|2021/10/08|
|87|[reruin/sharelist](https://github.com/reruin/sharelist)|快速分享 GoogleDrive OneDrive |2.2k|JavaScript|2021/11/05|
|88|[iamxjb/winxin-app-watch-life.net](https://github.com/iamxjb/winxin-app-watch-life.net)|微慕小程序开源版-WordPress版微信小程序|2.1k|JavaScript|2021/08/22|
|89|[llh911001/mostly-adequate-guide-chinese](https://github.com/llh911001/mostly-adequate-guide-chinese)|函数式编程指北中文版|2.1k|JavaScript|2021/09/17|
|90|[gkajs/gka](https://github.com/gkajs/gka)|一款高效、高性能的帧动画生成工具|1.8k|JavaScript|2021/10/10|
|91|[guanguans/notes](https://github.com/guanguans/notes)|:notebook_with_decorative_cover: Linux、MySQL、Nginx、PHP、Git、Shell 等笔记|1.8k|JavaScript|2021/09/17|
|92|[mindskip/xzs](https://github.com/mindskip/xzs)|学之思开源考试系统 - postgresql版，支持多种题型：选择题、多选题、判断题、填空题、解答题以及数学公式，包含PC端、小程序端，扩展性强，部署方便(集成部署、前后端分离部署、docker部署)、界面设计友好、代码结构清晰|1.8k|JavaScript|2021/12/01|
|93|[nmxiaowei/avue](https://github.com/nmxiaowei/avue)|Avue.js2.0是基于现有的element-ui库进行的二次封装，简化一些繁琐的操作，核心理念为数据驱动视图,主要的组件库针对table表格和form表单场景，同时衍生出更多企业常用的组件，达到高复用，容易维护和扩展的框架，同时内置了丰富了数据展示组件，让开发变得更加容易|1.8k|JavaScript|2021/11/29|
|94|[dai-siki/vue-image-crop-upload](https://github.com/dai-siki/vue-image-crop-upload)|A beautiful vue component for image cropping and uploading. （vue图片剪裁上传组件）|1.9k|JavaScript|2021/09/10|
|95|[soyaine/JavaScript30](https://github.com/soyaine/JavaScript30)|有关 @wesbos 的课程 JavaScript-30 的中文练习指南|1.9k|JavaScript|2021/10/12|
|96|[dntzhang/cax](https://github.com/dntzhang/cax)|HTML5 Canvas 2D Rendering Engine - 小程序、小游戏以及 Web 通用 Canvas 渲染引擎|1.9k|JavaScript|2021/05/25|
|97|[adlered/CSDNGreener](https://github.com/adlered/CSDNGreener)|《专 业 团 队》🕺🏿 🕺🏿 🕺🏿 🕺🏿 ⚰️🕺🏿 🕺🏿 🕺🏿 🕺🏿   专治 CSDN 广告与各种灵魂打击   🐵 油猴脚本   TamperMonkey   Chrome   FireFox   CSDN 页面浮窗广告完全过滤净化   国服最强 CSDN 绿化脚本|1.8k|JavaScript|2021/10/21|
|98|[yioMe/nodejs_wx_aipay_api](https://github.com/yioMe/nodejs_wx_aipay_api)|微信支付宝个人免签收款Api系统，有了它对接再也不用担心我的业务不能支付了|1.8k|JavaScript|2021/11/18|
|99|[mengkunsoft/MKOnlineMusicPlayer](https://github.com/mengkunsoft/MKOnlineMusicPlayer)|⛔【停止维护】一个在线音乐播放器（仅 UI，无功能）|1.8k|JavaScript|2021/06/28|
|100|[mumuy/widget](https://github.com/mumuy/widget)|A set of widgets based on jQuery&&javascript. 一套基于jquery或javascript的插件库 ：轮播、标签页、滚动条、下拉框、对话框、搜索提示、城市选择(城市三级联动)、日历等|1.6k|JavaScript|2021/11/05|
|101|[leochen-g/wechatBot](https://github.com/leochen-g/wechatBot)|微信每日说，三步教你用Node做一个微信哄女友(基友)神器！还能帮女朋友解决垃圾分类难题|1.6k|JavaScript|2021/09/04|
|102|[CreditTone/hooker](https://github.com/CreditTone/hooker)|🔥🔥hooker是一个基于frida实现的逆向工具包。为逆向开发人员提供统一化的脚本包管理方式、通杀脚本、自动化生成hook脚本、内存漫游探测activity和service、firda版JustTrustMe、disable ssl pinning|1.6k|JavaScript|2021/11/09|
|103|[edusoho/edusoho](https://github.com/edusoho/edusoho)|EduSoho 网络课堂是由杭州阔知网络科技有限公司研发的开源网校系统。EduSoho 包含了在线教学、招生和管理等完整功能，让教育机构可以零门槛建立网校，成功转型在线教育。EduSoho 也可作为企业内训平台，帮助企业实现人才培养。|1.6k|JavaScript|2021/11/18|
|104|[thinkgem/jeesite4](https://github.com/thinkgem/jeesite4)|Java EE 企业级快速开发平台，基于经典技术组合（Spring Boot、Spring MVC、Apache Shiro、MyBatis、Beetl、Bootstrap、AdminLTE），在线代码生成功能，包括核心模块如：组织机构、角色用户、菜单及按钮授权、数据权限、系统参数、内容管理、工作流等。采用松耦合设计；界面无刷新，一键换肤；众多账号安全设置，密码策略；在线定时任务配置；支持集群，支持SAAS；支持多数据源|1.6k|JavaScript|2021/11/18|
|105|[LANIF-UI/dva-boot-admin](https://github.com/LANIF-UI/dva-boot-admin)|:cake: react admin dashboard ui LANIF-ADMIN --- react 16 + react-router 4 + dva 2 + antd 4 后台管理 脚手架|1.5k|JavaScript|2021/10/06|
|106|[iBase4J/iBase4J](https://github.com/iBase4J/iBase4J)|Spring，SpringBoot 2.0，SpringMVC，Mybatis，mybatis-plus，motan/dubbo分布式，Redis缓存，Shiro权限管理，Spring-Session单点登录，Quartz分布式集群调度，Restful服务，QQ/微信登录，App token登录，微信/支付宝支付；日期转换、数据类型转换、序列化、汉字转拼音、身份证号码验证、数字转人民币、发送短信、发送邮件、加密解密、图片处理、excel导入导出、FTP/SFTP/fastDFS上传下载、二维码、XML读写、高精度计算、系统配置工具类等等。|1.5k|JavaScript|2021/09/09|
|107|[dillonzq/LoveIt](https://github.com/dillonzq/LoveIt)|❤️A clean, elegant but advanced blog theme for Hugo 一个简洁、优雅且高效的 Hugo 主题|1.5k|JavaScript|2021/12/01|
|108|[airyland/china-area-data](https://github.com/airyland/china-area-data)|中国省市区数据|1.5k|JavaScript|2021/07/19|
|109|[dream2023/f-render](https://github.com/dream2023/f-render)|f-render   基于 ElementUI 的表单设计器|1.5k|JavaScript|2021/05/11|
|110|[jones2000/HQChart](https://github.com/jones2000/HQChart)|HQChart - H5, 微信小程序 沪深/港股/数字货币/期货/美股 K线图(kline),走势图,缩放,拖拽,十字光标,画图工具,截图,筹码图. 分析家语法,通达信语法,(麦语法),第3方数据替换接口|1.5k|JavaScript|2021/11/30|
|111|[cnodejs/egg-cnode](https://github.com/cnodejs/egg-cnode)|CNode 社区 Egg 版本|1.5k|JavaScript|2021/09/07|
|112|[dream7180/foobox-cn](https://github.com/dream7180/foobox-cn)|CUI 整合 for foobar2000|1.5k|JavaScript|2021/11/22|
|113|[willnewii/qiniuClient](https://github.com/willnewii/qiniuClient)|云存储管理客户端。支持七牛云、腾讯云、青云、阿里云、又拍云、亚马逊S3、京东云，仿文件夹管理、图片预览、拖拽上传、文件夹上传、同步、批量导出URL等功能|1.4k|JavaScript|2021/11/27|
|114|[nodeWechat/wechat4u](https://github.com/nodeWechat/wechat4u)|微信 wechat web 网页版接口的 JavaScript 实现，兼容Node和浏览器，微信机器人|1.4k|JavaScript|2021/09/01|
|115|[phalapi/phalapi](https://github.com/phalapi/phalapi)|PhalApi开源接口框架，简称π框架，一个轻量级PHP开源接口框架，专注于接口服务开发。接口，从简单开始！|1.4k|JavaScript|2021/11/03|
|116|[karsonzhang/fastadmin](https://github.com/karsonzhang/fastadmin)|基于 ThinkPHP5 和 Bootstrap 的极速后台开发框架，一键生成 CRUD，自动生成控制器、模型、视图、JS、语言包、菜单、回收站。|1.4k|JavaScript|2021/10/11|
|117|[purplebamboo/font-carrier](https://github.com/purplebamboo/font-carrier)|font-carrier是一个功能强大的字体操作库，使用它你可以随心所欲的操作字体。让你可以在svg的维度改造字体的展现形状。|1.4k|JavaScript|2021/08/20|
|118|[dcloudio/casecode](https://github.com/dcloudio/casecode)|DCloud开源项目集锦|1.4k|JavaScript|2021/06/09|
|119|[TommyLemon/APIAuto](https://github.com/TommyLemon/APIAuto)|☔ 敏捷开发最强大易用的 HTTP 接口工具，机器学习零代码测试、生成代码与静态检查、生成文档与光标悬浮注释。☔ The most advanced tool for HTTP API. Testing with machine learning, generating codes and static analysis, generating comments and floating hints.|1.4k|JavaScript|2021/11/16|
|120|[lateautumn4lin/Review_Reverse](https://github.com/lateautumn4lin/Review_Reverse)|:wave:2019年末总结下今年做过的逆向，整理代码，复习思路。:pray:拼夕夕Web端anti_content参数逆向分析:japanese_goblin: WEB淘宝sign逆向分析；:smiley_cat:努比亚Cookie生成逆向分析；:raised_hands:百度指数data加密逆向分析 :footprints:今日头条WEB端_signature、as、cp参数逆向分析:notes:知乎登录formdata加密逆向分析 :clown_face:KNN猫眼字体反爬:tongue:Boss直聘Cookie加密字段__zp_stoken__逆向分析|1.4k|JavaScript|2021/09/08|
|121|[uiwjs/province-city-china](https://github.com/uiwjs/province-city-china)|🇨🇳最全最新中国【省、市、区县、乡镇街道】json,csv,sql数据|1.3k|JavaScript|2021/09/23|
|122|[fex-team/ua-device](https://github.com/fex-team/ua-device)|userAgent解析库|1.3k|JavaScript|2021/08/13|
|123|[yingye/weapp-qrcode](https://github.com/yingye/weapp-qrcode)|weapp.qrcode.js 在 微信小程序 中，快速生成二维码|1.3k|JavaScript|2021/09/16|
|124|[fuce1314/Springboot_v2](https://github.com/fuce1314/Springboot_v2)|SpringBoot_v2项目是努力打造springboot框架的极致细腻的脚手架。包括一套漂亮的前台。无其他杂七杂八的功能，原生纯净。|1.3k|JavaScript|2021/08/11|
|125|[VKSRC/Github-Monitor](https://github.com/VKSRC/Github-Monitor)|Github Sensitive Information Leakage Monitor(Github信息泄漏监控系统)|1.3k|JavaScript|2021/10/26|
|126|[Liiked/VS-Code-Extension-Doc-ZH](https://github.com/Liiked/VS-Code-Extension-Doc-ZH)|VS Code插件开发文档-中文版|1.3k|JavaScript|2021/07/18|
|127|[JavisPeng/taojinbi](https://github.com/JavisPeng/taojinbi)|淘宝淘金币自动执行脚本，包含蚂蚁森林收取能量，芭芭农场全任务，解放你的双手|1.3k|JavaScript|2021/08/03|
|128|[liyupi/code-nav](https://github.com/liyupi/code-nav)|💎 专业的编程导航，帮你找到优质的编程学习资源！公众号【编程导航】 ☁️ 前后端均开源，励志成为最好的全栈云开发项目！|1.3k|JavaScript|2021/08/30|
|129|[lefex/FE](https://github.com/lefex/FE)|前端 100 天，帮助 10W 人入门并进阶前端。|1.3k|JavaScript|2021/11/03|
|130|[savingrun/WeHalo](https://github.com/savingrun/WeHalo)|WeHalo 简约风 的微信小程序版博客:sparkles:|1.3k|JavaScript|2021/08/02|
|131|[xuwujing/springBoot-study](https://github.com/xuwujing/springBoot-study)|SpringBoot学习的相关工程并辅以博文讲解。主要包括入门的Hello World、自定义配置的获取、集成mybatis的xml和注解使用、集成jpa的使用、集成druid进行项目的监控、事物实战使用， 项目打包、使用logback日志文件管理、添加过滤器和拦截器、多数据源、Restful风格的服务、集成elasticsearch、redis、netty、集成jsp和thymeleaf、集成storm、kafka等相关技术。|1.3k|JavaScript|2021/08/03|
|132|[MonsterNone/tmall-miao](https://github.com/MonsterNone/tmall-miao)|双十一天猫、京东任务一键完成（绕过淘宝检测，新版淘宝也能用）|1.3k|JavaScript|2021/11/03|
|133|[deepwel/Chinese-Annotator](https://github.com/deepwel/Chinese-Annotator)|Annotator for Chinese Text Corpus (UNDER DEVELOPMENT) 中文文本标注工具|1.3k|JavaScript|2021/09/02|
|134|[node-pinus/pinus](https://github.com/node-pinus/pinus)|A fast,scalable,distributed game server framework for Node.js, Powered by TypeScript.  一个TypeScript写的node.js分布式游戏/应用服务器框架（原型基于pomelo）。|1.2k|JavaScript|2021/12/01|
|135|[tnfe/FFCreator](https://github.com/tnfe/FFCreator)|一个基于node.js的高速短视频制作库  A fast short video processing library based on node.js|1.2k|JavaScript|2021/10/26|
|136|[ming-soft/MCMS](https://github.com/ming-soft/MCMS)|完整开源！Java快速开发平台！基于Spring、SpringMVC、Mybatis架构，MStore提供更多好用的插件与模板（文章、商城、微信、论坛、会员、评论、支付、积分、工作流、任务调度等，同时提供上百套免费模板任意选择），价值源自分享！铭飞系统不仅一套简单好用的开源系统、更是一整套优质的开源生态内容体系。铭飞的使命就是降低开发成本提高开发效率，提供全方位的企业级开发解决方案，每月28定期更新版本|1.2k|JavaScript|2021/10/21|
|137|[EasyTuan/taro-msparis](https://github.com/EasyTuan/taro-msparis)|🌱用 React 编写的基于Taro + Dva构建的适配不同端（微信/百度/支付宝小程序、H5、React-Native 等）的时装衣橱|1.2k|JavaScript|2021/10/06|
|138|[lihongxun945/gobang](https://github.com/lihongxun945/gobang)|javascript gobang AI，可能是github上最受欢迎的五子棋AI，源码+教程|1.2k|JavaScript|2021/11/29|
|139|[Suwings/MCSManager](https://github.com/Suwings/MCSManager)|全中文，轻量级，开箱即用，多实例和支持 Docker 的 Minecraft 服务端管理面板|1.2k|JavaScript|2021/11/27|
|140|[shfshanyue/blog](https://github.com/shfshanyue/blog)|在这里写一些工作中遇到的前端，后端以及运维的问题|1.2k|JavaScript|2021/11/27|
|141|[ant-move/Antmove](https://github.com/ant-move/Antmove)|小程序转换器，基于支付宝/微信小程序， 轻松地转换成其它平台的小程序。|1.2k|JavaScript|2021/11/24|
|142|[tinajs/tina](https://github.com/tinajs/tina)|:dancer: 一款轻巧的渐进式微信小程序框架|1.2k|JavaScript|2021/09/14|
|143|[BingKui/javascript-zh](https://github.com/BingKui/javascript-zh)|Airbnb 出品，目前非常流行的 JavaScript 代码规范（中文版）。其内对各种 js 范式的写法进行了详细的规定与说明，按照此规范写出的代码将会更加合理。|1.2k|JavaScript|2021/11/21|
|144|[im3x/Scriptables](https://github.com/im3x/Scriptables)|iOS14桌面组件神器（Scriptable）开发框架、教程、精美脚本分享|1.2k|JavaScript|2021/10/30|
|145|[antvis/wx-f2](https://github.com/antvis/wx-f2)|F2 的微信小程序|1.2k|JavaScript|2021/09/03|
|146|[AntSwordProject/antSword](https://github.com/AntSwordProject/antSword)|中国蚁剑是一款跨平台的开源网站管理工具。AntSword is a cross-platform website management toolkit.|1.2k|JavaScript|2021/11/30|
|147|[liyupi/daxigua](https://github.com/liyupi/daxigua)|最简单的魔改发布『 合成大西瓜 』，配套改图工具，不用改代码，修改配置即可！|1.2k|JavaScript|2021/08/20|
|148|[ITDragonBlog/daydayup](https://github.com/ITDragonBlog/daydayup)|每天都在进步，每周都在总结，Java架构师成长之路。目前已经完成：MongoDB，Netty，Nginx，MySQL，Java，Redis，Shiro，Solr，SpringBoot，SpringData，SSO，Mybatis，Kotlin，还在持续更新中|1.2k|JavaScript|2021/09/21|
|149|[l0o0/translators_CN](https://github.com/l0o0/translators_CN)|Zotero translator中文网页抓取插件|1.1k|JavaScript|2021/11/04|
|150|[yuri2peter/win10-ui](https://github.com/yuri2peter/win10-ui)|Win10风格的UI框架。Windows10 style UI framework.|1.1k|JavaScript|2021/07/13|
|151|[sl1673495/leetcode-javascript](https://github.com/sl1673495/leetcode-javascript)|:beers: 喝杯小酒，一起做题。前端攻城狮从零入门算法的宝藏题库，根据知名算法老师的经验总结了 100+ 道 LeetCode 力扣的经典题型 JavaScript 题解和思路。已按题目类型分 label，一起加油。|1.1k|JavaScript|2021/10/09|
|152|[vitozyf/lucky-draw](https://github.com/vitozyf/lucky-draw)|年会抽奖程序|1.1k|JavaScript|2021/10/06|
|153|[suxiaogang/WeiboPicBed](https://github.com/suxiaogang/WeiboPicBed)|新浪微博图床 Chrome扩展|1.1k|JavaScript|2021/10/12|
|154|[liguobao/58HouseSearch](https://github.com/liguobao/58HouseSearch)|地图搜租房【微信公众号、小程序：人生删除指南】|1.1k|JavaScript|2021/10/06|
|155|[sentsin/laydate](https://github.com/sentsin/laydate)|一款被广泛使用的高级 Web 日历组件，完全开源无偿且颜值与功能兼备，足以应对日期相关的各种业务场景|1.1k|JavaScript|2021/05/21|
|156|[cnwhy/nzh](https://github.com/cnwhy/nzh)|数字转中文（大写，小写）数字，金额。|1.1k|JavaScript|2021/07/02|
|157|[moshowgame/SpringBootCodeGenerator](https://github.com/moshowgame/SpringBootCodeGenerator)|又名大狼狗代码生成器，基于SpringBoot2+Freemarker的JAVA代码生成器，以释放双手为目的，支持mysql/oracle/pgsql三大数据库， 用DDL-SQL语句生成JPA/JdbcTemplate/Mybatis/MybatisPlus/BeetlSQL等相关代码.|1.1k|JavaScript|2021/10/31|
|158|[czj2369/jd_tb_auto](https://github.com/czj2369/jd_tb_auto)|基于AutoJs编写的618  双十一  淘宝  京东  赚喵币  赚汪汪币任务自动完成脚本|1.1k|JavaScript|2021/11/07|
|159|[sanyuan0704/my_blog](https://github.com/sanyuan0704/my_blog)|神三元的博客，一起构建完整知识体系|1.1k|JavaScript|2021/10/11|
|160|[Tsuk1ko/cq-picsearcher-bot](https://github.com/Tsuk1ko/cq-picsearcher-bot)|🤖 基于 saucenao / ascii2d / whatanime 的搜图机器人|1.1k|JavaScript|2021/11/23|
|161|[elecV2/elecV2P](https://github.com/elecV2/elecV2P)|一款基于 NodeJS，可通过 JS 修改网络请求，以及定时运行脚本或 SHELL 指令的网络工具。|1.1k|JavaScript|2021/11/21|
|162|[lan-tianxiang/jd_shell](https://github.com/lan-tianxiang/jd_shell)|Node网页开发工具|1.1k|JavaScript|2021/07/07|
|163|[wechatsync/Wechatsync](https://github.com/wechatsync/Wechatsync)|一键同步文章到多个内容平台，支持今日头条、WordPress、知乎、简书、掘金、CSDN、typecho各大平台，一次发布，多平台同步发布。解放个人生产力|1.0k|JavaScript|2021/11/27|
|164|[welkinwong/nodercms](https://github.com/welkinwong/nodercms)|轻量级内容管理系统，基于 Node.js + MongoDB 开发，拥有灵活的内容模型以及完善的权限角色机制。|1.0k|JavaScript|2021/11/04|
|165|[gdtool/zhaopp](https://github.com/gdtool/zhaopp)|一个Google Drive搜索引擎 https://GeZhong.vip|1.0k|JavaScript|2021/08/03|
|166|[xxxily/h5player](https://github.com/xxxily/h5player)|网页播放器增强脚本，支持：播放进度记录、播放倍率记录、快进快退、倍速播放、画面缩放等|1.0k|JavaScript|2021/11/27|
|167|[airuikun/front-core](https://github.com/airuikun/front-core)|前端前沿技术实现原理+源码解析|1.0k|JavaScript|2021/08/15|
|168|[hoothin/UserScripts](https://github.com/hoothin/UserScripts)|Greasemonkey scripts (e.g.,True URL downloads迅雷、快车、QQ旋风等专有链解密 & HacgGodTurn琉璃神社工具集 & DownloadAllContent懒人小说下载器 & Easy offline一键离线下载)|1.0k|JavaScript|2021/10/13|
|169|[lqqyt2423/wechat_spider](https://github.com/lqqyt2423/wechat_spider)|微信爬虫，获取文章内容、阅读量、点赞量、评论等，获取公众号所有历史文章链接。|1.0k|JavaScript|2021/08/26|
|170|[Tencent/nohost](https://github.com/Tencent/nohost)|基于 Whistle 实现的多账号多环境远程配置及抓包调试平台|1.0k|JavaScript|2021/12/01|
|171|[qiushi123/xiaochengxu_demos](https://github.com/qiushi123/xiaochengxu_demos)|小程序优秀项目源码汇总，每个项目都有图有源码，零基础学微信小程序，小程序表格，小程序视频，小程序视频弹幕，小程序仿天猫大转盘抽奖等源码，小程序云开发，小程序发邮件，小程序支付，微信支付，持续更新。。。|1.0k|JavaScript|2021/05/30|
|172|[onblog/BlogHelper](https://github.com/onblog/BlogHelper)|帮助国内用户写作的托盘助手，一键发布本地文章到主流博客平台（知乎、简书、博客园、CSDN、SegmentFault、掘金、开源中国），剪贴板图片一键上传至图床（新浪、Github、图壳、腾讯云、阿里云、又拍云、七牛云）|999|JavaScript|2021/11/26|
|173|[mumuy/relationship](https://github.com/mumuy/relationship)|Chinese kinship system.中国亲戚关系计算器 - 家庭称谓/称呼计算/亲戚关系算法|986|JavaScript|2021/11/30|
|174|[qappleh/Interview](https://github.com/qappleh/Interview)|我是追梦赤子心，公众号「深圳湾码农」的作者，某上市集团公司高级前端开发，深耕前端领域多年，每天攻破一道题，带你从0到1系统构建web全栈完整的知识体系！|978|JavaScript|2021/10/12|
|175|[ElemeFE/react-amap](https://github.com/ElemeFE/react-amap)|基于 React 封装的高德地图组件。AMap Component Based On React.|978|JavaScript|2021/11/03|
|176|[HeiSir2014/M3U8-Downloader](https://github.com/HeiSir2014/M3U8-Downloader)| M3U8-Downloader 支持多线程、断点续传、加密视频下载缓存。|978|JavaScript|2021/11/27|
|177|[gz-yami/mall4j](https://github.com/gz-yami/mall4j)|电商商城系统 java商城|977|JavaScript|2021/11/30|
|178|[BeautyYuYanli/full-mark-composition-generator](https://github.com/BeautyYuYanli/full-mark-composition-generator)|将专业术语和名人名言以随机报菜名的方式填入模板，生成一篇只有聪明人才能看懂的满分作文！|968|JavaScript|2021/11/15|
|179|[gopeak/masterlab](https://github.com/gopeak/masterlab)|简单高效、基于敏捷开发的项目管理工具|961|JavaScript|2021/09/09|
|180|[xuhuai66/used-book-pro](https://github.com/xuhuai66/used-book-pro)|微信小程序云开发校园二手书商城，可在线支付提现，源码全开源|936|JavaScript|2021/05/30|
|181|[zhp8341/flink-streaming-platform-web](https://github.com/zhp8341/flink-streaming-platform-web)|基于flink-sql的实时流计算web平台|936|JavaScript|2021/10/28|
|182|[WeBankFinTech/fes.js](https://github.com/WeBankFinTech/fes.js)|Fes.js 是一个好用的前端应用解决方案。提供覆盖编译构建到代码运行的每个生命周期的插件体系，支持各种功能扩展和业务需求。以 路由为基础，同时支持配置式路由和约定式路由，保证路由的功能完备。整体上以约定、配置化、组件化的设计思想，让用户仅仅关心用组件搭建页面内容。基于Vue.js3.0，充分利用Vue丰富的生态。技术曲线平缓，上手也简单。在经过多个项目中打磨后趋于稳定。|933|JavaScript|2021/11/30|
|183|[dingyong0214/ThorUI](https://github.com/dingyong0214/ThorUI)|ThorUI组件库，微信小程序项目代码分享，组件文档地址：https://www.thorui.cn/doc  。 最近更新时间：2021-10-01|926|JavaScript|2021/11/16|
|184|[leejaen/react-lz-editor](https://github.com/leejaen/react-lz-editor)|A multilingual react rich-text editor component includes media support such as texts, images, videos, audios, links etc. Development based on Draft-Js and Ant-design, good support html, markdown, draft-raw mode. 一款基于 draft-Js 和 ant-design 实现的 react 富文本编辑器组件，支持文本、图片、视频、音频、链接等元素插入，同时支持HTML、markdown、dr ...|921|JavaScript|2021/08/11|
|185|[Plortinus/element-china-area-data](https://github.com/Plortinus/element-china-area-data)|:cn: Element UI && antd Cascader级联选择器 中国省市区三级、二级联动option数据|919|JavaScript|2021/10/06|
|186|[JacksonTian/anywhere](https://github.com/JacksonTian/anywhere)|Running static file server anywhere / 随启随用的静态文件服务器|911|JavaScript|2021/10/24|
|187|[wangduanduan/jsplumb-chinese-tutorial](https://github.com/wangduanduan/jsplumb-chinese-tutorial)|jsplumb中文教程,  README中没有的内容，可以查看项目的Wiki。有问题提issue|907|JavaScript|2021/11/04|
|188|[jpush/jpush-phonegap-plugin](https://github.com/jpush/jpush-phonegap-plugin)|JPush's officially supported PhoneGap/Cordova plugin (Android & iOS).  极光推送官方支持的 PhoneGap/Cordova 插件（Android & iOS）。|906|JavaScript|2021/07/28|
|189|[Wenmoux/checkbox](https://github.com/Wenmoux/checkbox)|常用网站签到本地/云函数/青龙脚本( 人人视频、刺猬猫小说、Acfun、WPS、 时光相册、书香门第论坛、绅士领域、好游快爆、埋堆堆、多看阅读、一亩三分地、闪艺app、吾爱破解、香网小说、晋江、橙光、什么值得买、网易蜗牛读书,网易云游戏平台、龙空论坛、NGA论坛、csdn、mt论坛、sf轻小说、猫耳FM、联想智选app、联想智选联想延保、联动云租车、数码之家、玩物志好物商店、togamemod、好书友论坛、鱼C论坛、帆软社区、村花论坛、纪录片之家、富贵论坛、ug爱好者、阅次元论坛、菜鸟图库、魅族社区、经管之家、有分享论坛、bigfun社区、阡陌居、HiFiNi、Hires后花园、曲奇云盘、游戏 ...|906|JavaScript|2021/11/29|
|190|[xuedingmiaojun/mp-unpack](https://github.com/xuedingmiaojun/mp-unpack)|基于electron-vue开发的跨平台微信小程序自助解包(反编译)客户端|906|JavaScript|2021/10/19|
|191|[mdnice/markdown-resume](https://github.com/mdnice/markdown-resume)|:necktie:支持 Markdown 和富文本的在线简历排版工具|894|JavaScript|2021/10/06|
|192|[lkyero/GitHubDesktop_zh](https://github.com/lkyero/GitHubDesktop_zh)|【GitHub Desktop】客户端中文汉化（简体）；【GitHub Desktop】 Simplified Chinese interface；（GitHub桌面版 中文汉化，非网页插件）|891|JavaScript|2021/07/23|
|193|[holynova/gushi_namer](https://github.com/holynova/gushi_namer)|古诗文起名: 利用诗经 楚辞 唐诗 宋词等给小朋友起名字|888|JavaScript|2021/09/04|
|194|[wx-chevalier/m-fe-boilerplates](https://github.com/wx-chevalier/m-fe-boilerplates)|Lucid & Futuristic Production Boilerplates For Frontend(Web) Apps, React/RN/Vue, with TypeScript(Optional), Webpack 4/Parcel, MobX/Redux :dizzy: 多技术栈前端项目模板|875|JavaScript|2021/10/13|
|195|[rust-lang-cn/rust-by-example-cn](https://github.com/rust-lang-cn/rust-by-example-cn)|Rust By Example 中文版(包含在线代码编辑器)|875|JavaScript|2021/11/23|
|196|[zhangdaren/miniprogram-to-uniapp](https://github.com/zhangdaren/miniprogram-to-uniapp)|轻松将各种小程序转换为uni-app项目|875|JavaScript|2021/11/25|
|197|[xuxuxu-ni/vue-xuAdmin](https://github.com/xuxuxu-ni/vue-xuAdmin)|vue+element-ui-admin 后台权限管理模板 演示地址:|873|JavaScript|2021/10/07|
|198|[JserWang/rc-bmap](https://github.com/JserWang/rc-bmap)|当百度地图遇上React，会产生怎样的火花🔥     🎉欢迎您的加入🎉|873|JavaScript|2021/09/01|
|199|[jabbany/ABPlayerHTML5](https://github.com/jabbany/ABPlayerHTML5)|Video Player for danmaku comments. ABPlayer in HTML5. ABPlayer核心构件以动态HTML编写的版本。向HTML5进发！HTML5弹幕播放器|871|JavaScript|2021/08/11|
|200|[microapp-store/linjiashop](https://github.com/microapp-store/linjiashop)|邻家小铺，轻量，简洁的商城系统,包括后台管理，手机h5，小程序，app版|860|JavaScript|2021/11/12|

⬆ [回到目录](#内容目录)

<br/>

## Python

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[jackfrued/Python-100-Days](https://github.com/jackfrued/Python-100-Days)|Python - 100天从新手到大师|112.6k|Python|2021/11/22|
|2|[521xueweihan/HelloGitHub](https://github.com/521xueweihan/HelloGitHub)|:octocat: 分享 GitHub 上有趣、入门级的开源项目。Share interesting, entry-level open source projects on GitHub.|49.0k|Python|2021/11/26|
|3|[0voice/interview_internal_reference](https://github.com/0voice/interview_internal_reference)|2021年最新总结，阿里，腾讯，百度，美团，头条等技术面试题目，以及答案，专家出题人分析汇总。|32.9k|Python|2021/08/25|
|4|[apachecn/AiLearning](https://github.com/apachecn/AiLearning)|AiLearning: 机器学习 - MachineLearning - ML、深度学习 - DeepLearning - DL、自然语言处理 NLP|32.1k|Python|2021/09/07|
|5|[testerSunshine/12306](https://github.com/testerSunshine/12306)|12306智能刷票，订票|30.3k|Python|2021/08/25|
|6|[d2l-ai/d2l-zh](https://github.com/d2l-ai/d2l-zh)|《动手学深度学习》：面向中文读者、能运行、可讨论。中英文版被55个国家的300所大学用于教学。|28.3k|Python|2021/12/01|
|7|[fxsjy/jieba](https://github.com/fxsjy/jieba)|结巴中文分词|27.4k|Python|2021/07/25|
|8|[hankcs/HanLP](https://github.com/hankcs/HanLP)|中文分词 词性标注 命名实体识别 依存句法分析 成分句法分析 语义依存分析 语义角色标注 指代消解 风格转换 语义相似度 新词发现 关键词短语提取 自动摘要 文本分类聚类 拼音简繁转换 自然语言处理|24.5k|Python|2021/11/25|
|9|[wangzheng0822/algo](https://github.com/wangzheng0822/algo)|数据结构和算法必知必会的50个代码实现|20.0k|Python|2021/11/15|
|10|[jumpserver/jumpserver](https://github.com/jumpserver/jumpserver)|JumpServer 是全球首款开源的堡垒机，是符合 4A 的专业运维安全审计系统。|17.5k|Python|2021/11/30|
|11|[PaddlePaddle/Paddle](https://github.com/PaddlePaddle/Paddle)|PArallel Distributed Deep LEarning: Machine Learning Framework from Industrial Practice （『飞桨』核心框架，深度学习&机器学习高性能单机、分布式训练和跨平台部署）|17.1k|Python|2021/12/01|
|12|[Jack-Cherish/python-spider](https://github.com/Jack-Cherish/python-spider)|:rainbow:Python3网络爬虫实战：淘宝、京东、网易云、B站、12306、抖音、笔趣阁、漫画小说下载、音乐电影下载等|14.1k|Python|2021/10/15|
|13|[Kr1s77/awesome-python-login-model](https://github.com/Kr1s77/awesome-python-login-model)|😮python模拟登陆一些大型网站，还有一些简单的爬虫，希望对你们有所帮助❤️，如果喜欢记得给个star哦🌟|13.9k|Python|2021/07/24|
|14|[jhao104/proxy_pool](https://github.com/jhao104/proxy_pool)|Python爬虫代理IP池(proxy pool)|13.8k|Python|2021/11/30|
|15|[leisurelicht/wtfpython-cn](https://github.com/leisurelicht/wtfpython-cn)|wtfpython的中文翻译/施工结束/ 能力有限，欢迎帮我改进翻译|12.0k|Python|2021/07/08|
|16|[pjialin/py12306](https://github.com/pjialin/py12306)|🚂 12306 购票助手，支持集群，多账号，多任务购票以及 Web 页面管理 |11.5k|Python|2021/10/21|
|17|[meolu/walle-web](https://github.com/meolu/walle-web)|walle - 瓦力 Devops开源项目代码部署平台|11.3k|Python|2021/05/09|
|18|[Vonng/ddia](https://github.com/Vonng/ddia)|《Designing Data-Intensive Application》DDIA中文翻译|10.2k|Python|2021/11/30|
|19|[darknessomi/musicbox](https://github.com/darknessomi/musicbox)|网易云音乐命令行版本|9.1k|Python|2021/11/05|
|20|[programthink/zhao](https://github.com/programthink/zhao)|【编程随想】整理的《太子党关系网络》，专门揭露赵国的权贵|8.9k|Python|2021/08/01|
|21|[sfyc23/EverydayWechat](https://github.com/sfyc23/EverydayWechat)|微信助手：1.每日定时给好友（女友）发送定制消息。2.机器人自动回复好友。3.群助手功能（例如：查询垃圾分类、天气、日历、电影实时票房、快递物流、PM2.5等）|8.6k|Python|2021/06/22|
|22|[x-hw/amazing-qr](https://github.com/x-hw/amazing-qr)|💮 amazing QRCode generator in Python (supporting animated gif) - Python amazing 二维码生成器（支持 gif 动态图片二维码）|8.6k|Python|2021/09/08|
|23|[521xueweihan/GitHub520](https://github.com/521xueweihan/GitHub520)|:kissing_heart: 让你“爱”上 GitHub，解决访问时图裂、加载慢的问题。（无需安装）|8.4k|Python|2021/12/01|
|24|[Dod-o/Statistical-Learning-Method_Code](https://github.com/Dod-o/Statistical-Learning-Method_Code)|手写实现李航《统计学习方法》书中全部算法|8.4k|Python|2021/09/13|
|25|[wistbean/learn_python3_spider](https://github.com/wistbean/learn_python3_spider)|python爬虫教程系列、从0到1学习python爬虫，包括浏览器抓包，手机APP抓包，如 fiddler、mitmproxy，各种爬虫涉及的模块的使用，如：requests、beautifulSoup、selenium、appium、scrapy等，以及IP代理，验证码识别，Mysql，MongoDB数据库的python使用，多线程多进程爬虫的使用，css 爬虫加密逆向破解，JS爬虫逆向，分布式爬虫，爬虫项目实战实例等|8.4k|Python|2021/11/16|
|26|[jindongwang/transferlearning](https://github.com/jindongwang/transferlearning)|Transfer learning / domain adaptation / domain generalization / multi-task learning etc. Papers, codes, datasets, applications, tutorials.-迁移学习|8.3k|Python|2021/11/29|
|27|[tgbot-collection/YYeTsBot](https://github.com/tgbot-collection/YYeTsBot)|🎬 人人影视bot，完全对接人人影视全部无删减资源|8.3k|Python|2021/11/28|
|28|[wangshub/Douyin-Bot](https://github.com/wangshub/Douyin-Bot)|😍 Python 抖音机器人，论如何在抖音上找到漂亮小姐姐？ |7.7k|Python|2021/09/08|
|29|[QUANTAXIS/QUANTAXIS](https://github.com/QUANTAXIS/QUANTAXIS)|QUANTAXIS 支持任务调度 分布式部署的 股票/期货/期权/港股/虚拟货币  数据/回测/模拟/交易/可视化/多账户 纯本地量化解决方案|6.1k|Python|2021/12/01|
|30|[kangvcar/InfoSpider](https://github.com/kangvcar/InfoSpider)|INFO-SPIDER 是一个集众多数据源于一身的爬虫工具箱🧰，旨在安全快捷的帮助用户拿回自己的数据，工具代码开源，流程透明。支持数据源包括GitHub、QQ邮箱、网易邮箱、阿里邮箱、新浪邮箱、Hotmail邮箱、Outlook邮箱、京东、淘宝、支付宝、中国移动、中国联通、中国电信、知乎、哔哩哔哩、网易云音乐、QQ好友、QQ群、生成朋友圈相册、浏览器浏览历史、12306、博客园、CSDN博客、开源中国博客、简书。|6.0k|Python|2021/09/08|
|31|[lancopku/pkuseg-python](https://github.com/lancopku/pkuseg-python)|pkuseg多领域中文分词工具; The pkuseg toolkit for multi-domain Chinese word segmentation|5.7k|Python|2021/10/19|
|32|[Jack-Cherish/Machine-Learning](https://github.com/Jack-Cherish/Machine-Learning)|:zap:机器学习实战（Python3）：kNN、决策树、贝叶斯、逻辑回归、SVM、线性回归、树回归|5.5k|Python|2021/07/07|
|33|[Jrohy/multi-v2ray](https://github.com/Jrohy/multi-v2ray)|v2ray/xray多用户管理部署程序|5.3k|Python|2021/11/23|
|34|[lcdevelop/ChatBotCourse](https://github.com/lcdevelop/ChatBotCourse)|自己动手做聊天机器人教程|5.2k|Python|2021/09/06|
|35|[wb14123/seq2seq-couplet](https://github.com/wb14123/seq2seq-couplet)|Play couplet with seq2seq model. 用深度学习对对联。|5.1k|Python|2021/10/15|
|36|[chyroc/WechatSogou](https://github.com/chyroc/WechatSogou)|基于搜狗微信搜索的微信公众号爬虫接口|5.2k|Python|2021/10/09|
|37|[pythonstock/stock](https://github.com/pythonstock/stock)|stock，股票系统。使用python进行开发。|4.8k|Python|2021/11/14|
|38|[dataabc/weiboSpider](https://github.com/dataabc/weiboSpider)|新浪微博爬虫，用python爬取新浪微博数据|4.8k|Python|2021/11/01|
|39|[xiaolai/regular-investing-in-box](https://github.com/xiaolai/regular-investing-in-box)|定投改变命运 —— 让时间陪你慢慢变富 https://onregularinvesting.com|4.7k|Python|2021/11/30|
|40|[biancangming/wtv](https://github.com/biancangming/wtv)|解决电脑、手机看电视直播的苦恼，收集各种直播源，电视直播网站|4.6k|Python|2021/11/22|
|41|[nl8590687/ASRT_SpeechRecognition](https://github.com/nl8590687/ASRT_SpeechRecognition)|A Deep-Learning-Based Chinese Speech Recognition System 基于深度学习的中文语音识别系统|4.9k|Python|2021/11/27|
|42|[QingdaoU/OnlineJudge](https://github.com/QingdaoU/OnlineJudge)|open source online judge based on Vue, Django and Docker.   青岛大学开源 Online Judge   QQ群 496710125   admin@qduoj.com|4.4k|Python|2021/11/19|
|43|[akfamily/akshare](https://github.com/akfamily/akshare)|AKShare is an elegant and simple financial data interface library for Python, built for human beings! 开源财经数据接口库|4.3k|Python|2021/11/30|
|44|[tychxn/jd-assistant](https://github.com/tychxn/jd-assistant)|京东抢购助手：包含登录，查询商品库存/价格，添加/清空购物车，抢购商品(下单)，查询订单等功能|4.3k|Python|2021/09/06|
|45|[liangliangyy/DjangoBlog](https://github.com/liangliangyy/DjangoBlog)|🍺基于Django的博客系统|4.2k|Python|2021/11/17|
|46|[shmilylty/OneForAll](https://github.com/shmilylty/OneForAll)|OneForAll是一款功能强大的子域收集工具|4.2k|Python|2021/11/20|
|47|[EssayKillerBrain/EssayKiller_V2](https://github.com/EssayKillerBrain/EssayKiller_V2)|基于开源GPT2.0的初代创作型人工智能   可扩展、可进化|4.1k|Python|2021/06/18|
|48|[yuanxiaosc/DeepNude-an-Image-to-Image-technology](https://github.com/yuanxiaosc/DeepNude-an-Image-to-Image-technology)|DeepNude's algorithm and general image generation theory and practice research, including pix2pix, CycleGAN, UGATIT, DCGAN, SinGAN, ALAE, mGANprior, StarGAN-v2 and VAE models (TensorFlow2 implementation). DeepNude的算法以及通用生成对抗网络（GAN,Generative Adversarial Network）图像生成的理论与实践研究。|4.0k|Python|2021/10/02|
|49|[chatopera/Synonyms](https://github.com/chatopera/Synonyms)|:herb: 中文近义词：聊天机器人，智能问答工具包|4.0k|Python|2021/09/14|
|50|[offu/WeRoBot](https://github.com/offu/WeRoBot)|WeRoBot 是一个微信公众号开发框架|4.0k|Python|2021/11/29|
|51|[TheKingOfDuck/fuzzDicts](https://github.com/TheKingOfDuck/fuzzDicts)|Web Pentesting Fuzz 字典,一个就够了。|4.0k|Python|2021/11/29|
|52|[PyQt5/PyQt](https://github.com/PyQt5/PyQt)|PyQt Examples（PyQt各种测试和例子） PyQt4 PyQt5|3.7k|Python|2021/07/13|
|53|[rootphantomer/Blasting_dictionary](https://github.com/rootphantomer/Blasting_dictionary)|爆破字典|3.7k|Python|2021/07/20|
|54|[wbt5/real-url](https://github.com/wbt5/real-url)|获取斗鱼&虎牙&哔哩哔哩&抖音&快手等 58 个直播平台的真实流媒体地址(直播源)和弹幕，直播源可在 PotPlayer、flv.js 等播放器中播放。|3.6k|Python|2021/11/30|
|55|[mozillazg/python-pinyin](https://github.com/mozillazg/python-pinyin)|汉字转拼音(pypinyin)|3.6k|Python|2021/11/14|
|56|[Jack-Cherish/PythonPark](https://github.com/Jack-Cherish/PythonPark)|Python 开源项目之「自学编程之路」，保姆级教程：AI实验室、宝藏视频、数据结构、学习指南、机器学习实战、深度学习实战、网络爬虫、大厂面经、程序人生、资源分享。|3.6k|Python|2021/11/07|
|57|[liuhuanyong/QASystemOnMedicalKG](https://github.com/liuhuanyong/QASystemOnMedicalKG)| A tutorial and implement of disease centered Medical knowledge graph and qa system based on it。知识图谱构建，自动问答，基于kg的自动问答。以疾病为中心的一定规模医药领域知识图谱，并以该知识图谱完成自动问答与分析服务。|3.5k|Python|2021/10/14|
|58|[yoshiko2/AV_Data_Capture](https://github.com/yoshiko2/AV_Data_Capture)|本地电影刮削与整理一体化解决方案|3.4k|Python|2021/11/30|
|59|[PantsuDango/Dango-Translator](https://github.com/PantsuDango/Dango-Translator)|团子翻译器 —— 个人兴趣制作的一款基于OCR技术的翻译器|3.4k|Python|2021/12/01|
|60|[PaddlePaddle/PaddleX](https://github.com/PaddlePaddle/PaddleX)|PaddlePaddle End-to-End Development Toolkit（『飞桨』深度学习全流程开发工具）|3.4k|Python|2021/12/01|
|61|[shidenggui/easyquotation](https://github.com/shidenggui/easyquotation)|实时获取新浪 / 腾讯 的免费股票行情 / 集思路的分级基金行情|3.2k|Python|2021/07/29|
|62|[aaPanel/BaoTa](https://github.com/aaPanel/BaoTa)|宝塔Linux面板 - 简单好用的服务器运维面板|3.1k|Python|2021/11/10|
|63|[hhyo/Archery](https://github.com/hhyo/Archery)|SQL 审核查询平台|3.1k|Python|2021/11/30|
|64|[imfht/fanhaodaquan](https://github.com/imfht/fanhaodaquan)| 番号大全。|3.1k|Python|2021/07/11|
|65|[wzpan/wukong-robot](https://github.com/wzpan/wukong-robot)|🤖 wukong-robot 是一个简单、灵活、优雅的中文语音对话机器人/智能音箱项目，还可能是首个支持脑机交互的开源智能音箱项目。|3.1k|Python|2021/09/11|
|66|[the0demiurge/ShadowSocksShare](https://github.com/the0demiurge/ShadowSocksShare)|Python爬虫/Flask网站/免费ShadowSocks账号/ssr订阅/json 订阅|3.1k|Python|2021/06/04|
|67|[DropsDevopsOrg/ECommerceCrawlers](https://github.com/DropsDevopsOrg/ECommerceCrawlers)|实战🐍多种网站、电商数据爬虫🕷。包含🕸：淘宝商品、微信公众号、大众点评、企查查、招聘网站、闲鱼、阿里任务、博客园、微博、百度贴吧、豆瓣电影、包图网、全景网、豆瓣音乐、某省药监局、搜狐新闻、机器学习文本采集、fofa资产采集、汽车之家、国家统计局、百度关键词收录数、蜘蛛泛目录、今日头条、豆瓣影评、携程、小米应用商店、安居客、途家民宿❤️❤️❤️。微信爬虫展示项目:|3.0k|Python|2021/09/08|
|68|[opendevops-cn/opendevops](https://github.com/opendevops-cn/opendevops)|CODO是一款为用户提供企业多混合云、一站式DevOps、自动化运维、完全开源的云管理平台、自动化运维平台|3.0k|Python|2021/10/24|
|69|[Rockyzsu/stock](https://github.com/Rockyzsu/stock)|30天掌握量化交易 (持续更新)|2.9k|Python|2021/11/30|
|70|[huangrt01/CS-Notes](https://github.com/huangrt01/CS-Notes)|我的自学笔记，终身更新，当前专注System基础、MLSys。|2.9k|Python|2021/08/18|
|71|[bowenpay/wechat-spider](https://github.com/bowenpay/wechat-spider)|微信公众号爬虫|2.9k|Python|2021/08/10|
|72|[DA-southampton/NLP_ability](https://github.com/DA-southampton/NLP_ability)|总结梳理自然语言处理工程师(NLP)需要积累的各方面知识，包括面试题，各种基础知识，工程能力等等，提升核心竞争力|2.9k|Python|2021/11/01|
|73|[FeeiCN/Cobra](https://github.com/FeeiCN/Cobra)|Source Code Security Audit (源代码安全审计)|2.8k|Python|2021/11/16|
|74|[cirosantilli/linux-kernel-module-cheat](https://github.com/cirosantilli/linux-kernel-module-cheat)|The perfect emulation setup to study and develop the Linux kernel v5.4.3, kernel modules, QEMU, gem5 and x86_64, ARMv7 and ARMv8 userland and baremetal assembly, ANSI C, C++ and POSIX. GDB step debug and KGDB just work. Powered by Buildroot and crosstool-NG. Highly automated. Thoroughly documented.  ...|2.7k|Python|2021/11/05|
|75|[jiangxufeng/v2rayL](https://github.com/jiangxufeng/v2rayL)|v2ray linux GUI客户端，支持订阅、vemss、ss等协议，自动更新订阅、检查版本更新|2.7k|Python|2021/09/08|
|76|[0xHJK/music-dl](https://github.com/0xHJK/music-dl)|search and download music 从网易云音乐、QQ音乐、酷狗音乐、百度音乐、虾米音乐、咪咕音乐等搜索和下载歌曲|2.7k|Python|2021/08/15|
|77|[JustMachiavelli/javsdt](https://github.com/JustMachiavelli/javsdt)|影片信息整理工具，抓取元数据nfo，自定义重命名文件(夹)，下载fanart裁剪poster，为emby、kodi、极影派铺路。|2.7k|Python|2021/11/22|
|78|[7eu7d7/genshin_auto_fish](https://github.com/7eu7d7/genshin_auto_fish)|基于深度强化学习的原神自动钓鱼AI|2.6k|Python|2021/10/30|
|79|[momosecurity/aswan](https://github.com/momosecurity/aswan)|陌陌风控系统静态规则引擎，零基础简易便捷的配置多种复杂规则，实时高效管控用户异常行为。|2.6k|Python|2021/06/15|
|80|[makelove/OpenCV-Python-Tutorial](https://github.com/makelove/OpenCV-Python-Tutorial)|OpenCV问答群不再维护。有问题，自己去搜索。Google能找到大部分答案。|2.6k|Python|2021/10/13|
|81|[Ehco1996/django-sspanel](https://github.com/Ehco1996/django-sspanel)|用diango开发的shadowsocks/V2ray面板|2.5k|Python|2021/11/26|
|82|[NewFuture/DDNS](https://github.com/NewFuture/DDNS)|:triangular_flag_on_post: 自动更新域名解析到本机IP(支持dnspod,阿里DNS,CloudFlare,华为云,DNSCOM...)|2.5k|Python|2021/10/12|
|83|[nghuyong/WeiboSpider](https://github.com/nghuyong/WeiboSpider)|This is a sina weibo spider built by scrapy [微博爬虫/持续维护]|2.4k|Python|2021/10/07|
|84|[newpanjing/simpleui](https://github.com/newpanjing/simpleui)|A modern theme based on vue+element-ui for django admin.一款基于vue+element-ui的django admin现代化主题。全球20000+网站都在使用！喜欢可以点个star✨|2.4k|Python|2021/11/05|
|85|[CLUEbenchmark/CLUE](https://github.com/CLUEbenchmark/CLUE)|中文语言理解测评基准 Chinese Language Understanding Evaluation Benchmark: datasets, baselines, pre-trained models, corpus and leaderboard  |2.4k|Python|2021/11/11|
|86|[iswbm/magic-python](https://github.com/iswbm/magic-python)|Python 黑魔法手册|2.3k|Python|2021/10/22|
|87|[GeneralNewsExtractor/GeneralNewsExtractor](https://github.com/GeneralNewsExtractor/GeneralNewsExtractor)| 新闻网页正文通用抽取器 Beta 版.|2.3k|Python|2021/10/11|
|88|[PyJun/Mooc_Downloader](https://github.com/PyJun/Mooc_Downloader)|学无止下载器，慕课下载器，Mooc下载，慕课网下载，中国大学下载，爱课程下载，网易云课堂下载，学堂在线下载，超星学习通下载；支持视频，课件同时下载|2.3k|Python|2021/06/30|
|89|[nickliqian/cnn_captcha](https://github.com/nickliqian/cnn_captcha)|use cnn recognize captcha by tensorflow. 本项目针对字符型图片验证码，使用tensorflow实现卷积神经网络，进行验证码识别。|2.3k|Python|2021/09/08|
|90|[LyleMi/Learn-Web-Hacking](https://github.com/LyleMi/Learn-Web-Hacking)|Study Notes For Web Hacking / Web安全学习笔记|2.2k|Python|2021/11/29|
|91|[kwai/DouZero](https://github.com/kwai/DouZero)|[ICML 2021] DouZero: Mastering DouDizhu with Self-Play Deep Reinforcement Learning   斗地主AI|2.2k|Python|2021/10/13|
|92|[kerlomz/captcha_trainer](https://github.com/kerlomz/captcha_trainer)|[验证码识别-训练] This project is based on CNN/ResNet/DenseNet+GRU/LSTM+CTC/CrossEntropy to realize verification code identification. This project is only for training the model.|2.2k|Python|2021/11/08|
|93|[shinnytech/tqsdk-python](https://github.com/shinnytech/tqsdk-python)|天勤量化开发包, 期货量化, 实时行情/历史数据/实盘交易|2.2k|Python|2021/11/26|
|94|[PegasusWang/python_data_structures_and_algorithms](https://github.com/PegasusWang/python_data_structures_and_algorithms)|Python 中文数据结构和算法教程|2.2k|Python|2021/11/08|
|95|[TophantTechnology/ARL](https://github.com/TophantTechnology/ARL)|ARL(Asset Reconnaissance Lighthouse)资产侦察灯塔系统旨在快速侦察与目标关联的互联网资产，构建基础资产信息库。 协助甲方安全团队或者渗透测试人员有效侦察和检索资产，发现存在的薄弱点和攻击面。|2.1k|Python|2021/11/07|
|96|[china-testing/python-api-tesing](https://github.com/china-testing/python-api-tesing)|python中文库-python人工智能大数据自动化接口测试开发。 书籍下载及python库汇总https://china-testing.github.io/  |2.1k|Python|2021/07/16|
|97|[BlankerL/DXY-COVID-19-Data](https://github.com/BlankerL/DXY-COVID-19-Data)|2019新型冠状病毒疫情时间序列数据仓库   COVID-19/2019-nCoV Infection Time Series Data Warehouse|2.0k|Python|2021/12/01|
|98|[zhzyker/vulmap](https://github.com/zhzyker/vulmap)|Vulmap 是一款 web 漏洞扫描和验证工具, 可对 webapps 进行漏洞扫描, 并且具备漏洞验证功能|2.0k|Python|2021/11/15|
|99|[brightmart/roberta_zh](https://github.com/brightmart/roberta_zh)|RoBERTa中文预训练模型: RoBERTa for Chinese |1.9k|Python|2021/07/08|
|100|[649453932/Bert-Chinese-Text-Classification-Pytorch](https://github.com/649453932/Bert-Chinese-Text-Classification-Pytorch)|使用Bert，ERNIE，进行中文文本分类|1.9k|Python|2021/10/19|
|101|[crownpku/Information-Extraction-Chinese](https://github.com/crownpku/Information-Extraction-Chinese)|Chinese Named Entity Recognition with IDCNN/biLSTM+CRF, and Relation Extraction with biGRU+2ATT 中文实体识别与关系提取|1.9k|Python|2021/11/11|
|102|[zhanyong-wan/dongbei](https://github.com/zhanyong-wan/dongbei)|东北方言编程语言|1.8k|Python|2021/10/08|
|103|[FeeiCN/GSIL](https://github.com/FeeiCN/GSIL)|GitHub Sensitive Information Leakage（GitHub敏感信息泄露监控）|1.8k|Python|2021/11/14|
|104|[guanguans/favorite-link](https://github.com/guanguans/favorite-link)|❤️ 每日收集喜欢的开源项目   RSS 订阅    快知 app 订阅|1.7k|Python|2021/12/01|
|105|[Xyntax/POC-T](https://github.com/Xyntax/POC-T)|渗透测试插件化并发框架 / Open-sourced remote vulnerability PoC/EXP framework|1.7k|Python|2021/09/24|
|106|[HuberTRoy/leetCode](https://github.com/HuberTRoy/leetCode)|:pencil2: 算法相关知识储备 LeetCode with Python and JavaScript :books:|1.7k|Python|2021/07/26|
|107|[seisman/how-to-write-makefile](https://github.com/seisman/how-to-write-makefile)|跟我一起写Makefile重制版|1.7k|Python|2021/09/22|
|108|[taojy123/KeymouseGo](https://github.com/taojy123/KeymouseGo)|类似按键精灵的鼠标键盘录制和自动化操作 模拟点击和键入   automate mouse clicks and keyboard input|1.7k|Python|2021/11/19|
|109|[TechXueXi/TechXueXi](https://github.com/TechXueXi/TechXueXi)|强国通 科技强国 学习强国 xuexiqiangguo 全网最好用开源网页学习强国助手：TechXueXi （懒人刷分工具 自动学习）技术强国，支持答题，支持 docker 45分/天|1.7k|Python|2021/11/29|
|110|[iswbm/pycharm-guide](https://github.com/iswbm/pycharm-guide)|PyCharm 中文指南：安装   破解   效率   技巧|1.5k|Python|2021/09/11|
|111|[Mr-xn/BurpSuite-collections](https://github.com/Mr-xn/BurpSuite-collections)|有关burpsuite的插件(非商店),文章以及使用技巧的收集(此项目不再提供burpsuite破解文件,如需要请在博客mrxn.net下载)---Collection of burpsuite plugins (non-stores), articles and tips for using Burpsuite, no crack version file|1.5k|Python|2021/11/21|
|112|[nonebot/nonebot](https://github.com/nonebot/nonebot)|基于 OneBot 标准的 Python 异步 QQ 机器人框架 / Asynchronous QQ robot framework based on OneBot for Python|1.5k|Python|2021/11/20|
|113|[abbeyokgo/PyOne](https://github.com/abbeyokgo/PyOne)|PyOne-一款给力的onedrive文件管理、分享程序|1.5k|Python|2021/09/24|
|114|[rabbitmask/WeblogicScan](https://github.com/rabbitmask/WeblogicScan)|Weblogic一键漏洞检测工具，V1.5，更新时间：20200730|1.5k|Python|2021/08/20|
|115|[dataabc/weibo-crawler](https://github.com/dataabc/weibo-crawler)|新浪微博爬虫，用python爬取新浪微博数据，并下载微博图片和微博视频|1.4k|Python|2021/10/10|
|116|[songyouwei/ABSA-PyTorch](https://github.com/songyouwei/ABSA-PyTorch)|Aspect Based Sentiment Analysis, PyTorch Implementations.  基于方面的情感分析，使用PyTorch实现。|1.4k|Python|2021/08/05|
|117|[sczhengyabin/Image-Downloader](https://github.com/sczhengyabin/Image-Downloader)|Download images from Google, Bing, Baidu. 谷歌、百度、必应图片下载.|1.4k|Python|2021/06/02|
|118|[HatBoy/Struts2-Scan](https://github.com/HatBoy/Struts2-Scan)|Struts2全漏洞扫描利用工具|1.4k|Python|2021/07/12|
|119|[wnma3mz/wechat_articles_spider](https://github.com/wnma3mz/wechat_articles_spider)|微信公众号文章的爬虫|1.4k|Python|2021/09/09|
|120|[ymcui/Chinese-XLNet](https://github.com/ymcui/Chinese-XLNet)|Pre-Trained Chinese XLNet（中文XLNet预训练模型）|1.4k|Python|2021/10/23|
|121|[MoyuScript/bilibili-api](https://github.com/MoyuScript/bilibili-api)|哔哩哔哩的API调用模块|1.4k|Python|2021/11/27|
|122|[blackholll/loonflow](https://github.com/blackholll/loonflow)|基于django的工作流引擎,工单(a workflow engine base on django python)|1.3k|Python|2021/11/21|
|123|[caj2pdf/caj2pdf](https://github.com/caj2pdf/caj2pdf)|Convert CAJ (China Academic Journals) files to PDF. 转换中国知网 CAJ 格式文献为 PDF。佛系转换，成功与否，皆是玄学。|1.3k|Python|2021/11/27|
|124|[conanhujinming/comments-for-awesome-courses](https://github.com/conanhujinming/comments-for-awesome-courses)|名校公开课程评价网|1.3k|Python|2021/11/29|
|125|[imcaspar/gpt2-ml](https://github.com/imcaspar/gpt2-ml)|GPT2 for Multiple Languages, including pretrained models. GPT2 多语言支持, 15亿参数中文预训练模型|1.3k|Python|2021/08/25|
|126|[guofei9987/blind_watermark](https://github.com/guofei9987/blind_watermark)|Blind Watermark （图片盲水印，提取水印无须原图！）|1.3k|Python|2021/07/21|
|127|[dingdang-robot/dingdang-robot](https://github.com/dingdang-robot/dingdang-robot)|🤖 叮当是一款可以工作在 Raspberry Pi 上的中文语音对话机器人/智能音箱项目。|1.3k|Python|2021/10/28|
|128|[Ascotbe/Medusa](https://github.com/Ascotbe/Medusa)|:cat2:Medusa是一个红队武器库平台，目前包括XSS平台、协同平台、CVE监控、免杀生成、DNSLOG、钓鱼邮件、文件获取等功能，持续开发中 |1.3k|Python|2021/11/27|
|129|[LoRexxar/Kunlun-M](https://github.com/LoRexxar/Kunlun-M)|KunLun-M是一个完全开源的静态白盒扫描工具，支持PHP、JavaScript的语义扫描，基础安全、组件安全扫描，Chrome Ext\Solidity的基础扫描。|1.3k|Python|2021/11/25|
|130|[luopeixiang/named_entity_recognition](https://github.com/luopeixiang/named_entity_recognition)|中文命名实体识别（包括多种模型：HMM，CRF，BiLSTM，BiLSTM+CRF的具体实现）|1.3k|Python|2021/06/24|
|131|[crownpku/Rasa_NLU_Chi](https://github.com/crownpku/Rasa_NLU_Chi)|Turn Chinese natural language into structured data 中文自然语言理解|1.3k|Python|2021/11/11|
|132|[jimmy201602/webterminal](https://github.com/jimmy201602/webterminal)|ssh rdp vnc telnet sftp bastion/jump web putty xshell terminal jumpserver audit realtime monitor rz/sz 堡垒机 云桌面 linux devops sftp websocket  file management rz/sz otp 自动化运维 审计 录像 文件管理 sftp上传 实时监控 录像回放 网页版rz/sz上传下载/动态口令 django|1.3k|Python|2021/10/06|
|133|[githublitao/api_automation_test](https://github.com/githublitao/api_automation_test)|接口自动化测试平台，已停止维护，看心情改改|1.2k|Python|2021/06/11|
|134|[yongzhuo/nlp_xiaojiang](https://github.com/yongzhuo/nlp_xiaojiang)|自然语言处理（nlp），小姜机器人（闲聊检索式chatbot），BERT句向量-相似度（Sentence Similarity），XLNET句向量-相似度（text xlnet embedding），文本分类（Text classification）， 实体提取（ner，bert+bilstm+crf），数据增强（text augment, data enhance），同义句同义词生成，句子主干提取（mainpart），中文汉语短文本相似度，文本特征工程，keras-http-service调用|1.2k|Python|2021/09/23|
|135|[ZainCheung/netease-cloud](https://github.com/ZainCheung/netease-cloud)|网易云音乐全自动每日打卡300首歌升级账号等级，支持微信提醒，支持无服务器云函数部署|1.2k|Python|2021/11/20|
|136|[dongweiming/web_develop](https://github.com/dongweiming/web_develop)|《Python Web开发实战》书中源码|1.2k|Python|2021/09/08|
|137|[yongzhuo/Keras-TextClassification](https://github.com/yongzhuo/Keras-TextClassification)|中文长文本分类、短句子分类、多标签分类、两句子相似度（Chinese Text Classification of Keras NLP, multi-label classify, or sentence classify, long or short），字词句向量嵌入层（embeddings）和网络层（graph）构建基类，FastText，TextCNN，CharCNN，TextRNN,  RCNN,  DCNN, DPCNN, VDCNN, CRNN, Bert, Xlnet, Albert, Attention, DeepMoji, HAN, 胶囊网络-CapsuleNet, Tran ...|1.2k|Python|2021/11/26|
|138|[jerry3747/taobao_seckill](https://github.com/jerry3747/taobao_seckill)|淘宝、天猫半价抢购，抢电视、抢茅台，干死黄牛党|1.2k|Python|2021/11/09|
|139|[dixudx/tumblr-crawler](https://github.com/dixudx/tumblr-crawler)|Easily download all the photos/videos from tumblr blogs. 下载指定的 Tumblr 博客中的图片，视频|1.1k|Python|2021/08/22|
|140|[DQinYuan/chinese_province_city_area_mapper](https://github.com/DQinYuan/chinese_province_city_area_mapper)|一个用于提取简体中文字符串中省，市和区并能够进行映射，检验和简单绘图的python模块|1.1k|Python|2021/09/08|
|141|[bubbliiiing/yolo3-pytorch](https://github.com/bubbliiiing/yolo3-pytorch)| 这是一个yolo3-pytorch的源码，可以用于训练自己的模型。|1.1k|Python|2021/11/17|
|142|[refraction-ray/xalpha](https://github.com/refraction-ray/xalpha)|基金投资管理回测引擎|1.1k|Python|2021/11/10|
|143|[PeterH0323/Smart_Construction](https://github.com/PeterH0323/Smart_Construction)|Head Person Helmet Detection on Construction Sites，基于目标检测工地安全帽和禁入危险区域识别系统，🚀😆附 YOLOv5 训练自己的数据集超详细教程🚀😆2021.3新增可视化界面❗❗|1.1k|Python|2021/09/28|
|144|[plctlab/v8-internals](https://github.com/plctlab/v8-internals)|面向编译器开发人员的V8内部实现文档|1.1k|Python|2021/11/15|
|145|[twtrubiks/docker-tutorial](https://github.com/twtrubiks/docker-tutorial)|Docker 基本教學 - 從無到有 Docker-Beginners-Guide   教你用 Docker 建立 Django + PostgreSQL 📝|1.1k|Python|2021/10/12|
|146|[alisen39/TrWebOCR](https://github.com/alisen39/TrWebOCR)|开源易用的中文离线OCR，识别率媲美大厂，并且提供了易用的web页面及web的接口，方便人类日常工作使用或者其他程序来调用~|1.0k|Python|2021/10/13|
|147|[TideSec/TideFinger](https://github.com/TideSec/TideFinger)|TideFinger——指纹识别小工具，汲取整合了多个web指纹库，结合了多种指纹检测方法，让指纹检测更快捷、准确。|1.0k|Python|2021/08/20|
|148|[ymcui/Chinese-ELECTRA](https://github.com/ymcui/Chinese-ELECTRA)|Pre-trained Chinese ELECTRA（中文ELECTRA预训练模型）|1.0k|Python|2021/11/04|
|149|[StepfenShawn/Cantonese](https://github.com/StepfenShawn/Cantonese)|粤语编程语言.The Cantonese programming language.|1.0k|Python|2021/11/28|
|150|[fastnlp/fitlog](https://github.com/fastnlp/fitlog)|fitlog是一款在深度学习训练中用于辅助用户记录日志和管理代码的工具|1.0k|Python|2021/10/28|
|151|[lengjibo/RedTeamTools](https://github.com/lengjibo/RedTeamTools)|记录自己编写、修改的部分工具|1.0k|Python|2021/07/11|
|152|[phodal/iot](https://github.com/phodal/iot)|IoT, 这是一个最小Internet of Things ，一个Internet of Things相关的毕业设计产生的一个简化的物联网系统。 。|1.0k|Python|2021/05/17|
|153|[aceimnorstuvwxz/dgk_lost_conv](https://github.com/aceimnorstuvwxz/dgk_lost_conv)| dgk_lost_conv 中文对白语料 chinese conversation corpus |1.0k|Python|2021/05/06|
|154|[yqchilde/JDMemberCloseAccount](https://github.com/yqchilde/JDMemberCloseAccount)|学习python操作selenium的一个🌰 ，也是一种京东全自动退会方案|1.0k|Python|2021/11/30|
|155|[mtianyan/VueDjangoAntdProBookShop](https://github.com/mtianyan/VueDjangoAntdProBookShop)|Vue前台 + Django3.1 + DjangoRestful Framework + Ant Design Pro V4 开发的二手书商城网站及后台管理|993|Python|2021/11/03|
|156|[HelloGitHub-Team/HelloDjango-blog-tutorial](https://github.com/HelloGitHub-Team/HelloDjango-blog-tutorial)|免费带你学 django 全栈！基于 django 2.2 的个人博客，初学者绝对不能错过的 django 教程！｡◕ᴗ◕｡|991|Python|2021/06/11|
|157|[MorvanZhou/Evolutionary-Algorithm](https://github.com/MorvanZhou/Evolutionary-Algorithm)|Evolutionary Algorithm using Python, 莫烦Python 中文AI教学|990|Python|2021/07/14|
|158|[nosarthur/gita](https://github.com/nosarthur/gita)|Manage many git repos with sanity 从容管理多个git库|987|Python|2021/11/26|
|159|[SimmerChan/KG-demo-for-movie](https://github.com/SimmerChan/KG-demo-for-movie)|从无到有构建一个电影知识图谱，并基于该KG，开发一个简易的KBQA程序。|987|Python|2021/10/05|
|160|[gxcuizy/Python](https://github.com/gxcuizy/Python)|Python3编写的各种大小程序，包含从零学Python系列、12306抢票、省市区地址库以及系列网站爬虫等学习源码|980|Python|2021/09/24|
|161|[anyant/rssant](https://github.com/anyant/rssant)|蚁阅 - 让 RSS 更好用，轻松订阅你喜欢的博客和资讯|979|Python|2021/11/13|
|162|[zjunlp/DeepKE](https://github.com/zjunlp/DeepKE)|An Open Toolkit for Knowledge Base Population（DeepKE是一个支持低资源、长篇章的开源知识抽取工具，具备命名实体识别、关系抽取和属性抽取等功能）|973|Python|2021/11/30|
|163|[yhangf/PythonCrawler](https://github.com/yhangf/PythonCrawler)| :heartpulse:用python编写的爬虫项目集合|968|Python|2021/05/14|
|164|[HaddyYang/django2.0-course](https://github.com/HaddyYang/django2.0-course)|Django2.0视频教程相关代码（杨仕航）|967|Python|2021/09/08|
|165|[zhimingshenjun/DD_Monitor](https://github.com/zhimingshenjun/DD_Monitor)|DD监控室第一版|965|Python|2021/10/14|
|166|[yzddmr6/WebCrack](https://github.com/yzddmr6/WebCrack)|WebCrack是一款web后台弱口令/万能密码批量检测工具，在工具中导入后台地址即可进行自动化检测。|958|Python|2021/09/07|
|167|[timwhitez/crawlergo_x_XRAY](https://github.com/timwhitez/crawlergo_x_XRAY)|360/0Kee-Team/crawlergo动态爬虫结合长亭XRAY扫描器的被动扫描功能|923|Python|2021/11/10|
|168|[nonebot/nonebot2](https://github.com/nonebot/nonebot2)|跨平台 Python 异步机器人框架|920|Python|2021/12/01|
|169|[rachpt/lanzou-gui](https://github.com/rachpt/lanzou-gui)|蓝奏云   蓝奏云客户端   蓝奏网盘 GUI版本|920|Python|2021/05/16|
|170|[mlouielu/twstock](https://github.com/mlouielu/twstock)|台灣股市股票價格擷取 (含即時股票資訊) - Taiwan Stock Opendata with realtime|908|Python|2021/11/20|
|171|[FeeiCN/ESD](https://github.com/FeeiCN/ESD)|Enumeration sub domains(枚举子域名)|904|Python|2021/09/16|
|172|[Alic-yuan/nlp-beginner-finish](https://github.com/Alic-yuan/nlp-beginner-finish)|此项目完成了关于 NLP-Beginner：自然语言处理入门练习 的所有任务，所有代码都经过测试,可以正常运行。|891|Python|2021/05/01|
|173|[yangxudong/deeplearning](https://github.com/yangxudong/deeplearning)|深度学习相关的模型训练、评估和预测相关代码|898|Python|2021/07/26|
|174|[kelvinBen/AppInfoScanner](https://github.com/kelvinBen/AppInfoScanner)|一款适用于以HW行动/红队/渗透测试团队为场景的移动端(Android、iOS、WEB、H5、静态网站)信息收集扫描工具，可以帮助渗透测试工程师、攻击队成员、红队成员快速收集到移动端或者静态WEB站点中关键的资产信息并提供基本的信息输出,如：Title、Domain、CDN、指纹信息、状态信息等。|874|Python|2021/08/31|
|175|[FunnyWolf/pystinger](https://github.com/FunnyWolf/pystinger)|Bypass firewall for traffic forwarding using webshell 一款使用webshell进行流量转发的出网工具|873|Python|2021/09/29|
|176|[andyzys/jd_seckill](https://github.com/andyzys/jd_seckill)|京东秒杀商品抢购|869|-|2021/11/11|
|177|[josonle/Coding-Now](https://github.com/josonle/Coding-Now)|学习记录的一些笔记，以及所看得一些电子书eBooks、视频资源和平常收纳的一些自己认为比较好的博客、网站、工具。涉及大数据几大组件、Python机器学习和数据分析、Linux、操作系统、算法、网络等|867|Python|2021/09/08|
|178|[baidu/CUP](https://github.com/baidu/CUP)|CUP, common useful python-lib.  (Currently, Most popular python lib in baidu).  Python 开发底层库, 涵盖util、service(threadpool/generator/executor/cache等等)、logging、monitoring、增强型配置 等等库支持|864|Python|2021/08/31|
|179|[Neeky/mysqltools](https://github.com/Neeky/mysqltools)|一个用于快速构建大规模，高质量，全自动化的 mysql分布式集群环境的工具；包含mysql 安装、备份、监控、高可用、读写分离、优化、巡检、自行化运维|860|Python|2021/11/16|
|180|[Hopetree/izone](https://github.com/Hopetree/izone)|django+bootstrap4 个人博客|860|Python|2021/11/20|
|181|[liuhuanyong/TextGrapher](https://github.com/liuhuanyong/TextGrapher)|Text Content Grapher based on keyinfo extraction by NLP method。输入一篇文档，将文档进行关键信息提取，进行结构化，并最终组织成图谱组织形式，形成对文章语义信息的图谱化展示。|852|Python|2021/10/20|
|182|[ZSAIm/iqiyi-parser](https://github.com/ZSAIm/iqiyi-parser)|解析下载爱奇艺、哔哩哔哩、腾讯视频|849|Python|2021/09/08|
|183|[sngyai/Sequoia](https://github.com/sngyai/Sequoia)|A股自动选股程序，实现了海龟交易法则、缠中说禅牛市买点，以及其他若干种技术形态|842|Python|2021/11/08|
|184|[zhuifengshen/DingtalkChatbot](https://github.com/zhuifengshen/DingtalkChatbot)|钉钉群自定义机器人消息Python封装|840|Python|2021/11/19|
|185|[Hackxiaoya/CuteOne](https://github.com/Hackxiaoya/CuteOne)|这大概是最好的onedrive挂载程序了吧，我猜。|835|Python|2021/06/02|
|186|[datawhalechina/fun-rec](https://github.com/datawhalechina/fun-rec)|本推荐算法教程主要是针对具有机器学习基础并想找推荐算法岗位的同学，教程由推荐算法基础、推荐算法入门赛、新闻推荐项目及推荐算法面经组成，形成了一个完整的从基础到实战再到面试的闭环。|826|Python|2021/12/01|
|187|[dongrixinyu/JioNLP](https://github.com/dongrixinyu/JioNLP)|中文 NLP 预处理、解析工具包，准确、高效、易用 A Chinese NLP Preprocessing and Parsing Package|813|Python|2021/11/24|
|188|[425776024/nlpcda](https://github.com/425776024/nlpcda)|一键中文数据增强包 ； NLP数据增强、bert数据增强、EDA：pip install nlpcda|811|Python|2021/10/13|
|189|[tp4a/teleport](https://github.com/tp4a/teleport)|Teleport是一款简单易用的堡垒机系统。|810|Python|2021/11/26|
|190|[FutunnOpen/py-futu-api](https://github.com/FutunnOpen/py-futu-api)|富途 OpenAPI  Python SDK|804|Python|2021/11/08|
|191|[HaoZhang95/Python24](https://github.com/HaoZhang95/Python24)|网上搜集的自学python语言的资料集合,包括整套代码和讲义集合，这是至今为止所开放网上能够查找到的最新视频教程，网上找不到其他最新的python整套视频了,. 具体的无加密的mp4视频教程和讲义集合可以在更新的Readme文件中找到，下载直接打开就能播放，项目从零基础的Python教程到深度学习，总共30章节，其中包含Python基础中的飞机大战项目，WSGI项目，Flask新经资讯项目， Django的电商项目(本应该的美多商城项目因为使用的是Vue技术，所以替换为Django天天生鲜项目)等等，希望能够帮助大家。资源搜集劳神费力，能帮到你的话是我的福分,望大家多多支持,喜欢本仓库的话，记 ...|800|Python|2021/10/07|
|192|[fkxxyz/rime-cloverpinyin](https://github.com/fkxxyz/rime-cloverpinyin)|🍀️四叶草拼音输入方案，做最好用的基于rime开源的简体拼音输入方案！|787|Python|2021/06/30|
|193|[iswbm/GolangCodingTime](https://github.com/iswbm/GolangCodingTime)|Go编程时光，一个零基础入门 Golang 的教程|731|Python|2021/10/22|
|194|[minivision-ai/Silent-Face-Anti-Spoofing](https://github.com/minivision-ai/Silent-Face-Anti-Spoofing)|静默活体检测（Silent-Face-Anti-Spoofing）|729|Python|2021/09/08|
|195|[icopy-site/awesome-cn](https://github.com/icopy-site/awesome-cn)|超赞列表合集|728|Python|2021/12/01|
|196|[WenmuZhou/PytorchOCR](https://github.com/WenmuZhou/PytorchOCR)|基于Pytorch的OCR工具库，支持常用的文字检测和识别算法|726|Python|2021/10/10|
|197|[hhaAndroid/mmdetection-mini](https://github.com/hhaAndroid/mmdetection-mini)|mmdetection最小学习版|719|Python|2021/11/21|
|198|[fate0/getproxy](https://github.com/fate0/getproxy)|getproxy 是一个抓取发放代理网站，获取 http/https 代理的程序|713|Python|2021/09/16|
|199|[LingDong-/qiji-font](https://github.com/LingDong-/qiji-font)|齊伋體 - typeface from Ming Dynasty woodblock printed books|713|Python|2021/09/02|
|200|[yidao620c/core-algorithm](https://github.com/yidao620c/core-algorithm)|算法集锦的python实现|712|Python|2021/09/12|

⬆ [回到目录](#内容目录)

<br/>

## Go

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[unknwon/the-way-to-go_ZH_CN](https://github.com/unknwon/the-way-to-go_ZH_CN)|《The Way to Go》中文译本，中文正式名《Go 入门指南》|28.2k|Go|2021/10/25|
|2|[halfrost/LeetCode-Go](https://github.com/halfrost/LeetCode-Go)|✅ Solutions to LeetCode by Go, 100% test coverage, runtime beats 100% / LeetCode 题解|22.0k|Go|2021/12/01|
|3|[ehang-io/nps](https://github.com/ehang-io/nps)|一款轻量级、高性能、功能强大的内网穿透代理服务器。支持tcp、udp、socks5、http等几乎所有流量转发，可用来访问内网网站、本地支付接口调试、ssh访问、远程桌面，内网dns解析、内网socks5代理等等……，并带有功能强大的web管理端。a lightweight, high-performance, powerful intranet penetration proxy server, with a powerful web management terminal.|19.3k|Go|2021/11/09|
|4|[chai2010/advanced-go-programming-book](https://github.com/chai2010/advanced-go-programming-book)|:books: 《Go语言高级编程》开源图书，涵盖CGO、Go汇编语言、RPC实现、Protobuf插件实现、Web框架实现、分布式系统等高阶主题(完稿)|16.3k|Go|2021/11/25|
|5|[greyireland/algorithm-pattern](https://github.com/greyireland/algorithm-pattern)|算法模板，最科学的刷题方式，最快速的刷题路径，你值得拥有~|12.9k|Go|2021/05/14|
|6|[txthinking/brook](https://github.com/txthinking/brook)|Brook is a cross-platform strong encryption and not detectable proxy. Zero-Configuration. Brook 是一个跨平台的强加密无特征的代理软件. 零配置.|12.7k|Go|2021/11/21|
|7|[cloudreve/Cloudreve](https://github.com/cloudreve/Cloudreve)|🌩支持多家云存储的云盘系统 (Self-deployed file management and sharing system, supports multiple storage providers)|12.1k|Go|2021/11/30|
|8|[peterq/pan-light](https://github.com/peterq/pan-light)|百度网盘不限速客户端, golang + qt5, 跨平台图形界面|11.7k|Go|2021/10/06|
|9|[snail007/goproxy](https://github.com/snail007/goproxy)|🔥  Proxy is a high performance HTTP(S) proxies, SOCKS5 proxies,WEBSOCKET, TCP, UDP proxy server implemented by golang. Now, it supports chain-style proxies,nat forwarding in different lan,TCP/UDP port forwarding, SSH forwarding.Proxy是golang实现的高性能http,https,websocket,tcp,socks5代理服务器,支持内网穿透,链式代理,通讯加密, ...|11.2k|Go|2021/11/29|
|10|[flipped-aurora/gin-vue-admin](https://github.com/flipped-aurora/gin-vue-admin)|基于vite+vue3+gin搭建的开发基础平台，集成jwt鉴权，权限管理，动态路由，分页封装，多点登录拦截，资源权限，上传下载，代码生成器，表单生成器等开发必备功能，五分钟一套CURD前后端代码。|10.4k|Go|2021/12/01|
|11|[halfrost/Halfrost-Field](https://github.com/halfrost/Halfrost-Field)|✍🏻 这里是写博客的地方 —— Halfrost-Field 冰霜之地|10.3k|Go|2021/06/11|
|12|[talkgo/night](https://github.com/talkgo/night)|Weekly Go Online Meetup via Bilibili｜Go 夜读｜通过 bilibili 在线直播的方式分享 Go 相关的技术话题，每天大家在微信/telegram/Slack 上及时沟通交流编程技术话题。|10.0k|Go|2021/11/25|
|13|[geektutu/7days-golang](https://github.com/geektutu/7days-golang)|7 days golang programs from scratch (web framework Gee, distributed cache GeeCache, object relational mapping ORM framework GeeORM, rpc framework GeeRPC etc)  7天用Go动手写/从零实现系列|9.1k|Go|2021/11/26|
|14|[crawlab-team/crawlab](https://github.com/crawlab-team/crawlab)|Distributed web crawler admin platform for spiders management regardless of languages and frameworks. 分布式爬虫管理平台，支持任何语言和框架|8.3k|Go|2021/11/22|
|15|[polaris1119/The-Golang-Standard-Library-by-Example](https://github.com/polaris1119/The-Golang-Standard-Library-by-Example)|Golang标准库。对于程序员而言，标准库与语言本身同样重要，它好比一个百宝箱，能为各种常见的任务提供完美的解决方案。以示例驱动的方式讲解Golang的标准库。|8.2k|Go|2021/10/19|
|16|[unknwon/go-fundamental-programming](https://github.com/unknwon/go-fundamental-programming)|《Go 编程基础》是一套针对 Google 出品的 Go 语言的视频语音教程，主要面向新手级别的学习者。|8.0k|Go|2021/10/25|
|17|[panjf2000/ants](https://github.com/panjf2000/ants)|🐜🐜🐜 ants is a high-performance and low-cost goroutine pool in Go, inspired by fasthttp./ ants 是一个高性能且低损耗的 goroutine 池。|7.0k|Go|2021/11/30|
|18|[smallnest/rpcx](https://github.com/smallnest/rpcx)|Best microservices framework in Go, like alibaba Dubbo, but with more features, Scale easily. Try it. Test it. If you feel it's better, use it! 𝐉𝐚𝐯𝐚有𝐝𝐮𝐛𝐛𝐨, 𝐆𝐨𝐥𝐚𝐧𝐠有𝐫𝐩𝐜𝐱!|6.4k|Go|2021/11/25|
|19|[senghoo/golang-design-pattern](https://github.com/senghoo/golang-design-pattern)|设计模式 Golang实现－《研磨设计模式》读书笔记|5.8k|Go|2021/11/10|
|20|[panjf2000/gnet](https://github.com/panjf2000/gnet)|🚀 gnet is a high-performance, lightweight, non-blocking, event-driven networking framework written in pure Go./ gnet 是一个高性能、轻量级、非阻塞的事件驱动 Go 网络框架。|5.6k|Go|2021/12/01|
|21|[mindoc-org/mindoc](https://github.com/mindoc-org/mindoc)|Golang实现的基于beego框架的接口在线文档管理系统|5.3k|Go|2021/11/25|
|22|[go-admin-team/go-admin](https://github.com/go-admin-team/go-admin)|基于Gin + Vue + Element UI的前后端分离权限管理系统脚手架（包含了：多租户的支持，基础用户管理功能，jwt鉴权，代码生成器，RBAC资源控制，表单构建，定时任务等）3分钟构建自己的中后台项目；文档：https://doc.go-admin.dev   Demo： https://www.go-admin.dev Antd beta版本：https://preview.go-admin.dev|5.3k|Go|2021/10/22|
|23|[OpenIMSDK/Open-IM-Server](https://github.com/OpenIMSDK/Open-IM-Server)|OpenIM: Instant messaging open source project based on go built by former WeChat technology experts. Backend in Go.（由前微信技术专家打造的基于 Go 实现的即时通讯（IM）项目，从服务端到客户端SDK开源即时通讯（IM）整体解决方案，可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。）|5.3k|Go|2021/12/01|
|24|[EasyDarwin/EasyDarwin](https://github.com/EasyDarwin/EasyDarwin)|open source、high performance、industrial rtsp streaming server,a lot of optimization on streaming relay,KeyFrame cache,RESTful,and web management,also EasyDarwin support distributed load balancing,a simple streaming media cloud platform architecture.高性能开源RTSP流媒体服务器，基于go语言研发，维护和优化：RTSP推模式转发、RTSP拉模式转发、 ...|5.1k|Go|2021/10/26|
|25|[fanux/sealos](https://github.com/fanux/sealos)|一条命令离线安装高可用kubernetes，3min装完，700M，100年证书，生产环境稳如老狗|5.1k|Go|2021/11/09|
|26|[tophubs/TopList](https://github.com/tophubs/TopList)|今日热榜，一个获取各大热门网站热门头条的聚合网站，使用Go语言编写，多协程异步快速抓取信息，预览:https://mo.fish|4.3k|Go|2021/08/28|
|27|[chaosblade-io/chaosblade](https://github.com/chaosblade-io/chaosblade)|An easy to use and powerful chaos engineering experiment toolkit.（阿里巴巴开源的一款简单易用、功能强大的混沌实验注入工具）|4.3k|Go|2021/11/29|
|28|[Tencent/bk-cmdb](https://github.com/Tencent/bk-cmdb)|蓝鲸智云配置平台(BlueKing CMDB)|4.2k|Go|2021/11/30|
|29|[ouqiang/gocron](https://github.com/ouqiang/gocron)|定时任务管理系统|4.2k|Go|2021/09/01|
|30|[KubeOperator/KubeOperator](https://github.com/KubeOperator/KubeOperator)|KubeOperator 是一个开源的轻量级 Kubernetes 发行版，专注于帮助企业规划、部署和运营生产级别的 K8s 集群。|4.1k|Go|2021/11/30|
|31|[lanyulei/ferry](https://github.com/lanyulei/ferry)|本系统是集工单统计、任务钩子、权限管理、灵活配置流程与模版等等于一身的开源工单系统，当然也可以称之为工作流引擎。 致力于减少跨部门之间的沟通，自动任务的执行，提升工作效率与工作质量，减少不必要的工作量与人为出错率。|4.1k|Go|2021/11/17|
|32|[aceld/zinx](https://github.com/aceld/zinx)|基于Golang轻量级TCP并发服务器框架|4.1k|Go|2021/09/06|
|33|[huichen/wukong](https://github.com/huichen/wukong)|高度可定制的全文搜索引擎|4.1k|Go|2021/08/24|
|34|[SmartKeyerror/Psyduck](https://github.com/SmartKeyerror/Psyduck)|Record CS knowlegement with XMind, version 2.0. 使用 XMind 记录 Linux 操作系统，网络，C++，Golang 以及数据库的一些设计|4.0k|Go|2021/11/06|
|35|[p4gefau1t/trojan-go](https://github.com/p4gefau1t/trojan-go)|Go实现的Trojan代理，支持多路复用/路由功能/CDN中转/Shadowsocks混淆插件，多平台，无依赖。A Trojan proxy written in Go. An unidentifiable mechanism that helps you bypass GFW. https://p4gefau1t.github.io/trojan-go/|4.0k|Go|2021/10/15|
|36|[chai2010/go-ast-book](https://github.com/chai2010/go-ast-book)|:books: 《Go语法树入门——开启自制编程语言和编译器之旅》(开源免费图书/Go语言进阶/掌握抽象语法树/Go语言AST/凹语言)|3.9k|Go|2021/11/18|
|37|[yedf/dtm](https://github.com/yedf/dtm)|🔥A cross-language distributed transaction manager. Support xa, tcc, saga, transactional messages. 跨语言分布式事务管理器|3.8k|Go|2021/12/01|
|38|[moonD4rk/HackBrowserData](https://github.com/moonD4rk/HackBrowserData)|Decrypt passwords/cookies/history/bookmarks from the browser. 一款可全平台运行的浏览器数据导出解密工具。|3.8k|Go|2021/11/25|
|39|[gwuhaolin/lightsocks](https://github.com/gwuhaolin/lightsocks)|⚡️一个轻巧的网络混淆代理🌏|3.7k|Go|2021/05/18|
|40|[overnote/over-golang](https://github.com/overnote/over-golang)|Golang相关：[审稿进度80%]Go语法、Go并发思想、Go与web开发、Go微服务设施等|3.4k|Go|2021/10/28|
|41|[douyu/jupiter](https://github.com/douyu/jupiter)|Jupiter是斗鱼开源的面向服务治理的Golang微服务框架|3.4k|Go|2021/11/17|
|42|[golang-design/Go-Questions](https://github.com/golang-design/Go-Questions)|📖 从问题切入，串连  Go 语言相关的所有知识，融会贯通。 https://golang.design/go-questions|3.4k|Go|2021/11/17|
|43|[golang-design/under-the-hood](https://github.com/golang-design/under-the-hood)|📚 Go: Under The Hood   Go 语言原本   https://golang.design/under-the-hood|3.3k|Go|2021/11/19|
|44|[Mrs4s/go-cqhttp](https://github.com/Mrs4s/go-cqhttp)|cqhttp的golang实现，轻量、原生跨平台.|3.3k|Go|2021/11/29|
|45|[silenceper/wechat](https://github.com/silenceper/wechat)|WeChat SDK for Go （微信SDK：简单、易用）|3.1k|Go|2021/11/29|
|46|[qax-os/ElasticHD](https://github.com/qax-os/ElasticHD)|Elasticsearch 可视化DashBoard, 支持Es监控、实时搜索，Index template快捷替换修改，索引列表信息查看， SQL converts to DSL等 |3.0k|Go|2021/09/28|
|47|[sjqzhang/go-fastdfs](https://github.com/sjqzhang/go-fastdfs)|go-fastdfs 是一个简单的分布式文件系统(私有云存储)，具有无中心、高性能，高可靠，免维护等优点，支持断点续传，分块上传，小文件合并，自动同步，自动修复。Go-fastdfs is a simple distributed file system (private cloud storage), with no center, high performance, high reliability, maintenance free and other advantages, support breakpoint continuation, block upload, small fil ...|2.9k|Go|2021/11/06|
|48|[goodrain/rainbond](https://github.com/goodrain/rainbond)|Cloud-native and easy-to-use application management platform   云原生且易用的应用管理平台|2.9k|Go|2021/11/05|
|49|[XIU2/CloudflareSpeedTest](https://github.com/XIU2/CloudflareSpeedTest)|🌩「自选优选 IP / 过滤假墙」测试 Cloudflare CDN 延迟和速度，获取最快 IP (IPv4+IPv6)！|2.8k|Go|2021/11/29|
|50|[Jrohy/trojan](https://github.com/Jrohy/trojan)|trojan多用户管理部署程序, 支持web页面管理|2.8k|Go|2021/11/30|
|51|[eolinker/goku_lite](https://github.com/eolinker/goku_lite)|A Powerful HTTP API Gateway in pure golang！Goku API Gateway （中文名：悟空 API 网关）是一个基于 Golang开发的微服务网关，能够实现高性能 HTTP API 转发、服务编排、多租户管理、API 访问权限控制等目的，拥有强大的自定义插件系统可以自行扩展，并且提供友好的图形化配置界面，能够快速帮助企业进行 API 服务治理、提高 API 服务的稳定性和安全性。|2.8k|Go|2021/09/28|
|52|[gopl-zh/gopl-zh.github.com](https://github.com/gopl-zh/gopl-zh.github.com)|Go语言圣经中文版(只接收PR, Issue请提交到golang-china/gopl-zh)|2.7k|Go|2021/11/23|
|53|[shadow1ng/fscan](https://github.com/shadow1ng/fscan)|一款内网综合扫描工具，方便一键自动化、全方位漏扫扫描。|2.7k|Go|2021/11/25|
|54|[xinliangnote/go-gin-api](https://github.com/xinliangnote/go-gin-api)|基于 Gin 进行模块化设计的 API 框架，封装了常用功能，使用简单，致力于进行快速的业务研发。比如，支持 cors 跨域、jwt 签名验证、zap 日志收集、panic 异常捕获、trace 链路追踪、prometheus 监控指标、swagger 文档生成、viper 配置文件解析、gorm 数据库组件、gormgen 代码生成工具、graphql 查询语言、errno 统一定义错误码、gRPC 的使用、cron 定时任务 等等。|2.6k|Go|2021/11/28|
|55|[geektutu/high-performance-go](https://github.com/geektutu/high-performance-go)|high performance coding with golang（Go 语言高性能编程，Go 语言陷阱，Gotchas，Traps）|2.5k|Go|2021/06/05|
|56|[TruthHun/BookStack](https://github.com/TruthHun/BookStack)|BookStack，基于MinDoc，使用Beego开发的在线文档管理系统，功能类似Gitbook和看云。|2.5k|Go|2021/11/16|
|57|[wxbool/video-srt-windows](https://github.com/wxbool/video-srt-windows)|这是一个可以识别视频语音自动生成字幕SRT文件的开源 Windows-GUI 软件工具。|2.4k|Go|2021/10/09|
|58|[lifei6671/interview-go](https://github.com/lifei6671/interview-go)|golang面试题集合|2.4k|Go|2021/09/03|
|59|[eyebluecn/tank](https://github.com/eyebluecn/tank)|《蓝眼云盘》(Eyeblue Cloud Storage)|2.4k|Go|2021/11/26|
|60|[chanxuehong/wechat](https://github.com/chanxuehong/wechat)|weixin/wechat/微信公众平台/微信企业号/微信商户平台/微信支付 go/golang sdk |2.3k|Go|2021/10/09|
|61|[phachon/mm-wiki](https://github.com/phachon/mm-wiki)|MM-Wiki 一个轻量级的企业知识分享与团队协同软件，可用于快速构建企业 Wiki 和团队知识分享平台。部署方便，使用简单，帮助团队构建一个信息共享、文档管理的协作环境。|2.3k|Go|2021/11/02|
|62|[darjun/go-daily-lib](https://github.com/darjun/go-daily-lib)|Go 每日一库|2.3k|Go|2021/11/02|
|63|[studygolang/studygolang](https://github.com/studygolang/studygolang)|Go 语言中文网   Golang中文社区   Go语言学习园地 源码|2.2k|Go|2021/08/19|
|64|[liangdas/mqant](https://github.com/liangdas/mqant)|mqant是一款基于Golang语言的简洁,高效,高性能的分布式微服务框架|2.2k|Go|2021/10/17|
|65|[roseduan/rosedb](https://github.com/roseduan/rosedb)|🚀A fast, stable and embedded k-v storage in pure Golang, supports string, list, hash, set, sorted set. 一个 Go 语言实现的快速、稳定、内嵌的 k-v 存储引擎。|2.1k|Go|2021/11/28|
|66|[dreamans/syncd](https://github.com/dreamans/syncd)|syncd是一款开源的代码部署工具，它具有简单、高效、易用等特点，可以提高团队的工作效率.|2.1k|Go|2021/10/06|
|67|[alibaba/RedisShake](https://github.com/alibaba/RedisShake)|redis-shake is a tool for synchronizing data between two redis databases. Redis-shake 是一个用于在两个 redis之 间同步数据的工具，满足用户非常灵活的同步、迁移需求。|2.0k|Go|2021/10/25|
|68|[xinliangnote/Go](https://github.com/xinliangnote/Go)|【Go 从入门到实战】学习笔记，从零开始学 Go、Gin 框架，基本语法包括 26 个Demo，Gin 框架包括：Gin 自定义路由配置、Gin 使用 Logrus 进行日志记录、Gin 数据绑定和验证、Gin 自定义错误处理、Go gRPC Hello World... 持续更新中... |1.9k|Go|2021/11/13|
|69|[zu1k/proxypool](https://github.com/zu1k/proxypool)|自动抓取tg频道、订阅地址、公开互联网上的ss、ssr、vmess、trojan节点信息，聚合去重后提供节点列表。欢迎star|1.9k|Go|2021/12/01|
|70|[bilibili/overlord](https://github.com/bilibili/overlord)|Overlord是哔哩哔哩基于Go语言编写的memcache和redis&cluster的代理及集群管理功能，致力于提供自动化高可用的缓存服务解决方案。|1.9k|Go|2021/10/06|
|71|[8treenet/freedom](https://github.com/8treenet/freedom)|Freedom是一个基于六边形架构的框架，可以支撑充血的领域模型范式。|1.8k|Go|2021/11/29|
|72|[go-pay/gopay](https://github.com/go-pay/gopay)|微信、支付宝、PayPal 的Go版本SDK。【极简、易用的聚合支付SDK】|1.8k|Go|2021/11/23|
|73|[alberliu/gim](https://github.com/alberliu/gim)|golang写的IM服务器(服务组件形式)|1.7k|Go|2021/11/10|
|74|[yanyiwu/gojieba](https://github.com/yanyiwu/gojieba)|"结巴"中文分词的Golang版本|1.7k|Go|2021/06/05|
|75|[link1st/go-stress-testing](https://github.com/link1st/go-stress-testing)|go 实现的压测工具，ab、locust、Jmeter压测工具介绍【单台机器100w连接压测实战】|1.7k|Go|2021/09/28|
|76|[go-ego/gse](https://github.com/go-ego/gse)|Go efficient multilingual NLP and text segmentation; support english, chinese, japanese and other. Go 高性能多语言 NLP 和分词|1.7k|Go|2021/11/18|
|77|[naiba/nezha](https://github.com/naiba/nezha)|:trollface: 哪吒监控 一站式轻监控轻运维系统。支持系统状态、HTTP、TCP、Ping 监控报警，计划任务和在线终端。|1.7k|Go|2021/11/28|
|78|[mlogclub/bbs-go](https://github.com/mlogclub/bbs-go)|基于Golang的开源社区系统。|1.6k|Go|2021/11/22|
|79|[openscrm/api-server](https://github.com/openscrm/api-server)|OpenSCRM是一套基于Go和React的超高质量企业微信私域流量管理系统 。遵守Apache2.0协议，全网唯一免费商用。企业微信、私域流量、SCRM。|1.6k|Go|2021/11/12|
|80|[huichen/sego](https://github.com/huichen/sego)|Go中文分词|1.6k|Go|2021/08/24|
|81|[xiaobaiTech/golangFamily](https://github.com/xiaobaiTech/golangFamily)|【超全golang面试题合集+golang学习指南+golang知识图谱+入门成长路线】 一份涵盖大部分golang程序员所需要掌握的核心知识。常用第三方库(mysql,mq,es,redis等)+机器学习库+算法库+游戏库+开源框架+自然语言处理nlp库+网络库+视频库+微服务框架+视频教程+音频音乐库+图形图片库+物联网库+地理位置信息+嵌入式脚本库+编译器库+数据库+金融库+电子邮件库+电子书籍+分词+数据结构+设计模式+去html tag标签等+go学习+go面试+计算机网络基础+图解网络+操作系统面试题+数据库面试题+面试题合集|1.5k|Go|2021/10/17|
|82|[HDT3213/godis](https://github.com/HDT3213/godis)|A Golang implemented Redis Server and Cluster. Go 语言实现的 Redis 服务器和分布式集群|1.5k|Go|2021/11/18|
|83|[funny/link](https://github.com/funny/link)|Go语言网络层脚手架|1.5k|Go|2021/10/09|
|84|[lwch/natpass](https://github.com/lwch/natpass)|新一代主机管理工具|1.5k|Go|2021/11/23|
|85|[xormplus/xorm](https://github.com/xormplus/xorm)|xorm是一个简单而强大的Go语言ORM库，通过它可以使数据库操作非常简便。本库是基于原版xorm的定制增强版本，为xorm提供类似ibatis的配置文件及动态SQL支持，支持AcitveRecord操作|1.5k|Go|2021/08/22|
|86|[davyxu/tabtoy](https://github.com/davyxu/tabtoy)|高性能表格数据导出器|1.4k|Go|2021/11/26|
|87|[pibigstar/go-demo](https://github.com/pibigstar/go-demo)|Go语言实例教程从入门到进阶，包括基础库使用、设计模式、面试易错点、工具类、对接第三方等|1.4k|Go|2021/11/01|
|88|[hwholiday/learning_tools](https://github.com/hwholiday/learning_tools)|Go 学习、Go 进阶、Go 实用工具类、Go-DDD,Go-kit 、Go-Micro 、Go 推送、微服务实践|1.4k|Go|2021/11/30|
|89|[knownsec/ksubdomain](https://github.com/knownsec/ksubdomain)|无状态子域名爆破工具|1.3k|Go|2021/10/21|
|90|[didi/sharingan](https://github.com/didi/sharingan)|Sharingan（写轮眼）是一个基于golang的流量录制回放工具，适合项目重构、回归测试等。|1.3k|Go|2021/11/24|
|91|[link1st/gowebsocket](https://github.com/link1st/gowebsocket)|golang基于websocket单台机器支持百万连接分布式聊天(IM)系统|1.3k|Go|2021/09/28|
|92|[gobyexample-cn/gobyexample](https://github.com/gobyexample-cn/gobyexample)|Go by Example 通过例子学 Golang|1.3k|Go|2021/11/02|
|93|[zxh0/jvmgo-book](https://github.com/zxh0/jvmgo-book)|《自己动手写Java虚拟机》随书源代码|1.3k|Go|2021/07/31|
|94|[george518/PPGo_Job](https://github.com/george518/PPGo_Job)|PPGo_Job是一款可视化的、多人多权限的、一任务多机执行的定时任务管理系统，采用golang开发，安装方便，资源消耗少，支持大并发，可同时管理多台服务器上的定时任务。|1.3k|Go|2021/05/19|
|95|[inbug-team/InScan](https://github.com/inbug-team/InScan)|边界打点后的自动化渗透工具|1.2k|Go|2021/07/19|
|96|[xluohome/phonedata](https://github.com/xluohome/phonedata)|手机号码归属地信息库、手机号归属地查询   phone.dat 最后更新：2021年08月 |1.2k|Go|2021/08/31|
|97|[smartwalle/alipay](https://github.com/smartwalle/alipay)|支付宝 AliPay SDK for Go, 集成简单，功能完善，持续更新，支持公钥证书和普通公钥进行签名和验签。|1.2k|Go|2021/11/29|
|98|[alibaba/MongoShake](https://github.com/alibaba/MongoShake)|MongoShake is a universal data replication platform based on MongoDB's oplog. Redundant replication and active-active replication are two most important functions. 基于mongodb oplog的集群复制工具，可以满足迁移和同步的需求，进一步实现灾备和多活功能。|1.2k|Go|2021/11/26|
|99|[karldoenitz/Tigo](https://github.com/karldoenitz/Tigo)|Tigo is an HTTP web framework written in Go (Golang).It features a Tornado-like API with better performance.  Tigo是一款用Go语言开发的web应用框架，API特性类似于Tornado并且拥有比Tornado更好的性能。|1.2k|Go|2021/09/22|
|100|[smallnest/dive-to-gosync-workshop](https://github.com/smallnest/dive-to-gosync-workshop)|深入Go并发编程研讨课|1.2k|Go|2021/08/03|
|101|[Xhofe/alist](https://github.com/Xhofe/alist)|一个目录列表程序|1.2k|Go|2021/12/01|
|102|[gourouting/singo](https://github.com/gourouting/singo)|Gin+Gorm开发Golang API快速开发脚手架|1.2k|Go|2021/05/31|
|103|[txthinking/mr2](https://github.com/txthinking/mr2)|mr2 can help you expose local server to external network. Support both TCP/UDP, of course support HTTP.  Zero-Configuration. mr2 帮助你将本地端口暴露在外网.支持TCP/UDP, 当然也支持HTTP.|1.2k|Go|2021/07/16|
|104|[iwannay/jiacrontab](https://github.com/iwannay/jiacrontab)|简单可信赖的任务管理工具|1.1k|Go|2021/08/18|
|105|[go-kiss/sniper](https://github.com/go-kiss/sniper)|轻量级 go 业务框架。|1.1k|Go|2021/11/17|
|106|[Go-zh/tour](https://github.com/Go-zh/tour)|Go 语言官方教程中文版|1.1k|Go|2021/11/04|
|107|[saltbo/zpan](https://github.com/saltbo/zpan)|A self-hosted cloud disk base on the cloud storage./ 一个基于云存储的网盘系统，用于自建私人网盘或企业网盘。|1.1k|Go|2021/10/27|
|108|[hanchuanchuan/goInception](https://github.com/hanchuanchuan/goInception)|一个集审核、执行、备份及生成回滚语句于一身的MySQL运维工具|1.1k|Go|2021/11/21|
|109|[3xxx/engineercms](https://github.com/3xxx/engineercms)|工程师知识管理系统：基于golang go语言（beego框架）。每个行业都有自己的知识管理系统，engineercms旨在为土木工程师们打造一款适用的基于web的知识管理系统。它既可以用于管理个人的项目资料，也可以用于管理项目团队资料；它既可以运行于个人电脑，也可以放到服务器上。支持提取码分享文件，onlyoffice实时文档协作，直接在线编辑dwg文件、office文档，在线利用mindoc创作你的书籍，阅览PDF文件。通用的业务流程设置。手机端配套小程序，微信搜索“珠三角设代”或“青少儿书画”即可呼出小程序。|1.1k|Go|2021/11/09|
|110|[KubeOperator/KubePi](https://github.com/KubeOperator/KubePi)|KubePi 是一款简单易用的开源 Kubernetes 可视化管理面板|1.0k|Go|2021/11/30|
|111|[mozillazg/go-pinyin](https://github.com/mozillazg/go-pinyin)|汉字转拼音|1.0k|Go|2021/05/13|
|112|[taoshihan1991/go-fly](https://github.com/taoshihan1991/go-fly)|开源客服系统GO语言开发GO-FLY,免费客服系统/open source live customer chat by golang|1.0k|Go|2021/07/26|
|113|[sjlleo/netflix-verify](https://github.com/sjlleo/netflix-verify)|流媒体NetFlix解锁检测脚本 / A script used to determine whether your network can watch native Netflix movies or not|994|Go|2021/11/27|
|114|[go-spring/go-spring](https://github.com/go-spring/go-spring)|基于 IoC 的 Go 后端一站式开发框架 ( All-in-One Development Framework on IoC for Go ) 🚀 |992|Go|2021/11/30|
|115|[royalrick/weapp](https://github.com/royalrick/weapp)|微信小程序服务端 SDK (for Golang)|975|Go|2021/11/30|
|116|[gookit/color](https://github.com/gookit/color)|🎨 Terminal color rendering library, support 8/16 colors, 256 colors, RGB color rendering output, support Print/Sprintf methods, compatible with Windows. GO CLI 控制台颜色渲染工具库，支持16色，256色，RGB色彩渲染输出，使用类似于 Print/Sprintf，兼容并支持 Windows 环境的色彩渲染|975|Go|2021/11/13|
|117|[jeessy2/ddns-go](https://github.com/jeessy2/ddns-go)|简单好用的DDNS。自动更新域名解析到公网IP(支持阿里云、腾讯云dnspod、Cloudflare、华为云)|971|Go|2021/11/30|
|118|[indes/flowerss-bot](https://github.com/indes/flowerss-bot)|A telegram bot  for rss reader. 一个支持应用内阅读的 Telegram RSS Bot。|963|Go|2021/11/11|
|119|[master-coder-ll/v2ray-web-manager](https://github.com/master-coder-ll/v2ray-web-manager)|v2ray-web-manager 是一个v2ray的面板，也是一个集群的解决方案；同时增加了流量控制/账号管理/限速等功能。key: admin , panel ,web,cluster,集群,proxy|958|Go|2021/05/20|
|120|[bobohume/gonet](https://github.com/bobohume/gonet)|go分布式服务器，基于内存mmo|958|Go|2021/11/28|
|121|[marmotedu/iam](https://github.com/marmotedu/iam)|企业级的 Go 语言实战项目（可作为Go项目开发脚手架）|940|Go|2021/11/23|
|122|[brokercap/Bifrost](https://github.com/brokercap/Bifrost)|Bifrost ---- 面向生产环境的 MySQL 同步到Redis,MongoDB,ClickHouse,MySQL等服务的异构中间件|936|Go|2021/11/24|
|123|[tjfoc/gmsm](https://github.com/tjfoc/gmsm)|GM SM2/3/4 library based on Golang (基于Go语言的国密SM2/SM3/SM4算法库)|922|Go|2021/12/01|
|124|[EdgeSecurityTeam/EHole](https://github.com/EdgeSecurityTeam/EHole)|EHole(棱洞)3.0 重构版-红队重点攻击系统指纹探测工具|919|Go|2021/11/17|
|125|[qjfoidnh/BaiduPCS-Go](https://github.com/qjfoidnh/BaiduPCS-Go)|iikira/BaiduPCS-Go原版基础上集成了分享链接/秒传链接转存功能|908|Go|2021/10/06|
|126|[guonaihong/gout](https://github.com/guonaihong/gout)|gout to become the Swiss Army Knife of the http client @^^@--->  gout 是http client领域的瑞士军刀，小巧，强大，犀利。具体用法可看文档，如使用迷惑或者API用得不爽都可提issues|898|Go|2021/11/26|
|127|[go-eagle/eagle](https://github.com/go-eagle/eagle)|🦅一款小巧的基于Go构建的开发框架，可以快速构建API服务或者Web网站进行业务开发，遵循SOLID设计原则|895|Go|2021/11/30|
|128|[awake1t/linglong](https://github.com/awake1t/linglong)|一款甲方资产巡航扫描系统。系统定位是发现资产，进行端口爆破。帮助企业更快发现弱口令问题。主要功能包括: 资产探测、端口爆破、定时任务、管理后台识别、报表展示|862|Go|2021/09/30|
|129|[Janusec/janusec](https://github.com/Janusec/janusec)|Janusec Application Gateway, Provides Fast and Secure Application Delivery (Authentication, WAF/CC, HTTPS and ACME automatic certificates).  JANUSEC应用网关，提供快速、安全的应用交付（身份认证, WAF/CC, HTTPS以及ACME自动证书）。|852|Go|2021/11/14|
|130|[wangsongyan/wblog](https://github.com/wangsongyan/wblog)|基于gin+gorm开发的个人博客项目|826|Go|2021/11/28|
|131|[jweny/pocassist](https://github.com/jweny/pocassist)|全新的开源漏洞测试框架，实现poc在线编辑、运行、批量测试。使用文档：|826|Go|2021/10/29|
|132|[sevenelevenlee/go-patterns](https://github.com/sevenelevenlee/go-patterns)|Golang 设计模式|826|Go|2021/07/22|
|133|[caixw/apidoc](https://github.com/caixw/apidoc)|RESTful API 文档生成工具，支持 Go、Java、Swift、JavaScript、Rust、PHP、Python、Typescript、Kotlin 和 Ruby 等大部分语言。|805|Go|2021/11/25|
|134|[zu1k/nali](https://github.com/zu1k/nali)|An offline tool for querying IP geographic information and CDN provider.一个查询IP地理信息和CDN服务提供商的离线终端工具.|779|Go|2021/11/24|
|135|[gopcp/example.v2](https://github.com/gopcp/example.v2)|An example project for book 'Go Programming & Concurrency in Practice, 2nd edition' (《Go并发编程实战》第2版).|775|Go|2021/11/09|
|136|[EndlessCheng/mahjong-helper](https://github.com/EndlessCheng/mahjong-helper)|日本麻将助手：牌效+防守+记牌（支持雀魂、天凤）|771|Go|2021/10/30|
|137|[ixre/go2o](https://github.com/ixre/go2o)|基于DDD的o2o的业务模型及基础, 使用Golang+gRPC/Thrift实现|762|Go|2021/12/01|
|138|[yunionio/cloudpods](https://github.com/yunionio/cloudpods)|A cloud-native open-source unified multi-cloud and hybrid-cloud platform. 开源、云原生的多云管理及混合云融合平台|757|Go|2021/12/01|
|139|[labulaka521/crocodile](https://github.com/labulaka521/crocodile)|Distributed Task Scheduling System 分布式定时任务调度平台|751|Go|2021/10/06|
|140|[hr3lxphr6j/bililive-go](https://github.com/hr3lxphr6j/bililive-go)|一个直播录制工具|745|Go|2021/10/07|
|141|[baiyutang/meetup](https://github.com/baiyutang/meetup)|【❤️ 互联网最全大厂技术分享PPT 👍🏻 持续更新中！】🍻各大技术交流会、活动资料汇总 ，如 👉QCon👉全球运维技术大会 👉 GDG 👉 全球技术领导力峰会👉大前端大会👉架构师峰会👉敏捷开发DevOps👉OpenResty👉Elastic，欢迎  PR  / Issues|738|Go|2021/10/26|
|142|[cnbattle/douyin](https://github.com/cnbattle/douyin)|抖音推荐/搜索页视频列表视频爬虫方案,基于app(虚拟机或真机) 相关技术 golang adb|726|Go|2021/12/01|
|143|[opentrx/seata-golang](https://github.com/opentrx/seata-golang)|A Distributed Transaction Framework, like SEATA, support TCC mode and AT mode. DingTalk: seata-golang 社区|722|Go|2021/11/09|
|144|[k8gege/LadonGo](https://github.com/k8gege/LadonGo)|Ladon Pentest Scanner framework  全平台Go开源内网渗透扫描器框架,Windows/Linux/Mac内网渗透，使用它可轻松一键批量探测C段、B段、A段存活主机、高危漏洞检测MS17010、SmbGhost，远程执行SSH/Winrm，密码爆破SMB/SSH/FTP/Mysql/Mssql/Oracle/Winrm/HttpBasic/Redis，端口扫描服务识别PortScan指纹识别/HttpBanner/HttpTitle/TcpBanner/Weblogic/Oxid多网卡主机，端口扫描服务识别PortScan。|710|Go|2021/07/27|
|145|[seccome/Ehoney](https://github.com/seccome/Ehoney)|安全、快捷、高交互、企业级的蜜罐管理系统，支持多种协议蜜罐、蜜签、诱饵等功能。A safe, fast, highly interactive and enterprise level honeypot management system, supports multiple protocol honeypots, honeytokens, baits and other functions.|708|Go|2021/11/27|
|146|[b1gcat/DarkEye](https://github.com/b1gcat/DarkEye)|渗透测试情报收集工具|699|Go|2021/10/14|
|147|[itcloudy/ERP](https://github.com/itcloudy/ERP)|基于beego的进销存系统|697|Go|2021/08/11|
|148|[tiger1103/gfast](https://github.com/tiger1103/gfast)|基于GF(Go Frame)的后台管理系统|695|Go|2021/11/02|
|149|[xiecat/goblin](https://github.com/xiecat/goblin)|一款适用于红蓝对抗中的仿真钓鱼系统|670|Go|2021/11/01|
|150|[XrayR-project/XrayR](https://github.com/XrayR-project/XrayR)|A Xray backend framework that can easily support many panels. 一个基于Xray的后端框架，支持V2ay,Trojan,Shadowsocks协议，极易扩展，支持多面板对接|664|Go|2021/11/21|
|151|[inherd/coca](https://github.com/inherd/coca)|Coca is a toolbox which is design for legacy system refactoring and analysis, includes call graph, concept analysis, api tree, design patterns suggest. Coca 是一个用于系统重构、系统迁移和系统分析的工具箱。它可以分析代码中的测试坏味道、模块化分析、行数统计、分析调用与依赖、Git 分析以及自动化重构等。|662|Go|2021/07/09|
|152|[Mrs4s/MiraiGo](https://github.com/Mrs4s/MiraiGo)|qq协议的golang实现, 移植于mirai|647|Go|2021/11/27|
|153|[DOUBLE-Baller/momo](https://github.com/DOUBLE-Baller/momo)|php直播go直播,短视频,直播带货,仿比心,猎游,tt语音聊天,美女约玩,陪玩系统源码开黑,约玩源码.|640|Go|2021/10/22|
|154|[zxh0/luago-book](https://github.com/zxh0/luago-book)|《自己动手实现Lua》随书源代码|628|Go|2021/06/17|
|155|[lcvvvv/kscan](https://github.com/lcvvvv/kscan)|Kscan是一款纯go开发的轻量级的资产发现工具，可针对指定IP段、资产清单、存活网段自动化进行端口扫描以及TCP指纹识别和Banner抓取，在不发送更多的数据包的情况下尽可能的获取端口更多信息。并且能够针对扫描结果进行自动化暴力破解，且是go平台首款开源的RDP暴力破解工具。|621|Go|2021/11/27|
|156|[childe/gohangout](https://github.com/childe/gohangout)|golang版本的hangout, 希望能省些内存. 使用了自己写的Kafka lib .. 虚. 不过我们在生产环境已经使用近1年, kafka 版本从0.9.0.1到2.0都在使用, 目前情况稳定. 吞吐量在每天2000亿条以上.|615|Go|2021/11/23|
|157|[yonyoucloud/install_k8s](https://github.com/yonyoucloud/install_k8s)|一键安装kubernets(k8s)系统，采用RBAC模式运行（证书安全认证模式），既可以单台安装、也可以集群安装，并且完全是生产环境的安装标准。有疑问大家可以加我微信沟通：bsh888|614|Go|2021/10/28|
|158|[33cn/chain33](https://github.com/33cn/chain33)|高度模块化, 遵循 KISS原则的区块链开发框架|605|Go|2021/12/01|
|159|[admpub/nging](https://github.com/admpub/nging)|漂亮的Go语言通用后台管理系统，包含：计划任务 / MySQL管理 / Redis管理 / FTP管理 / SSH管理 / 服务器管理 / Caddy配置 / DDNS / FRP可视化配置 / 云存储管理 等功能。可运行于 Windows / Linux / MacOS 等主流系统平台和 树莓派 / 路由器 等ARM设备|603|Go|2021/12/01|
|160|[Golangltd/LollipopGo](https://github.com/Golangltd/LollipopGo)|稳定分支2.9.X 版本已更新，由【Golang语言游戏服务器】维护，全球服游戏服务器及区域服框架,目前协议支持websocket、http、KCP、TCP及RPC，采用状态同步(帧同步内测)，愿景：打造MMO多人竞技游戏框架！ 功能持续更新中... ...|589|Go|2021/10/15|
|161|[kplcloud/kplcloud](https://github.com/kplcloud/kplcloud)|基于Kubernetes的PaaS平台|587|Go|2021/11/25|
|162|[hidu/mysql-schema-sync](https://github.com/hidu/mysql-schema-sync)|mysql表结构自动同步工具(目前只支持字段、索引的同步，分区等高级功能暂不支持)|584|Go|2021/09/03|
|163|[awake1t/PortBrute](https://github.com/awake1t/PortBrute)|一款跨平台小巧的端口爆破工具，支持爆破FTP/SSH/SMB/MSSQL/MYSQL/POSTGRESQL/MONGOD / A cross-platform compact port blasting tool that supports blasting FTP/SSH/SMB/MSSQL/MYSQL/POSTGRESQL/MONGOD|580|Go|2021/10/09|
|164|[axiaoxin-com/investool](https://github.com/axiaoxin-com/investool)|Golang实现财报分析、股票检测、基本面选股、基金检测、基金筛选、4433法则、基金持仓相似度、股票选基|570|Go|2021/12/01|
|165|[libsgh/PanIndex](https://github.com/libsgh/PanIndex)|网盘目录列表，目前支持天翼云、teambition盘、阿里云盘、OneDrive等|570|Go|2021/11/16|
|166|[heiyeluren/koala](https://github.com/heiyeluren/koala)|koala通用频率控制系统，一个应对所有频次控制高度配置化的通用反作弊系统（规则引擎），高性能可扩展。 (the require golang >= 1.0)|557|Go|2021/07/13|
|167|[jiajunhuang/blog](https://github.com/jiajunhuang/blog)|Jiajun的编程随想|554|Go|2021/11/26|
|168|[vidar-team/Cardinal](https://github.com/vidar-team/Cardinal)|CTF🚩 AWD (Attack with Defense) 线下赛平台 / AWD platform - 欢迎 Star~ ✨|537|Go|2021/11/13|
|169|[zhzyker/dismap](https://github.com/zhzyker/dismap)|Asset discovery and identification tools 快速识别 Web 指纹信息，定位资产类型。辅助红队快速定位目标资产信息，辅助蓝队发现疑似脆弱点|527|Go|2021/10/15|
|170|[easychen/wecomchan](https://github.com/easychen/wecomchan)|通过企业微信向微信推送消息的配置文档、直推函数和可自行搭建的在线服务代码。可以看成Server酱的开源替代方案之一。|517|Go|2021/09/12|
|171|[baidu/EasyFaaS](https://github.com/baidu/EasyFaaS)|EasyFaaS是一个依赖轻、适配性强、资源占用少、无状态且高性能的函数计算服务引擎|516|Go|2021/10/18|
|172|[Tencent/bk-bcs](https://github.com/Tencent/bk-bcs)|蓝鲸智云容器管理平台(BlueKing Container Service)|514|Go|2021/12/01|
|173|[mohuishou/go-design-pattern](https://github.com/mohuishou/go-design-pattern)|golang design pattern go 设计模式实现，包含 23 种常见的设计模式实现，同时这也是极客时间-设计模式之美 的笔记|508|Go|2021/06/04|
|174|[didi/falcon-log-agent](https://github.com/didi/falcon-log-agent)|用于监控系统的日志采集agent，可无缝对接open-falcon|505|Go|2021/05/13|
|175|[zxysilent/blog](https://github.com/zxysilent/blog)|一个go、echo、xorm、vue 开发的快速、简洁、美观、前后端分离的个人博客系统(blog)、也可方便二次开发为CMS(内容管理系统)和各种企业门户网站。此仓库只提供单用户版本方便拿来即用，不考虑权限，正在积极开发,多用户版正在规划中...敬请期待|502|Go|2021/10/21|
|176|[chai2010/ugo-compiler-book](https://github.com/chai2010/ugo-compiler-book)|:books: µGo语言实现(如何从头开发一个迷你Go语言编译器)|502|Go|2021/12/01|
|177|[micro-plat/hydra](https://github.com/micro-plat/hydra)|后端一站式微服务框架，提供API、web、websocket，RPC、任务调度、消息消费服务器|500|Go|2021/11/22|
|178|[CTF-MissFeng/GoScan](https://github.com/CTF-MissFeng/GoScan)|GoScan是采用Golang语言编写的一款分布式综合资产管理系统，适合红队、SRC等使用|498|Go|2021/05/06|
|179|[gookit/validate](https://github.com/gookit/validate)|⚔ Go package for data validation and filtering. support Map, Struct, Form data. Go通用的数据验证与过滤库，使用简单，内置大部分常用验证、过滤器，支持自定义验证器、自定义消息、字段翻译。|493|Go|2021/11/13|
|180|[allanpk716/ChineseSubFinder](https://github.com/allanpk716/ChineseSubFinder)|自动化中文字幕下载。字幕网站支持 zimuku、subhd、shooter、xunlei 。支持 Emby、Jellyfin、Plex、Sonarr、Radarr、TMM|492|Go|2021/11/30|
|181|[88250/lute](https://github.com/88250/lute)|🎼 一款结构化的 Markdown 引擎，支持 Go 和 JavaScript。A structured Markdown engine that supports Go and JavaScript. |487|Go|2021/11/21|
|182|[meshplus/bitxhub](https://github.com/meshplus/bitxhub)|Interchain protocol 跨链协议|483|Go|2021/12/01|
|183|[yqchilde/Golang-Interview](https://github.com/yqchilde/Golang-Interview)|Golang面试题，收集自Golang中文网，收集是为了自己可以巩固复习，不必要每次去找|482|Go|2021/07/09|
|184|[Monibuca/engine](https://github.com/Monibuca/engine)|Monibuca 核心引擎，包含流媒体核心转发逻辑，需要配合功能插件一起组合运行|476|Go|2021/11/30|
|185|[sohaha/zlsgo](https://github.com/sohaha/zlsgo)|简单易用、足够轻量、性能好的 Golang 库 - Easy to use, light enough, good performance Golang library|468|Go|2021/11/11|
|186|[Mikubill/transfer](https://github.com/Mikubill/transfer)|🍭 集合多个API的大文件传输工具.|461|Go|2021/11/24|
|187|[mix-go/mix](https://github.com/mix-go/mix)|✨ Standard Toolkit for Go fast development / Go 快速开发标准工具包|453|Go|2021/11/30|
|188|[Echosong/beego_blog](https://github.com/Echosong/beego_blog)| beego+layui go入门开发 简洁美观的个人博客系统|453|Go|2021/10/24|
|189|[cdle/sillyGirl](https://github.com/cdle/sillyGirl)|傻妞机器人|442|Go|2021/11/30|
|190|[chenjiandongx/mandodb](https://github.com/chenjiandongx/mandodb)|🤔 A minimize Time Series Database, written from scratch as a learning project. 从零开始实现一个 TSDB|437|Go|2021/08/08|
|191|[gotomicro/ego](https://github.com/gotomicro/ego)|简单的 Go 微服务框架|432|Go|2021/11/28|
|192|[shenghui0779/yiigo](https://github.com/shenghui0779/yiigo)|🔥 一个好用的 Go 轻量级开发通用库 🚀🚀🚀|429|Go|2021/11/26|
|193|[gookit/goutil](https://github.com/gookit/goutil)|💪 Helper Utils For The Go: int, string, array/slice, map, dump, format, CLI, ENV, filesystem, test and more. Go 的一些工具函数，格式化，特殊处理，常用信息获取等等|428|Go|2021/11/27|
|194|[hteen/apple-store-helper](https://github.com/hteen/apple-store-helper)|Apple Store iPhone预约助手|419|Go|2021/11/06|
|195|[iissy/goweb](https://github.com/iissy/goweb)|Golang写的程序员网址导航|417|Go|2021/09/23|
|196|[ma6254/FictionDown](https://github.com/ma6254/FictionDown)|小说下载 小说爬取 起点 笔趣阁 导出Markdown 导出txt 转换epub 广告过滤 自动校对|416|Go|2021/08/11|
|197|[IrineSistiana/mosdns](https://github.com/IrineSistiana/mosdns)|一个 DNS 转发器。|414|Go|2021/11/18|
|198|[flipped-aurora/gf-vue-admin](https://github.com/flipped-aurora/gf-vue-admin)|基于goframe+vite+vue3搭建的开发基础平台，集成jwt鉴权，权限管理，动态路由，分页封装，多点登录拦截，资源权限，上传下载，代码生成器，表单生成器等开发必备功能，五分钟一套CURD前后端代码，欢迎issue和pr~|409|Go|2021/11/29|
|199|[nanmu42/orly](https://github.com/nanmu42/orly)|:football: Generate your own O'RLY animal book cover to troll your colleagues   生成你自己的O'RLY动物书封面，让你的同事惊掉下巴|405|Go|2021/09/24|
|200|[sunshinev/go-sword](https://github.com/sunshinev/go-sword)|【Go-sword】可视化CRUD管理后台生成工具|397|Go|2021/10/07|

⬆ [回到目录](#内容目录)

<br/>

## PHP

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[w7corp/easywechat](https://github.com/w7corp/easywechat)|:package: 一个 PHP 微信 SDK|9.7k|PHP|2021/11/29|
|2|[guyueyingmu/avbook](https://github.com/guyueyingmu/avbook)|AV 电影管理系统， avmoo , javbus , javlibrary 爬虫，线上 AV 影片图书馆，AV 磁力链接数据库，Japanese Adult Video Library,Adult Video Magnet Links - Japanese Adult Video Database|8.1k|PHP|2021/10/06|
|3|[top-think/think](https://github.com/top-think/think)|ThinkPHP Framework ——十年匠心的高性能PHP框架|7.7k|PHP|2021/07/22|
|4|[fecshop/yii2_fecshop](https://github.com/fecshop/yii2_fecshop)|yii2 ( PHP ) fecmall（fecshop） core code used for ecommerce shop 多语言多货币多入口的开源电商 B2C 商城，支持移动端vue, app, html5，微信小程序微店，微信小程序商城等|4.9k|PHP|2021/09/02|
|5|[dodgepudding/wechat-php-sdk](https://github.com/dodgepudding/wechat-php-sdk)|微信公众平台php开发包, weixin developer SDK.|4.5k|PHP|2021/10/21|
|6|[yansongda/pay](https://github.com/yansongda/pay)|可能是我用过的最优雅的 Alipay 和 WeChat 的支付 SDK 扩展包了|4.2k|PHP|2021/11/16|
|7|[overtrue/pinyin](https://github.com/overtrue/pinyin)|:cn: 基于词库的中文转拼音优质解决方案|3.8k|PHP|2021/12/01|
|8|[owner888/phpspider](https://github.com/owner888/phpspider)|《我用爬虫一天时间“偷了”知乎一百万用户，只为证明PHP是世界上最好的语言 》所使用的程序|3.4k|PHP|2021/07/02|
|9|[jqhph/dcat-admin](https://github.com/jqhph/dcat-admin)|🔥 基于 Laravel 的后台系统构建工具 (Laravel Admin)，使用很少的代码快速构建一个功能完善的高颜值后台系统，内置丰富的后台常用组件，开箱即用，让开发者告别冗杂的HTML代码|2.7k|PHP|2021/11/25|
|10|[Qsnh/meedu](https://github.com/Qsnh/meedu)|知识付费、企业线上培训解决方案。|2.7k|PHP|2021/11/23|
|11|[overtrue/laravel-wechat](https://github.com/overtrue/laravel-wechat)|微信 SDK for Laravel, 基于 overtrue/wechat|2.7k|PHP|2021/11/30|
|12|[overtrue/easy-sms](https://github.com/overtrue/easy-sms)|:calling: 一款满足你的多种发送需求的短信发送组件|2.6k|PHP|2021/12/01|
|13|[yuantuo666/baiduwp-php](https://github.com/yuantuo666/baiduwp-php)|PanDownload网页复刻版|2.4k|PHP|2021/11/15|
|14|[mashirozx/sakura](https://github.com/mashirozx/sakura)|A Wonderful WordPress Theme: 樱花庄的白猫博客主题|2.4k|PHP|2021/11/13|
|15|[jae-jae/QueryList](https://github.com/jae-jae/QueryList)|:spider: The progressive PHP crawler framework!  优雅的渐进式PHP采集框架。|2.4k|PHP|2021/08/08|
|16|[summerblue/laravel-shop](https://github.com/summerblue/laravel-shop)|Laravel 电商实战教程的项目代码|2.3k|PHP|2021/10/06|
|17|[helei112g/payment](https://github.com/helei112g/payment)|Payment是php版本的支付聚合第三方sdk，集成了微信支付、支付宝支付、招商一网通支付。提供统一的调用接口，方便快速接入各种支付、查询、退款、转账能力。服务端接入支付功能，方便、快捷。|2.3k|PHP|2021/06/27|
|18|[matyhtf/framework](https://github.com/matyhtf/framework)|SPF （Swoole PHP Framework），世界第一款基于Swoole扩展的PHP框架，开发者是Swoole创始人 |2.2k|PHP|2021/08/04|
|19|[assimon/dujiaoka](https://github.com/assimon/dujiaoka)|🦄独角数卡(自动售货系统)-开源式站长自动化售货解决方案、高效、稳定、快速！🚀🚀🎉🎉|2.2k|PHP|2021/10/28|
|20|[zoujingli/ThinkAdmin](https://github.com/zoujingli/ThinkAdmin)|基于 ThinkPHP 基础开发平台（登录账号密码都是 admin ）|1.9k|PHP|2021/12/01|
|21|[hui-ho/WebStack-Laravel](https://github.com/hui-ho/WebStack-Laravel)|一个开源的网址导航网站项目，您可以拿来制作自己的网址导航。|1.9k|PHP|2021/08/11|
|22|[Tai7sy/card-system](https://github.com/Tai7sy/card-system)|卡密商城系统，高效安全的在线卡密商城|1.9k|PHP|2021/10/12|
|23|[wususu/effective-resourses](https://github.com/wususu/effective-resourses)| :book:学习资源整合|1.8k|PHP|2021/06/10|
|24|[gongfuxiang/shopxo](https://github.com/gongfuxiang/shopxo)|ShopXO企业级免费开源商城系统，可视化DIY拖拽装修、包含PC、H5、多端小程序(微信+支付宝+百度+头条&抖音+QQ)、APP、多仓库、多商户，遵循MIT开源协议发布、基于ThinkPHP6框架研发|1.8k|PHP|2021/11/30|
|25|[mix-php/mix](https://github.com/mix-php/mix)|☄️  PHP CLI mode development framework, supports Swoole, WorkerMan, FPM, CLI-Server / PHP 命令行模式开发框架，支持 Swoole、WorkerMan、FPM、CLI-Server|1.7k|PHP|2021/11/27|
|26|[solstice23/argon-theme](https://github.com/solstice23/argon-theme)|📖 Argon - 一个轻盈、简洁的 WordPress 主题|1.7k|PHP|2021/11/30|
|27|[mylxsw/wizard](https://github.com/mylxsw/wizard)|Wizard是一款开源的文档管理工具，支持Markdown/Swagger/Table类型的文档。|1.7k|PHP|2021/09/01|
|28|[louislivi/SMProxy](https://github.com/louislivi/SMProxy)|Swoole MySQL Proxy 一个基于 MySQL 协议，Swoole 开发的MySQL数据库连接池。 A MySQL database connection pool based on MySQL protocol and Swoole.|1.7k|PHP|2021/11/18|
|29|[wisp-x/lsky-pro](https://github.com/wisp-x/lsky-pro)|☁️兰空图床(Lsky Pro) - Your photo album on the cloud.|1.6k|PHP|2021/10/31|
|30|[zhanghuanchong/icon-workshop](https://github.com/zhanghuanchong/icon-workshop)|图标工场 - 移动应用图标生成工具，一键生成所有尺寸的应用图标和启动图|1.6k|PHP|2021/11/06|
|31|[zorlan/skycaiji](https://github.com/zorlan/skycaiji)|蓝天采集器是一款免费的数据采集发布爬虫软件，采用php+mysql开发，可部署在云服务器，几乎能采集所有类型的网页，无缝对接各类CMS建站程序，免登录实时发布数据，全自动无需人工干预！是网页大数据采集软件中完全跨平台的云端爬虫系统|1.5k|PHP|2021/09/10|
|32|[hightman/xunsearch](https://github.com/hightman/xunsearch)|免费开源的中文搜索引擎，采用 C/C++ 编写 (基于 xapian 和 scws)，提供 PHP 的开发接口和丰富文档|1.5k|PHP|2021/05/22|
|33|[yanfeizhang/coder-kung-fu](https://github.com/yanfeizhang/coder-kung-fu)|开发内功修炼|1.5k|PHP|2021/11/19|
|34|[ZsgsDesign/NOJ](https://github.com/ZsgsDesign/NOJ)|⚡ The most advanced open-source automatic algorithm online judge system   南京邮电大学开源 Online Judge   QQ群：668108264|1.4k|PHP|2021/12/01|
|35|[mochat-cloud/mochat](https://github.com/mochat-cloud/mochat)|基于企业微信的开源SCRM应用开发框架&引擎，也是一套通用的企业私域流量管理系统!|1.3k|PHP|2021/11/23|
|36|[typecho-fans/plugins](https://github.com/typecho-fans/plugins)|Typecho Fans插件作品目录|1.3k|PHP|2021/10/29|
|37|[MoeNetwork/Tieba-Cloud-Sign](https://github.com/MoeNetwork/Tieba-Cloud-Sign)|百度贴吧云签到，在服务器上配置好就无需进行任何操作便可以实现贴吧的全自动签到。配合插件使用还可实现云灌水、点赞、封禁、删帖、审查等功能。注意：Gitee (原Git@osc) 仓库将不再维护，目前唯一指定的仓库为 Github。本项目没有官方交流群，如需交流可以直接使用Github的Discussions。没有商业版本，目前贴吧云签到由社区共同维护，不会停止更新（PR 通常在一天内处理）。|1.3k|PHP|2021/11/25|
|38|[SegmentFault/HyperDown](https://github.com/SegmentFault/HyperDown)|一个结构清晰的，易于维护的，现代的PHP Markdown解析器|1.2k|PHP|2021/08/19|
|39|[fukuball/jieba-php](https://github.com/fukuball/jieba-php)|"結巴"中文分詞：做最好的 PHP 中文分詞、中文斷詞組件。 / "Jieba" (Chinese for "to stutter") Chinese text segmentation: built to be the best PHP Chinese word segmentation module.|1.1k|PHP|2021/05/25|
|40|[magicblack/maccms10](https://github.com/magicblack/maccms10)|苹果cms官网,苹果cmsv10,maccmsv10,麦克cms,开源cms,内容管理系统,视频分享程序,分集剧情程序,网址导航程序,文章程序,漫画程序,图片程序|1.1k|PHP|2021/11/30|
|41|[zoujingli/WeChatDeveloper](https://github.com/zoujingli/WeChatDeveloper)|【新】微信服务号+微信小程序+微信支付+支付宝支付|1.1k|PHP|2021/11/11|
|42|[iqiqiya/iqiqiya-API](https://github.com/iqiqiya/iqiqiya-API)|API接口大全不断更新中~欢迎Fork和Star(✎ 1.一言(古诗句版)api  ✎ 2.必应每日一图api  ✎ 3.在线ip查询  ✎ 4.m3u8视频在线解析api  ✎ 5.随机生成二次元图片api  ✎ 6.快递查询api-支持国内百家快递  ✎ 7.flv视频在线解析api ✎ 8.抖音视频无水印解析api✎ 9.一句话随机图片api✎ 10.QQ用户信息获取api✎11.哔哩哔哩封面图获取api✎12.千图网58pic无水印解析下载api✎13.喜马拉雅主播FM数据采集api✎14.网易云音乐api✎15.CCTV央视网视频解析api✎16.微信运动刷步数api✎17.皮皮搞笑 ...|1.1k|PHP|2021/08/26|
|43|[dedemao/alipay](https://github.com/dedemao/alipay)|一个PHP文件搞定支付宝支付系列，包括电脑网站支付，手机网站支付，现金红包、消费红包、扫码支付，JSAPI支付、单笔转账到支付宝账户、交易结算（分账、分润）、网页授权获取用户信息等|1.0k|PHP|2021/08/15|
|44|[Leslin/thinkphp5-restfulapi](https://github.com/Leslin/thinkphp5-restfulapi)|restful-api风格接口 APP接口 APP接口权限  oauth2.0 接口版本管理 接口鉴权|980|PHP|2021/05/13|
|45|[ZainCheung/netease-cloud-api](https://github.com/ZainCheung/netease-cloud-api)|网易云音乐升级API|957|PHP|2021/07/03|
|46|[xiebruce/PicUploader](https://github.com/xiebruce/PicUploader)|一个还不错的图床工具，支持Mac/Win/Linux服务器、支持压缩后上传、添加图片或文字水印、多文件同时上传、同时上传到多个云、右击任意文件上传、快捷键上传剪贴板截图、Web版上传、支持作为Mweb/Typora发布图片接口、作为PicGo/ShareX/uPic等的自定义图床，支持在服务器上部署作为图床接口，支持上传任意格式文件。|942|PHP|2021/09/30|
|47|[dedemao/weixinPay](https://github.com/dedemao/weixinPay)|微信支付单文件版。一个PHP文件搞定微信支付系列。包括原生支付（扫码支付），H5支付，公众号支付，现金红包、企业付款到零钱等。新增V3版。|939|PHP|2021/05/13|
|48|[swlib/saber](https://github.com/swlib/saber)|⚔️ Saber, PHP异步协程HTTP客户端   PHP Coroutine HTTP client - Swoole Humanization Library|935|PHP|2021/10/15|
|49|[yansongda/laravel-pay](https://github.com/yansongda/laravel-pay)|可能是我用过的最优雅的 Alipay 和 WeChat 的 laravel 支付扩展包了|904|PHP|2021/09/27|
|50|[peinhu/AetherUpload-Laravel](https://github.com/peinhu/AetherUpload-Laravel)|A Laravel package to upload large files  上传大文件的Laravel扩展包|869|PHP|2021/08/05|
|51|[mirai-mamori/Sakurairo](https://github.com/mirai-mamori/Sakurairo)|一个多彩，轻松上手，体验完善，具有强大自定义功能的WordPress主题（基于Sakura主题）A Colorful, Easy-to-use, Perfect Experience, and Powerful Customizable WordPress Theme (Based on Theme Sakura)|841|PHP|2021/12/01|
|52|[imiphp/imi](https://github.com/imiphp/imi)|imi 是一款支持长连接微服务分布式的 PHP 开发框架，它可以运行在 PHP-FPM、Swoole、Workerman、RoadRunner 等多种容器环境下。它支持 HttpApi、WebSocket、TCP、UDP、MQTT 服务的开发。特别适合互联网微服务、即时通讯聊天im、物联网等场景！QQ群：17916227|832|PHP|2021/12/01|
|53|[owen0o0/WebStack](https://github.com/owen0o0/WebStack)|WordPress 版 WebStack 导航主题 https://nav.iowen.cn|794|PHP|2021/09/07|
|54|[zdhxiong/mdclub](https://github.com/zdhxiong/mdclub)|MDClub 社区系统后端代码|791|PHP|2021/09/25|
|55|[Yurunsoft/PaySDK](https://github.com/Yurunsoft/PaySDK)|PHP 集成支付 SDK ，集成了支付宝、微信支付的支付接口和其它相关接口的操作。支持 php-fpm 和 Swoole，所有框架通用。宇润PHP全家桶技术支持群：17916227|780|PHP|2021/10/14|
|56|[5iux/sou](https://github.com/5iux/sou)|简单搜索，一个简单的前端界面。用惯了各种导航首页，满屏幕尽是各种不厌其烦的广告和资讯；尝试自己写个自己的主页。|751|PHP|2021/08/02|
|57|[4x99/code6](https://github.com/4x99/code6)|码小六 - GitHub 代码泄露监控系统|720|PHP|2021/11/26|
|58|[susers/Writeups](https://github.com/susers/Writeups)|国内各大CTF赛题及writeup整理|713|PHP|2021/06/02|
|59|[jiangxianli/ProxyIpLib](https://github.com/jiangxianli/ProxyIpLib)|全球免费代理IP库，高可用IP，精心筛选优质IP,2s必达|681|PHP|2021/07/05|
|60|[xaboy/form-builder](https://github.com/xaboy/form-builder)|PHP表单生成器，快速生成现代化的form表单,支持前后端分离。内置复选框、单选框、输入框、下拉选择框,省市区三级联动,时间选择,日期选择,颜色选择,文件/图片上传等17种常用组件。|662|PHP|2021/05/07|
|61|[Beipy/BeipyVideoResolution](https://github.com/Beipy/BeipyVideoResolution)|北漂鱼解析开源视频播放框架，响应式布局！|660|PHP|2021/07/04|
|62|[ledccn/IYUUAutoReseed](https://github.com/ledccn/IYUUAutoReseed)|IYUU自动辅种工具，目前能对国内大部分的PT站点自动辅种，支持下载器集群，支持多盘位，支持多下载目录，支持远程连接等。|654|PHP|2021/09/05|
|63|[poetries/mywiki](https://github.com/poetries/mywiki)|:books:收集整理日常发现的好资源、前端资源汇总。关注公众号「前端进阶之旅」，一起学习|643|PHP|2021/10/03|
|64|[lkeme/BiliHelper-personal](https://github.com/lkeme/BiliHelper-personal)|哔哩哔哩（Bilibili）助手 - PHP 版（Personal）|611|PHP|2021/11/29|
|65|[zencodex/composer-mirror](https://github.com/zencodex/composer-mirror)|Composer 全量镜像发布于2017年3月，曾不间断运行2年多。这个开源有助于理解 Composer 镜像的工作原理|604|PHP|2021/07/05|
|66|[init-engineer/init.engineer](https://github.com/init-engineer/init.engineer)|這是一份純靠北工程師的專案，請好好愛護它，謝謝。|603|PHP|2021/11/22|
|67|[jxlwqq/id-validator](https://github.com/jxlwqq/id-validator)|中华人民共和国居民身份证、中华人民共和国港澳居民居住证以及中华人民共和国台湾居民居住证号码验证工具（PHP 版）|600|PHP|2021/11/24|
|68|[maccmspro/maccms10](https://github.com/maccmspro/maccms10)|苹果cms-v10,maccms-v10,麦克cms,开源cms,内容管理系统,视频分享程序,分集剧情程序,网址导航程序,文章程序,漫画程序,图片程序|598|PHP|2021/08/17|
|69|[zblogcn/zblogphp](https://github.com/zblogcn/zblogphp)|Z-BlogPHP博客程序|595|PHP|2021/11/26|
|70|[letwang/HookPHP](https://github.com/letwang/HookPHP)|HookPHP基于C扩展搭建内置AI编程的架构系统-支持微服务部署 热插拔业务组件-集成业务模型 权限模型 UI组件库 多模板 多平台 多域名 多终端 多语言-含常驻内存 前后分离 API平台 LUA QQ群：679116380|586|PHP|2021/11/26|
|71|[Licoy/wordpress-theme-puock](https://github.com/Licoy/wordpress-theme-puock)|:art: 一款基于WordPress开发的高颜值的自适应主题，支持白天与黑夜模式、无刷新加载等。|585|PHP|2021/11/28|
|72|[ZeroDream-CN/SakuraPanel](https://github.com/ZeroDream-CN/SakuraPanel)|樱花内网穿透网站源代码，2020 重制版|559|PHP|2021/06/15|
|73|[zdhxiong/Material-Design-Chinese](https://github.com/zdhxiong/Material-Design-Chinese)|Material Design 指南的中文翻译|551|PHP|2021/11/09|
|74|[bettershop/LaikeTui](https://github.com/bettershop/LaikeTui)|来客电商，微信小程序商城 + APP商城 + 公众号商城 + PC商城系统 + 支付宝小程序商城 + 抖音小程序商城 + 百度小程序电商系统（前后端代码全部开源） 注重界面美感与用户体验，打造独特电商系统生态圈|547|PHP|2021/12/01|
|75|[anerg2046/sns_auth](https://github.com/anerg2046/sns_auth)|通用第三方登录SDK，支持微信，微信扫码，QQ，微博登录，支付宝登录，Facebook，Line，Twitter，Google|545|PHP|2021/11/15|
|76|[nick-bai/snake](https://github.com/nick-bai/snake)|🚀thinkphp5.1 + layui 实现的带rbac的基础管理后台，方便快速开发法使用|539|PHP|2021/11/02|
|77|[DasSecurity-HatLab/AoiAWD](https://github.com/DasSecurity-HatLab/AoiAWD)|AoiAWD-专为比赛设计，便携性好，低权限运行的EDR系统。|528|PHP|2021/07/09|
|78|[litepress/wp-china-yes](https://github.com/litepress/wp-china-yes)|此插件将你的WordPress接入本土生态体系之中，使之更适合国内应用环境|521|PHP|2021/11/01|
|79|[jacobcyl/Aliyun-oss-storage](https://github.com/jacobcyl/Aliyun-oss-storage)|阿里云OSS laravel storage Filesystem adapter, 打造Laravel最好的OSS Storage扩展.|509|PHP|2021/08/17|
|80|[seth-shi/monday-shop](https://github.com/seth-shi/monday-shop)|网上在线商城、综合网上购物平台|508|PHP|2021/11/10|
|81|[bingcool/swoolefy](https://github.com/bingcool/swoolefy)|swoolefy是一个基于swoole实现的轻量级、高性能、协程级、开放性的API应用服务框架|503|PHP|2021/11/13|
|82|[jpush/jpush-api-php-client](https://github.com/jpush/jpush-api-php-client)|JPush's officially supported PHP client library for accessing JPush APIs.  极光推送官方支持的 PHP 版本服务器端 SDK。|503|PHP|2021/08/12|
|83|[ghboke/CorePressWPTheme](https://github.com/ghboke/CorePressWPTheme)|CorePress 主题，一款高性能，高颜值的WordPress主题|480|PHP|2021/11/30|
|84|[xiaochong0302/course-tencent-cloud](https://github.com/xiaochong0302/course-tencent-cloud)|网课系统，网校系统，知识付费系统，在线教育系统，基于高性能 C 扩展框架 Phalcon ，可免费商用。|479|PHP|2021/11/28|
|85|[Zhao-github/ApiAdmin](https://github.com/Zhao-github/ApiAdmin)|基于ThinkPHP V6.*开发的面向API的后台管理系统！|463|PHP|2021/10/29|
|86|[galnetwen/H-Siren](https://github.com/galnetwen/H-Siren)|WordPress单栏主题，支持全局PJAX，自适应手机端|461|PHP|2021/10/05|
|87|[flutterbest/easytbk](https://github.com/flutterbest/easytbk)|淘客5合一SDK，支持淘宝联盟、京东联盟、多多进宝、唯品会、苏宁|455|PHP|2021/07/13|
|88|[Yurunsoft/ChineseUtil](https://github.com/Yurunsoft/ChineseUtil)|PHP 中文工具包，支持汉字转拼音、拼音分词、简繁互转、数字、金额大写；QQ群：17916227|453|PHP|2021/10/14|
|89|[top-think/think-queue](https://github.com/top-think/think-queue)|ThinkPHP 队列支持|452|PHP|2021/07/27|
|90|[78778443/xssplatform](https://github.com/78778443/xssplatform)|一个经典的XSS渗透管理平台|452|PHP|2021/06/24|
|91|[zhongshaofa/easyadmin](https://github.com/zhongshaofa/easyadmin)|框架主要使用ThinkPHP6.0 + layui，拥有完善的权限的管理模块以及敏捷的开发方式，让你开发起来更加的舒服。|445|PHP|2021/11/05|
|92|[helloxz/onenav](https://github.com/helloxz/onenav)|使用PHP开发的简约导航/书签管理系统。|444|PHP|2021/09/10|
|93|[hiliqi/xiaohuanxiong](https://github.com/hiliqi/xiaohuanxiong)|开源有态度的漫画CMS|442|PHP|2021/10/25|
|94|[gyxuehu/EwoMail](https://github.com/gyxuehu/EwoMail)|EwoMail是基于Linux的企业邮箱服务器，集成了众多优秀稳定的组件，是一个快速部署、简单高效、多语言、安全稳定的邮件解决方案|437|PHP|2021/10/20|
|95|[smalls0098/video-parse-tools](https://github.com/smalls0098/video-parse-tools)|短视频的PHP拓展包，集成各大短视频的去水印功能、抖音、快手、微视主流短视频。PHP去水印|432|PHP|2021/11/25|
|96|[icret/EasyImages2.0](https://github.com/icret/EasyImages2.0)|新版简单强大的无数据库的图床2.0版  演示地址：|431|PHP|2021/11/28|
|97|[emlog/emlog](https://github.com/emlog/emlog)|emlog是一款基于PHP和MySQL的功能强大的博客及CMS建站系统，追求快速、稳定、简单、舒适的建站体验 （emlog is a fast, stable and easy-to-use blog and CMS website building system based on PHP and MySQL）。|430|PHP|2021/11/27|
|98|[moell-peng/mojito](https://github.com/moell-peng/mojito)|Mojito  Admin 基于 Laravel, Vue3, Element Plus构建的后台管理系统|428|PHP|2021/11/06|
|99|[iiYii/getyii](https://github.com/iiYii/getyii)|Yii2 community 请访问|426|PHP|2021/11/03|
|100|[baijunyao/thinkphp-bjyblog](https://github.com/baijunyao/thinkphp-bjyblog)|基于 ThinkPHP 开发的的 blog|418|PHP|2021/06/04|
|101|[nick-bai/laychat](https://github.com/nick-bai/laychat)|layIM+workerman+thinkphp5的webIM即时通讯系统 V2.0|411|PHP|2021/11/02|
|102|[weipxiu/Art_Blog](https://github.com/weipxiu/Art_Blog)|WordPress响应式免费主题，Art_Blog唯品秀博客（weipxiu.com/备用域名weipxiu.cn），开源给小伙伴免费使用，如使用过程有任何问题，在线技术支持QQ:343049466，欢迎打扰。原创不易，如喜欢，请多多打赏。演示：|401|PHP|2021/11/29|
|103|[cong5/myPersimmon](https://github.com/cong5/myPersimmon)|基于Laravel 5.4 的开发的博客系统，代号：myPersimmon|399|PHP|2021/05/12|
|104|[luolongfei/freenom](https://github.com/luolongfei/freenom)|Freenom 域名自动续期。Freenom domain name renews automatically.|389|PHP|2021/11/17|
|105|[OMGZui/noteBook](https://github.com/OMGZui/noteBook)|🍎  笔记本|386|PHP|2021/10/07|
|106|[likeyun/WeChat-Group-HuoMa](https://github.com/likeyun/WeChat-Group-HuoMa)|微信群二维码活码工具，生成微信群活码，随时可以切换二维码！|381|PHP|2021/05/22|
|107|[qmpaas/leadshop](https://github.com/qmpaas/leadshop)|🔥🔥🔥🔥🔥 Leadshop是一款提供持续更新迭代服务的免费开源商城系统，旨在打造极致的用户体验！开箱即可商用！ Leadshop主要面向中小型企业，助力搭建电商平台，并提供专业的技术支持。 商城涉及多种主题色、可视化DIY装修、促销转化、裂变分销、用户精细化管理、数据分析等多个维度，追求极致体验，全面赋能商家，为商家创造价值。 演示站：https://demo.leadshop.vip 账号:18888888888 密码:123456 前端技术栈 ES6、vue、vuex、vue-router、vue-cli、axios、element-ui、uni-app 后端技术栈 Yii2、Jwt、M ...|378|PHP|2021/11/23|
|108|[dspurl/dsshop](https://github.com/dspurl/dsshop)|vue2.0+Laravel7商城电商解决方案，完全前后端分离，免费开源可商用，H5商城电商平台，微信小程序商城电商平台；支持网站、PWA、H5、微信小程序，支付宝小程序、百度小程序、字节跳动小程序、安卓、IOS等等|372|PHP|2021/11/26|
|109|[oubingbing/wechatAlliance](https://github.com/oubingbing/wechatAlliance)|微信小程序--校园小情书后台源码，好玩的表白墙，告白墙。|371|PHP|2021/10/20|
|110|[storyflow/PHPer](https://github.com/storyflow/PHPer)|一个PHPer的升级之路|368|PHP|2021/11/01|
|111|[sqc157400661/XiaoTShop](https://github.com/sqc157400661/XiaoTShop)|laravel5.5搭建的后台管理 和 api服务 的小程序商城|363|PHP|2021/08/11|
|112|[largezhou/admin](https://github.com/largezhou/admin)|laravel + ant design vue 权限后台|363|PHP|2021/05/19|
|113|[JaguarJack/catch-admin](https://github.com/JaguarJack/catch-admin)|CatchAdmin是一款基于thinkphp6 和 element admin 开发的后台管理系统，基于 ServiceProvider，系统模块完全接耦。随时卸载安装模块。提供了完整的权限和数据权限等功能，大量内置的开发工具提升你的开发体验。官网地址：|357|PHP|2021/11/30|
|114|[Yurunsoft/YurunOAuthLogin](https://github.com/Yurunsoft/YurunOAuthLogin)|PHP 第三方登录授权 SDK，集成了QQ、微信、微博、Github等常用接口。支持 php-fpm 和 Swoole，所有框架通用。QQ群：17916227|357|PHP|2021/11/29|
|115|[zbfzn/douyin-clear-php](https://github.com/zbfzn/douyin-clear-php)|抖音去水印PHP版接口|356|PHP|2021/08/10|
|116|[YukiCoco/OLAINDEX-Magic](https://github.com/YukiCoco/OLAINDEX-Magic)|魔改版本，为 OLAINDEX 添加多网盘挂载及一些小修复|354|PHP|2021/10/06|
|117|[yupoxiong/BearAdmin](https://github.com/yupoxiong/BearAdmin)|基于ThinkPHP5+AdminLTE的后台管理系统|352|PHP|2021/08/31|
|118|[SmallRuralDog/laravel-vue-admin](https://github.com/SmallRuralDog/laravel-vue-admin)|开箱即用的Laravel后台扩展,前后端分离，后端控制前端组件，无需编写vue即可创建一个vue+vuex+vue-route+elment-ui+laravel的项目 ,丰富的表单 表格组件，强大的自定义组件功能。|349|PHP|2021/10/06|
|119|[KitePig/FatRat-Collect](https://github.com/KitePig/FatRat-Collect)|胖鼠采集 WordPress优秀开源采集插件|346|PHP|2021/10/12|
|120|[yitd/Any-Proxy](https://github.com/yitd/Any-Proxy)|Any-Proxy可以用PHP帮助你完美匿名反向代理浏览任何网站|344|PHP|2021/07/07|
|121|[FlxSNX/TeambitionShare](https://github.com/FlxSNX/TeambitionShare)|挂载Teambition文件 可直链分享 支持网盘(需申请)和项目文件(无需邀请码)|341|PHP|2021/06/16|
|122|[hyperf-admin/hyperf-admin](https://github.com/hyperf-admin/hyperf-admin)|hyperf-admin 是基于 hyperf + vue 的配置化后台开发工具|337|PHP|2021/11/18|
|123|[liufee/yii2-swoole](https://github.com/liufee/yii2-swoole)|yii2 swoole，让yii2运行在swoole上|332|PHP|2021/11/09|
|124|[Mickeyto/phpVideos](https://github.com/Mickeyto/phpVideos)|php 写的视频下载工具，现已支持：Youku、Miaopai、腾讯、XVideos、Pornhub、91porn、微博酷燃、bilibili、今日头条、芒果TV|326|PHP|2021/11/08|
|125|[gaoming13/wechat-php-sdk](https://github.com/gaoming13/wechat-php-sdk)|微信公众平台php版开发包|325|PHP|2021/06/08|
|126|[inhere/php-console](https://github.com/inhere/php-console)|🖥 PHP CLI application library, provide console argument parse, console controller/command run, color style, user interactive, format information show and more.  功能全面的PHP命令行应用库。提供控制台参数解析, 命令运行，颜色风格输出, 用户信息交互, 特殊格式信息显示|321|PHP|2021/11/30|
|127|[kasuganosoras/Pigeon](https://github.com/kasuganosoras/Pigeon)|💬 一个轻量化的留言板 / 记事本 / 社交系统 / 博客。人类的本质是……咕咕咕？|308|PHP|2021/10/25|
|128|[lokielse/omnipay-wechatpay](https://github.com/lokielse/omnipay-wechatpay)|（微信支付）WeChatPay driver for the Omnipay PHP payment processing library|308|PHP|2021/07/26|
|129|[klsf/kldns](https://github.com/klsf/kldns)|快乐二级域名分发系统|305|PHP|2021/10/06|
|130|[lizhipay/faka](https://github.com/lizhipay/faka)|个人发卡源码，发卡系统，二次元发卡系统，二次元发卡源码，发卡程序，动漫发卡，PHP发卡源码|301|PHP|2021/11/27|
|131|[txperl/Story-for-Typecho](https://github.com/txperl/Story-for-Typecho)|Typecho Theme Story - 爱上你我的故事|298|PHP|2021/09/27|
|132|[uuk020/logistics](https://github.com/uuk020/logistics)|PHP 多接口获取快递物流信息包|297|PHP|2021/06/12|
|133|[fooleap/disqus-php-api](https://github.com/fooleap/disqus-php-api)|利用 PHP cURL 转发 Disqus API 请求|297|PHP|2021/05/11|
|134|[baigoStudio/baigoSSO](https://github.com/baigoStudio/baigoSSO)|单点登录系统|296|PHP|2021/10/15|
|135|[eddy8/LightCMS](https://github.com/eddy8/LightCMS)|LightCMS 是一个基于 Laravel 开发的轻量级 CMS 系统，也可以作为一个通用的后台管理框架使用。A lightweight cms/admin framework powered by Laravel.|296|PHP|2021/11/25|
|136|[422926799/note](https://github.com/422926799/note)|记录自己写的工具和学习笔记|281|PHP|2021/11/29|
|137|[FeMiner/wms](https://github.com/FeMiner/wms)|企业仓库管理系统|281|PHP|2021/11/26|
|138|[yii2-chinesization/yii2-zh-cn](https://github.com/yii2-chinesization/yii2-zh-cn)|Yii2 官方文档及其他文本的中文本土化项目，授权维护分支：|280|PHP|2021/10/26|
|139|[DOUBLE-Baller/WebRTC_IM](https://github.com/DOUBLE-Baller/WebRTC_IM)|纯 go   php 实现的分布式IM即时通讯系统，各层可单独部署，音视频webrtc独立部署|271|PHP|2021/08/23|
|140|[we7coreteam/w7-rangine-empty](https://github.com/we7coreteam/w7-rangine-empty)|软擎是基于 Php 7.2+ 和 Swoole 4.4+ 的高性能、简单易用的开发框架。支持同时在 Swoole Server 和 php-fpm 两种模式下运行。内置了 Http (Swoole, Fpm)，Tcp，WebSocket，Process，Crontab服务。集成了大量成熟的组件，可以用于构建高性能的Web系统、API、中间件、基础服务等等。|266|PHP|2021/11/05|
|141|[vincenth520/pinche_xcx_data](https://github.com/vincenth520/pinche_xcx_data)|同城拼车微信小程序后端代码|256|PHP|2021/05/22|
|142|[coffeehb/tools](https://github.com/coffeehb/tools)|一些实用的python脚本|253|PHP|2021/08/10|
|143|[xingwenge/canal-php](https://github.com/xingwenge/canal-php)|Alibaba mysql database binlog incremental subscription & consumer components Canal's php client[阿里巴巴mysql数据库binlog的增量订阅&消费组件 Canal 的 php 客户端 ] https://github.com/alibaba/canal|252|PHP|2021/10/12|
|144|[MercyCloudTeam/TomatoIDC](https://github.com/MercyCloudTeam/TomatoIDC)| TomatoIDC/HStack是一款以MIT协议开源销售系统，具备易于扩展的插件系统，模版系统，使用强大的Laravel框架进行驱动，能帮助你轻松的扩展业务。|249|PHP|2021/11/17|
|145|[yzmcms/yzmcms](https://github.com/yzmcms/yzmcms)|YzmCMS是一款基于YZMPHP开发的一套轻量级开源内容管理系统，YzmCMS简洁、安全、开源、实用，可运行在Linux、Windows、MacOSX、Solaris等各种平台上，专注为公司企业、个人站长快速建站提供解决方案。|236|PHP|2021/08/30|
|146|[5ime/API-Admin](https://github.com/5ime/API-Admin)|API管理系统 内置20+API接口|235|PHP|2021/10/22|
|147|[52admln/vue-questionnaire](https://github.com/52admln/vue-questionnaire)|使用 Vue + CI 开发的简易问卷调查系统，演示账户：admin / admin|230|PHP|2021/08/11|
|148|[absafe/phpshell](https://github.com/absafe/phpshell)|php大马 php一句话 webshell 渗透|230|PHP|2021/08/14|
|149|[hyperf-plus/admin](https://github.com/hyperf-plus/admin)|【全新架构】使用体检和laravel-admin类似，无需写前端vue代码即可实现漂亮的ElementUI框架页面，Auth组件和 laravel的auth 类似支持多用户认证功能，hyperf-admin 插件式快速开发框架|228|PHP|2021/10/26|
|150|[Clago/workflow](https://github.com/Clago/workflow)|基于laravel的工作流项目|227|PHP|2021/08/04|
|151|[Hanson/laravel-admin-wechat](https://github.com/Hanson/laravel-admin-wechat)|laravel admin 的微信扩展包，支持多公众号、多小程序、多微信支付，包含基础接口与后台|227|PHP|2021/08/29|
|152|[celaraze/chemex](https://github.com/celaraze/chemex)|☕ 咖啡壶是一个免费、开源、高效且漂亮的运维资产管理平台。软硬件资产管理、归属/使用者追溯、盘点以及可靠的服务器状态管理面板。基于优雅的Laravel框架和DcatAdmin开发。|227|PHP|2021/11/30|
|153|[jack15083/laravel-admin](https://github.com/jack15083/laravel-admin)|基于laravel + vue + element ui 的后台管理系统，自带权限管理系统|226|PHP|2021/09/27|
|154|[Yurunsoft/YurunHttp](https://github.com/Yurunsoft/YurunHttp)|YurunHttp 是开源的 PHP HTTP 客户端，支持链式操作，简单易用。完美支持Curl、Swoole 协程。QQ群：17916227|217|PHP|2021/11/25|
|155|[easyswoole-panel/easyswoole_panel](https://github.com/easyswoole-panel/easyswoole_panel)|easyswoole写的后台，权限管理，前后分离模板|209|PHP|2021/11/22|
|156|[ha-ni-cc/hyperf-watch](https://github.com/ha-ni-cc/hyperf-watch)|🚀 Hyperf Watch Hot Reload Scripts  😊 Make Coding More Happy  👉 监听文件变化自动重启Hyperf|205|PHP|2021/11/18|
|157|[txperl/airAnime](https://github.com/txperl/airAnime)|轻量化动漫聚合搜索工具|203|PHP|2021/06/18|
|158|[mokeyjay/Pixiv-daily-ranking-widget](https://github.com/mokeyjay/Pixiv-daily-ranking-widget)|会自动更新的P站每日榜小挂件，适合放在博客侧边栏等地方 Pixiv daily list widget that automatically updates, suitable for blog sidebar, etc.|201|PHP|2021/09/26|
|159|[littlebossERP/erp_opensource](https://github.com/littlebossERP/erp_opensource)|小老板erp开源|200|PHP|2021/11/21|
|160|[jqhph/laravel-wherehasin](https://github.com/jqhph/laravel-wherehasin)|Laravel wherehasin是一个可以提升Laravel ORM关联关系查询性能的扩展包，可以替代Laravel ORM中的whereHas以及whereHasMorphIn查询方法。|199|PHP|2021/07/30|
|161|[gogobody/onecircle](https://github.com/gogobody/onecircle)|一款typecho 社交圈子 主题|199|PHP|2021/10/28|
|162|[Jader/PcmToWav](https://github.com/Jader/PcmToWav)|:musical_note: PHP 实现 PCM 格式音波文件转 WAV 格式音频文件|198|PHP|2021/06/15|
|163|[openBI-kwc/openBI](https://github.com/openBI-kwc/openBI)|可视化数据分析工具|198|PHP|2021/08/23|
|164|[anruence/yii2-tech](https://github.com/anruence/yii2-tech)|Yii2 通用后台管理系统|197|PHP|2021/10/14|
|165|[bg5sbk/MiniCMS](https://github.com/bg5sbk/MiniCMS)|至简的个人网站内容管理系统|196|PHP|2021/07/20|
|166|[yybawang/laravel-ebank](https://github.com/yybawang/laravel-ebank)|:robot: 电商类站内虚拟积分与聚合支付解决方案|194|PHP|2021/11/24|
|167|[yunfeilangwu/Echo](https://github.com/yunfeilangwu/Echo)|一套基于layui框架的Typecho主题Echo|194|PHP|2021/11/28|
|168|[fuchaoqun/colaphp](https://github.com/fuchaoqun/colaphp)|一个简单好用的PHP框架，莫有文档，请先读代码。|189|PHP|2021/05/11|
|169|[yunluo/Git](https://github.com/yunluo/Git)|WordPress主题|188|PHP|2021/10/07|
|170|[FantasticLBP/Company-Website-Pro](https://github.com/FantasticLBP/Company-Website-Pro)|现代公司企业官网以及电子商务产品网站|187|PHP|2021/08/13|
|171|[Layne666/oneindex](https://github.com/Layne666/oneindex)|Onedrive Directory Index 默认世纪互联版本，自用|182|PHP|2021/09/11|
|172|[Abbotton/alipay-sdk-php](https://github.com/Abbotton/alipay-sdk-php)|🐜支付宝（蚂蚁金服）开放平台第三方 PHP SDK，基于官方最新版本。|182|PHP|2021/10/12|
|173|[kiang/pharmacies](https://github.com/kiang/pharmacies)|藥局口罩採購地圖|174|PHP|2021/10/24|
|174|[hongweipeng/GreenGrapes](https://github.com/hongweipeng/GreenGrapes)|typecho 响应式绿色科技感双栏主题|172|PHP|2021/08/25|
|175|[wechatpay-apiv3/wechatpay-guzzle-middleware](https://github.com/wechatpay-apiv3/wechatpay-guzzle-middleware)|微信支付 APIv3 Guzzle HTTP Client中间件（middleware）|172|PHP|2021/07/14|
|176|[cxp1539/laravel-core-learn](https://github.com/cxp1539/laravel-core-learn)|laravel核心知识学习|172|PHP|2021/11/17|
|177|[D-xuanmo/Nuxtjs-Wordpress](https://github.com/D-xuanmo/Nuxtjs-Wordpress)|🎉 Nuxt.js + Wordpress REST API 主题；支持企业微信通知功能，全站前后端分离，自适应，白日、黑夜两种主题自动切换|171|PHP|2021/11/30|
|178|[9IPHP/9IPHP](https://github.com/9IPHP/9IPHP)|响应式WordPress主题|168|PHP|2021/06/02|
|179|[typecho/plugins](https://github.com/typecho/plugins)|Typecho插件列表|168|PHP|2021/11/01|
|180|[vanry/laravel-scout-tntsearch](https://github.com/vanry/laravel-scout-tntsearch)|包含中文分词的 Laravel Scout TNTSearch 驱动，支持 scws, phpanalysis 和 jieba 分词。|167|PHP|2021/11/20|
|181|[yhf7952/mmPic](https://github.com/yhf7952/mmPic)|11万张随机妹子图一次看个够，可另存，可自动播放。营养快线在路上……|165|PHP|2021/05/13|
|182|[jiix/xiunobbs](https://github.com/jiix/xiunobbs)|Xiuno BBS 4.0 是一款轻论坛程序。 本版修复了php7.4、php8.0兼容问题；采用utf8mb4，支持emoji；，jQuery更新到 3.5.1；bootstrap更新到4.5.0。移除部分插件，更新默认主题。|163|PHP|2021/05/13|
|183|[sy-records/game-ddz](https://github.com/sy-records/game-ddz)|♦️ 使用Hyperf框架开发斗地主游戏|163|PHP|2021/07/20|
|184|[Macr0phag3/webshell-bypassed-human](https://github.com/Macr0phag3/webshell-bypassed-human)|过人 webshell 的生成工具|162|PHP|2021/11/05|
|185|[simoole/simoole](https://github.com/simoole/simoole)|基于swoole引擎的PHP框架，结构清晰，部署简单，使用方便。可以灵活应对HTTP/Websocket服务，另有定时器、异步任务等。|162|PHP|2021/11/03|
|186|[dyedd/lanstar](https://github.com/dyedd/lanstar)|一款三栏简约typecho主题|161|PHP|2021/11/08|
|187|[likeyun/liKeYun_Huoma](https://github.com/likeyun/liKeYun_Huoma)|这是一套开源、免费、可上线运营的活码系统，便于协助自己、他人进行微信私域流量资源获取，更大化地进行营销推广活动！降低运营成本，提高工作效率，获取更多资源。|158|PHP|2021/11/29|
|188|[overtrue/weather](https://github.com/overtrue/weather)|:rainbow: 基于高德开放平台接口的 PHP 天气信息组件。|157|PHP|2021/12/01|
|189|[pfinal/wechat](https://github.com/pfinal/wechat)|一个简单易用的微信公众平台SDK, 支持PHP5.3+到7.x版本, 方便与主流框架集成(ThinkPHP、Yii、CI、Laravel…), IDE提示支持良好, 有视频|156|PHP|2021/06/18|
|190|[ChinaBygones/PHP-DouyinRobot](https://github.com/ChinaBygones/PHP-DouyinRobot)|PHP抖音机器人 抖音自动找好看的小姐姐😍,自动点赞+关注|154|PHP|2021/11/23|
|191|[dnomd343/echoIP](https://github.com/dnomd343/echoIP)|显示客户端IP的详细信息|154|PHP|2021/10/25|
|192|[learnku/learnku](https://github.com/learnku/learnku)|learnku.net 网站使用 laravel5.5 重新架构|153|PHP|2021/10/06|
|193|[abelzhou/PHP-TrieTree](https://github.com/abelzhou/PHP-TrieTree)|Make a trie tree with php language.使用场景为中文 英文 敏感词过滤/关键词过滤字典树,前缀树，内链建设，搜索提示。|153|PHP|2021/06/09|
|194|[isecret/yuncun](https://github.com/isecret/yuncun)|🎵网易云乐评 · 一言 API - 随机获取网易云热评|152|PHP|2021/10/06|
|195|[le31ei/ctf_challenges](https://github.com/le31ei/ctf_challenges)|适用于一线安服的ctf培训题目，全docker环境一键启动|151|PHP|2021/05/18|
|196|[Seevil/fantasy](https://github.com/Seevil/fantasy)|一款极简Typecho 博客主题|151|PHP|2021/11/22|
|197|[bewhale/thinkphp_gui_tools](https://github.com/bewhale/thinkphp_gui_tools)|ThinkPHP 漏洞 综合利用工具, 图形化界面, 命令执行, 一键getshell, 批量检测, 日志遍历, session包含, 宝塔绕过|149|PHP|2021/08/30|
|198|[edenleung/think-admin](https://github.com/edenleung/think-admin)|ThinkPHP 6.0 与 Ant Design Pro Vue 基础前后分离权限系统|149|PHP|2021/06/29|
|199|[Fanli2012/lqycms](https://github.com/Fanli2012/lqycms)|基于laravel框架的企业级开源cms管理系统，开源php商城源码，B2C微商城系统，企业建站cms。|148|PHP|2021/08/27|
|200|[justmd5/pinduoduo-sdk](https://github.com/justmd5/pinduoduo-sdk)|拼多多API SDK【多多客｜多多进宝&拼多多开放平台】|148|PHP|2021/11/24|

⬆ [回到目录](#内容目录)

<br/>

## C++

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[huihut/interview](https://github.com/huihut/interview)|📚 C/C++ 技术面试基础知识总结，包括语言、程序库、数据结构、算法、系统、网络、链接装载库等知识及面试经验、招聘、内推等信息。This repository is a summary of the basic knowledge of recruiting job seekers and beginners in the direction of C/C++ technology, including language, program library, data structure, algorithm, system, network, link loading library, in ...|21.4k|C++|2021/10/06|
|2|[Light-City/CPlusPlusThings](https://github.com/Light-City/CPlusPlusThings)|C++那些事|16.9k|C++|2021/11/29|
|3|[vnpy/vnpy](https://github.com/vnpy/vnpy)|基于Python的开源量化交易平台开发框架|16.9k|C++|2021/11/28|
|4|[zhongyang219/TrafficMonitor](https://github.com/zhongyang219/TrafficMonitor)|这是一个用于显示当前网速、CPU及内存利用率的桌面悬浮窗软件，并支持任务栏显示，支持更换皮肤。|14.7k|C++|2021/11/13|
|5|[Qv2ray/Qv2ray](https://github.com/Qv2ray/Qv2ray)|:star: Linux / Windows / macOS 跨平台 V2Ray 客户端   支持 VMess / VLESS / SSR / Trojan / Trojan-Go / NaiveProxy / HTTP / HTTPS / SOCKS5   使用 C++ / Qt 开发   可拓展插件式设计 :star:|12.8k|C++|2021/08/17|
|6|[ChenYilong/iOSInterviewQuestions](https://github.com/ChenYilong/iOSInterviewQuestions)|iOS interview questions;iOS面试题集锦（附答案）--学习qq群或 Telegram 群交流 https://github.com/ChenYilong/iOSBlog/issues/21|8.9k|C++|2021/09/03|
|7|[DayBreak-u/chineseocr_lite](https://github.com/DayBreak-u/chineseocr_lite)|超轻量级中文ocr，支持竖排文字识别, 支持ncnn、mnn、tnn推理 ( dbnet(1.8M) + crnn(2.5M) + anglenet(378KB)) 总模型仅4.7M |8.2k|C++|2021/11/24|
|8|[Ewenwan/MVision](https://github.com/Ewenwan/MVision)|机器人视觉 移动机器人 VS-SLAM ORB-SLAM2 深度学习目标检测 yolov3 行为检测 opencv  PCL 机器学习 无人驾驶|6.1k|C++|2021/07/29|
|9|[PaddlePaddle/Paddle-Lite](https://github.com/PaddlePaddle/Paddle-Lite)|Multi-platform high performance  deep learning inference engine (『飞桨』多平台高性能深度学习预测引擎）|5.8k|C++|2021/12/01|
|10|[me115/design_patterns](https://github.com/me115/design_patterns)|图说设计模式|5.6k|C++|2021/08/10|
|11|[weolar/miniblink49](https://github.com/weolar/miniblink49)|a lighter, faster browser kernel of blink to integrate HTML UI in your app. 一个小巧、轻量的浏览器内核，用来取代wke和libcef|5.4k|C++|2021/09/23|
|12|[qinguoyi/TinyWebServer](https://github.com/qinguoyi/TinyWebServer)|:fire: Linux下C++轻量级Web服务器|4.6k|C++|2021/10/10|
|13|[szad670401/HyperLPR](https://github.com/szad670401/HyperLPR)|基于深度学习高性能中文车牌识别 High Performance Chinese License Plate Recognition Framework.|4.4k|C++|2021/10/13|
|14|[TonyChen56/WeChatRobot](https://github.com/TonyChen56/WeChatRobot)|PC版微信机器人 微信Api、微信api、微信发卡机器人、微信聊天机器人 python微信api 微信接口 微信数据库解密|4.1k|C++|2021/10/29|
|15|[MegEngine/MegEngine](https://github.com/MegEngine/MegEngine)|MegEngine 是一个快速、可拓展、易于使用且支持自动求导的深度学习框架|4.1k|C++|2021/11/29|
|16|[yedf/handy](https://github.com/yedf/handy)|🔥简洁易用的C++11网络库 / 支持单机千万并发连接 / a simple C++11 network server framework|3.8k|C++|2021/10/11|
|17|[forhappy/Cplusplus-Concurrency-In-Practice](https://github.com/forhappy/Cplusplus-Concurrency-In-Practice)|A Detailed Cplusplus Concurrency Tutorial 《C++ 并发编程指南》|3.8k|C++|2021/07/25|
|18|[applenob/Cpp_Primer_Practice](https://github.com/applenob/Cpp_Primer_Practice)|搞定C++:punch:。C++ Primer 中文版第5版学习仓库，包括笔记和课后练习答案。|3.2k|C++|2021/11/28|
|19|[wang-bin/QtAV](https://github.com/wang-bin/QtAV)|A cross-platform multimedia framework based on Qt and FFmpeg(https://github.com/wang-bin/avbuild). High performance. User & developer friendly. Supports Android, iOS, Windows store and desktops. 基于Qt和FFmpeg的跨平台高性能音视频播放框架|3.0k|C++|2021/11/12|
|20|[balloonwj/flamingo](https://github.com/balloonwj/flamingo)|flamingo 一款高性能轻量级开源即时通讯软件|2.8k|C++|2021/10/20|
|21|[baidu/lac](https://github.com/baidu/lac)|百度NLP：分词，词性标注，命名实体识别，词重要性|2.8k|C++|2021/05/25|
|22|[liuchuo/PAT](https://github.com/liuchuo/PAT)|🍭 浙江大学PAT题解(C/C++/Java/Python) - 努力成为萌萌的程序媛～|2.7k|C++|2021/10/08|
|23|[huaxz1986/cplusplus-_Implementation_Of_Introduction_to_Algorithms](https://github.com/huaxz1986/cplusplus-_Implementation_Of_Introduction_to_Algorithms)|《算法导论》第三版中算法的C++实现|2.5k|C++|2021/06/17|
|24|[ffffffff0x/1earn](https://github.com/ffffffff0x/1earn)|ffffffff0x 团队维护的安全知识框架,内容包括不仅限于 web安全、工控安全、取证、应急、蓝队设施部署、后渗透、Linux安全、各类靶机writup|2.3k|C++|2021/11/17|
|25|[liuyubobobo/Play-Leetcode](https://github.com/liuyubobobo/Play-Leetcode)|My Solutions to Leetcode problems. All solutions support C++ language, some support Java and Python. Multiple solutions will be given by most problems. Enjoy:) 我的Leetcode解答。所有的问题都支持C++语言，一部分问题支持Java语言。近乎所有问题都会提供多个算法解决。大家加油！：）|2.2k|C++|2021/11/30|
|26|[rime/weasel](https://github.com/rime/weasel)|【小狼毫】Rime for Windows|2.1k|C++|2021/11/08|
|27|[19PDP/Bilibili-plus](https://github.com/19PDP/Bilibili-plus)|课程视频、PPT和源代码：侯捷C++系列；台大郭彦甫MATLAB|1.8k|C++|2021/11/30|
|28|[Tencent/plato](https://github.com/Tencent/plato)|腾讯高性能分布式图计算框架Plato|1.7k|C++|2021/08/14|
|29|[CodePanda66/CSPostgraduate-408](https://github.com/CodePanda66/CSPostgraduate-408)|💯  CSPostgraduate 计算机考研 408 专业课资料及真题资源～✍🏻 更新中～欢迎Star！⭐️|1.6k|C++|2021/09/15|
|30|[callmePicacho/Data-Structres](https://github.com/callmePicacho/Data-Structres)|浙江大学《数据结构》上课笔记 + 数据结构实现 + 课后题题解|1.6k|C++|2021/08/17|
|31|[FISCO-BCOS/FISCO-BCOS](https://github.com/FISCO-BCOS/FISCO-BCOS)|A consortium  blockchain platform （联盟区块链底层技术平台）|1.5k|C++|2021/12/01|
|32|[githubhaohao/NDK_OpenGLES_3_0](https://github.com/githubhaohao/NDK_OpenGLES_3_0)|Android OpenGL ES 3.0 从入门到精通系统性学习教程|1.4k|C++|2021/11/17|
|33|[sylar-yin/sylar](https://github.com/sylar-yin/sylar)|C++高性能分布式服务器框架,webserver,websocket server,自定义tcp_server（包含日志模块，配置模块，线程模块，协程模块，协程调度模块，io协程调度模块，hook模块，socket模块，bytearray序列化，http模块，TcpServer模块，Websocket模块，Https模块等, Smtp邮件模块, MySQL, SQLite3, ORM,Redis,Zookeeper)|1.4k|C++|2021/11/03|
|34|[scarsty/kys-cpp](https://github.com/scarsty/kys-cpp)|《金庸群侠传》c++复刻版，已完工|1.3k|C++|2021/11/10|
|35|[zhongyang219/MusicPlayer2](https://github.com/zhongyang219/MusicPlayer2)|这是一款可以播放常见音频格式的音频播放器。支持歌词显示、歌词卡拉OK样式显示、歌词在线下载、歌词编辑、歌曲标签识别、Win10小娜搜索显示歌词、频谱分析、音效设置、任务栏缩略图按钮、主题颜色等功能。 播放内核为BASS音频库(V2.4)。|1.3k|C++|2021/11/29|
|36|[netease-im/NIM_Duilib_Framework](https://github.com/netease-im/NIM_Duilib_Framework)|网易云信Windows应用界面开发框架（基于Duilib）。招人招人，windows/mac/duilib/qt/electron http://mobile.bole.netease.com/bole/boleDetail?id=19904&employeeId=510064bce318835c&key=all&type=2&from=timeline|1.2k|C++|2021/07/30|
|37|[HarleysZhang/2021_algorithm_intern_information](https://github.com/HarleysZhang/2021_algorithm_intern_information)|2021年的算法实习岗位/校招公司信息表，和常见深度学习基础、计算机视觉知识笔记、算法岗面试题答案，及暑期计算机视觉实习面经和总结。|1.2k|C++|2021/10/23|
|38|[netwarm007/GameEngineFromScratch](https://github.com/netwarm007/GameEngineFromScratch)|配合我的知乎专栏写的项目|1.1k|C++|2021/11/29|
|39|[qdtroy/DuiLib_Ultimate](https://github.com/qdtroy/DuiLib_Ultimate)|duilib 旗舰版-高分屏、多语言、样式表、资源管理器、异形窗口、窗口阴影、简单动画|1.1k|C++|2021/11/29|
|40|[fasiondog/hikyuu](https://github.com/fasiondog/hikyuu)|Hikyuu Quant Framework 基于C++/Python的开源量化交易研究框架|1.1k|C++|2021/12/01|
|41|[SFUMECJF/cmake-examples-Chinese](https://github.com/SFUMECJF/cmake-examples-Chinese)|快速入门CMake,通过例程学习语法。在线阅读地址：https://sfumecjf.github.io/cmake-examples-Chinese/|1.1k|C++|2021/06/22|
|42|[0voice/cpp_new_features](https://github.com/0voice/cpp_new_features)|2021年最新整理， C++ 学习资料，含C++ 11 / 14 / 17 / 20 / 23 新特性、入门教程、推荐书籍、优质文章、学习笔记、教学视频等|1.1k|C++|2021/10/21|
|43|[tencentyun/TRTCSDK](https://github.com/tencentyun/TRTCSDK)|腾讯云TRTC音视频服务，国内下载镜像：|1.1k|C++|2021/11/26|
|44|[Tencent/GameAISDK](https://github.com/Tencent/GameAISDK)|基于图像的游戏AI自动化框架|1.1k|C++|2021/05/17|
|45|[ZLMediaKit/ZLToolKit](https://github.com/ZLMediaKit/ZLToolKit)|一个基于C++11的轻量级网络框架，基于线程池技术可以实现大并发网络IO|1.0k|C++|2021/11/29|
|46|[Greedysky/TTKMusicplayer](https://github.com/Greedysky/TTKMusicplayer)|TTKMusicPlayer that imitation Kugou music, the music player uses of qmmp core library based on Qt for windows and linux.(支持网易云音乐、QQ音乐、酷我音乐、酷狗音乐)|971|C++|2021/12/01|
|47|[Dr-Incognito/V2Ray-Desktop](https://github.com/Dr-Incognito/V2Ray-Desktop)|最优雅的跨平台代理客户端，支持Shadowsocks(R)，V2Ray和Trojan协议。The most elegant cross-platform proxy GUI client that supports Shadowsocks(R), V2Ray, and Trojan. Built with Qt5 and QML2.|947|C++|2021/10/31|
|48|[aiyaapp/AiyaEffectsAndroid](https://github.com/aiyaapp/AiyaEffectsAndroid)|宝宝特效Demo通过短视频SDK、直播SDK轻松实现特效与视频剪辑，为用户提供特效相机，拍摄辅助，自动美颜相机，抖音滤镜、直播礼物、直播贴纸等，超低占用空间，十秒大型场景仅100KB+， 精准人脸识别、人脸跟踪，支持3D特效，3D动画特效，2D特效、动画渲染、特效渲染等,  visual effects IOS demo, support 3D effect, 3D Animation, 2D effect|936|C++|2021/07/09|
|49|[xmuli/QtExamples](https://github.com/xmuli/QtExamples)|Qt 的 GUI 控件使用和网络；DTK 重绘控件方式的框架架构解析；Qt 原理/运行机制理解；QtCrator 使用和一些小技巧；系列文章教程|921|C++|2021/10/09|
|50|[nwpuhq/AwesomeCpp](https://github.com/nwpuhq/AwesomeCpp)|---AWESOME--- C++学习笔记和常见面试知识点，C++11特性，包括智能指针、四种强制转换、function和bind、移动语义、完美转发、tuple、多态原理、虚表、友元函数、符号重载、函数指针、深浅拷贝、struct内存对齐、volatile以及union\static等各种关键字的用法等等，还新添了其他算法和计算机基础的难点，力求简洁清晰|891|C++|2021/06/26|
|51|[Captain1986/CaptainBlackboard](https://github.com/Captain1986/CaptainBlackboard)|船长关于机器学习、计算机视觉和工程技术的总结和分享|854|C++|2021/06/15|
|52|[Tencent/flare](https://github.com/Tencent/flare)| Flare是广泛投产于腾讯广告后台的现代化C++开发框架，包含了基础库、RPC、各种客户端等。主要特点为易用性强、长尾延迟低。 |815|C++|2021/11/25|
|53|[ylmbtm/GameProject3](https://github.com/ylmbtm/GameProject3)|游戏服务器框架，网络层分别用SocketAPI、Boost Asio、Libuv三种方式实现， 框架内使用共享内存，无锁队列，对象池，内存池来提高服务器性能。还包含一个不断完善的Unity 3D客户端，客户端含大量完整资源，坐骑，宠物，伙伴，装备, 这些均己实现上阵和穿戴, 并可进入副本战斗，多人玩法也己实现, 持续开发中。|802|C++|2021/11/22|
|54|[didi/AoE](https://github.com/didi/AoE)|AoE (AI on Edge，终端智能，边缘计算) 是一个终端侧AI集成运行时环境 (IRE)，帮助开发者提升效率。|800|C++|2021/09/28|
|55|[MKXJun/DirectX11-With-Windows-SDK](https://github.com/MKXJun/DirectX11-With-Windows-SDK)|现代DX11系列教程：使用Windows SDK(C++)开发Direct3D 11.x|783|C++|2021/11/09|
|56|[limingfan2016/game_service_system](https://github.com/limingfan2016/game_service_system)|从0开始开发 基础库（配置文件读写、日志、多线程、多进程、锁、对象引用计数、内存池、免锁消息队列、免锁数据缓冲区、进程信号、共享内存、定时器等等基础功能组件），网络库（socket、TCP、UDP、epoll机制、连接自动收发消息等等），数据库操作库（mysql，redis、memcache API 封装可直接调用），开发框架库（消息调度处理、自动连接管理、服务开发、游戏框架、服务间消息收发、消息通信等等），消息中间件服务（不同网络节点间自动传递收发消息）等多个功能组件、服务，最后完成一套完整的服务器引擎，基于该框架引擎可开发任意的网络服务。  主体架构：N网关+N服务+N数据库代理+内存DB ...|765|C++|2021/11/14|
|57|[acm-clan/algorithm-stone](https://github.com/acm-clan/algorithm-stone)|ACM/LeetCode算法竞赛路线图，最全的算法学习地图！|752|C++|2021/11/18|
|58|[markparticle/WebServer](https://github.com/markparticle/WebServer)|C++  Linux WebServer服务器|737|C++|2021/10/11|
|59|[BlueMatthew/WechatExporter](https://github.com/BlueMatthew/WechatExporter)|Wechat Chat History Exporter 微信聊天记录导出程序|715|C++|2021/11/14|
|60|[downdemo/Cpp-Templates-2ed](https://github.com/downdemo/Cpp-Templates-2ed)|📚 C++ Templates 2ed 笔记：C++11/14/17 模板技术|699|C++|2021/11/25|
|61|[OAID/AutoKernel](https://github.com/OAID/AutoKernel)|AutoKernel 是一个简单易用，低门槛的自动算子优化工具，提高深度学习算法部署效率。|686|C++|2021/09/27|
|62|[mmc-maodun/Data-Structure-And-Algorithm](https://github.com/mmc-maodun/Data-Structure-And-Algorithm)|Data Structure And Algorithm（常用数据结构与算法C/C++实现）|667|C++|2021/11/01|
|63|[Rvn0xsy/Cooolis-ms](https://github.com/Rvn0xsy/Cooolis-ms)|Cooolis-ms是一个包含了Metasploit Payload Loader、Cobalt Strike External C2 Loader、Reflective DLL injection的代码执行工具，它的定位在于能够在静态查杀上规避一些我们将要执行且含有特征的代码，帮助红队人员更方便快捷的从Web容器环境切换到C2环境进一步进行工作。|658|C++|2021/08/03|
|64|[SOUI2/soui](https://github.com/SOUI2/soui)|SOUI是目前为数不多的轻量级可快速开发window桌面程序开源DirectUI库.其前身为Duiengine,更早期则是源自于金山卫士开源版本UI库Bkwin.经过多年持续更新方得此库|635|C++|2021/09/14|
|65|[downdemo/Cpp-Concurrency-in-Action-2ed](https://github.com/downdemo/Cpp-Concurrency-in-Action-2ed)|📚 C++ Concurrency in Action 2ed 笔记：C++11/14/17 多线程技术|630|C++|2021/05/12|
|66|[anhkgg/SuperDllHijack](https://github.com/anhkgg/SuperDllHijack)|SuperDllHijack：A general DLL hijack technology, don't need to manually export the same function interface of the DLL, so easy! 一种通用Dll劫持技术，不再需要手工导出Dll的函数接口了|598|C++|2021/11/10|
|67|[czs108/Cpp-Primer-5th-Notes-CN](https://github.com/czs108/Cpp-Primer-5th-Notes-CN)|📚 《C++ Primer中文版（第5版）》笔记|593|C++|2021/05/08|
|68|[Chaoses-Ib/IbEverythingExt](https://github.com/Chaoses-Ib/IbEverythingExt)|Everything 拼音搜索、快速选择扩展|585|C++|2021/11/18|
|69|[AngelMonica126/GraphicAlgorithm](https://github.com/AngelMonica126/GraphicAlgorithm)|:octopus: :octopus:图形学论文实现|583|C++|2021/08/24|
|70|[wlgq2/uv-cpp](https://github.com/wlgq2/uv-cpp)|libuv wrapper in C++11 /libuv C++11网络库|568|C++|2021/10/26|
|71|[eritpchy/Fingerprint-pay-magisk-wechat](https://github.com/eritpchy/Fingerprint-pay-magisk-wechat)|微信指纹支付 (Fingerprint pay for WeChat)|563|C++|2021/09/01|
|72|[richenyunqi/CCF-CSP-and-PAT-solution](https://github.com/richenyunqi/CCF-CSP-and-PAT-solution)|CCF CSP和PAT考试题解（使用C++14语法）|550|C++|2021/11/14|
|73|[lcatro/Source-and-Fuzzing](https://github.com/lcatro/Source-and-Fuzzing)|一些阅读源码和Fuzzing 的经验,涵盖黑盒与白盒测试..|548|C++|2021/08/25|
|74|[githubhaohao/OpenGLCamera2](https://github.com/githubhaohao/OpenGLCamera2)|🔥 Android OpenGL Camera 2.0  实现 30 种滤镜和抖音特效|538|C++|2021/10/11|
|75|[PaddlePaddle/Serving](https://github.com/PaddlePaddle/Serving)|A flexible, high-performance carrier for machine learning models（『飞桨』服务化部署框架）|518|C++|2021/11/30|
|76|[YYC572652645/QCoolPage](https://github.com/YYC572652645/QCoolPage)|Qt炫酷界面|513|C++|2021/07/03|
|77|[DefTruth/lite.ai.toolkit](https://github.com/DefTruth/lite.ai.toolkit)|Lite.AI.ToolKit 🚀🚀🌟:  A lite C++ toolkit of awesome AI models(一个开箱即用的C++ AI工具箱，支持ONNXRuntime/NCNN/MNN/TNN), RobustVideoMatting🔥, YOLOX🔥, YOLOP🔥 etc. https://github.com/DefTruth/lite.ai.toolkit|508|C++|2021/12/01|
|78|[githubhaohao/LearnFFmpeg](https://github.com/githubhaohao/LearnFFmpeg)|Android FFmpeg 音视频开发教程|496|C++|2021/10/11|
|79|[wangzuohuai/WebRunLocal](https://github.com/wangzuohuai/WebRunLocal)|PluginOK(牛插)中间件是一个实现网页浏览器(Web Browser)与本地程序(Local App)之间进行双向调用的低成本、强兼容、安全可控、轻量级、易集成、可扩展、跨浏览器的原生小程序系统。通过此中间件可实现网页前端JS脚本无障碍操作本地电脑各种硬件、调用本地系统API及相关组件功能，可彻底解决DLL模块、ActiveX控件及自动化程序(如微软Office、金山WPS、AutoCAD等)在Chrome、Edge、360、FireFox、IE、Opera、QQ、搜狗等浏览器各版本中的嵌入使用问题，媲美原Java Applet的效果|478|C++|2021/11/28|
|80|[liuchuo/LeetCode](https://github.com/liuchuo/LeetCode)|🍡 LeetCode Online Judge刷题题解(Java/C++/Python/Ruby/Swift)|464|C++|2021/10/18|
|81|[sukhoeing/aoapc-bac2nd-keys](https://github.com/sukhoeing/aoapc-bac2nd-keys)|算法竞赛入门经典第2版-习题选解|463|C++|2021/09/08|
|82|[USTC-Hackergame/hackergame2020-writeups](https://github.com/USTC-Hackergame/hackergame2020-writeups)|中国科学技术大学第七届信息安全大赛的官方与非官方题解|459|C++|2021/08/16|
|83|[gloomyfish1998/opencv_tutorial](https://github.com/gloomyfish1998/opencv_tutorial)|基于OpenCV4.0 C++/Python SDK的案例代码演示程序与效果图像|448|C++|2021/08/25|
|84|[KikoPlayProject/KikoPlay](https://github.com/KikoPlayProject/KikoPlay)|KikoPlay - NOT ONLY A Full-Featured Danmu Player  不仅仅是全功能弹幕播放器|443|C++|2021/11/21|
|85|[Syencil/tensorRT](https://github.com/Syencil/tensorRT)|TensorRT-7 Network Lib 包括常用目标检测、关键点检测、人脸检测、OCR等 可训练自己数据|438|C++|2021/07/17|
|86|[KangLin/RabbitIm](https://github.com/KangLin/RabbitIm)|玉兔即时通讯。开源的跨平台的的即时通信系统。包括文本、音视频、白板、远程控制|426|C++|2021/11/30|
|87|[FengGuanxi/HDU-Experience](https://github.com/FengGuanxi/HDU-Experience)|用于向所有杭电学子分享在杭电的知识与经验|425|C++|2021/07/24|
|88|[youngyangyang04/Skiplist-CPP](https://github.com/youngyangyang04/Skiplist-CPP)|A tiny KV storage based on skiplist written in C++ language   使用C++开发，基于跳表实现的轻量级键值数据库🔥🔥 🚀|423|C++|2021/10/26|
|89|[no5ix/realtime-server](https://github.com/no5ix/realtime-server)|A lightweight game server engine. 一个轻量级的游戏服务器引擎|423|C++|2021/09/29|
|90|[188080501/JQHttpServer](https://github.com/188080501/JQHttpServer)|基于Qt开发的轻量级HTTP/HTTPS服务器|408|C++|2021/11/30|
|91|[cc20110101/RedisView](https://github.com/cc20110101/RedisView)|RedisView implements open source, cross-platform and high performance Redis interface tools through self-written RESP protocol parsing, self-written tree model and thread pool.   RedisView业余爱好通过自写RESP协议解析、自写树模型、线程池实现开源、跨平台、高性能Redis界面图形化工具|404|C++|2021/06/24|
|92|[fletcherjiang/ssm-alipay](https://github.com/fletcherjiang/ssm-alipay)|将微信支付和支付宝支付集成到ssm项目中|401|C++|2021/10/09|
|93|[WallBreaker2/op](https://github.com/WallBreaker2/op)|Windows消息模拟,gdi,dx,opengl截图，找图,找字(OCR)等功能|399|C++|2021/11/23|
|94|[BADBADBADBOY/pytorchOCR](https://github.com/BADBADBADBOY/pytorchOCR)|基于pytorch的ocr算法库，包括 psenet, pan, dbnet, sast , crnn|386|C++|2021/05/19|
|95|[HackerDev-Felix/WechatDecrypt](https://github.com/HackerDev-Felix/WechatDecrypt)|微信消息解密工具|384|C++|2021/07/12|
|96|[feiyangqingyun/qucsdk](https://github.com/feiyangqingyun/qucsdk)|Qt编写的自定义控件插件的sdk集合，包括了各个操作系统的动态库文件以及控件的头文件和sdk使用demo。心中有坐标，万物皆painter，欢迎各位咨询、购买、定制控件，QQ：517216493 微信：feiyangqingyun。|381|C++|2021/11/14|
|97|[xdnice/PCShare](https://github.com/xdnice/PCShare)|PCShare是一款强大的远程控制软件，可以监视目标机器屏幕、注册表、文件系统等。|379|C++|2021/07/24|
|98|[Project-LemonLime/Project_LemonLime](https://github.com/Project-LemonLime/Project_LemonLime)|为了 OI 比赛而生的基于 Lemon + LemonPlus 的轻量评测系统   三大桌面系统支持|374|C++|2021/11/10|
|99|[y123456yz/reading-and-annotate-mongodb-3.6](https://github.com/y123456yz/reading-and-annotate-mongodb-3.6)|分布式文档数据库mongodb-3.6(mongos、mongod、wiredtiger存储引擎)源码中文注释分析，持续更新。后期重点进行mongodb-4.4最新版本内核源码分析|370|C++|2021/09/17|
|100|[itas109/CSerialPort](https://github.com/itas109/CSerialPort)|基于C++的轻量级开源跨平台串口类库Lightweight cross-platform serial port library based on C++|365|C++|2021/11/13|
|101|[lizhenghn123/zl_threadpool](https://github.com/lizhenghn123/zl_threadpool)|Linux平台下C++(C++98、C++03、C++11)实现的线程池|363|C++|2021/05/01|
|102|[chatopera/clause](https://github.com/chatopera/clause)|:horse_racing: 聊天机器人，自然语言理解，语义理解|360|C++|2021/09/14|
|103|[CDDSCLab/training-plan](https://github.com/CDDSCLab/training-plan)|电子科技大学分布式存储与计算实验室新生训练计划|358|C++|2021/11/28|
|104|[tearshark/librf](https://github.com/tearshark/librf)|基于C++ Coroutines编写的无栈协程库|356|C++|2021/11/15|
|105|[HuangCongQing/pcl-learning](https://github.com/HuangCongQing/pcl-learning)|🔥PCL（Point Cloud Library）点云库学习记录|353|C++|2021/09/10|
|106|[openvanilla/McBopomofo](https://github.com/openvanilla/McBopomofo)|小麥注音輸入法|353|C++|2021/11/28|
|107|[LiveStockShapeAnalysis/Point-Cloud-Processing-example](https://github.com/LiveStockShapeAnalysis/Point-Cloud-Processing-example)|点云库PCL从入门到精通 书中配套案例|339|C++|2021/09/04|
|108|[mayerui/sudoku](https://github.com/mayerui/sudoku)|C++实现的跨平台数独游戏，命令行操作易上手，可以在开发间隙用来放松身心。数百行代码，初学者也可以轻松掌握。|337|C++|2021/05/28|
|109|[anbingxu666/WangDao-DataStructure](https://github.com/anbingxu666/WangDao-DataStructure)|《数据结构》经典算法代码|318|C++|2021/07/16|
|110|[0x727/SqlKnife_0x727](https://github.com/0x727/SqlKnife_0x727)|适合在命令行中使用的轻巧的SQL Server数据库安全检测工具|317|C++|2021/10/23|
|111|[lengjibo/NetUser](https://github.com/lengjibo/NetUser)|使用windows api添加用户，可用于net无法使用时.分为nim版，c++版本，RDI版，BOF版。|316|C++|2021/09/29|
|112|[vczh/GacUIBlog](https://github.com/vczh/GacUIBlog)|记录 GacUI 开发10年来背后的故事，以及对架构设计的考量。|310|C++|2021/10/30|
|113|[Dice-Developer-Team/Dice](https://github.com/Dice-Developer-Team/Dice)|QQ Dice Robot For TRPG QQ跑团掷骰机器人|305|C++|2021/12/01|
|114|[chengyangkj/Ros_Qt5_Gui_App](https://github.com/chengyangkj/Ros_Qt5_Gui_App)|ROS human computer interface based on Qt5(基于Qt5的ROS人机交互界面)|303|C++|2021/07/17|
|115|[jashking/UnrealPakViewer](https://github.com/jashking/UnrealPakViewer)|查看 UE4 Pak 文件的图形化工具，支持 UE4 pak/ucas 文件|297|C++|2021/09/28|
|116|[ChunelFeng/caiss](https://github.com/ChunelFeng/caiss)|一款简单好用的 跨平台/多语言的 相似向量/相似词/相似句 高性能检索引擎。欢迎star & fork。Build together! Power another !|295|C++|2021/09/11|
|117|[Tencent/deepx_core](https://github.com/Tencent/deepx_core)|deepx_core是一个专注于张量计算/深度学习的基础库|288|C++|2021/07/06|
|118|[Beipy/Mac-Hackintosh-Clover](https://github.com/Beipy/Mac-Hackintosh-Clover)|PC主机黑苹果引导驱动文件|287|C++|2021/08/16|
|119|[AmazingPP/subVerison_GTAV_Hack](https://github.com/AmazingPP/subVerison_GTAV_Hack)|subVerison重置版——GTA5外置修改器|286|C++|2021/08/15|
|120|[AZE98/CQUPT-Study-Data](https://github.com/AZE98/CQUPT-Study-Data)|在重邮本科阶段积累的实验报告、PPT等课程资料及课外学习资料。|285|C++|2021/08/16|
|121|[iUIShop/LibUIDK](https://github.com/iUIShop/LibUIDK)|mfc skin ui，not directui。视频教程：https://v.youku.com/v_show/id_XNTczMzg5MDky.html  简单来说，LibUIDK是用来开发QQ、360安全卫士那样的漂亮软件界面的。 LibUIDK原来是商业界面库，2019年9月8号开源。是专业开发Windows平台下图形用户界面的开发包，该开发包基于Microsoft的MFC库。使用此开发工具包可轻易把美工制作的精美界面用Visual C++实现，由于LibUIDK采用所见即所得的方式创建产品界面，所以极大的提高了产品的开发速度，并大大增强图形用户界面(GUI)的亲和力。LibUIDK还 ...|283|C++|2021/08/19|
|122|[Z-yq/TensorflowASR](https://github.com/Z-yq/TensorflowASR)|集成了Tensorflow 2版本的端到端语音识别模型，并且RTF(实时率)在0.1左右/Mandarin State-of-the-art Automatic Speech Recognition in Tensorflow 2|280|C++|2021/11/15|
|123|[chengciming/wechatPc](https://github.com/chengciming/wechatPc)|PC微信hook源码，PC微信注入，逆向编程，可以制作微信机器人玩玩，仅供学习，请不要用于商业、违法途径，本人不对此源码造成的违法负责！|278|C++|2021/07/08|
|124|[tsingsee/EasyPlayerPro-Win](https://github.com/tsingsee/EasyPlayerPro-Win)|EasyPlayerPro是一款免费的全功能流媒体播放器，支持RTSP、RTMP、HTTP、HLS、UDP、RTP、File等多种流媒体协议播放、支持本地文件播放，支持本地抓拍、本地录像、播放旋转、多屏播放、倍数播放等多种功能特性，核心基于ffmpeg，稳定、高效、可靠、可控，支持Windows、Android、iOS三个平台，目前在多家教育、安防、行业型公司，都得到的应用，广受好评！|274|C++|2021/08/05|
|125|[wh201906/Proxmark3GUI](https://github.com/wh201906/Proxmark3GUI)|A cross-platform GUI for Proxmark3 client   为PM3设计的图形界面|274|C++|2021/11/24|
|126|[kevinlq/SmartHome-Qt](https://github.com/kevinlq/SmartHome-Qt)|基于zigbee和stm32的智能家居系统，上位机使用Qt编写，实现了基本的监控。主要包括监控室内温度、湿度、烟雾浓度，用led灯模拟控制家中的灯。界面良好。|273|C++|2021/11/18|
|127|[Qv2ray/QvPlugin-Trojan](https://github.com/Qv2ray/QvPlugin-Trojan)|在 Qv2ray 中使用 Trojan, 感谢 Trojan-Qt5 0.x|272|C++|2021/08/17|
|128|[ToanTech/Deng-s-foc-controller](https://github.com/ToanTech/Deng-s-foc-controller)|灯哥开源 FOC 双路迷你无刷电机驱动|271|C++|2021/11/15|
|129|[TJ-CSCCG/TJCS-Course](https://github.com/TJ-CSCCG/TJCS-Course)|:bulb: 同济大学计算机科学与技术、信息安全专业课程资源共享仓库。含部分科目介绍、报告模板、实验工具等内容。期待更多课程加入……|266|C++|2021/12/01|
|130|[BesLyric-for-X/BesLyric-for-X](https://github.com/BesLyric-for-X/BesLyric-for-X)|BesLyric-for-X 是一款操作简单、功能实用的歌词制作软件。我们开发此软件的目的是为了为广大网易云音乐云村村民提供一个良好的歌词制作体验。|263|C++|2021/10/10|
|131|[SequoiaDB/SequoiaDB](https://github.com/SequoiaDB/SequoiaDB)|SequoiaDB 巨杉数据库是一款金融级分布式关系型数据库。 自研的原生分布式存储引擎支持完整 ACID，具备弹性扩展、高并发和高可用特性，支持 MySQL、PostgreSQL 和 SparkSQL 等多种 SQL 访问形式，适用于核心交易、数据中台、内容管理等应用场景。 |260|C++|2021/08/19|
|132|[Qv2ray/QvPlugin-SSR](https://github.com/Qv2ray/QvPlugin-SSR)|适用于 Qv2ray 的 ShadowSocksR 插件，使用此插件在 Qv2ray 中启用 SSR 功能|259|C++|2021/08/17|
|133|[eritpchy/Fingerprint-pay-magisk-alipay](https://github.com/eritpchy/Fingerprint-pay-magisk-alipay)|支付宝指纹支付 (Fingerprint pay for Alipay)|258|C++|2021/09/01|
|134|[Ubpa/Utopia](https://github.com/Ubpa/Utopia)|Utopia Game Engine 无境游戏引擎|257|C++|2021/08/21|
|135|[oceancx/CXEngine](https://github.com/oceancx/CXEngine)|CXEngine是一个方便大家使用lua+imgui做游戏的游戏引擎,目前主要整合了vscode lua调试器，网络库，还有lua imgui，以及2D回合制MMORPG的框架|255|C++|2021/08/18|
|136|[tsingsee/EasyScreenLive](https://github.com/tsingsee/EasyScreenLive)|Streaming media sdk tool：EasyScreenLive是一款简单、高效、稳定的集采集，编码，组播，推流和流媒体RTSP服务于一身的同屏功能组件，具低延时，高效能，低丢包等特点。目前支持Windows，Android平台，通过EasyScreenLive我们就可以避免接触到稍显复杂的音视频源采集，编码和流媒体推送以及RTSP/RTP/RTCP/RTMP服务流程，只需要调用EasyScreenLive的几个API接口，就能轻松、稳定地把流媒体音视频数据RTMP推送给EasyDSS服务器以及发布RTSPServer服务，RTSP同屏服务支持组播和单播两种模式。|251|C++|2021/07/14|
|137|[DragonQuestHero/Kernel-Anit-Anit-Debug-Plugins](https://github.com/DragonQuestHero/Kernel-Anit-Anit-Debug-Plugins)|Kernel Anit Anit Debug Plugins 内核反反调试插件|248|C++|2021/08/31|
|138|[ZhuYanzhen1/miniFOC](https://github.com/ZhuYanzhen1/miniFOC)|你还在为有刷电机的高噪声、低响应速度和低寿命而烦恼吗？这个项目是一个20块钱就能搞定的FOC无刷电机控制方案！This project is a FOC (Field Oriented Control) BLDC Motor control scheme that can be done for 3$!|246|C++|2021/11/08|
|139|[qinyuLT/CloudPan](https://github.com/qinyuLT/CloudPan)|linux下c/c++模拟实现云盘项目|235|C++|2021/10/02|
|140|[LeechanX/Ring-Log](https://github.com/LeechanX/Ring-Log)|Ring-Log是一个高效简洁的C++异步日志， 其特点是效率高（每秒支持至少125万+日志写入）、易拓展，尤其适用于频繁写日志的场景|234|C++|2021/06/27|
|141|[DayBreak-u/yolo-face-with-landmark](https://github.com/DayBreak-u/yolo-face-with-landmark)|yoloface大礼包  使用pytroch实现的基于yolov3的轻量级人脸检测（包含关键点）|233|C++|2021/09/08|
|142|[iwxyi/Bilibili-MagicalDanmaku](https://github.com/iwxyi/Bilibili-MagicalDanmaku)|【神奇弹幕】哔哩哔哩直播万能机器人，弹幕姬+答谢姬+回复姬+点歌姬+各种小骚操作，目前唯一可编程机器人|225|C++|2021/11/30|
|143|[anyRTC-UseCase/SipRtcProxy](https://github.com/anyRTC-UseCase/SipRtcProxy)|网关服务：Sip与Rtc互通，实现Web，Android，iOS，小程序，SIP座机，PSTN电话，手机互通。|224|C++|2021/11/30|
|144|[CoderMJLee/audio-video-dev-tutorial](https://github.com/CoderMJLee/audio-video-dev-tutorial)|简单易懂的音视频开发教程|223|C++|2021/07/08|
|145|[DreamWaterFound/self_commit_ORB-SLAM2](https://github.com/DreamWaterFound/self_commit_ORB-SLAM2)|ORB-SLAM2 源码注释， 基于泡泡机器人的注释版本|218|C++|2021/10/30|
|146|[MistEO/MeoAssistance-Arknights](https://github.com/MistEO/MeoAssistance-Arknights)|明日方舟助手，自动刷图、智能基建换班|215|C++|2021/12/01|
|147|[elerao/Smark](https://github.com/elerao/Smark)|简洁的 markdown 编辑器 Smark|207|C++|2021/09/23|
|148|[834810071/muduo_study](https://github.com/834810071/muduo_study)|Linux多线程服务端编程[muduo C++网络库]|204|C++|2021/09/09|
|149|[q191201771/libchef](https://github.com/q191201771/libchef)|🍀 c++ standalone header-only basic library.    c++头文件实现无第三方依赖基础库|198|C++|2021/08/07|
|150|[0x727/ShuiYing_0x727](https://github.com/0x727/ShuiYing_0x727)|检测域环境内，域机器的本地管理组成员是否存在弱口令和通用口令，对域用户的权限分配以及域内委派查询|197|C++|2021/08/10|
|151|[KangLin/FaceRecognizer](https://github.com/KangLin/FaceRecognizer)|人脸识别应用|195|C++|2021/11/29|
|152|[lilinxiong/cppPrimerPlus-six-](https://github.com/lilinxiong/cppPrimerPlus-six-)|C++PrimerPlus(第6版)中文版源码|195|C++|2021/11/09|
|153|[Tencent/yadcc](https://github.com/Tencent/yadcc)|Yet Another Distributed C++ Compiler. yadcc是一套腾讯广告自研的分布式编译系统，用于支撑腾讯广告的日常开发及流水线。相对于已有的同类解决方案，我们针对实际的工业生产环境做了性能、可靠性、易用性等方面优化。|195|C++|2021/11/25|
|154|[bifang-fyh/gude](https://github.com/bifang-fyh/gude)|gude - 一个C++编写的DHT爬虫，用于爬取DHT网络上的torrent文件|194|C++|2021/05/28|
|155|[aaaddress1/Windows-APT-Warfare](https://github.com/aaaddress1/Windows-APT-Warfare)|著作《Windows APT Warfare：惡意程式前線戰術指南》各章節技術實作之原始碼內容|191|C++|2021/05/05|
|156|[neil3d/50YearsOfRayTracing](https://github.com/neil3d/50YearsOfRayTracing)|以历史的发展的眼光来看光线追踪技术，1968年至2018年重点论文相关算法复现。|190|C++|2021/06/06|
|157|[kevinlq/LQFramKit](https://github.com/kevinlq/LQFramKit)|c++ Qt5 implementation of some control(使用C++ Qt5封装的一些控件，以便后期项目中直接使用。这些控件有些是来自于网络有些属于个人封装，代码中都有出处)|187|C++|2021/11/23|
|158|[tiny656/PAT](https://github.com/tiny656/PAT)|浙江大学PAT题解|186|C++|2021/11/08|
|159|[netease-kit/NIM_PC_Demo](https://github.com/netease-kit/NIM_PC_Demo)|云信Windows(PC) C/C++ Demo源码仓库|185|C++|2021/10/29|
|160|[huoji120/CobaltStrikeDetected](https://github.com/huoji120/CobaltStrikeDetected)|40行代码检测到大部分CobaltStrike的shellcode|184|C++|2021/07/25|
|161|[nowar-fonts/Warcraft-Font-Merger](https://github.com/nowar-fonts/Warcraft-Font-Merger)|Warcraft Font Merger，魔兽世界字体合并/补全工具。|181|C++|2021/06/15|
|162|[moranzcw/LeetCode-NOTES](https://github.com/moranzcw/LeetCode-NOTES)|LeetCode 算法解答|180|C++|2021/11/14|
|163|[ExtremeMart/dev-docs](https://github.com/ExtremeMart/dev-docs)|服务于极市平台开发者的项目，提供SDK接口文件规范与常见问题示例代码。欢迎所有开发者一起参与示例代码编写。|179|C++|2021/09/09|
|164|[esrrhs/fake](https://github.com/esrrhs/fake)|嵌入式脚本语言 Lightweight embedded scripting language|179|C++|2021/07/02|
|165|[huanzheWu/Data-Structure](https://github.com/huanzheWu/Data-Structure)|基于C++模板 实现的数据结构代码|178|C++|2021/06/16|
|166|[Qv2ray/QvPlugin-Trojan-Go](https://github.com/Qv2ray/QvPlugin-Trojan-Go)|适用于 Qv2ray v2.6+ 的 Trojan-Go 插件|178|C++|2021/08/17|
|167|[kongpf8848/Animation](https://github.com/kongpf8848/Animation)|Android各种动画效果合集，项目包含了丰富的动画实例(逐帧动画，补间动画，Lottie动画，GIF动画，SVGA动画)，体验动画之美，让Android动起来:smile::smile::smile:|171|C++|2021/11/15|
|168|[dekuan/VwFirewall](https://github.com/dekuan/VwFirewall)|微盾®VirtualWall®防火墙整套源代码|169|C++|2021/06/17|
|169|[hinesboy/ai_tetris](https://github.com/hinesboy/ai_tetris)|AI 俄罗斯方块（C++）|167|C++|2021/08/06|
|170|[metartc/yangwebrtc](https://github.com/metartc/yangwebrtc)|中国人自己的webrtc,非谷歌lib|166|C++|2021/11/30|
|171|[zhuzhu-Top/Wx_Socket_Helper](https://github.com/zhuzhu-Top/Wx_Socket_Helper)|PC微信助手|160|C++|2021/08/27|
|172|[mlzeng/CSC2020-USTC-FlammingMyCompiler](https://github.com/mlzeng/CSC2020-USTC-FlammingMyCompiler)|全国大学生计算机系统能力大赛编译系统设计赛项目|159|C++|2021/05/17|
|173|[JelinYao/HttpInterface](https://github.com/JelinYao/HttpInterface)|Windows上C++封装的HTTP库，包含三种实现模式（WinInet、WinHttp、socket）|159|C++|2021/05/01|
|174|[tsingsee/EasyRTSPLive](https://github.com/tsingsee/EasyRTSPLive)|Streaming media middleware：RTSP to RTMP，拉流IPC摄像机或者NVR硬盘录像机RTSP流转成RTMP推送到阿里云CDN/腾讯云CDN/RTMP流媒体服务器，支持多路RTSP流同时拉取并以RTMP协议推送发布，EasyRTSPLive我们支持任何平台，包括但不限于Windows/Linux/Android/ARM|155|C++|2021/06/04|
|175|[flexih/Snake](https://github.com/flexih/Snake)|Yet Another Mach-O Unused ObjC Selector/Class/Protocol Detector. 检测ObjC无用方法、无用类、无用协议。|152|C++|2021/08/18|
|176|[neil3d/UnrealCookbook](https://github.com/neil3d/UnrealCookbook)|虚幻4引擎的一些编程实践分享|151|C++|2021/11/06|
|177|[changfeng1050/SerialWizard](https://github.com/changfeng1050/SerialWizard)|使用C++ 20 & Qt 编写的跨平台多功能串口调试工具|151|C++|2021/07/26|
|178|[SmartKeyerror/reading-source-code-of-leveldb-1.23](https://github.com/SmartKeyerror/reading-source-code-of-leveldb-1.23)|leveldb 源码阅读，分析 DB 运作流程与 WAL、SSTable 等文件格式与 Compaction 过程。|149|C++|2021/09/24|
|179|[dustpg/LongUI](https://github.com/dustpg/LongUI)|Lightweight C++ GUI Library 轻量级C++图形界面库|145|C++|2021/10/20|
|180|[chxuan/cpp-utils](https://github.com/chxuan/cpp-utils)|:hibiscus: 一些C/C++常用封装例子|144|C++|2021/09/25|
|181|[BigPig0/RelayLive](https://github.com/BigPig0/RelayLive)|视频服务中继，转换传输协议。将rtsp、gb28181转为html5可以直接播放的协议。|144|C++|2021/05/24|
|182|[richardchien/modern-cmake-by-example](https://github.com/richardchien/modern-cmake-by-example)|IPADS 实验室新人培训第二讲（2021.11.3）：CMake|142|C++|2021/11/04|
|183|[GoatGirl98/Walkthrough-of-ACCoding-in-BUAA](https://github.com/GoatGirl98/Walkthrough-of-ACCoding-in-BUAA)|北航OJ通关攻略，包括北航软件学院的在线评测网站（OJ）——AC编程（accoding.cn）开放课程的全部题解|142|C++|2021/11/23|
|184|[muluoleiguo/interview](https://github.com/muluoleiguo/interview)|linux C++ 服务器/后台开发 秋招整理资料 |141|C++|2021/11/07|
|185|[mohuihui/DingTalk_Assistant](https://github.com/mohuihui/DingTalk_Assistant)|钉钉助手，主要功能包括：聊天消息防撤回、程序多开、屏蔽频繁升级等。|141|C++|2021/08/21|
|186|[hongwenjun/WinKcp_Launcher](https://github.com/hongwenjun/WinKcp_Launcher)|Windows udp2raw+kcptun 加速tcp流量 简易工具 by 蘭雅sRGB|135|C++|2021/09/23|
|187|[wlxklyh/SoftRenderer](https://github.com/wlxklyh/SoftRenderer)|Soft Renderer软渲染器：安卓工程师、iOS工程师、Unity工程师、Unreal工程师、Java工程师、C++工程师、C#工程师可以通过此来快速深刻理解渲染管线|135|C++|2021/10/23|
|188|[rongweihe/CPPNotes](https://github.com/rongweihe/CPPNotes)|【C++ 面试 + C++ 学习指南】 一份涵盖大部分 C++ 程序员所需要掌握的核心知识。|133|C++|2021/08/28|
|189|[luosiwei-cmd/CarYon](https://github.com/luosiwei-cmd/CarYon)|🔖一款基于C++的OI/ACM比赛出题解题辅助工具⭐|127|C++|2021/05/23|
|190|[zero-rp/ZUI](https://github.com/zero-rp/ZUI)|一个小巧但强大的C界面库|126|C++|2021/05/15|
|191|[u0u0/Cocos2d-Lua-Community](https://github.com/u0u0/Cocos2d-Lua-Community)|基于Cocos2d-x 4.0，打造易用稳定的Cocos2d-x lua引擎。|125|C++|2021/11/30|
|192|[compilelife/loginsight](https://github.com/compilelife/loginsight)|loginsight致力于打造一款日志分析的利器|125|C++|2021/06/27|
|193|[0x727/CloneX_0x727](https://github.com/0x727/CloneX_0x727)|进行克隆用户、添加用户等账户防护安全检测的轻巧工具|123|C++|2021/09/03|
|194|[BiBoyang/BoyangBlog](https://github.com/BiBoyang/BoyangBlog)|伯阳写东西的地方|123|C++|2021/08/29|
|195|[xiangli0608/Creating-2D-laser-slam-from-scratch](https://github.com/xiangli0608/Creating-2D-laser-slam-from-scratch)|从零开始创建二维激光SLAM|121|C++|2021/11/22|
|196|[yuangu/sxtwl_cpp](https://github.com/yuangu/sxtwl_cpp)|寿星天文历的C++实现版本|121|C++|2021/11/21|
|197|[ziqiangxu/words-picker](https://github.com/ziqiangxu/words-picker)|希望成为一款好的取词应用|120|C++|2021/08/21|
|198|[esrrhs/hookso](https://github.com/esrrhs/hookso)|linux动态链接库的注入修改查找工具 A tool for injection, modification and search of linux dynamic link library|119|C++|2021/11/05|
|199|[yongplus/tinypng](https://github.com/yongplus/tinypng)|基于TinyPNG图片压缩软件，操作简单高效，支持MacOS和Windows系统，无需其它依赖运行。|119|C++|2021/11/11|
|200|[HuyaInc/Hercules](https://github.com/HuyaInc/Hercules)|Hercules 是以json+lua的灵活方式控制视频混画混流mcu，简单灵活完成业务需求。|116|C++|2021/06/21|

⬆ [回到目录](#内容目录)

<br/>

## C#

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[huiyadanli/RevokeMsgPatcher](https://github.com/huiyadanli/RevokeMsgPatcher)|:trollface: A hex editor for WeChat/QQ/TIM - PC版微信/QQ/TIM防撤回补丁（我已经看到了，撤回也没用了）|12.3k|C#|2021/09/17|
|2|[JeffreySu/WeiXinMPSDK](https://github.com/JeffreySu/WeiXinMPSDK)|微信全平台 SDK Senparc.Weixin for C#，支持 .NET Framework 及 .NET Core、.NET 6.0。已支持微信公众号、小程序、小游戏、企业号、企业微信、开放平台、微信支付、JSSDK、微信周边等全平台。 WeChat SDK for C#.|7.1k|C#|2021/11/30|
|3|[TGSAN/CMWTAT_Digital_Edition](https://github.com/TGSAN/CMWTAT_Digital_Edition)|CloudMoe Windows 10 Activation Toolkit get digital license, the best open source Win 10 activator in GitHub. GitHub 上最棒的开源 Win10 数字权利（数字许可证）激活工具！|6.6k|C#|2021/06/16|
|4|[nilaoda/N_m3u8DL-CLI](https://github.com/nilaoda/N_m3u8DL-CLI)|[.NET] m3u8 downloader 开源的命令行m3u8/HLS/dash下载器，支持普通AES-128-CBC解密，多线程，自定义请求头等. 支持简体中文,繁体中文和英文. English Supported.|6.5k|C#|2021/11/26|
|5|[studyzy/imewlconverter](https://github.com/studyzy/imewlconverter)|一款开源免费的输入法词库转换程序|4.5k|C#|2021/11/06|
|6|[SteamTools-Team/SteamTools](https://github.com/SteamTools-Team/SteamTools)|  🛠「Steam++」是一个包含多种Steam工具功能的工具箱。|4.3k|C#|2021/12/01|
|7|[dotnetcore/Util](https://github.com/dotnetcore/Util)|Util是一个.net core平台下的应用框架，旨在提升小型团队的开发输出能力，由常用公共操作类(工具类)、分层架构基类、Ui组件，第三方组件封装，第三方业务接口封装，配套代码生成模板，权限等组成。|3.9k|C#|2021/11/22|
|8|[donet5/SqlSugar](https://github.com/donet5/SqlSugar)|Best ORM    Fastest ORM   Simple Easy  Sqlite  orm Oracle ORM Mysql Orm  postgresql ORm  SqlServer oRm     达梦 ORM 人大金仓 ORM 神通ORM  C# ORM|3.7k|C#|2021/11/29|
|9|[BluePointLilac/ContextMenuManager](https://github.com/BluePointLilac/ContextMenuManager)|🖱️ 纯粹的Windows右键菜单管理程序|3.6k|C#|2021/11/19|
|10|[anjoy8/Blog.Core](https://github.com/anjoy8/Blog.Core)|💖 ASP.NET Core 6.0 全家桶教程，前后端分离后端接口，vue教程姊妹篇，官方文档：|3.5k|C#|2021/11/29|
|11|[Richasy/Bili.Uwp](https://github.com/Richasy/Bili.Uwp)|适用于新系统UI的哔哩|3.3k|C#|2021/11/28|
|12|[dotnetcore/FastGithub](https://github.com/dotnetcore/FastGithub)|github加速神器，解决github打不开、用户头像无法加载、releases无法上传下载、git-clone、git-pull、git-push失败等问题|3.2k|C#|2021/12/01|
|13|[dotnetcore/FreeSql](https://github.com/dotnetcore/FreeSql)|🦄 .NET orm, Mysql orm, Postgresql orm, SqlServer orm, Oracle orm, Sqlite orm, Firebird orm, 达梦 orm, 人大金仓 orm, 神通 orm, 翰高 orm, 南大通用 orm, MsAccess orm.|3.0k|C#|2021/11/30|
|14|[XINCGer/Unity3DTraining](https://github.com/XINCGer/Unity3DTraining)|Unity的练习项目|3.0k|C#|2021/11/30|
|15|[siteserver/cms](https://github.com/siteserver/cms)|SS CMS 基于 .NET Core，能够以最低的成本、最少的人力投入在最短的时间内架设一个功能齐全、性能优异、规模庞大并易于维护的网站平台。|2.9k|C#|2021/11/28|
|16|[k8gege/Ladon](https://github.com/k8gege/Ladon)|大型内网渗透扫描器&Cobalt Strike，Ladon8.9内置120个模块，包含信息收集/存活主机/端口扫描/服务识别/密码爆破/漏洞检测/漏洞利用。漏洞检测含MS17010/SMBGhost/Weblogic/ActiveMQ/Tomcat/Struts2，密码口令爆破(Mysql/Oracle/MSSQL)/FTP/SSH(Linux)/VNC/Windows(IPC/WMI/SMB/Netbios/LDAP/SmbHash/WmiHash/Winrm),远程执行命令(smbexec/wmiexe/psexec/atexec/sshexec/webshell),降权提权Runas、G ...|2.9k|C#|2021/10/30|
|17|[leiurayer/downkyi](https://github.com/leiurayer/downkyi)|哔哩下载姬downkyi，B站视频下载工具，支持批量下载，支持4K，支持解除地区限制下载，提供工具箱（音视频提取、去水印等）。|2.8k|C#|2021/10/28|
|18|[SeriaWei/ZKEACMS](https://github.com/SeriaWei/ZKEACMS)|ZKEACMS build with .Net 5 (.Net CMS)可视化设计在线编辑内容管理系统|2.6k|C#|2021/11/30|
|19|[cq-panda/Vue.NetCore](https://github.com/cq-panda/Vue.NetCore)|.NetCore+Vue2/Vue3+Element plus，前后端分离，不一样的快速开发框架；提供Vue2、Vue3版本，。http://www.volcore.xyz/|2.3k|C#|2021/12/01|
|20|[jynew/jynew](https://github.com/jynew/jynew)|金庸群侠传3D重制版|2.3k|C#|2021/11/30|
|21|[hanmin0822/MisakaTranslator](https://github.com/hanmin0822/MisakaTranslator)|御坂翻译器—Galgame/文字游戏/漫画多语种实时机翻工具|2.2k|C#|2021/11/20|
|22|[dotnetcore/osharp](https://github.com/dotnetcore/osharp)|OSharp是一个基于.NetCore的快速开发框架，框架对 AspNetCore 的配置、依赖注入、日志、缓存、实体框架、Mvc(WebApi)、身份认证、功能权限、数据权限等模块进行更高一级的自动化封装，并规范了一套业务实现的代码结构与操作流程，使 .Net Core 框架更易于应用到实际项目开发中。|2.1k|C#|2021/11/12|
|23|[nilaoda/BBDown](https://github.com/nilaoda/BBDown)|Bilibili Downloader. 一款命令行式哔哩哔哩下载器.|2.0k|C#|2021/11/28|
|24|[kenvix/USBCopyer](https://github.com/kenvix/USBCopyer)|😉 用于在插上U盘后自动按需复制该U盘的文件。”备份&偷U盘文件的神器”（写作USBCopyer，读作USBCopier）|1.8k|C#|2021/10/05|
|25|[xasset/xasset](https://github.com/xasset/xasset)|开箱即用的 Unity 分包，加密和热更技术。加快你的 Unity 项目的迭代速度和减少用户等待时间，并为程序运行时的流畅度和稳定性带来更多保障。|1.6k|C#|2021/11/24|
|26|[liukuo362573/YiShaAdmin](https://github.com/liukuo362573/YiShaAdmin)|基于 .NET Core MVC 的权限管理系统，代码易读易懂、界面简洁美观。演示版 http://106.14.124.170/admin|1.6k|C#|2021/09/17|
|27|[monitor1394/unity-ugui-XCharts](https://github.com/monitor1394/unity-ugui-XCharts)|A charting and data visualization library for Unity.   一款基于UGUI的数据可视化图表插件。|1.6k|C#|2021/11/30|
|28|[yimengfan/BDFramework.Core](https://github.com/yimengfan/BDFramework.Core)|Simple and powerful Unity3d game workflow!  简单、高效的商业级unity3d 工作流。（已被多个大厂借鉴使用）|1.5k|C#|2021/11/29|
|29|[MonkSoul/Furion](https://github.com/MonkSoul/Furion)|让 .NET 开发更简单，更通用，更流行。|1.4k|C#|2021/11/30|
|30|[Richasy/BiliBili-UWP](https://github.com/Richasy/BiliBili-UWP)|BiliBili的UWP客户端，当然，是第三方的了|1.4k|C#|2021/09/03|
|31|[944095635/DMSkin](https://github.com/944095635/DMSkin)|DMSkin WPF 样式 UI 框架   WPF Borderless Window   Custom Controls & Styles   MVVM Support|1.3k|C#|2021/08/15|
|32|[zengzhan/qqzeng-ip](https://github.com/zengzhan/qqzeng-ip)|最新IP地址数据库-多语言解析以及导入数据库脚本|1.3k|C#|2021/11/17|
|33|[iamoldli/NetModular](https://github.com/iamoldli/NetModular)|NetModular 是基于.Net Core 和 Vue.js 的业务模块化以及前后端分离的快速开发框架|1.2k|C#|2021/09/07|
|34|[neil3d/excel2json](https://github.com/neil3d/excel2json)|把Excel表转换成json对象，并保存到一个文本文件中。|1.2k|C#|2021/10/07|
|35|[Coolapk-UWP/Coolapk-UWP](https://github.com/Coolapk-UWP/Coolapk-UWP)|一个基于UWP平台的第三方酷安客户端|1.2k|C#|2021/11/17|
|36|[essensoft/paylink](https://github.com/essensoft/paylink)|一套基于 .NET Core 开发的支付SDK集，它极大简化了API调用及通知的处理流程。|1.2k|C#|2021/11/16|
|37|[lampo1024/DncZeus](https://github.com/lampo1024/DncZeus)|DncZeus 是一个基于ASP.NET Core 3 + Vue.js(iview-admin) 的前后端分离的通用后台权限(页面访问、操作按钮控制)管理系统框架。后端使用.NET Core 3 + Entity Framework Core构建，UI则是目前流行的基于Vue.js的iView(iview-admin)。项目实现了前后端的动态权限管理和控制以及基于JWT的用户令牌认证机制，让前后端的交互更流畅。码云镜像:https://gitee.com/rector/DncZeus 。演示地址(demo):|1.2k|C#|2021/09/16|
|38|[Bililive/BililiveRecorder](https://github.com/Bililive/BililiveRecorder)|B站录播姬   BiliBili Stream Recorder   哔哩哔哩直播录制|1.1k|C#|2021/11/30|
|39|[zhaopeiym/quartzui](https://github.com/zhaopeiym/quartzui)|基于Quartz.NET3.0的定时任务Web可视化管理。docker打包开箱即用、内置SQLite持久化、语言无关、业务代码零污染、支持 RESTful风格接口、傻瓜式配置|1.1k|C#|2021/10/06|
|40|[Meowv/Blog](https://github.com/Meowv/Blog)|🤣本项目有不同开发版本，最新版底层基于 abp vNext 搭建和免费开源跨平台框架 .NET5 进行开发，使用 MongoDB 存储数据，Redis 缓存数据。项目采用前后端分离的模式进行开发，API 遵循 RESTful 接口规范，页面使用 Blazor 进行开发，可作为 .NET Core 入门项目进行学习。If you liked `Blog` project or if it helped you, please give a star ⭐️ for this repository. 👍👍👍|1.0k|C#|2021/09/21|
|41|[JasonXuDeveloper/JEngine](https://github.com/JasonXuDeveloper/JEngine)|JEngine是针对Unity开发者设计的开箱即用的框架，封装了强大的功能，小白也能快速上手，轻松制作可以热更新的游戏   JEngine is a streamlined and easy-to-use framework designed for Unity Programmers which contains powerful features, beginners can start up quickly and making hot update-able games easily|1.0k|C#|2021/11/28|
|42|[the1812/Malware-Patch](https://github.com/the1812/Malware-Patch)|阻止中国流氓软件的管理员授权. / Prevent UAC authorization of Chinese malware.|1.0k|C#|2021/10/08|
|43|[hemaju/Wireboy.Socket.P2PSocket](https://github.com/hemaju/Wireboy.Socket.P2PSocket)|P2P内网穿透，实现【公司】-【家】远程控制|968|C#|2021/09/08|
|44|[yhuse/SunnyUI](https://github.com/yhuse/SunnyUI)|SunnyUI.Net, 基于.Net Framework 4.0+、.Net Core3.1、.Net 5 框架的 C# WinForm 开源控件库、工具类库、扩展类库、多页面开发框架。|890|C#|2021/11/30|
|45|[huiyadanli/PasteEx](https://github.com/huiyadanli/PasteEx)|:clipboard: Paste As File 把剪贴板的内容直接粘贴为文件|877|C#|2021/09/21|
|46|[dotnetcore/Natasha](https://github.com/dotnetcore/Natasha)|基于 Roslyn 的 C# 动态程序集构建库，该库允许开发者在运行时使用 C# 代码构建域 / 程序集 / 类 / 结构体 / 枚举 / 接口 / 方法等，使得程序在运行的时候可以增加新的模块及功能。Natasha 集成了域管理/插件管理，可以实现域隔离，域卸载，热拔插等功能。 该库遵循完整的编译流程，提供完整的错误提示， 可自动添加引用，完善的数据结构构建模板让开发者只专注于程序集脚本的编写，兼容 stanadard2.0 / netcoreapp3.0+, 跨平台，统一、简便的链式 API。 且我们会尽快修复您的问题及回复您的 issue.|861|C#|2021/11/26|
|47|[cixingguangming55555/wechat-bot](https://github.com/cixingguangming55555/wechat-bot)|带二次开发接口的PC微信聊天机器人|845|C#|2021/11/30|
|48|[bing-framework/Bing.NetCore](https://github.com/bing-framework/Bing.NetCore)|Bing是基于 .net core 2.0 的框架，旨在提升团队的开发输出能力，由常用公共操作类（工具类、帮助类）、分层架构基类，第三方组件封装，第三方业务接口封装等组成。|820|C#|2021/11/30|
|49|[EngTW/English-for-Programmers](https://github.com/EngTW/English-for-Programmers)|《程式英文》：用英文提昇程式可讀性|800|C#|2021/05/21|
|50|[ntminer/NtMiner](https://github.com/ntminer/NtMiner)|GPU miner. github不太慢，https://ntminer.coding.net/public/NtMiner/NtMiner/git/files|753|C#|2021/11/27|
|51|[YSGStudyHards/DotNetGuide](https://github.com/YSGStudyHards/DotNetGuide)|🦸【C#/.NET/.NET Core学习、工作、面试指南】概述：C#/.NET/.NET Core基础知识，学习资料、文章、书籍，社区组织，工具和常见的面试题总结。以及面试时需要注意的事项和优秀简历编写技巧，希望能和大家一起成长进步👊。【让现在的自己不再迷茫✨】|741|C#|2021/11/22|
|52|[dotnetcore/AgileConfig](https://github.com/dotnetcore/AgileConfig)|基于.NET Core开发的轻量级分布式配置中心 / .NET  Core lightweight configuration server|739|C#|2021/11/26|
|53|[wuxiongbin/XIL](https://github.com/wuxiongbin/XIL)|使用ILRuntime实现的类似XLUA功能的Unity3D下热修复BUG的解决方案|721|C#|2021/10/26|
|54|[VictorTzeng/Zxw.Framework.NetCore](https://github.com/VictorTzeng/Zxw.Framework.NetCore)|基于EF Core的Code First模式的DotNetCore快速开发框架，其中包括DBContext、IOC组件autofac和AspectCore.Injector、代码生成器（也支持DB First）、基于AspectCore的memcache和Redis缓存组件，以及基于ICanPay的支付库和一些日常用的方法和扩展，比如批量插入、更新、删除以及触发器支持，当然还有demo。欢迎提交各种建议、意见和pr~|719|C#|2021/06/06|
|55|[Jimmey-Jiang/ABP-ASP.NET-Boilerplate-Project-CMS](https://github.com/Jimmey-Jiang/ABP-ASP.NET-Boilerplate-Project-CMS)|ABP module-zero +AdminLTE+Bootstrap Table+jQuery+Redis + sql server+quartz+hangfire权限管理系统|700|C#|2021/08/05|
|56|[kengwang/BiliDuang](https://github.com/kengwang/BiliDuang)|(项目正在重构) Bilibili 哔哩哔哩视频下载 C# GUI版 - 支持BV 支持4K 支持地区限制下载 支持互动视频 支持无水印 支持弹幕/字幕下载转换 支持课程下载|683|C#|2021/10/06|
|57|[ldqk0/Masuit.Tools](https://github.com/ldqk0/Masuit.Tools)|该仓库为 https://github.com/ldqk/Masuit.Tools 的镜像仓库，代码更新存在较大的延迟。建议前往源仓库：https://github.com/ldqk/Masuit.Tools|676|C#|2021/10/19|
|58|[FastTunnel/FastTunnel](https://github.com/FastTunnel/FastTunnel)|expose a local server to the internet. 高性能跨平台的内网穿透工具 远程内网计算机 域名访问内网站点 反向代理内网服务 花生壳 端口转发 http代理 |672|C#|2021/11/17|
|59|[q315523275/FamilyBucket](https://github.com/q315523275/FamilyBucket)|集合.net core、ocelot、consul、netty、rpc、eventbus、configserver、tracing、sqlsugar、vue-admin、基础管理平台等构建的微服务一条龙应用|667|C#|2021/08/25|
|60|[feature-flags-co/feature-flags-co](https://github.com/feature-flags-co/feature-flags-co)|从版本控制到数据实验，创新你的产品迭代方式。使用源码自建或访问我们的SaaS服务 at https://feature-flags.co/|663|C#|2021/12/01|
|61|[HMBSbige/NatTypeTester](https://github.com/HMBSbige/NatTypeTester)|测试当前网络的 NAT 类型（STUN）|641|C#|2021/12/01|
|62|[cyq1162/cyqdata](https://github.com/cyq1162/cyqdata)|cyq.data is a  high-performance and the most powerful orm.（.NET 最好用的ORM数据层框架，木有之一！支持.NET Core）Support：Txt、Xml、Access、Sqlite、Mssql、Mysql、Oracle、Sybase、Postgres、DB2、Redis、MemCache。|636|C#|2021/11/19|
|63|[chengderen/Smartflow-Sharp](https://github.com/chengderen/Smartflow-Sharp)|基于C#语言研发的Smartflow-Sharp工作流组件，该工作流组件的特点是简单易用、方便扩展、支持多种数据库访问、高度可定制化，支持用户按需求做功能的定制开发，节省用户的使用成本|631|C#|2021/09/30|
|64|[Justin62628/Squirrel-RIFE](https://github.com/Justin62628/Squirrel-RIFE)|效果更好的补帧软件，显存占用更小，是DAIN速度的10-25倍，包含抽帧处理，去除动漫卡顿感|624|C#|2021/10/13|
|65|[tmoonlight/NSmartProxy](https://github.com/tmoonlight/NSmartProxy)|NSmartProxy是一款开源免费的内网穿透工具。采用.NET CORE的全异步模式打造。(NSmartProxy is an open source reverse proxy tool that creates a secure tunnel from a public endpoint to a locally service.)|620|C#|2021/10/27|
|66|[NewLifeX/NewLife.Redis](https://github.com/NewLifeX/NewLife.Redis)|高性能Redis客户端，支持.NETCore/.NET4.0/.NET4.5，为大数据与消息队列而特别优化，线上单应用日均100亿调用量|619|C#|2021/10/01|
|67|[copyliu/bililive_dm](https://github.com/copyliu/bililive_dm)|B站直播彈幕工具|612|C#|2021/10/08|
|68|[CHKZL/DDTV](https://github.com/CHKZL/DDTV)|可进行B站(bilibili)直播开播提醒、自动录制、在线播放、合并、转码、上传的跨平台部署工具|610|C#|2021/11/27|
|69|[mili-tan/AuroraDNS.GUI](https://github.com/mili-tan/AuroraDNS.GUI)|纯净抗污染，简单轻量级|604|C#|2021/11/06|
|70|[m969/EGamePlay](https://github.com/m969/EGamePlay)|一个基于Entity-Component模式的灵活、通用、可扩展的轻量战斗（技能）框架，配置可选使用ScriptableObject或是Excel表格. A flexible, generic, easy to extend, lightweight combat (skills) framework based on Entity-Component pattern. Configuration can choose to use ScriptableObject or Excel tables.|602|C#|2021/11/26|
|71|[XINCGer/UnityToolchainsTrick](https://github.com/XINCGer/UnityToolchainsTrick)|提供一些UnityEditor工具链开发的常用小技巧与示例(Provides some common tips and examples for developing the UnityEditor toolchain)|554|C#|2021/11/19|
|72|[duyanming/Viper](https://github.com/duyanming/Viper)|Viper 是一个基于Anno微服务引擎开发的Dashboard示例项目。Anno 底层通讯采用 grpc、thrift。自带服务发现、调用链追踪、Cron 调度、限流、事件总线等等|541|C#|2021/11/25|
|73|[jm33-m0/kms-activate](https://github.com/jm33-m0/kms-activate)|Microsoft Windows/Office 一键激活工具|523|C#|2021/11/02|
|74|[AlphaYu/Adnc](https://github.com/AlphaYu/Adnc)|微服务框架，同时也适用于单体架构系统的开发。支持经典三层与DDD架构开发模式、集成了一系列主流稳定的微服务配套技术栈。一个前后端分离的框架，前端基于Vue、后端基于.Net 5.0构建。|522|C#|2021/11/23|
|75|[qq576067421/cshotfix](https://github.com/qq576067421/cshotfix)|C# HotFix C#热更新 unity3d 热更新|520|C#|2021/11/19|
|76|[foxofice/sub_share](https://github.com/foxofice/sub_share)|字幕共享计划|515|C#|2021/10/27|
|77|[yomunsam/TinaX](https://github.com/yomunsam/TinaX)|TinaX Framework : Unity based Game Client Framework   基于 Unity 的游戏客户端开发框架|513|C#|2021/11/12|
|78|[xiaoyaocz/biliuwp-lite](https://github.com/xiaoyaocz/biliuwp-lite)|哔哩哔哩UWP Lite|506|C#|2021/07/14|
|79|[luoyunchong/lin-cms-dotnetcore](https://github.com/luoyunchong/lin-cms-dotnetcore)|😃A simple and practical CMS implemented by .NET 6 + FreeSql；前后端分离、Docker部署、OAtuh2授权登录、自动化部署DevOps、自动同步至Gitee、代码生成器、仿掘金专栏|504|C#|2021/11/18|
|80|[zhontai/Admin.Core](https://github.com/zhontai/Admin.Core)|Admin后端，前后端分离的权限管理系统，集成统一认证授权、多租户、缓存、Ip限流、全Api鉴权。支持国内外主流数据库自由切换和动态高级查询，基于.Net跨平台开发的WebApi|498|C#|2021/11/16|
|81|[iccfish/FSLib.App.SimpleUpdater](https://github.com/iccfish/FSLib.App.SimpleUpdater)|A simple automatic update library for .net. 一个炒鸡简单但是足够好用的自动更新库。|478|C#|2021/11/18|
|82|[QAX-A-Team/sharpwmi](https://github.com/QAX-A-Team/sharpwmi)|sharpwmi是一个基于rpc的横向移动工具，具有上传文件和执行命令功能。|472|C#|2021/08/03|
|83|[jadepeng/XMusicDownloader](https://github.com/jadepeng/XMusicDownloader)|一款 支持从百度、网易、qq、酷狗、咪咕等音乐网站搜索并下载歌曲的程序，支持下载无损音乐|470|C#|2021/11/09|
|84|[zs9024/quick_psd2ugui](https://github.com/zs9024/quick_psd2ugui)|parse psd file and auto generate ui prefab（解析psd文件，一键生成ugui面板）|464|C#|2021/10/22|
|85|[91270/Meiam.System](https://github.com/91270/Meiam.System)|.NET 5 / .NET Core 3.1 WebAPI + Vue 2.0 + RBAC 企业级前后端分离权限框架|459|C#|2021/10/27|
|86|[HyPlayer/HyPlayer](https://github.com/HyPlayer/HyPlayer)|第三方网易云音乐播放器   A Netease Cloud Music Player|457|C#|2021/11/07|
|87|[YukiCoco/YukiDrive](https://github.com/YukiCoco/YukiDrive)|Onedrive & SharePoint 文件浏览网页程序|456|C#|2021/11/09|
|88|[donet5/WebFirst](https://github.com/donet5/WebFirst)|.NET CORE 代码生成器 ，Web中使用CodeFirst模式， 实体生成器，UI代码生成器，在线建表，导出文档，模版配置， WEB代码生成器 ，API代码生成器|455|C#|2021/11/18|
|89|[real-zony/ZonyLrcToolsX](https://github.com/real-zony/ZonyLrcToolsX)|ZonyLrcToolsX 是一个能够方便地下载歌词的小软件。|455|C#|2021/11/21|
|90|[SeriaWei/ASP.NET-MVC-CMS](https://github.com/SeriaWei/ASP.NET-MVC-CMS)|ZKEACMS，建网站就像搭积木一样|454|C#|2021/07/21|
|91|[lysilver/KopSoftWms](https://github.com/lysilver/KopSoftWms)|KopSoft仓库管理系统|448|C#|2021/07/09|
|92|[cuiliang/ClickShow](https://github.com/cuiliang/ClickShow)|鼠标点击特效|445|C#|2021/10/09|
|93|[zhaopeiym/IoTClient](https://github.com/zhaopeiym/IoTClient)|This is an IoT device communication protocol implementation client, which will include common industrial communication protocols such as mainstream PLC communication reading, ModBus protocol, and Bacnet protocol. This component is open source and free for life, using the most relaxed MIT open source ...|444|C#|2021/09/18|
|94|[trueai-org/module-shop](https://github.com/trueai-org/module-shop)|一个基于 .NET Core构建的简单、跨平台、模块化的商城系统|441|C#|2021/05/25|
|95|[qwqdanchun/DcRat](https://github.com/qwqdanchun/DcRat)|A simple remote tool written in C#.    一个简单的c#远控|440|C#|2021/11/05|
|96|[mai1zhi2/SharpBeacon](https://github.com/mai1zhi2/SharpBeacon)|CobaltStrike Beacon written in .Net 4  用.net重写了stager及Beacon，其中包括正常上线、文件管理、进程管理、令牌管理、结合SysCall进行注入、原生端口转发、关ETW等一系列功能|435|C#|2021/09/01|
|97|[XINCGer/ColaFrameWork](https://github.com/XINCGer/ColaFrameWork)|ColaFrameWork 一个Unity客户端框架|435|C#|2021/09/29|
|98|[CHKZL/DDTV_Old](https://github.com/CHKZL/DDTV_Old)|本项目已经停止维护，重构优化后项目发布为【DDTV】|432|C#|2021/10/18|
|99|[yuzd/Hangfire.HttpJob](https://github.com/yuzd/Hangfire.HttpJob)|httpjob for Hangfire,restful api for Hangfire,job调度与业务分离|431|C#|2021/09/14|
|100|[dathlin/HslControlsDemo](https://github.com/dathlin/HslControlsDemo)|HslControls控件库的使用demo，HslControls是一个工业物联网的控件库，基于C#开发，配套HslCommunication组件可以实现工业上位机软件的快速开发，支持常用的工业图形化控件，快速的集成界面开发。 主要包含了按钮，开关，进度条，信号灯，数码管，时钟，曲线显示控件，仪表盘控件，管道控件，瓶子控件，饼图控件，传送带控件，温度计控件，鼓风机控件，阀门控件，电池控件等等。|430|C#|2021/10/11|
|101|[yuzhengyang/Fork](https://github.com/yuzhengyang/Fork)|a c# utility library. C#工具包，C#工具类，常用方法，系统API，文件处理、加密解密、Winform美化（C# Tools）|426|C#|2021/11/26|
|102|[jerrytang67/SoMall](https://github.com/jerrytang67/SoMall)|社交电商商城开源项目.socail+mall即取名SoMall ,abp netcore 3.1 angular vue uni-app typescript docker mssql|425|C#|2021/10/07|
|103|[zhuxb711/RX-Explorer](https://github.com/zhuxb711/RX-Explorer)|一款优雅的UWP文件管理器   An elegant UWP Explorer|419|C#|2021/11/30|
|104|[CopyPlusPlus/CopyPlusPlus](https://github.com/CopyPlusPlus/CopyPlusPlus)|让复制变得更加简单！|413|C#|2021/11/22|
|105|[apolloconfig/apollo.net](https://github.com/apolloconfig/apollo.net)|Apollo配置中心.Net客户端|412|C#|2021/11/26|
|106|[uknowsec/SharpSQLTools](https://github.com/uknowsec/SharpSQLTools)|SharpSQLTools 和@Rcoil一起写的小工具，可上传下载文件，xp_cmdshell与sp_oacreate执行命令回显和clr加载程序集执行相应操作。|406|C#|2021/08/05|
|107|[X-Lucifer/AI-Lossless-Zoomer](https://github.com/X-Lucifer/AI-Lossless-Zoomer)|AI无损放大工具|401|C#|2021/10/03|
|108|[anjoy8/Blog.IdentityServer](https://github.com/anjoy8/Blog.IdentityServer)|🥗 打造一个功能强大的通用型Ids4用户认证授权服务中心，配合之前的所有开源项目|383|C#|2021/08/31|
|109|[dreamanlan/Cs2Lua](https://github.com/dreamanlan/Cs2Lua)|CSharp代码转lua，适用于使用lua实现热更新而又想有一个强类型检查的语言的场合|374|C#|2021/08/04|
|110|[colinin/abp-vue-admin-element-typescript](https://github.com/colinin/abp-vue-admin-element-typescript)|这是基于vue-admin ui适用于abp Vnext的前端管理项目，vben-admin的前端ui地址:https://github.com/colinin/vue-vben-admin-abp-vnext|374|C#|2021/12/01|
|111|[BladeMight/Mahou](https://github.com/BladeMight/Mahou)|Mahou(魔法) - The magic layout switcher.|369|C#|2021/11/30|
|112|[DaZiYuan/LiveWallpaper](https://github.com/DaZiYuan/LiveWallpaper)|A tiny win10 (dynamic) wallpaper changer   巨应壁纸   动态壁纸|368|C#|2021/10/24|
|113|[KumoKyaku/KCP](https://github.com/KumoKyaku/KCP)|KCP C#版。线程安全，运行时无alloc，对gc无压力。|365|C#|2021/10/06|
|114|[xuejmnet/sharding-core](https://github.com/xuejmnet/sharding-core)|high performance lightweight solution for efcore sharding table and sharding database support read-write-separation .一款ef-core下高性能、轻量级针对分表分库读写分离的解决方案，具有零依赖、零学习成本、零业务代码入侵|352|C#|2021/11/30|
|115|[yukuyoulei/ILRuntime_HotGames](https://github.com/yukuyoulei/ILRuntime_HotGames)|基于ILRuntime的热更新能力实现的可以直接使用的框架，友情赠送C# WebService + WebSocketServer服务器端。|350|C#|2021/10/19|
|116|[yswenli/SAEA](https://github.com/yswenli/SAEA)|SAEA.Socket is a high-performance IOCP framework TCP based on dotnet standard 2.0; Src contains its application test scenarios, such as websocket,rpc, redis driver, MVC WebAPI, lightweight message server, ultra large file transmission, etc. SAEA.Socket是一个高性能IOCP框架的 TCP，基于dotnet standard 2.0；Src中含有其应 ...|347|C#|2021/11/28|
|117|[NewLifeX/AntJob](https://github.com/NewLifeX/AntJob)|分布式任务调度系统，纯NET打造的重量级大数据实时计算平台，万亿级调度经验积累！面向中小企业大数据分析场景。|339|C#|2021/10/01|
|118|[pungin/Beanfun](https://github.com/pungin/Beanfun)|繽放 - 樂豆第三方客戶端|337|C#|2021/09/28|
|119|[Baidu-AIP/dotnet-sdk](https://github.com/Baidu-AIP/dotnet-sdk)|百度AI开放平台 .Net SDK|332|C#|2021/06/10|
|120|[SkyChenSky/Sikiro](https://github.com/SkyChenSky/Sikiro)|整合了社区主流开源框架（CAP、SkyAPM、WebAPIClient、Chloe等）的微服务项目示例|329|C#|2021/08/25|
|121|[CatImmortal/Trinity](https://github.com/CatImmortal/Trinity)|基于Unity的纯C#（客户端+服务端+热更新）游戏开发整合方案|327|C#|2021/08/07|
|122|[chatop2020/AKStream](https://github.com/chatop2020/AKStream)|AKStream是一套全平台(Linux,MacOS,Windows)、全架构(X86_64,Arm...)、全功能的流媒体管理控制接口平台。集成GB28181,RTSP,RTMP,HTTP等设备推拉流控制、PTZ控制、音视频文件录制管理、音视频文件裁剪合并等功能与一体|327|C#|2021/11/24|
|123|[SmallChi/JT808](https://github.com/SmallChi/JT808)|JT808协议、GB808协议、道路运输车辆卫星定位系统-北斗兼容车载终端通讯协议(支持2011、2013、2019版本)|324|C#|2021/11/18|
|124|[KevinWG/OSS.Clients.Pay](https://github.com/KevinWG/OSS.Clients.Pay)|开源支付SDK(标准库)，主要打造微信支付，支付宝支付，标准库项目，同时支持.net framework和.net core|323|C#|2021/10/18|
|125|[Senparc/Senparc.CO2NET](https://github.com/Senparc/Senparc.CO2NET)|支持 .NET Framework & .NET Core 的公共基础扩展库|321|C#|2021/11/26|
|126|[91270/Emby.MeiamSub](https://github.com/91270/Emby.MeiamSub)|Emby Server 端字幕插件 ，使用  迅雷影音、 射手网 接口精准匹配视频字幕|319|C#|2021/11/23|
|127|[GeorGeWzw/Sukt.Core](https://github.com/GeorGeWzw/Sukt.Core)|Sukt.Core 本项目是基于.Net Core开发的一个开源后台管理框架目前有以下模块      组织机构、菜单管理、按钮管理、用户管理、部门管理、角色管理、用户角色、角色权限、任务计划调度。|313|C#|2021/11/25|
|128|[fudiwei/DotNetCore.SKIT.FlurlHttpClient.Wechat](https://github.com/fudiwei/DotNetCore.SKIT.FlurlHttpClient.Wechat)|可能是目前全网最完整的 C# 版微信 SDK，封装全部已知的微信 API，支持公众平台（订阅号、服务号、小程序、小游戏、小商店）& 开放平台 & 商户平台（微信支付）& 企业微信（企业号）& 广告平台（广点通）& 对话开放平台（微信智能对话）等模块，可跨平台，持续随官方更新。欢迎 Star / Fork。|307|C#|2021/12/01|
|129|[focus-creative-games/luban](https://github.com/focus-creative-games/luban)|成熟完备灵活的游戏配置解决方案。支持常见的excel、json、xml、lua、yaml等等数据格式；支持导出binary、protobuf（同时生成schema定义文件）、msgpack、flatbuffers、json、xml、yaml格式数据，支持c++、java、c#、go、lua、javascript、typescript、python、erlang、rust 等等语言代码；实现了完善的本地化机制。a powerful game configure export tool and code generator that supports many common file format ...|304|C#|2021/11/30|
|130|[liaozb/APIJSON.NET](https://github.com/liaozb/APIJSON.NET)|后端接口自动化 .NET CORE版本|300|C#|2021/08/25|
|131|[Planshit/ProjectEye](https://github.com/Planshit/ProjectEye)|😎 一个基于20-20-20规则的用眼休息提醒Windows软件|297|C#|2021/11/19|
|132|[ixre/cms](https://github.com/ixre/cms)|DDD 开源.NET CMS、跨平台,  兼容.NET Core和ASP.NET,支持Docker容器|296|C#|2021/11/28|
|133|[cyq1162/Taurus.MVC](https://github.com/cyq1162/Taurus.MVC)|Taurus.mvc is a high-performance mvc and webapi framework for asp.net or asp.net core（适合场景：对性能和并发有较高要求的电商、站点、WebAPI等系统，支持.Net Core）|295|C#|2021/11/19|
|134|[duyanming/Anno.Core](https://github.com/duyanming/Anno.Core)|Anno 是一个微服务快速开发框架，底层通讯可以随意切换 grpc、thrift。自带服务发现、调用链追踪、Cron 调度、限流、事件总线、CQRS 、DDD、类似MVC的开发体验，插件化开发。 Anno is a microservices rapid development framework, the underlying communication can be arbitrary switch GRPC, thrift.Built-in service discovery, call chain tracking, Cron scheduling, current limiting, ...|292|C#|2021/11/30|
|135|[axzxs2001/Asp.NetCoreExperiment](https://github.com/axzxs2001/Asp.NetCoreExperiment)|原来所有项目都移动到**OleVersion**目录下进行保留。新的案例装以.net 5.0为主，一部分对以前案例进行升级，一部分将以前的工作经验总结出来，以供大家参考！|291|C#|2021/11/30|
|136|[ZetaSp/PowerToys-Chinese-TransMOD](https://github.com/ZetaSp/PowerToys-Chinese-TransMOD)|PowerToys Custom Simplified Chinese Translation Patch - 微软大力玩具自制简中翻译|287|C#|2021/10/31|
|137|[PxGame/XMLib.AM](https://github.com/PxGame/XMLib.AM)|XMLib 动作游戏开发套件|280|C#|2021/08/16|
|138|[skydevil88/XboxDownload](https://github.com/skydevil88/XboxDownload)|Xbox下载助手，支持Xbox、微软商店、PSN、NS、EA Desktop & Origin、战网国际服 下载加速。|280|C#|2021/11/20|
|139|[ldqk/Masuit.LuceneEFCore.SearchEngine](https://github.com/ldqk/Masuit.LuceneEFCore.SearchEngine)|基于EntityFrameworkCore和Lucene.NET实现的全文检索搜索引擎|277|C#|2021/11/11|
|140|[XIU2/TileTool](https://github.com/XIU2/TileTool)|🎨 Windows10 磁贴美化小工具|276|C#|2021/08/18|
|141|[microfisher/Simple-Web-Crawler](https://github.com/microfisher/Simple-Web-Crawler)|基于C#.NET的简单网页爬虫，支持异步并发、切换代理、操作Cookie、Gzip加速。|269|C#|2021/08/24|
|142|[WindFgg/kingfeng](https://github.com/WindFgg/kingfeng)|fork多了就跑路了|264|C#|2021/10/18|
|143|[anjoy8/Student.Achieve.Manager](https://github.com/anjoy8/Student.Achieve.Manager)|🎨学生教学教务管理系统：NetCore 3.1 + Vue + EleUI，（star100+，优化多表联查+录制视频+）|257|C#|2021/08/19|
|144|[EZhex1991/EZUnity](https://github.com/EZhex1991/EZUnity)|Unity3D工具集+API二次封装+编辑器扩展|251|C#|2021/10/03|
|145|[WeihanLi/DbTool](https://github.com/WeihanLi/DbTool)|数据库工具，根据表结构文档生成创建表sql，根据数据库表信息导出Model和表结构文档，根据文档生成数据库表，根据已有Model文件生成创建数据库表sql|250|C#|2021/10/09|
|146|[Scighost/KeqingNiuza](https://github.com/Scighost/KeqingNiuza)|刻记牛杂店|250|C#|2021/11/27|
|147|[StarEliteCore/Nebula.Admin](https://github.com/StarEliteCore/Nebula.Admin)|Destiny.Core.Flow是基于.Net Core，VUE前后分离，开发的一个开源Admin管理框架目前有以下模块：菜单管理、用户管理、角色管理、用户角色、角色权限等功能。|249|C#|2021/08/25|
|148|[superzhan/UnityBubbleGame](https://github.com/superzhan/UnityBubbleGame)|   BubbleGame《天天萌泡泡》是一款简单好玩的消除游戏，点击三个或以上同颜色、相连接的泡泡来进行消除，简单而有趣。|249|C#|2021/10/20|
|149|[overtly/core-grpc](https://github.com/overtly/core-grpc)|C# Grpc、Consul结合驱动封装，Overt.Core.Grpc基于Consul/Grpc实现服务注册服务发现，支持dotnetcore / framework，可快速实现基于Grpc的微服务；另外Overt.Core.Grpc.H2基于Consul/Grpc.Net实现服务注册服务发现，支持Net5.0， 内部有完整案例，包含服务端Server 客户端 Client，core+grpc, netcore+grpc, dotnetcore+grpc|248|C#|2021/09/22|
|150|[HMBSbige/BilibiliLiveRecordDownLoader](https://github.com/HMBSbige/BilibiliLiveRecordDownLoader)|Bilibili 直播录制与回放下载|247|C#|2021/12/01|
|151|[xin-lai/Magicodes.Pay](https://github.com/xin-lai/Magicodes.Pay)|Magicodes.Pay，是心莱科技团队提供的统一支付库，相关库均使用.NET标准库编写，支持.NET Framework以及.NET Core。目前已提供Abp模块的封装，支持开箱即用。|229|C#|2021/10/16|
|152|[pdone/FreeControl](https://github.com/pdone/FreeControl)|在PC上控制Android设备。|229|C#|2021/09/22|
|153|[NewLifeX/NewLife.Net](https://github.com/NewLifeX/NewLife.Net)|单机吞吐2266万tps的网络通信框架|228|C#|2021/07/31|
|154|[Azure99/GenshinPlayerQuery](https://github.com/Azure99/GenshinPlayerQuery)|Query genshin impact palyer info by uid. 识破云玩家 根据原神游戏内uid查询其他玩家详情(基础数据、角色详情&命座&装备、深境螺旋战绩&阵容等)|228|C#|2021/10/12|
|155|[jinglikeblue/Zero](https://github.com/jinglikeblue/Zero)|Zero是Unity中的游戏开发框架，为游戏开发核心问题提供轻量高效的解决方案|228|C#|2021/10/13|
|156|[aprilyush/EasyCMS](https://github.com/aprilyush/EasyCMS)|EasyCms基于Asp.net Core 的后台快速开发框架,内容管理系统|227|C#|2021/06/24|
|157|[ouyangzhaoxing/LiteREC](https://github.com/ouyangzhaoxing/LiteREC)|轻量、绿色便携的屏幕录制工具。(Lightweight portable screen recording tool.)|225|C#|2021/09/26|
|158|[NewLifeX/XCoder](https://github.com/NewLifeX/XCoder)|新生命码神工具，代码生成、网络工具、API工具、串口工具、正则工具、图标工具、加解密工具、地图接口。|224|C#|2021/09/25|
|159|[dreamanlan/CSharpGameFramework](https://github.com/dreamanlan/CSharpGameFramework)|基于unity3d引擎与c#语言的游戏框架/架构（包括客户端与服务器）。使用ServerPlatform作为服务端通信基础设施。|218|C#|2021/10/20|
|160|[haifengat/pyctp](https://github.com/haifengat/pyctp)|上期技术期货交易api之python封装，实现接口调用。支持windows linux.|215|C#|2021/10/03|
|161|[xiajingren/HelloAbp](https://github.com/xiajingren/HelloAbp)|ABP vNext + vue-element-admin入门级项目实战|215|C#|2021/08/01|
|162|[EasyAbp/Abp.WeChat](https://github.com/EasyAbp/Abp.WeChat)|Abp 微信 SDK 模块，包含对微信小程序、公众号、企业微信、开放平台、第三方平台等相关接口封装。|214|C#|2021/10/17|
|163|[qingfeng346/Scorpio-CSharp](https://github.com/qingfeng346/Scorpio-CSharp)|Unity游戏热更新脚本|205|C#|2021/11/29|
|164|[xyfll7/CV-translation](https://github.com/xyfll7/CV-translation)|我叫CV翻译，因为我的精髓就是Ctrl + c 不用v  （原名QTranser）|202|C#|2021/08/05|
|165|[yswenli/WebRedisManager](https://github.com/yswenli/WebRedisManager)|WebRedis Manager is a simple management to implement Redis using SAEA. RedisSocket, SAEA.MVC and running speed quickly.WebRedisManager是使用的SAEA.RedisSocket、SAEA.MVC等实现Redis的简便管理功能，轻松运行~ |200|C#|2021/11/28|
|166|[sunbrando/ParticleEffectProfiler](https://github.com/sunbrando/ParticleEffectProfiler)|Unity 特效性能分析工具|196|C#|2021/09/25|
|167|[JeffreySu/WxOpen](https://github.com/JeffreySu/WxOpen)|微信小程序 C# SDK，Senparc.Weixin.WxOpen.dll|194|C#|2021/09/04|
|168|[qwqcode/SubRenamer](https://github.com/qwqcode/SubRenamer)|🎞 番剧字幕文件自动化一键批量重命名（改名）工具：自动匹配，正则过滤，手动精选，一键操作；Automatically and easily Match and Rename Subtitles for Videos through the SubRenamer (VideoRenamer).|194|C#|2021/05/23|
|169|[overtly/core-data](https://github.com/overtly/core-data)|基于Dapper封装的Linq表达式数据库访问驱动，内置自定义分库，分表的实现|186|C#|2021/09/28|
|170|[qwqcode/Nacollector](https://github.com/qwqcode/Nacollector)|⚔ 一个采集工具箱，据说是一个用于采集各种 WEB 资源的工作站？！你可以认为这是一个框架，可拓展。淘宝、天猫、苏宁、国美 等电商平台数据采集... 一键邀请 一键打包 账号登录获取Cookie 任务多线程 下载内容管理 实时日志 dll 热更新 无边框窗体 Web App, CefSharp, WebDriver|180|C#|2021/05/23|
|171|[yanghuan/CSharpLuaForUnity](https://github.com/yanghuan/CSharpLuaForUnity)|CSharp.lua的Unity适配，可将Unity工程中的C#代码编译至Lua|180|C#|2021/09/06|
|172|[simple-phil/ketchup](https://github.com/simple-phil/ketchup)|ketchup （番茄酱） 是一个基于dotnet core的微服务框架。|179|C#|2021/10/07|
|173|[WeihanLi/WeihanLi.Common](https://github.com/WeihanLi/WeihanLi.Common)|common tools, methods, extension methods, etc...  .net 常用工具类，公共方法，常用扩展方法等，基础类库|176|C#|2021/11/30|
|174|[ZHOURUIH/MyFramework](https://github.com/ZHOURUIH/MyFramework)|一个在unity上使用的网络游戏客户端开发框架,为unity所有使用方式提供完善的封装和管理,只需要专注于游戏逻辑的编写|174|C#|2021/08/23|
|175|[poerin/Stroke](https://github.com/poerin/Stroke)|鼠标手势（Mouse Gestures）|174|C#|2021/09/27|
|176|[chi8708/NBCZ_Admin](https://github.com/chi8708/NBCZ_Admin)|asp.net MVC5 + Dapper + layUI/easyUI 通用权限管理系统、后台框架、信息管理系统基础框架|172|C#|2021/09/17|
|177|[yswenli/GFF](https://github.com/yswenli/GFF)|GFF is a imitation QQ communication project, based on high IOCP. GFF是模仿QQ通讯项目，通信基于SAEA.MessageSocket、SAEA.Http、SAEA.MVC实现|172|C#|2021/11/28|
|178|[yangan666/SuperNAT](https://github.com/yangan666/SuperNAT)|SuperNAT是基于.NET 5.0开源跨平台的内网穿透程序，功能类似花生壳，可用于穿透内网web应用，微信公众号本地调试等，目前支持http穿透，tcp穿透，websocket穿透。QQ交流群：854594944|169|C#|2021/09/09|
|179|[YukiCoco/CheapSteam](https://github.com/YukiCoco/CheapSteam)|Cheap Steam 为您提供 STEAM 与 BUFF 的饰品价格对比数据.|167|C#|2021/08/21|
|180|[NewLifeX/NewLife.RocketMQ](https://github.com/NewLifeX/NewLife.RocketMQ)|纯托管轻量级RocketMQ客户端，支持发布消息、消费消息、负载均衡等核心功能！|164|C#|2021/11/18|
|181|[jellydong/LJDAPP](https://github.com/jellydong/LJDAPP)|基于ASP.Net Core开发一套通用后台框架|164|C#|2021/10/08|
|182|[li-zheng-hao/StickyNotes](https://github.com/li-zheng-hao/StickyNotes)|一个便捷的Windows桌面便利贴/A convenitent Windows Notes |163|C#|2021/07/24|
|183|[zmjack/Chinese](https://github.com/zmjack/Chinese)|中文解析通用工具。包括拼音，简繁转换，数字读法，货币读法。|161|C#|2021/05/25|
|184|[evilashz/SharpADUserIP](https://github.com/evilashz/SharpADUserIP)|提取DC日志，快速获取域用户对应IP地址|159|C#|2021/09/28|
|185|[CoreUnion/CoreShop](https://github.com/CoreUnion/CoreShop)|基于 Asp.Net Core 5.0、Uni-App开发，支持可视化布局的小程序商城系统，前后端分离，支持分布式部署，跨平台运行，拥有分销、代理、团购、拼团、秒杀、直播、优惠券、自定义表单等众多营销功能，拥有完整SKU、下单、售后、物流流程。支持一套代码编译发布微信小程序版、H5版、Android版、iOS版、支付宝小程序版、字节跳动小程序版、QQ小程序版等共10个平台。|158|C#|2021/11/30|
|186|[871041532/zstring](https://github.com/871041532/zstring)|C# string零GC补充方案|155|C#|2021/09/01|
|187|[xplusky/MoeLoaderP](https://github.com/xplusky/MoeLoaderP)|🖼二次元图片下载器 Pics downloader for Pixiv.net,Bilibili.com,Konachan.com,Yande.re , behoimi.org, safebooru.org, danbooru.donmai.us,Gelbooru,SankakuComplex,Kawainyan,MiniTokyo,e-shuushuu.net,Zerochan,WorldCosplay ,Yuriimgetc.|154|C#|2021/11/15|
|188|[yuzd/Autofac.Annotation](https://github.com/yuzd/Autofac.Annotation)|Autofac extras library for component registration via attributes 用注解来load autofac 摆脱代码或者xml配置和java的spring的注解注入一样的体验|153|C#|2021/07/18|
|189|[An0nySec/ShadowUser](https://github.com/An0nySec/ShadowUser)|影子用户 克隆|150|C#|2021/08/06|
|190|[mrhuo/MrHuo.OAuth](https://github.com/mrhuo/MrHuo.OAuth)|.netcore 下最好用的第三方登录组件集合，集成了国内外大部分平台，欢迎使用。|148|C#|2021/07/04|
|191|[TheKingOfDuck/MatryoshkaDollTool](https://github.com/TheKingOfDuck/MatryoshkaDollTool)|MatryoshkaDollTool-程序加壳/捆绑工具|147|C#|2021/08/26|
|192|[RayWangQvQ/RayPI](https://github.com/RayWangQvQ/RayPI)|一个基于.NET Core 3.1的DDD（领域驱动）的极简风WebApi开发框架。|146|C#|2021/08/25|
|193|[AlifeLine/Emby.Plugins.Douban](https://github.com/AlifeLine/Emby.Plugins.Douban)|Emby的豆瓣削刮器|146|C#|2021/08/21|
|194|[0x727/SchTask_0x727](https://github.com/0x727/SchTask_0x727)|创建隐藏计划任务，权限维持，Bypass AV|142|C#|2021/09/01|
|195|[KevinWG/OSS.Clients.SNS](https://github.com/KevinWG/OSS.Clients.SNS)|社交网站sdk(标准库)，微信公众号（订阅号，服务号，小程序）接口sdk-包含消息回复(明文和安全模式)，Oauth2.0授权等|139|C#|2021/10/19|
|196|[garsonlab/GText](https://github.com/garsonlab/GText)|Emoji and Hyperlink for Unity UGUI Text ,图文混排、超链接、下划线的UGUI解决方案|139|C#|2021/05/28|
|197|[chenxuuu/llcom](https://github.com/chenxuuu/llcom)|🛠可运行lua脚本辅助调试的串口调试助手Serial debugger, with lua scripts.|137|C#|2021/12/01|
|198|[fuluteam/ICH.Snowflake](https://github.com/fuluteam/ICH.Snowflake)|雪花分布式id的.net core实现方案。支持k8s等分布式场景部署。|135|C#|2021/11/25|
|199|[jarjin/FinalFramework](https://github.com/jarjin/FinalFramework)|为独立游戏而生，Gameplay热更新游戏框架。（对初学者不友好）|135|C#|2021/06/03|
|200|[yfl8910/DataPie](https://github.com/yfl8910/DataPie)|EXCEL导入、导出、存储过程运算工具：DataPie（支持MS SQL、ACCESS 、SQLite）|134|C#|2021/11/28|

⬆ [回到目录](#内容目录)

<br/>

## HTML

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[QSCTech/zju-icicles](https://github.com/QSCTech/zju-icicles)|浙江大学课程攻略共享计划|23.5k|HTML|2021/11/29|
|2|[fengdu78/Coursera-ML-AndrewNg-Notes](https://github.com/fengdu78/Coursera-ML-AndrewNg-Notes)|吴恩达老师的机器学习课程个人笔记|23.5k|HTML|2021/07/24|
|3|[PKUanonym/REKCARC-TSC-UHT](https://github.com/PKUanonym/REKCARC-TSC-UHT)|清华大学计算机系课程攻略 Guidance for courses in Department of Computer Science and Technology, Tsinghua University|19.2k|HTML|2021/10/12|
|4|[TeamStuQ/skill-map](https://github.com/TeamStuQ/skill-map)|程序员技能图谱|18.5k|HTML|2021/06/25|
|5|[nndl/nndl.github.io](https://github.com/nndl/nndl.github.io)|《神经网络与深度学习》 邱锡鹏著 Neural Network and Deep Learning |15.0k|HTML|2021/05/18|
|6|[fengdu78/deeplearning_ai_books](https://github.com/fengdu78/deeplearning_ai_books)|deeplearning.ai（吴恩达老师的深度学习课程笔记及资源）|13.9k|HTML|2021/10/23|
|7|[wowchemy/wowchemy-hugo-themes](https://github.com/wowchemy/wowchemy-hugo-themes)|🔥 Hugo website builder, Hugo themes & Hugo CMS. No code, build with widgets! 创建在线课程，学术简历或初创网站。|6.1k|HTML|2021/11/29|
|8|[javascript-tutorial/zh.javascript.info](https://github.com/javascript-tutorial/zh.javascript.info)|现代 JavaScript 教程（The Modern JavaScript Tutorial）|5.5k|HTML|2021/11/29|
|9|[paddingme/Front-end-Web-Development-Interview-Question](https://github.com/paddingme/Front-end-Web-Development-Interview-Question)|前端开发面试题大收集，前端面试集锦 :heart: :gift_heart: :cupid:|5.0k|HTML|2021/10/13|
|10|[me115/linuxtools_rst](https://github.com/me115/linuxtools_rst)|Linux工具快速教程|4.8k|HTML|2021/08/16|
|11|[zhangkaitao/shiro-example](https://github.com/zhangkaitao/shiro-example)|跟我学Shiro（我的公众号：kaitao-1234567，我的新书：《亿级流量网站架构核心技术》） |4.8k|HTML|2021/09/01|
|12|[itorr/nbnhhsh](https://github.com/itorr/nbnhhsh)|😩「能不能好好说话？」 拼音首字母缩写翻译工具|4.5k|HTML|2021/11/18|
|13|[doocs/technical-books](https://github.com/doocs/technical-books)|😆 国内外互联网技术大牛们都写了哪些书籍：计算机基础、网络、前端、后端、数据库、架构、大数据、深度学习...|3.9k|HTML|2021/10/24|
|14|[gwuhaolin/dive-into-webpack](https://github.com/gwuhaolin/dive-into-webpack)|全面的Webpack教程《深入浅出Webpack》电子书|3.7k|HTML|2021/10/27|
|15|[biaochenxuying/blog](https://github.com/biaochenxuying/blog)|大前端技术为主，读书笔记、随笔、理财为辅，做个终身学习者。|3.1k|HTML|2021/10/11|
|16|[zfaka-plus/zfaka](https://github.com/zfaka-plus/zfaka)|免费、安全、稳定、高效的发卡系统，值得拥有!|2.9k|HTML|2021/09/20|
|17|[golang101/golang101](https://github.com/golang101/golang101)|Go语言101 : 一本侧重于Go语言语法和语义的编程解释和指导书|2.8k|HTML|2021/11/17|
|18|[eddycjy/blog](https://github.com/eddycjy/blog)|煎鱼的博客，有点忙，传送门：https://eddycjy.com|2.8k|HTML|2021/11/18|
|19|[refscn/rplibs](https://github.com/refscn/rplibs)|Refs.cn 原型设计元件库，基于Axure RP 10/9/8，支持 Android、Apple、Windows、微信，移动、桌面平台的应用和网站原型设计。五年历程 2.6k+ star，感谢大家使用。|2.6k|HTML|2021/05/27|
|20|[HiddenStrawberry/Crawler_Illegal_Cases_In_China](https://github.com/HiddenStrawberry/Crawler_Illegal_Cases_In_China)|Collection of China illegal cases about web crawler 本项目用来整理所有中国大陆爬虫开发者涉诉与违规相关的新闻、资料与法律法规。致力于帮助在中国大陆工作的爬虫行业从业者了解我国相关法律，避免触碰数据合规红线。 [AD]中文知识图谱门户 |2.4k|HTML|2021/10/28|
|21|[xxlllq/system_architect](https://github.com/xxlllq/system_architect)|:100:高质量、最全面的系统架构设计师（软考高级）备考资源库。备考QQ群：系统架构设计师（⑥群：1009309733），系统分析师（②群：699260558），信息系统项目管理师（②群：785329399）。|2.4k|HTML|2021/11/29|
|22|[kenzok8/openwrt-packages](https://github.com/kenzok8/openwrt-packages)|openwrt常用软件包|2.3k|HTML|2021/11/28|
|23|[phodal/designiot](https://github.com/phodal/designiot)|教你设计物联网系统。构建自己的Internet of Things 。|2.0k|HTML|2021/06/25|
|24|[kujian/frontendDaily](https://github.com/kujian/frontendDaily)|前端开发博客，分享互联网最精彩的前端技术，欢迎关注我微信公众号：前端开发博客，回复 1024，领取前端进阶资料包，回复 加群，与大神一起交流学习。|1.9k|HTML|2021/12/01|
|25|[demopark/electron-api-demos-Zh_CN](https://github.com/demopark/electron-api-demos-Zh_CN)|这是 electron-api-demos 的中文版本, 更新至 v2.0.2|1.8k|HTML|2021/10/13|
|26|[huyaocode/webKnowledge](https://github.com/huyaocode/webKnowledge)|前端知识点总结|1.8k|HTML|2021/05/06|
|27|[F4bwDP6a6W/FLY_US](https://github.com/F4bwDP6a6W/FLY_US)|美国大学备考资料 How to apply US colleges|1.7k|HTML|2021/11/26|
|28|[esofar/cnblogs-theme-silence](https://github.com/esofar/cnblogs-theme-silence)|📖 一款专注于阅读的博客园主题|1.6k|HTML|2021/09/10|
|29|[godbasin/godbasin.github.io](https://github.com/godbasin/godbasin.github.io)|被删前端博客--喜欢请star|1.6k|HTML|2021/11/28|
|30|[Ehco1996/Python-crawler](https://github.com/Ehco1996/Python-crawler)|从头开始 系统化的 学习如何写Python爬虫。 Python版本 3.6 |1.6k|HTML|2021/09/02|
|31|[tencentyun/qcloud-documents](https://github.com/tencentyun/qcloud-documents)|腾讯云官方文档|1.6k|HTML|2021/12/01|
|32|[yangkun19921001/Blog](https://github.com/yangkun19921001/Blog)|Android 面试宝典、数据结构和算法、音视频 (FFmpeg、AAC、x264、MediaCodec)、 C/C++ 、OpenCV、跨平台等学习记录。【0基础音视频进阶学习路线】|1.5k|HTML|2021/10/17|
|33|[yangzongzhuan/RuoYi](https://github.com/yangzongzhuan/RuoYi)|:tada: (RuoYi)官方仓库 基于SpringBoot的权限管理系统 易读易懂、界面简洁美观。 核心技术采用Spring、MyBatis、Shiro没有任何其它重度依赖。直接运行即可用|1.5k|HTML|2021/11/25|
|34|[wx-chevalier/Awesome-CS-Books-and-Digests](https://github.com/wx-chevalier/Awesome-CS-Books-and-Digests)|:books: Awesome CS Books(with Digests)/Series(.pdf by git lfs) Warehouse for Geeks, ProgrammingLanguage, SoftwareEngineering, Web, AI, ServerSideApplication, Infrastructure, FE etc. :dizzy: 优秀计算机科学与技术领域相关的书籍归档，以及我的读书笔记。|1.4k|HTML|2021/11/24|
|35|[haixiangyan/one-day-one-npm-lib](https://github.com/haixiangyan/one-day-one-npm-lib)|🔥 🔥 这本小书会带你造 10 个非常实用的 npm 库 📦|1.4k|HTML|2021/09/10|
|36|[TransparentLC/WechatMomentScreenshot](https://github.com/TransparentLC/WechatMomentScreenshot)|朋友圈转发截图生成工具|1.4k|HTML|2021/11/16|
|37|[feiyu563/PrometheusAlert](https://github.com/feiyu563/PrometheusAlert)|Prometheus Alert是开源的运维告警中心消息转发系统,支持主流的监控系统Prometheus,Zabbix,日志系统Graylog和数据可视化系统Grafana发出的预警消息,支持钉钉,微信,华为云短信,腾讯云短信,腾讯云电话,阿里云短信,阿里云电话等|1.3k|HTML|2021/11/23|
|38|[wx-chevalier/DistributedSystem-Series](https://github.com/wx-chevalier/DistributedSystem-Series)|:books: 深入浅出分布式基础架构，Linux 与操作系统篇   分布式系统篇   分布式计算篇   数据库篇   网络篇   虚拟化与编排篇   大数据与云计算篇|1.3k|HTML|2021/11/28|
|39|[PeiQi0/PeiQi-WIKI-POC](https://github.com/PeiQi0/PeiQi-WIKI-POC)|鹿不在侧，鲸不予游🐋|1.2k|HTML|2021/07/04|
|40|[HZFE/awesome-interview](https://github.com/HZFE/awesome-interview)|剑指前端 Offer|1.2k|HTML|2021/11/20|
|41|[mzlogin/mzlogin.github.io](https://github.com/mzlogin/mzlogin.github.io)|Jekyll Themes / GitHub Pages 博客模板 / A template repository for Jekyll based blog|1.2k|HTML|2021/12/01|
|42|[SolidZORO/zpix-pixel-font](https://github.com/SolidZORO/zpix-pixel-font)|Zpix (最像素) is a pixel font supporting English, Traditional Chinese, Simplified Chinese and Japanese.|1.2k|HTML|2021/11/20|
|43|[gh0stkey/Web-Fuzzing-Box](https://github.com/gh0stkey/Web-Fuzzing-Box)|Web Fuzzing Box - Web 模糊测试字典与一些Payloads，主要包含：弱口令暴力破解、目录以及文件枚举、Web漏洞...字典运用于实战案例：https://gh0st.cn/archives/2019-11-11/1|1.1k|HTML|2021/10/22|
|44|[shengxinjing/my_blog](https://github.com/shengxinjing/my_blog)|:snail:写一点博客，python web 前端 运维|1.1k|HTML|2021/10/06|
|45|[TurboWay/big_screen](https://github.com/TurboWay/big_screen)|数据大屏可视化|1.1k|HTML|2021/06/04|
|46|[UmeLabs/node.umelabs.dev](https://github.com/UmeLabs/node.umelabs.dev)|每天24:00点前更新免费SS/SSR节点|991|HTML|2021/11/30|
|47|[TIM168/technical_books](https://github.com/TIM168/technical_books)|:books:🔥收集全网最热门的技术书籍 (GO、黑客、Android、计算机原理、人工智能、大数据、机器学习、数据库、PHP、java、架构、消息队列、算法、python、爬虫、操作系统、linux、C语言)，不间断更新中:hotsprings:|969|HTML|2021/06/07|
|48|[netkiller/netkiller.github.io](https://github.com/netkiller/netkiller.github.io)|Netkiller Free ebook - 免费电子书|940|HTML|2021/11/19|
|49|[stellarkey/912_project](https://github.com/stellarkey/912_project)|清华大学计算机系考研攻略 Guidance for postgraduate entrance examination in Department of Computer Science and Technology, Tsinghua University|902|HTML|2021/08/21|
|50|[Jocs/jocs.github.io](https://github.com/Jocs/jocs.github.io)|💯Jocs 的个人博客，所有的文章都在 issues 里面|893|HTML|2021/08/13|
|51|[QianMo/Real-Time-Rendering-4th-Bibliography-Collection](https://github.com/QianMo/Real-Time-Rendering-4th-Bibliography-Collection)|Real-Time Rendering 4th (RTR4) 参考文献合集典藏   Collection of <Real-Time Rendering 4th (RTR4)> Bibliography / Reference|893|HTML|2021/11/01|
|52|[aisuda/amis-admin](https://github.com/aisuda/amis-admin)|基于 amis 的后台项目前端模板|846|HTML|2021/11/25|
|53|[FoXZilla/Pxer](https://github.com/FoXZilla/Pxer)|A tool for pixiv.net. 人人可用的P站爬虫|822|HTML|2021/08/11|
|54|[helloxz/ccaa](https://github.com/helloxz/ccaa)|Linux一键安装Aria2 + AriaNg + FileBrowse实现离线下载、文件管理。|820|HTML|2021/05/07|
|55|[lyy289065406/re0-web](https://github.com/lyy289065406/re0-web)|Re0：从零开始的异世界生活 （WEB版）|811|HTML|2021/11/21|
|56|[HuangCongQing/UCAS_Course_2019](https://github.com/HuangCongQing/UCAS_Course_2019)|中国科学院大学2019-2020课程（秋季，春季，夏季）|808|HTML|2021/08/24|
|57|[zhengmin1989/MyArticles](https://github.com/zhengmin1989/MyArticles)|蒸米的文章（iOS冰与火之歌系列，一步一步学ROP系列，安卓动态调试七种武器系列等）|805|HTML|2021/08/12|
|58|[pingan8787/Leo-JavaScript](https://github.com/pingan8787/Leo-JavaScript)|欢迎关注公众号“前端自习课”，本仓库包含丰富的 JavaScript 学习资料，包括 JavaScript、前端框架、HTTP、GraphQL、TS、Webpack等，还有很多我的原创文章，喜欢的朋友欢迎stat。:rocket:持续更新中...|766|HTML|2021/10/30|
|59|[NICEXAI/leaflet_zh](https://github.com/NICEXAI/leaflet_zh)|Leaflet中文文档|763|HTML|2021/11/28|
|60|[BaizeSec/bylibrary](https://github.com/BaizeSec/bylibrary)|白阁文库是白泽Sec安全团队维护的一个漏洞POC和EXP公开项目|705|HTML|2021/10/11|
|61|[lzjun567/python_scripts](https://github.com/lzjun567/python_scripts)|一些python相关的演示代码|698|HTML|2021/10/02|
|62|[fwonggh/Bthub](https://github.com/fwonggh/Bthub)|Bthub最新地址发布页|694|HTML|2021/09/17|
|63|[dennis-jiang/Front-End-Knowledges](https://github.com/dennis-jiang/Front-End-Knowledges)|前端知识进阶|676|HTML|2021/10/22|
|64|[Coq-zh/SF-zh](https://github.com/Coq-zh/SF-zh)|《软件基础》中译版 Software Foundations Chinese Translation|666|HTML|2021/11/02|
|65|[unclestrong/DeepLearning_LHY21_Notes](https://github.com/unclestrong/DeepLearning_LHY21_Notes)|深度学习 李宏毅 2021 学习笔记|657|HTML|2021/10/30|
|66|[lucifersun/China-Telecom-ShangHai-IPTV-list](https://github.com/lucifersun/China-Telecom-ShangHai-IPTV-list)|上海电信IPTV播放地址列表|655|HTML|2021/11/08|
|67|[wx-chevalier/ProgrammingLanguage-Series](https://github.com/wx-chevalier/ProgrammingLanguage-Series)|:books: 编程语言语法基础与工程实践，JavaScript   Java   Python   Go   Rust   CPP   Swift|652|HTML|2021/11/02|
|68|[koala-coding/goodBlog](https://github.com/koala-coding/goodBlog)|我是koala, 公众号【程序员成长指北】的作者， 专注Node.js技术栈分享，从前端到Node.js再到后端数据库，帮您成为优秀的Node.js全栈工程师。和我一起进阶全栈吧！|636|HTML|2021/10/07|
|69|[wx-chevalier/Web-Series](https://github.com/wx-chevalier/Web-Series)|:books: 现代 Web 开发语法基础与工程实践，涵盖 Web 开发基础、前端工程化、应用架构、性能与体验优化、混合开发、React 实践、Vue 实践、WebAssembly 等多方面。|634|HTML|2021/11/28|
|70|[tanjiti/sec_profile](https://github.com/tanjiti/sec_profile)|爬取secwiki和xuanwu.github.io/sec.today,分析安全信息站点、安全趋势、提取安全工作者账号(twitter,weixin,github等)|616|HTML|2021/11/30|
|71|[NJU-SE-15-share-review/professional-class](https://github.com/NJU-SE-15-share-review/professional-class)|NJUSE-专业课|614|HTML|2021/10/29|
|72|[cnych/qikqiak.com](https://github.com/cnych/qikqiak.com)|关注容器、kubernetes、devops、python、golang、微服务等技术 🎉🎉🎉|606|HTML|2021/11/10|
|73|[pengan1987/computer-museum-dnbwg](https://github.com/pengan1987/computer-museum-dnbwg)|电脑博物馆 - compumuseum.com|593|HTML|2021/11/27|
|74|[justjavac/justjavac.github.com](https://github.com/justjavac/justjavac.github.com)|🇨🇳个人博客，喜欢的话请点 star，想订阅点 watch :sparkles: |592|HTML|2021/09/28|
|75|[wx-chevalier/Database-Series](https://github.com/wx-chevalier/Database-Series)|📚深入浅出数据库存储：数据库理论、关系型数据库、文档型数据库、键值型数据库、New SQL、搜索引擎、数据仓库与 OLAP、大数据与数据中台|591|HTML|2021/11/06|
|76|[ningbonb/HTML5](https://github.com/ningbonb/HTML5)|HTML5学习、总结、实践|533|HTML|2021/05/23|
|77|[wx-chevalier/Awesome-Lists](https://github.com/wx-chevalier/Awesome-Lists)|:books: Guide to Galaxy, curated, worthy and up-to-date links/reading list for ITCS-Coding/Algorithm/SoftwareArchitecture/AI.  :dizzy: ITCS-编程/算法/软件架构/人工智能等领域的文章/书籍/资料/项目链接精选，岁月沉淀的美好|528|HTML|2021/11/28|
|78|[asyncins/antispider](https://github.com/asyncins/antispider)|书籍《Python3 反爬虫原理与绕过实战》配套代码|515|HTML|2021/10/25|
|79|[w3c/chinese-ig](https://github.com/w3c/chinese-ig)|Web中文兴趣组|512|HTML|2021/11/17|
|80|[roy-tian/learning-area](https://github.com/roy-tian/learning-area)|MDN 学习区示例中文版|496|HTML|2021/05/30|
|81|[nestcn/docs.nestjs.cn](https://github.com/nestcn/docs.nestjs.cn)|nestjs 中文文档|494|HTML|2021/11/30|
|82|[openwhu/OpenWHU](https://github.com/openwhu/OpenWHU)|武汉大学课程资料整理-WHU课代表计划|493|HTML|2021/10/25|
|83|[cncounter/translation](https://github.com/cncounter/translation)|翻译文档|480|HTML|2021/12/01|
|84|[arunboy/love](https://github.com/arunboy/love)|表白网站|476|HTML|2021/05/19|
|85|[TZG-official/Jvav](https://github.com/TZG-official/Jvav)|J v a v 与 您|472|HTML|2021/10/09|
|86|[wardseptember/notes](https://github.com/wardseptember/notes)|算法刷题指南、Java多线程与高并发、Java集合源码、Spring boot、Spring Cloud等笔记，源码级学习笔记后续也会更新。|470|HTML|2021/08/31|
|87|[click33/Sa-Admin](https://github.com/click33/Sa-Admin)|一个无需脚手架即可直接运行的后台模板，流畅、易上手、提高生产力|445|HTML|2021/10/25|
|88|[ojeveryday/AlgoWiki](https://github.com/ojeveryday/AlgoWiki)|总结算法刷题套路，在线阅读：|437|HTML|2021/11/30|
|89|[ffffffff0x/AboutSecurity](https://github.com/ffffffff0x/AboutSecurity)|Everything for pentest.   用于渗透测试的 payload 和 bypass 字典.|434|HTML|2021/11/25|
|90|[oneStarLR/myblog-mybatis](https://github.com/oneStarLR/myblog-mybatis)|个人博客系统（SpringBoot+Mybatis）|427|HTML|2021/08/10|
|91|[daacheng/PythonBasic](https://github.com/daacheng/PythonBasic)|平时工作中常用的Python零碎知识总结，爬虫学习总结与练习，Python数据分析学习总结，目前正在重新整理中......|416|HTML|2021/09/28|
|92|[aui/pinyin-engine](https://github.com/aui/pinyin-engine)|JavaScript 拼音匹配引擎|409|HTML|2021/09/06|
|93|[OpenIoTHub/OpenIoTHub](https://github.com/OpenIoTHub/OpenIoTHub)|💖A free IoT (Internet of Things)  platform and private cloud. [一个免费的物联网和私有云平台，支持内网穿透]|409|HTML|2021/10/12|
|94|[cckuailong/vulbase](https://github.com/cckuailong/vulbase)|各大漏洞文库合集|407|HTML|2021/10/05|
|95|[holylovelqq/vue-unit-test-with-jest](https://github.com/holylovelqq/vue-unit-test-with-jest)|吃透本仓库，变身vue项目单体测试大神|406|HTML|2021/11/17|
|96|[geeeeeeeek/videoproject](https://github.com/geeeeeeeek/videoproject)|基于python/django的视频点播网站|404|HTML|2021/09/08|
|97|[apachecn/fastai-ml-dl-notes-zh](https://github.com/apachecn/fastai-ml-dl-notes-zh)|:book:  [译] fast.ai 机器学习和深度学习中文笔记|382|HTML|2021/08/13|
|98|[r00tSe7en/Flash-Pop](https://github.com/r00tSe7en/Flash-Pop)|Flash钓鱼弹窗优化版|382|HTML|2021/10/22|
|99|[noahlam/articles](https://github.com/noahlam/articles)|个人技术帖合集|369|HTML|2021/06/05|
|100|[momo0853/kkndme](https://github.com/momo0853/kkndme)|kkndme聊房，数据整理自天涯。提供HTML、PDF和Markdown三种形式。|365|HTML|2021/11/24|
|101|[iamjoel/front-end-note](https://github.com/iamjoel/front-end-note)|:memo: Web前端洞见。有深度的Web 前端内容。|364|HTML|2021/09/01|
|102|[Ed1s0nZ/cool](https://github.com/Ed1s0nZ/cool)|Golang-Gin 框架写的免杀平台，内置分离、捆绑等多种BypassAV方式。|356|HTML|2021/11/25|
|103|[SummerSec/JavaLearnVulnerability](https://github.com/SummerSec/JavaLearnVulnerability)|Java漏洞学习笔记 Deserialization Vulnerability|355|HTML|2021/11/30|
|104|[xizhibei/blog](https://github.com/xizhibei/blog)|个人博客，(Node.js/Golang/Backend/DevOps)，欢迎 Star, Watch 订阅以及评论|354|HTML|2021/08/30|
|105|[shengxinjing/vue3-vs-vue2](https://github.com/shengxinjing/vue3-vs-vue2)|《前端会客厅第一期代码》和尤大聊vue3的 提升|350|HTML|2021/11/04|
|106|[Liberxue/liberxue.github.io](https://github.com/Liberxue/liberxue.github.io)|Liberxue blog for lightweight Jekyll  themes  轻量级自适应 简洁 卡片式博客主题 3秒搞定GitHub blog|341|HTML|2021/07/13|
|107|[netnr/kms](https://github.com/netnr/kms)|KMS激活服务，slmgr命令激活Windows系统、Office|339|HTML|2021/11/17|
|108|[wx-chevalier/System-Series](https://github.com/wx-chevalier/System-Series)|:books: 服务端开发实践与工程架构，服务端基础篇   微服务与云原生篇   Spring 篇   Node.js 篇   DevOps 篇   信息安全与渗透测试篇|336|HTML|2021/11/28|
|109|[jjeejj/geektime2pdf](https://github.com/jjeejj/geektime2pdf)|极客时间专栏文章 转为 PDF 包含评论 音频|332|HTML|2021/09/10|
|110|[yangchuansheng/prometheus-handbook](https://github.com/yangchuansheng/prometheus-handbook)|Prometheus 中文文档|329|HTML|2021/08/28|
|111|[zhongzhong0505/CodeBe](https://github.com/zhongzhong0505/CodeBe)|CodeBe(码币)是一个是使用angular2整合各种插件的项目，包括（layer，bootstrap-table，markdown编辑器，highcharts，ckeditor，高德地图,fullcalendar 等等）。如果你有什么想要集成的插件，请告诉我，我来加进去。(请给我加个星，谢谢。)|304|HTML|2021/08/11|
|112|[gosoon/source-code-reading-notes](https://github.com/gosoon/source-code-reading-notes)|源码阅读笔记|300|HTML|2021/11/23|
|113|[CruxF/IMOOC](https://github.com/CruxF/IMOOC)|IMOCC辛勤的搬运工:fire:|300|HTML|2021/10/07|
|114|[huangz1990/blog](https://github.com/huangz1990/blog)|我的个人博客。|299|HTML|2021/11/22|
|115|[6mb/Microsoft-365-Admin](https://github.com/6mb/Microsoft-365-Admin)|基于微软 API 的 office 365 用户管理（支持多账户切换）|290|HTML|2021/07/16|
|116|[HollowMan6/LZU-Auto-COVID-Health-Report](https://github.com/HollowMan6/LZU-Auto-COVID-Health-Report)|LZU Auto COVID Health Report.(兰州大学疫情期间自动定时健康打卡)|286|HTML|2021/11/22|
|117|[doocs/doocs.github.io](https://github.com/doocs/doocs.github.io)|💁‍♀️ Welcome to the Doocs Open Source organization   欢迎加入 Doocs 开源社区|279|HTML|2021/09/25|
|118|[FrankKai/FrankKai.github.io](https://github.com/FrankKai/FrankKai.github.io)|趁你还年轻的前端技术博客，目前就职于涂鸦智能。|279|HTML|2021/10/29|
|119|[xianyunyh/spider_job](https://github.com/xianyunyh/spider_job)|招聘网数据爬虫|279|HTML|2021/11/14|
|120|[mtf-wiki/MtF-Wiki](https://github.com/mtf-wiki/MtF-Wiki)|试图整理汇总 MtF 的相关资料，为大家提供更好的帮助~|279|HTML|2021/11/30|
|121|[daliansky/daliansky.github.io](https://github.com/daliansky/daliansky.github.io)|黑果小兵的部落阁|276|HTML|2021/11/10|
|122|[qwerttvv/Beijing-IPTV](https://github.com/qwerttvv/Beijing-IPTV)|最好用的北京联通IPTV频道列表。https://bjiptv.tk|271|HTML|2021/11/29|
|123|[zoeminghong/go-library](https://github.com/zoeminghong/go-library)|Go语言入门、Go相关优质资料等分享|270|HTML|2021/11/17|
|124|[GoSSIP-SJTU/dailyPaper](https://github.com/GoSSIP-SJTU/dailyPaper)|每日论文推荐|270|HTML|2021/06/21|
|125|[yangjingyu/vs-tree](https://github.com/yangjingyu/vs-tree)|移动端PC端通用树组件，适用于企业组织通讯录，百万数据支持|269|HTML|2021/11/11|
|126|[nkeonkeo/nekonekostatus](https://github.com/nkeonkeo/nekonekostatus)|一个Material Design风格的探针|269|HTML|2021/11/28|
|127|[Xmader/aria-ng-gui-android](https://github.com/Xmader/aria-ng-gui-android)|一个 Aria2 图形界面安卓客户端   An Aria2 GUI Android App|267|HTML|2021/08/11|
|128|[2010yhh/springBoot-demos](https://github.com/2010yhh/springBoot-demos)|springBoot-demos基于1.5.x版本|266|HTML|2021/06/05|
|129|[DTStack/jlogstash](https://github.com/DTStack/jlogstash)|java 版本的logstash|261|HTML|2021/06/08|
|130|[huangshiyu13/webtemplete](https://github.com/huangshiyu13/webtemplete)|收集各种网站模板|261|HTML|2021/09/09|
|131|[godbasin/front-end-playground](https://github.com/godbasin/front-end-playground)|被删和阿猪的前端游乐场！！快来一起撸猫一起学习前端吧~|258|HTML|2021/11/28|
|132|[flutterchina/flutter_in_action_2nd](https://github.com/flutterchina/flutter_in_action_2nd)|《Flutter实战 第二版》- 书稿（未完成）|256|HTML|2021/11/19|
|133|[boism-org/northpole](https://github.com/boism-org/northpole)|存储北极学派的哲学，思考，教义repository|256|HTML|2021/09/26|
|134|[kaityo256/github](https://github.com/kaityo256/github)|GitHub演習|251|HTML|2021/11/17|
|135|[Pad0y/Django2_dailyfresh](https://github.com/Pad0y/Django2_dailyfresh)|dailyfresh电商项目，替换django框架为2.X并重构，美化了下后台管理页面，提供docker版本，该项目包含了实际开发中的电商项目中大部分的功能开发和知识点实践， 是一个非常不错的django学习项目，同时也记录在替换框架中遇到的坑，希望对各位的学习有所帮助。|249|HTML|2021/09/08|
|136|[fguby/Electron-elf](https://github.com/fguby/Electron-elf)|使用electron和live2D开发的类似桌面精灵的应用（A desktop application developed using electron and live2D）|245|HTML|2021/10/13|
|137|[wx-chevalier/Product-Series](https://github.com/wx-chevalier/Product-Series)|:books: 产品迷思，不仅仅是产品经理，对于产品设计、交互体验、项目管理、行业视点等多方面的思考。|244|HTML|2021/11/02|
|138|[lazyparser/weloveinterns](https://github.com/lazyparser/weloveinterns)|中科院软件所智能软件中心实习生社区|243|HTML|2021/11/29|
|139|[mzkmzk/Read](https://github.com/mzkmzk/Read)|阅读总结|242|HTML|2021/11/06|
|140|[ecnice/flow](https://github.com/ecnice/flow)|企业级流程中心（基于flowable和bpmn.js封装的流程引擎，采用Springboot，Mybatis-plus, Ehcache, Shiro 等框架技术,前端采用Vue3&Antd，Vben）|234|HTML|2021/11/30|
|141|[OBKoro1/web_accumulate](https://github.com/OBKoro1/web_accumulate)|前端进阶积累:http://obkoro1.com/web_accumulate/accumulate/|231|HTML|2021/11/05|
|142|[hua1995116/360-sneakers-viewer](https://github.com/hua1995116/360-sneakers-viewer)|鸿星尔克全景（360°）鞋子展示(包含建模过程)|227|HTML|2021/09/16|
|143|[tp-yan/WebStockPredict](https://github.com/tp-yan/WebStockPredict)|基于Web的股票预测系统|214|HTML|2021/08/25|
|144|[Cl0udG0d/HXnineTails](https://github.com/Cl0udG0d/HXnineTails)|python3实现的集成了github上多个扫描工具的命令行WEB扫描工具|214|HTML|2021/09/05|
|145|[satan1a/awesome-cybersecurity-blueteam-cn](https://github.com/satan1a/awesome-cybersecurity-blueteam-cn)|网络安全 · 攻防对抗 · 蓝队清单，中文版|212|HTML|2021/11/18|
|146|[cosname/cosx.org](https://github.com/cosname/cosx.org)|统计之都主站|208|HTML|2021/12/01|
|147|[ikxin/KMS-Scripts](https://github.com/ikxin/KMS-Scripts)|一个生成kms激活脚本的小网站|206|HTML|2021/11/30|
|148|[tobycroft/BiliHP-APP](https://github.com/tobycroft/BiliHP-APP)|BiliBili助手-哔哩哔哩助手苹果/安卓/IOS/PC/C2C/Mac/路由器/单用户/多用户/手机版全平台支持挂机软件库（2020-BiliHP）|205|HTML|2021/09/21|
|149|[UnityITellYou/UnityITellYou.github.io](https://github.com/UnityITellYou/UnityITellYou.github.io)|Unity“特供版”替代资源整理|204|HTML|2021/11/09|
|150|[jenkins-zh/jenkins-zh](https://github.com/jenkins-zh/jenkins-zh)|Jenkins 中文社区网站源码|213|HTML|2021/08/05|
|151|[FHWWC/KeyCheck](https://github.com/FHWWC/KeyCheck)|一个 密钥检测/密钥分享/密钥查询/获取CID等微软产品激活 相关的小工具，小巧方便|204|HTML|2021/11/18|
|152|[Yuezi32/flipClock](https://github.com/Yuezi32/flipClock)|翻牌效果时钟的演示，包含原生JavaScript、Vue、React三种实现方式。|203|HTML|2021/10/06|
|153|[huataihuang/cloud-atlas](https://github.com/huataihuang/cloud-atlas)|云计算指南|203|HTML|2021/11/27|
|154|[plctlab/PLCT-Weekly](https://github.com/plctlab/PLCT-Weekly)|软件所PLCT实验室在开源领域的不定期简报|202|HTML|2021/12/01|
|155|[wx-chevalier/Frontend-Series](https://github.com/wx-chevalier/Frontend-Series)|:books: 大前端的工程实践：iOS 篇   Android 篇   混合式开发篇 - ReactNative、Weex、Weapp|202|HTML|2021/11/02|
|156|[pubdreamcc/Node.js](https://github.com/pubdreamcc/Node.js)|一步一步学习Node.js，带你从零开始学习Node.js！本仓库是自己总结的Node.js学习图文教程，里面有学习案列和源代码（pubdreamcc原创，欢迎转载，欢迎star）|199|HTML|2021/05/10|
|157|[wenfengSAT/wenfengSAT-UI](https://github.com/wenfengSAT/wenfengSAT-UI)|HTML开发模板，包含Bootstrap、EasyUI、LayUI、AmazeUI等主题模板，欢迎star...|170|HTML|2021/05/26|
|158|[Yuxin-Alpha/Frontend-Learning](https://github.com/Yuxin-Alpha/Frontend-Learning)|持续充电ing|163|HTML|2021/11/30|
|159|[yingoja/DemoUI](https://github.com/yingoja/DemoUI)|selenium UI自动化测试框架|163|HTML|2021/06/21|
|160|[dafeijiketang666/2021-2022-it-video](https://github.com/dafeijiketang666/2021-2022-it-video)|🔥🔥🔥各类it学习视频教程，最新版，持续更新，java，c++，前端，后台，测试，python，图灵,享学,鲁班,拉勾,马士兵,慕客,极客,黑马,奈学,咕泡,爪哇,网易微专业,开课吧,小码哥,云析学院,极客小马哥,大厂学院,京程一灯,知播渔李南江,千锋,尚硅谷,珠峰,博学谷,幕课,金职位,奈学,扔物线,码牛学院,动脑学院,看雪,易锦零声学院,csdn,左程云,贪心学院,码同学,霍格沃兹,优点知识,李振良,老男孩,马哥教育,极客|162|HTML|2021/11/26|
|161|[MugglePay/MugglePay](https://github.com/MugglePay/MugglePay)|Make Crypto Payment Easy 让数字货币支付更简单|161|HTML|2021/07/20|
|162|[Wscats/omi-snippets](https://github.com/Wscats/omi-snippets)|🔖Visual Studio Code Syntax Highlighting For Single File React And Omi Components - 编写React和Omi单文件组件的VSC语法高亮插件|149|HTML|2021/08/11|
|163|[sspanel-uim/Wiki](https://github.com/sspanel-uim/Wiki)|我们安装，我们更新，我们开发|148|HTML|2021/11/29|
|164|[mrbulb/ONEPIECE-KG](https://github.com/mrbulb/ONEPIECE-KG)|a knowledge graph project for ONEPIECE /《海贼王》知识图谱|148|HTML|2021/09/19|
|165|[koolshare/koolcenter](https://github.com/koolshare/koolcenter)|提供评测文章，固件教程，插件使用说明等。|147|HTML|2021/11/09|
|166|[YongxinLiu/QIIME2ChineseManual](https://github.com/YongxinLiu/QIIME2ChineseManual)|QIIME 2中文文档(QIIME 2 Chinese Manual)|144|HTML|2021/07/26|
|167|[chinese-poetry/huajianji](https://github.com/chinese-poetry/huajianji)|🌟🎸🌟UI很简洁的中文诗歌主页, 包含唐诗宋词三百首，花间集，南唐二主词，古诗十九首，教科书选诗等文集 |143|HTML|2021/10/25|
|168|[lunasaw/luna-platform](https://github.com/lunasaw/luna-platform)|党建管理平台|140|HTML|2021/06/08|
|169|[zhangxiang958/Blog](https://github.com/zhangxiang958/Blog)|阿翔的个人技术博客，博文写在 Issues 里，如有收获请 star 鼓励~|140|HTML|2021/11/07|
|170|[liyupi/better-coder](https://github.com/liyupi/better-coder)|😄 一起快乐成长为更好的程序员吧！编程学习经验、技术干货、资源分享|139|HTML|2021/10/07|
|171|[BlueSkyXN/TencentCloud-Order](https://github.com/BlueSkyXN/TencentCloud-Order)|腾讯云产品-自定义参数购买一键单页，免额外抓包，直接输入参数提交自动跳转对应订单页面|138|HTML|2021/07/12|
|172|[KevinWang15/fdty](https://github.com/KevinWang15/fdty)|复旦体育理论考试 自动做题器|138|HTML|2021/11/27|
|173|[lipengzhou/toutiao-m](https://github.com/lipengzhou/toutiao-m)|基于 Vue.js 开发的移动端项目——今日头条（页面下方扫码体验）|137|HTML|2021/05/11|
|174|[Ovilia/omynote](https://github.com/Ovilia/omynote)|众山小笔记 - 集中管理你的读书笔记|136|HTML|2021/11/21|
|175|[Woqurefan/ApiTest](https://github.com/Woqurefan/ApiTest)|接口测试平台-python3+django+requests实现  公众号：测试开发干货   testerhome社团：测试方舟号|136|HTML|2021/05/19|
|176|[jokester/coc-zh](https://github.com/jokester/coc-zh)|克苏鲁神话 中文合集|133|HTML|2021/09/15|
|177|[xiaolai/apple-computer-literacy](https://github.com/xiaolai/apple-computer-literacy)|个人电脑使用（以苹果产品为例）|133|HTML|2021/11/26|
|178|[huangboju/Moots](https://github.com/huangboju/Moots)|收集了一下学习资料|131|HTML|2021/11/28|
|179|[apicloudcom/act](https://github.com/apicloudcom/act)|[AVM] [组件] [案例] ACT 是 AVM Component & Template 的缩写。 Vant 组件库的 AVM 实现。|131|HTML|2021/11/26|
|180|[xuliker/kde](https://github.com/xuliker/kde)|记录我的内核成长贡献之路。IMO, fork or clone this repo would be very stupid.  If you have any questions, just send me an email.|126|HTML|2021/11/30|
|181|[HaoZhang95/dailyfresh](https://github.com/HaoZhang95/dailyfresh)|天天生鲜是传智播客黑马出品的python实战项目， 项目的[在线视频教程], 项目的讲义被放在了Python24期整套视频的讲义中的**第20章节**，具体的天天生鲜 [在线讲义查看]，除了天天生鲜项目之外，传智播客&黑马出品的Python24期人工智能整套代码和讲义集合，项目从零基础的Python教程到深度学习，总共30章节，其中包含Python基础中的飞机大战项目，WSGI项目，Flask新经资讯项目， Django的电商项目(本应该的美多商城项目因为使用的是Vue技术，所以替换为Django天天生鲜项目)等等，请[点击此处查看](https://github.com/HaoZhang95 ...|126|HTML|2021/10/07|
|182|[akiritsu/cslearner.cn](https://github.com/akiritsu/cslearner.cn)|计算机科学学习指南-个人向|125|HTML|2021/08/20|
|183|[zwxs/frontInterview](https://github.com/zwxs/frontInterview)|web前端面试经历|125|HTML|2021/05/17|
|184|[hirocaster/wdpress69](https://github.com/hirocaster/wdpress69)|WEB+DB PRESS Voll.69 詳解GitHub|123|HTML|2021/09/25|
|185|[Agda-zh/PLFA-zh](https://github.com/Agda-zh/PLFA-zh)|《编程语言基础：Agda 描述》，Programming Language Foundations in Agda 中文版|122|HTML|2021/11/16|
|186|[gouguoyin/phprap](https://github.com/gouguoyin/phprap)|PHPRAP，是一个PHP轻量级开源API接口文档管理系统，致力于减少前后端沟通成本，提高团队协作开发效率，打造PHP版的RAP。如果您觉得PHPRAP对您有用的话，别忘了给点个赞哦^_^ ！ |118|HTML|2021/08/30|
|187|[zwc456baby/file-proxy](https://github.com/zwc456baby/file-proxy)|文件代下载服务，github文件加速下载，支持任意文件格式。支持命令行代下，支持子节点权重负载均衡。|118|HTML|2021/08/22|
|188|[HeroIsUseless/MyBook](https://github.com/HeroIsUseless/MyBook)|简约优美的电子书阅读器（支持全平台）|118|HTML|2021/10/25|
|189|[su37josephxia/bytedance-youth-training-camp](https://github.com/su37josephxia/bytedance-youth-training-camp)|字节青训营教程|117|HTML|2021/10/08|
|190|[BlueSkyXN/KIENG-FigureBed](https://github.com/BlueSkyXN/KIENG-FigureBed)|image.kieng.cn 的仿站源码，支持Vercel，CloudFlare，Github等无服务器部署|116|HTML|2021/11/12|
|191|[CnPeng/LearningNotes](https://github.com/CnPeng/LearningNotes)|个人综合学习笔记, 主要为 Android , 其他还有 Java、Kotlin、IOS、Go、Web 等内容|115|HTML|2021/10/29|
|192|[281677160/openwrt-package](https://github.com/281677160/openwrt-package)|281677160的openwrt专用插件包|114|HTML|2021/12/01|
|193|[airyland/we-extract](https://github.com/airyland/we-extract)|解析微信公众号文章元信息|113|HTML|2021/08/29|
|194|[Thinkingcao/silence-boot](https://github.com/Thinkingcao/silence-boot)|SpringBoot2.x开发微信后台管理系统：  基于RuoYi3.4版本和SpringBoot2.1.1.RELEASE升级模块构建后台权限管理系统，在原有的框架上增加了集成微信开发框架和lombok代码插件，开发更便捷|113|HTML|2021/09/21|
|195|[gxlmyacc/sciter-doc-zh](https://github.com/gxlmyacc/sciter-doc-zh)|sciter中文帮助手册|112|HTML|2021/09/07|
|196|[youerning/blog](https://github.com/youerning/blog)|文章列表|112|HTML|2021/09/07|
|197|[wx-chevalier/K8s-Series](https://github.com/wx-chevalier/K8s-Series)|深入浅出 K8s：概念与部署 工作载荷 服务负载 存储 权限 网络 生态扩展|111|HTML|2021/11/02|
|198|[luoxuhai/chinese-novel](https://github.com/luoxuhai/chinese-novel)|📙 Chinese novel database 最全的中国古典小说数据库。|111|HTML|2021/09/12|
|199|[Wscats/omi-electron](https://github.com/Wscats/omi-electron)|🚀Build cross platform desktop apps with Omi.js and Electron.js 基于Omi.js和Electron.js构建跨平台的桌面应用|110|HTML|2021/10/13|
|200|[cnymw/GolangStudy](https://github.com/cnymw/GolangStudy)|《golang 面试学习》：从简单到难最全总结，go基础，数据结构，算法，设计模式。微信小程序搜索“GolangStudy”可以手机端学习Golang。|109|HTML|2021/11/08|

⬆ [回到目录](#内容目录)

<br/>

## TypeScript

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[wenyan-lang/wenyan](https://github.com/wenyan-lang/wenyan)|文言文編程語言 A programming language for the ancient Chinese.|17.8k|TypeScript|2021/10/25|
|2|[alibaba/ice](https://github.com/alibaba/ice)|🚀 The Progressive Framework Based On React（基于 React 的渐进式研发框架）|16.9k|TypeScript|2021/12/01|
|3|[pnpm/pnpm](https://github.com/pnpm/pnpm)|Fast, disk space efficient package manager -- 快速的，节省磁盘空间的包管理工具|13.8k|TypeScript|2021/12/01|
|4|[wangeditor-team/wangEditor](https://github.com/wangeditor-team/wangEditor)|wangEditor —— 轻量级web富文本框|12.7k|TypeScript|2021/12/01|
|5|[unlock-music/unlock-music](https://github.com/unlock-music/unlock-music)|Unlock encrypted music file in browser. 在浏览器中解锁加密的音乐文件。|10.2k|TypeScript|2021/11/11|
|6|[the1812/Bilibili-Evolved](https://github.com/the1812/Bilibili-Evolved)|强大的哔哩哔哩增强脚本: 下载视频, 音乐, 封面, 弹幕 / 简化直播间, 评论区, 首页 / 自定义顶栏, 删除广告, 夜间模式 / 触屏设备支持|9.7k|TypeScript|2021/12/01|
|7|[baidu/amis](https://github.com/baidu/amis)|前端低代码框架，通过 JSON 配置就能生成各种页面。|8.7k|TypeScript|2021/12/01|
|8|[xcatliu/typescript-tutorial](https://github.com/xcatliu/typescript-tutorial)|TypeScript 入门教程|8.3k|TypeScript|2021/09/10|
|9|[thx/rap2-delos](https://github.com/thx/rap2-delos)|阿里妈妈前端团队出品的开源接口管理工具RAP第二代|7.2k|TypeScript|2021/11/26|
|10|[any86/any-rule](https://github.com/any86/any-rule)|🦕  常用正则大全, 支持web / vscode / idea / Alfred Workflow多平台|5.6k|TypeScript|2021/11/19|
|11|[zhongsp/TypeScript](https://github.com/zhongsp/TypeScript)|TypeScript 使用手册（中文版）翻译。http://www.typescriptlang.org|5.3k|TypeScript|2021/11/20|
|12|[jkchao/typescript-book-chinese](https://github.com/jkchao/typescript-book-chinese)|TypeScript Deep Dive 中文版 |5.0k|TypeScript|2021/11/11|
|13|[PaddlePaddle/VisualDL](https://github.com/PaddlePaddle/VisualDL)|Deep Learning Visualization Toolkit（『飞桨』深度学习可视化工具 ）|4.2k|TypeScript|2021/11/30|
|14|[x-extends/vxe-table](https://github.com/x-extends/vxe-table)|🐬 vxe-table vue  表格解决方案|4.2k|TypeScript|2021/11/30|
|15|[whyour/qinglong](https://github.com/whyour/qinglong)|支持python3、javaScript、shell、typescript 的定时任务管理面板（A timed task management panel that supports typescript, javaScript, python3, and shell）|4.1k|TypeScript|2021/11/29|
|16|[remaxjs/remax](https://github.com/remaxjs/remax)|使用真正的 React 构建跨平台小程序|4.1k|TypeScript|2021/11/29|
|17|[brick-design/brick-design](https://github.com/brick-design/brick-design)|全场景流式布局，可视化拖拽、随意嵌套组合、实时渲染、实时辅助线展示，实时组件间距展示、实时拖拽排序、状态域管理，可视化属性配置、可视化样式配置、多设备适配展示，支持逻辑渲染、模板字符变量、表达式、自定义方法、自定义状态|4.0k|TypeScript|2021/11/22|
|18|[NervJS/taro-ui](https://github.com/NervJS/taro-ui)|一款基于 Taro 框架开发的多端 UI 组件库|3.8k|TypeScript|2021/11/01|
|19|[doodlewind/jshistory-cn](https://github.com/doodlewind/jshistory-cn)|🇨🇳 《JavaScript 二十年》中文版|3.7k|TypeScript|2021/11/25|
|20|[cuixiaorui/mini-vue](https://github.com/cuixiaorui/mini-vue)|实现最简 vue3 模型( Help you learn more efficiently vue3 source code )|3.6k|TypeScript|2021/11/26|
|21|[Vanessa219/vditor](https://github.com/Vanessa219/vditor)|♏  一款浏览器端的 Markdown 编辑器，支持所见即所得（富文本）、即时渲染（类似 Typora）和分屏预览模式。An In-browser Markdown editor, support WYSIWYG (Rich Text),  Instant Rendering (Typora-like) and Split View modes.|3.5k|TypeScript|2021/11/26|
|22|[Kaiyiwing/qwerty-learner](https://github.com/Kaiyiwing/qwerty-learner)|为键盘工作者设计的单词记忆与英语肌肉记忆锻炼软件 / Words learning and English muscle memory training software designed for keyboard workers|3.5k|TypeScript|2021/11/29|
|23|[7kms/react-illustration-series](https://github.com/7kms/react-illustration-series)|图解react源码, 用大量配图的方式, 致力于将react原理表述清楚.|3.5k|TypeScript|2021/11/29|
|24|[hustcc/echarts-for-react](https://github.com/hustcc/echarts-for-react)|⛳️  Apache ECharts components for React wrapper. 一个简单的 Apache echarts 的 React 封装。|3.4k|TypeScript|2021/11/05|
|25|[NaoTu/DesktopNaotu](https://github.com/NaoTu/DesktopNaotu)|桌面版脑图 (百度脑图离线版，思维导图) 跨平台支持 Windows/Linux/Mac OS. (A cross-platform multilingual Mind Map Tool)|3.3k|TypeScript|2021/05/31|
|26|[le5le-com/topology](https://github.com/le5le-com/topology)|A diagram (topology, UML) framework uses canvas and typescript.  一个轻量（100k左右）、功能丰富的绘图工具（微服务架构图、拓扑图、流程图、类图等UML图、脑图，动画、视频支持）。 【在线使用】：|3.2k|TypeScript|2021/11/26|
|27|[YunYouJun/air-conditioner](https://github.com/YunYouJun/air-conditioner)|❄️ 云空调，便携小空调，为你的夏日带去清凉！|2.9k|TypeScript|2021/11/01|
|28|[buuing/lucky-canvas](https://github.com/buuing/lucky-canvas)|🎖🎖🎖 基于 TS + Canvas 开发的【大转盘 / 九宫格 / 老虎机】抽奖插件，🌈 一套源码适配多端框架 JS / Vue / React / Taro / UniApp / 微信小程序等，🎨 奖品 / 文字 / 图片 / 颜色 / 按钮均可配置，支持同步 / 异步抽奖，🎯 概率前 / 后端可控，🚀 自动根据 dpr 调整清晰度适配移动端|2.6k|TypeScript|2021/11/28|
|29|[notadd/notadd](https://github.com/notadd/notadd)|A microservice development architecture based on nest.js. —— 基于 Nest.js 的微服务开发架构。|2.6k|TypeScript|2021/09/28|
|30|[alibaba/pont](https://github.com/alibaba/pont)|🌉数据服务层解决方案|2.5k|TypeScript|2021/11/29|
|31|[hujiulong/gcoord](https://github.com/hujiulong/gcoord)|地理坐标系转换工具|2.2k|TypeScript|2021/11/27|
|32|[sparrow-js/sparrow](https://github.com/sparrow-js/sparrow)|🎉场景化低代码（LowCode）搭建工作台，实时输出源代码|2.2k|TypeScript|2021/09/18|
|33|[LeekHub/leek-fund](https://github.com/LeekHub/leek-fund)|:chart_with_upwards_trend:  韭菜盒子——VSCode 里也可以看股票 & 基金实时数据，做最好用的投资插件 🐥|2.1k|TypeScript|2021/11/30|
|34|[lzxb/vue2-demo](https://github.com/lzxb/vue2-demo)|Vue 基于 Genesis + TS + Vuex 实现的 SSR demo|2.1k|TypeScript|2021/08/26|
|35|[zenozeng/fonts.css](https://github.com/zenozeng/fonts.css)|跨平台 CSS 中文字体解决方案|1.9k|TypeScript|2021/11/08|
|36|[alibaba/kiwi](https://github.com/alibaba/kiwi)|🐤 Kiwi-国际化全流程解决方案|1.9k|TypeScript|2021/11/26|
|37|[suanmei/callapp-lib](https://github.com/suanmei/callapp-lib)|🔥call app from h5（H5唤起客户端 ）|1.8k|TypeScript|2021/07/09|
|38|[zhangyuang/egg-react-ssr](https://github.com/zhangyuang/egg-react-ssr)|最小而美的Egg + React + SSR 服务端渲染应用骨架，同时支持JS和TS|1.8k|TypeScript|2021/09/14|
|39|[Xmader/musescore-downloader](https://github.com/Xmader/musescore-downloader)|Download sheet music (MSCZ, PDF, MusicXML, MIDI, MP3, download individual parts as PDF) from musescore.com for free, no login or Musescore Pro required   免登录、免 Musescore Pro，免费下载 musescore.com 上的曲谱   Installation: https://msdl.librescore.org/install.user.js|1.8k|TypeScript|2021/08/19|
|40|[phodal/ledge](https://github.com/phodal/ledge)|Ledge —— DevOps knowledge learning platform. DevOps、研发效能知识和工具平台，是我们基于在 ThoughtWorks 进行的一系列 DevOps 实践、敏捷实践、软件开发与测试、精益实践提炼出来的知识体系。它包含了各种最佳实践、操作手册、原则与模式、度量、工具，用于帮助您的企业在数字化时代更好地前进，还有 DevOps 转型。|1.7k|TypeScript|2021/10/26|
|41|[Meituan-Dianping/beeshell](https://github.com/Meituan-Dianping/beeshell)|React Native 组件库|1.7k|TypeScript|2021/08/26|
|42|[jabbany/CommentCoreLibrary](https://github.com/jabbany/CommentCoreLibrary)|Javascript Live Comment (Danmaku) Engine Implementation. JS弹幕模块核心，提供从基本骨架到高级弹幕的支持。|1.7k|TypeScript|2021/09/02|
|43|[CodFrm/cxmooc-tools](https://github.com/CodFrm/cxmooc-tools)|一个 超星(学习通)/智慧树(知到)/中国大学mooc 学习工具,火狐,谷歌,油猴支持.全自动任务,视频倍速秒过,作业考试题库,验证码自动打码(੧ᐛ੭挂科模式,启动)|1.6k|TypeScript|2021/11/03|
|44|[xuejianxianzun/PixivBatchDownloader](https://github.com/xuejianxianzun/PixivBatchDownloader)|Chrome 扩展，批量下载 Pixiv 的插画和小说。过滤作品、下载时重命名、转换动态图片等。Powerful Pixiv batch downloader. Batch download artworks and novels, filter works, rename when downloading, convert animated images, and more.|1.5k|TypeScript|2021/11/30|
|45|[ice-lab/icestark](https://github.com/ice-lab/icestark)|:tiger: Micro Frontends solution for large application（面向大型应用的微前端解决方案），站点国内镜像：https://icestark.gitee.io|1.5k|TypeScript|2021/11/29|
|46|[jaywcjlove/github-rank](https://github.com/jaywcjlove/github-rank)|🕷️Github 中国和全球用户排名，全球仓库 Star 最多排名(自动日更)。|1.5k|TypeScript|2021/12/01|
|47|[mcuking/mobile-web-best-practice](https://github.com/mcuking/mobile-web-best-practice)|:tiger: 移动 web 最佳实践|1.5k|TypeScript|2021/09/10|
|48|[wechat-miniprogram/weui-miniprogram](https://github.com/wechat-miniprogram/weui-miniprogram)|小程序WeUI组件库|1.4k|TypeScript|2021/11/24|
|49|[genaller/genal-chat](https://github.com/genaller/genal-chat)|🐱‍🏍阿童木聊天室 nestjs+vue全栈聊天室 前后端分离 typescript一把梭|1.4k|TypeScript|2021/10/07|
|50|[zenghongtu/PPet](https://github.com/zenghongtu/PPet)|👻在你的桌面放一个萌妹子，多一点趣味😏~（支持Mac、Win和Linux）|1.4k|TypeScript|2021/10/13|
|51|[ZhongAnTech/zarm](https://github.com/ZhongAnTech/zarm)|基于 React、React-Native 的移动端UI组件库|1.3k|TypeScript|2021/12/01|
|52|[EhTagTranslation/EhSyringe](https://github.com/EhTagTranslation/EhSyringe)|E 站注射器，将中文翻译注入到 E 站体内|1.2k|TypeScript|2021/12/01|
|53|[aisuda/amis-editor-demo](https://github.com/aisuda/amis-editor-demo)|amis 可视化编辑器示例 http://aisuda.github.io/amis-editor-demo|1.1k|TypeScript|2021/11/10|
|54|[mengsixing/cdfang-spider](https://github.com/mengsixing/cdfang-spider)|📊 成都房协网数据分析，喜欢请点 star！|1.1k|TypeScript|2021/09/22|
|55|[cool-team-official/cool-admin-midway](https://github.com/cool-team-official/cool-admin-midway)|cool-admin(midway版)一个很酷的后台权限管理框架，模块化、插件化、CRUD极速开发，永久开源免费，基于midway.js 2.0、typescript、typeorm、mysql、jwt、element-ui等构建|1.1k|TypeScript|2021/11/23|
|56|[kmvan/x-prober](https://github.com/kmvan/x-prober)|🐘 A probe program for PHP environment (一款精美的 PHP 探針, 又名X探針、劉海探針)|1.1k|TypeScript|2021/10/25|
|57|[lsqy/taro-music](https://github.com/lsqy/taro-music)|🎉基于taro + taro-ui + redux + react-hooks + typescript 开发的网易云音乐小程序,taro3已升级完毕，目前正在使用react-hooks重构中（不定时更新）|1.1k|TypeScript|2021/11/24|
|58|[clouDr-f2e/monitor](https://github.com/clouDr-f2e/monitor)|👀 一款轻量级的收集页面的用户点击行为、路由跳转、接口报错、代码报错、页面性能并上报服务端的SDK|1.1k|TypeScript|2021/12/01|
|59|[geekape/geek-navigation](https://github.com/geekape/geek-navigation)|❤️ 极客猿导航－独立开发者的导航站！|1.1k|TypeScript|2021/09/29|
|60|[qiuxiang/react-native-amap3d](https://github.com/qiuxiang/react-native-amap3d)|react-native 高德地图组件，使用最新 3D SDK，支持 Android + iOS|1.1k|TypeScript|2021/11/23|
|61|[buqiyuan/vue3-antd-admin](https://github.com/buqiyuan/vue3-antd-admin)|基于vue-cli/vite + vue3.0 + ant-design-vue2.0 + typescript hooks 的基础后台管理系统模板  RBAC的权限系统, JSON Schema动态表单,动态表格,漂亮锁屏界面|1.0k|TypeScript|2021/09/26|
|62|[hua1995116/react-resume-site](https://github.com/hua1995116/react-resume-site)|木及简历，一款markdown的在线简历工具。|1.0k|TypeScript|2021/10/04|
|63|[xjh22222228/nav](https://github.com/xjh22222228/nav)|🔍 发现导航 , 打造最强静态导航网站(支持SEO)    Discovery Navigation: A purely static, powerful navigation website that supports SEO and online editing|972|TypeScript|2021/11/24|
|64|[takayama-lily/oicq](https://github.com/takayama-lily/oicq)|QQ机器人协议库|948|TypeScript|2021/11/07|
|65|[cangzhang/champ-r](https://github.com/cangzhang/champ-r)|🐶 Yet another League of Legends helper, compatible with CN/NA/EU/JP/KR version of LCU. 英雄联盟出装/天赋助手, 支持多服客户端|918|TypeScript|2021/11/30|
|66|[qiu8310/minapp](https://github.com/qiu8310/minapp)|重新定义微信小程序的开发|916|TypeScript|2021/08/11|
|67|[shalldie/vscode-background](https://github.com/shalldie/vscode-background)|A vscode extension to make it lovely. vscode background 背景扩展插件。|855|TypeScript|2021/11/12|
|68|[RxJS-CN/learn-rxjs-operators](https://github.com/RxJS-CN/learn-rxjs-operators)|Learn RxJS 中文版 (通过清晰的示例来学习 RxJS 5 操作符)|841|TypeScript|2021/07/08|
|69|[buqiyuan/vite-vue3-lowcode](https://github.com/buqiyuan/vite-vue3-lowcode)|vue3.x + vite2.x + vant + element-plus H5移动端低代码平台 lowcode 可视化拖拽 可视化编辑器 visual editor 类似易企秀的H5制作、建站工具、可视化搭建工具|825|TypeScript|2021/10/22|
|70|[any86/any-touch](https://github.com/any86/any-touch)|:wave:    手势库, 按需2kb~5kb,  兼容PC / 移动端|759|TypeScript|2021/11/30|
|71|[dzzzzzy/Nestjs-Learning](https://github.com/dzzzzzy/Nestjs-Learning)|nestjs 学习教程 :books:，跟我一起学习 nest 框架~ :muscle:|746|TypeScript|2021/10/19|
|72|[MoeFE/GoogleTranslate](https://github.com/MoeFE/GoogleTranslate)|🌐 Google 翻译 Mac 客户端|732|TypeScript|2021/07/27|
|73|[Serverless-Devs/Serverless-Devs](https://github.com/Serverless-Devs/Serverless-Devs)|:fire::fire::fire: Serverless Devs developer tool ( Serverless Devs 开发者工具 )|728|TypeScript|2021/11/29|
|74|[iSpring/WebGlobe](https://github.com/iSpring/WebGlobe)|基于HTML5原生WebGL实现的轻量级Google Earth三维地图引擎|717|TypeScript|2021/07/16|
|75|[hylerrix/deno-tutorial](https://github.com/hylerrix/deno-tutorial)|:sauropod: 长期更新的《Deno 钻研之术》！循序渐进学 Deno & 先易后难补 Node & 面向未来的 Deno Web 应用开发|712|TypeScript|2021/07/22|
|76|[3Shain/Comen](https://github.com/3Shain/Comen)|📺直播用弹幕栏【原bilichat】|707|TypeScript|2021/10/11|
|77|[fjc0k/docker-YApi](https://github.com/fjc0k/docker-YApi)|接口管理平台 YApi 的 Docker 镜像。|703|TypeScript|2021/11/11|
|78|[apptools-lab/AppWorks](https://github.com/apptools-lab/AppWorks)|🐻  基于 VS Code 插件的前端研发工具集，站点国内镜像：https://apptools.gitee.io|698|TypeScript|2021/12/01|
|79|[zh-lx/pinyin-pro](https://github.com/zh-lx/pinyin-pro)|中文转拼音、拼音音调、拼音声母、拼音韵母、多音字拼音、拼音首字母|690|TypeScript|2021/10/19|
|80|[sorrycc/weekly](https://github.com/sorrycc/weekly)|前端周刊，每周一发布。|687|TypeScript|2021/11/28|
|81|[woopen/nplayer](https://github.com/woopen/nplayer)|🚀 支持移动端、支持 SSR、支持直播，可以接入任何流媒体。高性能的弹幕系统。高度可定制，所有图标、主题色等都可以替换，并且提供了内置组件方便二次开发。无第三方运行时依赖。|681|TypeScript|2021/12/01|
|82|[koishijs/koishi](https://github.com/koishijs/koishi)|现代、高效的跨平台聊天机器人框架|679|TypeScript|2021/12/01|
|83|[HarryChen0506/react-markdown-editor-lite](https://github.com/HarryChen0506/react-markdown-editor-lite)|a light-weight Markdown editor based on  React. 一款轻量的基于React的markdown编辑器|671|TypeScript|2021/11/30|
|84|[chenshenhai/deno_note](https://github.com/chenshenhai/deno_note)|《Deno进阶开发笔记》 (不定时更新)  🌶🌶🌶|670|TypeScript|2021/05/08|
|85|[aliyun/alibabacloud-alfa](https://github.com/aliyun/alibabacloud-alfa)|阿里云微前端解决方案|608|TypeScript|2021/11/19|
|86|[wsdo/taro-kit](https://github.com/wsdo/taro-kit)|🏆✌️taro 小程序脚手架。 特性： 封装api、redux优雅集成、生成海报类，异常日志上报。 如果能帮到你，就给个 star😊|602|TypeScript|2021/11/30|
|87|[wyb10a10/cocos_creator_framework](https://github.com/wyb10a10/cocos_creator_framework)|cocos creator 基础框架，包含资源、ui管理，网络模块|592|TypeScript|2021/09/20|
|88|[MajsoulPlus/majsoul-plus](https://github.com/MajsoulPlus/majsoul-plus)|雀魂Plus——雀魂麻将Majsoul专用浏览器，提供了一些专有特性|584|TypeScript|2021/10/13|
|89|[chinese-chess-everywhere/type-chess](https://github.com/chinese-chess-everywhere/type-chess)|一个玩具，用 TypeScript 类型系统写的中国象棋|573|TypeScript|2021/11/07|
|90|[54sword/xiaoduyu.com](https://github.com/54sword/xiaoduyu.com)|🐟小度鱼 - 年轻人的交流社区 https://www.xiaoduyu.com|573|TypeScript|2021/07/01|
|91|[takayama-lily/vscode-qq](https://github.com/takayama-lily/vscode-qq)|基于安卓QQ协议的vscode-qq扩展|549|TypeScript|2021/10/09|
|92|[michalyao/evermonkey](https://github.com/michalyao/evermonkey)|Evernote Editing. Redefined.  关于 token 的问题请去 https://github.com/michalyao/evermonkey/issues/94 中查看！|547|TypeScript|2021/09/01|
|93|[zlq4863947/triangular-arbitrage](https://github.com/zlq4863947/triangular-arbitrage)|数字货币-三角套利机器人|539|TypeScript|2021/06/06|
|94|[H5-Dooring/dooringx](https://github.com/H5-Dooring/dooringx)|快速高效搭建可视化拖拽平台|514|TypeScript|2021/11/29|
|95|[LuckDraw/lucky-canvas-invalid](https://github.com/LuckDraw/lucky-canvas-invalid)|🚧 该库已迁移到 https://github.com/LuckDraw/lucky-canvas|512|TypeScript|2021/07/25|
|96|[tianyong90/we-vue](https://github.com/tianyong90/we-vue)|we-vue， 不只是 vue.js + weui!|491|TypeScript|2021/10/06|
|97|[eyebluecn/tank-front](https://github.com/eyebluecn/tank-front)|蓝眼系列软件之《蓝眼云盘》前端项目|490|TypeScript|2021/11/26|
|98|[textbus/textbus](https://github.com/textbus/textbus)|TextBus 是一个面向未来的 web 富文本框架，同时也可以作为一个开箱即用的富文本编辑器，拥有非常好的扩展性和可定制性，是驱动复杂富文本的不二之选！|489|TypeScript|2021/11/23|
|99|[SunshowerC/fund-strategy](https://github.com/SunshowerC/fund-strategy)|基金投资策略分析，基金回测工具|484|TypeScript|2021/07/02|
|100|[bilive/bilive_client](https://github.com/bilive/bilive_client)|基于Node.JS的bilibili账号活跃系统|482|TypeScript|2021/07/23|
|101|[theajack/cnchar](https://github.com/theajack/cnchar)|功能全面的汉字工具库 (拼音 笔画 笔顺 偏旁 成语等) (Chinese character util)|468|TypeScript|2021/11/29|
|102|[wuba/Picasso](https://github.com/wuba/Picasso)|一款UI自动生成代码插件，提供UI自动生成代码全流程解决方案。|458|TypeScript|2021/11/22|
|103|[wechat-miniprogram/computed](https://github.com/wechat-miniprogram/computed)|小程序自定义组件 computed / watch 扩展|454|TypeScript|2021/09/09|
|104|[yangrds/file-chunk](https://github.com/yangrds/file-chunk)|file-chunk是一款针对大文件，超大文件上传的全方位解决方案，支持断点续传，持久化续传，全程状态监控，严格的请求队列模式，分片传输造成高并发的同时，又保障了数据传输的稳定性。|451|TypeScript|2021/07/11|
|105|[YaoZeyuan/zhihuhelp](https://github.com/YaoZeyuan/zhihuhelp)|基于node&typescript重写知乎助手|448|TypeScript|2021/09/13|
|106|[gh-kL/cocoscreator-list](https://github.com/gh-kL/cocoscreator-list)|这是一个基于CocosCreator的List（列表）组件。支持虚拟列表、循环列表、不定宽/高、选择模式、滑动模式等。|445|TypeScript|2021/08/27|
|107|[TencentCloudBase/cloudbase-extension-cms](https://github.com/TencentCloudBase/cloudbase-extension-cms)|🚀 一站式云端内容管理系统 - An open source Node.js headless cms based on CloudBase|443|TypeScript|2021/11/17|
|108|[chowa/ejyy](https://github.com/chowa/ejyy)|「e家宜业」是一整套开源、无加密、无阉割的智慧物业解决方案，基于nodejs、typescript、koa、vue开发，包含web中台、业主小程序、员工小程序、公众号、物联网应用等，涵盖业主服务、物业运营、智能物联、数据统计等主要业务。|434|TypeScript|2021/12/01|
|109|[magic-akari/lrc-maker](https://github.com/magic-akari/lrc-maker)|歌词滚动姬｜可能是你所能见到的最好用的歌词制作工具|433|TypeScript|2021/10/03|
|110|[AlloyTeam/alloy-worker](https://github.com/AlloyTeam/alloy-worker)|面向事务的高可用 Web Worker 通信框架|422|TypeScript|2021/11/10|
|111|[niu-grandpa/rabbit-ui](https://github.com/niu-grandpa/rabbit-ui)|基于 TypeScript 编写的 JavaScript 简洁 UI 库|421|TypeScript|2021/08/27|
|112|[redux-model/redux-model](https://github.com/redux-model/redux-model)|[已废弃] A redux framework for TS project.|421|TypeScript|2021/11/27|
|113|[myopenresources/cc](https://github.com/myopenresources/cc)|一个基于angular5.0.0+ng-bootstrap1.0.0-beta.8+bootstrap4.0.0-beta.2+scss的后台管理系统界面(没基础的同学请先自学基础，谢谢！)|413|TypeScript|2021/08/11|
|114|[Javen205/TNWX](https://github.com/Javen205/TNWX)|TNWX: TypeScript + Node.js + WeiXin 微信系开发脚手架，支持微信公众号、微信支付、微信小游戏、微信小程序、企业微信/企业号。最最最重要的是能快速的集成至任何 Node.js 框架(Express、Nest、Egg、Koa 等)|413|TypeScript|2021/10/06|
|115|[vue-mini/vue-mini](https://github.com/vue-mini/vue-mini)|像写 Vue 3 一样写小程序|413|TypeScript|2021/11/26|
|116|[asdjgfr/operationRecord](https://github.com/asdjgfr/operationRecord)|在浏览器中录制任意界面并实现导出、保存与管理|412|TypeScript|2021/10/16|
|117|[vortesnail/qier-player](https://github.com/vortesnail/qier-player)|:clapper:  A simple and easy-to-use h5 video player with highly customizable UI and rich features. / 简单易用的h5播放器，UI 高度定制化，功能丰富。|406|TypeScript|2021/09/23|
|118|[kuangshp/nestjs-mysql-api](https://github.com/kuangshp/nestjs-mysql-api)|NestJs CRUD for RESTful API使用nestjs+mysql+typeorm+jwt+swagger企业项目中的RBAC权限管理、实现单点登录。|402|TypeScript|2021/11/04|
|119|[chuyun/taro-plugin-canvas](https://github.com/chuyun/taro-plugin-canvas)|基于 Taro 框架的微信小程序 canvas 绘图组件，封装了常用的操作，通过配置的方式生成分享图片|391|TypeScript|2021/10/06|
|120|[umijs/fabric](https://github.com/umijs/fabric)|💪严格但是不严苛的编码规范|390|TypeScript|2021/10/24|
|121|[YXL76/cloudmusic-vscode](https://github.com/YXL76/cloudmusic-vscode)|Netease Music for VS Code / 网易云音乐vscode扩展|386|TypeScript|2021/11/30|
|122|[liqi0816/bilitwin](https://github.com/liqi0816/bilitwin)|bilibili merged flv+mp4+ass+enhance / 哔哩哔哩: 超清FLV下载, FLV合并, 原生MP4下载, 弹幕ASS下载, MKV打包, 播放体验增强, 原生appsecret, 不借助其他网站|380|TypeScript|2021/07/24|
|123|[deathmemory/FridaContainer](https://github.com/deathmemory/FridaContainer)|FridaContainer 整合了网上流行的和自己编写的常用的 frida 脚本，为逆向工作提效之用。 frida 脚本模块化，Java & Jni Trace。|374|TypeScript|2021/10/01|
|124|[iconfont-cli/react-native-iconfont-cli](https://github.com/iconfont-cli/react-native-iconfont-cli)|把iconfont.cn的图标转换成标准RN组件，不依赖字体，支持多色彩，支持热更新|372|TypeScript|2021/10/09|
|125|[idrawjs/idraw](https://github.com/idrawjs/idraw)|A simple JavaScript framework for Drawing on the web.(一个面向Web绘图的JavaScript框架)|371|TypeScript|2021/11/29|
|126|[Ewall1106/vue-h5-template](https://github.com/Ewall1106/vue-h5-template)|使用Vue3.0+Typescript+Vant搭建移动端H5页面开发所需的基础模板，并提供一些通用型的解决方案及扩展功能。|371|TypeScript|2021/11/22|
|127|[yinLiangDream/mp-colorui](https://github.com/yinLiangDream/mp-colorui)|MP-COLORUI 是基于 Taro框架的组件库，由于作者平时工作较忙，有时回复不及时，有事可以加QQ群 1145534886 咨询，文档请看这里→|369|TypeScript|2021/10/06|
|128|[neroneroffy/react-music-player](https://github.com/neroneroffy/react-music-player)|React，TS的音乐播放插件，歌词同步功能，适配PC和移动端；A music player build with react and typescript for mobile and PC|368|TypeScript|2021/10/06|
|129|[ZhangMingZhao1/StreamerHelper](https://github.com/ZhangMingZhao1/StreamerHelper)|全平台主播录制工具 & 自动投稿b站|367|TypeScript|2021/08/21|
|130|[lizhiyao/sentry-miniapp](https://github.com/lizhiyao/sentry-miniapp)|Sentry 小程序/小游戏 SDK：用于小程序/小游戏平台的 Sentry SDK（目前支持微信、字节跳动、支付宝、钉钉、QQ、百度小程序，微信、QQ 小游戏）|364|TypeScript|2021/09/03|
|131|[rdkmaster/jigsaw](https://github.com/rdkmaster/jigsaw)|Jigsaw七巧板 provides a set of web components based on Angular5/8/9+. The main purpose of Jigsaw is to help the application developers to construct complex & intensive interacting & user friendly web pages. Jigsaw is supporting the development of all applications of Big Data Product of ZTE.|364|TypeScript|2021/12/01|
|132|[axetroy/vm.js](https://github.com/axetroy/vm.js)|Javascript 解释器. Javascript Interpreter|364|TypeScript|2021/06/04|
|133|[xd-tayde/mcanvas](https://github.com/xd-tayde/mcanvas)|用于合成图片的canvas绘制库|363|TypeScript|2021/08/11|
|134|[alibaba/cloud-charts](https://github.com/alibaba/cloud-charts)|开箱即用的前端图表库，简单配置就能拥有漂亮的可视化图表|358|TypeScript|2021/11/08|
|135|[hydro-dev/Hydro](https://github.com/hydro-dev/Hydro)|Hydro - 新一代高效强大的信息学在线测评系统 (a.k.a. vj5)|354|TypeScript|2021/11/30|
|136|[HunterXuan/wx-tfjs-demo](https://github.com/HunterXuan/wx-tfjs-demo)|微信小程序运行 TensorFlow 的 Demo|352|TypeScript|2021/10/28|
|137|[yaklang/yakit](https://github.com/yaklang/yakit)|yak gRPC Client GUI - 集成化单兵工具平台|350|TypeScript|2021/12/01|
|138|[lycHub/ng-wyy](https://github.com/lycHub/ng-wyy)|angular8+ngrx8 🦌网易云|349|TypeScript|2021/10/18|
|139|[linjc/smooth-signature](https://github.com/linjc/smooth-signature)|H5带笔锋手写签名，支持PC端和移动端，任何前端框架均可使用|345|TypeScript|2021/11/22|
|140|[kirikayakazuto/CocosCreator_UIFrameWork](https://github.com/kirikayakazuto/CocosCreator_UIFrameWork)|基于CocosCreator的轻量框架, 主要是针对单场景的游戏管理, 将界面制作成预制体, 提供了对界面预制体的显示, 隐藏, 释放等功能, 游戏管理更简单!|336|TypeScript|2021/11/20|
|141|[enncy/online-course-script](https://github.com/enncy/online-course-script)|ocs   网课刷课脚本，帮助大学生解决网课难题，目前支持的平台：超星，智慧树|332|TypeScript|2021/11/26|
|142|[tiddly-gittly/TiddlyGit-Desktop](https://github.com/tiddly-gittly/TiddlyGit-Desktop)|TiddlyGit is an auto-git-backup, privatcy-in-mind, freely-deployed Tiddlywiki knowledgement Desktop app, with local REST API. 「 太记 」是一个基于「 太微 TiddlyWiki 」的知识管理桌面应用，能自动用Git备份、保护隐私内容、部署为博客，且可通过RESTAPI与Anki等应用连接。（迭代开发中欢迎试用，开发进度见下方链接，点 Watch 里面选 Release 可以在更新修复的时候收到通知！）(Under active development, see web ...|329|TypeScript|2021/11/15|
|143|[jetlinks/jetlinks-ui-antd](https://github.com/jetlinks/jetlinks-ui-antd)|jetlinks community ant design 演示地址：http://demo.jetlinks.cn  账号/密码: test/test123456|328|TypeScript|2021/11/30|
|144|[javaLuo/react-admin](https://github.com/javaLuo/react-admin)|动态菜单配置、权限精确到按钮、通用模块；标准后台管理系统解决方案|325|TypeScript|2021/09/28|
|145|[easychen/telechan](https://github.com/easychen/telechan)|message api for telegram bot  可供多人发送消息的 telegram 机器人 api ， 类似server酱的开源实现|325|TypeScript|2021/07/10|
|146|[yanlele/node-index](https://github.com/yanlele/node-index)|学习笔记、博文、简书、工作日常踩坑记录以及一些独立作品的汇总目录|324|TypeScript|2021/11/28|
|147|[AttoJS/vue-request](https://github.com/AttoJS/vue-request)|⚡️ Vue 3 composition API for data fetching, supports SWR, polling, error retry, cache request, pagination, etc. ⚡️ 一个能轻松帮你管理请求状态（支持SWR，轮询，错误重试，缓存，分页等）的 Vue 3 请求库|323|TypeScript|2021/10/12|
|148|[fxy5869571/blog-react](https://github.com/fxy5869571/blog-react)|基于typescript koa2 react的个人博客|321|TypeScript|2021/09/21|
|149|[IvinWu/weRequest](https://github.com/IvinWu/weRequest)|解决繁琐的小程序会话管理，一款自带登录态管理的网络请求组件。|319|TypeScript|2021/10/09|
|150|[shidenggui/bloghub](https://github.com/shidenggui/bloghub)|一群自由而有趣的灵魂，终将在此相遇   独立个人博客推荐导航|316|TypeScript|2021/11/06|
|151|[xjh22222228/tomato-work](https://github.com/xjh22222228/tomato-work)|🍅   Tomato Work for React 个人事务管理系统|314|TypeScript|2021/11/23|
|152|[antvis/antvis.github.io](https://github.com/antvis/antvis.github.io)|🔜 AntV 新站点！|312|TypeScript|2021/11/22|
|153|[MinJieLiu/react-photo-view](https://github.com/MinJieLiu/react-photo-view)|一款精致的 React 图片预览组件|310|TypeScript|2021/10/06|
|154|[topfullstack/topfullstack](https://github.com/topfullstack/topfullstack)|NodeJs+VueJs全栈开发《全栈之巅》视频网站 - 源码|306|TypeScript|2021/10/06|
|155|[apptools-lab/AppToolkit](https://github.com/apptools-lab/AppToolkit)|🐘 The Front-end Env Toolkit（前端环境管理工具）|306|TypeScript|2021/11/23|
|156|[jiayisheji/jianshu](https://github.com/jiayisheji/jianshu)|仿简书nx+nodejs+nestjs6+express+mongodb+angular8+爬虫|301|TypeScript|2021/11/25|
|157|[enylin/taiwan-id-validator](https://github.com/enylin/taiwan-id-validator)|中華民國統一編號、身分證字號驗證規則|298|TypeScript|2021/11/01|
|158|[landluck/react-ant-admin](https://github.com/landluck/react-ant-admin)|使用 ant-design react react-hook ts 开发的类 ant-design-pro 管理后台，具有完整的权限系统和配套的node + ts 的 api|297|TypeScript|2021/07/03|
|159|[Qinmei/qinVideo](https://github.com/Qinmei/qinVideo)|基于node.js开发的一套CMS后台管理系统,支持番剧,漫画,文章,弹幕等等|296|TypeScript|2021/08/12|
|160|[nimoc/fe](https://github.com/nimoc/fe)|前端理论与实践|292|TypeScript|2021/09/26|
|161|[MrXujiang/pc-Dooring](https://github.com/MrXujiang/pc-Dooring)|LowCode, PC Page Maker, PC Editor. Make PC as easy as building blocks.   让网页制作像搭积木一样简单, 轻松搭建PC页面, Web网站, PC端网站. lowcode(low-code)可视化搭建平台|291|TypeScript|2021/06/13|
|162|[notadd/ng-notadd](https://github.com/notadd/ng-notadd)|In-middle background front-end solution based on angular material 基于Angular Material的中后台前端解决方案|290|TypeScript|2021/10/06|
|163|[WangJunZzz/abp-vnext-pro](https://github.com/WangJunZzz/abp-vnext-pro)|Abp Vnext  的 Vue3 实现版本|283|TypeScript|2021/11/30|
|164|[formulahendry/vscode-ycy](https://github.com/formulahendry/vscode-ycy)|超越鼓励师 for VS Code|279|TypeScript|2021/10/06|
|165|[uniquemo/react-netease-music](https://github.com/uniquemo/react-netease-music)|React Netease Music——一个基于React、TypeScript的高仿网易云mac客户端🎵播放器。|279|TypeScript|2021/11/22|
|166|[wuhan2020/map-viz](https://github.com/wuhan2020/map-viz)|通用的地图可视化组件|276|TypeScript|2021/10/06|
|167|[intellism/vscode-comment-translate](https://github.com/intellism/vscode-comment-translate)|vscode 注释翻译插件, 不干扰正常代码，方便快速阅读源码。|275|TypeScript|2021/09/02|
|168|[1zilc/fishing-funds](https://github.com/1zilc/fishing-funds)|基金,大盘,股票,虚拟货币状态栏显示小应用,基于Electron开发,支持MacOS,Windows,Linux客户端,数据源来自天天基金,蚂蚁基金,爱基金,腾讯证券,新浪基金等|274|TypeScript|2021/12/01|
|169|[surmon-china/simple-netease-cloud-music](https://github.com/surmon-china/simple-netease-cloud-music)|🎵A simple netease music api lib. 简单、统一、轻巧的 Node.js 版网易云音乐 API|269|TypeScript|2021/09/22|
|170|[iconfont-cli/mini-program-iconfont-cli](https://github.com/iconfont-cli/mini-program-iconfont-cli)|把iconfont图标批量转换成多个平台小程序的标准组件。支持多色彩，支持自定义颜色|269|TypeScript|2021/09/14|
|171|[worldzhao/react-ui-library-tutorial](https://github.com/worldzhao/react-ui-library-tutorial)|📚React组件库搭建指南|268|TypeScript|2021/11/04|
|172|[fengkx/NodeRSSBot](https://github.com/fengkx/NodeRSSBot)|Another Telegram RSS bot  but in Node.js Telegram RSS 机器人|268|TypeScript|2021/12/01|
|173|[Maslow/laf](https://github.com/Maslow/laf)|laf 是开源的云开发框架，提供 client-db、云函数、文件管理能力，让前端成为全栈开发。使用文档：https://docs.lafyun.com|267|TypeScript|2021/11/26|
|174|[axetroy/anti-redirect](https://github.com/axetroy/anti-redirect)|:rocket:去除各搜索引擎/常用网站的重定向|263|TypeScript|2021/11/30|
|175|[fjc0k/yapi-to-typescript](https://github.com/fjc0k/yapi-to-typescript)|根据 YApi 或 Swagger 的接口定义生成 TypeScript 或 JavaScript 的接口类型及其请求函数代码。|257|TypeScript|2021/11/23|
|176|[OBKoro1/autoCommit](https://github.com/OBKoro1/autoCommit)|这是一个可以在任意时间范围自动提交commit的VSCode插件觉得插件不错的话，点击右上角给个Star⭐️呀~|257|TypeScript|2021/11/05|
|177|[veaba/ncov](https://github.com/veaba/ncov)|【在开发2.x版本，暂时关闭站点！新版大屏beta 版本，全实时数据，交互式大屏，精确到城市级，弹幕来袭】关注2019新型冠状病毒（2019-nCoV），数据可视化感染人群热点图、迁徙扩散轨迹，以提供帮助分析疫情。 愿世界安好。Focus on Wuhan 2019-nCoV, data visualization, to help analyze the epidemic situation.  May the world be well.|252|TypeScript|2021/11/11|
|178|[AnnaSearl/annar](https://github.com/AnnaSearl/annar)|优雅、简洁的 Remax 组件库|247|TypeScript|2021/08/15|
|179|[hbt-org/hearthstone-battlegrounds-tools](https://github.com/hbt-org/hearthstone-battlegrounds-tools)|炉石传说酒馆战棋插件：统计每天的战棋战绩，并通过数据分析得到你最拿手的英雄！|246|TypeScript|2021/06/12|
|180|[antfu/wenyan-lang-vscode](https://github.com/antfu/wenyan-lang-vscode)|文言 Wenyan Lang for VS Code|246|TypeScript|2021/05/02|
|181|[acccco/zebra-editor-core](https://github.com/acccco/zebra-editor-core)|一款强大、现代的可视化编辑器。|238|TypeScript|2021/10/27|
|182|[zmide/study.zmide.com](https://github.com/zmide/study.zmide.com)|全能搜题网页端源代码，全能搜题项目是一个基于开源社区公开贡献的永久免费搜题系统。|238|TypeScript|2021/11/21|
|183|[wkl007/vue-wechat-login](https://github.com/wkl007/vue-wechat-login)|Vue 3.0 + TypeScript 微信网页授权登录示例|237|TypeScript|2021/09/25|
|184|[MrXujiang/v6.dooring.public](https://github.com/MrXujiang/v6.dooring.public)|可视化大屏解决方案, 提供一套可视化编辑引擎, 助力个人或企业轻松定制自己的可视化大屏应用.|235|TypeScript|2021/08/08|
|185|[WindrunnerMax/TKScript](https://github.com/WindrunnerMax/TKScript)|油猴插件|234|TypeScript|2021/11/22|
|186|[LeoEatle/git-webhook-wework-robot](https://github.com/LeoEatle/git-webhook-wework-robot)|企业微信github/gitlab机器人|229|TypeScript|2021/08/11|
|187|[originjs/webpack-to-vite](https://github.com/originjs/webpack-to-vite)|convert a webpack project to vite project. 将 webpack 项目转换为 vite 项目。|228|TypeScript|2021/10/30|
|188|[dohooo/react-native-reanimated-carousel](https://github.com/dohooo/react-native-reanimated-carousel)|Simple React Native carousel component,fully implemented using reanimated v2,support to IOS/Android.（轮播图、swiper）|227|TypeScript|2021/11/28|
|189|[visiky/resume](https://github.com/visiky/resume)|🚀 在线简历生成器|221|TypeScript|2021/10/08|
|190|[2234839/web-font](https://github.com/2234839/web-font)|字体裁剪工具|221|TypeScript|2021/08/19|
|191|[justjavac/weibo-trending-hot-search](https://github.com/justjavac/weibo-trending-hot-search)|微博热搜榜，记录从 2020-11-24 日开始的微博热门搜索。每小时抓取一次数据，按天归档。|219|TypeScript|2021/12/01|
|192|[seawind8888/Nobibi](https://github.com/seawind8888/Nobibi)|一款基于Next.js+mongo的轻量级开源社区（open community by Next.js & mongo）|218|TypeScript|2021/06/16|
|193|[Kanaries/Rath](https://github.com/Kanaries/Rath)|Automated data exploratory analysis and visualization tools.自动化数据探索分析和智能可视化设计应用.|218|TypeScript|2021/11/29|
|194|[fkworld/cocos-game-framework](https://github.com/fkworld/cocos-game-framework)|cocos-creator游戏框架，typescript版本。|216|TypeScript|2021/09/22|
|195|[vincentzyc/vue3-demo](https://github.com/vincentzyc/vue3-demo)|基于 vue3 + vant3 的 H5移动端 demo|216|TypeScript|2021/08/17|
|196|[xuejianxianzun/PixivFanboxDownloader](https://github.com/xuejianxianzun/PixivFanboxDownloader)|A Chrome extension for downloading files on Pixiv Fanbox in batches.  Chrome 扩展，用于批量下载 Pixiv Fanbox 上的文件。|215|TypeScript|2021/09/02|
|197|[rxdrag/dragit](https://github.com/rxdrag/dragit)|Build front end without the tears, just drag it.不需要代码的可视化前端，基于React Mui构建。|213|TypeScript|2021/11/15|
|198|[buqiyuan/vite-vue3-admin](https://github.com/buqiyuan/vite-vue3-admin)|基于vite2.0 + vue3.0 + ant-design-vue2.0 + typescript hooks 的基础后台管理系统模板 RBAC的权限系统, JSON Schema动态表单,动态表格,漂亮锁屏界面|210|TypeScript|2021/09/05|
|199|[Luncher/alipay](https://github.com/Luncher/alipay)|Alipay Node.js SDK 基于最新版蚂蚁金服 支付宝开发文档|209|TypeScript|2021/09/21|
|200|[szpoppy/rimjs](https://github.com/szpoppy/rimjs)|JS周边|209|TypeScript|2021/11/23|

⬆ [回到目录](#内容目录)

<br/>

## Vue

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[bailicangdu/vue2-elm](https://github.com/bailicangdu/vue2-elm)|基于 vue2 + vuex 构建一个具有 45 个页面的大型单页面应用|37.2k|Vue|2021/09/13|
|2|[lin-xin/vue-manage-system](https://github.com/lin-xin/vue-manage-system)|基于Vue3 + Element Plus 的后台管理系统解决方案|14.1k|Vue|2021/11/28|
|3|[bailicangdu/vue2-manage](https://github.com/bailicangdu/vue2-manage)|基于 vue + element-ui 的后台管理系统|11.3k|Vue|2021/07/15|
|4|[weilanwl/ColorUI](https://github.com/weilanwl/ColorUI)|鲜亮的高饱和色彩，专注视觉的小程序组件库|10.9k|Vue|2021/11/17|
|5|[chuzhixin/vue-admin-beautiful-pro](https://github.com/chuzhixin/vue-admin-beautiful-pro)|🚀🚀🚀vue3 admin,vue3.0 admin,vue后台管理,vue-admin,vue3.0-admin,admin,vue-admin,vue-element-admin,ant-design，vue-admin-beautiful-pro,vab admin pro,vab admin plus主线版本基于element-plus、element-ui、ant-design-vue三者并行开发维护，同时支持电脑，手机，平板，切换分支查看不同的vue版本，element-plus版本已发布(vue3,vue3.0,vue,vue3.x,vue.js)|10.8k|Vue|2021/11/26|
|6|[cuiocean/ZY-Player](https://github.com/cuiocean/ZY-Player)|▶️ 跨平台桌面端视频资源播放器.简洁无广告.免费高颜值. 🎞|10.4k|Vue|2021/10/13|
|7|[macrozheng/mall-admin-web](https://github.com/macrozheng/mall-admin-web)|mall-admin-web是一个电商后台管理系统的前端项目，基于Vue+Element实现。 主要包括商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等功能。|9.1k|Vue|2021/08/09|
|8|[chaitin/xray](https://github.com/chaitin/xray)|一款完善的安全评估工具，支持常见 web 安全问题扫描和自定义 poc   使用之前务必先阅读文档|6.1k|Vue|2021/11/29|
|9|[DataV-Team/DataV](https://github.com/DataV-Team/DataV)|Vue数据可视化组件库（类似阿里DataV，大屏数据展示），提供SVG的边框及装饰、图表、水位图、飞线图等组件，简单易用，长期更新(React版已发布)|5.8k|Vue|2021/11/29|
|10|[JakHuang/form-generator](https://github.com/JakHuang/form-generator)|:sparkles:Element UI表单设计及代码生成器|5.4k|Vue|2021/11/21|
|11|[GavinZhuLei/vue-form-making](https://github.com/GavinZhuLei/vue-form-making)|A visual form designer/generator base on Vue.js, make form development simple and efficient.（基于Vue的可视化表单设计器，让表单开发简单而高效。）|4.5k|Vue|2021/09/03|
|12|[fenixsoft/awesome-fenix](https://github.com/fenixsoft/awesome-fenix)|讨论如何构建一套可靠的大型分布式系统|4.4k|Vue|2021/11/23|
|13|[ymm-tech/gods-pen](https://github.com/ymm-tech/gods-pen)|基于vue的高扩展在线网页制作平台，可自定义组件，可添加脚本，可数据统计。A mobile page builder/editor, similar with amolink. |4.0k|Vue|2021/10/06|
|14|[jdf2e/nutui](https://github.com/jdf2e/nutui)|京东风格的移动端 Vue2、Vue3 组件库 (A Vue.js UI Toolkit for Mobile Web)|3.8k|Vue|2021/11/30|
|15|[herozhou/vue-framework-wz](https://github.com/herozhou/vue-framework-wz)|👏vue后台管理框架👏|3.7k|Vue|2021/08/12|
|16|[wdlhao/vue2-element-touzi-admin](https://github.com/wdlhao/vue2-element-touzi-admin)| 基于vue2.0 +vuex+ element-ui后台管理系统 |3.5k|Vue|2021/08/11|
|17|[crmeb/CRMEB](https://github.com/crmeb/CRMEB)|🔥🔥🔥 CRMEB打通版v4免费开源商城系统，uni-app+thinkphp6框架商城，系统可商用；包含小程序商城、H5商城、公众号商城、PC商城，支持分销、拼团、砍价、秒杀、优惠券、积分、会员等级、小程序直播、可视化设置，前后端分离，方便二开，更有详细使用文档、接口文档、数据字典、二开文档/视频教程，欢迎大家提出宝贵意见和建议|3.4k|Vue|2021/11/18|
|18|[elunez/eladmin-web](https://github.com/elunez/eladmin-web)|eladmin前端源码，项目基于 Spring Boot 2.1.0 、 Spring Boot Jpa、 Spring Security、Redis、Vue的前后端分离后台管理系统， 权限控制采用 RBAC，菜单动态路由|3.4k|Vue|2021/11/27|
|19|[umicro/uView](https://github.com/umicro/uView)|uView UI，是uni-app生态最优秀的UI框架，全面的组件和便捷的工具会让您信手拈来，如鱼得水|3.3k|Vue|2021/11/30|
|20|[ZyqGitHub1/h-player-v2](https://github.com/ZyqGitHub1/h-player-v2)|资源采集站在线播放|3.0k|Vue|2021/10/13|
|21|[newbee-ltd/newbee-mall-vue3-app](https://github.com/newbee-ltd/newbee-mall-vue3-app)|🔥 🎉Vue3 全家桶 + Vant 搭建大型单页面商城项目，新蜂商城 Vue3 版本，技术栈为 Vue 3.0 + Vue-Router 4.0 + Vuex 4.0 + Vant 3.0。|2.9k|Vue|2021/06/28|
|22|[shfshanyue/Daily-Question](https://github.com/shfshanyue/Daily-Question)|互联网大厂内推及大厂面经整理，并且每天一道面试题推送。每天五分钟，半年大厂中|2.9k|Vue|2021/11/30|
|23|[lybenson/bilibili-vue](https://github.com/lybenson/bilibili-vue)|前端vue+后端koa，全栈式开发bilibili首页|2.6k|Vue|2021/10/13|
|24|[Exrick/xmall-front](https://github.com/Exrick/xmall-front)|基于Vue开发的XMall商城前台页面 PC端|2.3k|Vue|2021/08/04|
|25|[Geek-James/ddBuy](https://github.com/Geek-James/ddBuy)|🎉Vue全家桶+Vant 搭建大型单页面电商项目.http://ddbuy.7-orange.cn|2.2k|Vue|2021/11/13|
|26|[open-source-labs/OverVue](https://github.com/open-source-labs/OverVue)|Prototyping Tool For Vue Devs 适用于Vue的原型工具|2.1k|Vue|2021/11/27|
|27|[github1586/nuxt-bnhcp](https://github.com/github1586/nuxt-bnhcp)|nuxt、express、vue、mysql、redis、nginx、socket.io (实战商城)|1.9k|Vue|2021/09/02|
|28|[woai3c/visual-drag-demo](https://github.com/woai3c/visual-drag-demo)|一个低代码（可视化拖拽）教学项目|1.9k|Vue|2021/11/10|
|29|[dcloudio/hello-uniapp](https://github.com/dcloudio/hello-uniapp)|uni-app框架演示示例|1.9k|Vue|2021/12/01|
|30|[sunzongzheng/music](https://github.com/sunzongzheng/music)|electron跨平台音乐播放器；可搜网易云、QQ音乐、虾米音乐；支持QQ、微博、Github登录，云歌单; 支持一键导入音乐平台歌单|1.9k|Vue|2021/10/13|
|31|[dingyong0214/ThorUI-uniapp](https://github.com/dingyong0214/ThorUI-uniapp)|ThorUI组件库，轻量、简洁的移动端组件库。组件文档地址：https://thorui.cn/doc/    。    最近更新时间：2021-10-01|1.8k|Vue|2021/10/01|
|32|[biaochenxuying/blog-vue-typescript](https://github.com/biaochenxuying/blog-vue-typescript)|Vue3 + TypeScript + Vite2 + Vuex4 + Vue-Router4 + element-plus 支持 markdown 渲染的博客前台展示|1.8k|Vue|2021/08/29|
|33|[zhaohaodang/vue-WeChat](https://github.com/zhaohaodang/vue-WeChat)|:fire: 基于Vue2.0高仿微信App的单页应用|1.8k|Vue|2021/10/07|
|34|[x2rr/funds](https://github.com/x2rr/funds)|自选基金助手是一款Chrome扩展，用来快速获取关注基金的实时数据，查看自选基金的实时估值情况|1.8k|Vue|2021/10/11|
|35|[sl1673495/vue-netease-music](https://github.com/sl1673495/vue-netease-music)|🎵 基于 Vue2、Vue-CLI3 的高仿网易云 mac 客户端播放器（PC） Online Music Player|1.8k|Vue|2021/10/06|
|36|[0xbug/Hawkeye](https://github.com/0xbug/Hawkeye)|GitHub 泄露监控系统(GitHub Sensitive Information Leakage Monitor Spider)|1.7k|Vue|2021/11/15|
|37|[fofapro/vulfocus](https://github.com/fofapro/vulfocus)|🚀Vulfocus 是一个漏洞集成平台，将漏洞环境 docker 镜像，放入即可使用，开箱即用。|1.7k|Vue|2021/11/09|
|38|[Yin-Hongwei/music-website](https://github.com/Yin-Hongwei/music-website)|🎧 Vue + SpringBoot + MyBatis 音乐网站|1.6k|Vue|2021/09/08|
|39|[maomao1996/Vue-mmPlayer](https://github.com/maomao1996/Vue-mmPlayer)|🎵 基于 Vue 的在线音乐播放器（PC） Online music player|1.6k|Vue|2021/10/13|
|40|[a54552239/pearProject](https://github.com/a54552239/pearProject)|pear，梨子，轻量级的在线项目/任务协作系统，远程办公协作|1.6k|Vue|2021/08/27|
|41|[xugaoyi/vuepress-theme-vdoing](https://github.com/xugaoyi/vuepress-theme-vdoing)|🚀一款简洁高效的VuePress知识管理&博客(blog)主题|1.5k|Vue|2021/11/29|
|42|[xiaoxian521/vue-pure-admin](https://github.com/xiaoxian521/vue-pure-admin)|🔥 ✨✨ ✨ Vue3.0+TypeScript+Vite2.0+Element-Plus编写的一套后台管理系统|1.5k|Vue|2021/12/01|
|43|[inoutcode/ethereum_book](https://github.com/inoutcode/ethereum_book)|精通以太坊 （中文版）|1.5k|Vue|2021/09/19|
|44|[KuangPF/mpvue-weui](https://github.com/KuangPF/mpvue-weui)|用 vue 写小程序，基于 mpvue 框架重写 weui。|1.5k|Vue|2021/09/21|
|45|[HongqingCao/GitDataV](https://github.com/HongqingCao/GitDataV)|基于Vue框架构建的github数据可视化平台|1.4k|Vue|2021/10/06|
|46|[GitHub-Laziji/VBlog](https://github.com/GitHub-Laziji/VBlog)|使用GitHub API 搭建一个可动态发布文章的博客|1.4k|Vue|2021/11/30|
|47|[haoziqaq/varlet](https://github.com/haoziqaq/varlet)|基于Vue3的Material design风格移动端组件库 Material design mobile component library for Vue3|1.4k|Vue|2021/11/29|
|48|[jae-jae/Userscript-Plus](https://github.com/jae-jae/Userscript-Plus)|:monkey: Show current site all UserJS，The easier way to install UserJs for Tampermonkey. 显示当前网站的所有可用Tampermonkey脚本|1.3k|Vue|2021/08/11|
|49|[chenxuan0000/vue-seamless-scroll](https://github.com/chenxuan0000/vue-seamless-scroll)| :beginner:A simple, seamless scrolling for Vue.js  vue无缝滚动component|1.3k|Vue|2021/10/16|
|50|[myide/see](https://github.com/myide/see)|基于开源组件（Inception & SQLAdvisor & SOAR）的SQL审核&SQL优化的Web平台|1.3k|Vue|2021/11/30|
|51|[pipipi-pikachu/PPTist](https://github.com/pipipi-pikachu/PPTist)|基于 Vue3.x + TypeScript 的在线演示文稿（幻灯片）应用，还原了大部分 Office PowerPoint 常用功能，实现在线PPT的编辑、演示。支持导出PPT文件。|1.3k|Vue|2021/11/27|
|52|[newbee-ltd/vue3-admin](https://github.com/newbee-ltd/vue3-admin)|🔥 🎉 Vue 3.0 + Vite 2.0 + Vue-Router 4.0 + Element-Plus + Echarts 5.0 + Axios 开发的后台管理系统|1.3k|Vue|2021/09/13|
|53|[jsososo/NeteaseMusic](https://github.com/jsososo/NeteaseMusic)|网易云音乐 & QQ音乐 & 咪咕音乐 第三方 web端 (可播放 vip、下架歌曲)|1.2k|Vue|2021/07/13|
|54|[dcloudio/uni-ui](https://github.com/dcloudio/uni-ui)|基于uni-app的ui框架|1.1k|Vue|2021/11/26|
|55|[eshengsky/iBlog](https://github.com/eshengsky/iBlog)|基于 Node.js 的开源个人博客系统，采用 Nuxt + Vue + TypeScript 技术栈。|1.1k|Vue|2021/08/12|
|56|[loveRandy/vue-cli3.0-vueadmin](https://github.com/loveRandy/vue-cli3.0-vueadmin)|基于vue-cli3.0+vue+elementUI+vuex+axios+权限管理的后台管理系统|1.1k|Vue|2021/10/06|
|57|[GoodManWEN/GoodManWEN.github.io](https://github.com/GoodManWEN/GoodManWEN.github.io)|📕 A website simulating linux system's GUI, using theme of Deepin distro. 网页模拟桌面|1.1k|Vue|2021/07/28|
|58|[jekip/naive-ui-admin](https://github.com/jekip/naive-ui-admin)|Naive Ui Admin 是一个基于 vue3,vite2,TypeScript 的中后台解决方案，它使用了最新的前端技术栈，并提炼了典型的业务模型，页面，包括二次封装组件、动态菜单、权限校验、粒子化权限控制等功能，它可以帮助你快速搭建企业级中后台项目，相信不管是从新技术使用还是其他方面，都能帮助到你，持续更新中。|1.1k|Vue|2021/11/26|
|59|[newbee-ltd/newbee-mall-vue-app](https://github.com/newbee-ltd/newbee-mall-vue-app)|🔥 🎉Vue2 全家桶 + Vant 搭建大型单页面商城项目。新蜂商城前后端分离版本-前端Vue项目源码|1.1k|Vue|2021/10/07|
|60|[PowerDos/Mall-Vue](https://github.com/PowerDos/Mall-Vue)|基于Vue+Vuex+iView的电子商城网站|1.1k|Vue|2021/08/18|
|61|[stavyan/TinyShop-UniApp](https://github.com/stavyan/TinyShop-UniApp)|基于 RageFrame2 的一款免费开源的基础商城销售功能的开源微商城。|1.0k|Vue|2021/10/06|
|62|[XPoet/picx](https://github.com/XPoet/picx)|基于 GitHub API 开发的图床神器，图片外链使用 jsDelivr 进行 CDN 加速。免下载、免安装，打开网站即可直接使用。免费、稳定、高效。|988|Vue|2021/10/12|
|63|[RainManGO/vue3-composition-admin](https://github.com/RainManGO/vue3-composition-admin)|🎉  基于vue3 的管理端模板(Vue3 TS Vuex4  element-plus vue-i18n-next  composition-api)  vue3-admin   vue3-ts-admin|983|Vue|2021/11/26|
|64|[lavyun/vue-demo-kugou](https://github.com/lavyun/vue-demo-kugou)|酷狗webapp demo(vue2.0+vue-router+vuex)|958|Vue|2021/10/06|
|65|[jackchen0120/vueDataV](https://github.com/jackchen0120/vueDataV)|基于Vue + Echarts 构建的数据可视化平台，酷炫大屏展示模板和组件库，持续更新各行各业实用模板和炫酷小组件。|909|Vue|2021/10/07|
|66|[IFmiss/vue-music](https://github.com/IFmiss/vue-music)|基于vue2.0的网易云音乐播放器，api来自于NeteaseCloudMusicApi，v2.0为最新版本|853|Vue|2021/05/09|
|67|[topfullstack/node-vue-moba](https://github.com/topfullstack/node-vue-moba)|Node.js (Express.js) + Vue.js (Element UI) 全栈开发王者荣耀手机端官网和管理后台|843|Vue|2021/10/06|
|68|[moxi624/mogu_blog_v2](https://github.com/moxi624/mogu_blog_v2)|蘑菇博客(MoguBlog)，一个基于微服务架构的前后端分离博客系统。Web端使用Vue + Element , 移动端使用uniapp和ColorUI。后端使用Spring cloud + Spring boot + mybatis-plus进行开发，使用 Jwt + Spring Security做登录验证和权限校验，使用ElasticSearch和Solr作为全文检索服务，使用Github Actions完成博客的持续集成，使用ELK收集博客日志，文件支持上传七牛云和Minio，支持Docker Compose脚本一键部署。|834|Vue|2021/11/30|
|69|[sscfaith/avue-form-design](https://github.com/sscfaith/avue-form-design)|本项目是一款基于 Avue 的表单设计器，拖拽式操作让你快速构建一个表单。|825|Vue|2021/11/17|
|70|[lsgwr/spring-boot-online-exam](https://github.com/lsgwr/spring-boot-online-exam)|基于Spring Boot的在线考试系统(预览地址 http://129.211.88.191 ，账户分别是admin、teacher、student，密码是admin123)，也有Python实现|820|Vue|2021/11/21|
|71|[wuyawei/Vchat](https://github.com/wuyawei/Vchat)|💘🍦🙈Vchat — 从头到脚，撸一个社交聊天系统（vue + node + mongodb）|808|Vue|2021/08/11|
|72|[wmhello/laravel_template_with_vue](https://github.com/wmhello/laravel_template_with_vue)|laravel6和vue.js结合的前后端分离项目模板。包含接口端、管理端、小程序和微信公众号端，绝对是你做外包或者自建项目的首选模板。模板内容包括基础的用户管理和权限管理、日志管理、集成第三方登录，整合laravel-echo-server 实现了websocket 做到了消息的实时推送，并在此基础上，实现了聊天室和客服功能。|806|Vue|2021/11/29|
|73|[zhengguorong/h5maker](https://github.com/zhengguorong/h5maker)|h5编辑器类似maka、易企秀 账号/密码：admin|791|Vue|2021/11/16|
|74|[dream2023/vue-ele-form](https://github.com/dream2023/vue-ele-form)|基于element-ui的数据驱动表单组件|789|Vue|2021/10/23|
|75|[lss5270/vue-admin-spa](https://github.com/lss5270/vue-admin-spa)|基于vue2.0生态的后台管理系统模板（spa）。 a vue management system template based on ：vue2.0 + vue-router + vuex + element-ui +ES6+ webpack + npm。|782|Vue|2021/07/01|
|76|[TangSY/echarts-map-demo](https://github.com/TangSY/echarts-map-demo)|echarts地图geoJson行政边界数据的实时获取与应用，省市区县多级联动下钻，真正意义的下钻至县级【附最新geoJson文件下载】|759|Vue|2021/10/06|
|77|[fengT-T/996_list](https://github.com/fengT-T/996_list)|996 公司展示、讨论|758|Vue|2021/10/06|
|78|[liupan1890/xiaobaiyang2](https://github.com/liupan1890/xiaobaiyang2)|6pan 6盘小白羊 第二版 vue3+antd+typescript|706|Vue|2021/05/27|
|79|[blogwy/BilibiliVideoDownload](https://github.com/blogwy/BilibiliVideoDownload)|跨平台下载bilibili视频桌面端软件，支持windows、macOS、Linux|686|Vue|2021/11/06|
|80|[wsydxiangwang/Mood](https://github.com/wsydxiangwang/Mood)|Vue的Nuxt.js服务端渲染框架，NodeJS为后端的全栈项目，Docker一键部署，面向小白的完美博客系统|678|Vue|2021/11/30|
|81|[bxm0927/vue-meituan](https://github.com/bxm0927/vue-meituan)|:hamburger: :meat_on_bone: :fork_and_knife:  基于Vue 全家桶 (2.x)制作的美团外卖APP |677|Vue|2021/08/02|
|82|[Exrick/xboot-front](https://github.com/Exrick/xboot-front)|基于Vue+iView Admin开发的XBoot前后端分离开放平台前端 权限可控制至按钮显示 动态路由权限菜单/多语言/简洁美观 前后端分离|667|Vue|2021/07/02|
|83|[CS-Tao/whu-library-seat](https://github.com/CS-Tao/whu-library-seat)|武汉大学图书馆助手 - 桌面端|653|Vue|2021/11/28|
|84|[niefy/wx-manage](https://github.com/niefy/wx-manage)|🔥微信公众号管理系统，包含公众号菜单管理🗄、自动回复🗨、素材管理📂、模板消息☘、粉丝管理🤹‍♂️等功能，前后端都开源免费🛩|634|Vue|2021/10/06|
|85|[zhangyuang/fe-dev-playbook](https://github.com/zhangyuang/fe-dev-playbook)|教你如何打造舒适、高效、时尚的前端开发环境|628|Vue|2021/09/29|
|86|[wangy8961/flask-vuejs-madblog](https://github.com/wangy8961/flask-vuejs-madblog)|基于 Flask 和 Vue.js 前后端分离的微型博客项目，支持多用户、Markdown文章（喜欢/收藏文章）、粉丝关注、用户评论（点赞）、动态通知、站内私信、黑名单、邮件支持、管理后台、权限管理、RQ任务队列、Elasticsearch全文搜索、Linux VPS部署、Docker容器部署等|624|Vue|2021/06/02|
|87|[woai3c/vue-admin-template](https://github.com/woai3c/vue-admin-template)|Vue 轻量级后台管理系统基础模板|610|Vue|2021/10/06|
|88|[ddiu8081/ChartFun](https://github.com/ddiu8081/ChartFun)|🎲数据大屏可视化编辑器|609|Vue|2021/10/08|
|89|[Couy69/vue-idle-game](https://github.com/Couy69/vue-idle-game)|一个全随机的刷装备小游戏|603|Vue|2021/07/18|
|90|[SNFocus/approvalFlow](https://github.com/SNFocus/approvalFlow)|基于form-generator，仿钉钉审批流程创建（表单创建/流程节点可视化配置/必填条件及校验）|598|Vue|2021/11/15|
|91|[YXJ2018/SpringBoot-Vue-OnlineExam](https://github.com/YXJ2018/SpringBoot-Vue-OnlineExam)|在线考试系统，springboot+vue前后端分离的一个项目.|593|Vue|2021/10/06|
|92|[iamdarcy/hioshop-admin](https://github.com/iamdarcy/hioshop-admin)|海风小店,开源商城,微信小程序商城管理后台,后台管理,VUE|593|Vue|2021/10/13|
|93|[heyui/heyui-admin](https://github.com/heyui/heyui-admin)|基于 vue 和 heyui 组件库的中后端系统 https://admin.heyui.top|592|Vue|2021/10/06|
|94|[go-admin-team/go-admin-ui](https://github.com/go-admin-team/go-admin-ui)|基于Gin + Vue + Element UI的前后端分离权限管理系统的前端模块|587|Vue|2021/09/17|
|95|[leadream/wedding-invitation-for-programmers](https://github.com/leadream/wedding-invitation-for-programmers)|程序猿的婚礼邀请函。|585|Vue|2021/10/31|
|96|[Tsuk1ko/bilibili-live-chat](https://github.com/Tsuk1ko/bilibili-live-chat)|📽️ 无后端的仿 YouTube Live Chat 风格的简易 Bilibili 弹幕姬|581|Vue|2021/11/10|
|97|[anjoy8/Blog.Admin](https://github.com/anjoy8/Blog.Admin)|✨ 基于vue 的管理后台，配合Blog.Core与Blog.Vue等多个项目使用|573|Vue|2021/10/09|
|98|[StavinLi/Workflow](https://github.com/StavinLi/Workflow)|仿钉钉审批流程设置|567|Vue|2021/10/09|
|99|[CS-Tao/whu-library-seat-mobile](https://github.com/CS-Tao/whu-library-seat-mobile)|武汉大学图书馆助手 - 移动端|561|Vue|2021/08/11|
|100|[mafengwo/vue-drag-tree-table](https://github.com/mafengwo/vue-drag-tree-table)|vue 可以拖拽排序的树形表格|556|Vue|2021/10/30|
|101|[hai-27/vue-store](https://github.com/hai-27/vue-store)|基于Vue+Vue-Router+Vuex+Element-ui+axios，参考小米商城，实现的电商项目。|555|Vue|2021/10/06|
|102|[CNOliverZhang/PotatofieldImageToolkit](https://github.com/CNOliverZhang/PotatofieldImageToolkit)|一个适用于摄影从业者/爱好者、设计师等创意行业从业者的图像工具箱。|550|Vue|2021/10/16|
|103|[hilanmiao/LanMiaoDesktop](https://github.com/hilanmiao/LanMiaoDesktop)|一个完整electron桌面记账程序，技术栈主要使用electron-vue+vuetify。开机自动启动，自动更新，托盘最小化，闪烁等常用功能，Nsis制作漂亮的安装包。|535|Vue|2021/10/13|
|104|[Codennnn/vue-color-avatar](https://github.com/Codennnn/vue-color-avatar)|🥳 一个纯前端实现的头像生成网站，使用 Vite + Vue3 开发|534|Vue|2021/10/31|
|105|[daoshengfu/Vue-NeteaseCloud-WebMusicApp](https://github.com/daoshengfu/Vue-NeteaseCloud-WebMusicApp)|Vue高仿网易云音乐，基本实现网易云所有音乐、MV相关功能，现已更新到第二版，仅用于学习，下面有详细教程。 |532|Vue|2021/10/07|
|106|[F-loat/ithome-lite](https://github.com/F-loat/ithome-lite)|🥛 IT之家第三方小程序版客户端（使用 mpvue 开发，兼容 web）|530|Vue|2021/10/06|
|107|[hql7/tree-transfer](https://github.com/hql7/tree-transfer)|一个基于vue和element-ui的树形穿梭框及邮件通讯录。A tree shaped shuttle box assembly based on Vue and element-ui.  Vuecli3版本见https://github.com/hql7/wl-tree-transfer  示例见->|526|Vue|2021/10/06|
|108|[yangyuji/h5-factory](https://github.com/yangyuji/h5-factory)|h5制作，移动端专题活动页面可视化编辑|517|Vue|2021/11/17|
|109|[pandaGao/bilibili-live-helper](https://github.com/pandaGao/bilibili-live-helper)|Bilibili直播弹幕库 for Mac / Windows / Linux|514|Vue|2021/10/13|
|110|[geekskai/vue3-jd-h5](https://github.com/geekskai/vue3-jd-h5)|:fire: Based on vue3.0.0, vant3.0.0, vue-router v4.0.0-0, vuex^4.0.0-0, vue-cli3, mockjs, imitating Jingdong Taobao, mobile H5 e-commerce platform! 基于vue3.0.0 ,vant3.0.0,vue-router v4.0.0-0, vuex^4.0.0-0,vue-cli3,mockjs,仿京东淘宝的,移动端H5电商平台!|487|Vue|2021/10/17|
|111|[fengli01/vue-bpmn-element](https://github.com/fengli01/vue-bpmn-element)|bpmn.js流程设计器组件，基于vue-elementui美化属性面板，满足90%以上的业务需求|483|Vue|2021/07/20|
|112|[Neveryu/vue-cms](https://github.com/Neveryu/vue-cms)|基于 Vue 和 ElementUI 构建的一个企业级后台管理系统|481|Vue|2021/07/14|
|113|[ITmonkey-cn/shopro-uniapp](https://github.com/ITmonkey-cn/shopro-uniapp)|Shopro分销商城 uniapp前端开源代码，一款落地生产的 基于uni-app的多端商城。使用文档：https://gitee.com/itmonkey-cn/shopro.git|479|Vue|2021/10/25|
|114|[jeecgboot/jeecg-uniapp](https://github.com/jeecgboot/jeecg-uniapp)|JEECG BOOT APP 移动解决方案，采用uniapp框架，一份代码多终端适配，同时支持APP、小程序、H5！实现了与JeecgBoot平台完美对接的移动解决方案！目前已经实现登录、用户信息、通讯录、公告、移动首页、九宫格等基础功能。|474|Vue|2021/07/28|
|115|[bullteam/zeus-admin](https://github.com/bullteam/zeus-admin)|Zeus基于Golang Gin +casbin，致力于做企业统一权限&账号中心管理系统。包含账号管理，数据权限，功能权限，应用管理，多数据库适配，可docker 一键运行。社区活跃，版本迭代快，加群免费技术支持。|469|Vue|2021/09/30|
|116|[ovenslove/vue-mdEditor](https://github.com/ovenslove/vue-mdEditor)|基于VUE的markdown文本编辑器|469|Vue|2021/07/23|
|117|[vincentzyc/form-design](https://github.com/vincentzyc/form-design)|动态表单页面设计--自动生成页面|456|Vue|2021/10/06|
|118|[hql7/wl-micro-frontends](https://github.com/hql7/wl-micro-frontends)|Micro front end practical project tutorial. 微前端项目实战vue项目。基于vue3.0&qiankun2.0进阶版：https://github.com/wl-ui/wl-mfe|456|Vue|2021/10/06|
|119|[tower1229/Vue-Giant-Tree](https://github.com/tower1229/Vue-Giant-Tree)|🌳 巨树：基于ztree封装的Vue树形组件，轻松实现海量数据的高性能渲染。|455|Vue|2021/05/20|
|120|[powerdong/Music-player](https://github.com/powerdong/Music-player)|Vue高仿网易云音乐(Vue入门实践)——在线预览 -- 暂时停止|454|Vue|2021/10/06|
|121|[cd-dongzi/vue-project](https://github.com/cd-dongzi/vue-project)|Vue实战项目|445|Vue|2021/10/07|
|122|[seeksdream/relation-graph](https://github.com/seeksdream/relation-graph)|Vue 关联关系图谱组件，可以展示如组织机构图谱、股权架构图谱、集团关系图谱等知识图谱，可提供多种图谱布局，包括树状布局、中心布局、力学布局自动布局等。Vue component for relationship graph , which can display knowledge graphs, such as organization graph, equity structure graph, group relationship graph,|444|Vue|2021/10/20|
|123|[zhaoyiming0803/VueNode](https://github.com/zhaoyiming0803/VueNode)|VueNode 是一套基于 TypeScript + Vue.js@3.x + Node.js + MySQL 的前后端分离项目。|435|Vue|2021/09/29|
|124|[a7650/h5-editor](https://github.com/a7650/h5-editor)|📕h5可视化编辑器，支持添加图片/文本/形状等，拥有图层/参考线/标尺/自动吸附对齐等功能|429|Vue|2021/09/17|
|125|[miyuesc/bpmn-process-designer](https://github.com/miyuesc/bpmn-process-designer)|Base on Vue 2.x and ElementUI，基于 Bpmn.js、Vue 2.x 和 ElementUI 的流程编辑器（前端部分），支持监听器，扩展属性，表单等配置，可自由扩展|429|Vue|2021/11/30|
|126|[wjkang/d2-admin-pm](https://github.com/wjkang/d2-admin-pm)|基于 d2-admin的RBAC权限管理解决方案|418|Vue|2021/05/09|
|127|[likaia/chat-system](https://github.com/likaia/chat-system)|本项目是一个在线聊天系统，最大程度的还原了Mac客户端QQ。|403|Vue|2021/10/06|
|128|[anjoy8/Blog.Vue](https://github.com/anjoy8/Blog.Vue)|☘ 一个vue的个人博客项目，配合.net core api教程，打造前后端分离|397|Vue|2021/11/28|
|129|[arkntools/arknights-toolbox](https://github.com/arkntools/arknights-toolbox)|🔨 Arknights Toolbox, all servers are supported. 明日方舟工具箱，支持中台美日韩服|395|Vue|2021/11/29|
|130|[ifzc/Shkjem](https://github.com/ifzc/Shkjem)|基于Vue&ElementUI的企业官网|394|Vue|2021/10/06|
|131|[TyCoding/tumo-vue](https://github.com/TyCoding/tumo-vue)|Tumo Blog For Vue.js. 前后端分离|390|Vue|2021/11/01|
|132|[wmz1930/Jeebase](https://github.com/wmz1930/Jeebase)|    Jeebase是一款前后端分离的开源开发框架，基于springboot+vue（vue-element-admin/Ant Design Pro Vue）开发，一套SpringBoot后台，两套前端页面，可以自由选择基于ElementUI或者AntDesign的前端界面。二期会整合react前端框架（Ant Design React）。在实际应用中已经使用这套框架开发了CMS网站系统，社区论坛系统，微信小程序，微信服务号等，后面会逐步整理开源。 本项目主要目的在于整合主流技术框架，寻找应用最佳项目实践方案，实现可直接使用的快速开发框架。|388|Vue|2021/10/13|
|133|[L-noodle/vue-big-screen](https://github.com/L-noodle/vue-big-screen)|一个基于 vue、datav、Echart 框架的大数据可视化（大屏展示）模板，实现大数据可视化。通过 vue 组件实现数据动态刷新渲染，内部图表可自由替换。部分图表使用 DataV 自带组件，可自由进行更改（ps：最新的更新请前往码云查看，下面有链接）。|384|Vue|2021/10/06|
|134|[MrZHLF/vue-admin](https://github.com/MrZHLF/vue-admin)|vue-cli3.0后台管理模板|382|Vue|2021/11/30|
|135|[Sandop/NuxtPC](https://github.com/Sandop/NuxtPC)|基于Nuxt的企业官网|382|Vue|2021/08/11|
|136|[IFmiss/vue-website](https://github.com/IFmiss/vue-website)|:cake: 想用vue把我现在的个人网站重新写一下，新的风格，新的技术，什么都是新的！|380|Vue|2021/05/07|
|137|[xxjwxc/caoguo](https://github.com/xxjwxc/caoguo)|golang,微信小程序,电商系统|379|Vue|2021/11/09|
|138|[cmdparkour/vue-admin-box](https://github.com/cmdparkour/vue-admin-box)|vue3,vite,element-plus中后台管理系统，集成四套基础模板，大量可利用组件，模板页面|378|Vue|2021/12/01|
|139|[bingo-oss/bui-weex](https://github.com/bingo-oss/bui-weex)|专门为 Weex 前端开发者打造的一套高质量UI框架|371|Vue|2021/10/06|
|140|[Acmenlei/Many-people-blog](https://github.com/Acmenlei/Many-people-blog)|基于Vue2+Nodejs+MySQL的博客，有后台管理系统。支持：登陆/注册，留言，评论/回复，点赞，记录浏览数，相册，发表文章等，内容丰富。|370|Vue|2021/10/06|
|141|[OXOYO/X-WebDesktop-Vue](https://github.com/OXOYO/X-WebDesktop-Vue)|基于 Vue & Koa 的 WebDesktop 视窗系统   The WebDesktop system based on Vue|367|Vue|2021/09/21|
|142|[mizuka-wu/vue2-verify](https://github.com/mizuka-wu/vue2-verify)|vue的验证码插件|360|Vue|2021/10/06|
|143|[huxiaocheng/vue-gn-components](https://github.com/huxiaocheng/vue-gn-components)|这里有一些标准组件库可能没有的功能组件，已有组件：放大镜、签到、图片标签、滑动验证、倒计时、水印、拖拽、大家来找茬。|358|Vue|2021/10/06|
|144|[zhixuanziben/gouyan-movie-vue](https://github.com/zhixuanziben/gouyan-movie-vue)|这是一个基于vue全家桶制作的在线电影影讯网站，利用豆瓣api的接口获取数据，模仿猫眼电影制作的webapp，实现了当前热映电影，即将上映电影，电影详细信息，短评和长评论信息，影星个人信息，以及电影查询的功能|357|Vue|2021/07/10|
|145|[CCZX/wechat](https://github.com/CCZX/wechat)|聊天室、websocket、socket.io、毕业设计。|355|Vue|2021/11/07|
|146|[HuberTRoy/vue-shiyanlou](https://github.com/HuberTRoy/vue-shiyanlou)|:kissing_heart:基于vue2和vuex的复杂单页面应用，20+页面53个API（仿实验楼）:sparkles::sparkles:|354|Vue|2021/10/06|
|147|[GeekQiaQia/vue3.0-template-admin](https://github.com/GeekQiaQia/vue3.0-template-admin)|本项目基于vue3+ElementPlus+Typescript+Vite搭建一套通用的后台管理模板；并基于常见业务场景，抽象出常见功能组件；包括动态菜单，菜单权限、登录、主题切换、国际化、个人中心、表单页、列表页、复制文本、二维码分享等等|354|Vue|2021/11/29|
|148|[lentoo/vue-admin](https://github.com/lentoo/vue-admin)|使用 vue-cli3 搭建的vue-vuex-router-element 开发模版，集成常用组件，功能模块|347|Vue|2021/11/30|
|149|[baimingxuan/vue-admin-design](https://github.com/baimingxuan/vue-admin-design)|基于vue + elementUI的管理系统模板|338|Vue|2021/08/25|
|150|[Peachick/ktv-select_music-system](https://github.com/Peachick/ktv-select_music-system)|KTV点歌系统,含后台管理系统(完整版)|337|Vue|2021/11/04|
|151|[lhz960904/movie-trailer](https://github.com/lhz960904/movie-trailer)|:popcorn:Vue3 + TypeScript开发的电影预告片webAPP，可以查看正在热映与即将上映的电影信息和短片|337|Vue|2021/10/06|
|152|[GeekPark/smeditor](https://github.com/GeekPark/smeditor)|✎ 基于 Vue.js 2.0+ 石墨文档样式的富文本编辑器组件|324|Vue|2021/08/11|
|153|[J1ong/FilmSys](https://github.com/J1ong/FilmSys)|一个使用Vue全家桶和后台Express框架结合Mysql数据库搭建起来的移动端电影售票和管理系统，实现了热映、即将上映、电影和影院全局搜索、评论、选座、购票、点赞、收藏、订单等一系列购票和管理流程功能|318|Vue|2021/10/06|
|154|[hzlshen/vue-project](https://github.com/hzlshen/vue-project)|基于vue-cli构建的财务后台管理系统(vue2+vuex+axios+vue-router+element-ui+echarts+websocket+vue-i18n)|313|Vue|2021/11/06|
|155|[wuyawei/webrtc-stream](https://github.com/wuyawei/webrtc-stream)|🍧🍭😻包括但不局限于 WebRTC 的各种栗子|308|Vue|2021/10/06|
|156|[acccccccb/vue-img-cutter](https://github.com/acccccccb/vue-img-cutter)|简单易用的vue图片裁剪插件，支持移动图像，裁剪图片，放大缩小图片，上下左右移动，固定比例，固定尺寸，远程图片裁剪，只需要很少的代码就可以实现裁剪功能，也可以通过调整参数以适应你自己的业务需求。|307|Vue|2021/11/08|
|157|[zhuyihe/vue-admin-project](https://github.com/zhuyihe/vue-admin-project)|vue-cli3搭建后台管理模板|307|Vue|2021/10/06|
|158|[w1301625107/Vue-Gantt-chart](https://github.com/w1301625107/Vue-Gantt-chart)|使用Vue做数据控制的Gantt图表|306|Vue|2021/10/06|
|159|[linjinze999/vue-llplatform](https://github.com/linjinze999/vue-llplatform)|vue-llplatform，基于vue、element搭建的后台管理平台。|303|Vue|2021/10/06|
|160|[d2-projects/d2-crud](https://github.com/d2-projects/d2-crud)|D2 Crud 是一个基于 Vue.js 和 Element UI 的表格组件，封装了常用的表格操作。|296|Vue|2021/10/06|
|161|[WishMelz/imgurl](https://github.com/WishMelz/imgurl)|基于github的图片管理系统/免费|293|Vue|2021/11/22|
|162|[xusenlin/vue-element-ui-admin](https://github.com/xusenlin/vue-element-ui-admin)|:maple_leaf:  一个基于 Vue Element UI 的后台模板，做了目录结构的整理和常用方法的封装，开箱即用 :)|290|Vue|2021/10/06|
|163|[fuyi501/web-video-live](https://github.com/fuyi501/web-video-live)|网页H5播放视频流/直播系统，使用 flv.js，vue-video-player播放器，测试支持 rtmp，http-flv，hls 视频流格式，可以做视频监控，也可以通过视频截图。|288|Vue|2021/10/06|
|164|[warriorBrian/nuxt-blog](https://github.com/warriorBrian/nuxt-blog)|基于Nuxt.js服务器渲染(SSR)搭建的个人博客系统|288|Vue|2021/07/29|
|165|[MPComponent/mpvue-weui](https://github.com/MPComponent/mpvue-weui)|基于 mpvue 的 weui 框架|282|Vue|2021/10/06|
|166|[easy-wheel/ts-vue](https://github.com/easy-wheel/ts-vue)|基于typescript+vue-cli3+elementui搭建的壳子|280|Vue|2021/08/22|
|167|[phynos/WebTopo](https://github.com/phynos/WebTopo)|基于VUE的web组态（组态，拓扑图，拓扑编辑器）|279|Vue|2021/10/13|
|168|[zhangyuang/vite-design](https://github.com/zhangyuang/vite-design)|下一代构建工具 vite 文档翻译 源码解析|277|Vue|2021/10/17|
|169|[kouchao/vue-layui](https://github.com/kouchao/vue-layui)|基于vue的layui|277|Vue|2021/10/06|
|170|[febsteam/FEBS-Cloud-Web](https://github.com/febsteam/FEBS-Cloud-Web)|FEBS Cloud 微服务权限系统前端，使用 vue-element-admin 构建|275|Vue|2021/10/11|
|171|[pengxiaotian/datav-vue](https://github.com/pengxiaotian/datav-vue)|A Powerful Data Visualization Tool. Uses TypeScript And Vue3. Scenario-specific templates. User-friendly interfaces. 一款数据可视化应用搭建工具|273|Vue|2021/10/17|
|172|[daoket/vue.news](https://github.com/daoket/vue.news)|项目地址|273|Vue|2021/10/06|
|173|[fujiazhang/Music-For-The-Poor](https://github.com/fujiazhang/Music-For-The-Poor)|Vue技术栈 打造精美音乐WebAppp，且能听付费歌曲（比如周杰伦等），提供优雅的用户体验。仿网易云音乐、仿QQ音乐、vue音乐播放器、music player。|273|Vue|2021/09/24|
|174|[Tencent/WeComponents](https://github.com/Tencent/WeComponents)|基于通用组件语言规范 (CLS) 实现的 Vue.js 声明式组件库|267|Vue|2021/06/15|
|175|[hsiangleev/element-plus-admin](https://github.com/hsiangleev/element-plus-admin)|基于vite+ts+elementPlus|267|Vue|2021/11/13|
|176|[zhengqingya/xiao-xiao-su](https://github.com/zhengqingya/xiao-xiao-su)|基于Spring Boot+Spring Security+JWT+Vue前后端分离的旺旺小小酥  ( 一口一口又香又脆❤~ )|260|Vue|2021/10/06|
|177|[komomoo/vuepress-theme-resume](https://github.com/komomoo/vuepress-theme-resume)|🐈 书写简洁优雅的前端程序员 markdown 简历，由 vuepress 驱动|260|Vue|2021/11/19|
|178|[ssshooter/img-vuer](https://github.com/ssshooter/img-vuer)|An Mobile-First image viewer for Vue3  / 一个移动端优先的 Vue3 图片预览插件|259|Vue|2021/11/08|
|179|[FXLP/MarkTool](https://github.com/FXLP/MarkTool)|DoTAT 是一款基于web、面向领域的通用文本标注工具，支持大规模实体标注、关系标注、事件标注、文本分类、基于字典匹配和正则匹配的自动标注以及用于实现归一化的标准名标注，同时也支持迭代标注、嵌套实体标注和嵌套事件标注。标注规范可自定义且同类型任务中可“一次创建多次复用”。通过分级实体集合扩大了实体类型的规模，并设计了全新高效的标注方式，提升了用户体验和标注效率。此外，本工具增加了审核环节，可对多人的标注结果进行一致性检验、自动合并和手动调整，提高了标注结果的准确率。|254|Vue|2021/11/27|
|180|[sirfuao/vue_shop](https://github.com/sirfuao/vue_shop)|这是一个vue商城项目|251|Vue|2021/10/06|
|181|[chenquincy/vue-dynamic-form-component](https://github.com/chenquincy/vue-dynamic-form-component)|Vue dynamic nested form component, support nested Object/Hashmap/Array. Vue动态多级表单组件，支持嵌套对象/Hashmap/数组。|248|Vue|2021/11/02|
|182|[langyuxiansheng/vue-sign-canvas](https://github.com/langyuxiansheng/vue-sign-canvas)|一个基于canvas开发,封装于Vue组件的通用手写签名板(电子签名板),支持pc端和移动端,属性支持自定义配置|245|Vue|2021/05/17|
|183|[roncoo/roncoo-education-web](https://github.com/roncoo/roncoo-education-web)|《领课教育》的前端门户系统。领课教育系统（roncoo-education）是基于领课网络多年的在线教育平台开发和运营经验打造出来的产品，致力于打造一个全行业都适用的分布式在线教育系统。|244|Vue|2021/08/12|
|184|[tangjinzhou/geektime-vue-1](https://github.com/tangjinzhou/geektime-vue-1)|极客时间基础篇&生态篇代码|241|Vue|2021/10/07|
|185|[amazingTest/Taisite-Platform](https://github.com/amazingTest/Taisite-Platform)|最强接口测试平台|237|Vue|2021/05/07|
|186|[dnyz520/careyshop-admin](https://github.com/dnyz520/careyshop-admin)|基于ThinkPHP5和Vue的高性能前后端分离商城后台管理框架系统|236|Vue|2021/08/31|
|187|[Deja-vuuu/vue-ele](https://github.com/Deja-vuuu/vue-ele)|🥗🥗     vue教程 --- 从0-1高仿饿了么App |235|Vue|2021/09/25|
|188|[gzydong/LumenIM](https://github.com/gzydong/LumenIM)|Lumen IM 是一个网页版在线聊天项目，前端使用 Element-ui + Vue，后端采用了基于 Swoole 开发的 Hyperf 协程框架进行接口开发，并使用 WebSocket 服务进行消息实时推送。|234|Vue|2021/11/07|
|189|[fyl080801/vjdesign](https://github.com/fyl080801/vjdesign)|Vue 界面可视化设计器，支持任何 html 标签以及项目中引用的组件，可实现仅通过配置文件就能增加支持的组件和组件属性|233|Vue|2021/10/07|
|190|[Zhuyi731/echarts-for-wx-uniapp](https://github.com/Zhuyi731/echarts-for-wx-uniapp)|小程序echarts，兼容uni-app|231|Vue|2021/11/28|
|191|[shuax/MouseInc.Settings](https://github.com/shuax/MouseInc.Settings)|MouseInc设置界面|228|Vue|2021/08/28|
|192|[xlogiccc/vue-picture-preview](https://github.com/xlogiccc/vue-picture-preview)|移动端、PC 端 Vue.js 图片预览插件   Friendly picture file preview Vue.js plugin based on PhotoSwipe.|227|Vue|2021/08/11|
|193|[QinZhen001/didi](https://github.com/QinZhen001/didi)|:car: mpvue框架仿滴滴出行微信小程序|227|Vue|2021/06/05|
|194|[caiya/vue-neditor-wrap](https://github.com/caiya/vue-neditor-wrap)|基于vue和neditor的双向绑定组件，使用vue-cli3的项目可以直接使用|225|Vue|2021/08/11|
|195|[maclxf/supermall](https://github.com/maclxf/supermall)|练习coderwhy的项目|225|Vue|2021/10/06|
|196|[shiguanghuxian/etcd-manage](https://github.com/shiguanghuxian/etcd-manage)|一个现代的etcd v3管理ui|224|Vue|2021/11/09|
|197|[pwx123/vue-vant-store](https://github.com/pwx123/vue-vant-store)|基于vue，vantUI的商城demo，包含前端和后端|223|Vue|2021/10/06|
|198|[CyberFei/puzzle](https://github.com/CyberFei/puzzle)|A pluggable micro-frontend structure based on Vue and Webpack4. 基于 Vue 和 Webpack4 的可热插拔式微前端架构|223|Vue|2021/10/06|
|199|[Liugq5713/vue-element-nocode-admin](https://github.com/Liugq5713/vue-element-nocode-admin)|element-ui 的代码可视化编辑|222|Vue|2021/10/06|
|200|[lyt-Top/vue-next-admin](https://github.com/lyt-Top/vue-next-admin)|🎉🎉🔥基于vue3.x 、Typescript、vite、Element plus等，适配手机、平板、pc 的后台开源免费模板库（vue2.x请切换vue-prev-admin分支）|221|Vue|2021/09/25|

⬆ [回到目录](#内容目录)

<br/>

## Kotlin

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[Tamsiree/RxTool](https://github.com/Tamsiree/RxTool)|Android开发人员不得不收集的工具类集合   支付宝支付   微信支付（统一下单）   微信分享   Zip4j压缩（支持分卷压缩与加密）   一键集成UCrop选择圆形头像   一键集成二维码和条形码的扫描与生成   常用Dialog   WebView的封装可播放视频   仿斗鱼滑动验证码   Toast封装   震动   GPS   Location定位   图片缩放   Exif 图片添加地理位置信息（经纬度）   蛛网等级   颜色选择器   ArcGis   VTPK   编译运行一下说不定会找到惊喜|11.6k|Kotlin|2021/06/05|
|2|[mamoe/mirai](https://github.com/mamoe/mirai)|高效率 QQ 机器人支持库|7.9k|Kotlin|2021/11/30|
|3|[gedoor/legado](https://github.com/gedoor/legado)|阅读3.0, 阅读是一款可以自定义来源阅读网络内容的工具，为广大网络文学爱好者提供一种方便、快捷舒适的试读体验。|5.5k|Kotlin|2021/11/30|
|4|[bennyhuo/Kotlin-Tutorials](https://github.com/bennyhuo/Kotlin-Tutorials)|【持续更新中】本仓库持续记录以 Kotlin 为基础的视频内容的制作过程|4.0k|Kotlin|2021/11/27|
|5|[svga/SVGAPlayer-Android](https://github.com/svga/SVGAPlayer-Android)|Similar to Lottie. Render After Effects / Animate CC (Flash) animations natively on Android and iOS, Web.  使用 SVGAPlayer 在 Android、iOS、Web中播放 After Effects / Animate CC (Flash) 动画。|2.7k|Kotlin|2021/08/30|
|6|[yujincheng08/BiliRoaming](https://github.com/yujincheng08/BiliRoaming)|哔哩漫游，解除B站客户端番剧区域限制的Xposed模块，并且提供其他小功能。An Xposed module that unblocks bangumi area limit of BILIBILI with miscellaneous features.|2.5k|Kotlin|2021/11/30|
|7|[caiyonglong/MusicLake](https://github.com/caiyonglong/MusicLake)|MediaPlayer、Exoplayer音乐播放器，可播在线音乐，qq音乐，百度音乐，虾米音乐，网易云音乐，YouTuBe|2.3k|Kotlin|2021/07/27|
|8|[princekin-f/EasyFloat](https://github.com/princekin-f/EasyFloat)|🔥 EasyFloat：浮窗从未如此简单（Android可拖拽悬浮窗口，支持页面过滤、自定义动画，可设置单页面浮窗、前台浮窗、全局浮窗，浮窗权限按需自动申请...）|2.2k|Kotlin|2021/12/01|
|9|[Ccixyj/JBusDriver](https://github.com/Ccixyj/JBusDriver)|这是去幼儿园的班车(滑稽|2.0k|Kotlin|2021/07/03|
|10|[iielse/imageviewer](https://github.com/iielse/imageviewer)|A simple and customizable Android full-screen image viewer 一个简单且可自定义的Android全屏图像浏览器|1.9k|Kotlin|2021/11/27|
|11|[hegaojian/JetpackMvvm](https://github.com/hegaojian/JetpackMvvm)|:chicken::basketball:一个Jetpack结合MVVM的快速开发框架，基于MVVM模式集成谷歌官方推荐的JetPack组件库：LiveData、ViewModel、Lifecycle、Navigation组件 使用Kotlin语言，添加大量拓展函数，简化代码 加入Retrofit网络请求,协程，帮你简化各种操作，让你快速开发项目|1.8k|Kotlin|2021/10/21|
|12|[Tencent/bk-ci](https://github.com/Tencent/bk-ci)|蓝鲸CI平台(BlueKing CI) |1.7k|Kotlin|2021/12/01|
|13|[running-libo/Tiktok](https://github.com/running-libo/Tiktok)|高仿抖音APP|1.7k|Kotlin|2021/11/07|
|14|[iceCola7/WanAndroid](https://github.com/iceCola7/WanAndroid)|🔥项目采用 Kotlin 语言，基于 MVP + RxJava + Retrofit + Glide + EventBus 等架构设计，努力打造一款优秀的  [玩Android] 客户端|1.4k|Kotlin|2021/06/28|
|15|[VIPyinzhiwei/Eyepetizer](https://github.com/VIPyinzhiwei/Eyepetizer)|🔥基于 Kotlin 语言仿写「开眼 Eyepetizer」的一个短视频 Android 客户端项目，采用 Jetpack + 协程实现的 MVVM 架构。|1.3k|Kotlin|2021/11/19|
|16|[HMBSbige/ShadowsocksR-Android](https://github.com/HMBSbige/ShadowsocksR-Android)|【自用】咕咕咕|1.3k|Kotlin|2021/09/13|
|17|[xdtianyu/CallerInfo](https://github.com/xdtianyu/CallerInfo)|来电信息 - 一个获取号码归属地和其他信息（诈骗、骚扰等）的开源 Android 应用|1.3k|Kotlin|2021/08/16|
|18|[CarGuo/GSYGithubAppKotlin](https://github.com/CarGuo/GSYGithubAppKotlin)|超完整的Android Kotlin 项目，功能丰富，适合学习和日常使用。GSYGithubApp系列的优势：目前已经拥有Flutter、Weex、ReactNative、Kotlin四个版本。 功能齐全，项目框架内技术涉及面广，完成度高。开源Github客户端App，更好的体验，更丰富的功能，旨在更好的日常管理和维护个人Github，提供更好更方便的驾车体验Σ(￣。￣ﾉ)ﾉ。同款Weex版本： https://github.com/CarGuo/GSYGithubAppWeex  、同款React Native版本 ： https://github.com/CarGuo/GSYGithubA ...|1.1k|Kotlin|2021/09/04|
|19|[EspoirX/StarrySky](https://github.com/EspoirX/StarrySky)|🔥A Powerful and Streamline MusicLibrary(一个丰富的音乐播放封装库,支持多种音频格式,完美解决你的问题。)|1.1k|Kotlin|2021/11/22|
|20|[qingmei2/RxImagePicker](https://github.com/qingmei2/RxImagePicker)|:rocket:RxJava2 and RxJava3 external support. Android flexible picture selector, provides the support for theme of Zhihu and WeChat (灵活的Android图片选择器，提供了知乎和微信主题的支持）.|1.1k|Kotlin|2021/10/13|
|21|[lulululbj/wanandroid](https://github.com/lulululbj/wanandroid)|Jetpack MVVM For Wanandroid 最佳实践 ！|1.1k|Kotlin|2021/08/27|
|22|[hi-dhl/AndroidX-Jetpack-Practice](https://github.com/hi-dhl/AndroidX-Jetpack-Practice)|本仓库致力于建立最全、最新的的 AndroidX Jetpack 相关组件的实践项目 以及组件对应的分析文章（持续更新中）如果对你有帮助，请在右上角 star 一下，感谢|1.0k|Kotlin|2021/08/16|
|23|[mCyp/Hoo](https://github.com/mCyp/Hoo)|🚀 Android Jetpack系列组件实战案例，配上专栏学习更轻松~|988|Kotlin|2021/08/16|
|24|[mamoe/mirai-console](https://github.com/mamoe/mirai-console)|mirai 的高效率 QQ 机器人控制台|983|Kotlin|2021/12/01|
|25|[zskingking/Jetpack-Mvvm](https://github.com/zskingking/Jetpack-Mvvm)|使用Jetpack全家桶+Kotlin实现的Android社区App加音乐播放器。不写晦涩难懂的代码，尽量标清每一行注释，严格遵守六大基本原则，大量运用设计模式，此项目可快速帮你入手Kotlin、Jetpack。如果觉得对你有帮助，右上角点个star，事先谢过🍉🍉🍉|961|Kotlin|2021/08/11|
|26|[teprinciple/UpdateAppUtils](https://github.com/teprinciple/UpdateAppUtils)|一行代码快速实现app版本更新|955|Kotlin|2021/07/07|
|27|[adisonhuang/awesome-kotlin-android](https://github.com/adisonhuang/awesome-kotlin-android)|🔥📱收集利用 Kotlin 进行 Android 开发的开源库，扩展，工具，开源项目，资料等高质量资源|827|Kotlin|2021/10/19|
|28|[Rabtman/AcgClub](https://github.com/Rabtman/AcgClub)|一款纯粹的ACG聚合类App|826|Kotlin|2021/07/21|
|29|[zhujiang521/PlayAndroid](https://github.com/zhujiang521/PlayAndroid)|🔥🔥🔥 Kotlin + MVVM + LCE版玩安卓，暗黑模式、横竖屏、无网、弱网、无数据、加载失败等等各种情况，协程、Room、Hilt、DataStore、LiveData、Retrofit、屏幕适配、本地缓存、多语言切换、多 lib，你想要的我都有！！！|774|Kotlin|2021/11/04|
|30|[rosuH/EasyWatermark](https://github.com/rosuH/EasyWatermark)|🔒 🖼 Securely, easily add a watermark to your sensitive photos. 安全、简单地为你的敏感照片添加水印，防止被小人泄露、利用|773|Kotlin|2021/12/01|
|31|[angcyo/DslTabLayout](https://github.com/angcyo/DslTabLayout)|:hearts: Android界最万能的TabLayout(不仅仅是TabLayout), 支持任意类型的item, 支持Drawable类型的指示器,智能开启滚动,支持横竖向布局等|742|Kotlin|2021/11/26|
|32|[liangjingkanji/Net](https://github.com/liangjingkanji/Net)|🍉 Android 最强大的创新式协程并发网络请求, 开发效率提升603% |678|Kotlin|2021/11/24|
|33|[liangjingkanji/BRV](https://github.com/liangjingkanji/BRV)|🌽 Android 最强大的RecyclerView库, 比BRVAH更强大, 开发效率提升601.458%|631|Kotlin|2021/11/29|
|34|[tuchg/ChinesePinyin-CodeCompletionHelper](https://github.com/tuchg/ChinesePinyin-CodeCompletionHelper)|让你的 JetBrains 系 IDE ( IDEA ,PyCharm,PhpStorm,WebStorm,AndroidStudio,DevEco等 )支持中文标识符以拼音输入方式完成代码补全，享受和英文环境一致的中文智能编码体验，为代码表达提供更多选择，一种值得考虑的折中解决方案|624|Kotlin|2021/10/10|
|35|[Leifzhang/AndroidAutoTrack](https://github.com/Leifzhang/AndroidAutoTrack)|Android Asm 插桩 教学|610|Kotlin|2021/10/06|
|36|[loperSeven/DateTimePicker](https://github.com/loperSeven/DateTimePicker)|:star::tada:一个高颜值日期时间选择器；极简API，内置弹窗，可动态配置样式及主题，选择器支持完全自定义UI。|565|Kotlin|2021/11/26|
|37|[edxposedd/wework](https://github.com/edxposedd/wework)|【Xposed Hook 企业微信 微信】企业微信机器人 微信机器人 自动抢回复 会话 自动通过 好友列表 群管理 SDK|522|Kotlin|2021/07/01|
|38|[javakam/FileOperator](https://github.com/javakam/FileOperator)|🔥 涵盖了Android系统文件的创建/删除/复制/打开文件(目录)、获取文件(目录)大小、获取常用目录、获取文件名称及后缀、获取MimeType以及MediaStore和SAF的相关操作等常用功能，并且也处理了获取文件Uri/Path的兼容问题、图片压缩和文件选择等功能。|521|Kotlin|2021/11/29|
|39|[AAChartModel/AAChartCore-Kotlin](https://github.com/AAChartModel/AAChartCore-Kotlin)|📈📊⛰⛰⛰An elegant modern declarative data visualization chart framework for Android . Extremely powerful, supports line, spline, area, areaspline, column, bar, pie, scatter, angular gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar chart types.极 ...|513|Kotlin|2021/11/17|
|40|[yueeng/hacg](https://github.com/yueeng/hacg)|琉璃神社 hacg android app by scala|505|Kotlin|2021/11/22|
|41|[ifmvo/TogetherAd](https://github.com/ifmvo/TogetherAd)|🔥持续更新。Android广告聚合：帮助 Android 开发者快速、便捷、灵活的接入国内多家主流安卓广告 SDK。广点通（优量汇）、穿山甲、快手联盟、芒果互动、百青藤（百度Mob）。开屏广告、Banner横幅广告、插屏广告、激励广告、原生信息流、全屏广告。|502|Kotlin|2021/11/27|
|42|[Zhao-Yan-Yan/MultiStatePage](https://github.com/Zhao-Yan-Yan/MultiStatePage)|Android APP缺省页的正确打开方式 高度解耦、低侵入、易拓展 多状态视图状态切换器|498|Kotlin|2021/07/22|
|43|[AnJoiner/FFmpegCommand](https://github.com/AnJoiner/FFmpegCommand)|FFmpegCommand适用于Android的FFmpeg命令库，实现了对音视频相关的处理，能够快速的处理音视频，大概功能包括：音视频剪切，音视频转码，音视频解码原始数据，音视频编码，视频转图片或gif，视频添加水印，多画面拼接，音频混音，视频亮度和对比度，音频淡入和淡出效果等|496|Kotlin|2021/06/09|
|44|[eprendre/tingshu](https://github.com/eprendre/tingshu)|一款可在线播放多个免费听书站点的安卓app|485|Kotlin|2021/09/20|
|45|[yyuueexxiinngg/onebot-kotlin](https://github.com/yyuueexxiinngg/onebot-kotlin)|OneBot标准的Kotlin实现及mirai插件 - 原cqhttp-mirai|480|Kotlin|2021/07/07|
|46|[fabricezhang/lcg](https://github.com/fabricezhang/lcg)|吾爱破解第三方安卓应用|479|Kotlin|2021/05/31|
|47|[wuyr/intellij-media-player](https://github.com/wuyr/intellij-media-player)|【🐟摸鱼专用】上班偷偷看视频📺而不会被老板打🔨的IDE插件，适配JetBrains全家桶|475|Kotlin|2021/09/02|
|48|[Moriafly/DsoMusic](https://github.com/Moriafly/DsoMusic)|Kotlin 开发的美观安卓音乐软件，音源：网易云音乐、QQ 音乐|473|Kotlin|2021/11/18|
|49|[huannan/XArch](https://github.com/huannan/XArch)|🔥🔥🔥Android架构最佳实践 - 手把手带你搭建一个优秀的Android项目架构|458|Kotlin|2021/11/30|
|50|[leavesC/ReactiveHttp](https://github.com/leavesC/ReactiveHttp)|一个基于 Kotlin + Jetpack + Coroutines + Retrofit 封装的网络请求框架|458|Kotlin|2021/11/29|
|51|[compose-museum/hello-compose](https://github.com/compose-museum/hello-compose)|Jetpack Compose 基础教程，持续更新|452|Kotlin|2021/11/27|
|52|[SkyD666/Imomoe](https://github.com/SkyD666/Imomoe)|樱花动漫第三方安卓Android客户端，不含广告，免费开源，目的是学习Android开发。|451|Kotlin|2021/11/21|
|53|[izhangzhihao/intellij-rainbow-fart](https://github.com/izhangzhihao/intellij-rainbow-fart)|🌈一个在你编程时持续夸你写的牛逼的扩展，可以根据代码关键字播放贴近代码意义的真人语音。Inspired by vscode-rainbow-fart|447|Kotlin|2021/09/16|
|54|[Quyunshuo/AndroidBaseFrameMVVM](https://github.com/Quyunshuo/AndroidBaseFrameMVVM)|Android 组件化 MVVM 框架 基于 Jetpack + Kotlin|446|Kotlin|2021/09/26|
|55|[crazyandcoder/blog_backups](https://github.com/crazyandcoder/blog_backups)|Android 项目优化、面试题集，包含Android、Java、数据结构、算法、个人blog备份等。|428|Kotlin|2021/07/22|
|56|[zhiwei1990/android-jetpack-demo](https://github.com/zhiwei1990/android-jetpack-demo)|🔥  快速入门Android Jetpack以及相关Kotlin、RxJava、MVVM等主流技术，独立构架App的基础技能|409|Kotlin|2021/05/30|
|57|[xuehuayous/DelegationAdapter](https://github.com/xuehuayous/DelegationAdapter)|一种优雅的方式来使用RecyclerView|395|Kotlin|2021/10/24|
|58|[DylanCaiCoding/ViewBindingKTX](https://github.com/DylanCaiCoding/ViewBindingKTX)|The most comprehensive utils of ViewBinding. (最全面的 ViewBinding 工具，支持 Kotlin 和 Java 用法，支持拓展函数和基类改造，支持 BRVAH)|373|Kotlin|2021/11/29|
|59|[fuusy/component-jetpack-mvvm](https://github.com/fuusy/component-jetpack-mvvm)|💖组件化+Jetpack+Kotlin+MVVM项目实战，涉及Jetpack相关组件，Kotlin相关技术，协程+Retrofit，Paging3+Room等。|352|Kotlin|2021/07/24|
|60|[KyuubiRan/QQCleaner](https://github.com/KyuubiRan/QQCleaner)|瘦身模块|340|Kotlin|2021/11/30|
|61|[smashinggit/Study](https://github.com/smashinggit/Study)|记录学习过程中的demo及博客|334|Kotlin|2021/08/16|
|62|[hi-dhl/Leetcode-Solutions-with-Java-And-Kotlin](https://github.com/hi-dhl/Leetcode-Solutions-with-Java-And-Kotlin)|LeetCode 系列题解, 在线阅读 https://offer.hi-dhl.com|333|Kotlin|2021/09/14|
|63|[iTXTech/mirai-native](https://github.com/iTXTech/mirai-native)|强大的 mirai 原生插件加载器|326|Kotlin|2021/09/10|
|64|[helloklf/vtools](https://github.com/helloklf/vtools)|一个集高级重启、应用安装自动点击、CPU调频等多项功能于一体的工具箱。|310|Kotlin|2021/11/30|
|65|[hi-dhl/Binding](https://github.com/hi-dhl/Binding)|Simple API implement DataBinding and ViewBinding.  简单的 API 实现 DataBinding 和 ViewBinding，欢迎 star|307|Kotlin|2021/06/22|
|66|[ldlywt/FastJetpack](https://github.com/ldlywt/FastJetpack)|基于Kotlin、协程、Retrofit的网络请求封装，快速简单轻便。|302|Kotlin|2021/11/24|
|67|[WGwangguan/SeparatedEditText](https://github.com/WGwangguan/SeparatedEditText)|仿支付宝密码输入框、微信密码输入框，美团外卖验证码输入框等。有实心，空心以及下划线形式。可控制文本是否显示。|299|Kotlin|2021/05/20|
|68|[xyoye/DanDanPlayForAndroid](https://github.com/xyoye/DanDanPlayForAndroid)|弹弹play 概念版，弹弹play系列应用安卓平台上的实现，是一个提供了视频播放（本地+局域网）和弹幕加载（在线+本地）功能的本地播放器|294|Kotlin|2021/11/29|
|69|[kukyxs/CoroutinesWanAndroid](https://github.com/kukyxs/CoroutinesWanAndroid)|超完整的「玩 Android」客户端，项目采用 MVVM, Android Jetpack, Retrofit, Kotlin 协程, Koin 编写。如果该项目对你学习过程有用，请给个 star，感谢|294|Kotlin|2021/09/29|
|70|[mzdluo123/MiraiAndroid](https://github.com/mzdluo123/MiraiAndroid)|QQ机器人 /（实验性）在Android上运行Mirai-console，支持插件|283|Kotlin|2021/11/10|
|71|[OCNYang/RecyclerViewEvent](https://github.com/OCNYang/RecyclerViewEvent)|RecyclerView onItemClick、onItemLongClick、drag、swipe、divider、reuse disorder  RecyclerView 梳理：点击&长按事件、分割线、拖曳排序、滑动删除、优雅解决 EditText 和 CheckBox 复用错乱问题、定向刷新、DiffUtil、局部刷新|274|Kotlin|2021/07/03|
|72|[angcyo/DslAdapter](https://github.com/angcyo/DslAdapter)|:fire: Android Kotlin时代的Adapter, Dsl 的形式使用 RecyclerView.Adapter, 支持折叠展开, 树结构,悬停,情感图状态切换, 加载更多, 多类型Item,侧滑菜单等|274|Kotlin|2021/11/27|
|73|[KwaiAppTeam/AkDanmaku](https://github.com/KwaiAppTeam/AkDanmaku)|一款利用游戏引擎理念打造的原生弹幕库|261|Kotlin|2021/11/17|
|74|[gdutxiaoxu/AnchorTask](https://github.com/gdutxiaoxu/AnchorTask)|锚点任务，可以用来解决多线程加载任务依赖的问题。常见的，比如 Android 启动优化，通常会进行多线程异步加载|259|Kotlin|2021/09/15|
|75|[benhero/GLStudio](https://github.com/benhero/GLStudio)|OpenGL基础入门|250|Kotlin|2021/07/26|
|76|[ibaozi-cn/RecyclerViewAdapter](https://github.com/ibaozi-cn/RecyclerViewAdapter)|科学分包，动态扩展，功能完善，书写简洁，按需依赖，合理抽象，超高内聚，超低耦合，没有最好，只有更好，希望能帮助到您，🙏❤️感谢您的光临🙏❤️|249|Kotlin|2021/09/01|
|77|[forJrking/KLuban](https://github.com/forJrking/KLuban)|Lifecycle + Kotlin 协程 + flow + LiveData + Glide 识别和内存优化 + Luban采样算法 = KLuban图片压缩|247|Kotlin|2021/11/04|
|78|[DUpdateSystem/UpgradeAll](https://github.com/DUpdateSystem/UpgradeAll)|Android 版本自定义更新检查器|244|Kotlin|2021/11/26|
|79|[YvesCheung/TouchEventBus](https://github.com/YvesCheung/TouchEventBus)|一种处理嵌套和非嵌套滑动冲突的解决方案|239|Kotlin|2021/08/18|
|80|[Flywith24/Flywith24-Jetpack-Demo](https://github.com/Flywith24/Flywith24-Jetpack-Demo)|【背上Jetpack】demo，本仓库提供 Jetpack 一个个可以独立运行的小示例，帮助您更好的上手 Jetpack|229|Kotlin|2021/09/14|
|81|[Leon406/ToolsFx](https://github.com/Leon406/ToolsFx)|基于kotlin+tornadoFx开发的跨平台密码学工具箱.包含编解码,编码转换,加解密, 哈希,MAC,签名,二维码功能,ctf等实用功能,支持插件|224|Kotlin|2021/11/25|
|82|[miaowmiaow/fragmject](https://github.com/miaowmiaow/fragmject)|适合初学者入门的项目，通过对Kotlin的系统运用，实现的一个功能完备符合主流市场标准App。包含知识点（MVVM开发架构、单Activity多Fragment项目设计、暗夜模式、屏幕录制、图片编辑、字节码插桩）。项目结构清晰，代码简洁优雅。|216|Kotlin|2021/12/01|
|83|[imyyq-star/MVVMArchitecture](https://github.com/imyyq-star/MVVMArchitecture)|MVVM 框架，采用 Kotlin+Jetpack，可自由配置功能，欢迎 star，fork，issue|211|Kotlin|2021/11/28|
|84|[hegaojian/MvvmHelper](https://github.com/hegaojian/MvvmHelper)|:chicken::basketball:这是一个快速开发的项目壳，Kotlin语言开发，MVVM+Jetpack架构，封装了公共头部、界面状态管理、ViewModel、LiveData、DataBinding、头部刷新、加载更多、沉浸式、全局通知、丰富好用的拓展函数、RxHttp网络请求等等一系列工具|209|Kotlin|2021/11/02|
|85|[fengzhizi715/SAF-Kotlin-log](https://github.com/fengzhizi715/SAF-Kotlin-log)|完全基于 Kotlin 开发的 Android 日志框架，提供极简的 API|209|Kotlin|2021/10/24|
|86|[aitsuki/SwipeMenuRecyclerView](https://github.com/aitsuki/SwipeMenuRecyclerView)|Swipe menu for recyclerView. Android 侧滑菜单，可轻松定制各种样式的菜单。|202|Kotlin|2021/11/28|
|87|[idisfkj/android-api-analysis](https://github.com/idisfkj/android-api-analysis)|Android精华录: 该库的目的是结合详细的Demo来全面解析Android相关的知识点, 帮助读者能够更快的掌握与理解所阐述的要点。  不定时更新，与预期接下的要做的事，希望点进来的你能够喜欢😍😍|202|Kotlin|2021/05/17|
|88|[funnywolfdadada/HollowKit](https://github.com/funnywolfdadada/HollowKit)|自己常用的一些工具的合集|200|Kotlin|2021/11/25|
|89|[ekibun/Bangumi](https://github.com/ekibun/Bangumi)|Bangumi番组计划Android客户端|200|Kotlin|2021/06/23|
|90|[hadix-lin/ideavim_extension](https://github.com/hadix-lin/ideavim_extension)|IdeaVIM插件的扩展插件|195|Kotlin|2021/09/25|
|91|[re-ovo/iwara4a](https://github.com/re-ovo/iwara4a)|基于Jetpack Compose开发的iwara安卓app (Unofficial Iwara Android Application)|192|Kotlin|2021/11/10|
|92|[BillyWei01/Udid](https://github.com/BillyWei01/Udid)|一种Android客户端获取唯一设备ID的方案|190|Kotlin|2021/11/26|
|93|[ForteScarlet/simpler-robot](https://github.com/ForteScarlet/simpler-robot)|simple-robot是一个通用bot开发框架，以同一种灵活的标准来编写不同平台的bot应用。而simpler-robot便是simple-robot 2.x版本命名。|188|Kotlin|2021/11/30|
|94|[blindmonk/WanArchitecture](https://github.com/blindmonk/WanArchitecture)|汇聚了业界知名架构文章。从建筑学的知识中得到一些对架构的思考，并以架构设计原则和目的对Jetpack MVVM 重新构造！|188|Kotlin|2021/06/17|
|95|[liangjingkanji/StateLayout](https://github.com/liangjingkanji/StateLayout)|🍘 Android 一行代码构建整个应用的缺省页|183|Kotlin|2021/09/14|
|96|[390057892/reader](https://github.com/390057892/reader)|小说阅读软件📕，采用 Jetpack + 协程实现的 MVVM 架构。Kotlin+AndroidX编写，支持TTS听书，字体切换，繁简转换，黑夜模式，各种翻页(仿真、滚动、滑动、覆盖、无动画)，插页图片加载等。|181|Kotlin|2021/07/02|
|97|[2BAB/Seal](https://github.com/2BAB/Seal)|A Gradle Plugin to resolve AndroidManifest.xml merge conflicts. 处理 AndroidManifest.xml 合并冲突的 Gradle 插件。|181|Kotlin|2021/11/29|
|98|[TanJiaJunBeyond/AndroidGenericFramework](https://github.com/TanJiaJunBeyond/AndroidGenericFramework)|Android通用框架|180|Kotlin|2021/08/03|
|99|[Shouheng88/Android-VMLib](https://github.com/Shouheng88/Android-VMLib)|🔥🔥🔥 Jetpack MVVM 框架封装、最佳实践，简单易用，为快速开发而设计，可以为你节省大量冗余的代码。|178|Kotlin|2021/10/02|
|100|[yechaoa/MaterialDesign](https://github.com/yechaoa/MaterialDesign)|Material Design 控件集合。ConstraintLayout、NestedScrollView、Toolbar、TabLayout、TextInputLayout。。。|177|Kotlin|2021/05/29|
|101|[anyRTC-UseCase/anyHouse](https://github.com/anyRTC-UseCase/anyHouse)|高仿 ClubHouse，语音直播、语聊房、高音质、极速上麦，开源 ClubHouse，实现了Clubhouse的上麦，下麦，邀请，语音音量提示等功能。|171|Kotlin|2021/06/01|
|102|[fengzhizi715/SAF-Kotlin-Utils](https://github.com/fengzhizi715/SAF-Kotlin-Utils)|用 Kolin 做的 Android Utils 库，包括 utils 和 extension|171|Kotlin|2021/10/12|
|103|[bennyhuo/DiveIntoKotlinCoroutines-Sources](https://github.com/bennyhuo/DiveIntoKotlinCoroutines-Sources)|《深入理解 Kotlin 协程》源码|167|Kotlin|2021/05/31|
|104|[liangjingkanji/Channel](https://github.com/liangjingkanji/Channel)|🍯  LiveData / Coroutine / Lifecycle / EventBus, 开发效率提升600%|165|Kotlin|2021/09/12|
|105|[StarWishsama/Comet-Bot](https://github.com/StarWishsama/Comet-Bot)|☄ 多平台动态推送, 抽卡模拟器, 以及更多   Powered by Mirai|165|Kotlin|2021/11/28|
|106|[panpf/assembly-adapter](https://github.com/panpf/assembly-adapter)|AssemblyAdapter 是 Android 上的一个为各种 Adapter 提供开箱即用实现的库。AssemblyAdapter is a library on Android that provides out-of-the-box implementations for various Adapters.|162|Kotlin|2021/10/28|
|107|[xdd666t/getx_template](https://github.com/xdd666t/getx_template)|Used to generate the template code of GetX framework   Flutter GetX模板代码生成（一个有用的IDEA插件）|161|Kotlin|2021/11/30|
|108|[DylanCaiCoding/Longan](https://github.com/DylanCaiCoding/Longan)|A collection of Kotlin utils which makes Android application development faster and easier. (让 Android 开发更简单的 Kotlin 工具类集合)|154|Kotlin|2021/11/28|
|109|[tencentyun/httpdns-android-sdk](https://github.com/tencentyun/httpdns-android-sdk)|智营防劫持SDK|150|Kotlin|2021/06/29|
|110|[li-xiaojun/StateLayout](https://github.com/li-xiaojun/StateLayout)|一种无侵入，使用简单，无需修改现有布局，动态切换布局状态(Loading/Error/Empty/Content)的解决方案。|147|Kotlin|2021/10/23|
|111|[iostyle/ContinuousTrigger](https://github.com/iostyle/ContinuousTrigger)|多线程连续触发器，用于如按序展示弹窗（App更新、公告、签到之类）。支持多接口返回，等待超时，任意位置注册，任意位置绑定，支持DSL语法。喜欢请star⭐️|145|Kotlin|2021/10/15|
|112|[cdalwyn/mvvmcomponent](https://github.com/cdalwyn/mvvmcomponent)|:v::fist::wave:玩安卓Mvvm组件化客户端，整合Jetpack组件DataBinding、ViewModel以及LiveData；屏幕适配:heavy_check_mark:状态栏沉浸式:heavy_check_mark:黑夜模式:heavy_check_mark:，使用Koin实现的依赖注入等|145|Kotlin|2021/09/07|
|113|[maoqitian/Nice-Knowledge-System](https://github.com/maoqitian/Nice-Knowledge-System)|:books:不积跬步无以至千里，每天进步一点点，Passion，Self-regulation，Love and Share|144|Kotlin|2021/11/11|
|114|[q876625596/GenjiDialogV2](https://github.com/q876625596/GenjiDialogV2)|一个实用的Dialog库|140|Kotlin|2021/08/30|
|115|[Leifzhang/Router-Android](https://github.com/Leifzhang/Router-Android)|通过注解实现路由跳转规则 同时增加ksp支持|132|Kotlin|2021/11/10|
|116|[iceCola7/AndroidModuleSamples](https://github.com/iceCola7/AndroidModuleSamples)|🎨基于 MVP + RxJava + Retrofit + EventBus + Arouter 的 Android 组件化开发框架实践|131|Kotlin|2021/06/05|
|117|[Leon406/SubCrawler](https://github.com/Leon406/SubCrawler)|纯kotlin实现, 节点爬取,筛选, 支持Clash,base64订阅解析,生成可用的ss, ssr, v2ray, trojan节点.|128|Kotlin|2021/11/29|
|118|[itsxtt/pattern-lock](https://github.com/itsxtt/pattern-lock)|Awesome pattern lock view for android written in kotlin. #手势密码|127|Kotlin|2021/10/08|
|119|[iceCola7/KotlinMVPSamples](https://github.com/iceCola7/KotlinMVPSamples)|🚀（Kotlin 版 ）快速搭建 Kotlin + MVP + RxJava + Retrofit + EventBus 的框架，方便快速开发新项目、减少开发成本。|125|Kotlin|2021/06/23|
|120|[xloger/ExLink](https://github.com/xloger/ExLink)|屏蔽国产流氓们内置浏览器的 Xposed 模块|123|Kotlin|2021/10/19|
|121|[lzan13/VMTemplateAndroid](https://github.com/lzan13/VMTemplateAndroid)|一套包含了社区匹配聊天语音以及直播相关的社交系统模板项目|120|Kotlin|2021/08/04|
|122|[ydstar/AdapterKit](https://github.com/ydstar/AdapterKit)|对Recyclerview的adapter的封装,轻松实现各种复杂列表|116|Kotlin|2021/11/11|
|123|[xjunz/AutoSkip](https://github.com/xjunz/AutoSkip)|基于Shizuku授权的安卓"自动跳过"工具|111|Kotlin|2021/10/27|
|124|[Petterpx/FloatingX](https://github.com/Petterpx/FloatingX)|Android免权限悬浮窗，支持全局、局部悬浮，支持边缘吸附、回弹、自定义动画、位置保存、窗口化及分屏后位置修复等。Android without permission suspension window, support global, local suspension, support edge adsorption, rebound, custom animation, position saving, windowing and split-screen position repair.|110|Kotlin|2021/11/20|
|125|[helloklf/kr-scripts](https://github.com/helloklf/kr-scripts)|使用 xml + linux shell 代码，快速创建使用ROOT权限执行的脚本管理器|98|Kotlin|2021/11/23|
|126|[mzdluo123/TxCaptchaHelper](https://github.com/mzdluo123/TxCaptchaHelper)|腾讯滑动验证码助手|98|Kotlin|2021/06/27|
|127|[KitsunePie/QAssistant](https://github.com/KitsunePie/QAssistant)|兼具实用与美观于一身的 QQ 小帮手|97|Kotlin|2021/09/15|
|128|[only52607/lua-mirai](https://github.com/only52607/lua-mirai)|基于lua的bot快速开发框架|95|Kotlin|2021/09/04|
|129|[ysy950803/FiveGSwitcher](https://github.com/ysy950803/FiveGSwitcher)|给MIUI开发一个5G快捷开关。|91|Kotlin|2021/11/13|
|130|[trycatchx/RocketXPlugin](https://github.com/trycatchx/RocketXPlugin)|android 端编译加速插件|91|Kotlin|2021/11/30|
|131|[Samarium150/mirai-console-lolicon](https://github.com/Samarium150/mirai-console-lolicon)|基于mirai-console和LoliconAPI的涩图机器人|89|Kotlin|2021/11/24|
|132|[DylanCaiCoding/ActivityResultLauncher](https://github.com/DylanCaiCoding/ActivityResultLauncher)|Replace startActivityForResult() method gracefully, based on the Activity Result API. (优雅地替代 startActivityForResult()，基于 Activity Result API)|88|Kotlin|2021/10/24|
|133|[hi-dhl/KtKit](https://github.com/hi-dhl/KtKit)|KtKit 小巧而实用，用 Kotlin 语言编写的工具库（长期更新中）|87|Kotlin|2021/09/28|
|134|[scauzhangpeng/NfcSample](https://github.com/scauzhangpeng/NfcSample)|NFC库，兼容4.3之前API以及4.4之后的API，读卡器模式，Sample读取羊城通卡号、余额、交易记录|85|Kotlin|2021/11/25|
|135|[ichenhe/QQ-Notify-Evolution](https://github.com/ichenhe/QQ-Notify-Evolution)|免ROOT优化QQ通知，支持多会话/多消息/多渠道，兼容手环手表。|85|Kotlin|2021/09/23|
|136|[AoEiuV020/PaNovel](https://github.com/AoEiuV020/PaNovel)|我们不生产小说，我们只做网站的搬运工，|84|Kotlin|2021/11/29|
|137|[shenzhen2017/ComposeDouban](https://github.com/shenzhen2017/ComposeDouban)|Compose仿豆瓣榜单页面|83|Kotlin|2021/08/27|
|138|[CysionLiu/KtDevBox](https://github.com/CysionLiu/KtDevBox)|一款基于Kotlin+MVP+组件化的app|83|Kotlin|2021/05/13|
|139|[bloodyrabbit/mirai-setu](https://github.com/bloodyrabbit/mirai-setu)|一个mirai-console的简单的色图插件|83|Kotlin|2021/10/28|
|140|[leavesC/compose_chat](https://github.com/leavesC/compose_chat)|🎁🎁🎁 用 Jetpack Compose 实现一个 IM APP|82|Kotlin|2021/11/22|
|141|[liangjingkanji/StatusBar](https://github.com/liangjingkanji/StatusBar)|🍥 Android 一行代码配置透明状态栏|81|Kotlin|2021/11/28|
|142|[hlgithub369/QuickMvpFrame](https://github.com/hlgithub369/QuickMvpFrame)|这是一款kotlin Mvp的快速开发框架！基于KCommonProject开发，由于原项目很久没更新了基于它上面进行迭代与优化|80|Kotlin|2021/06/28|
|143|[EspoirX/EfficientAdapter](https://github.com/EspoirX/EfficientAdapter)|一个可以提高开发效率的adapter|80|Kotlin|2021/06/17|
|144|[biubiuqiu0/flow-event-bus](https://github.com/biubiuqiu0/flow-event-bus)|EventBus for Android，消息总线，基于SharedFlow，具有生命周期感知能力，支持Sticky，支持线程切换，支持延迟发送。|80|Kotlin|2021/08/18|
|145|[lelelongwang/WanJetpack](https://github.com/lelelongwang/WanJetpack)|💪 持续更新。WanJetpack使用Jetpack MVVM开发架构、单Activity多Fragment设计，项目结构清晰，代码简洁优雅，追求最官方的实现方式。欢迎star，非常感谢。已用到知识点：LiveData、ViewModel、DataBinding、ViewBinding、coroutines、Hilt、Paging3、Room、Navigation、TabLayout、BottomNavigationView、RecycleView、ViewPager2、Banner、Glide、Cookie、Retrofit2、启动页面、深色主题、沉浸式模式、Kotlin高阶函数。|77|Kotlin|2021/08/08|
|146|[Stars-One/M3u8Downloader](https://github.com/Stars-One/M3u8Downloader)|a downloader for m3u8 video 一款m3u8视频下载解密合并工具|76|Kotlin|2021/09/05|
|147|[Tan-yi-xiong/Bangumi_Jetpack](https://github.com/Tan-yi-xiong/Bangumi_Jetpack)|一个看番和追番的Android应用|76|Kotlin|2021/09/19|
|148|[zippo88888888/ZFileManager](https://github.com/zippo88888888/ZFileManager)|Android 文件操作|74|Kotlin|2021/11/05|
|149|[Alonsol/PerfectFloatWindow](https://github.com/Alonsol/PerfectFloatWindow)|android全局悬浮窗，目前已经适配华为，小米，vivo，oppo，一加，三星，魅族，索尼，LG,IQOO,努比亚，中兴，金立，360，锤子等目前是市面上所有机型兼容android4.1至android11版本，支持androidX|74|Kotlin|2021/08/12|
|150|[ziwenL/SrcScrollFrameLayout](https://github.com/ziwenL/SrcScrollFrameLayout)|仿小红书登陆页面背景图无限滚动 FrameLayout|74|Kotlin|2021/05/26|
|151|[fengzhizi715/AndroidServer](https://github.com/fengzhizi715/AndroidServer)|基于 Kotlin + Netty 开发，为 Android App 提供 Server 的功能，包括 Http、TCP、WebSocket 服务|74|Kotlin|2021/10/26|
|152|[aqi00/kotlin](https://github.com/aqi00/kotlin)|《Kotlin从零到精通Android开发》附录源码|73|Kotlin|2021/07/30|
|153|[StevenYan88/MovieApp](https://github.com/StevenYan88/MovieApp)|电影资讯App是一个纯练手项目，使用Kotlin语言开发。|71|Kotlin|2021/11/22|
|154|[xyoye/ComicTools](https://github.com/xyoye/ComicTools)|将BiliBili漫画缓存文件转换成可直接观看的webp文件|71|Kotlin|2021/10/14|
|155|[XuQK/KDTabLayout](https://github.com/XuQK/KDTabLayout)|参考自MagicIndicator撸的自用版本，Kotlin编写，适配ViewPager2。|66|Kotlin|2021/12/01|
|156|[behring/8x-flow-diagram](https://github.com/behring/8x-flow-diagram)|通过Kotlin DSL创建8x Flow建模图|66|Kotlin|2021/11/10|
|157|[sonder-joker/mirai-compose](https://github.com/sonder-joker/mirai-compose)|mirai-console的跨平台桌面端|66|Kotlin|2021/11/30|
|158|[Yinzeyu/YMvRxAndroid](https://github.com/Yinzeyu/YMvRxAndroid)|基于AndroidX的架构，包含 Retrofit2(okhttp3)+Glide+ navigation +fragment+coroutines +LiveData +ViewModel|64|Kotlin|2021/05/02|
|159|[QiYuTechOrg/QiYuTkAndroid](https://github.com/QiYuTechOrg/QiYuTkAndroid)|奇遇淘客 Android 客户端|63|Kotlin|2021/09/13|
|160|[Vove7/BottomDialog](https://github.com/Vove7/BottomDialog)|可高度自定义的底部对话框，使用BottomSheet，支持滚动布局，同时底部布局不会因BottomSheet未显示全部内容而隐藏。|63|Kotlin|2021/09/09|
|161|[fmtjava/Jetpack_Kotlin_Eyepetizer](https://github.com/fmtjava/Jetpack_Kotlin_Eyepetizer)|一款基于Kotlin + Jetpack核心组件 + 协程 + 组件化实现的精美仿开眼视频App(提供Flutter、React Native版本 😁  )|62|Kotlin|2021/10/26|
|162|[iTXTech/mirai-js](https://github.com/iTXTech/mirai-js)|强大的 Mirai JavaScript 插件运行时|62|Kotlin|2021/11/17|
|163|[kongpf8848/ViewWorld](https://github.com/kongpf8848/ViewWorld)|自定义View合集，展示各种自定义View/控件。项目包含了自定义Banner轮播图控件，自定义验证码输入框，自定义TabLayout等控件，持续更新中:wink::wink::wink:|61|Kotlin|2021/08/17|
|164|[iftechio/SquareLayoutManager](https://github.com/iftechio/SquareLayoutManager)|小宇宙 App 广场组件，带惯性运动的二维矩阵卡片列表 UI，自定义 LayoutManager + SnapHelper|61|Kotlin|2021/11/17|
|165|[OpenFlutter/rammus](https://github.com/OpenFlutter/rammus)|Flutter Plugin for AliCloud Push.阿里云推送插件|61|Kotlin|2021/05/08|
|166|[yhsj0919/KMusic](https://github.com/yhsj0919/KMusic)|音乐网站api,支持6大网站音乐搜索,qq,wy,bd,kg,kw,mg|61|Kotlin|2021/06/10|
|167|[FuckAntiAddiction/BiligameAddictionNotLimited](https://github.com/FuckAntiAddiction/BiligameAddictionNotLimited)|B站游戏防沉迷不限制，关键字:B站游戏 BiliBili游戏 实名认证 防沉迷|61|Kotlin|2021/10/30|
|168|[re-ovo/ASoulZhiWang](https://github.com/re-ovo/ASoulZhiWang)|ASOUL评论区小作文助手|60|Kotlin|2021/10/30|
|169|[JanYoStudio/WhatAnime](https://github.com/JanYoStudio/WhatAnime)|如果你在网络上看到一张酷似二次元番剧的插图，并且相当的感兴趣，你可以保存起来，使用WhatAnime找到它的出处。|59|Kotlin|2021/09/13|
|170|[danzekr/Immersive](https://github.com/danzekr/Immersive)|一行代码实现沉浸式，适配android4.4到8.0+，沉浸式前所未有的简单。|59|Kotlin|2021/08/12|
|171|[YggdrasilOfficialProxy/YggdrasilOfficialProxy](https://github.com/YggdrasilOfficialProxy/YggdrasilOfficialProxy)|MojangYggdrasil的更新! 以代理的方式提供伪正版与正版的实现|59|Kotlin|2021/10/09|
|172|[zrq1060/SpanBuilder](https://github.com/zrq1060/SpanBuilder)|一个TextView可以生成的span样式|58|Kotlin|2021/06/30|
|173|[wangyiqian/StockChart](https://github.com/wangyiqian/StockChart)|StockChart是一款适用于Android的高扩展性、高性能股票图开发库，轻松完成各种子图的组合，还能灵活的定制自己的子图满足复杂的业务需求。|58|Kotlin|2021/11/30|
|174|[ouhoukyo/MIUIDock](https://github.com/ouhoukyo/MIUIDock)|一个xposed模块，将MIUI高斯模糊的搜索栏修改成Dock背景|57|Kotlin|2021/06/08|
|175|[wuyr/incremental-compiler](https://github.com/wuyr/incremental-compiler)|适用于Android项目的Gradle插件，用来增量编译class和生成增量dex。跟自带的增量编译Task（assemble）的区别是：这个插件只会编译源文件，不做其他多余的动作|56|Kotlin|2021/07/22|
|176|[StageGuard/SuperCourseTimetableBot](https://github.com/StageGuard/SuperCourseTimetableBot)|基于 mirai-console 的 超级课表上课提醒QQ机器人 插件|55|Kotlin|2021/11/05|
|177|[2BAB/Polyfill](https://github.com/2BAB/Polyfill)|A middleware to assist writing Gradle Plugins for Android build system. 该中间件用于编写 Android 构建环境下的 Gradle 插件。|54|Kotlin|2021/11/28|
|178|[cssxsh/pixiv-helper](https://github.com/cssxsh/pixiv-helper)|mirai-console 插件开发计划|53|Kotlin|2021/11/24|
|179|[TianGuisen/KtRetrofit2](https://github.com/TianGuisen/KtRetrofit2)|Kotlin+Rxjava2+Retrofit2二次封装,使用kotlin语言,有loading,token,防多次重复请求等处理|53|Kotlin|2021/07/28|
|180|[Ifxcyr/ActivityMessenger](https://github.com/Ifxcyr/ActivityMessenger)|ActivityMessenger，借助Kotlin特性，简化Activity之间传参和回调的逻辑代码。|53|Kotlin|2021/05/15|
|181|[yechaoa/YUtils](https://github.com/yechaoa/YUtils)|Android快速开发工具集合——YUtils，同时支持java和kotlin|53|Kotlin|2021/10/16|
|182|[liangjingkanji/Serialize](https://github.com/liangjingkanji/Serialize)|🍒 Android 快速读写本地/Intent数据, 比MMKV/SQLite/SP更快速, 开发效率提升634%|52|Kotlin|2021/11/23|
|183|[yechaoa/wanandroid_jetpack](https://github.com/yechaoa/wanandroid_jetpack)|玩安卓的Jetpack版本|51|Kotlin|2021/10/16|
|184|[7449/Album](https://github.com/7449/Album)|android 图片视频加载库，单选，多选，预览，自定义UI，相机，裁剪...等等 已适配android10，11|51|Kotlin|2021/10/17|
|185|[zhoulinda/eyepetizer_kotlin](https://github.com/zhoulinda/eyepetizer_kotlin)|一款仿开眼短视频App，分别采用MVP、MVVM两种模式实现。一、组件化 + Kotlin + MVP + RxJava + Retrofit + OkHttp 二、组件化 + Kotlin + MVVM + LiveData + DataBinding + Coroutines + RxJava + Retrofit + OkHttp |50|Kotlin|2021/11/06|
|186|[shenzhen2017/compose-refreshlayout](https://github.com/shenzhen2017/compose-refreshlayout)|Compose版SmartRefreshLayout|50|Kotlin|2021/10/05|
|187|[q876625596/YasuoRecyclerViewAdapter](https://github.com/q876625596/YasuoRecyclerViewAdapter)|一个能让你感受到快乐的RecyclerViewAdapter库，A RecyclerViewAdapter library that can make you feel happy|50|Kotlin|2021/09/29|
|188|[linxiangcheer/PlayAndroid](https://github.com/linxiangcheer/PlayAndroid)|Jetpack Compose版本的WanAndroid|49|Kotlin|2021/09/30|
|189|[fmtjava/Jetpack_Compose_News](https://github.com/fmtjava/Jetpack_Compose_News)|基于Jetpack Compose实现的一款集新闻、视频、美图、音乐、天气等功能的资讯App,持续完善中...|49|Kotlin|2021/11/24|
|190|[wuyr/ActivityMessenger](https://github.com/wuyr/ActivityMessenger)|ActivityMessenger，借助Kotlin特性，简化Activity之间传参和回调的逻辑代码。|48|Kotlin|2021/05/15|
|191|[IceCream-Open/Rain](https://github.com/IceCream-Open/Rain)|Java Application 开发框架|48|Kotlin|2021/08/20|
|192|[congHu/DD_Monitor-android-kotlin](https://github.com/congHu/DD_Monitor-android-kotlin)|DD监控室，安卓版，Kotlin|48|Kotlin|2021/10/28|
|193|[dacaoyuan/YPKTabDemo](https://github.com/dacaoyuan/YPKTabDemo)|一个自定义tablayout View|48|Kotlin|2021/09/23|
|194|[itning/YunShuClassSchedule](https://github.com/itning/YunShuClassSchedule)|Android的开源课程表应用，支持上下课提醒，上课手机自动静音等功能。遵循Material Design设计|47|Kotlin|2021/09/18|
|195|[forJrking/DrawableDsl](https://github.com/forJrking/DrawableDsl)|用dsl语法创建drawable|47|Kotlin|2021/05/21|
|196|[JackLiaoJH/ImageSelect](https://github.com/JackLiaoJH/ImageSelect)|android 经量级选择图片框架，支持拍照，获取相册图片，可以多选，单选|47|Kotlin|2021/09/11|
|197|[cssxsh/bilibili-helper](https://github.com/cssxsh/bilibili-helper)|mirai-console 插件开发计划|47|Kotlin|2021/11/26|
|198|[Dr-TSNG/Fuck-Dmzj](https://github.com/Dr-TSNG/Fuck-Dmzj)|动漫之家增强模块|45|Kotlin|2021/07/04|
|199|[RC1844/MIUI_IME_Unlock](https://github.com/RC1844/MIUI_IME_Unlock)|解锁 MIUI 全面屏优化限制|45|Kotlin|2021/10/13|
|200|[leavesC/Activity](https://github.com/leavesC/Activity)|一个用于查看系统安装的所有应用的详细信息的 App|45|Kotlin|2021/06/07|

⬆ [回到目录](#内容目录)

<br/>

## Swift

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[Caldis/Mos](https://github.com/Caldis/Mos)|一个用于在 macOS 上平滑你的鼠标滚动效果或单独设置滚动方向的小工具, 让你的滚轮爽如触控板     A lightweight tool used to smooth scrolling and set scroll direction independently for your mouse on macOS|7.6k|Swift|2021/06/05|
|2|[longitachi/ZLPhotoBrowser](https://github.com/longitachi/ZLPhotoBrowser)|Wechat-like image picker. Support select normal photos, videos, gif and livePhoto. Support edit image and crop video. 微信样式的图片选择器，支持预览/相册内拍照及录视频、拖拽/滑动选择，编辑图片/视频，支持多语言国际化等功能; |3.9k|Swift|2021/11/24|
|3|[SwiftOldDriver/iOS-Weekly](https://github.com/SwiftOldDriver/iOS-Weekly)|🇨🇳 老司机 iOS 周报|3.9k|Swift|2021/11/29|
|4|[100mango/zen](https://github.com/100mango/zen)|iOS, macOS, Swift, Objective-C 心得|2.8k|Swift|2021/09/10|
|5|[wxxsw/SwiftTheme](https://github.com/wxxsw/SwiftTheme)|🎨 Powerful theme/skin manager for iOS 9+ 主题/换肤, 暗色模式|2.2k|Swift|2021/11/04|
|6|[Danie1s/Tiercel](https://github.com/Danie1s/Tiercel)|简单易用、功能丰富的纯 Swift 下载框架|2.2k|Swift|2021/11/08|
|7|[tid-kijyun/Kanna](https://github.com/tid-kijyun/Kanna)|Kanna(鉋) is an XML/HTML parser for Swift.|2.2k|Swift|2021/12/01|
|8|[AAChartModel/AAChartKit-Swift](https://github.com/AAChartModel/AAChartKit-Swift)|📈📊📱💻🖥️An elegant modern declarative data visualization chart framework for iOS, iPadOS and macOS. Extremely powerful, supports line, spline, area, areaspline, column, bar, pie, scatter, angular gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar ...|2.0k|Swift|2021/11/17|
|9|[pujiaxin33/JXSegmentedView](https://github.com/pujiaxin33/JXSegmentedView)|A powerful and easy to use segmented view (segmentedcontrol, pagingview, pagerview, pagecontrol, categoryview) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)|1.9k|Swift|2021/08/18|
|10|[MxABC/swiftScan](https://github.com/MxABC/swiftScan)|A barcode and qr code scanner( 二维码 各种码识别，生成，界面效果)|1.4k|Swift|2021/11/19|
|11|[netyouli/WHC_ConfuseSoftware](https://github.com/netyouli/WHC_ConfuseSoftware)|iOS代码混淆工具，iOS代码混淆助手，过机器审核，过4.3审核，过other审核，u3d、cocos2dx、flutter、自动代码翻新(WHC_ConfuseSoftware)是一款新一代运行在MAC OS平台的App、完美支持Objc和Swift、U3D、Flutter、Cocos2dx项目代码的自动翻新(混淆)、支持文件夹名称、文件名、修改资源文件hash值、类名、方法名、属性名、添加混淆函数方法体、添加混淆属性、自动调用生成的混淆方法、字符串混淆加密等。。。功能强大而稳定。|1.2k|Swift|2021/11/29|
|12|[JiongXing/PhotoBrowser](https://github.com/JiongXing/PhotoBrowser)| Elegant photo browser in Swift. 图片与视频浏览器。|1.1k|Swift|2021/09/20|
|13|[youngsoft/TangramKit](https://github.com/youngsoft/TangramKit)|TangramKit is a powerful iOS UI framework implemented by Swift. It integrates the functions with Android layout,iOS AutoLayout,SizeClass, HTML CSS float and flexbox and bootstrap. So you can use LinearLayout,RelativeLayout,FrameLayout,TableLayout,FlowLayout,FloatLayout,LayoutSizeClass to build your  ...|1.1k|Swift|2021/08/29|
|14|[dengzemiao/DZMeBookRead](https://github.com/dengzemiao/DZMeBookRead)|支持项目使用！最完整小说阅读器Demo！仿iReader(掌阅)，QQ阅读 ... 常用阅读器阅读页面，支持 翻页效果(仿真,覆盖,平移,滚动,无效果)、字体切换、书签功能、阅读记录、亮度调整、背景颜色切换 ...|1.0k|Swift|2021/11/30|
|15|[gltwy/LTScrollView](https://github.com/gltwy/LTScrollView)|ScrollView嵌套ScrolloView（UITableView 、UICollectionView）解决方案， 支持OC / Swift（持续更新中...）实现原理：http://blog.csdn.net/glt_code/article/details/78576628|968|Swift|2021/11/20|
|16|[spicyShrimp/U17](https://github.com/spicyShrimp/U17)|精仿有妖气漫画(Swift5)|920|Swift|2021/10/25|
|17|[xjbeta/iina-plus](https://github.com/xjbeta/iina-plus)|Extra danmaku support for iina. (iina 弹幕支持|650|Swift|2021/11/30|
|18|[ming1016/SwiftPamphletApp](https://github.com/ming1016/SwiftPamphletApp)|戴铭的 Swift 小册子，一本活的 Swift 手册|629|Swift|2021/11/30|
|19|[lixiang1994/AttributedString](https://github.com/lixiang1994/AttributedString)|基于Swift插值方式优雅的构建富文本, 支持点击长按事件, 支持不同类型过滤, 支持自定义视图等.|472|Swift|2021/10/29|
|20|[Cay-Zhang/RSSBud](https://github.com/Cay-Zhang/RSSBud)|RSSHub 的辅助 iOS App，和 RSSHub Radar 类似，他可以帮助你快速发现和订阅网站的 RSS。现已在 App Store 上架。|456|Swift|2021/11/28|
|21|[Coder-TanJX/JXBanner](https://github.com/Coder-TanJX/JXBanner)|🚀🚀🚀 A super - custom multifunctional framework for banner unlimited rollover diagrams  [一个超自定义多功能无限轮播图框架]|454|Swift|2021/09/22|
|22|[lixiang1994/AutoInch](https://github.com/lixiang1994/AutoInch)|优雅的iPhone全尺寸/等比例精准适配工具|429|Swift|2021/10/11|
|23|[easyui/EZPlayer](https://github.com/easyui/EZPlayer)|基于AVPlayer封装的视频播放器，功能丰富，快速集成，可定制性强，支持react-native。|415|Swift|2021/11/08|
|24|[zqqf16/SYM](https://github.com/zqqf16/SYM)|A crash log symbolicating Mac app   一个图形化的崩溃日志符号化工具|411|Swift|2021/11/02|
|25|[cmushroom/redis-pro](https://github.com/cmushroom/redis-pro)|redis-pro redis 桌面管理工具|407|Swift|2021/11/30|
|26|[choiceyou/FWPopupView](https://github.com/choiceyou/FWPopupView)|弹窗控件：支持AlertView、Sheet、自定义视图的PopupView。AlertView中可以嵌套自定义视图，各组件的显示隐藏可配置；Sheet仿微信样式；同时提供自定义弹出。更多配置请参考”可设置参数“，提供OC使用Demo。|399|Swift|2021/07/14|
|27|[Tliens/SpeedySwift](https://github.com/Tliens/SpeedySwift)|这是一个app开发的加速库。This is an accelerated library for app development|391|Swift|2021/11/09|
|28|[mengxianliang/XLCardSwitch](https://github.com/mengxianliang/XLCardSwitch)|iOS 利用余弦函数特性实现可以居中放大的图片浏览工具|372|Swift|2021/11/11|
|29|[dudongge/DDGScreenShot](https://github.com/dudongge/DDGScreenShot)|DDGScreenShot截屏图片处理，只需一句代码,复杂屏幕截屏（如view ScrollView webView wkwebView）,图片后期处理，拼图，裁剪等|369|Swift|2021/05/24|
|30|[Sunnyyoung/AppleReserver](https://github.com/Sunnyyoung/AppleReserver)|Apple 官方预约命令行监控助手|362|Swift|2021/11/26|
|31|[fcbox/Lantern](https://github.com/fcbox/Lantern)|基于Swift的高可用视图框架|361|Swift|2021/08/31|
|32|[MQZHot/ZCycleView](https://github.com/MQZHot/ZCycleView)|使用UICollectionView实现常见图片无限轮播，支持自定义cell，自定义pageControl，以及轮播样式|357|Swift|2021/07/23|
|33|[pro648/BasicDemos-iOS](https://github.com/pro648/BasicDemos-iOS)|iOS学习进程中的demo汇总|350|Swift|2021/10/15|
|34|[JmoVxia/CLDemo](https://github.com/JmoVxia/CLDemo)|这是一个Demo空间，持续更新|348|Swift|2021/10/26|
|35|[LvJianfeng/LLCycleScrollView](https://github.com/LvJianfeng/LLCycleScrollView)|Swift - 轮播图，文本轮播，支持左右箭头|340|Swift|2021/10/27|
|36|[Liaoworking/Advanced-Swift](https://github.com/Liaoworking/Advanced-Swift)|Notes of Advanced Swift. 《swift进阶》学习笔记 swift 5.3|308|Swift|2021/10/30|
|37|[Coder-TanJX/JXPageControl](https://github.com/Coder-TanJX/JXPageControl)|🚀🚀🚀 自定义pageControl指示器, 支持多种动画, 自定义布局.|284|Swift|2021/09/20|
|38|[zhangzhao4444/Fastmonkey](https://github.com/zhangzhao4444/Fastmonkey)|非插桩 iOS Monkey,  支持控件，每秒4-5 action事件|261|Swift|2021/10/11|
|39|[Allen0828/AEAlertView](https://github.com/Allen0828/AEAlertView)|Custom AlertView supports multiple modes 自定义AlertView 支持多种模式弹窗|254|Swift|2021/11/19|
|40|[04zhujunjie/ZJJPopup](https://github.com/04zhujunjie/ZJJPopup)|选择器 弹框|249|Swift|2021/05/13|
|41|[04zhujunjie/ZJJForm](https://github.com/04zhujunjie/ZJJForm)|TableView 表单|249|Swift|2021/05/17|
|42|[Light413/dctt](https://github.com/Light413/dctt)|xx头条【完整项目持续迭代中】(一款本地生活信息发布APP，分享新鲜事、找人找对象等生活服务)。已App Store上架，这里仅供学习交流|245|Swift|2021/09/23|
|43|[pujiaxin33/JXTheme](https://github.com/pujiaxin33/JXTheme)|A powerful and lightweight and customization theme/skin library for iOS 9+ in swift. 主题、换肤、暗黑模式|233|Swift|2021/08/27|
|44|[Code-T/salon-resources](https://github.com/Code-T/salon-resources)|你可以在这里获取相关的资料。|217|Swift|2021/07/21|
|45|[SunshineBrother/SwiftTools](https://github.com/SunshineBrother/SwiftTools)|一个基于BeeHive实现的组件化方案|215|Swift|2021/09/27|
|46|[Noah37/zhuishushenqi](https://github.com/Noah37/zhuishushenqi)|追书神器Swift版客户端（非官方）。 不断更新中......|212|Swift|2021/06/13|
|47|[wxxsw/GSPlayer](https://github.com/wxxsw/GSPlayer)|⏯ Video player, support for caching, preload, fullscreen transition and custom control view. 视频播放器，支持边下边播、预加载、全屏转场和自定义控制层|209|Swift|2021/07/20|
|48|[CoderLinLee/LLSegmentViewController](https://github.com/CoderLinLee/LLSegmentViewController)|可添加header的多控制器列表,主流APP分类切换滚动视图(京东，网易新闻，爱奇艺，QQ弹性小球等，可高度自定义,项目结构清晰);UIScrollView 嵌套,可做个人详情页，商品详情页，页面多tableView滑动悬停|206|Swift|2021/06/28|
|49|[imeiju/iMeiJu_Mac](https://github.com/imeiju/iMeiJu_Mac)|爱美剧Mac客户端|201|Swift|2021/06/21|
|50|[JoanKing/JKSwiftExtension](https://github.com/JoanKing/JKSwiftExtension)|Swift常用扩展、组件、协议，方便项目快速搭建，提供完整清晰的Demo示例，不断的完善中...... |197|Swift|2021/11/25|
|51|[hui-z/image_gallery_saver](https://github.com/hui-z/image_gallery_saver)|flutter中用于保存图片到相册的Plugin|173|Swift|2021/10/13|
|52|[wangrui460/WRCycleScrollView](https://github.com/wangrui460/WRCycleScrollView)|Swift 自动无限轮播用这个就够了 swift 4|159|Swift|2021/07/19|
|53|[Liaoworking/MoyaNetworkTool](https://github.com/Liaoworking/MoyaNetworkTool)|a robust networkTool based on Moya. Moya Demo   一个强健的基于moya二次封装的网络框架|156|Swift|2021/11/18|
|54|[xindong/anti-addiction-kit](https://github.com/xindong/anti-addiction-kit)|📱手机游戏防沉迷系统 SDK，支持 iOS+Android+Unity，快速接入！|150|Swift|2021/09/17|
|55|[rztime/RZRichTextView](https://github.com/rztime/RZRichTextView)|iOS 原生UITextView 富文本编辑器|138|Swift|2021/11/18|
|56|[pujiaxin33/JXPopupView](https://github.com/pujiaxin33/JXPopupView)|一个轻量级的自定义视图弹出框架|129|Swift|2021/10/12|
|57|[FlutterTaoBaoKe/flutter_alibc](https://github.com/FlutterTaoBaoKe/flutter_alibc)|flutter版本的阿里百川插件|121|Swift|2021/08/03|
|58|[937447974/YJCocoa](https://github.com/937447974/YJCocoa)|YJ 系列 Pod 开源库|118|Swift|2021/10/18|
|59|[gl-lei/algorithm](https://github.com/gl-lei/algorithm)|《数据结构与算法之美》学习笔记以及 Swift 代码实现 ，原始仓库 https://github.com/wangzheng0822/algo|113|Swift|2021/08/24|
|60|[ethanhuang13/blahker](https://github.com/ethanhuang13/blahker)|巴拉剋 - Safari 蓋版廣告消除器|110|Swift|2021/07/13|
|61|[qyz777/DanmakuKit](https://github.com/qyz777/DanmakuKit)|高性能弹幕框架 DanmakuKit is a high performance library that provides the basic functions of danmaku.|105|Swift|2021/09/05|
|62|[parkingwang/vehicle-keyboard-ios](https://github.com/parkingwang/vehicle-keyboard-ios)|停车王车牌号码专用键盘 - iOS|103|Swift|2021/07/27|
|63|[kingsic/PagingViewKit](https://github.com/kingsic/PagingViewKit)|A powerful and easy to use segment view 【QQ、淘宝、微博、腾讯、网易新闻、今日头条等标题滚动视图】|96|Swift|2021/11/20|
|64|[LinXunFeng/LXFProtocolTool](https://github.com/LinXunFeng/LXFProtocolTool)|由Swift中协议方式实现功能的实用工具库【Refreshable、EmptyDataSetable 支持 Rx 】|96|Swift|2021/10/19|
|65|[wxxsw/Refresh](https://github.com/wxxsw/Refresh)|🎈 Great SwiftUI drop-down refresh and scroll up to load more. 下拉刷新、上拉加载|93|Swift|2021/09/13|
|66|[william0wang/MagicCamera](https://github.com/william0wang/MagicCamera)|iOS多功能AI相机：人像卡通化、变老变年轻、美颜、滤镜、艺术效果等|85|Swift|2021/11/08|
|67|[JWAutumn/ACarousel](https://github.com/JWAutumn/ACarousel)|A carousel view for SwiftUI   SwiftUI 旋转木马效果|77|Swift|2021/10/12|
|68|[sulioppa/ChineseChess](https://github.com/sulioppa/ChineseChess)|Chinese Chess（中国象棋） - A Free iOS App（C & Obj-C & Swift）|73|Swift|2021/09/12|
|69|[BoxDengJZ/AudioJz](https://github.com/BoxDengJZ/AudioJz)|个人学过的，音频相关的技巧。从 ray wenderlich 开始|72|Swift|2021/06/11|
|70|[iOSPrincekin/PrincekinKlineFrame](https://github.com/iOSPrincekin/PrincekinKlineFrame)|一款用Swift开发的K线图和深度图组件，具有轻量、敏捷等特点，可供虚拟货币行业和金融行业使用|70|Swift|2021/11/10|
|71|[HuaZao/TC1-NG](https://github.com/HuaZao/TC1-NG)|斐讯TC1 DC1 A1 M1 iOS客户端|66|Swift|2021/06/30|
|72|[trilliwon/JNaturalKorean](https://github.com/trilliwon/JNaturalKorean)|한글 (조사, 助詞, postposition)|64|Swift|2021/11/30|
|73|[Allen0828/GPUImage2-Swift](https://github.com/Allen0828/GPUImage2-Swift)|使用GPUImage2时常见的问题。常用滤镜组合的参考。|63|Swift|2021/07/15|
|74|[Tao93/NetTool](https://github.com/Tao93/NetTool)|macOS 状态栏小工具实时显示网速. macOS menubar tool to monitor network speed.|60|Swift|2021/08/28|
|75|[WangLiquan/EWAddressPicker-Swift](https://github.com/WangLiquan/EWAddressPicker-Swift)|A custom addressPicker.Swift.地址选择器,选择省市地区.|59|Swift|2021/05/18|
|76|[xaoxuu/ProHUD](https://github.com/xaoxuu/ProHUD)|完全可定制化的HUD，包含顶部区域的Toast，中央区域的Alert和底部区域的ActionSheet。|59|Swift|2021/08/16|
|77|[YTiOSer/YTAnimation](https://github.com/YTiOSer/YTAnimation)|iOS动画集锦, swift, 核心动画, 基础动画,关键帧动画, 组动画, 过渡动画, 进度条,项目案例.|58|Swift|2021/10/06|
|78|[shinnytech/shinny-futures-ios](https://github.com/shinnytech/shinny-futures-ios)|一个开源的 ios 平台期货行情交易终端|54|Swift|2021/07/13|
|79|[xidian-rs/Ruisi_iOS](https://github.com/xidian-rs/Ruisi_iOS)|西电睿思手机客户端[iOS]适用于discuz论坛|53|Swift|2021/11/26|
|80|[yungfan/SwiftUI-learning](https://github.com/yungfan/SwiftUI-learning)|SwiftUI教程配套代码(SwiftUI+SwiftUI 2.0+SwiftUI 3.0)|47|Swift|2021/11/01|
|81|[yuldong/iOS-interviews](https://github.com/yuldong/iOS-interviews)|尝试解答 -> 阿里、字节：一套高效的iOS面试题|46|Swift|2021/05/16|
|82|[V5zhou/ZZXcodeFormat](https://github.com/V5zhou/ZZXcodeFormat)|支持OC与swift代码格式化|46|Swift|2021/05/19|
|83|[Boxzhi/HZNavigationBar](https://github.com/Boxzhi/HZNavigationBar)|A very simple to use, can be completely customized navigation bar.  一款使用非常简单，可以完全自定义的导航栏。 |46|Swift|2021/05/13|
|84|[seasonZhu/RxStudy](https://github.com/seasonZhu/RxStudy)|RxSwift/RxCocoa框架，MVVM模式编写wanandroid客户端|45|Swift|2021/11/05|
|85|[liujunliuhong/DragCardContainer](https://github.com/liujunliuhong/DragCardContainer)|高度还原类似探探等社交应用的滑牌、卡牌交互效果|43|Swift|2021/10/22|
|86|[simon9211/privacyInsight](https://github.com/simon9211/privacyInsight)|读取app「记录App活动」文件，展示app访问权限及网络记录|43|Swift|2021/10/28|
|87|[kingjiajie/JJCollectionViewRoundFlowLayout_Swift](https://github.com/kingjiajie/JJCollectionViewRoundFlowLayout_Swift)|JJCollectionViewRoundFlowLayout_Swift是JJCollectionViewRoundFlowLayout（OC：https://github.com/kingjiajie/JJCollectionViewRoundFlowLayout ）的Swift版本，JJCollectionViewRoundFlowLayout可设置CollectionView的BackgroundColor，可根据用户Cell个数计算背景图尺寸，可自定义是否包括计算CollectionViewHeaderView、CollectionViewFootererView或只计算Cells。 ...|43|Swift|2021/11/23|
|88|[CoderHuiYu/MMAdvertScrollView](https://github.com/CoderHuiYu/MMAdvertScrollView)|一个简单、轻量级的swift版公告轮播框架|41|Swift|2021/09/15|
|89|[jaywcjlove/swiftui-example](https://github.com/jaywcjlove/swiftui-example)|SwiftUI 示例，技巧和技术集合，帮助我构建应用程序，解决问题以及了解SwiftUI的实际工作方式。|41|Swift|2021/11/27|
|90|[jackiehu/SwiftMediator](https://github.com/jackiehu/SwiftMediator)|swift砖块系列：Swift组件化解耦中间件，router，Mirror反射，函数调用，SceneDelegate解耦，AppDelegate解耦|40|Swift|2021/11/19|
|91|[WangLiquan/EWDatePicker](https://github.com/WangLiquan/EWDatePicker)|A custom datePicker.基于ViewController的从下方弹出日期选择器|40|Swift|2021/05/18|
|92|[BugenZhao/MNGA](https://github.com/BugenZhao/MNGA)|💬 A refreshing NGA Forum App in SwiftUI. Make NGA Great Again! aka "NGA 论坛 iOS 开源客户端"|40|Swift|2021/11/27|
|93|[Nemocdz/ImageCompress-iOS](https://github.com/Nemocdz/ImageCompress-iOS)|基于 ImageIO 支持动静态的图片压缩库|38|Swift|2021/10/20|
|94|[ChinaArJun/shopping-cart-Demo](https://github.com/ChinaArJun/shopping-cart-Demo)|IOS购物车: 用Swift 写的购物车Demo， shopping cart ， swift 4.0|37|Swift|2021/09/16|
|95|[SketchK/ImportSanitizer](https://github.com/SketchK/ImportSanitizer)|一款能够帮助开发者自动解决工程中各类头文件引用问题的 CLI 工具，简单，高效，全能的它会让你爱不释手！|36|Swift|2021/05/11|
|96|[isHYE/HYPlayerDemo](https://github.com/isHYE/HYPlayerDemo)|音视频播放器|33|Swift|2021/11/25|
|97|[ZhongshanHuang/PoReader](https://github.com/ZhongshanHuang/PoReader)|本地小说阅读器，支持深色模式，Wifi传书，代码简洁有注释(local text reader, support dark modal, upload text by wifi)|32|Swift|2021/10/20|
|98|[FreeYXY/SwiftProject](https://github.com/FreeYXY/SwiftProject)|swift项目实战|32|Swift|2021/10/08|
|99|[Tliens/SSPlan](https://github.com/Tliens/SSPlan)|《今日计划》源代码（open source for the app：今日计划）|30|Swift|2021/10/07|
|100|[ilobos/DeviceSupport](https://github.com/ilobos/DeviceSupport)|iOS真机调试支持文件，支持iOS9-15，已支持最新的iOS15.2；解决【Your Xcode version may be too old for your iOS version.】、无法识别设备等问题。|29|Swift|2021/11/11|

⬆ [回到目录](#内容目录)

<br/>

## Objective-C

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[MustangYM/WeChatExtension-ForMac](https://github.com/MustangYM/WeChatExtension-ForMac)|Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)|18.9k|Objective-C|2021/11/26|
|2|[banchichen/TZImagePickerController](https://github.com/banchichen/TZImagePickerController)|一个支持多选、选原图和视频的图片选择器，同时有预览、裁剪功能，支持iOS6+。  A clone of UIImagePickerController, support picking multiple photos、original photo、video, also allow preview photo and video, support iOS6+|7.7k|Objective-C|2021/11/06|
|3|[renzifeng/ZFPlayer](https://github.com/renzifeng/ZFPlayer)|Support customization of any player SDK and control layer(支持定制任何播放器SDK和控制层)|6.7k|Objective-C|2021/11/04|
|4|[ChenYilong/CYLTabBarController](https://github.com/ChenYilong/CYLTabBarController)|[EN]It is an iOS UI module library for adding animation to iOS tabbar items and icons with Lottie, and adding a bigger center UITabBar Item.  [CN]【中国特色 TabBar】一行代码实现 Lottie 动画TabBar，支持中间带+号的TabBar样式，自带红点角标，支持动态刷新。【iOS13 & Dark Mode  & iPhone XS MAX supported】|6.6k|Objective-C|2021/08/30|
|5|[Tencent/QMUI_iOS](https://github.com/Tencent/QMUI_iOS)|QMUI iOS——致力于提高项目 UI 开发效率的解决方案|6.4k|Objective-C|2021/11/29|
|6|[Sunnyyoung/WeChatTweak-macOS](https://github.com/Sunnyyoung/WeChatTweak-macOS)|A dynamic library tweak for WeChat macOS - 首款微信 macOS 客户端撤回拦截与多开|6.4k|Objective-C|2021/10/19|
|7|[gsdios/SDCycleScrollView](https://github.com/gsdios/SDCycleScrollView)|Autoscroll Banner.   无限循环图片、文字轮播器。|6.1k|Objective-C|2021/08/03|
|8|[pujiaxin33/JXCategoryView](https://github.com/pujiaxin33/JXCategoryView)|A powerful and easy to use category view (segmentedcontrol, segmentview, pagingview, pagerview, pagecontrol) (腾讯新闻、今日头条、QQ音乐、网易云音乐、京东、爱奇艺、腾讯视频、淘宝、天猫、简书、微博等所有主流APP分类切换滚动视图)|5.5k|Objective-C|2021/10/25|
|9|[ripperhe/Bob](https://github.com/ripperhe/Bob)|Bob 是一款 Mac 端翻译软件，支持划词翻译、截图翻译以及手动输入翻译。|4.5k|Objective-C|2021/11/30|
|10|[AAChartModel/AAChartKit](https://github.com/AAChartModel/AAChartKit)|📈📊🚀🚀🚀An elegant modern declarative data visualization chart framework for iOS, iPadOS and macOS. Extremely powerful, supports line, spline, area, areaspline, column, bar, pie, scatter, angular gauges, arearange, areasplinerange, columnrange, bubble, box plot, error bars, funnel, waterfall and polar  ...|4.3k|Objective-C|2021/11/23|
|11|[skyming/iOS-Performance-Optimization](https://github.com/skyming/iOS-Performance-Optimization)|关于iOS 性能优化梳理、内存泄露、卡顿、网络、GPU、电量、 App 包体积瘦身、启动速度优化等、Instruments 高级技巧、常见的优化技能- Get — Edit|4.3k|Objective-C|2021/08/09|
|12|[youngsoft/MyLinearLayout](https://github.com/youngsoft/MyLinearLayout)|MyLayout is a powerful iOS UI framework implemented by Objective-C. It integrates the functions with Android Layout,iOS AutoLayout,SizeClass, HTML CSS float and flexbox and bootstrap. So you can use LinearLayout,RelativeLayout,FrameLayout,TableLayout,FlowLayout,FloatLayout,PathLayout,GridLayout,Layo ...|4.2k|Objective-C|2021/07/26|
|13|[JackJiang2011/MobileIMSDK](https://github.com/JackJiang2011/MobileIMSDK)|一个原创移动端IM通信层框架，轻量级、高度提炼，历经8年、久经考验。可能是市面上唯一同时支持UDP+TCP+WebSocket三种协议的同类开源框架，支持iOS、Android、Java、H5，服务端基于Netty。|3.8k|Objective-C|2021/11/08|
|14|[zhengwenming/WMPlayer](https://github.com/zhengwenming/WMPlayer)|WMPlayer-AVPlayer的封装，继承UIView，支持pods，手势快进、快退，全面适配全面屏，同时支持网络和本地视频的播放|3.2k|Objective-C|2021/08/02|
|15|[rime/squirrel](https://github.com/rime/squirrel)|【鼠鬚管】Rime for macOS|3.0k|Objective-C|2021/11/25|
|16|[wangrui460/WRNavigationBar](https://github.com/wangrui460/WRNavigationBar)|超简单！！！ 一行代码设置状态栏、导航栏按钮、标题、颜色、透明度，移动等    WRNavigationBar which allows you to change NavigationBar's appearance dynamically|2.9k|Objective-C|2021/07/19|
|17|[liberalisman/iOS-InterviewQuestion-collection](https://github.com/liberalisman/iOS-InterviewQuestion-collection)|iOS 开发者在面试过程中，常见的一些面试题，建议尽量弄懂了原理，并且多实践。|2.9k|Objective-C|2021/06/09|
|18|[tigerAndBull/TABAnimated](https://github.com/tigerAndBull/TABAnimated)|A skeleton screen framework based on native for iOS. (一个由iOS原生组件映射出骨架屏的框架，包含快速植入，低耦合，兼容复杂视图等特点，提供国内主流骨架屏动画的加载方案，同时支持上拉加载更多、自定制动画。)|2.9k|Objective-C|2021/11/16|
|19|[Tencent/vap](https://github.com/Tencent/vap)|VAP是企鹅电竞开发，用于播放特效动画的实现方案。具有高压缩率、硬件解码等优点。同时支持 iOS,Android,Web 平台。|2.7k|Objective-C|2021/11/29|
|20|[iodefog/VipVideo](https://github.com/iodefog/VipVideo)|各大网站vip视频免费观看 等 Mac版。付费电影，VIP会员剧等，去广告播放。自用视频或者电影URL，音乐破解URL，CCTV等电视播放URL|2.4k|Objective-C|2021/10/20|
|21|[SilenceLove/HXPhotoPicker](https://github.com/SilenceLove/HXPhotoPicker)|图片/视频选择器 - 支持LivePhoto、GIF图片选择、3DTouch预览、在线下载iCloud上的资源、编辑图片/视频、浏览网络图片 功能    Imitation wx photo/image picker - support for LivePhoto, GIF image selection, 3DTouch preview, Download the resources on iCloud online, browse the web image function|2.3k|Objective-C|2021/11/30|
|22|[pujiaxin33/JXPagingView](https://github.com/pujiaxin33/JXPagingView)|类似微博主页、简书主页等效果。多页面嵌套，既可以上下滑动，也可以左右滑动切换页面。支持HeaderView悬浮、支持下拉刷新、上拉加载更多。|2.3k|Objective-C|2021/11/20|
|23|[indulgeIn/YBImageBrowser](https://github.com/indulgeIn/YBImageBrowser)|iOS image browser / iOS 图片浏览器 (支持视频) |2.2k|Objective-C|2021/08/17|
|24|[buginux/WeChatRedEnvelop](https://github.com/buginux/WeChatRedEnvelop)|iOS版微信抢红包Tweak|2.2k|Objective-C|2021/05/09|
|25|[91renb/BRPickerView](https://github.com/91renb/BRPickerView)|BRPickerView 封装的是iOS中常用的选择器组件，主要包括：日期选择器（支持年月日、年月等15种日期样式选择，支持设置星期、至今等）、地址选择器（支持省市区、省市、省三种地区选择）、自定义字符串选择器（支持单列、多列、二级联动、三级联动选择）。支持自定义主题样式，适配深色模式，支持将选择器组件添加到指定容器视图。|2.1k|Objective-C|2021/10/12|
|26|[objccn/articles](https://github.com/objccn/articles)|Articles for objccn.io. objc.io的完整、准确、优雅的中文翻译版本|2.1k|Objective-C|2021/11/02|
|27|[changsanjiang/SJVideoPlayer](https://github.com/changsanjiang/SJVideoPlayer)|iOS VideoPlayer MediaPlayer video player media player 短视频播放器 可接入 ijkplayer aliplayer alivodplayer plplayer|2.1k|Objective-C|2021/10/28|
|28|[pili-engineering/PLPlayerKit](https://github.com/pili-engineering/PLPlayerKit)|PLPlayerKit 是七牛推出的一款免费的适用于 iOS 平台的播放器 SDK，采用全自研的跨平台播放内核，拥有丰富的功能和优异的性能，可高度定制化和二次开发。|2.0k|Objective-C|2021/11/05|
|29|[nacker/LZEasemob3](https://github.com/nacker/LZEasemob3)|酷信   高仿微信  WeChat   高仿朋友圈   moments  Github上最牛逼的高仿微信项目没有之一|1.9k|Objective-C|2021/08/30|
|30|[shaojiankui/SmartPush](https://github.com/shaojiankui/SmartPush)|SmartPush,一款iOS苹果远程推送测试程序,Mac OS下的APNS工具APP,iOS Push Notification Debug App|1.8k|Objective-C|2021/09/17|
|31|[zekunyan/TTGTagCollectionView](https://github.com/zekunyan/TTGTagCollectionView)|Useful for showing text or custom view tags in a vertical or horizontal scrollable view and support Autolayout at the same time. It is highly customizable that most features of the text tag can be configured. 标签流显示控件，同时支持文字或自定义View|1.6k|Objective-C|2021/08/11|
|32|[huanxsd/LinkMap](https://github.com/huanxsd/LinkMap)|检查每个类占用空间大小工具|1.6k|Objective-C|2021/07/29|
|33|[zhenglibao/FlexLib](https://github.com/zhenglibao/FlexLib)|FlexLib是一个基于flexbox模型，使用xml文件进行界面布局的框架，融合了web快速布局的能力，让iOS界面开发像写网页一样简单快速|1.6k|Objective-C|2021/10/11|
|34|[kingsic/SGQRCode](https://github.com/kingsic/SGQRCode)|The easy to use bar code and QR code scan library for iOS【支持二维码生成、从相册中读取二维码、条形码和二维码扫描】|1.6k|Objective-C|2021/09/19|
|35|[ming1016/GCDFetchFeed](https://github.com/ming1016/GCDFetchFeed)|“已阅”新版，RSS阅读器，使用FMDB做存储，ReactiveCocoa处理数据流向|1.6k|Objective-C|2021/10/27|
|36|[didi/DiDiPrism](https://github.com/didi/DiDiPrism)|小桔棱镜，一款专注移动端操作行为的利器！ A powerful tool (iOS & Android) that focuses on mobile operation behavior!|1.5k|Objective-C|2021/11/10|
|37|[kingsic/SGPagingView](https://github.com/kingsic/SGPagingView)|A powerful and easy to use segment view 【QQ、淘宝、微博、腾讯、网易新闻、今日头条等标题滚动视图】|1.5k|Objective-C|2021/05/05|
|38|[jkpang/PPRows](https://github.com/jkpang/PPRows)|It Can Calculate how many lines of code you write on Mac ; 在Mac上计算你写了多少行代码|1.4k|Objective-C|2021/10/21|
|39|[sinaweibosdk/weibo_ios_sdk](https://github.com/sinaweibosdk/weibo_ios_sdk)|新浪微博 IOS SDK|1.4k|Objective-C|2021/10/12|
|40|[LeoMobileDeveloper/Blogs](https://github.com/LeoMobileDeveloper/Blogs)|一点心得 - iOS,Swift,React Native,Python...|1.4k|Objective-C|2021/11/30|
|41|[jezzmemo/JJException](https://github.com/jezzmemo/JJException)|Protect the objective-c application(保护App不闪退)|1.4k|Objective-C|2021/09/28|
|42|[netease-kit/NIM_iOS_UIKit](https://github.com/netease-kit/NIM_iOS_UIKit)|网易云信 iOS UI 组件，提供聊天界面，文本消息，图片消息，语音消息，视频消息，地理位置消息，自定义消息（阅后即焚）等消息示例。#推荐客户得比特币/京东卡，现在推荐使用网易云信，最低得0.02BTC或3000元京东卡/单，点击参与：https://yunxin.163.com/promotion/recommend|1.4k|Objective-C|2021/11/15|
|43|[weidian-inc/hera](https://github.com/weidian-inc/hera)|A framework for running WeChat applet. （小程序 SDK，小程序转 H5，小程序转安卓、iOS 原生应用、小程序渲染引擎）|1.3k|Objective-C|2021/05/05|
|44|[jpush/jpush-react-native](https://github.com/jpush/jpush-react-native)|JPush's officially supported React Native plugin (Android & iOS). 极光推送官方支持的 React Native 插件（Android & iOS）。|1.3k|Objective-C|2021/11/25|
|45|[QuintGao/GKPageScrollView](https://github.com/QuintGao/GKPageScrollView)|iOS类似微博、抖音、网易云等个人详情页滑动嵌套效果|1.2k|Objective-C|2021/11/08|
|46|[lixiang1994/LEEAlert](https://github.com/lixiang1994/LEEAlert)|优雅的可自定义 Alert ActionSheet|1.2k|Objective-C|2021/09/03|
|47|[SunshineBrother/JHBlog](https://github.com/SunshineBrother/JHBlog)|iOS开发：我的初级到中级的晋级之路|1.2k|Objective-C|2021/07/30|
|48|[liangdahong/BMLongPressDragCellCollectionView](https://github.com/liangdahong/BMLongPressDragCellCollectionView)|🎉 🎉 🎉 🎉 🎉  让你轻松实现类似支付宝的拖拽重排功能, 支持各种自定义操作。|1.2k|Objective-C|2021/05/26|
|49|[DKJone/DKWechatHelper](https://github.com/DKJone/DKWechatHelper)|不止于抢红包，功能丰富的微信插件。|1.1k|Objective-C|2021/09/18|
|50|[GGGHub/Reader](https://github.com/GGGHub/Reader)|iOS基于CoreText实现的电子书阅读器，支持txt，epub格式|1.1k|Objective-C|2021/08/28|
|51|[yongyuandouneng/YNPageViewController](https://github.com/yongyuandouneng/YNPageViewController)|特斯拉组件、QQ联系人布局、多页面嵌套滚动、悬停效果、美团、淘宝、京东、微博、腾讯新闻、网易新闻、今日头条等标题滚动视图|1.1k|Objective-C|2021/05/21|
|52|[ChenYilong/iOSBlog](https://github.com/ChenYilong/iOSBlog)| 微博@iOS程序犭袁 的blog|1.1k|Objective-C|2021/09/14|
|53|[DaidoujiChen/Dai-Hentai](https://github.com/DaidoujiChen/Dai-Hentai)|一個普通的看漫畫 App|1.1k|Objective-C|2021/11/24|
|54|[BigShow1949/BigShow1949](https://github.com/BigShow1949/BigShow1949)|iOS教学/各类知识点总结:运行时/贝塞尔曲线/水纹/粒子发射器/核心动画/渐变色/网络请求/按钮/标签/视图布局/视图效果/文字视图/表情键盘/旋转动画/2048/网易/微信/猿题库/阿里巴巴/设计模式/数据持久化/多次点击按钮/微信注册按钮/展开按钮/跑马灯/闪烁文字/球形滚动标签/自动布局标签/快播动态标签/水平滚动布局/瀑布流布局/浏览卡/半圆布局/滑动标题/抽卡效果/百度视图切换/领英动画/折卡效果/卡牌拖动翻页/滚动悬浮视图/侧滑形变效果/评分条/打印机特效/Masonry/生命周期/响应者链条/引导页/通知中心/抖动密码框/余额宝数字跳动/UIDynamic/碰撞行为/捕捉行为/ ...|1.1k|Objective-C|2021/05/23|
|55|[wildfirechat/ios-chat](https://github.com/wildfirechat/ios-chat)|开源的即时通讯(野火IM)系统|943|Objective-C|2021/11/30|
|56|[czl0325/ZLCollectionView](https://github.com/czl0325/ZLCollectionView)|为应对类似淘宝首页，京东首页，国美首页等复杂布局而写的Collectionview。基于UICollectionView实现，目前支持标签布局，列布局，百分比布局，定位布局，填充式布局，瀑布流布局等。支持纵向布局和横向布局，可以根据不同的section设置不同的布局，支持拖动cell，头部悬浮，设置section背景色和自定义section背景view，向自定义背景view传递自定义方法。实现了电影选座等高难度的布局。|941|Objective-C|2021/07/12|
|57|[QuintGao/GKPhotoBrowser](https://github.com/QuintGao/GKPhotoBrowser)|iOS仿微信、今日头条等图片浏览器|940|Objective-C|2021/11/17|
|58|[didi/echo](https://github.com/didi/echo)|Echo是一款桌面端调试工具，旨在提高客户端的研发调试效率|932|Objective-C|2021/09/27|
|59|[svga/SVGAPlayer-iOS](https://github.com/svga/SVGAPlayer-iOS)|Similar to Lottie. Render After Effects / Animate CC (Flash) animations natively on Android and iOS, Web.  使用 SVGAPlayer 在 Android、iOS、Web中播放 After Effects / Animate CC (Flash) 动画。|892|Objective-C|2021/07/06|
|60|[HeathWang/HWPanModal](https://github.com/HeathWang/HWPanModal)|HWPanModal presents controller from bottom and drag to dismiss, high customize. iOS13 default modalPresentationStyle. 任意形式的底部弹框动画；头条、知乎、抖音弹出评论效果；地图浮层，iOS13 present默认模态效果。|855|Objective-C|2021/11/30|
|61|[CRAnimation/CRBoxInputView](https://github.com/CRAnimation/CRBoxInputView)|Verify code input view. Support security type for password.短信验证码输入框，支持密文模式|840|Objective-C|2021/08/27|
|62|[wwmz/WMZDialog](https://github.com/wwmz/WMZDialog)|功能最多样式最多的弹窗，支持普通/底部/日期/地区/日历/选择/编辑/分享/菜单/自定义弹窗等,支持多种动画,链式编程调用(Pop-up windows with the most functions and styles, support normal/bottom/date/region/calendar/select/edit/share/menu/custom pop-up windows, etc., support multiple animations, chain programming calls)|829|Objective-C|2021/11/12|
|63|[dingding3w/DHGuidePageHUD](https://github.com/dingding3w/DHGuidePageHUD)|一键合成APP引导页,包含不同状态下的引导页操作方式,同时支持动态图片引导页和静态图片引导页;|804|Objective-C|2021/09/20|
|64|[lixiang1994/LEETheme](https://github.com/lixiang1994/LEETheme)|优雅的主题管理库- 一行代码完成多样式切换|781|Objective-C|2021/07/28|
|65|[yiplee/YPNavigationBarTransition](https://github.com/yiplee/YPNavigationBarTransition)|A Full functional UINavigationBar framework for making bar transition more natural! You don't need to call any UINavigationBar api, implementing YPNavigationBarConfigureStyle protocol for your view controller instead.                （类似微信 iOS Navigation Bar 的切换方案）|772|Objective-C|2021/11/03|
|66|[tencentyun/MLVBSDK](https://github.com/tencentyun/MLVBSDK)|移动直播 SDK，国内下载镜像：|758|Objective-C|2021/11/05|
|67|[MarcWeigert/GGCharts](https://github.com/MarcWeigert/GGCharts)|可以高度自定义的图表框架。柱状图、折线图、雷达图、饼图、K线图、分时图。|739|Objective-C|2021/08/20|
|68|[lifution/Popover](https://github.com/lifution/Popover)|一款优雅易用的类似QQ和微信消息页面的右上角微型菜单弹窗|731|Objective-C|2021/07/27|
|69|[jpush/jpush-flutter-plugin](https://github.com/jpush/jpush-flutter-plugin)|JPush's officially supported Flutter plugin (Android & iOS). 极光推送官方支持的 Flutter 插件（Android & iOS）。|731|Objective-C|2021/11/30|
|70|[huangfangyi/YiChat](https://github.com/huangfangyi/YiChat)|YiChat-基于tigase的独立IM系统|731|Objective-C|2021/10/15|
|71|[QuintGao/GKNavigationBarViewController](https://github.com/QuintGao/GKNavigationBarViewController)|iOS自定义导航栏-导航栏联动|703|Objective-C|2021/09/02|
|72|[12207480/LovePlayNews](https://github.com/12207480/LovePlayNews)|LovePlayNews精仿爱玩iOS app，使用AsyncDisplayKit提高UI流畅性，项目结构及代码清晰明了|669|Objective-C|2021/06/24|
|73|[520coding/confuse](https://github.com/520coding/confuse)|iOS混淆加固差异化翻新加密工具，模拟人工手动混淆，识别上下文 ，支持继承链、类型识别、方法多参等复杂高级混淆。source-to-source obfuscation of iOS projects，Xcode's refactor->rename. 告别插入毫无关联的垃圾代码、弃用无脑单词随机拼接替换，模拟正常开发，一款最好的混淆最彻底的Mac App Tools。支持OC(Objc、Objective-C)、C、C++(Cocos2d-x、Cocos2dx和Lua游戏开发)、Swift、C#(Unity)混淆，可用于ios马甲包游戏SDK混淆，减少账号调查过机审上架过包过审4.3、2.3 ...|665|Objective-C|2021/11/20|
|74|[Rogue24/JPImageresizerView](https://github.com/Rogue24/JPImageresizerView)|一个专门裁剪图片、GIF、视频的轮子😋 简单易用、功能丰富☕️（高自由度的参数设定、支持旋转和镜像翻转、蒙版、压缩等），能满足绝大部分裁剪的需求。|635|Objective-C|2021/10/20|
|75|[wwmz/WMZDropDownMenu](https://github.com/wwmz/WMZDropDownMenu)|🌹一个能几乎实现所有App各种类型筛选菜单的控件,可悬浮,目前已实现闲鱼/美团/Boss直聘/京东/饿了么/淘宝/拼多多/赶集网/美图外卖等等的筛选菜单,可以自由调用代理实现自己想组装的筛选功能和UI,且控件的生命周期自动管理,悬浮自动管理🌹(A control that can implement almost all types of filtering menus of all apps)|617|Objective-C|2021/10/25|
|76|[AllLuckly/LBLaunchImageAd](https://github.com/AllLuckly/LBLaunchImageAd)|iOS开发轻量级启动广告，动态获取网络启动图片，具有渐变的启动动画，支持半屏和全屏，类似百度ssp广告和广点通的广告。支持广告点击等，集成非常的方便。欢迎使用，好用请star|570|Objective-C|2021/09/09|
|77|[wuba/WBBlades](https://github.com/wuba/WBBlades)|基于mach-o解析技术的包大小占比分析、Objective-C & Swift无用代码（冗余类）检测、无符号表时的日志符号化 (Based on mach-o technology, unused code (unused class) detection tool, crash log symbolization tool, and app size analysis tool.)|569|Objective-C|2021/11/17|
|78|[wwmz/WMZPageController](https://github.com/wwmz/WMZPageController)|分页控制器,替换UIPageController方案,具备完整的生命周期,多种指示器样式,多种标题样式,可悬浮,支持ios13暗黑模式(仿优酷,爱奇艺,今日头条,简书,京东等多种标题菜单) (Pagination controller with full life cycle, multiple indicator styles, multiple title styles)|538|Objective-C|2021/11/17|
|79|[pili-engineering/PLMediaStreamingKit](https://github.com/pili-engineering/PLMediaStreamingKit)|PLMediaStreamingKit 是七牛推出的一款适用于 iOS 平台的推流 SDK，支持 RTMP 推流，h.264 和 AAC 编码，硬编、软编支持。具有丰富的数据和状态回调，方便用户根据自己的业务定制化开发。具有直播场景下的重要功能，如：美颜、背景音乐、水印等功能。|534|Objective-C|2021/09/27|
|80|[anyrtcIO-Community/anyRTC-RTMPC-iOS](https://github.com/anyrtcIO-Community/anyRTC-RTMPC-iOS)|基于RTMP和RTC混合引擎的视频连麦互动直播|534|Objective-C|2021/09/07|
|81|[OpenFlutter/tobias](https://github.com/OpenFlutter/tobias)|AliPay For Flutter.支付宝Flutter插件|525|Objective-C|2021/11/10|
|82|[sensorsdata/sa-sdk-ios](https://github.com/sensorsdata/sa-sdk-ios)|神策数据官方 iOS 埋点 SDK，是一款轻量级用于 iOS 端的数据采集埋点 SDK。神策分析 iOS SDK 不仅有代码埋点功能，还通过使用运行时机制（Runtime）中的相关技术实现 iOS 端的全埋点（无埋点、无码埋点、无痕埋点、自动埋点）、点击图、可视化全埋点。|509|Objective-C|2021/11/26|
|83|[2621532542/iOS_NQConfuseTool](https://github.com/2621532542/iOS_NQConfuseTool)|iOS代码混淆(iOS_NQConfuseTool)是一款运行在MACOS平台的App、完美支持OC和Swift项目代码的自动混淆、支持、文件名、修改资源文件、类名、方法名、属性名、添加混淆函数方法体、添加混淆属性、自动调用生成的混淆代码，功能强大而稳定，全局自动化，安全加固。马甲包混淆工具，最重要的是完全免费。（随机单词拼接）|509|Objective-C|2021/09/13|
|84|[g0v/moedict-webkit](https://github.com/g0v/moedict-webkit)|萌典網站|501|Objective-C|2021/10/06|
|85|[MxABC/DevDataTool](https://github.com/MxABC/DevDataTool)|编码转换、摘要(hash)、加解密（MD5、SHA1、SHA256、SHA3、SM3、HMAC、DES、3DES、AES、SM4）|467|Objective-C|2021/07/13|
|86|[czhen09/ESJsonFormatForMac](https://github.com/czhen09/ESJsonFormatForMac)|软件化ESJsonFormat插件,脱离Xcode环境运行;|465|Objective-C|2021/07/04|
|87|[QuintGao/GKDYVideo](https://github.com/QuintGao/GKDYVideo)|iOS仿抖音短视频|448|Objective-C|2021/05/02|
|88|[lincf0912/LFMediaEditingController](https://github.com/lincf0912/LFMediaEditingController)|Media Editor (图片编辑、视频编辑)|448|Objective-C|2021/10/15|
|89|[MobClub/ShareSDK-for-iOS](https://github.com/MobClub/ShareSDK-for-iOS)|快捷好用的社会化分享组件 Convenient SDK for SNS Share Feature|442|Objective-C|2021/08/23|
|90|[yangyangFeng/TTPatch](https://github.com/yangyangFeng/TTPatch)|热修复、热更新、JS代码动态下发、动态创建类|438|Objective-C|2021/11/09|
|91|[04zhujunjie/ZJJTimeCountDown](https://github.com/04zhujunjie/ZJJTimeCountDown)|iOS倒计时 验证码倒计时 秒杀倒计时 支持cell中的多个倒计时 支持自定义 样式多 支持时间差设置|438|Objective-C|2021/11/04|
|92|[LGCooci/KCiOSGrocery](https://github.com/LGCooci/KCiOSGrocery)|iOS开发杂货铺: 面试题+考试试卷+懒人开发tips|426|Objective-C|2021/10/10|
|93|[pili-engineering/PLShortVideoKit](https://github.com/pili-engineering/PLShortVideoKit)|PLShortVideoKit 是七牛推出的一款适用于 iOS 平台的短视频 SDK，提供了包括美颜、滤镜、水印、断点录制、分段回删、视频编辑、混音特效、视频剪辑、本地转码、视频上传在内的多种功能，支持高度定制以及二次开发。|423|Objective-C|2021/06/28|
|94|[liangdahong/UITableViewDynamicLayoutCacheHeight](https://github.com/liangdahong/UITableViewDynamicLayoutCacheHeight)|🖖高性能的自动计算采用 Autolayout 布局的 UITableViewCell 和 UITableViewHeaderFooterView 的高度，内部自动管理高度缓存。|391|Objective-C|2021/08/08|
|95|[donggelaile/HDCollectionView](https://github.com/donggelaile/HDCollectionView)|An efficient and flexible listView (data driven). Based on Flexbox, it supports floating, waterfall, decorative view, horizontal sliding, segmented layout, and various alignments. Support diff refresh, animation update UI  /  数据驱动(data driven)的高效灵活列表。基于Flexbox，支持 悬浮、瀑布流、装饰view、横向滑动、分段布局、各种对齐方式。支持链式语 ...|369|Objective-C|2021/10/27|
|96|[sealtalk/sealtalk-ios](https://github.com/sealtalk/sealtalk-ios)|iOS App of SealTalk powered by RongCloud. 基于融云开发的 iOS 版即时通讯（IM）应用程序 - 嗨豹。|365|Objective-C|2021/10/09|
|97|[BinBear/breadtrip-ReactiveCocoa-MVVM-](https://github.com/BinBear/breadtrip-ReactiveCocoa-MVVM-)|仿面包旅行，ReactiveCocoa+MVVM|353|Objective-C|2021/08/26|
|98|[zhu410289616/RHSocketKit](https://github.com/zhu410289616/RHSocketKit)|socket网络通信框架 － qq: 410289616  －  qq群2: 371293816  qq群3: 231199626|348|Objective-C|2021/06/27|
|99|[wjd-jax/WJDStudyLibrary](https://github.com/wjd-jax/WJDStudyLibrary)|这是一个大工程,里边包含了项目中常用模块的封装,我的简书地址:|340|Objective-C|2021/08/23|
|100|[Jonhory/LiveSendGift](https://github.com/Jonhory/LiveSendGift)|直播发送弹幕效果|339|Objective-C|2021/07/12|
|101|[vvusu/LNRefresh](https://github.com/vvusu/LNRefresh)|轻量级下拉刷新控件,各种动画集合（京东，天猫，淘宝，考拉海购，美团，今日头条，飞猪）|336|Objective-C|2021/06/16|
|102|[mengxianliang/XLPageViewController](https://github.com/mengxianliang/XLPageViewController)|一个开放、高度可定制化的分页视图控制器|335|Objective-C|2021/10/04|
|103|[FantasticLBP/Hotels](https://github.com/FantasticLBP/Hotels)|酒店预订App|331|Objective-C|2021/09/30|
|104|[yuping1989/YPTabBarController](https://github.com/yuping1989/YPTabBarController)|功能强大，使用方便，可高度自定义的TabBarController。|331|Objective-C|2021/10/09|
|105|[LuKane/KNPhotoBrowser](https://github.com/LuKane/KNPhotoBrowser)|图片浏览器(本地和网络) ,视频浏览器 (本地和网络), 无耦合性,自定义控件,资源路径保存和获取,  完美适配 iPhone 以及 iPad ,屏幕旋转功能.|329|Objective-C|2021/11/17|
|106|[xrtc-cc/xrtc](https://github.com/xrtc-cc/xrtc)|WebRTC 融合音视频解决方案：封装声网Agora、华为云hrtc、腾讯云trtc及网易云信rtc，支持动态切换和定制，iOS、Android、Web极简集成WebRTC。|325|Objective-C|2021/11/05|
|107|[huangxuan518/HXTagsView](https://github.com/huangxuan518/HXTagsView)|HXTagsView是一款支持自动布局的标签tag 演示地址:https://appetize.io/app/f9a5kn2tnfe0kade2zy7g2mja|315|Objective-C|2021/09/10|
|108|[xuuhan/HXCharts](https://github.com/xuuhan/HXCharts)|📊 Chart for iOS 仪表盘、柱状图、圆形图、折线图、环形图|306|Objective-C|2021/07/26|
|109|[JmoVxia/CLPlayer](https://github.com/JmoVxia/CLPlayer)|自定义视频播放器|302|Objective-C|2021/10/29|
|110|[jiaxiaogang/he4o](https://github.com/jiaxiaogang/he4o)|HE —— “信息熵减机系统”|297|Objective-C|2021/12/01|
|111|[SmileZXLee/ZXNavigationBar](https://github.com/SmileZXLee/ZXNavigationBar)|灵活轻量的自定义导航栏，导航栏属于控制器view，支持导航栏联动，一行代码实现【导航栏背景图片设置、导航栏渐变、折叠、修改Item大小和边距、自定义导航栏高度、全屏手势返回、pop拦截、仿系统导航栏历史堆栈】等各种效果|286|Objective-C|2021/10/14|
|112|[MJCIOS/MJCSegmentInterface](https://github.com/MJCIOS/MJCSegmentInterface)|一款类似百思不得姐,今日头条,腾讯新闻等app的分段界面功能的,分段界面框架|285|Objective-C|2021/07/30|
|113|[puti94/react-native-puti-pay](https://github.com/puti94/react-native-puti-pay)|基于 React Native 的微信支付，支付宝支付插件|281|Objective-C|2021/09/14|
|114|[tangtiancheng/DouYinComment](https://github.com/tangtiancheng/DouYinComment)|1.抖音视频转场动画, 评论手势拖拽效果 , 视频播放, 边下边播, 预加载, TikTok  2.铃声多多,上传铃声音频到库乐队(GarageBand) 3.微博主页、简书主页等。多页面嵌套列表分页滚动，上下滑动，左右滑动切换页面。类似TableView共用HeaderView悬浮,仿头条标签编辑,铃声多多音乐播放界面(豆瓣电影主页)。|272|Objective-C|2021/11/29|
|115|[changsanjiang/SJFullscreenPopGesture](https://github.com/changsanjiang/SJFullscreenPopGesture)|Fullscreen pop gesture. OC&Swift. It is very suitable for the application of the video player. Support `cocoapods`.  只需`pod`即可自带全屏返回手势. 支持pod. 支持OC&Swift.|272|Objective-C|2021/08/05|
|116|[ripperhe/Debugo](https://github.com/ripperhe/Debugo)|一个可能有点用的 iOS 调试工具~|271|Objective-C|2021/06/04|
|117|[Faceunity/FULiveDemo](https://github.com/Faceunity/FULiveDemo)|Faceunity 面部跟踪和虚拟道具 SDK 在 iOS 平台中的集成 Demo|261|Objective-C|2021/11/18|
|118|[baozoudiudiu/CWCarousel](https://github.com/baozoudiudiu/CWCarousel)|轮播图banner|253|Objective-C|2021/10/28|
|119|[yangKJ/KJBannerViewDemo](https://github.com/yangKJ/KJBannerViewDemo)|🏂 🏂 🏂 轮播图无限自动循环滚动、缩放布局、缓存预加载读取、支持自定义继承，网络GIF和网图混合轮播，支持Storyboard和Xib中创建并配置属性，多种分页控件选择，自动清理缓存等等|253|Objective-C|2021/11/27|
|120|[LINGLemon/LXFAVFoundation](https://github.com/LINGLemon/LXFAVFoundation)|2021已更新最新iOS API。这是一个模仿iOS微信拍照，录像，保存照片或视频到自己本地自定义app相册里面的demo，具有快速简易接入项目的接口。并通过 AVAssetWriter 实现高分辨率录制视频，生成低体积的视频文件|247|Objective-C|2021/05/11|
|121|[QuintGao/GKNavigationBar](https://github.com/QuintGao/GKNavigationBar)|GKNavigationBarViewController的分类实现方式，耦合度底，使用更加便捷|243|Objective-C|2021/11/08|
|122|[Tate-zwt/WTSDK](https://github.com/Tate-zwt/WTSDK)|开发项目积累的一些category、tools、自定义View，持续更新|239|Objective-C|2021/10/19|
|123|[mengxianliang/XLPlayButton](https://github.com/mengxianliang/XLPlayButton)|爱奇艺、优酷的播放/暂停按钮动画效果|230|Objective-C|2021/11/30|
|124|[lincf0912/LFImagePickerController](https://github.com/lincf0912/LFImagePickerController)|一个支持多选图片和视频的图片选择器，同时有预览、编辑功能|220|Objective-C|2021/06/24|
|125|[finogeeks/finclip-ios-demo](https://github.com/finogeeks/finclip-ios-demo)|FinClip 苹果运行环境，让小程序在苹果应用中无缝运行  / iOS DEMO for FinClip|220|Objective-C|2021/11/14|
|126|[shixueqian/PrintBeautifulLog](https://github.com/shixueqian/PrintBeautifulLog)|将字典(NSDictionary)和数组(NSArray)打印的Log显示为Json格式。直接将分类拖入到工程即可使用。|217|Objective-C|2021/09/22|
|127|[Suzhibin/ZBNetworking](https://github.com/Suzhibin/ZBNetworking)| AFNetworking4.X封装  GET/POST /PUT/PATCH /DELETE / UPLOAD /DOWNLOAD 网络请求 添加了缓存机制,插件机制,断点下载,重复请求的处理,取消当前请求等功能|208|Objective-C|2021/11/30|
|128|[Zws-China/startMovie](https://github.com/Zws-China/startMovie)|启动视频  APP第一次启动播放视频欢迎  启动页|202|Objective-C|2021/10/08|
|129|[GPPG/GPHandMade](https://github.com/GPPG/GPHandMade)|初始化|193|Objective-C|2021/07/18|
|130|[QuintGao/GKCover](https://github.com/QuintGao/GKCover)|GKCover-一行代码实现遮罩视图，让你的弹窗更easy|185|Objective-C|2021/09/01|
|131|[maltsugar/CustomPopoverView](https://github.com/maltsugar/CustomPopoverView)|一款小巧灵活的自定义弹出视图, 可以做自定义AlertView、弹出窗口等等, A tiny and sweet custom popView (pop popup)|180|Objective-C|2021/11/30|
|132|[SmileZXLee/OpenDingTalkHelperForiOS](https://github.com/SmileZXLee/OpenDingTalkHelperForiOS)|iOS钉钉定时打卡助手【需要把备用机放在公司，完美避开钉钉虚拟定位检测，截止2021年10月依然可用】|177|Objective-C|2021/05/26|
|133|[FoneG/FGPopupScheduler](https://github.com/FoneG/FGPopupScheduler)| ✨ ✨ ✨ ✨ iOS弹窗调用器，控制弹窗按照指定的策略进行显示。Helps you control popups easily.  ✨ ✨ ✨ ✨|174|Objective-C|2021/11/08|
|134|[mengxianliang/XLImageViewer](https://github.com/mengxianliang/XLImageViewer)|iOS 仿照今日头条的图片浏览工具。|162|Objective-C|2021/09/08|
|135|[leancloud/leancloud-social-ios](https://github.com/leancloud/leancloud-social-ios)|LeanCloud 社交登录组件，轻松加入 QQ、微博、微信登录|161|Objective-C|2021/06/09|
|136|[leancloud/LeanStorageDemo-iOS](https://github.com/leancloud/LeanStorageDemo-iOS)|基于 iOS SDK，全面示例了 LeanCloud 的存储功能。|158|Objective-C|2021/06/23|
|137|[h-js/KLine](https://github.com/h-js/KLine)|高仿火币K线图 实现OC Swift Flutter 三个版本|155|Objective-C|2021/05/05|
|138|[Resory/RYCuteView](https://github.com/Resory/RYCuteView)|🍮 用UIBezierPath实现果冻效果|153|Objective-C|2021/09/17|
|139|[react-native-studio/react-native-MJRefresh](https://github.com/react-native-studio/react-native-MJRefresh)|基于ios MJRefresh https://github.com/CoderMJLee/MJRefresh 开发的插件，可提供自定义的弹性刷新|147|Objective-C|2021/09/21|
|140|[wayone/Indicator](https://github.com/wayone/Indicator)|仪表盘 / 指示器 / 温度计 / 湿度计 / Indicator /  Dashboard|146|Objective-C|2021/06/30|
|141|[Soldoros/SSChat](https://github.com/Soldoros/SSChat)|社交网络|145|Objective-C|2021/10/04|
|142|[remember17/WHDebugTool](https://github.com/remember17/WHDebugTool)|🔨简单的调试小工具|144|Objective-C|2021/09/03|
|143|[GetuiLaboratory/react-native-getui](https://github.com/GetuiLaboratory/react-native-getui)|个推官方提供的推送SDK React Native 插件（支持 Android & iOS）|143|Objective-C|2021/10/08|
|144|[Hurdery/jfc](https://github.com/Hurdery/jfc)|Mac端基金管理查看平台|142|Objective-C|2021/09/06|
|145|[changsanjiang/SJMediaCacheServer](https://github.com/changsanjiang/SJMediaCacheServer)|A HTTP Media Caching Framework. It can cache FILE or HLS media. 音视频边播边缓存框架, 支持 HLS(m3u8) 和 FILE(mp4, mp3等).|141|Objective-C|2021/11/23|
|146|[tencentyun/httpdns-ios-sdk](https://github.com/tencentyun/httpdns-ios-sdk)|智营防劫持SDK|140|Objective-C|2021/08/13|
|147|[wsl2ls/UIActivityViewController](https://github.com/wsl2ls/UIActivityViewController)|仿简书分享--系统原生分享:利用UIActivityViewController实现系统原生分享，不需要三方SDK，支持自定义分享，可以分享到微博、微信、QQ、信息、邮件、备忘录、通讯录、剪贴板、FaceBook.....等等|140|Objective-C|2021/10/07|
|148|[DmcSDK/cordova-plugin-mediaPicker](https://github.com/DmcSDK/cordova-plugin-mediaPicker)|cordova android ios mediaPicker support  selection of multiple image and video gif ✨  cordova android 和 ios  图片视频选择cordova插件，支持多图 视频 gif，ui类似微信|138|Objective-C|2021/07/06|
|149|[debugly/FFmpegTutorial](https://github.com/debugly/FFmpegTutorial)|MoviePlayer which based on FFmpeg step by step tutorials for iOS/macOS developer. （使用 FFMpeg 封装播放器系列教程，适合零基础的 iOS/macOS 开发者学习）|138|Objective-C|2021/11/29|
|150|[hackxhj/EllipsePageControl](https://github.com/hackxhj/EllipsePageControl)|椭圆形 长方形 PageControl 轮播图点|136|Objective-C|2021/09/02|
|151|[NervJS/taro-native-shell](https://github.com/NervJS/taro-native-shell)|Taro 原生 React Native 壳子|129|Objective-C|2021/11/25|
|152|[kevindcw/DNebula](https://github.com/kevindcw/DNebula)|模仿SOUL布局，写的一个笔记类应用|129|Objective-C|2021/10/08|
|153|[RITL/RITLPhotos](https://github.com/RITL/RITLPhotos)|一个基于Photos.framework的图片多选，模仿微信，还有很多不足，正在改进和优化|127|Objective-C|2021/09/07|
|154|[hydreamit/HyCycleView](https://github.com/hydreamit/HyCycleView)|多页面分页、循环、嵌套滑动悬停|125|Objective-C|2021/11/30|
|155|[rabbitmouse/ZQSearchController](https://github.com/rabbitmouse/ZQSearchController)|Search，SearchBar,  仿《饿了么》搜索栏。自定义搜索结果界面。搜索主页、模糊匹配、结果界面之间的状态切换。支持搜索历史和热门设置|124|Objective-C|2021/06/02|
|156|[ZZZZou/AwemeLike](https://github.com/ZZZZou/AwemeLike)|仿照抖音的特效相机，实现美颜、2D贴图、分屏、转场等|123|Objective-C|2021/06/26|
|157|[muzipiao/OOB](https://github.com/muzipiao/OOB)|基于 OpenCV，使用模板匹配法识别图像，供参考学习。|123|Objective-C|2021/11/08|
|158|[iotjin/JhForm](https://github.com/iotjin/JhForm)|JhForm - 自定义表单工具,更加简单,快捷的创建表单、设置页面|122|Objective-C|2021/11/26|
|159|[kingjiajie/JJCollectionViewRoundFlowLayout](https://github.com/kingjiajie/JJCollectionViewRoundFlowLayout)|JJCollectionViewRoundFlowLayout可设置CollectionView的BackgroundColor、Cell的对齐方式，可跟据用户Cell个数计算背景图尺寸，可自定义是否包括计算CollectionViewHeaderView、CollectionViewFootererView或只计算Cells。设置简单，可自定义背景颜色偏移，设置显示方向（竖向、横向）显示,不同Section设置不同的背景颜色，设置Cell的对齐方式，支持左对齐，右对齐，居中。|120|Objective-C|2021/09/12|
|160|[jiaxiaogang/HELIX_THEORY](https://github.com/jiaxiaogang/HELIX_THEORY)|theory of helix—— “熵减机理论（可用来构建信息熵减机、及自然演化熵减机系统等）”|119|Objective-C|2021/11/30|
|161|[fanbaoying/FBYBankCardRecognition-iOS](https://github.com/fanbaoying/FBYBankCardRecognition-iOS)|可快速高效对银行卡进行识别，可识别卡号是平的和凹凸的银行卡，烦请点个Star！|118|Objective-C|2021/09/03|
|162|[fanyuecheng/Pica](https://github.com/fanyuecheng/Pica)|一个简约的哔咔漫画iOS App|116|Objective-C|2021/11/30|
|163|[zhiyongzou/DynamicOC](https://github.com/zhiyongzou/DynamicOC)|深入理解 iOS 热修复原理|114|Objective-C|2021/06/13|
|164|[DevDragonLi/ProtocolServiceKit](https://github.com/DevDragonLi/ProtocolServiceKit)|iOS组件通信中间件(Protocol Service),Adapter Swift/Objective-C|114|Objective-C|2021/08/16|
|165|[Meiqia/MeiqiaSDK-iOS](https://github.com/Meiqia/MeiqiaSDK-iOS)|美洽 iOS SDK 3.7.8 面向开发者的 Demo。|113|Objective-C|2021/11/24|
|166|[umeng/MultiFunctioniOSDemo](https://github.com/umeng/MultiFunctioniOSDemo)|友盟多功能iOS Demo|112|Objective-C|2021/09/26|
|167|[internetWei/LLDynamicLaunchScreen](https://github.com/internetWei/LLDynamicLaunchScreen)|自动修复iPhone启动图显示异常，1行代码动态修改启动图|112|Objective-C|2021/07/04|
|168|[QuintGao/GKCycleScrollView](https://github.com/QuintGao/GKCycleScrollView)|一个轻量级的自定义轮播图组件|110|Objective-C|2021/06/01|
|169|[FighterLightning/ZHFJDAddressOC](https://github.com/FighterLightning/ZHFJDAddressOC)|OC 仿京东地址选择器，京东地址选择器（网络，本地数据。三级、四级、保证成功集成）|109|Objective-C|2021/05/17|
|170|[fanbaoying/FBYIDCardRecognition-iOS](https://github.com/fanbaoying/FBYIDCardRecognition-iOS)|可快速高效对身份证进行识别，可识别头像面(姓名、性别、民族、住址、身份证号码) 和 国徽面(签发机关、有效期)，烦请点个Star！|109|Objective-C|2021/09/03|
|171|[muzipiao/LFPhoneInfo](https://github.com/muzipiao/LFPhoneInfo)|iOS 快速获取硬件信息。|105|Objective-C|2021/10/11|
|172|[tianya2416/GKiOSApp](https://github.com/tianya2416/GKiOSApp)|项目，新闻、视频、壁纸。主要功能有：开机启动图、图片壁纸、新闻（仿头条新闻、搜索框）、视频、短视频、视频播放|105|Objective-C|2021/11/28|
|173|[uiwjs/react-native-alipay](https://github.com/uiwjs/react-native-alipay)|基于 React Native 的宝支付包，已更新到最新的支付宝 SDK 版本，支持Android/iOS。|102|Objective-C|2021/10/19|
|174|[Maxwin-z/xsmth-newsmth](https://github.com/Maxwin-z/xsmth-newsmth)|水木社区第三方客户端|101|Objective-C|2021/10/06|
|175|[mengxianliang/XLCircleProgress](https://github.com/mengxianliang/XLCircleProgress)|iOS 圆环进度指示器 |99|Objective-C|2021/11/30|
|176|[PaddlePaddle/LiteKit](https://github.com/PaddlePaddle/LiteKit)|Off-The-Shelf AI Development Kit for APP Developers based on Paddle Lite （『飞桨』移动端开箱即用AI套件, 包含Java & Objective C接口支持）|99|Objective-C|2021/06/25|
|177|[yangKJ/KJPlayerDemo](https://github.com/yangKJ/KJPlayerDemo)|🎷🎷🎷 音视频播放壳子：动态切换内核，来回拖动缓存播放，断点续载续播，记忆播放，视频支持试看预加载、无缝衔接、跳过片头片尾广告、加密流媒体m3u8下载播放，横竖屏自动连续播放，MIDI内核等功能|98|Objective-C|2021/11/25|
|178|[tianya2416/GKiOSNovel](https://github.com/tianya2416/GKiOSNovel)|Swift、小说、小说阅读、读书神器、自定义首页、千万部小说任你选择、数据缓存|96|Objective-C|2021/11/28|
|179|[Rain-dew/YLTextView](https://github.com/Rain-dew/YLTextView)|一行代码搞定textView占位符、字数限制、行数限制、自动计算高度— OC和Swift|92|Objective-C|2021/09/13|
|180|[Zws-China/CAAnimation](https://github.com/Zws-China/CAAnimation)|iOS动画演示，核心动画演示，CAAnimation动画|90|Objective-C|2021/09/24|
|181|[remember17/WHToast](https://github.com/remember17/WHToast)|🍞iOS提示框|89|Objective-C|2021/09/02|
|182|[bytesking/YJCategories](https://github.com/bytesking/YJCategories)|Objective-C 常用分类集合，支持Cocoapods|88|Objective-C|2021/06/15|
|183|[banchichen/TZImagePreviewController](https://github.com/banchichen/TZImagePreviewController)|对TZImagePickerController库的增强，支持用UIImage、NSURL预览照片和用NSURL预览视频。|88|Objective-C|2021/11/27|
|184|[X140Yu/WEWTweak](https://github.com/X140Yu/WEWTweak)|🥳 企业微信 Mac Tweak|87|Objective-C|2021/06/26|
|185|[BAHome/BAGridView](https://github.com/BAHome/BAGridView)|支付宝首页 九宫格 布局封装，可以自定义多种样式，自定义角标、分割线显示/隐藏、颜色等功能应有尽有！|84|Objective-C|2021/07/15|
|186|[rongcloud/callkit-ios](https://github.com/rongcloud/callkit-ios)|Open-source code of RongCloud VoIP Audio/Video UI. 融云音视频通话功能 UI 界面 SDK 开源代码。|84|Objective-C|2021/11/26|
|187|[kevindcw/DProgram_ios](https://github.com/kevindcw/DProgram_ios)|做的一款小项目-编程宝典iOS版|83|Objective-C|2021/10/08|
|188|[jpush/jverify-flutter-plugin](https://github.com/jpush/jverify-flutter-plugin)|JPush's officially supported Flutter plugin (Android & iOS). 极光推送官方支持的 Flutter 插件（Android & iOS）。|83|Objective-C|2021/11/05|
|189|[KeenTeam1990/HXWeiboPhotoPicker](https://github.com/KeenTeam1990/HXWeiboPhotoPicker)|🐱HXWeiboPhotoPicker-高仿微博照片选择，使用的是PhotoKit框架来获取系统相册以及照片/视频，支持查看/选择GIF图片 ， 照片、视频可同时多选/原图 ， 3DTouch预览照片 ， 长按拖动改变顺序 ， 自定义相机拍照/录制视频 ， 自定义转场动画 ， 查看/选择LivePhoto IOS9.1以上才有用 ， 支持浏览网络图片 ， 支持自定义裁剪图片， 支持传入本地图片，支持在线下载iCloud上的资源|83|Objective-C|2021/11/11|
|190|[Andrew5/DHPRO](https://github.com/Andrew5/DHPRO)|二维码,画圆角, tableView列表联动,收缩列表,图片放大,FMDB数据库,水波动画,仪表盘,自适应列表,绘画板,转盘,长按移动,PhotoClip,循环滑动ScrollView,UICollectionView卡片动画,图片找不同,导航栏程序框架|82|Objective-C|2021/08/08|
|191|[qinqi777/QQCorner](https://github.com/qinqi777/QQCorner)|给 UIView 或 UIImage 高性能添加(之后可以修改)圆角，生成渐变色图片等|82|Objective-C|2021/11/18|
|192|[HighwayLaw/HWThrottle](https://github.com/HighwayLaw/HWThrottle)|A lite Objective-C library for throttle and debounce, supporting leading and trailing. 节流/限流/防反跳/防重复点击/防重复调用|82|Objective-C|2021/11/19|
|193|[lionvoom/WeAppTongCeng](https://github.com/lionvoom/WeAppTongCeng)|小程序同层渲染|80|Objective-C|2021/10/29|
|194|[Jeepeng/react-native-xinge-push](https://github.com/Jeepeng/react-native-xinge-push)|信鸽推送React Native版，支持华为、小米、魅族官方推送通道|80|Objective-C|2021/08/11|
|195|[liangdahong/AMLeaksFinder](https://github.com/liangdahong/AMLeaksFinder)|A small tool for automatically detecting the [controller, view memory leak] in the project.  一款用于自动检测项目中【控制器内存泄漏，View 内存泄漏】的小工具，支持 ObjC，Swift。|79|Objective-C|2021/10/25|
|196|[kopuCoder/iOS_Development-Book](https://github.com/kopuCoder/iOS_Development-Book)|适合iOS中高级开发工程狮提升个人能力业务水平的书籍|79|Objective-C|2021/11/01|
|197|[tencentyun/UGSVSDK](https://github.com/tencentyun/UGSVSDK)|短视频 SDK，国内下载镜像：|79|Objective-C|2021/11/05|
|198|[MinMao-Hub/MMScan](https://github.com/MinMao-Hub/MMScan)|QRCode and barcode scanning tool【轻量级的二维码以及条码扫描-> 使用iOS自带API开发】|77|Objective-C|2021/10/29|
|199|[Xiexingda/XDPagesView](https://github.com/Xiexingda/XDPagesView)|XDPagesView 2.0 （可添加header的多控制器列表，强大而优雅）|76|Objective-C|2021/07/28|
|200|[kevindcw/DAudiobook](https://github.com/kevindcw/DAudiobook)|模仿QQ布局写的一个小项目--小熊有声小说|73|Objective-C|2021/10/08|

⬆ [回到目录](#内容目录)

<br/>

## C

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[julycoding/The-Art-Of-Programming-By-July](https://github.com/julycoding/The-Art-Of-Programming-By-July)|本项目曾冲到全球第一，干货集锦见本页面最底部，另完整精致的纸质版《编程之法：面试和算法心得》已在京东/当当上销售|20.2k|C|2021/07/03|
|2|[Awesome-HarmonyOS/HarmonyOS](https://github.com/Awesome-HarmonyOS/HarmonyOS)|A curated list of awesome things related to HarmonyOS. 华为鸿蒙操作系统。|18.4k|C|2021/06/17|
|3|[EZLippi/Tinyhttpd](https://github.com/EZLippi/Tinyhttpd)|Tinyhttpd 是J. David Blackstone在1999年写的一个不到 500 行的超轻量型 Http Server，用来学习非常不错，可以帮助我们真正理解服务器程序的本质。官网:http://tinyhttpd.sourceforge.net|7.7k|C|2021/10/08|
|4|[nonstriater/Learn-Algorithms](https://github.com/nonstriater/Learn-Algorithms)|算法学习笔记|6.1k|C|2021/11/30|
|5|[SecWiki/windows-kernel-exploits](https://github.com/SecWiki/windows-kernel-exploits)|windows-kernel-exploits   Windows平台提权漏洞集合|5.9k|C|2021/06/12|
|6|[sparklemotion/nokogiri](https://github.com/sparklemotion/nokogiri)|Nokogiri (鋸) makes it easy and painless to work with XML and HTML from Ruby.|5.7k|C|2021/11/29|
|7|[miloyip/json-tutorial](https://github.com/miloyip/json-tutorial)|从零开始的 JSON 库教程|5.5k|C|2021/10/21|
|8|[OpenAtomFoundation/TencentOS-tiny](https://github.com/OpenAtomFoundation/TencentOS-tiny)|腾讯物联网终端操作系统|5.3k|C|2021/11/30|
|9|[peng-zhihui/Dummy-Robot](https://github.com/peng-zhihui/Dummy-Robot)|我的超迷你机械臂机器人项目。|5.2k|C|2021/11/30|
|10|[pymumu/smartdns](https://github.com/pymumu/smartdns)|A local DNS server to obtain the fastest website IP for the best Internet experience， 一个本地DNS服务器，获取最快的网站IP，获得最佳上网体验。|4.3k|C|2021/11/26|
|11|[ming1016/study](https://github.com/ming1016/study)|学习记录|3.7k|C|2021/11/25|
|12|[y123456yz/reading-code-of-nginx-1.9.2](https://github.com/y123456yz/reading-code-of-nginx-1.9.2)|nginx-1.9.2源码通读分析注释，带详尽函数中文分析注释以及相关函数流程调用注释，最全面的nginx源码阅读分析中文注释，更新完毕|3.4k|C|2021/07/26|
|13|[microshow/RxFFmpeg](https://github.com/microshow/RxFFmpeg)|🔥💥RxFFmpeg 是基于 ( FFmpeg 4.0 + X264 + mp3lame + fdk-aac + opencore-amr + openssl ) 编译的适用于 Android 平台的音视频编辑、视频剪辑的快速处理框架，包含以下功能：视频拼接，转码，压缩，裁剪，片头片尾，分离音视频，变速，添加静态贴纸和gif动态贴纸，添加字幕，添加滤镜，添加背景音乐，加速减速视频，倒放音视频，音频裁剪，变声，混音，图片合成视频，视频解码图片，抖音首页，视频播放器及支持 OpenSSL https 等主流特色功能|3.3k|C|2021/08/03|
|14|[ithewei/libhv](https://github.com/ithewei/libhv)|🔥 比libevent、libuv更易用的国产网络库。A c/c++ network library for developing TCP/UDP/SSL/HTTP/WebSocket client/server.|3.3k|C|2021/11/26|
|15|[peng-zhihui/HoloCubic](https://github.com/peng-zhihui/HoloCubic)|带网络功能的伪全息透明显示桌面站|3.0k|C|2021/10/16|
|16|[badafans/better-cloudflare-ip](https://github.com/badafans/better-cloudflare-ip)|查找适合自己当前网络环境的优选cloudflare anycast IP|2.8k|C|2021/11/11|
|17|[xufuji456/FFmpegAndroid](https://github.com/xufuji456/FFmpegAndroid)|android端基于FFmpeg实现音频剪切、拼接、转码、编解码；视频剪切、水印、截图、转码、编解码、转Gif动图；音视频合成与分离，配音；音视频解码、同步与播放；FFmpeg本地推流、H264与RTMP实时推流直播；FFmpeg滤镜：素描、色彩平衡、hue、lut、模糊、九宫格等；歌词解析与显示|2.8k|C|2021/12/01|
|18|[firmianay/CTF-All-In-One](https://github.com/firmianay/CTF-All-In-One)|CTF竞赛权威指南|2.8k|C|2021/11/24|
|19|[feiyangqingyun/QWidgetDemo](https://github.com/feiyangqingyun/QWidgetDemo)|Qt编写的一些开源的demo，预计会有100多个，一直持续更新完善，代码简洁易懂注释详细，每个都是独立项目，非常适合初学者，代码随意传播使用，拒绝打赏和捐赠，欢迎留言评论！|2.7k|C|2021/11/28|
|20|[guanzhi/GmSSL](https://github.com/guanzhi/GmSSL)|支持国密SM2/SM3/SM4/SM9/ZUC/SSL的OpenSSL分支|2.7k|C|2021/11/19|
|21|[Mzzopublic/C](https://github.com/Mzzopublic/C)|C语言|2.7k|C|2021/10/15|
|22|[sunym1993/flash-linux0.11-talk](https://github.com/sunym1993/flash-linux0.11-talk)|你管这破玩意叫操作系统源码 — 像小说一样品读 Linux 0.11 核心代码|2.5k|C|2021/11/29|
|23|[WizTeam/WizQTClient](https://github.com/WizTeam/WizQTClient)|为知笔记跨平台客户端|2.4k|C|2021/05/26|
|24|[zlgopen/awtk](https://github.com/zlgopen/awtk)|AWTK = Toolkit AnyWhere(为嵌入式、手机和PC打造的通用GUI系统)|2.3k|C|2021/12/01|
|25|[snooda/net-speeder](https://github.com/snooda/net-speeder)|net-speeder 在高延迟不稳定链路上优化单线程下载速度 |2.2k|C|2021/11/10|
|26|[EZLippi/WebBench](https://github.com/EZLippi/WebBench)|Webbench是Radim Kolar在1997年写的一个在linux下使用的非常简单的网站压测工具。它使用fork()模拟多个客户端同时访问我们设定的URL，测试网站在压力下工作的性能，最多可以模拟3万个并发连接去测试网站的负载能力。官网地址:http://home.tiscali.cz/~cz210552/webbench.html|2.1k|C|2021/06/19|
|27|[zuoqing1988/ZQCNN](https://github.com/zuoqing1988/ZQCNN)|一款比mini-caffe更快的Forward库，觉得好用请点星啊，400星公布快速人脸检测模型，500星公布106点landmark，600星公布人头检测模型，700星公布人脸检测套餐（六种pnet,两种rnet随意混合使用满足各种速度/精度要求），800星公布更准的106点模型|2.1k|C|2021/12/01|
|28|[Tencent/puerts](https://github.com/Tencent/puerts)|Write your game with TypeScript in UE4 or Unity. Puerts can be pronounced as pu-erh TS（普洱TS）|2.1k|C|2021/11/29|
|29|[yangjie10930/EpMedia](https://github.com/yangjie10930/EpMedia)|Android上基于FFmpeg开发的视频处理框架，简单易用，体积小，帮助使用者快速实现视频处理功能。包含以下功能：剪辑，裁剪，旋转，镜像，合并，分离，变速，添加LOGO，添加滤镜，添加背景音乐，加速减速视频，倒放音视频。 The video processing framework based on FFmpeg developed on Android is simple, easy to use, and small in size, helping users quickly realize video processing functions. Contains the follow ...|1.9k|C|2021/08/09|
|30|[armink/EasyLogger](https://github.com/armink/EasyLogger)|An ultra-lightweight(ROM<1.6K, RAM<0.3k), high-performance C/C++ log library.   一款超轻量级(ROM<1.6K, RAM<0.3k)、高性能的 C/C++ 日志库|1.9k|C|2021/11/26|
|31|[monyhar/monyhar-lite](https://github.com/monyhar/monyhar-lite)|梦弘浏览器 自主研发版本 - 完全自主研发，打破国外垄断，比 Chrome 快 600%。缺少上网功能。|1.8k|C|2021/08/19|
|32|[Ascotbe/Kernelhub](https://github.com/Ascotbe/Kernelhub)|:palm_tree:Windows Kernel privilege escalation vulnerability collection, with compilation environment, demo GIF map, vulnerability details, executable file  (Windows提权漏洞合集) |1.6k|C|2021/11/27|
|33|[cppla/ServerStatus](https://github.com/cppla/ServerStatus)|云探针、多服务器探针、云监控、多服务器云监控，演示： https://tz.cloudcpp.com/|1.6k|C|2021/11/24|
|34|[nining377/dolby_beta](https://github.com/nining377/dolby_beta)|杜比大喇叭的β版迎来了重大的革新，合并了UnblockMusic Pro的所有功能且更加强大，同时UnblockMusicPro_Xposed项目将会停止维护，让我们欢送这位老朋友！|1.5k|C|2021/11/22|
|35|[momotech/MLN](https://github.com/momotech/MLN)|高性能、小巧、易上手的移动跨平台开发框架. A framework for building Mobile cross-platform apps with Lua|1.5k|C|2021/11/01|
|36|[liexusong/php-beast](https://github.com/liexusong/php-beast)|PHP source code encrypt module (PHP源码加密扩展)|1.5k|C|2021/08/16|
|37|[armink/EasyFlash](https://github.com/armink/EasyFlash)|Lightweight IoT device information storage solution: KV/IAP/LOG.   轻量级物联网设备信息存储方案：参数存储、在线升级及日志存储 ，全新一代版本请移步至 https://github.com/armink/FlashDB|1.4k|C|2021/10/30|
|38|[cokemine/ServerStatus-Hotaru](https://github.com/cokemine/ServerStatus-Hotaru)|云探针、多服务器探针、云监控、多服务器云监控|1.4k|C|2021/10/25|
|39|[gatieme/LDD-LinuxDeviceDrivers](https://github.com/gatieme/LDD-LinuxDeviceDrivers)|Linux内核与设备驱动程序学习笔记|1.3k|C|2021/11/30|
|40|[ChenLittlePing/LearningVideo](https://github.com/ChenLittlePing/LearningVideo)|【Android 音视频开发打怪升级】系列文章示例代码（A demo to introduce how to develop android video）。本项目将从MediaCodec硬解，FFmpeg软解，OpenGL等方面，全方位讲解如何在Android上进行音视频编辑开发。|1.3k|C|2021/05/16|
|41|[yianwillis/vimcdoc](https://github.com/yianwillis/vimcdoc)|Vim 中文文档计划|1.2k|C|2021/08/23|
|42|[Tencent/TencentOS-kernel](https://github.com/Tencent/TencentOS-kernel)|腾讯针对云的场景研发的服务器操作系统|1.1k|C|2021/11/30|
|43|[guanshuicheng/invoice](https://github.com/guanshuicheng/invoice)|增值税发票OCR识别，使用flask微服务架构，识别type：增值税电子普通发票，增值税普通发票，增值税专用发票；识别字段为：发票代码、发票号码、开票日期、校验码、税后金额等|921|C|2021/08/25|
|44|[hurley25/hurlex-doc](https://github.com/hurley25/hurlex-doc)|hurlex 小内核分章节代码和文档|912|C|2021/08/09|
|45|[BruceWind/AESJniEncrypt](https://github.com/BruceWind/AESJniEncrypt)|Make safest code in Android. (基于libsodium实现chacha20算法,key在native中,防止被二次打包){长期维护,请star,勿fork}|903|C|2021/10/28|
|46|[chenyahui/AnnotatedCode](https://github.com/chenyahui/AnnotatedCode)|知名开源代码库的注释版：C++、Golang等|872|C|2021/10/22|
|47|[rock-app/fabu.love](https://github.com/rock-app/fabu.love)|应用发布平台类似fir.im/蒲公英,支持检查更新,灰度发布等等.Demo地址：https://fabu.apppills.com/|864|C|2021/09/03|
|48|[ADD-SP/ngx_waf](https://github.com/ADD-SP/ngx_waf)|Handy, High performance, ModSecurity compatible Nginx firewall module & 方便、高性能、兼容 ModSecurity 的 Nginx 防火墙模块|810|C|2021/11/30|
|49|[armink/SFUD](https://github.com/armink/SFUD)|An using JEDEC's SFDP standard serial (SPI) flash universal driver library   一款使用 JEDEC SFDP 标准的串行 (SPI) Flash 通用驱动库|723|C|2021/07/25|
|50|[huaiyukeji/verification_code](https://github.com/huaiyukeji/verification_code)|验证码研究破解心得记录。包含网易易盾，阿里云验证码，极验验证码，通用汉字识别，梦幻西游验证等主流验证码破解。包含点按验证码、点选验证、语序点选等等。已更新极验验证码、企业公示网/工商/文书采集系统、极验打码接口。|700|C|2021/11/17|
|51|[LGCooci/KCCbjc4_debug](https://github.com/LGCooci/KCCbjc4_debug)|🌈可编译苹果官方源码objc！现在有objc4底层源码,以及libmalloc等可编译版本，大家可以自由LLDB调试！|681|C|2021/09/26|
|52|[froghui/yolanda](https://github.com/froghui/yolanda)|极客时间<网络编程实战>代码|670|C|2021/10/12|
|53|[wuzhouhui/misc](https://github.com/wuzhouhui/misc)|学习与工作中收集的一些资料|651|C|2021/10/12|
|54|[RainbowRoad1/Cgame](https://github.com/RainbowRoad1/Cgame)|一些用C编写的小游戏, 14行贪吃蛇  22行2048  22行俄罗斯方块  25行扫雷...以及各种小玩意|622|C|2021/07/10|
|55|[FantasticLBP/knowledge-kit](https://github.com/FantasticLBP/knowledge-kit)|iOS、Web前端、后端、数据库、计算机网络、设计模式经验总结|615|C|2021/11/29|
|56|[wuxx/nanoDAP](https://github.com/wuxx/nanoDAP)|建议大家star此仓库，仓库会持续更新。由于部分淘宝卖家“借鉴”实验室出品的nanoDAP详情描述和资料，请大家认准实验室官方链接|609|C|2021/06/24|
|57|[tsingsee/EasyRTMP](https://github.com/tsingsee/EasyRTMP)|EasyRTMP是一套调用简单、功能完善、运行高效稳定的RTMP功能组件，经过多年实战和线上运行打造，支持RTMP推送断线重连、环形缓冲、智能丢帧、网络事件回调，支持Windows、Linux、arm（hisiv100/hisiv200/hisiv300/hisiv400/hisiv500/hisiv600/etc..）、Android、iOS平台，支持市面上绝大部分的RTMP流媒体服务器，包括Wowza、Red5、ngnix_rtmp、crtmpserver等主流RTMP服务器，能够完美应用于各种行业的直播需求，手机直播、桌面直播、摄像机直播、课堂直播等等方面！ Android版本地址：ht ...|581|C|2021/06/04|
|58|[Echocipher/AUTO-EARN](https://github.com/Echocipher/AUTO-EARN)|一个利用OneForAll进行子域收集、Shodan API端口扫描、Xray漏洞Fuzz、Server酱的自动化漏洞扫描、即时通知提醒的漏洞挖掘辅助工具|569|C|2021/06/02|
|59|[langhuihui/jessibuca](https://github.com/langhuihui/jessibuca)|Jessibuca是一款开源的纯H5直播流播放器|565|C|2021/12/01|
|60|[Lojii/Knot](https://github.com/Lojii/Knot)|一款iOS端基于MITM(中间人攻击技术)实现的HTTPS抓包工具，完整的App，核心代码使用SwiftNIO实现|560|C|2021/10/15|
|61|[aqi00/android2](https://github.com/aqi00/android2)|《Android Studio开发实战：从零基础到App上线》随书源码（全面添加注释版）|557|C|2021/09/14|
|62|[armink/FlashDB](https://github.com/armink/FlashDB)|An ultra-lightweight database that supports key-value and time series data    一款支持 KV 数据和时序数据的超轻量级数据库|549|C|2021/10/07|
|63|[timwhitez/Cobalt-Strike-Aggressor-Scripts](https://github.com/timwhitez/Cobalt-Strike-Aggressor-Scripts)|Cobalt Strike Aggressor 插件包|545|C|2021/08/31|
|64|[li-xiaojun/AndroidKTX](https://github.com/li-xiaojun/AndroidKTX)|🔥Some very useful kotlin extensions for speed android development！好用到爆的Kotlin扩展，加速你的Android开发！|525|C|2021/11/29|
|65|[byhook/ffmpeg4android](https://github.com/byhook/ffmpeg4android)|音视频学习实践，从移植ffmpeg库到android平台，混合编译x264库以及fdk-aac库，到相机音频采集编码推流到RTMP服务器的学习记录|520|C|2021/10/10|
|66|[u0u0/Quick-Cocos2dx-Community](https://github.com/u0u0/Quick-Cocos2dx-Community)|Cocos2d-Lua 社区版|512|C|2021/09/28|
|67|[y123456yz/Reading-and-comprehense-redis-cluster](https://github.com/y123456yz/Reading-and-comprehense-redis-cluster)|分布式NOSQL redis源码阅读中文分析注释，带详尽注释以及相关流程调用注释，提出改造点，redis cluster集群功能、节点扩容、槽位迁移、failover故障切换、一致性选举完整分析，对理解redis源码很有帮助，解决了source insight中文注释乱码问题,更新完毕|511|C|2021/08/23|
|68|[aliyun/rds_dbsync](https://github.com/aliyun/rds_dbsync)|围绕 PostgreSQL Greenplum ,实现易用的数据的互迁功能项目|483|C|2021/11/16|
|69|[chenall/grub4dos](https://github.com/chenall/grub4dos)|外部命令和工具源码:https://github.com/chenall/grubutils 下载:|477|C|2021/11/19|
|70|[aliyun/iotkit-embedded](https://github.com/aliyun/iotkit-embedded)|高速镜像: https://code.aliyun.com/linkkit/c-sdk|473|C|2021/05/12|
|71|[0voice/algorithm-structure](https://github.com/0voice/algorithm-structure)|2021年最新总结 500个常用数据结构，算法，算法导论，面试常用，大厂高级工程师整理总结|469|C|2021/08/27|
|72|[hanson-young/nniefacelib](https://github.com/hanson-young/nniefacelib)|nniefacelib是一个在海思35xx系列芯片上运行的人脸算法库|459|C|2021/10/13|
|73|[riba2534/TCP-IP-NetworkNote](https://github.com/riba2534/TCP-IP-NetworkNote)|📘《TCP/IP网络编程》(韩-尹圣雨)学习笔记|433|C|2021/11/30|
|74|[ksyun-kenc/liuguang](https://github.com/ksyun-kenc/liuguang)|鎏光云游戏引擎|433|C|2021/11/05|
|75|[hairrrrr/C-CrashCourse](https://github.com/hairrrrr/C-CrashCourse)|C语言教程+博客+代码演示+课程设计。 帮助初学者更好的理解 C 难点，提升代码量！ For beginners:C tuition/self-learning|429|C|2021/11/25|
|76|[chronolaw/annotated_nginx](https://github.com/chronolaw/annotated_nginx)|Annotated Nginx Source（中文）|419|C|2021/11/12|
|77|[chexiongsheng/build_xlua_with_libs](https://github.com/chexiongsheng/build_xlua_with_libs)|为xLua集成几个常用库，方便使用|416|C|2021/06/17|
|78|[hewei2001/HITSZ-OpenCS](https://github.com/hewei2001/HITSZ-OpenCS)|哈尔滨工业大学（深圳）计算机专业课程攻略   Guidance for courses in Department of Computer Science, Harbin Institute of Technology (Shenzhen)|408|C|2021/11/28|
|79|[scriptiot/evm](https://github.com/scriptiot/evm)|超轻量级物联网虚拟机|408|C|2021/06/24|
|80|[wangbojing/NtyTcp](https://github.com/wangbojing/NtyTcp)|单线程用户态TCP/IP协议栈，epoll实现，包含服务器案例，并发测试案例|403|C|2021/07/30|
|81|[wenjun1055/c](https://github.com/wenjun1055/c)|C语言学习代码|399|C|2021/10/02|
|82|[wondertrader/wondertrader](https://github.com/wondertrader/wondertrader)|WonderTrader——量化研发交易一站式框架|398|C|2021/11/30|
|83|[chankeh/net-lenrning-reference](https://github.com/chankeh/net-lenrning-reference)|TCP/IP网络编程笔记|385|C|2021/08/31|
|84|[yitter/IdGenerator](https://github.com/yitter/IdGenerator)|💎迄今为止最全面的分布式主键ID生成器。 💎优化的雪花算法（SnowFlake）——雪花漂移算法，在缩短ID长度的同时，具备极高瞬时并发处理能力（50W/0.1s）。 💎原生支持 C#/Java/Go/Rust/C/SQL 等多语言，且提供 PHP 扩展及 Python、Node.js、Ruby 多线程安全调用动态库（FFI）。💎支持容器环境自动扩容（自动注册 WorkerId ），单机或分布式唯一IdGenerator。💎顶尖优化，超强效能。|379|C|2021/11/01|
|85|[DavadDi/bpf_study](https://github.com/DavadDi/bpf_study)|bpf 学习仓库|372|C|2021/09/26|
|86|[hzcx998/xbook2](https://github.com/hzcx998/xbook2)|xbook2是一个基于x86处理器的32位操作系统，实现了大量的基础功能，可以拿来学习操作系统知识。|362|C|2021/10/31|
|87|[fanyuan/MyMp3Convert](https://github.com/fanyuan/MyMp3Convert)|mp3转码，把wav转换成mp3格式|348|C|2021/05/23|
|88|[QingdaoU/Judger](https://github.com/QingdaoU/Judger)|Online judge sandbox based on seccomp   OnlineJudge 安全沙箱|340|C|2021/11/25|
|89|[Greedysky/TTKWidgetTools](https://github.com/Greedysky/TTKWidgetTools)|QWidget 自定义控件集合  持续更新中......|335|C|2021/11/29|
|90|[fenwii/OpenHarmony](https://github.com/fenwii/OpenHarmony)|华为鸿蒙分布式操作系统（Huawei OpenHarmony）开发技术交流，鸿蒙技术资料，手册，指南，共建国产操作系统万物互联新生态。|321|C|2021/10/29|
|91|[Al1ex/WindowsElevation](https://github.com/Al1ex/WindowsElevation)|Windows Elevation(持续更新)|320|C|2021/11/29|
|92|[zmrbak/PcWeChatHooK](https://github.com/zmrbak/PcWeChatHooK)|云课堂《2019 PC微信 探秘》示例代码|306|C|2021/06/03|
|93|[imengyu/JiYuTrainer](https://github.com/imengyu/JiYuTrainer)|极域电子教室防控制软件, StudenMain.exe 破解|303|C|2021/08/18|
|94|[forthespada/MyPoorWebServer](https://github.com/forthespada/MyPoorWebServer)|一款可运行的基于C++ 实现的HTTP服务器，基于《TCPIP网络编程》和《Linux高性能服务器编程》实现的服务器项目。|297|C|2021/10/15|
|95|[OliverLew/PAT](https://github.com/OliverLew/PAT)|PAT OJ exercises in C language 浙江大学PAT纯C语言题解。|293|C|2021/10/05|
|96|[xuhongv/StudyInEsp32](https://github.com/xuhongv/StudyInEsp32)|【深度开源】wiif+bt模块esp32学习之旅（持续更新，欢迎 Star...）|279|C|2021/11/29|
|97|[dustpg/StepFC](https://github.com/dustpg/StepFC)|Make FC(NES) Emulator Step-by-Step 一步一步模拟红白机|275|C|2021/05/21|
|98|[gatieme/AderXCoding](https://github.com/gatieme/AderXCoding)|介绍各类语言，库，系统编程以及算法的学习|271|C|2021/05/09|
|99|[Kevincoooool/KS_DAP_Wireless](https://github.com/Kevincoooool/KS_DAP_Wireless)|STM32F103C8T6 无线有线DAP下载器，脱机（离线）烧录器，脱机下载器|263|C|2021/07/02|
|100|[jntass/TASSL](https://github.com/jntass/TASSL)|支持SM2 SM3 SM4国密算法和国密openssl协议的TASSL 新版基于openssl-1.1.1b版本已经更新到tassl-1.1.1b_R_0.8版本：https://github.com/jntass/TASSL-1.1.1b|262|C|2021/11/26|
|101|[yundiantech/VideoPlayer](https://github.com/yundiantech/VideoPlayer)|Qt+ffmpeg实现的视频播放器|252|C|2021/05/11|
|102|[notrynohigh/BabyOS](https://github.com/notrynohigh/BabyOS)|专为MCU项目开发提速的代码框架|250|C|2021/12/01|
|103|[MarioCrane/LeagueLobby](https://github.com/MarioCrane/LeagueLobby)|英雄联盟自定义房间创建工具，包括5V5训练营，(血月杀等轮换模式已被官方关闭)|250|C|2021/11/27|
|104|[dpull/skynet-mingw](https://github.com/dpull/skynet-mingw)|对skynet无任何改动的windows版|248|C|2021/11/20|
|105|[dreamsxin/cphalcon7](https://github.com/dreamsxin/cphalcon7)|Dao7 - Web framework for PHP7.x，QQ群 9970484|244|C|2021/11/26|
|106|[usbxyz/CAN-Bootloader](https://github.com/usbxyz/CAN-Bootloader)|使用USB2XXX实现的CAN Bootloader功能，实现CAN节点固件远程升级|243|C|2021/06/29|
|107|[riscv2os/riscv2os](https://github.com/riscv2os/riscv2os)|從 RISC-V 處理器到 UNIX 作業系統|242|C|2021/11/25|
|108|[NEWPLAN/SMx](https://github.com/NEWPLAN/SMx)|国家商用加密算法 SMx（SM2,SM3,SM4）|241|C|2021/09/27|
|109|[Hansimov/csapp](https://github.com/Hansimov/csapp)|个人整理的《深入理解计算机系统》中文电子版（原书第 3 版）与实验材料：https://hansimov.gitbook.io/csapp/|240|C|2021/08/12|
|110|[numberwolf/FFmpeg-Plus-OpenGL](https://github.com/numberwolf/FFmpeg-Plus-OpenGL)|🔥  OpenGL Filter for FFmpeg: Support Effects/Transition/Picture-in-Picture/LUT, Render video by your shaders(GLSL)  . 🔥 FFmpeg Filter支持特效/转场/画中画、特效/LUT贴纸能力, 支持OpenGL Shader。|229|C|2021/11/03|
|111|[deepwzh/sdust-examination-materials](https://github.com/deepwzh/sdust-examination-materials)|山东科技大学课程资源共享计划|220|C|2021/10/07|
|112|[404name/winter](https://github.com/404name/winter)|2020大一寒假从零开始的 C语言 小程序集，纯字符画逻辑实现【C语言笔记本电脑，哔哩哔哩移动端，超级玛丽，QQ，绝地求生等】让C语言课设不仅仅局限于图书馆管理系统！配套视频演示，代码均通过自己思考无任何参考实现，规范性只能说是相当于我大一的标准，欢迎分享，学习和交流|218|C|2021/11/25|
|113|[tsingsee/EasyPlayer-RTSP-Win](https://github.com/tsingsee/EasyPlayer-RTSP-Win)|An free, elegant, simple, fast windows RTSP Player.EasyPlayer support RTSP(RTP over TCP/UDP),video support H.264/H.265,audio support G.711/G.726/AAC！EasyPlayer RTSP是一款免费精炼、高效、稳定的RTSP流媒体播放器，视频支持H.264/H.265，音频支持G.711/G.726/AAC，支持RTP over UDP/TCP两种模式！|217|C|2021/07/19|
|114|[yourtion/YOS](https://github.com/yourtion/YOS)|YourtionOS 基于 30dayMakeOS (OSASK) 构建你自己的操作系统|199|C|2021/05/24|
|115|[HuaZhuangNan/actions-build-padavan-openwrt](https://github.com/HuaZhuangNan/actions-build-padavan-openwrt)|GitHub Action 学习实例 - 自动编译 padavan 和 openWrt|199|C|2021/06/15|
|116|[yujunjiex/behavior_captcha_cracker](https://github.com/yujunjiex/behavior_captcha_cracker)|基于深度学习的行为式验证码研究及破解。类型包括滑块式/点选式，平台包括极验/易盾/云片|198|C|2021/09/08|
|117|[openhisilicon/HIVIEW](https://github.com/openhisilicon/HIVIEW)|Multi-process software framework for hisilicon (海思) ipc/dvr/nvr/ebox|195|C|2021/11/30|
|118|[Manistein/dummylua-tutorial](https://github.com/Manistein/dummylua-tutorial)|这是一个仿制lua解释器的项目，我希望通过逐步实现lua解释器的各个部分，更加深刻地掌握lua的基本结构和运作原理。|190|C|2021/11/17|
|119|[XeiTongXueFlyMe/J1939](https://github.com/XeiTongXueFlyMe/J1939)|基于SAE J1939协议，开源可移植的J1939驱动。技术支持群： 264864184       @使用说明书：|189|C|2021/08/06|
|120|[eboxmaker/eBox_Framework](https://github.com/eboxmaker/eBox_Framework)|ebox是类似于arduino的一层api，简化stm32编程|184|C|2021/11/30|
|121|[foxclever/Modbus](https://github.com/foxclever/Modbus)|一个Modbus通讯协议栈|179|C|2021/08/27|
|122|[jiejieTop/cmd-parser](https://github.com/jiejieTop/cmd-parser)|一个非常简单好用的命令解析器，占用资源极少极少，采用哈希算法超快匹配命令！|178|C|2021/07/23|
|123|[zhouchangxun/ngx_healthcheck_module](https://github.com/zhouchangxun/ngx_healthcheck_module)|nginx module for upstream servers health check.  support stream and http upstream.                             该模块可以为Nginx提供主动式后端服务器健康检查的功能（同时支持四层和七层后端服务器的健康检测）|175|C|2021/06/04|
|124|[Kiprey/Skr_Learning](https://github.com/Kiprey/Skr_Learning)|天问之路 - 学习笔记&学习周报。内容包括但不限于C++ STL、编译原理、LLVM IR Pass代码优化、CSAPP Lab、uCore操作系统等等。持续更新ing...|174|C|2021/11/27|
|125|[crosg/idCreator](https://github.com/crosg/idCreator)|这是一个id生成器，主要为互联网的各种业务生成id（也就是数据库主键）。该id生成器生成的id主要被用来做数据路由之用。和Albianj2配合，可以快速而简单的搭建完整的分布式业务系统！|164|C|2021/07/22|
|126|[JingYang1124/HEX-LINK](https://github.com/JingYang1124/HEX-LINK)|这是一套可适用于PC端游戏的体感操作设备。|163|C|2021/07/29|
|127|[lintingbin2009/C-language](https://github.com/lintingbin2009/C-language)|C语言练习代码|162|C|2021/09/05|
|128|[openLuat/LuatOS](https://github.com/openLuat/LuatOS)|合宙LuatOS -- Lua base RTOS, build for many embedded systems. LuatOS是运行在嵌入式硬件的实时操作系统|161|C|2021/11/30|
|129|[g0dA/linuxStack](https://github.com/g0dA/linuxStack)|Linux技术栈|161|C|2021/09/19|
|130|[holdyeah/wechat-pc-hook-python](https://github.com/holdyeah/wechat-pc-hook-python)|微信 pc hook|160|C|2021/08/31|
|131|[we11cheng/WCPotatso](https://github.com/we11cheng/WCPotatso)|iOS Potatso源码(配置下自己的开发证书就能编译版本)|160|C|2021/05/19|
|132|[bilibilifmk/wifi_link_tool](https://github.com/bilibilifmk/wifi_link_tool)|这是一个为esp8266设计的通用配网加集群库https://keai.icu/wifilinktool|160|C|2021/11/11|
|133|[kuangyufei/kernel_liteos_a_note](https://github.com/kuangyufei/kernel_liteos_a_note)|精读鸿蒙内核源码,百万汉字注解分析;百篇博客深入解剖,挖透内核地基工程.注解同步官方,工具文档齐全,多站点发布 . weharmonyos.com|158|C|2021/11/30|
|134|[mrpre/atls](https://github.com/mrpre/atls)|A light TLS implementation used for learning: TLS 1.0 TLS 1.1 TLS 1.2 TLS 1.3 GMSSL 1.1(国密SSL) based on libcrypto.so.|152|C|2021/11/11|
|135|[vllogic/vllink_lite](https://github.com/vllogic/vllink_lite)|低成本CMSIS-DAP V2调试器，IO时序SPI优化，速度级别200KB/S至450KB/S|147|C|2021/08/15|
|136|[ClimbSnail/HoloCubic_AIO](https://github.com/ClimbSnail/HoloCubic_AIO)|HoloCubic超多功能AIO固件 基于esp32的天气时钟、相册、视频播放、桌面投屏、web服务、bilibili粉丝等|145|C|2021/11/30|
|137|[iBreaker/OS-One](https://github.com/iBreaker/OS-One)|一个自制的树莓派操作系统|142|C|2021/09/24|
|138|[plctlab/riscv-operating-system-mooc](https://github.com/plctlab/riscv-operating-system-mooc)|《从头写一个RISC-V OS》课程配套的资源|140|C|2021/09/12|
|139|[sj15712795029/bluetooth_stack](https://github.com/sj15712795029/bluetooth_stack)|这是一个开源的双模蓝牙协议栈(bluetooth.stack)(btstack),可以运行在STM32,Linux.，包含HCI,L2CAP,SDP,RFCOMM,HFP,SPP,A2DP,AVRCP,AVDTP,AVCTP,OBEX,PBAP等协议，后续会继续维护，以达到商用的目的|138|C|2021/11/29|
|140|[trumanzhao/luna](https://github.com/trumanzhao/luna)|基于C++17的lua/C++绑定库,以及lua的二进制序列化等辅助代码|136|C|2021/07/23|
|141|[jntass/TASSL-1.1.1b](https://github.com/jntass/TASSL-1.1.1b)|支持SM2 SM3 SM4国密算法和国密openssl协议的TASSL 基于openssl-1.1.1b版本|136|C|2021/11/26|
|142|[hungtcs-lab/8051-examples](https://github.com/hungtcs-lab/8051-examples)|基于SDCC编译器的8051单片机示例|133|C|2021/09/11|
|143|[debugger999/gb28181ToH264](https://github.com/debugger999/gb28181ToH264)|作为上级域，可以对接海康、大华、宇视等gb28181平台，获取ps流，转换为标准h.264裸流|132|C|2021/08/15|
|144|[gofmt/iOSSniffer](https://github.com/gofmt/iOSSniffer)|iOS抓包工具|132|C|2021/05/08|
|145|[Shies/dtrace](https://github.com/Shies/dtrace)|用c语言编写forp PHP分析器是一个轻量级的PHP扩展提供脚本的调用堆栈,CPU和内存使用|131|C|2021/09/09|
|146|[Yu2erer/LuaJIT-5.3.6](https://github.com/Yu2erer/LuaJIT-5.3.6)|Lua 5.3.6 JIT && 多线程 垃圾回收|127|C|2021/09/06|
|147|[balloonwj/mybooksources](https://github.com/balloonwj/mybooksources)|《C++ 服务器开发精髓》随书配套源码|125|C|2021/09/01|
|148|[pymumu/tinylog](https://github.com/pymumu/tinylog)|A lightweight C,C++ log component developed for Linux, It is designed with high performance, asynchronized, thread-safe and process-safe;   tinylog是一个专为UNIX设计的轻量级的C/C++日志模块，其提供了高性能，异步，线程安全，进程安全的日志功能。|125|C|2021/08/01|
|149|[KyleRicardo/MentoHUST-OpenWrt-ipk](https://github.com/KyleRicardo/MentoHUST-OpenWrt-ipk)|MentoHUST-OpenWrt-ipk包|123|C|2021/11/28|
|150|[albuer/heapsnap](https://github.com/albuer/heapsnap)|HeapSnap 是一个定位内存泄露的工具，适用于Android平台。|122|C|2021/06/02|
|151|[EternalStarCHN/SmartHumidifier](https://github.com/EternalStarCHN/SmartHumidifier)|【⏹已完结】无线智能控制的加湿器设计“聪明的加湿器”（本科毕业设计）|122|C|2021/09/14|
|152|[antiwar3/py](https://github.com/antiwar3/py)|飘云ark（pyark） |122|C|2021/08/26|
|153|[Lighter-z/51-Single-chip](https://github.com/Lighter-z/51-Single-chip)|51单片机入门资料|120|C|2021/08/15|
|154|[jordiwang/web-capture](https://github.com/jordiwang/web-capture)|基于 ffmpeg + Webassembly 实现前端视频帧提取|117|C|2021/09/05|
|155|[0671/RedisModules-ExecuteCommand-for-Windows](https://github.com/0671/RedisModules-ExecuteCommand-for-Windows)|可在Windows下执行系统命令的Redis模块，可用于Redis主从复制攻击。|116|C|2021/07/06|
|156|[r0ysue/AndroidFridaBeginnersBook](https://github.com/r0ysue/AndroidFridaBeginnersBook)|《安卓Frida逆向与抓包实战》随书附件|116|C|2021/10/15|
|157|[aceld/Lars](https://github.com/aceld/Lars)|负载均衡远程服务调度系统|115|C|2021/08/09|
|158|[MBronsom/OfflineSWD](https://github.com/MBronsom/OfflineSWD)|STM32系列离线烧写器|114|C|2021/08/29|
|159|[zhangboyang/PAL3patch](https://github.com/zhangboyang/PAL3patch)|《仙剑奇侠传三》《仙剑奇侠传三外传·问情篇》分辨率补丁|113|C|2021/05/04|
|160|[leleliu008/C](https://github.com/leleliu008/C)|C语言学习项目|112|C|2021/11/23|
|161|[Manistein/SparkServer](https://github.com/Manistein/SparkServer)|SparkServer是一个参照skynet设计的C#服务端框架，能够无缝整合到skynet集群机制中，也能自行组网，构建只有SparkServer节点的集群|111|C|2021/05/10|
|162|[hujianzhe/util](https://github.com/hujianzhe/util)|纯C的跨平台基础库与网络库,提供list/hashtable/rbtree数据结构,封装各OS API,对使用者屏蔽诸如IO多路复用下的并发可靠UDP/TCP的传输与监听,断线重连,基于协程/回调的RPC调度核心机制等实现细节.内部还包括一个3D碰撞检测.|110|C|2021/11/30|
|163|[wudicgi/SpleeterMsvcExe](https://github.com/wudicgi/SpleeterMsvcExe)|Windows command line program for Spleeter, written in pure C, no need of Python.   Spleeter 的 Windows 命令行程序，纯 C 语言编写，无需 Python.|109|C|2021/05/12|
|164|[zl03jsj/mupdf](https://github.com/zl03jsj/mupdf)|扩展mupdf 实现图像数字证书签名, 手写笔迹数字证书签名, 插入图片, 手写签名注释..改进手写批注,实现模拟真实手写的算法, annot自定义数据, annot插入,删除密码校验等功能...支持ios, windows, mac, linux, c++, java, android|108|C|2021/06/16|
|165|[evilbinary/YiYiYa](https://github.com/evilbinary/YiYiYa)|YiYiYa 一个os|106|C|2021/11/21|
|166|[luopeiyu/million_game_server](https://github.com/luopeiyu/million_game_server)|《百万在线：大型游戏服务端开发》是C++与Lua方向游戏服务端开发的入门书籍，内容涵盖Skynet引擎、C++底层开发、服务端架构设计等多个方面，全景展现网络游戏核心技术。|103|C|2021/09/09|
|167|[txthinking/joker](https://github.com/txthinking/joker)|Joker can turn process into daemon. Zero-Configuration. Joker可以将进程变成守护进程. 无需配置.|103|C|2021/07/16|
|168|[zkwlx/HiMem](https://github.com/zkwlx/HiMem)|HiMem 是知乎内部使用的针对 Android 系统开发的线上应用内存使用监控与分析套件，目前支持 mmap/munmap/malloc/calloc/realloc/free 等库函数的监控，同时提供高性能的 Java 层和 Native 层调用栈捕获能力。|102|C|2021/11/09|
|169|[douzhongqiang/EasyCanvas](https://github.com/douzhongqiang/EasyCanvas)|基于Qt QGraphicsView的简易画图软件|102|C|2021/10/11|
|170|[hihkm/DanmakuFactory](https://github.com/hihkm/DanmakuFactory)|支持特殊弹幕的xml转ass格式转换工具|102|C|2021/09/02|
|171|[LookCos/learn-data-structures](https://github.com/LookCos/learn-data-structures)|数据结构（C语言描述）学习笔记|101|C|2021/11/29|
|172|[Mculover666/HAL_Driver_Lib](https://github.com/Mculover666/HAL_Driver_Lib)|基于STM32 HAL库的外设驱动库|101|C|2021/08/29|
|173|[vmrp/vmrp](https://github.com/vmrp/vmrp)|mrp emulator, virtual machine, mrp模拟器|100|C|2021/08/03|
|174|[Ai-Thinker-Open/Telink_825X_SDK](https://github.com/Ai-Thinker-Open/Telink_825X_SDK)|Telink TLS825X 蓝牙芯片SDK|99|C|2021/06/17|
|175|[pangdahua/php7-wxwork-finance-sdk](https://github.com/pangdahua/php7-wxwork-finance-sdk)|PHP企业微信会话存档扩展|98|C|2021/10/08|
|176|[archeryue/cpc](https://github.com/archeryue/cpc)|建党 100 周年！写一个名为 CPC 的 C 编译器，为党庆生！|97|C|2021/09/15|
|177|[AttackTeamFamily/cobaltstrike-bof-toolset](https://github.com/AttackTeamFamily/cobaltstrike-bof-toolset)|在cobaltstrike中使用的bof工具集，收集整理验证好用的bof。|96|C|2021/11/08|
|178|[arkdb/arkproxy](https://github.com/arkdb/arkproxy)|高性能、高可靠的数据库跨云中间件，完全兼容MySQL技术体系，由极数云舟出品开源|95|C|2021/11/05|
|179|[AntSwordProject/ant_php_extension](https://github.com/AntSwordProject/ant_php_extension)|PHP 扩展, 用于 PHP-FPM、FastCGI、LD_PRELOAD等模式下突破 disabled_functions|93|C|2021/09/08|
|180|[xuexiangjys/XVideo](https://github.com/xuexiangjys/XVideo)|一个能自动进行压缩的小视频录制库|92|C|2021/06/15|
|181|[cdians/FastChia](https://github.com/cdians/FastChia)|Chia奇亚 plot(P盘)快速一键化工具|92|C|2021/07/28|
|182|[Rvn0xsy/linux_dirty](https://github.com/Rvn0xsy/linux_dirty)|更改后的脏牛提权代码，可以往任意文件写入任意内容，去除交互过程|91|C|2021/10/28|
|183|[poemdistance/ScreenTranslation](https://github.com/poemdistance/ScreenTranslation)|Linux屏幕取词翻译(适用于如Gnome等基于X server的桌面发行版) -- 含界面|91|C|2021/07/18|
|184|[jingweizhanghuai/Morn](https://github.com/jingweizhanghuai/Morn)|Morn是一个C语言的基础工具和基础算法库，包括数据结构、图像处理、音频处理、机器学习等，具有简单、通用、高效的特点。|89|C|2021/11/07|
|185|[labplus-cn/mpython](https://github.com/labplus-cn/mpython)|mpython掌控板文档和固件源码|89|C|2021/11/30|
|186|[figozhang/runninglinuxkernel_5.0](https://github.com/figozhang/runninglinuxkernel_5.0)|奔跑吧linux内核第二版（卷1，卷2，入门篇） 实验平台|88|C|2021/11/28|
|187|[yhnu/op7t](https://github.com/yhnu/op7t)|oneplus 7t 自定义内核(for 安卓逆向/外挂分析)|87|C|2021/11/26|
|188|[zwluoqi/mobile-visual-shader-editor](https://github.com/zwluoqi/mobile-visual-shader-editor)|一款跨平台着色器编辑工具|87|C|2021/10/10|
|189|[windowsair/wireless-esp8266-dap](https://github.com/windowsair/wireless-esp8266-dap)|ESP8266 Wireless Debugger. Based on CMSIS-DAP v2.0.0. Optional SPI acceleration, etc.                                                                                          ESP8266 无线调试器|87|C|2021/11/14|
|190|[Hom-Wang/Quadcopter](https://github.com/Hom-Wang/Quadcopter)|101年大學專題－四軸飛行器|85|C|2021/06/21|
|191|[Harry-hhj/CVRM2021-sjtu](https://github.com/Harry-hhj/CVRM2021-sjtu)|2021赛季交龙哨兵代码开源|84|C|2021/10/09|
|192|[JingYang1124/Acoustic-controlled-Mini-Racer-Beat-Magnum](https://github.com/JingYang1124/Acoustic-controlled-Mini-Racer-Beat-Magnum)|本项目是一款声控的迷你赛车--“跃动冲锋”，可通过语音指令控制车子的运动，还原了动画片“四驱兄弟”中的场景。|82|C|2021/07/02|
|193|[tsingsee/EasyPlayer-RTSP-iOS](https://github.com/tsingsee/EasyPlayer-RTSP-iOS)|An free, elegant, simple, fast windows RTSP Player.EasyPlayer support RTSP(RTP over TCP/UDP),video support H.264/H.265,audio support G.711/G.726/AAC！EasyPlayer RTSP是一款免费、精炼、高效、稳定的RTSP流媒体播放器，视频支持H.264/H.265，音频支持G.711/G.726/AAC，支持RTP over UDP/TCP两种模式！|80|C|2021/08/19|
|194|[zengwangfa/2019-Electronic-Design-Competition](https://github.com/zengwangfa/2019-Electronic-Design-Competition)|【电赛】2019 全国大学生电子设计竞赛 （F题）纸张数量检测装置 （基于STM32F407 & FDC2214 & USART HMI）|79|C|2021/07/18|
|195|[yuawn/NTU-Computer-Security](https://github.com/yuawn/NTU-Computer-Security)|台大 計算機安全 - Pwn 簡報、影片、作業題目與解法 - Computer Security Fall 2019 @ CSIE NTU Taiwan|79|C|2021/11/28|
|196|[alibaba/id2_client_sdk](https://github.com/alibaba/id2_client_sdk)|ID²（Internet Device ID），是物联网设备的可信身份标识，具备不可篡改、不可伪造、全球唯一的安全属性，是实现万物互联、服务流转的关键基础设施。ID²支持多安全等级载体，合理地平衡物联网在安全、成本、功耗等各方面的诉求，为客户提供用得起、容易用、有保障的安全方案，适应物联网碎片化的市场需求。     ID² Client SDK是用于设备端开发的软件工具包，帮助开发者快速集成接入ID²开放平台.  |78|C|2021/10/01|
|197|[mysterywolf/RT-Thread-wrapper-of-uCOS-III](https://github.com/mysterywolf/RT-Thread-wrapper-of-uCOS-III)|RT-Thread操作系统的uCOS-III兼容层：让基于uC/OS-III操作系统开发的应用层无感地迁移到RT-Thread操作系统     A wrapper which can make codes developed by uCOS-III APIs directly run on RT-Thread|75|C|2021/11/26|
|198|[Cai-Zi/STM32_T12_Controller](https://github.com/Cai-Zi/STM32_T12_Controller)|T12烙铁控制器|73|C|2021/05/05|
|199|[ZhiyangZhou24/CANopen-STM32F1](https://github.com/ZhiyangZhou24/CANopen-STM32F1)|基于CANfestival的CANopen协议在STM32F1系列单片机上的实现|73|C|2021/05/25|
|200|[theanarkh/read-libuv-code](https://github.com/theanarkh/read-libuv-code)|libuv源码分析|72|C|2021/06/07|

⬆ [回到目录](#内容目录)

<br/>

## Shell

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[shengxinjing/programmer-job-blacklist](https://github.com/shengxinjing/programmer-job-blacklist)|:see_no_evil:程序员找工作黑名单，换工作和当技术合伙人需谨慎啊 更新有赞|27.5k|Shell|2021/08/10|
|2|[233boy/v2ray](https://github.com/233boy/v2ray)|最好用的 V2Ray 一键安装脚本 & 管理脚本|15.0k|Shell|2021/08/24|
|3|[rootsongjc/kubernetes-handbook](https://github.com/rootsongjc/kubernetes-handbook)|Kubernetes中文指南/云原生应用架构实践手册 -  https://jimmysong.io/kubernetes-handbook|9.3k|Shell|2021/11/18|
|4|[skywind3000/awesome-cheatsheets](https://github.com/skywind3000/awesome-cheatsheets)|超级速查表 - 编程语言、框架和开发工具的速查表，单个文件包含一切你需要知道的东西 :zap:|7.9k|Shell|2021/10/30|
|5|[judasn/Linux-Tutorial](https://github.com/judasn/Linux-Tutorial)|《Java 程序员眼中的 Linux》|7.7k|Shell|2021/07/05|
|6|[wulabing/Xray_onekey](https://github.com/wulabing/Xray_onekey)|Xray 基于 Nginx 的 VLESS + XTLS 一键安装脚本 |7.0k|Shell|2021/11/09|
|7|[opsnull/follow-me-install-kubernetes-cluster](https://github.com/opsnull/follow-me-install-kubernetes-cluster)|和我一步步部署 kubernetes 集群|6.6k|Shell|2021/06/04|
|8|[P3TERX/Actions-OpenWrt](https://github.com/P3TERX/Actions-OpenWrt)|A template for building OpenWrt with GitHub Actions   使用 GitHub Actions 云编译 OpenWrt|4.7k|Shell|2021/11/06|
|9|[mack-a/v2ray-agent](https://github.com/mack-a/v2ray-agent)|（VLESS+TCP+TLS/VLESS+TCP+XTLS/VLESS+gRPC+TLS/VLESS+WS+TLS/VMess+TCP+TLS/VMess+WS+TLS/Trojan+TCP+TLS/Trojan+gRPC+TLS/Trojan+TCP+XTLS）+伪装站点、八合一共存脚本，支持多内核安装|4.0k|Shell|2021/11/29|
|10|[wangdoc/bash-tutorial](https://github.com/wangdoc/bash-tutorial)|Bash 教程|3.3k|Shell|2021/10/24|
|11|[wangdoc/javascript-tutorial](https://github.com/wangdoc/javascript-tutorial)|JavaScript 教程 https://wangdoc.com/javascript|3.1k|Shell|2021/10/29|
|12|[klever1988/nanopi-openwrt](https://github.com/klever1988/nanopi-openwrt)|Openwrt for Nanopi R1S R2S R4S 香橙派 R1 Plus 固件编译 纯净版与大杂烩|3.0k|Shell|2021/12/01|
|13|[lmk123/oh-my-wechat](https://github.com/lmk123/oh-my-wechat)|微信小助手的安装 / 更新工具。|2.8k|Shell|2021/11/30|
|14|[CyC2018/Job-Recommend](https://github.com/CyC2018/Job-Recommend)|🔎 互联网内推信息（社招、校招、实习）|2.5k|Shell|2021/10/09|
|15|[wind-liang/leetcode](https://github.com/wind-liang/leetcode)|leetcode 顺序刷题，详细通俗题解，with JAVA|2.2k|Shell|2021/09/02|
|16|[licess/lnmp](https://github.com/licess/lnmp)|LNMP一键安装包是一个用Linux Shell编写的可以为CentOS/RHEL/Fedora/Aliyun/Amazon、Debian/Ubuntu/Raspbian/Deepin/Mint Linux VPS或独立主机安装LNMP(Nginx/MySQL/PHP)、LNMPA(Nginx/MySQL/PHP/Apache)、LAMP(Apache/MySQL/PHP)生产环境的Shell程序。|2.1k|Shell|2021/11/02|
|17|[hijkpw/scripts](https://github.com/hijkpw/scripts)|Shadowsocks/SS一键脚本、ShadowsocksR/SSR一键脚本、V2Ray一键脚本、trojan一键脚本、VPS教程|2.1k|Shell|2021/09/01|
|18|[P3TERX/aria2.conf](https://github.com/P3TERX/aria2.conf)|Aria2 配置文件   OneDrive & Google Drvive 离线下载   百度网盘转存|1.8k|Shell|2021/10/23|
|19|[LCTT/TranslateProject](https://github.com/LCTT/TranslateProject)|Linux中国翻译项目|1.8k|Shell|2021/12/01|
|20|[bclswl0827/v2ray-heroku](https://github.com/bclswl0827/v2ray-heroku)|用于在 Heroku 上部署 V2Ray WebSocket。|1.8k|Shell|2021/08/15|
|21|[neoFelhz/neohosts](https://github.com/neoFelhz/neohosts)|自由·负责·克制 去广告 Hosts 项目|1.6k|Shell|2021/06/14|
|22|[gaoyifan/china-operator-ip](https://github.com/gaoyifan/china-operator-ip)|中国运营商IPv4/IPv6地址库-每日更新|1.5k|Shell|2021/12/01|
|23|[jinwyp/one_click_script](https://github.com/jinwyp/one_click_script)|一键安装 trojan v2ray xray. Install v2ray / xray (VLESS) and trojan (trojan-go) script|1.4k|Shell|2021/12/01|
|24|[MvsCode/frps-onekey](https://github.com/MvsCode/frps-onekey)| Frps 一键安装脚本&管理脚本 A tool to auto-compile & install frps on Linux|1.4k|Shell|2021/10/31|
|25|[Sitoi/dailycheckin](https://github.com/Sitoi/dailycheckin)|基于【腾讯云函数】/【Docker】/【青龙面板】/【elecV2P】/【群晖】的每日签到脚本（支持多账号使用）签到列表: ｜爱奇艺｜全民K歌｜腾讯视频｜有道云笔记｜网易云音乐｜一加手机社区官方论坛｜百度贴吧｜Bilibili｜V2EX｜咔叽网单｜什么值得买｜AcFun｜天翼云盘｜吾爱破解｜芒果TV｜Fa米家｜小米运动｜百度搜索资源平台｜时光相册｜哔咔漫画｜联通营业厅｜|1.4k|Shell|2021/11/27|
|26|[devdawei/libstdc-](https://github.com/devdawei/libstdc-)|Xcode 10 之后删除的 libstdc++ 库|1.3k|Shell|2021/11/19|
|27|[huan/docker-wechat](https://github.com/huan/docker-wechat)|DoChat is a Dockerized WeChat (盒装微信) PC Windows Client for Linux|1.3k|Shell|2021/11/09|
|28|[ben1234560/k8s_PaaS](https://github.com/ben1234560/k8s_PaaS)|如何基于K8s(Kubernetes)部署成PaaS/DevOps(一套完整的软件研发和部署平台)--教程/学习(实战代码/欢迎讨论/大量注释/操作配图)，你将习得部署如：K8S(Kubernetes)、dashboard、Harbor、Jenkins、本地gitlab、Apollo框架、promtheus、grafana、spinnaker等。|1.3k|Shell|2021/11/16|
|29|[studygolang/GCTT](https://github.com/studygolang/GCTT)|GCTT Go中文网翻译组。|1.3k|Shell|2021/10/20|
|30|[johnrosen1/vpstoolbox](https://github.com/johnrosen1/vpstoolbox)|厌倦了总是需要手动输入命令安装博客，网盘，邮箱，代理了吗？VPSToolBox提供了一种全自动化的解决方案，解放双手，从今天开始！|1.3k|Shell|2021/11/22|
|31|[duguying/parsing-techniques](https://github.com/duguying/parsing-techniques)|📕 parsing techniques 中文译本——《解析技术》|1.2k|Shell|2021/11/08|
|32|[insightglacier/Dictionary-Of-Pentesting](https://github.com/insightglacier/Dictionary-Of-Pentesting)|Dictionary collection project such as Pentesing, Fuzzing, Bruteforce and BugBounty. 渗透测试、SRC漏洞挖掘、爆破、Fuzzing等字典收集项目。|947|Shell|2021/10/24|
|33|[ToyoDAdoubiBackup/doubi](https://github.com/ToyoDAdoubiBackup/doubi)|一个逗比写的各种逗比脚本~|918|Shell|2021/05/18|
|34|[liquanzhou/ops_doc](https://github.com/liquanzhou/ops_doc)|运维简洁实用手册|907|Shell|2021/06/02|
|35|[IvanSolis1989/OpenWrt-DIY](https://github.com/IvanSolis1989/OpenWrt-DIY)|       多设备 OpenWrt Aciton 固件云编译——X86、竞斗云、极路由 B70、K2T、K2P、K3、N1、红米 AC2100、Newifi D2、树莓派、小娱 C5、R2S、R4S、小米 R3G、小米 R3P、小米 Mini、网件 R7800、星际宝盒 CM520、Amlogic S905X3、OrangePi Zero Plus、网件 3800、Linksys Wrt1900acs、Linksys Wrt3200acm、Linksys Wrt32x——（QQ群：1130190364）|875|Shell|2021/11/26|
|36|[xiaoZ-hc/redtool](https://github.com/xiaoZ-hc/redtool)|日常积累的一些红队工具及自己写的脚本，更偏向于一些diy的好用的工具，并不是一些比较常用的msf/awvs/xray这种|866|Shell|2021/06/01|
|37|[goreliu/zshguide](https://github.com/goreliu/zshguide)|Zsh 开发指南|866|Shell|2021/11/26|
|38|[esirplayground/AutoBuild-OpenWrt](https://github.com/esirplayground/AutoBuild-OpenWrt)|Build OpenWrt using GitHub Actions   使用 GitHub Actions 编译 OpenWrt   感谢P3TERX的项目源码 感谢KFERMercer的项目源码|838|Shell|2021/11/28|
|39|[al0ne/LinuxCheck](https://github.com/al0ne/LinuxCheck)|Linux应急处置/信息搜集/漏洞检测工具，支持基础配置/网络流量/任务计划/环境变量/用户信息/Services/bash/恶意文件/内核Rootkit/SSH/Webshell/挖矿文件/挖矿进程/供应链/服务器风险等13类70+项检查|787|Shell|2021/10/19|
|40|[tonydeng/sdn-handbook](https://github.com/tonydeng/sdn-handbook)|SDN手册|786|Shell|2021/07/23|
|41|[godbasin/vue-ebook](https://github.com/godbasin/vue-ebook)|《深入理解Vue.js实战》- 介绍Vue.js框架的出现、设计和使用，结合实战让读者更深入理解Vue.js框架，掌握使用方法。|769|Shell|2021/11/03|
|42|[EternalPain/ZJL](https://github.com/EternalPain/ZJL)|ZJL 免流防跳脚本|768|Shell|2021/06/08|
|43|[RokasUrbelis/docker-wine-linux](https://github.com/RokasUrbelis/docker-wine-linux)|:boom::whale::fire:Linux运行wine应用(QQ/微信/百度网盘/TIM/迅雷极速版/Foxmail等)，适用于所有发行版------- Best wine-QQ/TIM/Wechat for all Linux distros|755|Shell|2021/09/29|
|44|[dunwu/linux-tutorial](https://github.com/dunwu/linux-tutorial)|:penguin: Linux教程，主要内容：Linux 命令、Linux 系统运维、软件运维、精选常用Shell脚本|750|Shell|2021/05/13|
|45|[Hagb/docker-easyconnect](https://github.com/Hagb/docker-easyconnect)|使深信服（Sangfor）开发的非自由的代理软件 EasyConnect 运行在 docker 中，并提供 socks5 服务|734|Shell|2021/10/02|
|46|[hongwenjun/vps_setup](https://github.com/hongwenjun/vps_setup)|linux  vim bash 脚本学习笔记 by 蘭雅sRGB   https://262235.xyz/|731|Shell|2021/11/24|
|47|[snail007/proxy_admin_free](https://github.com/snail007/proxy_admin_free)|Proxy是高性能全功能的http代理、https代理、socks5代理、内网穿透、内网穿透p2p、内网穿透代理、内网穿透反向代理、内网穿透服务器、Websocket代理、TCP代理、UDP代理、DNS代理、DNS加密代理，代理API认证，全能跨平台代理服务器。|718|Shell|2021/10/18|
|48|[rime/plum](https://github.com/rime/plum)|東風破 /plum/: Rime configuration manager and input schema repository|689|Shell|2021/10/02|
|49|[ctf-wiki/ctf-tools](https://github.com/ctf-wiki/ctf-tools)|CTF 工具集合|678|Shell|2021/08/14|
|50|[rehiy/dnspod-shell](https://github.com/rehiy/dnspod-shell)|基于DNSPod用户API实现的纯Shell动态域名客户端|676|Shell|2021/11/28|
|51|[skyline75489/Heart-First-JavaWeb](https://github.com/skyline75489/Heart-First-JavaWeb)|一个走心的 Java Web 入门开发教程|669|Shell|2021/11/11|
|52|[zhonghuasheng/Tutorial](https://github.com/zhonghuasheng/Tutorial)|Java全栈知识架构体系总结|656|Shell|2021/12/01|
|53|[ffffffff0x/f8x](https://github.com/ffffffff0x/f8x)|红/蓝队环境自动化部署工具   Red/Blue team environment automation deployment tool|650|Shell|2021/11/29|
|54|[phlinhng/v2ray-tcp-tls-web](https://github.com/phlinhng/v2ray-tcp-tls-web)|VLESS / Trojan-Go / Shadowsocks 脚本 支持纯 IPv6|618|Shell|2021/08/14|
|55|[UnblockNeteaseMusic/luci-app-unblockneteasemusic](https://github.com/UnblockNeteaseMusic/luci-app-unblockneteasemusic)|[OpenWrt] 解除网易云音乐播放限制|617|Shell|2021/11/27|
|56|[P3TERX/warp.sh](https://github.com/P3TERX/warp.sh)|Cloudflare WARP configuration script   Cloudflare WARP 一键配置脚本|606|Shell|2021/11/27|
|57|[woniuzfb/iptv](https://github.com/woniuzfb/iptv)|一键安装管理 FFmpeg / nginx / openresty / xray / v2ray / armbian /  proxmox ve / cloudflare partner,workers / ibm cloud foundry 脚本|603|Shell|2021/11/28|
|58|[kirin10000/Xray-script](https://github.com/kirin10000/Xray-script)|Xray：（VLESS/VMess）-（TCP/gRPC/WebSocket）-（XTLS/TLS）+Web 搭建/管理脚本|602|Shell|2021/11/27|
|59|[koolshare/armsoft](https://github.com/koolshare/armsoft)|梅林384软件中心 for armv7l架构机型|596|Shell|2021/11/24|
|60|[wangdoc/es6-tutorial](https://github.com/wangdoc/es6-tutorial)|一本开源的 JavaScript 语言教程，全面介绍 ECMAScript 6 新引入的语法特性。|593|Shell|2021/11/24|
|61|[wppurking/ocserv-docker](https://github.com/wppurking/ocserv-docker)|用于初始化 ocserv 的 Dockfile 脚本|582|Shell|2021/08/11|
|62|[TheKingOfDuck/ApkAnalyser](https://github.com/TheKingOfDuck/ApkAnalyser)|一键提取安卓应用中可能存在的敏感信息。|551|Shell|2021/10/21|
|63|[Nick233333/phper-linux-gitbook](https://github.com/Nick233333/phper-linux-gitbook)|💡PHPer 必知必会的 Linux 命令|540|Shell|2021/08/28|
|64|[soulteary/Home-Network-Note](https://github.com/soulteary/Home-Network-Note)|🚧 持续更新 🚧 记录搭建兼顾学习娱乐的家用网络环境的过程，折腾过的一些软硬件小经验。|516|Shell|2021/11/10|
|65|[felix-fly/v2ray-openwrt](https://github.com/felix-fly/v2ray-openwrt)|路由器Openwrt手工/脚本/ipk包安装V2ray简单流程|502|Shell|2021/10/22|
|66|[CoiaPrant/MediaUnlock_Test](https://github.com/CoiaPrant/MediaUnlock_Test)|流媒体解锁检测|484|Shell|2021/07/17|
|67|[laishulu/Sarasa-Mono-SC-Nerd](https://github.com/laishulu/Sarasa-Mono-SC-Nerd)|简体中文等距更纱黑体+Nerd图标字体库。中英文宽度完美2:1，图标长宽经过调整，不会出现对齐问题，尤其适合作为终端字体。|481|Shell|2021/10/20|
|68|[jaywcjlove/docker-tutorial](https://github.com/jaywcjlove/docker-tutorial)|🐳 Docker入门学习笔记|478|Shell|2021/11/17|
|69|[JinjunHan/iOSDeviceSupport](https://github.com/JinjunHan/iOSDeviceSupport)|各个版本的iOS Device Support|474|Shell|2021/09/16|
|70|[neodevpro/neodevhost](https://github.com/neodevpro/neodevhost)| The Powerful Friendly Uptodate AD Blocking Hosts  最新强大而友善的去广告|470|Shell|2021/11/30|
|71|[1orz/My-action](https://github.com/1orz/My-action)|自动编译-无人值守Auto release base on Github actions|462|Shell|2021/11/24|
|72|[andyzhshg/syno-acme](https://github.com/andyzhshg/syno-acme)|通过acme协议更新群晖HTTPS泛域名证书的自动脚本|453|Shell|2021/11/26|
|73|[KANIKIG/Multi-EasyGost](https://github.com/KANIKIG/Multi-EasyGost)|致力于最简单好用的GOST小白脚本|442|Shell|2021/08/17|
|74|[lovezzzxxx/liverecord](https://github.com/lovezzzxxx/liverecord)|自动录播并自动备份，支持youtube频道、twitcast频道、twitch频道、openrec频道、niconico生放送、niconico社区、niconico频道、mirrativ频道、reality频道、17live频道、bilibili频道、streamlink支持的直播网址、ffmpeg支持的m3u8地址|439|Shell|2021/11/22|
|75|[veip007/dd](https://github.com/veip007/dd)|萌咖大佬的Linux 一键DD脚本|437|Shell|2021/08/24|
|76|[rootsongjc/istio-handbook](https://github.com/rootsongjc/istio-handbook)|Istio 服务网格 —— 后 Kubernetes 时代的应用网络 - https://jimmysong.io/istio-handbook|426|Shell|2021/11/21|
|77|[KyleBing/rime-wubi86-jidian](https://github.com/KyleBing/rime-wubi86-jidian)|86五笔极点码表 for Rime (macOs, Windows, Ubuntu)|406|Shell|2021/11/27|
|78|[ellermister/mtproxy](https://github.com/ellermister/mtproxy)|MTProxyTLS一键安装绿色脚本|389|Shell|2021/11/28|
|79|[qichengzx/gopher-reading-list-zh_CN](https://github.com/qichengzx/gopher-reading-list-zh_CN)|Golang中文博客文章阅读列表|387|Shell|2021/11/28|
|80|[Hyy2001X/AutoBuild-Actions](https://github.com/Hyy2001X/AutoBuild-Actions)|在线编译 Openwrt   一键在线更新固件   Dev|382|Shell|2021/11/30|
|81|[rootsongjc/kubernetes-hardening-guidance](https://github.com/rootsongjc/kubernetes-hardening-guidance)|《Kubernetes 加固手册》（美国国家安全局出品）- https://jimmysong.io/kubernetes-hardening-guidance|379|Shell|2021/10/13|
|82|[lework/kainstall](https://github.com/lework/kainstall)|Use shell scripts to install kubernetes(k8s) high availability clusters and addon components based on kubeadmin with one click.使用shell脚本基于kubeadmin一键安装kubernetes 高可用集群和addon组件。|375|Shell|2021/11/22|
|83|[lxchuan12/blog](https://github.com/lxchuan12/blog)|若川的博客—撰写了学习源码整体架构系列多篇。组织了源码共读活动，每周一起学习200行左右的源码，微信搜索【若川视野】，回复【源码】加我微信参与。|370|Shell|2021/11/30|
|84|[SuperManito/LinuxMirrors](https://github.com/SuperManito/LinuxMirrors)|GNU/Linux 一键更换国内软件源脚本|367|Shell|2021/10/24|
|85|[ericwang2006/docker_ttnode](https://github.com/ericwang2006/docker_ttnode)|甜糖星愿|366|Shell|2021/12/01|
|86|[DHDAXCW/NanoPi-R4S-2021](https://github.com/DHDAXCW/NanoPi-R4S-2021)|基于 Lean&Lienol 源码的 NanoPi R4S 的 OpenWrt 固件。每天自动更新插件和内核，Fusion编译法|365|Shell|2021/12/01|
|87|[esrrhs/spp](https://github.com/esrrhs/spp)|简单强大的多协议双向代理工具  A simple and powerful proxy|364|Shell|2021/10/11|
|88|[JACK-THINK/SCRIPTS-BOOTLOADER-FOR-ASUS-ROUTER](https://github.com/JACK-THINK/SCRIPTS-BOOTLOADER-FOR-ASUS-ROUTER)|用于华硕路由器官方固件和梅林固件的自启动脚本系统（Self-starting scripts that can be used in both Asuswrt and Asuswrt-Merlin）|363|Shell|2021/09/24|
|89|[wang-bin/avbuild](https://github.com/wang-bin/avbuild)|ffmpeg花式编译. build tool for all platforms: iOS, android, raspberry pi, win32, uwp, linux, macOS etc.|359|Shell|2021/11/15|
|90|[cloudnativeto/academy](https://github.com/cloudnativeto/academy)|云原生学院 直播 活动 - https://cloudnative.to/academy/|354|Shell|2021/11/18|
|91|[YAWAsau/backup_script](https://github.com/YAWAsau/backup_script)|備份數據腳本|354|Shell|2021/11/29|
|92|[starnightcyber/Miscellaneous](https://github.com/starnightcyber/Miscellaneous)|百宝箱|351|Shell|2021/08/13|
|93|[collabH/repository](https://github.com/collabH/repository)|知识仓库涉及到数据仓库建模、实时计算、大数据、数据中台、系统设计、Java、算法等。|349|Shell|2021/11/29|
|94|[meetbill/op_practice_book](https://github.com/meetbill/op_practice_book)|📚 《运维实践指南》持续更新中，推荐大牛干货博客 https://me.csdn.net/g2V13ah|343|Shell|2021/08/02|
|95|[a244573118/WeChatIntercept](https://github.com/a244573118/WeChatIntercept)|微信防撤回插件，一键安装，仅MAC可用，支持新版3.2.0微信|340|Shell|2021/09/02|
|96|[DHDAXCW/NanoPi-R2S-2021](https://github.com/DHDAXCW/NanoPi-R2S-2021)|基于 Lean&Lienol 源码的 NanoPi R2S 的 OpenWrt 固件。每天自动更新插件和内核，Fusion编译法|318|Shell|2021/11/30|
|97|[fscarmen/warp](https://github.com/fscarmen/warp)|WARP one-click script. Add an IPv4, IPv6 or dual-stack CloudFlare WARP network interface and Socks5 proxy for VPS. 一键脚本|312|Shell|2021/12/01|
|98|[cdk8s/cdk8s-team-style](https://github.com/cdk8s/cdk8s-team-style)|寻找志同道合的人，引发自身的思考|307|Shell|2021/12/01|
|99|[BlueSkyXN/SKY-BOX](https://github.com/BlueSkyXN/SKY-BOX)|BlueSkyXN  综合工具箱 Linux Supported ONLY|301|Shell|2021/10/23|
|100|[cookcodeblog/k8s-deploy](https://github.com/cookcodeblog/k8s-deploy)|使用kubeadm一键部署kubernetes集群|298|Shell|2021/11/25|
|101|[jgsrty/jgsrty.github.io](https://github.com/jgsrty/jgsrty.github.io)|:sunny: 英语学习 :feet: 项目预览：https://jgsrty.github.io 国内访问：https://rtyxmd.gitee.io|285|Shell|2021/11/02|
|102|[guanguans/dnmp-plus](https://github.com/guanguans/dnmp-plus)|🐳Docker的LNMP一键安装开发环境 + PHP非侵入式监控平台xhgui(优化系统性能、定位Bug神器)|268|Shell|2021/09/08|
|103|[Oreomeow/VIP](https://github.com/Oreomeow/VIP)|DONOT FORK!!!别 Fork 了！超过一定数量就跑路！🏃‍💨 Gone|264|Shell|2021/11/29|
|104|[openwrtcompileshell/OpenwrtCompileScript](https://github.com/openwrtcompileshell/OpenwrtCompileScript)|Openwrt编译辅助脚本可以帮助你更快的搭建openwrt环境，但不会帮你完成整个编译过程|261|Shell|2021/11/12|
|105|[wyx176/Socks5](https://github.com/wyx176/Socks5)|Socks5代理服务器搭建脚本/Socks5 shortcut creation script|261|Shell|2021/09/20|
|106|[whunt1/onekeymakemtg](https://github.com/whunt1/onekeymakemtg)|编译安装最新版 mtproxy-go 一键脚本|257|Shell|2021/06/22|
|107|[ameizi/vagrant-kubernetes-cluster](https://github.com/ameizi/vagrant-kubernetes-cluster)|Vagrant一键安装Kubernetes集群。安装 Metrics Server 、Kuboard 、Kubernetes Dashboard、KubePi、Kubernetes集群监控prometheus-operator|256|Shell|2021/10/30|
|108|[Lancenas/actions-openwrt-helloworld](https://github.com/Lancenas/actions-openwrt-helloworld)|Actions使用Lean's lede源码编译含helloworld服务固件|256|Shell|2021/08/21|
|109|[jaywcjlove/shell-tutorial](https://github.com/jaywcjlove/shell-tutorial)|Shell入门教程（Shell tutorial book）|250|Shell|2021/10/03|
|110|[rime/rime-cantonese](https://github.com/rime/rime-cantonese)|Rime Cantonese input schema   粵語拼音輸入方案|249|Shell|2021/10/29|
|111|[maybe1229/jd-base](https://github.com/maybe1229/jd-base)|京东薅羊毛利器|243|-|2021/10/28|
|112|[XIU2/Shell](https://github.com/XIU2/Shell)|🐧 自用的一些乱七八糟 Linux 脚本~|243|Shell|2021/09/29|
|113|[zq99299/note-book](https://github.com/zq99299/note-book)|新笔记本，java、git、elasticsearch、mycat、设计模式、gradle、vue， 等 。vuepress 构建的 Markdown 笔记。|242|Shell|2021/06/23|
|114|[Messiahhh/blog](https://github.com/Messiahhh/blog)|akara的前端笔记|242|Shell|2021/09/21|
|115|[Netflixxp/jcnf-box](https://github.com/Netflixxp/jcnf-box)|这是个人常用的服务器指令和一键脚本|237|Shell|2021/11/24|
|116|[wuleying/PHP](https://github.com/wuleying/PHP)|PHP相关资料|235|Shell|2021/11/19|
|117|[PaddlePaddle/FleetX](https://github.com/PaddlePaddle/FleetX)|Paddle Distributed Training Extended. 飞桨分布式训练扩展包|226|Shell|2021/11/29|
|118|[shenuiuin/LXD_GPU_SERVER](https://github.com/shenuiuin/LXD_GPU_SERVER)|实验室GPU服务器的LXD虚拟化|226|Shell|2021/06/23|
|119|[rootsongjc/migrating-to-cloud-native-application-architectures](https://github.com/rootsongjc/migrating-to-cloud-native-application-architectures)|《迁移到云原生应用架构》中文版 https://jimmysong.io/migrating-to-cloud-native-application-architectures/|225|Shell|2021/10/22|
|120|[orangbus/Tool](https://github.com/orangbus/Tool)|Manjaro从入门到爱不释手．|217|Shell|2021/06/10|
|121|[iwrt/luci-app-ikoolproxy](https://github.com/iwrt/luci-app-ikoolproxy)|iKoolProxy（原godproxy）是基于KoolProxyR 重新整理的能识别adblock规则的免费开源软件,追求体验更快、更清洁的网络，屏蔽烦人的广告！|215|Shell|2021/12/01|
|122|[ineo6/homebrew-install](https://github.com/ineo6/homebrew-install)|homebrew安装使用中科大镜像|214|Shell|2021/11/02|
|123|[eryajf/magic-of-sysuse-scripts](https://github.com/eryajf/magic-of-sysuse-scripts)|运维外挂小工具|214|Shell|2021/10/09|
|124|[wangdoc/html-tutorial](https://github.com/wangdoc/html-tutorial)|HTML 语言教程|203|Shell|2021/09/23|
|125|[yanhuacuo/98wubi-tables](https://github.com/yanhuacuo/98wubi-tables)|98五笔基础码表|203|Shell|2021/11/20|
|126|[levie-vans/WeChatAssistant-ForMac](https://github.com/levie-vans/WeChatAssistant-ForMac)|Mac微信功能拓展/微信插件/微信小助手(A plugin for Mac WeChat)独立加载版。微信双开助手。|201|Shell|2021/11/16|
|127|[tianhao/alfred-mweb-workflow](https://github.com/tianhao/alfred-mweb-workflow)|搜索、打开MWeb 内部文档和外部 Markdown 文档|201|Shell|2021/10/06|
|128|[Lbingyi/HerokuXray](https://github.com/Lbingyi/HerokuXray)|使用Heroku部署Xray高性能代理服务，通过ws传输的 (vmess、vless、trojan shadowsocks、socks)等协议|197|Shell|2021/11/27|
|129|[jinfeijie/yapi](https://github.com/jinfeijie/yapi)|Docker for YApi 一键部署YApi|195|Shell|2021/10/19|
|130|[kenzok8/openwrt_Build](https://github.com/kenzok8/openwrt_Build)|一键多编译固件|194|Shell|2021/11/28|
|131|[tinyclub/cloud-lab](https://github.com/tinyclub/cloud-lab)|Docker based Cloud Lab Center, with noVNC and Gateone attachable LXDE Desktop and SSH Terminal. 本项目作者发布了《360° 剖析 Linux ELF》视频课程，欢迎订阅：https://www.cctalk.com/m/group/88089283|194|Shell|2021/11/09|
|132|[kkkyg/CFwarp](https://github.com/kkkyg/CFwarp)|Cloudflare WARP 多功能一键脚本，支持添加WARP+PLUS账户及无限刷WARP+流量功能 ，支持纯IPV4/纯IPV6/双栈V4V6的VPS共9种情况随意切换安装。支持X86/ARM，支持kvm，openvz，lxc等主流架构，自动配置最佳TUM吞吐量数值，实时显示当前WARP状态、IP所属区（中文显示）及IP服务商。已测试：hax纯v6站，Euserv(德鸡)，oracle（甲骨文云），gpc（谷歌云），aws（亚马逊云），azure（微软云）等…………功能更新增加中…|193|Shell|2021/11/29|
|133|[P3TERX/Aria2-Pro-Core](https://github.com/P3TERX/Aria2-Pro-Core)|Aria2 static binaries for GNU/Linux with some powerful feature patches.    破解无限线程 防掉线程优化 静态编译 二进制文件 增强版|191|Shell|2021/08/22|
|134|[xiaoyunjie/Shell_Script](https://github.com/xiaoyunjie/Shell_Script)|Linux系统的安全，通过脚本对Linux系统进行一键检测和一键加固|190|Shell|2021/08/02|
|135|[hoochanlon/helpdesk-guide](https://github.com/hoochanlon/helpdesk-guide)|📖《桌维网管实典》主机与程控终端信息安全运维，IT方向速成就业入职|189|Shell|2021/11/16|
|136|[SuperNG6/docker-bilibili-helper](https://github.com/SuperNG6/docker-bilibili-helper)|docker bilibili助手|185|Shell|2021/10/14|
|137|[risfeng/aliyun-ddns-shell](https://github.com/risfeng/aliyun-ddns-shell)|阿里云域名解析动态更新IP Shell脚本|183|Shell|2021/09/06|
|138|[WangWenBin2017/OpenWrt-SSRPLUS](https://github.com/WangWenBin2017/OpenWrt-SSRPLUS)|包含ShadowsocksR Plus+、Passwall和Clash/OpenClash IPK文件的GitHub云编译（x86_64架构专用，包含依赖）。最后一个编译的版本已经支持Xray。详见Release页面。|182|Shell|2021/12/01|
|139|[wangtunan/blog](https://github.com/wangtunan/blog)|:memo: 记录个人博客，见证成长之路  https://wangtunan.github.io/blog/|181|Shell|2021/10/28|
|140|[idoop/docker-apollo](https://github.com/idoop/docker-apollo)|docker image for Ctrip/Apollo(携程Apollo) |179|Shell|2021/09/28|
|141|[huweihuang/kubernetes-notes](https://github.com/huweihuang/kubernetes-notes)|Kubernetes 学习笔记-https://www.huweihuang.com/kubernetes-notes/|178|Shell|2021/11/26|
|142|[wangdoc/webapi-tutorial](https://github.com/wangdoc/webapi-tutorial)|Web API 教程|178|Shell|2021/09/05|
|143|[paniy/Xray_bash_onekey](https://github.com/paniy/Xray_bash_onekey)|Xray+Nginx 包含 VLESS WebSocket/gPRC+TLS / XTLS+TCP 协议的一键安装脚本|178|Shell|2021/11/30|
|144|[yanbuyu/XiaomiCTSPass](https://github.com/yanbuyu/XiaomiCTSPass)|强制小米设备通过谷歌CTS测试|175|Shell|2021/10/05|
|145|[DavidPisces/ReduceMIUI](https://github.com/DavidPisces/ReduceMIUI)|MIUI精简计划|171|Shell|2021/11/18|
|146|[hczhcz/the-elder-is-excited](https://github.com/hczhcz/the-elder-is-excited)|暴力膜蛤|170|Shell|2021/11/16|
|147|[kalasutra/Clash_For_Magisk](https://github.com/kalasutra/Clash_For_Magisk)|使用shell启动clash内核,以及创建iptables tproxy规则,以此在Android上达到透明代理的目的.|170|Shell|2021/08/07|
|148|[yuyicai/update-kube-cert](https://github.com/yuyicai/update-kube-cert)|K8s集群证书过期处理，更新kubeadm生成的证书有效期为10年。针对旧版集群(小于v1.15)，当然大于等于v1.15也是可以用这个脚本更新，新版可直接kubeadm alpha certs renew <cert_name>更新 (deal with K8s cluster certificate expired)|168|Shell|2021/09/26|
|149|[fanck0605/openwrt-nanopi-r2s](https://github.com/fanck0605/openwrt-nanopi-r2s)|自制 NanoPi R2s  OpenWrt  固件|168|Shell|2021/08/11|
|150|[liqianggh/blog](https://github.com/liqianggh/blog)|简洁至上|167|Shell|2021/08/19|
|151|[imroc/kubernetes-practice-guide](https://github.com/imroc/kubernetes-practice-guide)|Kubernetes Practice Guide (Kubernetes 实践指南)|162|Shell|2021/08/19|
|152|[FlechazoPh/QLDependency](https://github.com/FlechazoPh/QLDependency)|青龙全依赖一键安装脚本|162|Shell|2021/12/01|
|153|[KingFalse/ohmyiterm2](https://github.com/KingFalse/ohmyiterm2)|快速安装一个漂亮且强大的iterm2|161|Shell|2021/06/29|
|154|[stilleshan/frpc](https://github.com/stilleshan/frpc)|基于原版 frp 内网穿透客户端 frpc 的一键安装卸载脚本和 docker 镜像.支持群晖NAS,Linux 服务器和 docker 等多种环境安装部署.|161|Shell|2021/10/26|
|155|[tal-tech/alarm-dog](https://github.com/tal-tech/alarm-dog)|哮天犬是一个通用的统一告警平台，提供配置化、流程化、标准化的能力，支持多种告警通知渠道，支持告警收敛、过滤、升级、工作流、自动恢复等功能，实现统一输入、不同输出。可以对接Grafana、阿里云Arms、实时计算等监控能力，各业务也可以直接在代码中埋点上报告警，也可以定制化开发，实现监控告警全场景覆盖。https://tal-tech.github.io/alarm-dog-docs|161|Shell|2021/07/12|
|156|[nanhantianyi/rpi-backup](https://github.com/nanhantianyi/rpi-backup)|raspberry pi backup，树莓派系统备份，最小镜像备份|160|Shell|2021/05/29|
|157|[M1Screw/Airport-toolkit](https://github.com/M1Screw/Airport-toolkit)|各類方便機場主進行安裝維護的shell腳本|160|Shell|2021/06/14|
|158|[FantasticLBP/codesnippets](https://github.com/FantasticLBP/codesnippets)|iOS 代码规范、属性、方法、GCD、线程等代码块和控制器、单例、Model 类模版|159|Shell|2021/10/08|
|159|[felix-fly/v2ray-dnsmasq-dnscrypt](https://github.com/felix-fly/v2ray-dnsmasq-dnscrypt)|路由器openwrt配置dnsmasq、doh，高性能v2ray解决方案。|158|Shell|2021/11/26|
|160|[Netflixxp/N1HK1dabao](https://github.com/Netflixxp/N1HK1dabao)|项目为自动打包N1和HK1（s905x3）盒子的固件，请勿fork，因为fork无法获取最新的固件，请收藏短网址https://jcnf.xyz/gj 获取最新固件版本；如需只要插件，可收藏另一个短网址 https://jcnf.xyz/ipk|156|Shell|2021/11/23|
|161|[kumakichi/easy_rust_chs](https://github.com/kumakichi/easy_rust_chs)|https://github.com/Dhghomon/easy_rust 简体中文翻译|156|Shell|2021/09/29|
|162|[primovist/snell.sh](https://github.com/primovist/snell.sh)|snell的一键安装脚本|155|Shell|2021/10/04|
|163|[Petit-Abba/backup_script_zh-CN](https://github.com/Petit-Abba/backup_script_zh-CN)|数据备份脚本 简体中文版|151|Shell|2021/12/01|
|164|[ustclug/Linux101-docs](https://github.com/ustclug/Linux101-docs)|Linux 101 学生课程教材与讲义|149|Shell|2021/11/30|
|165|[Karmenzind/dotfiles-and-scripts](https://github.com/Karmenzind/dotfiles-and-scripts)|:fishing_pole_and_fish: Dotfiles and scripts providing cumbersome configure details and other senseless stuff. 一些无聊的脚本和配置文件|149|Shell|2021/07/19|
|166|[cloudnativeto/community](https://github.com/cloudnativeto/community)|云原生社区资料库 - https://cloudnative.to/community/|148|Shell|2021/09/03|
|167|[Masterchiefm/Aria2Dash](https://github.com/Masterchiefm/Aria2Dash)|快速部署Aria2的脚本。具有剩余容量显示监控及显示功能。本脚本会一同安装文件管理器|143|Shell|2021/10/03|
|168|[qxzg/Actions](https://github.com/qxzg/Actions)|每日自动更新fancyss规则|142|Shell|2021/12/01|
|169|[SuperNG6/docker-aria2](https://github.com/SuperNG6/docker-aria2)|Docker Aria2的最佳实践|142|Shell|2021/10/25|
|170|[Senorsen/netease-cloud-music-rpm](https://github.com/Senorsen/netease-cloud-music-rpm)|网易云音乐 rpm 打包 Fedora Linux / openSUSE / etc.|142|Shell|2021/05/14|
|171|[riverscn/openwrt-iptvhelper](https://github.com/riverscn/openwrt-iptvhelper)|方便地使用 Openwrt 融合IPTV到家庭局域网。Watching IPTV with Openwrt becomes easy. |139|Shell|2021/08/27|
|172|[clion007/dnsmasq](https://github.com/clion007/dnsmasq)|全自动dnsmasq防DNS劫持及全面广告屏蔽脚本（ADSI），项目搬迁至coding，此处已停止维护！|136|Shell|2021/05/08|
|173|[kkkgo/DSM_Login_BingWallpaper](https://github.com/kkkgo/DSM_Login_BingWallpaper)|群晖登录壁纸自动换|136|Shell|2021/10/29|
|174|[sunpma/mtp](https://github.com/sunpma/mtp)|MTProxy TLS 绿色版一键安装脚本|135|Shell|2021/09/14|
|175|[esrrhs/xiaohuangji](https://github.com/esrrhs/xiaohuangji)|小黄鸡表情收集|135|Shell|2021/11/19|
|176|[VergilGao/docker-avdc](https://github.com/VergilGao/docker-avdc)|https://github.com/yoshiko2/AV_Data_Capture 的 docker镜像，帮助你更优雅的管理硬盘中的大姐姐们。|134|Shell|2021/09/27|
|177|[TomAPU/poc_and_exp](https://github.com/TomAPU/poc_and_exp)|搜集的或者自己写的poc或者exp|133|Shell|2021/08/31|
|178|[snow-sprite/rzsz](https://github.com/snow-sprite/rzsz)|lrzsz上传下载mac配置 及两个必要的.sh文件 iterm2-recv-zmodem.sh 和 iterm2-send-zmodem.sh|132|Shell|2021/05/13|
|179|[overmind1980/oeasypython](https://github.com/overmind1980/oeasypython)|面向初学者的简明易懂的 Python3 课程，对没有编程经验的同学也非常友好。在vim下从浅入深，逐步学习。|130|Shell|2021/11/09|
|180|[tossp/redpill-tool-chain](https://github.com/tossp/redpill-tool-chain)|这是一个测试项目，可能会有不可预测的事情发生（比如：毁损数据、烧毁硬件等等），请谨慎使用。|130|Shell|2021/11/27|
|181|[lgs3137/MR_S1-macOS](https://github.com/lgs3137/MR_S1-macOS)|机械革命S1（MSI PS42 8RB） for macOS Monterey & Big Sur|127|Shell|2021/11/06|
|182|[daliansky/Lenovo-Air13-IWL-Hackintosh](https://github.com/daliansky/Lenovo-Air13-IWL-Hackintosh)|联想小新Air 13 IWL笔记本EFI|127|Shell|2021/06/23|
|183|[gd0772/AutoBuild-OpenWrt](https://github.com/gd0772/AutoBuild-OpenWrt)|GitHub Actions 云编译 OpenWrt|118|Shell|2021/12/01|
|184|[idoop/zentao](https://github.com/idoop/zentao)|auto build docker image for zentao(禅道).|116|Shell|2021/09/28|
|185|[LovelyHaochi/StreamUnlockTest](https://github.com/LovelyHaochi/StreamUnlockTest)|沙雕流媒体测试|115|Shell|2021/08/18|
|186|[cornjosh/Aminer](https://github.com/cornjosh/Aminer)|⛏️ Mining with Android devices. 使用 Android 设备来挖矿.|113|Shell|2021/05/08|
|187|[rootsongjc/serverless-handbook](https://github.com/rootsongjc/serverless-handbook)|Serverless Handbook 无服务架构实践手册 - https://jimmysong.io/serverless-handbook|108|Shell|2021/10/28|
|188|[E7KMbb/UnblockNeteaseMusic_for_Magisk](https://github.com/E7KMbb/UnblockNeteaseMusic_for_Magisk)|这是用于Magisk的UnblockNeteaseMusic|106|Shell|2021/06/14|
|189|[hepyu/k8s-app-config](https://github.com/hepyu/k8s-app-config)|提供kubernetes容器化生产级实践，包含配置，参数，流程，架构等。|106|Shell|2021/06/15|
|190|[neroxps/hassio_install](https://github.com/neroxps/hassio_install)|hassio 一键脚本，适配国内网络环境目前兼容(Debian Ubuntu Raspbian)|104|Shell|2021/10/29|
|191|[aitlp/docker-v2ray](https://github.com/aitlp/docker-v2ray)|使用docker-compose以ws+tls方式一键部署v2ray。|103|Shell|2021/06/06|
|192|[heiyeluren/heiyeluren-tools](https://github.com/heiyeluren/heiyeluren-tools)|Heiyeluren Tools Kit  黑夜路人工具箱，包括构建百万并发高性能服务器配置脚本；全自动构建LNMP环境自动化脚本工作等|99|Shell|2021/07/13|
|193|[kepuna/archiveScript](https://github.com/kepuna/archiveScript)|iOS自动打包上传到Fir平台和Appstore的脚本文件|98|Shell|2021/06/13|
|194|[pzcn/MIUI-Adapted-Icons-Complement-Project](https://github.com/pzcn/MIUI-Adapted-Icons-Complement-Project)|MIUI 完美图标补全计划|98|Shell|2021/11/29|
|195|[CloudNativeIndustryAlliance/whitepaper2020](https://github.com/CloudNativeIndustryAlliance/whitepaper2020)|中国信息通信研究院(CAICT)云原生发展白皮书（2020）- https://cloudnativeindustryalliance.github.io/whitepaper2020/|97|Shell|2021/08/12|
|196|[doublechaintech/daas-start-kit](https://github.com/doublechaintech/daas-start-kit)|React/Java 技术栈，代码和数据云端生成，本地部署低代码平台，手写代码和生成代码目录分离，适合专业开发人员开发长期维护的软件。|96|Shell|2021/10/26|
|197|[stilleshan/rssforever](https://github.com/stilleshan/rssforever)|本项目为 Nginx + TTRSS + RSSHub 整合 docker 容器化快速一键部署方案.|96|Shell|2021/09/07|
|198|[zszdevelop/java-study-gitbook](https://github.com/zszdevelop/java-study-gitbook)|java学习gitbook笔记 http://java.isture.com|96|Shell|2021/11/25|
|199|[stilleshan/frps](https://github.com/stilleshan/frps)|基于原版 frp 内网穿透服务端 frps 的一键安装卸载脚本和 docker 镜像.支持 Linux 服务器和 docker 等多种环境安装部署.|96|Shell|2021/10/26|
|200|[RainbowEngineer/taiwan_love_wins](https://github.com/RainbowEngineer/taiwan_love_wins)|資訊界連署挺同婚|96|Shell|2021/05/16|

⬆ [回到目录](#内容目录)

<br/>

## Jupyter Notebook

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[MLEveryday/100-Days-Of-ML-Code](https://github.com/MLEveryday/100-Days-Of-ML-Code)|100-Days-Of-ML-Code中文版|16.7k|Jupyter Notebook|2021/08/11|
|2|[zergtant/pytorch-handbook](https://github.com/zergtant/pytorch-handbook)|pytorch handbook是一本开源的书籍，目标是帮助那些希望和使用PyTorch进行深度学习开发和研究的朋友快速入门，其中包含的Pytorch教程全部通过测试保证可以成功运行|15.7k|Jupyter Notebook|2021/10/25|
|3|[fengdu78/lihang-code](https://github.com/fengdu78/lihang-code)|《统计学习方法》的代码实现|14.9k|Jupyter Notebook|2021/05/31|
|4|[ShusenTang/Dive-into-DL-PyTorch](https://github.com/ShusenTang/Dive-into-DL-PyTorch)|本项目将《动手学深度学习》(Dive into Deep Learning)原书中的MXNet实现改为PyTorch实现。|14.1k|Jupyter Notebook|2021/10/14|
|5|[dragen1860/Deep-Learning-with-TensorFlow-book](https://github.com/dragen1860/Deep-Learning-with-TensorFlow-book)|深度学习入门开源书，基于TensorFlow 2.0案例实战。Open source Deep Learning book, based on TensorFlow 2.0 framework.|12.1k|Jupyter Notebook|2021/08/30|
|6|[NLP-LOVE/ML-NLP](https://github.com/NLP-LOVE/ML-NLP)|此项目是机器学习(Machine Learning)、深度学习(Deep Learning)、NLP面试中常考到的知识点和代码实现，也是作为一个算法工程师必会的理论基础知识。|10.7k|Jupyter Notebook|2021/06/26|
|7|[apachecn/Interview](https://github.com/apachecn/Interview)|Interview = 简历指南 + LeetCode + Kaggle|7.2k|Jupyter Notebook|2021/11/07|
|8|[Mikoto10032/DeepLearning](https://github.com/Mikoto10032/DeepLearning)|深度学习入门教程, 优秀文章, Deep Learning Tutorial|6.7k|Jupyter Notebook|2021/10/21|
|9|[fengdu78/Data-Science-Notes](https://github.com/fengdu78/Data-Science-Notes)|数据科学的笔记以及资料搜集|6.0k|Jupyter Notebook|2021/08/16|
|10|[xianhu/LearnPython](https://github.com/xianhu/LearnPython)|以撸代码的形式学习Python|6.0k|Jupyter Notebook|2021/11/11|
|11|[roboticcam/machine-learning-notes](https://github.com/roboticcam/machine-learning-notes)|My continuously updated Machine Learning, Probabilistic Models and Deep Learning notes and demos (2000+ slides)  我不间断更新的机器学习，概率模型和深度学习的讲义(2000+页)和视频链接|5.3k|Jupyter Notebook|2021/10/13|
|12|[Alfred1984/interesting-python](https://github.com/Alfred1984/interesting-python)|有趣的Python爬虫和Python数据分析小项目(Some interesting Python crawlers and data analysis projects)|3.9k|Jupyter Notebook|2021/07/06|
|13|[snowkylin/tensorflow-handbook](https://github.com/snowkylin/tensorflow-handbook)|简单粗暴 TensorFlow 2   A Concise Handbook of TensorFlow 2   一本简明的 TensorFlow 2 入门指导教程|3.6k|Jupyter Notebook|2021/09/04|
|14|[TrickyGo/Dive-into-DL-TensorFlow2.0](https://github.com/TrickyGo/Dive-into-DL-TensorFlow2.0)|本项目将《动手学深度学习》(Dive into Deep Learning)原书中的MXNet实现改为TensorFlow 2.0实现，项目已得到李沐老师的认可|3.3k|Jupyter Notebook|2021/08/31|
|15|[datawhalechina/easy-rl](https://github.com/datawhalechina/easy-rl)|强化学习中文教程，在线阅读地址：https://datawhalechina.github.io/easy-rl/|2.9k|Jupyter Notebook|2021/11/19|
|16|[datawhalechina/joyful-pandas](https://github.com/datawhalechina/joyful-pandas)|pandas中文教程|2.8k|Jupyter Notebook|2021/10/05|
|17|[PaddlePaddle/book](https://github.com/PaddlePaddle/book)|Deep Learning 101 with PaddlePaddle （『飞桨』深度学习框架入门教程）|2.6k|Jupyter Notebook|2021/11/13|
|18|[datawhalechina/competition-baseline](https://github.com/datawhalechina/competition-baseline)|数据科学竞赛知识、代码、思路|2.5k|Jupyter Notebook|2021/11/29|
|19|[wangshub/RL-Stock](https://github.com/wangshub/RL-Stock)|📈 如何用深度强化学习自动炒股|2.1k|Jupyter Notebook|2021/09/08|
|20|[szcf-weiya/ESL-CN](https://github.com/szcf-weiya/ESL-CN)|The Elements of Statistical Learning (ESL)的中文翻译、代码实现及其习题解答。|1.8k|Jupyter Notebook|2021/11/23|
|21|[wowchemy/starter-hugo-academic](https://github.com/wowchemy/starter-hugo-academic)|🎓 Hugo Academic Theme 创建一个学术网站. Easily create a beautiful academic résumé or educational website using Hugo, GitHub, and Netlify.|1.7k|Jupyter Notebook|2021/11/28|
|22|[FinMind/FinMind](https://github.com/FinMind/FinMind)|Open Data, more than 50 financial data. 提供超過 50 個金融資料(台股為主)，每天更新 https://finmind.github.io/|1.6k|Jupyter Notebook|2021/11/06|
|23|[jm199504/Financial-Knowledge-Graphs](https://github.com/jm199504/Financial-Knowledge-Graphs)|小型金融知识图谱构建流程|1.5k|Jupyter Notebook|2021/05/09|
|24|[xavier-zy/Awesome-pytorch-list-CNVersion](https://github.com/xavier-zy/Awesome-pytorch-list-CNVersion)|Awesome-pytorch-list 翻译工作进行中......|1.5k|Jupyter Notebook|2021/07/26|
|25|[Fafa-DL/Lhy_Machine_Learning](https://github.com/Fafa-DL/Lhy_Machine_Learning)|李宏毅2021春季机器学习课程课件及作业|1.3k|Jupyter Notebook|2021/06/23|
|26|[Charmve/computer-vision-in-action](https://github.com/Charmve/computer-vision-in-action)|《计算机视觉实战演练：算法与应用》中文电子书、源码、读者交流社区（持续更新中 ...） 📘 在线电子书 https://charmve.github.io/computer-vision-in-action/   👇项目主页|1.2k|Jupyter Notebook|2021/11/26|
|27|[PaddlePaddle/awesome-DeepLearning](https://github.com/PaddlePaddle/awesome-DeepLearning)|深度学习入门课、资深课、特色课、学术案例、产业实践案例、深度学习知识百科及面试题库The course, case and knowledge of Deep Learning and AI|1.0k|Jupyter Notebook|2021/11/30|
|28|[ben1234560/AiLearning-Theory-Applying](https://github.com/ben1234560/AiLearning-Theory-Applying)|快速上手Ai理论及应用实战：基础知识Basic knowledge、机器学习MachineLearning、深度学习DeepLearning2、自然语言处理BERT，持续更新中。含大量注释及数据集，力求每一位能看懂并复现。|968|Jupyter Notebook|2021/10/27|
|29|[huaweicloud/ModelArts-Lab](https://github.com/huaweicloud/ModelArts-Lab)|ModelArts-Lab是示例代码库。更多AI开发学习交流信息，请访问华为云AI开发者社区：huaweicloud.ai|878|Jupyter Notebook|2021/11/26|
|30|[datawhalechina/team-learning-data-mining](https://github.com/datawhalechina/team-learning-data-mining)|主要存储Datawhale组队学习中“数据挖掘/机器学习”方向的资料。|877|Jupyter Notebook|2021/11/28|
|31|[eastmountyxz/ImageProcessing-Python](https://github.com/eastmountyxz/ImageProcessing-Python)|该资源为作者在CSDN的撰写Python图像处理文章的支撑，主要是Python实现图像处理、图像识别、图像分类等算法代码实现，希望该资源对您有所帮助，一起加油。|785|Jupyter Notebook|2021/11/08|
|32|[fengdu78/WZU-machine-learning-course](https://github.com/fengdu78/WZU-machine-learning-course)|温州大学《机器学习》课程资料（代码、课件等）|741|Jupyter Notebook|2021/11/21|
|33|[lxztju/pytorch_classification](https://github.com/lxztju/pytorch_classification)|利用pytorch实现图像分类的一个完整的代码，训练，预测，TTA，模型融合，模型部署，cnn提取特征，svm或者随机森林等进行分类，模型蒸馏，一个完整的代码|716|Jupyter Notebook|2021/11/26|
|34|[wx-chevalier/AI-Series](https://github.com/wx-chevalier/AI-Series)|:books: [.md & .ipynb] Series of Artificial Intelligence & Deep Learning, including Mathematics Fundamentals, Python Practices, NLP Application, etc. 💫 人工智能与深度学习实战，数理统计篇   机器学习篇   深度学习篇   自然语言处理篇   工具实践 Scikit & Tensoflow & PyTorch 篇   行业应用 & 课程笔记|713|Jupyter Notebook|2021/11/24|
|35|[CNFeffery/DataScienceStudyNotes](https://github.com/CNFeffery/DataScienceStudyNotes)|这个仓库保管从（数据科学学习手札69）开始的所有代码、数据等相关附件内容|672|Jupyter Notebook|2021/11/21|
|36|[zhouyanasd/or-pandas](https://github.com/zhouyanasd/or-pandas)|【运筹OR帷幄 数据科学】pandas教程系列电子书|660|Jupyter Notebook|2021/10/17|
|37|[geektutu/interview-questions](https://github.com/geektutu/interview-questions)|机器学习/深度学习/Python/Go语言面试题笔试题(Machine learning Deep Learning Python and Golang Interview Questions)|647|Jupyter Notebook|2021/06/12|
|38|[fly51fly/Practical_Python_Programming](https://github.com/fly51fly/Practical_Python_Programming)|北邮《Python编程与实践》课程资料|634|Jupyter Notebook|2021/06/09|
|39|[MemorialCheng/deep-learning-from-scratch](https://github.com/MemorialCheng/deep-learning-from-scratch)|《深度学习入门-基于Python的理论与实现》，包含源代码和高清PDF(带书签)；慕课网imooc《深度学习之神经网络(CNN-RNN-GAN)算法原理-实战》；《菜菜的机器学习sklearn》|625|Jupyter Notebook|2021/11/03|
|40|[gengyanlei/fire-smoke-detect-yolov4](https://github.com/gengyanlei/fire-smoke-detect-yolov4)|fire-smoke-detect-yolov4-yolov5 and fire-smoke-detection-dataset 火灾检测，烟雾检测|572|Jupyter Notebook|2021/10/29|
|41|[DataXujing/YOLO-v5](https://github.com/DataXujing/YOLO-v5)|:art: Pytorch YOLO v5 训练自己的数据集超详细教程！！！ :art: (提供PDF训练教程下载）|571|Jupyter Notebook|2021/08/26|
|42|[LYuhang/GNN_Review](https://github.com/LYuhang/GNN_Review)|GNN综述阅读报告|540|Jupyter Notebook|2021/08/17|
|43|[datawhalechina/statistical-learning-method-solutions-manual](https://github.com/datawhalechina/statistical-learning-method-solutions-manual)|《统计学习方法》（第二版）习题解答，在线阅读地址：https://datawhalechina.github.io/statistical-learning-method-solutions-manual|540|Jupyter Notebook|2021/11/17|
|44|[shibing624/python-tutorial](https://github.com/shibing624/python-tutorial)|Python实用教程，包括：Python基础，Python高级特性，面向对象编程，多线程，数据库，数据科学，Flask，爬虫开发教程。|539|Jupyter Notebook|2021/11/05|
|45|[datawhalechina/team-learning-program](https://github.com/datawhalechina/team-learning-program)|主要存储Datawhale组队学习中“编程、数据结构与算法”方向的资料。|537|Jupyter Notebook|2021/12/01|
|46|[LinXueyuanStdio/LaTeX_OCR_PRO](https://github.com/LinXueyuanStdio/LaTeX_OCR_PRO)|:art: 数学公式识别增强版：中英文手写印刷公式、支持初级符号推导（数据结构基于 LaTeX 抽象语法树）|503|Jupyter Notebook|2021/11/24|
|47|[datawhalechina/hands-on-data-analysis](https://github.com/datawhalechina/hands-on-data-analysis)|动手学数据分析以项目为主线，知识点孕育其中，通过边学、边做、边引导来得到更好的学习效果|488|Jupyter Notebook|2021/09/09|
|48|[ChileWang0228/Deep-Learning-With-Python](https://github.com/ChileWang0228/Deep-Learning-With-Python)|《Python深度学习》书籍代码|486|Jupyter Notebook|2021/05/13|
|49|[zkywsg/Daily-DeepLearning](https://github.com/zkywsg/Daily-DeepLearning)|🔥机器学习/深度学习/Python/算法面试/自然语言处理教程/剑指offer/machine learning/deeplearning/Python/Algorithm interview/NLP Tutorial|441|Jupyter Notebook|2021/07/13|
|50|[Syencil/mobile-yolov5-pruning-distillation](https://github.com/Syencil/mobile-yolov5-pruning-distillation)|mobilev2-yolov5s剪枝、蒸馏，支持ncnn，tensorRT部署。ultra-light but better performence！|441|Jupyter Notebook|2021/07/10|
|51|[DjangoPeng/tensorflow-101](https://github.com/DjangoPeng/tensorflow-101)|《TensorFlow 快速入门与实战》和《TensorFlow 2 项目进阶实战》课程代码与课件|414|Jupyter Notebook|2021/05/07|
|52|[bobo0810/PytorchNetHub](https://github.com/bobo0810/PytorchNetHub)|项目注释+论文复现+算法竞赛+Pytorch指北|401|Jupyter Notebook|2021/11/05|
|53|[ga642381/ML2021-Spring](https://github.com/ga642381/ML2021-Spring)|**Official** 李宏毅 (Hung-yi Lee) 機器學習 Machine Learning 2021 Spring|389|Jupyter Notebook|2021/06/18|
|54|[isee15/Card-Ocr](https://github.com/isee15/Card-Ocr)|身份证识别OCR|372|Jupyter Notebook|2021/09/22|
|55|[datawhalechina/team-learning-nlp](https://github.com/datawhalechina/team-learning-nlp)|主要存储Datawhale组队学习中“自然语言处理”方向的资料。|367|Jupyter Notebook|2021/09/17|
|56|[wolfparticle/machineLearningDeepLearning](https://github.com/wolfparticle/machineLearningDeepLearning)|李宏毅2021机器学习深度学习笔记PPT作业|327|Jupyter Notebook|2021/06/14|
|57|[linguishi/chinese_sentiment](https://github.com/linguishi/chinese_sentiment)|中文情感分析，CNN，BI-LSTM，文本分类|319|Jupyter Notebook|2021/08/25|
|58|[SummerLife/EmbeddedSystem](https://github.com/SummerLife/EmbeddedSystem)|:books: 计算机体系架构、嵌入式系统基础与主流编程语言相关内容总结|317|Jupyter Notebook|2021/11/02|
|59|[LiuChuang0059/Complex-Network](https://github.com/LiuChuang0059/Complex-Network)|复杂网络研究资源整理和基础知识学习|310|Jupyter Notebook|2021/05/14|
|60|[qiguming/MLAPP_CN_CODE](https://github.com/qiguming/MLAPP_CN_CODE)|《Machine Learning: A Probabilistic Perspective》（Kevin P. Murphy）中文翻译和书中算法的Python实现。|302|Jupyter Notebook|2021/07/14|
|61|[zhongqiangwu960812/AI-RecommenderSystem](https://github.com/zhongqiangwu960812/AI-RecommenderSystem)|该仓库尝试整理推荐系统领域的一些经典算法模型|297|Jupyter Notebook|2021/10/09|
|62|[zhouwei713/data_analysis](https://github.com/zhouwei713/data_analysis)|一些爬虫和数据分析相关实战练习|297|Jupyter Notebook|2021/08/29|
|63|[liuhuanshuo/zaoqi-Python](https://github.com/liuhuanshuo/zaoqi-Python)|公众号：早起Python|296|Jupyter Notebook|2021/10/20|
|64|[derekhe/mobike-crawler](https://github.com/derekhe/mobike-crawler)|摩拜单车爬虫|262|Jupyter Notebook|2021/07/26|
|65|[LinXueyuanStdio/LaTeX_OCR](https://github.com/LinXueyuanStdio/LaTeX_OCR)|:gem: 数学公式识别 Math Formula OCR|252|Jupyter Notebook|2021/09/08|
|66|[LogicJake/competition_baselines](https://github.com/LogicJake/competition_baselines)|开源的各大比赛baseline|251|Jupyter Notebook|2021/11/23|
|67|[ni1o1/pygeo-tutorial](https://github.com/ni1o1/pygeo-tutorial)|Tutorial of geospatial data processing using python 用python分析时空数据的教程(in Chinese and English )|248|Jupyter Notebook|2021/11/17|
|68|[d2l-ai/d2l-zh-pytorch-slides](https://github.com/d2l-ai/d2l-zh-pytorch-slides)|Pytorch版代码幻灯片|242|Jupyter Notebook|2021/11/29|
|69|[wzy6642/Dive-Into-Deep-Learning-PyTorch-PDF](https://github.com/wzy6642/Dive-Into-Deep-Learning-PyTorch-PDF)|本项目对中文版《动手学深度学习》中的代码进行了PyTorch实现并整理为PDF版本供下载|238|Jupyter Notebook|2021/05/19|
|70|[jarodHAN/Python-100-Days-master](https://github.com/jarodHAN/Python-100-Days-master)|python100天学习资料|228|Jupyter Notebook|2021/06/02|
|71|[CUHKSZ-TQL/WeiboSpider_SentimentAnalysis](https://github.com/CUHKSZ-TQL/WeiboSpider_SentimentAnalysis)|借助Python抓取微博数据，并对抓取的数据进行情绪分析|226|Jupyter Notebook|2021/10/07|
|72|[d2l-ai/courses-zh-v2](https://github.com/d2l-ai/courses-zh-v2)|中文版 v2 课程|215|Jupyter Notebook|2021/09/14|
|73|[Mryangkaitong/python-Machine-learning](https://github.com/Mryangkaitong/python-Machine-learning)|机器学习算法项目|210|Jupyter Notebook|2021/10/15|
|74|[yutiansut/QAStrategy](https://github.com/yutiansut/QAStrategy)|策略基类/ 支持QIFI协议|200|Jupyter Notebook|2021/09/01|
|75|[datawhalechina/fantastic-matplotlib](https://github.com/datawhalechina/fantastic-matplotlib)|Matplotlib中文教程，在线阅读地址：https://datawhalechina.github.io/fantastic-matplotlib/|199|Jupyter Notebook|2021/08/09|
|76|[Relph1119/statistical-learning-method-camp](https://github.com/Relph1119/statistical-learning-method-camp)|统计学习方法训练营课程作业及答案，视频笔记在线阅读地址：https://relph1119.github.io/statistical-learning-method-camp|189|Jupyter Notebook|2021/09/08|
|77|[zwq2018/AI_UAV](https://github.com/zwq2018/AI_UAV)|在人工智能、机器视觉、高精度导航定位和多传感器融合等技术的助推下，众多行业迎来了前所未有的发展机遇，人工智能+无人机（AI+UAV）正是一个具有无限想象力的应用方向。|186|Jupyter Notebook|2021/10/13|
|78|[LemenChao/PythonFromDAToDS](https://github.com/LemenChao/PythonFromDAToDS)|图书《Python编程：从数据分析到数据科学》的配套资源|184|Jupyter Notebook|2021/10/10|
|79|[gengyanlei/reflective-clothes-detect-yolov5](https://github.com/gengyanlei/reflective-clothes-detect-yolov5)|reflective-clothes-detect-dataset、helemet detection yolov5、工作服(反光衣)检测数据集、安全帽检测、施工人员穿戴检测|184|Jupyter Notebook|2021/07/02|
|80|[brain-zhang/xianglong](https://github.com/brain-zhang/xianglong)|资产配置方案|174|Jupyter Notebook|2021/09/07|
|81|[datamonday/Time-Series-Analysis-Tutorial](https://github.com/datamonday/Time-Series-Analysis-Tutorial)|时间序列分析教程|173|Jupyter Notebook|2021/06/02|
|82|[44670/SourceHanSans-Pixel](https://github.com/44670/SourceHanSans-Pixel)|基于思源黑体的开源像素字体|169|Jupyter Notebook|2021/08/20|
|83|[datawhalechina/team-learning-cv](https://github.com/datawhalechina/team-learning-cv)|主要存储Datawhale组队学习中“计算机视觉”方向的资料。|164|Jupyter Notebook|2021/09/06|
|84|[kingname/SourceCodeofMongoRedis](https://github.com/kingname/SourceCodeofMongoRedis)|《左手MongoDB，右手Redis——从入门到商业实战》书籍配套源代码。|163|Jupyter Notebook|2021/08/19|
|85|[fire717/Machine-Learning](https://github.com/fire717/Machine-Learning)|机器学习&深度学习资料笔记&基本算法实现&资源整理（ML / CV / NLP / DM...）|163|Jupyter Notebook|2021/11/16|
|86|[dengxiuqi/WeiboSentiment](https://github.com/dengxiuqi/WeiboSentiment)|基于各种机器学习和深度学习的中文微博情感分析|160|Jupyter Notebook|2021/06/07|
|87|[huangtinglin/Linear-Algebra-and-Its-Applications-notes](https://github.com/huangtinglin/Linear-Algebra-and-Its-Applications-notes)|《线性代数及其应用》笔记|152|Jupyter Notebook|2021/09/17|
|88|[duoergun0729/adversarial_examples](https://github.com/duoergun0729/adversarial_examples)|对抗样本|150|Jupyter Notebook|2021/10/28|
|89|[liuhuanshuo/zaoqi-data](https://github.com/liuhuanshuo/zaoqi-data)|公众号：可视化图鉴|139|Jupyter Notebook|2021/05/17|
|90|[makelove/Programer_Log](https://github.com/makelove/Programer_Log)|最新动态在这里【我的程序员日志】|139|Jupyter Notebook|2021/09/03|
|91|[liuhuanshuo/Pandas_Advanced_Exercise](https://github.com/liuhuanshuo/Pandas_Advanced_Exercise)|Pandas进阶修炼300题|137|Jupyter Notebook|2021/09/22|
|92|[yenlung/Deep-Learning-MOOC](https://github.com/yenlung/Deep-Learning-MOOC)|這是我在政治大學開設 Deep Learning MOOC 教學的相關檔案。|134|Jupyter Notebook|2021/07/09|
|93|[aipredict/ai-deployment](https://github.com/aipredict/ai-deployment)|关注AI模型上线、模型部署|128|Jupyter Notebook|2021/07/15|
|94|[hugo2046/Quantitative-analysis](https://github.com/hugo2046/Quantitative-analysis)|量化研究-券商金工研报复现|127|Jupyter Notebook|2021/10/29|
|95|[miracleyoo/pytorch-lightning-template](https://github.com/miracleyoo/pytorch-lightning-template)|An easy/swift-to-adapt PyTorch-Lighting template. 套壳模板，简单易用，稍改原来Pytorch代码，即可适配Lightning。You can translate your previous Pytorch code much easier using this template, and keep your freedom to edit all the functions as well. Big-project-friendly as well.|125|Jupyter Notebook|2021/05/15|
|96|[chansonZ/book-ml-sem](https://github.com/chansonZ/book-ml-sem)|《机器学习：软件工程方法与实现》Method and implementation of machine learning software engineering|122|Jupyter Notebook|2021/11/29|
|97|[nmcdev/meteva](https://github.com/nmcdev/meteva)|提供气象产品检验相关程序|122|Jupyter Notebook|2021/11/26|
|98|[fancyerii/deep_learning_theory_and_practice](https://github.com/fancyerii/deep_learning_theory_and_practice)|《深度学习理论与实战：基础篇》代码|118|Jupyter Notebook|2021/06/08|
|99|[beiciliang/intro2musictech](https://github.com/beiciliang/intro2musictech)|公众号“无痛入门音乐科技”开源代码|117|Jupyter Notebook|2021/11/01|
|100|[JackonYang/paper-reading](https://github.com/JackonYang/paper-reading)|比做算法的懂工程落地，比做工程的懂算法模型。|116|Jupyter Notebook|2021/09/08|
|101|[azy1988/ML-CV](https://github.com/azy1988/ML-CV)|机器学习实战|108|Jupyter Notebook|2021/09/08|
|102|[excelsimon/AI](https://github.com/excelsimon/AI)|机器学习、深度学习、自然语言处理、计算机视觉等AI领域相关技术的算法推导及应用|104|Jupyter Notebook|2021/08/05|
|103|[oldratlee/software-practice-thoughts](https://github.com/oldratlee/software-practice-thoughts)|📚 🐣 软件实践文集。主题不限，思考讨论有趣有料就好，包含如 系统的模型分析/量化分析、开源漫游者指南、软件可靠性设计实践…… 🥤|103|Jupyter Notebook|2021/11/11|
|104|[Estom/notes](https://github.com/Estom/notes)|一个码农的毕生所学.考研,就业,上学.语言篇，Android,C++,Java,JavaScript,Latex,MATLAB,NodeJS,PHP,Python,技术篇,docker,git,Linux,Maven,office,Spark,Spring,SVN,基础篇,编译原理,操作系统,单片机,计算机网络,计算机网络实验,架构模式,软件文档写作,设计模式,数据结构,数据库,算法,UML建模,Windows程序设计,数学篇,概率论与数理统计,微积分,线性代数,张量,机器学习篇,机器学习,pytorch,sklearn,TensorFlow|96|Jupyter Notebook|2021/05/28|
|105|[0809zheng/CS231n-assignment2019](https://github.com/0809zheng/CS231n-assignment2019)|CS231n 2019年春季学期课程作业|95|Jupyter Notebook|2021/11/09|
|106|[dsh0416/quantum-i-ching](https://github.com/dsh0416/quantum-i-ching)|A Quantum 爻 System Implementation for Divination |93|Jupyter Notebook|2021/05/18|
|107|[davidfrz/yolov5_distance_count](https://github.com/davidfrz/yolov5_distance_count)|使用yolov5，双目摄像头进行测距|93|Jupyter Notebook|2021/07/16|
|108|[chenghuige/pikachu2](https://github.com/chenghuige/pikachu2)|微信大数据2021 1st，qq浏览器2021 3rd，mind新闻推荐2020 1st，NAIC2020 AI+遥感影像 2nd|90|Jupyter Notebook|2021/11/17|
|109|[kevinfu1717/SuperInterstellarTerminal](https://github.com/kevinfu1717/SuperInterstellarTerminal)|【X世纪星际终端】A Wechat Social and AR Game: 基于微信聊天，结合增强现实技术AR+LBS（基于图像位置）的轻社交星际漂流瓶游戏。向外太空发送漂流信息，看看AI预测的外星人是长什么样的，寻找身边的外星人,逗逗外星生物，看看外星植物及外星建筑。Send the message to the outer space, find the aliens in the earth. Let`s see what they look like from LSGAN`s prediction. Also, Have a look  at the aliens' pets and  ...|89|Jupyter Notebook|2021/10/13|
|110|[Divsigma/2020-cs213n](https://github.com/Divsigma/2020-cs213n)|一些公开课的笔记及作业|87|Jupyter Notebook|2021/11/13|
|111|[xuwening/blog](https://github.com/xuwening/blog)|对过往做做总结|87|Jupyter Notebook|2021/09/16|
|112|[sherlcok314159/ML](https://github.com/sherlcok314159/ML)|此仓库将介绍Deep Learning 所需要的基础知识以及NLP方面的模型原理到项目实操 : )|83|Jupyter Notebook|2021/11/19|
|113|[PaddlePaddle/InterpretDL](https://github.com/PaddlePaddle/InterpretDL)|InterpretDL: Interpretation of Deep Learning Models，基于『飞桨』的模型可解释性算法库。https://interpretdl.readthedocs.io/en/latest/index.html|82|Jupyter Notebook|2021/11/17|
|114|[datamonday/Face-Recognition-Class-Attendance-System](https://github.com/datamonday/Face-Recognition-Class-Attendance-System)|基于人脸识别的课堂考勤系统v2.0|82|Jupyter Notebook|2021/08/10|
|115|[batermj/data_sciences_campaign](https://github.com/batermj/data_sciences_campaign)|【数据科学家系列课程】|82|Jupyter Notebook|2021/11/30|
|116|[zhuyuanxiang/NLTK-Python-CN](https://github.com/zhuyuanxiang/NLTK-Python-CN)|创建《Python自然语言处理》学习代码的中文注释版本。|81|Jupyter Notebook|2021/05/21|
|117|[andy6804tw/2020-12th-ironman](https://github.com/andy6804tw/2020-12th-ironman)|[全民瘋AI系列] 第12屆iT邦幫忙鐵人賽 影片教學組 |80|Jupyter Notebook|2021/09/27|
|118|[ZitongLu1996/Python-EEG-Handbook](https://github.com/ZitongLu1996/Python-EEG-Handbook)|Python脑电数据处理中文手册 - A Chinese handbook for EEG data analysis based on Python|79|Jupyter Notebook|2021/09/23|
|119|[chokkan/mlnote](https://github.com/chokkan/mlnote)|機械学習帳|79|Jupyter Notebook|2021/11/29|
|120|[zhangzhiqiangccm/NLP-project](https://github.com/zhangzhiqiangccm/NLP-project)|自然语言处理中的基础任务，包含但不限于文本表示，文本分类，命名实体识别，关系抽取，文本生成，文本摘要等，基于tensorflow2或Pytorch，所有代码均经过测试，项目中也包含相关数据。|77|Jupyter Notebook|2021/11/02|
|121|[feng-li/Distributed-Statistical-Computing](https://github.com/feng-li/Distributed-Statistical-Computing)|Teaching Materials for Distributed Statistical Computing (大数据分布式计算教学材料)|77|Jupyter Notebook|2021/11/21|
|122|[datawhalechina/machine-learning-toy-code](https://github.com/datawhalechina/machine-learning-toy-code)|《机器学习》（西瓜书）代码实战|74|Jupyter Notebook|2021/11/08|
|123|[China-ChallengeHub/ChallengeHub-Baselines](https://github.com/China-ChallengeHub/ChallengeHub-Baselines)|ChallengeHub开源的各大比赛baseline集合|73|Jupyter Notebook|2021/09/24|
|124|[1165048017/BlogLearning](https://github.com/1165048017/BlogLearning)|自己的学习历程，重点包括各种好玩的图像处理算法、运动捕捉、机器学习|67|Jupyter Notebook|2021/10/26|
|125|[ZhiningLiu1998/mesa](https://github.com/ZhiningLiu1998/mesa)|NeurIPS’20   Build powerful ensemble class-imbalanced learning models via meta-knowledge-powered resampler.   设计元知识驱动的采样器解决类别不平衡问题|62|Jupyter Notebook|2021/08/19|
|126|[xinychen/latex-cookbook](https://github.com/xinychen/latex-cookbook)|LaTeX论文写作教程 (中文版)|62|Jupyter Notebook|2021/11/12|
|127|[shibing624/nlp-tutorial](https://github.com/shibing624/nlp-tutorial)|自然语言处理（NLP）教程，包括：词向量，词法分析，预训练语言模型，文本分类，文本语义匹配，信息抽取，翻译，对话。|62|Jupyter Notebook|2021/10/21|
|128|[shiyanlou/louplus-dm](https://github.com/shiyanlou/louplus-dm)|实验楼 《楼+ 数据分析与挖掘实战》课程挑战作业参考答案|60|Jupyter Notebook|2021/08/16|
|129|[dota2heqiuzhi/dota2_data_analysis_tutorial](https://github.com/dota2heqiuzhi/dota2_data_analysis_tutorial)|《数据分析入门课程》配套代码|57|Jupyter Notebook|2021/11/29|
|130|[zhangjunhd/reading-notes](https://github.com/zhangjunhd/reading-notes)|张俊的读书笔记|57|Jupyter Notebook|2021/11/25|
|131|[CNFeffery/FefferyViz](https://github.com/CNFeffery/FefferyViz)|这个仓库存放（在模仿中精进数据可视化）系列文章代码及数据附件内容|56|Jupyter Notebook|2021/06/29|
|132|[vvlink/SIoT](https://github.com/vvlink/SIoT)|SIoT为一个为中小学STEM教育定制的跨平台的开源MQTT服务器程序，S指科学（Science）、简单（Simple）的意思。SIoT支持Win10、Win7、Mac、Linux等操作系统，支持虚谷号、树莓派等迷你电脑，一键启动，无需注册和设置即可使用。|56|Jupyter Notebook|2021/05/29|
|133|[HuangCongQing/3D-Point-Clouds](https://github.com/HuangCongQing/3D-Point-Clouds)|🔥3D点云目标检测&语义分割-SOTA方法,代码,论文,数据集等|54|Jupyter Notebook|2021/10/13|
|134|[aialgorithm/Blog](https://github.com/aialgorithm/Blog)|Python机器学习算法技术博客，有原创干货！有code实践！ |54|Jupyter Notebook|2021/11/27|
|135|[JuliaCN/MeetUpMaterials](https://github.com/JuliaCN/MeetUpMaterials)|Julia中文社区活动的各种材料 Meetup Materials |54|Jupyter Notebook|2021/05/01|
|136|[heucoder/ML-DL_book](https://github.com/heucoder/ML-DL_book)|机器学习、深度学习一些个人认为不错的书籍。|53|Jupyter Notebook|2021/11/09|
|137|[jm199504/Financial-Time-Series](https://github.com/jm199504/Financial-Time-Series)|金融数据预测分析（Random Forest; LSTM; CNN）|52|Jupyter Notebook|2021/05/27|
|138|[xiaoxiaoyao/MyApp](https://github.com/xiaoxiaoyao/MyApp)|随便写的各种，点链接可以进入我的知乎|50|Jupyter Notebook|2021/11/19|
|139|[cumtcssuld/RSP_of_CUMTCS](https://github.com/cumtcssuld/RSP_of_CUMTCS)|【矿大计算机学院资源共享计划（Resource SharingPlan of CUMTCS）】本仓库由矿大计算机学院学生会学习部牵头维护，由计算机学院全体同学共建共享。欢迎大家积极的参加到本资源库的建设中来吧！（每当有重大更新，我们都会将整个库克隆到码云，点击下边链接，到我们的码云仓库可以获得更好的下载体验）|49|Jupyter Notebook|2021/11/28|
|140|[OUCTheoryGroup/colab_demo](https://github.com/OUCTheoryGroup/colab_demo)|中国海洋大学视觉实验室前沿理论小组 pytorch 学习|48|Jupyter Notebook|2021/10/16|
|141|[wmpscc/CNN-Series-Getting-Started-and-PyTorch-Implementation](https://github.com/wmpscc/CNN-Series-Getting-Started-and-PyTorch-Implementation)|我的笔记和Demo，包含分类，检测、分割、知识蒸馏。|48|Jupyter Notebook|2021/10/27|
|142|[hiDaDeng/DaDengAndHisPython](https://github.com/hiDaDeng/DaDengAndHisPython)|【微信公众号：大邓和他的python】,    Python语法快速入门https://www.bilibili.com/video/av44384851     Python网络爬虫快速入门https://www.bilibili.com/video/av72010301, 我的联系邮箱thunderhit@qq.com|45|Jupyter Notebook|2021/11/25|
|143|[zipzou/captcha-recognition](https://github.com/zipzou/captcha-recognition)|验证码OCR识别|43|Jupyter Notebook|2021/09/08|
|144|[zhiyu1998/Python-Basis-Notes](https://github.com/zhiyu1998/Python-Basis-Notes)|一份包含了Python基础学习需要的知识框架 :snake: + 爬虫基础 :spider: + numpy基础 :bar_chart: + pandas基础 :panda_face:|43|Jupyter Notebook|2021/10/23|
|145|[SocratesAcademy/css](https://github.com/SocratesAcademy/css)|《计算社会科学》课程|42|Jupyter Notebook|2021/09/11|
|146|[BrambleXu/KGQA_SG](https://github.com/BrambleXu/KGQA_SG)|基于知识图谱的《三国演义》人物关系可视化及问答系统|42|Jupyter Notebook|2021/06/02|
|147|[zengwb-lx/yolov5-deepsort-pedestrian-counting](https://github.com/zengwb-lx/yolov5-deepsort-pedestrian-counting)|yolov5 + deepsort实现了行人计数功能， 统计摄像头内出现过的总人数，以及对穿越自定义黄线行人计数效果如下|42|Jupyter Notebook|2021/09/04|
|148|[chinapnr/python_study](https://github.com/chinapnr/python_study)|python 入门培训教材，实用、快速、清晰|42|Jupyter Notebook|2021/06/24|
|149|[skywateryang/timeseries101](https://github.com/skywateryang/timeseries101)|本教程独立网站已上线|42|Jupyter Notebook|2021/08/29|
|150|[Sharpiless/yolov5-distillation-5.0](https://github.com/Sharpiless/yolov5-distillation-5.0)|yolov5 5.0 version distillation    yolov5 5.0版本知识蒸馏，yolov5l >> yolov5s|41|Jupyter Notebook|2021/07/29|
|151|[wowchemy/hugo-blog-theme](https://github.com/wowchemy/hugo-blog-theme)|📝 Hugo Academic Blog Theme. 轻松创建一个简约博客. No code, highly customizable using widgets.|41|Jupyter Notebook|2021/11/24|
|152|[makelove/True_Artificial_Intelligence](https://github.com/makelove/True_Artificial_Intelligence)|真AI人工智能|39|Jupyter Notebook|2021/10/07|
|153|[Valuebai/Text-Auto-Summarization](https://github.com/Valuebai/Text-Auto-Summarization)|文本自动摘要|38|Jupyter Notebook|2021/09/08|
|154|[fly51fly/Principle_of_Web_Search_2021](https://github.com/fly51fly/Principle_of_Web_Search_2021)|北邮《网络搜索引擎原理》课程(2021)|38|Jupyter Notebook|2021/11/05|
|155|[oubindo/cs231n-cnn](https://github.com/oubindo/cs231n-cnn)|斯坦福的cs231n课程的assignments，非常好的课程，在这里也要强推|38|Jupyter Notebook|2021/09/08|
|156|[RuifMaxx/Multidimensional-time-series-with-transformer](https://github.com/RuifMaxx/Multidimensional-time-series-with-transformer)|transformer/self-attention for Multidimensional time series forecasting 使用transformer架构实现多维时间预测|37|Jupyter Notebook|2021/08/10|
|157|[howie6879/pylab](https://github.com/howie6879/pylab)|和Python相关的学习笔记：机器学习、算法、进阶书籍、文档，博客地址：https://www.howie6879.cn|37|Jupyter Notebook|2021/11/24|
|158|[PolarisRisingWar/cs224w-2021-winter-colab](https://github.com/PolarisRisingWar/cs224w-2021-winter-colab)|cs224w（图机器学习）2021冬季课程的colab|37|Jupyter Notebook|2021/07/09|
|159|[IBBD/IBBD.github.io](https://github.com/IBBD/IBBD.github.io)|IBBD技术博客|35|Jupyter Notebook|2021/07/17|
|160|[ZhangXinNan/DL-with-Python-and-PyTorch](https://github.com/ZhangXinNan/DL-with-Python-and-PyTorch)|《Python深度学习基于PyTorch》 Deep Learning with Python and PyTorch 作者：吴茂贵 郁明敏 杨本法 李涛 张粤磊 等|35|Jupyter Notebook|2021/08/08|
|161|[xiaohuiduan/dcgan_anime_avatars](https://github.com/xiaohuiduan/dcgan_anime_avatars)|基于keras使用dcgan自动生成动漫头像|35|Jupyter Notebook|2021/11/10|
|162|[lukewys/SunXiaoChuan-spider](https://github.com/lukewys/SunXiaoChuan-spider)|A scrapper and analyze result on weibos of SunXiaoChuan. 孙笑川微博的爬虫与分析|34|Jupyter Notebook|2021/10/07|
|163|[BohriumKwong/Deep_learning_in_WSI](https://github.com/BohriumKwong/Deep_learning_in_WSI)|将深度学习用于病理图像分析以及Openslide和OpenCV使用入門資料|34|Jupyter Notebook|2021/10/13|
|164|[liangyimingcom/Use-SageMaker_XGBoost-convert-Time-Series-into-Supervised-Learning-for-predictive-maintenance](https://github.com/liangyimingcom/Use-SageMaker_XGBoost-convert-Time-Series-into-Supervised-Learning-for-predictive-maintenance)|使用SageMaker+XGBoost，将时间序列转换为监督学习，完成预测性维护的实践|33|Jupyter Notebook|2021/05/09|
|165|[johnnychen94/Julia_and_its_applications](https://github.com/johnnychen94/Julia_and_its_applications)|2021 年《Julia 语言及其应用》系列讲座的材料|32|Jupyter Notebook|2021/11/22|
|166|[wine99/hfut-cs-assignments](https://github.com/wine99/hfut-cs-assignments)|合肥工业大学 计算机科学与技术专业 专业课作业和实验（大二下 - 大三下）|32|Jupyter Notebook|2021/10/24|
|167|[HuichuanLI/Recommand-Algorithme](https://github.com/HuichuanLI/Recommand-Algorithme)|推荐算法实战(Recommend algorithm)|32|Jupyter Notebook|2021/10/31|
|168|[WHUFT/WHU_FinTech_Workshop](https://github.com/WHUFT/WHU_FinTech_Workshop)|武汉大学金融科技研讨班|32|Jupyter Notebook|2021/11/30|
|169|[cador/Python_Predict_Analysis_Algorithm_Book_Codes](https://github.com/cador/Python_Predict_Analysis_Algorithm_Book_Codes)|《Python预测之美：数据分析与算法实战》书籍代码维护|32|Jupyter Notebook|2021/08/26|
|170|[cacolola/python-LEC](https://github.com/cacolola/python-LEC)|python数据分析基础|31|Jupyter Notebook|2021/09/14|
|171|[zhangqizky/ManTra_Net_Test_Demo](https://github.com/zhangqizky/ManTra_Net_Test_Demo)|🌹2019年CVPR论文：ManTra-Net: Manipulation Tracing Network For Detection And Localization of Image Forgeries With Anomalous Features |30|Jupyter Notebook|2021/08/25|
|172|[big-bombom/Python-100-Days](https://github.com/big-bombom/Python-100-Days)|python100天从新手到大师|30|Jupyter Notebook|2021/06/02|
|173|[zillionare/AI-trading-tutorial](https://github.com/zillionare/AI-trading-tutorial)|实战AI量化交易|30|Jupyter Notebook|2021/05/17|
|174|[sunnyswag/StockRL](https://github.com/sunnyswag/StockRL)|在A股(股票)市场上训练强化学习交易智能体|30|Jupyter Notebook|2021/08/28|
|175|[01ly/Codes](https://github.com/01ly/Codes)|不可能不会系列|28|Jupyter Notebook|2021/09/08|
|176|[xieliaing/Data_Science_Industrial_Practice](https://github.com/xieliaing/Data_Science_Industrial_Practice)|《数据科学工程实践》一书的Jupyter Notebook库，以及交流园地。|28|Jupyter Notebook|2021/06/18|
|177|[BrikerMan/classic_chinese_punctuate](https://github.com/BrikerMan/classic_chinese_punctuate)|classic Chinese punctuate experiment with keras using daizhige(殆知阁古代文献藏书) dataset|28|Jupyter Notebook|2021/08/25|
|178|[jinhualee/datashine](https://github.com/jinhualee/datashine)|《Python统计与数据分析实战》课程代码，包含了大部分统计与非参数统计和数据分析的模型、算法。回归分析、方差分析、点估计、假设检验、主成分分析、因子分析、聚类分析、判别分析、对数线性模型、分位回归模型以及列联表分析、非参数平滑、非参数密度估计等各种非参数统计方法。|27|Jupyter Notebook|2021/08/16|
|179|[sijichun/PythonTutor](https://github.com/sijichun/PythonTutor)|Python教学|27|Jupyter Notebook|2021/06/18|
|180|[ni1o1/plot_map](https://github.com/ni1o1/plot_map)|Python时空大数据处理工具包，可实现：matplotlib地图底图加载、坐标转换距离计算、栅格渔网划分与对应、点与点、点与线最近邻匹配|27|Jupyter Notebook|2021/10/21|
|181|[yenlung/Deep-Learning-Basics](https://github.com/yenlung/Deep-Learning-Basics)|使用 TensorFlow 2 的 Deep Learning 基本程式寫法示範。|26|Jupyter Notebook|2021/11/06|
|182|[lures2019/lures2020-demos](https://github.com/lures2019/lures2020-demos)|lures2020年写的一些项目和程序|26|Jupyter Notebook|2021/09/11|
|183|[QikaiXu/Recommender-System-Pytorch](https://github.com/QikaiXu/Recommender-System-Pytorch)|基于 Pytorch 实现推荐系统相关的算法|26|Jupyter Notebook|2021/11/17|
|184|[victorgau/MultiStrategies](https://github.com/victorgau/MultiStrategies)|多策略回測比較|25|Jupyter Notebook|2021/06/02|
|185|[ljyslyc/Book-KnowledgeGraph-Recommendation](https://github.com/ljyslyc/Book-KnowledgeGraph-Recommendation)|书籍知识图谱推荐系统|25|Jupyter Notebook|2021/07/21|
|186|[datawhalechina/learn-python-the-smart-way](https://github.com/datawhalechina/learn-python-the-smart-way)|聪明方法学Python，简明且系统的 Python 入门教程。|25|Jupyter Notebook|2021/11/14|
|187|[voidful/Phraseg](https://github.com/voidful/Phraseg)|Phraseg - 一言：新詞發現工具包|24|Jupyter Notebook|2021/11/30|
|188|[PanJinquan/python-learning-notes](https://github.com/PanJinquan/python-learning-notes)|代码|24|Jupyter Notebook|2021/10/13|
|189|[aialgorithm/AiPy](https://github.com/aialgorithm/AiPy)|Python、机器学习、深度学习算法开发等学习资源分享|24|Jupyter Notebook|2021/11/29|
|190|[HeXavi8/Mathematical-Modeling](https://github.com/HeXavi8/Mathematical-Modeling)|A sharing of the learning process of mathematical modeling 数学建模常用工具模型算法分享：数学建模竞赛优秀论文，数学建模常用算法模型，LaTeX论文模板，SPSS工具分享。|24|Jupyter Notebook|2021/11/20|
|191|[wolf-bailang/AI-Projects](https://github.com/wolf-bailang/AI-Projects)|AI项目（强化学习、深度学习、计算机视觉、推荐系统、自然语言处理、机器导航、医学影像处理）|23|Jupyter Notebook|2021/10/13|
|192|[lotapp/BaseCode](https://github.com/lotapp/BaseCode)|即是代码练习库，也是文章案例库（NetCore、Python、Node.js、Go）公众号：逸鹏说道|23|Jupyter Notebook|2021/05/26|
|193|[linbang/Metapath2vec](https://github.com/linbang/Metapath2vec)|使用DGL和pytorch实现metapath2vec|22|Jupyter Notebook|2021/09/17|
|194|[hzcforever/Something](https://github.com/hzcforever/Something)|面试知识点 + 笔试刷题总结。|22|Jupyter Notebook|2021/09/15|
|195|[damaainan/python100days](https://github.com/damaainan/python100days)|来源于 Python - 100天从新手到大师|22|Jupyter Notebook|2021/06/02|
|196|[anxiang1836/query_similar_tianchi_2020](https://github.com/anxiang1836/query_similar_tianchi_2020)|天池2020-新冠疫情相似句对判定大赛|22|Jupyter Notebook|2021/08/25|
|197|[tsaac/Python](https://github.com/tsaac/Python)|YINUXY的python脚本分享|22|Jupyter Notebook|2021/09/20|
|198|[yutiansut/QIFIAccount](https://github.com/yutiansut/QIFIAccount)|QIFI协议下的Account实现|21|Jupyter Notebook|2021/09/03|
|199|[wyfish/python-100-days](https://github.com/wyfish/python-100-days)|Python进阶100天|21|Jupyter Notebook|2021/06/02|
|200|[HuichuanLI/play-with-graph-algorithme](https://github.com/HuichuanLI/play-with-graph-algorithme)|python实现的初级图论算法库:环检测问题,桥和割点,最小生成树,最短路径,欧拉路径,哈密尔顿路径,拓扑排序，最大流问题，匹配问题(匈牙利算法)|21|Jupyter Notebook|2021/09/17|

⬆ [回到目录](#内容目录)

<br/>

## CSS

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[SwiftGGTeam/the-swift-programming-language-in-chinese](https://github.com/SwiftGGTeam/the-swift-programming-language-in-chinese)|中文版 Apple 官方 Swift 教程《The Swift Programming Language》|20.4k|CSS|2021/11/24|
|2|[chokcoco/iCSS](https://github.com/chokcoco/iCSS)|不止于 CSS|11.5k|CSS|2021/11/29|
|3|[chokcoco/CSS-Inspiration](https://github.com/chokcoco/CSS-Inspiration)|CSS Inspiration，在这里找到写 CSS 的灵感！|7.5k|CSS|2021/10/22|
|4|[eastlakeside/interpy-zh](https://github.com/eastlakeside/interpy-zh)|📘《Python进阶》（Intermediate Python 中文版）|5.9k|CSS|2021/08/22|
|5|[DMQ/mvvm](https://github.com/DMQ/mvvm)|剖析vue实现原理，自己动手实现mvvm|5.0k|CSS|2021/06/08|
|6|[Wei-Xia/most-frequent-technology-english-words](https://github.com/Wei-Xia/most-frequent-technology-english-words)|程序员工作中常见的英语词汇|4.7k|CSS|2021/11/19|
|7|[apachecn/sklearn-doc-zh](https://github.com/apachecn/sklearn-doc-zh)|:book: [译] scikit-learn（sklearn） 中文文档|4.5k|CSS|2021/10/14|
|8|[LearnOpenGL-CN/LearnOpenGL-CN](https://github.com/LearnOpenGL-CN/LearnOpenGL-CN)|http://learnopengl.com 系列教程的简体中文翻译|3.7k|CSS|2021/11/24|
|9|[apachecn/hands-on-ml-zh](https://github.com/apachecn/hands-on-ml-zh)|:book: [译] Sklearn 与 TensorFlow 机器学习实用指南【版权问题，网站已下线！！】|3.6k|CSS|2021/08/09|
|10|[ronggang/transmission-web-control](https://github.com/ronggang/transmission-web-control)|一个 Transmission 浏览器管理界面。Transmission Web Control is a custom web UI.|3.5k|CSS|2021/11/22|
|11|[hehonghui/the-economist-ebooks](https://github.com/hehonghui/the-economist-ebooks)|经济学人(含音频)、纽约客、自然、新科学人、卫报、科学美国人、连线、大西洋月刊、新闻周刊、国家地理等英语杂志免费下载、订阅(kindle推送),支持epub、mobi、pdf格式, 每周更新. The Economist 、The New Yorker 、Nature、The Atlantic 、New Scientist、The Guardian、Scientific American、Wired、Newsweek magazines, free download and subscription for kindle, mobi、epub、pdf format. |3.3k|CSS|2021/11/30|
|12|[LiangJunrong/document-library](https://github.com/LiangJunrong/document-library)|jsliang 的文档库. 里面包含了个人撰写的所有前端文章，例如 Vue、React,、ECharts、微信小程序、算法、数据结构等……|3.3k|CSS|2021/11/30|
|13|[QiShaoXuan/css_tricks](https://github.com/QiShaoXuan/css_tricks)|Some CSS tricks,一些 CSS 常用样式|3.1k|CSS|2021/05/27|
|14|[WebStackPage/WebStackPage.github.io](https://github.com/WebStackPage/WebStackPage.github.io)|❤️静态响应式网址导航网站 - webstack.cc|3.0k|CSS|2021/11/26|
|15|[billie66/TLCL](https://github.com/billie66/TLCL)|《快乐的 Linux 命令行》|2.9k|CSS|2021/10/07|
|16|[jukanntenn/django-blog-tutorial](https://github.com/jukanntenn/django-blog-tutorial)|基于 Python3.5 和 Django 1.10 的 Django Blog 项目。|2.2k|CSS|2021/06/11|
|17|[ethantw/Han](https://github.com/ethantw/Han)|「漢字標準格式」印刷品般的漢字排版框架 Han.css: the CSS typography framework optimised for Hanzi.|2.2k|CSS|2021/07/02|
|18|[abc-club/js-paradise](https://github.com/abc-club/js-paradise)|前端乐园|1.7k|CSS|2021/11/29|
|19|[nicejade/markdown-online-editor](https://github.com/nicejade/markdown-online-editor)|📝基于 Vue、Vditor，所构建的在线 Markdown 编辑器，支持流程图、甘特图、时序图、任务列表、HTML 自动转换为 Markdown 等功能；🎉新增「所见即所得」编辑模式。|1.6k|CSS|2021/10/06|
|20|[apachecn/home](https://github.com/apachecn/home)|ApacheCN  开源组织：公告、介绍、成员、活动、交流方式|1.3k|CSS|2021/10/21|
|21|[yihui/xaringan](https://github.com/yihui/xaringan)|Presentation Ninja 幻灯忍者 · 写轮眼|1.3k|CSS|2021/06/25|
|22|[smartping/smartping](https://github.com/smartping/smartping)|综合性网络质量(PING)检测工具，支持正/反向PING绘图、互PING拓扑绘图与报警、全国PING延迟地图与在线检测工具等功能 |1.2k|CSS|2021/10/29|
|23|[beeth0ven/RxSwift-Chinese-Documentation](https://github.com/beeth0ven/RxSwift-Chinese-Documentation)|RxSwift 中文文档|1.2k|CSS|2021/10/25|
|24|[leopardpan/leopardpan.github.io](https://github.com/leopardpan/leopardpan.github.io)|个人博客，看效果进入|1.2k|CSS|2021/10/17|
|25|[v2fly/v2ray-examples](https://github.com/v2fly/v2ray-examples)|v2ray-core 的模板们|1.1k|CSS|2021/10/11|
|26|[h5ds/h5ds](https://github.com/h5ds/h5ds)|H5DS (HTML5 Design software) 这是一款基于WEB的 H5制作工具。让不会写代码的人也能轻松快速上手制作H5页面。类似易企秀的H5制作、建站工具，示范网站：h5ds.com |1.1k|CSS|2021/09/23|
|27|[theme-nexmoe/hexo-theme-nexmoe](https://github.com/theme-nexmoe/hexo-theme-nexmoe)|🔥 一个比较特别的 Hexo 主题|1.1k|CSS|2021/11/21|
|28|[w-digital-scanner/w12scan](https://github.com/w-digital-scanner/w12scan)|🚀 A simple asset discovery engine for cybersecurity. (网络资产发现引擎)|1.0k|CSS|2021/06/02|
|29|[cnfeat/list-of-good-things](https://github.com/cnfeat/list-of-good-things)|list-of-good-things 好物清单|965|CSS|2021/11/13|
|30|[xuqiang521/nuxt-ssr-demo](https://github.com/xuqiang521/nuxt-ssr-demo)|:sparkles:  高仿掘金，整合 vue + nuxt + axios + vuex + vue-router (nuxt 自带 vuex 和 vue-router)，一个基于 Nuxt 的服务器端渲染 Demo|935|CSS|2021/06/16|
|31|[kaeyleo/jekyll-theme-H2O](https://github.com/kaeyleo/jekyll-theme-H2O)| 🎉 A clean and delicate Jekyll theme. Jekyll博客主题|899|CSS|2021/10/28|
|32|[limbopro/Adblock4limbo](https://github.com/limbopro/Adblock4limbo)|毒奶去广告计划（稳定版）For Quantumult X ；如去奈菲影视/低端影视/片库网/Pornhub/Jable/Netflav/HPjav等视频网站广告或其他ACG网站广告；|720|CSS|2021/11/25|
|33|[e282486518/yii2admin](https://github.com/e282486518/yii2admin)|通用的yii2后台，基于Yii2的advanced应用程序模板，整合RBAC、Menu、Config、Migration多语言、RESTfull等等...|629|CSS|2021/10/11|
|34|[andylei18/vue-shopping](https://github.com/andylei18/vue-shopping)|基于Vue模仿蘑菇街的单页应用http://andylei18.github.io/vue-shopping/|624|CSS|2021/08/03|
|35|[dkwingsmt/haha](https://github.com/dkwingsmt/haha)|蛤蛤体生成器|592|CSS|2021/08/26|
|36|[ldqk/Masuit.MyBlogs](https://github.com/ldqk/Masuit.MyBlogs)|ldqk.xyz个人博客站项目源码，https://ldqk.xyz ，供参考、学习、引用、非商业性质的部署。基于.net6|588|CSS|2021/11/29|
|37|[Cl0udG0d/SZhe_Scan](https://github.com/Cl0udG0d/SZhe_Scan)|碎遮SZhe_Scan Web漏洞扫描器，基于python Flask框架，对输入的域名/IP进行全面的信息搜集，漏洞扫描，可自主添加POC|584|CSS|2021/08/04|
|38|[zlq4863947/tradingViewWikiCn](https://github.com/zlq4863947/tradingViewWikiCn)|tradingView的中文开发文档|553|CSS|2021/10/27|
|39|[liangjingkanji/DrakeTyporaTheme](https://github.com/liangjingkanji/DrakeTyporaTheme)|最细腻的主题 Drake Light Dark Vue Ayu Material Black Purple Juejin Google style for typora-theme|534|CSS|2021/11/08|
|40|[d2-projects/folder-explorer](https://github.com/d2-projects/folder-explorer)|分析文件目录，统计数据并以树形结构和图表的形式展示结果，也可以导出多种格式留存|529|CSS|2021/10/13|
|41|[cool-team-official/cool-admin-vue](https://github.com/cool-team-official/cool-admin-vue)|cool-admin一个很酷的后台权限管理框架，模块化、插件化、CRUD极速开发，永久开源免费，基于midway.js 2.0、typeorm、mysql、jwt、element-ui、vuex、vue-router、vue等构建|519|CSS|2021/11/17|
|42|[Veal98/CS-Wiki](https://github.com/Veal98/CS-Wiki)|📙 致力打造完善的 Java 后端知识体系. Not only an Interview-Guide, but also a Learning-Direction.|500|CSS|2021/11/07|
|43|[Dreamer-Paul/Single](https://github.com/Dreamer-Paul/Single)|🎈 一个简洁大气，含夜间模式的 Typecho 博客主题|485|CSS|2021/11/30|
|44|[byrwiki/byrwiki](https://github.com/byrwiki/byrwiki)|北邮人导航：一个框，全能搜|472|CSS|2021/11/22|
|45|[52bp/52bp.github.io](https://github.com/52bp/52bp.github.io)|分享免费SSR V2ray Trojan 免费、优质节点机场大全导航推荐，记得点击star⭐==不迷路|466|CSS|2021/11/07|
|46|[trinitrotofu/Bubble](https://github.com/trinitrotofu/Bubble)|Typecho 清新风格响应式主题|403|CSS|2021/05/22|
|47|[xupsh/pp4fpgas-cn](https://github.com/xupsh/pp4fpgas-cn)|中文版 Parallel Programming for FPGAs|403|CSS|2021/10/26|
|48|[w-digital-scanner/w11scan](https://github.com/w-digital-scanner/w11scan)|分布式WEB指纹识别平台 Distributed WEB fingerprint identification platform|400|CSS|2021/06/11|
|49|[jhao104/django-blog](https://github.com/jhao104/django-blog)|django搭建博客|383|CSS|2021/06/11|
|50|[weeshop/WeeShop](https://github.com/weeshop/WeeShop)|优雅易用的微信小程序商城，PHP商城。 基于Laravel的基因，来自Symfony的底层技术，来自Drupal Commerce的核心技术，由Drupal中国开源社区维护。QQ群：714023327|357|CSS|2021/11/24|
|51|[livelyPeng/pl-drag-template](https://github.com/livelyPeng/pl-drag-template)|一个h5可视化编辑器项目|355|CSS|2021/10/07|
|52|[rawchen/blog-ssm](https://github.com/rawchen/blog-ssm)|一个简单漂亮的SSM博客系统。|341|CSS|2021/11/18|
|53|[Zisbusy/Akina-for-Typecho](https://github.com/Zisbusy/Akina-for-Typecho)|Akina for Typecho 主题模板|341|CSS|2021/11/06|
|54|[wizardforcel/modern-java-zh](https://github.com/wizardforcel/modern-java-zh)|:book: 【译】Java 8 简明教程|307|CSS|2021/05/25|
|55|[LIlGG/halo-theme-sakura](https://github.com/LIlGG/halo-theme-sakura)|Halo 版本的樱花🌸主题|307|CSS|2021/11/17|
|56|[sanjinhub/hexo-theme-geek](https://github.com/sanjinhub/hexo-theme-geek)|一个符合极客精神主义极简的 Hexo 主题|297|CSS|2021/06/05|
|57|[c10342/player](https://github.com/c10342/player)|electron-vue音视频播放器|297|CSS|2021/11/17|
|58|[mo-xiaoxi/AWD_CTF_Platform](https://github.com/mo-xiaoxi/AWD_CTF_Platform)|一个简单的AWD训练平台|281|CSS|2021/09/21|
|59|[eagleoflqj/p1a3_script](https://github.com/eagleoflqj/p1a3_script)|Tampermonkey Script for 1point3acres / 一亩三分地的油猴脚本|278|CSS|2021/12/01|
|60|[ustc-zzzz/YiGeDingLia](https://github.com/ustc-zzzz/YiGeDingLia)|一个顶俩|278|CSS|2021/10/06|
|61|[theme-nexmoe/typecho-theme-nexmoe](https://github.com/theme-nexmoe/typecho-theme-nexmoe)|🔥 一个特别的 Typecho 主题|262|CSS|2021/09/10|
|62|[hsxyhao/gridea-theme-next](https://github.com/hsxyhao/gridea-theme-next)|Gridea NexT主题，搬hexo-next-theme|261|CSS|2021/11/29|
|63|[dotnetclub-net/dotnetclub](https://github.com/dotnetclub-net/dotnetclub)|dotnetclub.net 的源代码|252|CSS|2021/10/06|
|64|[hellosee/swoole-webim-demo](https://github.com/hellosee/swoole-webim-demo)|使用swoole扩展和php开发的一个在线聊天室(Making a Web Chat With PHP and Swoole)|249|CSS|2021/05/13|
|65|[liuhuanyong/liuhuanyong.github.io](https://github.com/liuhuanyong/liuhuanyong.github.io)|面向中文自然语言处理的六十余类实践项目及学习索引，涵盖语言资源构建、社会计算、自然语言处理组件、知识图谱、事理图谱、知识抽取、情感分析、深度学习等几个学习主题。包括作者个人简介、学习心得、语言资源、工业落地系统等，是供自然语言处理入门学习者的一个较为全面的学习资源，欢迎大家使用，并提出批评意见。|245|CSS|2021/11/13|
|66|[LF112/BTCO](https://github.com/LF112/BTCO)|🎉 宝塔面板响应式解决方案|237|CSS|2021/09/04|
|67|[fingerchar/fingernft](https://github.com/fingerchar/fingernft)|FingerNFT是一款开源NFT市场，兼容Opensea、Rarible。|236|CSS|2021/11/30|
|68|[Yue-plus/hexo-theme-arknights](https://github.com/Yue-plus/hexo-theme-arknights)|明日方舟罗德岛阵营的 Hexo 主题，支持数学公式、Valine评论系统、Mermaid图表|231|CSS|2021/11/23|
|69|[workcheng/WeiYa](https://github.com/workcheng/WeiYa)|尾牙小程序|227|CSS|2021/08/16|
|70|[happypeter/haoduoshipin](https://github.com/happypeter/haoduoshipin)|好多视频|220|CSS|2021/10/22|
|71|[We-Hack-Studio/nuts](https://github.com/We-Hack-Studio/nuts)|坚果量化 - 数字货币量化交易系统。|213|CSS|2021/10/07|
|72|[xukimseven/HardCandy-Jekyll](https://github.com/xukimseven/HardCandy-Jekyll)|一款清新 糖果色🍬 的 ‘Jekyll’ 主题。A candy-colored 🍬 ‘Jekyll’ theme.|210|CSS|2021/07/13|
|73|[Soanguy/typora-theme-autumnus](https://github.com/Soanguy/typora-theme-autumnus)|Typora theme for 中文|204|CSS|2021/11/28|
|74|[rememberber/MooTool](https://github.com/rememberber/MooTool)|A handy tool set for developers. 开发者常备小工具|199|CSS|2021/11/06|
|75|[lovefc/china_school_badge](https://github.com/lovefc/china_school_badge)|全国高校校徽字体图标库|198|CSS|2021/05/04|
|76|[dmego/home.github.io](https://github.com/dmego/home.github.io)|个人主页|191|CSS|2021/12/01|
|77|[suruibuas/eadmin](https://github.com/suruibuas/eadmin)|eadmin - 极致用户体验与极简开发并存的开箱即用的后台UI框架|184|CSS|2021/08/11|
|78|[frankcbliu/Interview_Notes](https://github.com/frankcbliu/Interview_Notes)|📚 程序员面试基础知识总结、优质项目分享、助力春招秋招|179|CSS|2021/09/07|
|79|[Lavender-z/demo](https://github.com/Lavender-z/demo)|每天一个有趣的前端案例|178|CSS|2021/06/07|
|80|[HFO4/HideByPixel](https://github.com/HFO4/HideByPixel)|一个利用像素隐藏文字的程序|172|CSS|2021/07/12|
|81|[HCLonely/hexo-theme-webstack](https://github.com/HCLonely/hexo-theme-webstack)|A hexo theme based on webstack.   一个基于webstack的hexo主题。|169|CSS|2021/06/08|
|82|[idealclover/Life-in-NJU](https://github.com/idealclover/Life-in-NJU)|🏠 南哪指南—南哪大学网址导航|161|CSS|2021/08/22|
|83|[SummerSec/learning-codeql](https://github.com/SummerSec/learning-codeql)|CodeQL Java 全网最全的中文学习资料|157|CSS|2021/11/22|
|84|[hanc00l/nemo_go](https://github.com/hanc00l/nemo_go)|Nemo是用来进行自动化信息收集的一个简单平台，通过集成常用的信息收集工具和技术，实现对内网及互联网资产信息的自动收集，提高隐患排查和渗透测试的工作效率，用Go语言完全重构了原Python版本。|152|CSS|2021/10/18|
|85|[zhitom/zkweb](https://github.com/zhitom/zkweb)|zookeeper web管理和监控界面，使用内置的H2数据库，此版本基于淘宝大神yasenagat的zkWeb源码基础之上进行了大幅升级和修改，主要新增了集群监控和国际化功能，直接java -jar或将war包放入tomcat即可运行！最近新增加了docker镜像功能！|146|CSS|2021/10/26|
|86|[d2-projects/d2-daily](https://github.com/d2-projects/d2-daily)|D2 日报|140|CSS|2021/10/06|
|87|[vanyouseea/o365](https://github.com/vanyouseea/o365)|O365管理系统是一个以java语言开发的基于Microsoft Graph Restful API的多全局管理系统，理论上支持任何Office全局的管理(A1,A3,A1P,E3,E5等)，你可以很方便的使用它来批量添加，批量删除，批量启用，批量禁用，搜索和查看用户，生成邀请码，邀请朋友注册，提升和收回管理员权限，更新密钥，查看订阅，分配订阅(创新用户时)，查看多全局报告|140|CSS|2021/11/18|
|88|[qq8e/qq](https://github.com/qq8e/qq)|8亿QQ绑定数据泄露查询源码，附送数据。不定期更新下载地址 关注越多送的越多|137|CSS|2021/11/06|
|89|[smallnest/go-rpc-programming-guide](https://github.com/smallnest/go-rpc-programming-guide)|gitbook Go RPC开发指南 [中文文档]|135|CSS|2021/06/16|
|90|[xiangjianan/lks](https://github.com/xiangjianan/lks)|🔗 B站博主 -LKs- 网站推荐 🔗|127|CSS|2021/10/24|
|91|[hakadao/bilibili-simple-home](https://github.com/hakadao/bilibili-simple-home)|b站仿搜索引擎样式首页风格，支持几种不同显示效果|113|CSS|2021/07/13|
|92|[vakinge/oneplatform](https://github.com/vakinge/oneplatform)|onePlatform定位是企业级应用网关，提供提供服务路由、SSO、统一认证授权、统一日志、全局事件以及模块化管理等基础能力。基于Spring cloud、开箱即用、无额外学习成本、无缝对接老系统。→提供配套视频教程(Q群: 61859839) |111|CSS|2021/11/07|
|93|[lbc-team/deep_ethereum](https://github.com/lbc-team/deep_ethereum)|电子书：以太坊技术与实现|111|CSS|2021/10/31|
|94|[dolymood/dolymood.github.com](https://github.com/dolymood/dolymood.github.com)|我的博客|110|CSS|2021/06/17|
|95|[bit-ranger/blog](https://github.com/bit-ranger/blog)|博客|110|CSS|2021/10/14|
|96|[ymm-tech/gods-pen-admin](https://github.com/ymm-tech/gods-pen-admin)|码良管理后台项目|109|CSS|2021/10/06|
|97|[MingZhuLiu/ZLMediaServerManagent](https://github.com/MingZhuLiu/ZLMediaServerManagent)|ZLMediaKitServer后台管理系统|108|CSS|2021/05/15|
|98|[Bulandent/hexo-theme-bubuzou](https://github.com/Bulandent/hexo-theme-bubuzou)|一个仿Vue官网风格的hexo主题  https://bubuzou.com/|107|CSS|2021/07/09|
|99|[liuzhengyang/lets-hotfix](https://github.com/liuzhengyang/lets-hotfix)|Dynamic class reloading for java。Java代码热更新，支持本地、远程|106|CSS|2021/07/02|
|100|[405go/pdman](https://github.com/405go/pdman)|PDMan是一款开源免费的数据库模型建模工具，支持Windows,Mac,Linux等操作系统，是PowerDesigner之外，更好的免费的替代方案。他具有颜值高，使用简单的特点。包含数据库建模，灵活自动的自动生成代码模板，自动生成文档等多种开发人员实用的功能。 https://my.oschina.net/skymozn/blog/2992777|104|CSS|2021/10/13|
|101|[zenany/zenany.github.io](https://github.com/zenany/zenany.github.io)|记录对技术的思考和探索|98|CSS|2021/06/09|
|102|[Blokura/bv2av](https://github.com/Blokura/bv2av)|哔哩哔哩BV转AV|97|CSS|2021/10/16|
|103|[luokangyuan/ghost-theme-mj](https://github.com/luokangyuan/ghost-theme-mj)|This is a beautiful ghost blog theme，这是一个漂亮的Ghost博客主题。|94|CSS|2021/09/21|
|104|[DataXujing/TensorRT_CV](https://github.com/DataXujing/TensorRT_CV)|:rocket::rocket::rocket:NVIDIA TensorRT 加速推断教程！|94|CSS|2021/08/01|
|105|[lihanghang/CASR-DEMO](https://github.com/lihanghang/CASR-DEMO)|基于Flask Web的中文自动语音识别演示系统,包含语音识别、语音合成、声纹识别之说话人识别。|92|CSS|2021/10/28|
|106|[Bililive/www.danmuji.org](https://github.com/Bililive/www.danmuji.org)|弹幕姬的网站 https://www.danmuji.org 弹幕姬 https://github.com/copyliu/bililive_dm|92|CSS|2021/05/21|
|107|[JinJieTan/Palantir](https://github.com/JinJieTan/Palantir)|这是一个开源项目，励志打造桌面端的音视频IM项目，开发成员为SegmentFault前端交流群成员|91|CSS|2021/10/13|
|108|[kaedei/dandanplay-libraryindex](https://github.com/kaedei/dandanplay-libraryindex)|弹弹play Windows/UWP客户端远程访问功能html首页（媒体库内容的展示以及视频播放）|90|CSS|2021/07/07|
|109|[qu5/mogu](https://github.com/qu5/mogu)|一个简约的php书签网址导航|89|CSS|2021/11/17|
|110|[idcf-boat-house/boat-house-frontend](https://github.com/idcf-boat-house/boat-house-frontend)|IDCF Boat House 前端库，包括用户界面 和 管理界面|89|CSS|2021/11/30|
|111|[apachecn/quant-learning](https://github.com/apachecn/quant-learning)|:books: Quant 教程整理|88|CSS|2021/05/09|
|112|[hltj/kotlin-reference-chinese](https://github.com/hltj/kotlin-reference-chinese)|Kotlin 官方文档（参考部分）中文版|87|CSS|2021/08/26|
|113|[GoneTone/genshin-impact-wish-gacha-analyzer](https://github.com/GoneTone/genshin-impact-wish-gacha-analyzer)|原神祈願卡池分析 Genshin Impact Wish Gacha Analyzer   A utility for analyzing gacha history, where all data and numbers are well-organized in a convenient manner!|87|CSS|2021/11/25|
|114|[Ryqsky/cnode-vue](https://github.com/Ryqsky/cnode-vue)|Vue2版CNode社区WebApp 在线访问：http://cnode.ruanyq.cn/|85|CSS|2021/11/04|
|115|[TNK-Studio/Odoo-Book](https://github.com/TNK-Studio/Odoo-Book)|🤓Odoo 小书，Odoo 入门教程 （龟速填坑中...已更新到第 7 章第 2 节，要是看不到更新可以强制刷新一下浏览器。小书源码地址请看 README。）|83|CSS|2021/09/21|
|116|[nicehorse06/software-job-note](https://github.com/nicehorse06/software-job-note)|軟體工作入門與軟體工程求職記錄(目前只有前端)，相關問題可在issue討論|83|CSS|2021/08/12|
|117|[shuiche-it/cnblog-mouse](https://github.com/shuiche-it/cnblog-mouse)|博客园鼠标特效.  Demo 地址|81|CSS|2021/05/07|
|118|[wangyang0210/Cnblogs-Theme](https://github.com/wangyang0210/Cnblogs-Theme)|博客园皮肤|81|CSS|2021/06/17|
|119|[wqzwh/vue-ssr](https://github.com/wqzwh/vue-ssr)|基于vue-ssr搭建的一套node服务端渲染工作流|80|CSS|2021/05/10|
|120|[yfzhao20/Typora-markdown](https://github.com/yfzhao20/Typora-markdown)|试验性的存储库，用于存放Typora折腾过的css样式和其他|80|CSS|2021/05/28|
|121|[ZiXia1/Mr.Film-Theme](https://github.com/ZiXia1/Mr.Film-Theme)|苹果csmV10仿电影先生自适应模板|75|CSS|2021/05/09|
|122|[askeing/rust-book](https://github.com/askeing/rust-book)|Rust 程式語言 正體中文版 (The Rust Programming Language, Traditional Chinese)|75|CSS|2021/07/19|
|123|[shaonianzhentan/ha_sidebar](https://github.com/shaonianzhentan/ha_sidebar)|在HA里使用的侧边栏管理|74|CSS|2021/11/07|
|124|[CodeIgniter-Chinese/codeigniter4-user-guide](https://github.com/CodeIgniter-Chinese/codeigniter4-user-guide)|CodeIgniter 4 Chinese User Guide - CodeIgniter 4 简体中文手册|73|CSS|2021/07/27|
|125|[Mhy278/MinecraftServerHostGuide](https://github.com/Mhy278/MinecraftServerHostGuide)|我的世界服务器搭建指南|72|CSS|2021/05/08|
|126|[liuxiaoyucc/uni-music](https://github.com/liuxiaoyucc/uni-music)|:musical_note: uniapp音乐播放器|72|CSS|2021/10/06|
|127|[linuxkerneltravel/website](https://github.com/linuxkerneltravel/website)|Linux内核之旅开源社区网站|71|CSS|2021/11/18|
|128|[yitd/wxkp](https://github.com/yitd/wxkp)|微信卡片分享链接制作工具|69|CSS|2021/08/15|
|129|[afi-team/website](https://github.com/afi-team/website)|蚂蚁前端基础设施团队（Ant Frontend Infrastructure）官方网站|68|CSS|2021/09/13|
|130|[apachecn/ds-ipynb-zh](https://github.com/apachecn/ds-ipynb-zh)|:book:  [译] 数据科学 IPython 笔记本|68|CSS|2021/05/09|
|131|[LoyalWilliams/comic](https://github.com/LoyalWilliams/comic)|scrapy漫画爬虫+django页面展示项目，网易163漫画、腾讯漫画、神漫画在线爬取|66|CSS|2021/08/29|
|132|[ljk4160/GDOCK](https://github.com/ljk4160/GDOCK)|自动从lean的lede源码clone并生成竞斗云固件,大雕源码:|64|CSS|2021/08/10|
|133|[xOS/Nange](https://github.com/xOS/Nange)|个人主页|64|CSS|2021/06/13|
|134|[spring2go/spring-petclinic-msa](https://github.com/spring2go/spring-petclinic-msa)|微服务版spring-petlinic，课程《Kubernetes微服务实践》|63|CSS|2021/08/24|
|135|[wangyouworld/AdminLTE_CN](https://github.com/wangyouworld/AdminLTE_CN)|AdminLTE 文档本地化|63|CSS|2021/06/01|
|136|[BobCoderS9/SSPanel-Metron](https://github.com/BobCoderS9/SSPanel-Metron)|SSPanel-Metron主题，目前由@Bobs9维护开发中。|63|CSS|2021/11/30|
|137|[orange-form/orange-admin](https://github.com/orange-form/orange-admin)|橙单中台化低代码生成器。可完整支持多应用、多租户、多渠道、工作流 (Flowable & Activiti)、在线表单、自定义数据同步、自定义Job、多表关联、跨服务多表关联、框架技术栈自由组合等。|62|CSS|2021/11/25|
|138|[newdraw/AppTime](https://github.com/newdraw/AppTime)|桌面时间管理 Desktop Time Management|60|CSS|2021/07/31|
|139|[DuYi-Edu/DuYi-React](https://github.com/DuYi-Edu/DuYi-React)|React课程资料与代码|59|CSS|2021/10/06|
|140|[mltreegroup/MlTree-Forum](https://github.com/mltreegroup/MlTree-Forum)|MlTree Forum是一款由Thinkphp构建、Material Design风格的轻论坛。|58|CSS|2021/06/22|
|141|[issochen/qiankun-vue-demo](https://github.com/issochen/qiankun-vue-demo)|前端微服务 qiankun demo vue|57|CSS|2021/10/06|
|142|[python-chinese/python-chinese.github.io](https://github.com/python-chinese/python-chinese.github.io)|Python中文社区|57|CSS|2021/07/20|
|143|[N0ts123/NutssssIndex](https://github.com/N0ts123/NutssssIndex)|简约个人主页、自适应|56|CSS|2021/11/26|
|144|[apachecn/geeksforgeeks-dsal-zh](https://github.com/apachecn/geeksforgeeks-dsal-zh)|:book: [译] GeeksForGeeks 翻译计划|56|CSS|2021/11/25|
|145|[wilde3/typora-theme-redrail](https://github.com/wilde3/typora-theme-redrail)|This is a typora theme inspired by pie and ursine theme. 一个typora主题，具有pie和ursine风格。|56|CSS|2021/06/09|
|146|[ChenYFan-Tester/IPFS_PHOTO_SHARE](https://github.com/ChenYFan-Tester/IPFS_PHOTO_SHARE)|💰用甚嚒服务器，ServerLess搭建一个图片分享站点！  基于CloudFlareWorker无服务器函数和IPFS去中心化存储的图片分享网站|56|CSS|2021/06/18|
|147|[syfxlin/xkeditor](https://github.com/syfxlin/xkeditor)| :pencil: XK-Editor   一个支持富文本和Markdown的编辑器|56|CSS|2021/09/21|
|148|[Lihaogx/graph-note-of-greek-myth](https://github.com/Lihaogx/graph-note-of-greek-myth)|希腊神话读书笔记|55|CSS|2021/11/30|
|149|[Gamuxorg/AppImage-CN](https://github.com/Gamuxorg/AppImage-CN)|AppImage中文文档，源自官方英文文档。|54|CSS|2021/07/13|
|150|[cody1991/cody1991.github.io](https://github.com/cody1991/cody1991.github.io)|个人博客（也是自己构建的一个简单Jekyll开源主题模板）|53|CSS|2021/08/05|

⬆ [回到目录](#内容目录)

<br/>

## Dart

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[alibaba/flutter-go](https://github.com/alibaba/flutter-go)|flutter 开发者帮助 APP，包含 flutter 常用 140+ 组件的demo 演示与中文文档|22.9k|Dart|2021/05/20|
|2|[CarGuo/gsy_github_app_flutter](https://github.com/CarGuo/gsy_github_app_flutter)|Flutter 超完整的开源项目，功能丰富，适合学习和日常使用。GSYGithubApp系列的优势：我们目前已经拥有Flutter、Weex、ReactNative、kotlin 四个版本。 功能齐全，项目框架内技术涉及面广，完成度高，持续维护，配套文章，适合全面学习，对比参考。跨平台的开源Github客户端App，更好的体验，更丰富的功能，旨在更好的日常管理和维护个人Github，提供更好更方便的驾车体验Σ(￣。￣ﾉ)ﾉ。同款Weex版本 ： https://github.com/CarGuo/GSYGithubAppWeex    、同款React Native版本 ： https://g ...|12.9k|Dart|2021/11/28|
|3|[kaina404/FlutterDouBan](https://github.com/kaina404/FlutterDouBan)|🔥🔥🔥Flutter豆瓣客户端,Awesome Flutter Project,全网最100%还原豆瓣客户端。首页、书影音、小组、市集及个人中心，一个不拉。（ https://img.xuvip.top/douyademo.mp4）|6.7k|Dart|2021/08/01|
|4|[simplezhli/flutter_deer](https://github.com/simplezhli/flutter_deer)|🦌 Flutter 练习项目(包括集成测试、可访问性测试)。内含完整UI设计图，更贴近真实项目的练习。Flutter practice project. Includes a complete UI design and exercises that are closer to real projects.|5.7k|Dart|2021/11/29|
|5|[Sky24n/flutter_wanandroid](https://github.com/Sky24n/flutter_wanandroid)|🔥🔥🔥  基于Google Flutter的WanAndroid客户端，支持Android和iOS。包括BLoC、RxDart 、国际化、主题色、启动页、引导页！|5.3k|Dart|2021/05/21|
|6|[toly1994328/FlutterUnit](https://github.com/toly1994328/FlutterUnit)|【Flutter 集录指南 App】The unity of flutter, The unity of coder.|4.4k|Dart|2021/11/11|
|7|[liupan1890/aliyunpan](https://github.com/liupan1890/aliyunpan)|阿里云盘小白羊版  阿里云盘PC版 aliyundriver|4.0k|Dart|2021/11/29|
|8|[flutterchina/flukit](https://github.com/flutterchina/flukit)| A Flutter UI Kit（一个 Flutter UI组件库），2.0 beta is available now !|3.8k|Dart|2021/10/07|
|9|[yubo725/flutter-osc](https://github.com/yubo725/flutter-osc)|基于Google Flutter的开源中国客户端，支持Android和iOS。|2.8k|Dart|2021/08/07|
|10|[Notsfsssf/pixez-flutter](https://github.com/Notsfsssf/pixez-flutter)|一个支持免代理直连及查看动图的第三方Pixiv flutter客户端|2.7k|Dart|2021/11/29|
|11|[boyan01/flutter-netease-music](https://github.com/boyan01/flutter-netease-music)|flutter music player application. (仿网易云音乐)|2.6k|Dart|2021/11/28|
|12|[phoenixsky/fun_android_flutter](https://github.com/phoenixsky/fun_android_flutter)| 👿👿👿👿👿玩Android客户端Flutter版本。Provider的最佳实践.DarkMode、多色彩主题、国际化、切换字体、优美动画|2.5k|Dart|2021/05/21|
|13|[OpenFlutter/fluwx](https://github.com/OpenFlutter/fluwx)|Flutter版微信SDK.WeChat SDK for flutter.|2.5k|Dart|2021/10/27|
|14|[huanxsd/flutter_shuqi](https://github.com/huanxsd/flutter_shuqi)|高仿书旗小说 Flutter版，支持iOS、Android|2.3k|Dart|2021/10/08|
|15|[CarGuo/gsy_flutter_demo](https://github.com/CarGuo/gsy_flutter_demo)|Flutter 不同于 GSYGithubAppFlutter 完整项目，本项目将逐步完善各种 Flutter 独立例子，方便新手学习上手和小问题方案解决。  目前开始逐步补全完善，主要提供一些有用或者有趣的例子，如果你也有好例子，欢迎提交 PR 。|2.1k|Dart|2021/11/30|
|16|[shichunlei/flutter_app](https://github.com/shichunlei/flutter_app)|🔥🔥🔥本项目包括各种基本控件使用（Text、TextField、Icon、Image、Listview、Gridview、Picker、Stepper、Dialog、Slider、Row、Appbar、Sizebox、BottomSheet、Chip、Dismissible、FlutterLogo、Check、Switch、TabBar、BottomNavigationBar、Sliver等）、豆瓣电影、tubitv、每日一文、和天气、百姓生活、随机诗词、联系人、句子迷、好奇心日报、有道精品课、高德定位、音乐播放器🎵、追书神器等板块|2.1k|Dart|2021/10/05|
|17|[youxinLu/flutter_mall](https://github.com/youxinLu/flutter_mall)|Flutter_Mall是一款Flutter开源在线商城应用程序，是基于litemall基础上进行开发，litemall包含了Spring Boot后端 + Vue管理员前端 + 微信小程序用户前端 + Vue用户移动端感兴趣的同学可以自行研究部署，Flutter_Mall基本上包含了litemall中小程序的功能。|2.0k|Dart|2021/07/04|
|18|[LaoMengFlutter/flutter-do](https://github.com/LaoMengFlutter/flutter-do)|包含350多个组件用法、组件继承关系图、40多个 loading 组件，App升级、验证码、弹幕、音乐字幕 4个插件，一个小而全完整的App项目。|1.9k|Dart|2021/09/26|
|19|[fluttercandies/wechat_flutter](https://github.com/fluttercandies/wechat_flutter)|wechat_flutter  Flutter版本微信，一个优秀的Flutter即时通讯IM开源库！|1.9k|Dart|2021/11/14|
|20|[Sky24n/flustars](https://github.com/Sky24n/flustars)|🔥🔥🔥  Flutter common utils library. SpUtil, ScreenUtil,WidgetUtil.  也许是目前最好用的SharedPreferences工具类。WidgetUtil 获取图片尺寸宽高, View尺寸&在屏幕上的坐标。|1.6k|Dart|2021/05/10|
|21|[yukilzw/dy_flutter](https://github.com/yukilzw/dy_flutter)|斗鱼直播APP :rocket: 多元化Flutter开源项目。涵盖礼物特效、手势动画、弹幕池、抽奖、鱼吧等（另提供服务端Mock接口）|1.5k|Dart|2021/11/02|
|22|[befovy/fijkplayer](https://github.com/befovy/fijkplayer)|ijkplayer for flutter.  ijkplayer 的 flutter 封装。 Flutter video/audio player. Flutter media player plugin for android/iOS based on ijkplayer. fijkplayer 是基于 ijkplayer 封装的 flutter 媒体播放器，开箱即用，无需编译 ijkplayer|1.2k|Dart|2021/11/30|
|23|[Sky24n/common_utils](https://github.com/Sky24n/common_utils)|Dart common utils library. DateUtil, EncryptUtil, JsonUtil, LogUtil, MoneyUtil, NumUtil, ObjectUtil,  RegexUtil, TextUtil, TimelineUtil, TimerUtil. 包含日期，正则，倒计时，时间轴等工具类。|1.1k|Dart|2021/11/30|
|24|[git-touch/git-touch](https://github.com/git-touch/git-touch)|An open-source app for GitHub, GitLab, Bitbucket, Gitea, and Gitee(码云), built with Flutter|967|Dart|2021/10/31|
|25|[wuba/fair](https://github.com/wuba/fair)|A Flutter package used to update widget tree dynamically. Flutter Fair是为Flutter设计的，UI&模板动态化框架|851|Dart|2021/11/27|
|26|[flutterchina/azlistview](https://github.com/flutterchina/azlistview)|A Flutter sticky headers & index ListView. Flutter 城市列表、联系人列表，索引&悬停。|804|Dart|2021/09/07|
|27|[canhuah/WanAndroid-Flutter](https://github.com/canhuah/WanAndroid-Flutter)|Flutter版本 WanAndroid客户端  适合Flutter入门学习 被张鸿洋微信公众号推荐为优质Flutter开源项目啦|609|Dart|2021/11/30|
|28|[xausky/DockerRegisterCloud](https://github.com/xausky/DockerRegisterCloud)|基于 Docker 仓库协议的网盘客户端，可以将目前众多的免费容器仓库服务用于网盘。|577|Dart|2021/11/25|
|29|[xuexiangjys/flutter_template](https://github.com/xuexiangjys/flutter_template)|The project of the empty template with Flutter has built the basic framework to realize the functions of internationalization, theme peeling, login and registration, etc.(Flutter空壳模板工程，已搭建基础框架，实现国际化、主题换肤、登录注册、自动路由注册等功能，可在此基础上简单修改实现自己的应用功能)|532|Dart|2021/08/11|
|30|[kangshaojun/flutter-book](https://github.com/kangshaojun/flutter-book)|Flutter入门与实战随书源码 第2版|529|Dart|2021/07/18|
|31|[flutterchina/json_model](https://github.com/flutterchina/json_model)|Generate model class from Json file. 一行命令，通过Json文件生成Dart Model类。|522|Dart|2021/10/07|
|32|[crazecoder/flutter_bugly](https://github.com/crazecoder/flutter_bugly)|腾讯Bugly flutter应用更新统计及异常上报插件,支持Android、iOS|505|Dart|2021/11/18|
|33|[RxReader/wechat_kit](https://github.com/RxReader/wechat_kit)|Flutter 版微信登录/分享/支付 SDK|473|Dart|2021/10/13|
|34|[CCY0122/WanAndroid_Flutter](https://github.com/CCY0122/WanAndroid_Flutter)|🔥🔥超完整超漂亮的Flutter版wanAndroid客户端。含wanAndroid已开放的所有功能（包括TODO）。项目包含BloC模式、Provider模式、常规模式。|462|Dart|2021/06/17|
|35|[Tencent/mxflutter](https://github.com/Tencent/mxflutter)|使用 TypeScript/JavaScript 来开发 Flutter 应用的框架。|440|Dart|2021/07/29|
|36|[LianjiaTech/keframe](https://github.com/LianjiaTech/keframe)|Components that optimize Flutter fluency.（Flutter 流畅度优化的通用方案，轻松解决卡顿问题）|436|Dart|2021/11/25|
|37|[biyidev/biyi_app](https://github.com/biyidev/biyi_app)|Biyi (比译) is a convenient translation and dictionary app written in dart / Flutter. |419|Dart|2021/11/29|
|38|[naco-siren/mogicians-manual](https://github.com/naco-siren/mogicians-manual)|Flutter version【膜法指南】open source project|418|Dart|2021/05/10|
|39|[Im-Kevin/cool_ui](https://github.com/Im-Kevin/cool_ui)|用flutter实现一些我认为好看的UI控件,有Popover,仿Weui的Toast,自定义键盘|398|Dart|2021/11/08|
|40|[xiaoyaocz/dmzj_flutter](https://github.com/xiaoyaocz/dmzj_flutter)|动漫之家Flutter客户端|393|Dart|2021/11/29|
|41|[niuhuan/pikapika](https://github.com/niuhuan/pikapika)|美观易用且无广告的绅士漫画客户端, 同时适配手机和PC (Windows/Mac), 类似哔咔漫画。|381|Dart|2021/12/01|
|42|[lwlizhe/flutter_novel](https://github.com/lwlizhe/flutter_novel)|仿追书神器，具有仿真、滑动和滚动翻页、字体大小、行高、背景、目录等功能的Flutter 阅读APP|377|Dart|2021/11/22|
|43|[qq326646683/flutter_tencentplayer](https://github.com/qq326646683/flutter_tencentplayer)|支持视频、直播源播放；边下边播放；清晰度、播放速度设置;离线下载；支持下载断点续传|368|Dart|2021/11/21|
|44|[togettoyou/flutter-one-app](https://github.com/togettoyou/flutter-one-app)|🎊 Flutter 仿「ONE·一个」APP|327|Dart|2021/08/09|
|45|[cairuoyu/flutter_admin](https://github.com/cairuoyu/flutter_admin)|Flutter Admin: 一个基于 Flutter 的后台管理系统、开发模板。A backend management system and development template based on Flutter|320|Dart|2021/11/11|
|46|[fmtjava/Flutter_Eyepetizer](https://github.com/fmtjava/Flutter_Eyepetizer)|Flutter + 组件化实现的一款精美的仿开眼视频(Eyepetizer )跨平台App,适合入门,快速掌握Dart语言以及上手flutter开发(提供Kotlin、React Native版本 😁 ),希望和大家共同成长,喜欢的话，欢迎start或fork!|296|Dart|2021/10/26|
|47|[bladeofgod/Bedrock](https://github.com/bladeofgod/Bedrock)|一款基于MVVM+Provider的快速开发框架。|296|Dart|2021/11/09|
|48|[xieyezi/flutter-shopping-AiRi](https://github.com/xieyezi/flutter-shopping-AiRi)|一款基于Flutter开发的购物App，涵盖了购物App的常见功能|295|Dart|2021/07/01|
|49|[xdd666t/flutter_use](https://github.com/xdd666t/flutter_use)|some trick used by flutter     Flutter使用的一些骚操作|279|Dart|2021/11/25|
|50|[githubliruiyuan/HybridFlutter](https://github.com/githubliruiyuan/HybridFlutter)|Flutter + V8/JsCore 动态化|275|Dart|2021/10/06|
|51|[flutterchina/lpinyin](https://github.com/flutterchina/lpinyin)|Dart 汉字转拼音，Flutter, web, other|271|Dart|2021/05/11|
|52|[fluttercandies/flutter_smart_dialog](https://github.com/fluttercandies/flutter_smart_dialog)|An elegant Flutter Dialog solution   一种更优雅的 Flutter Dialog 解决方案|270|Dart|2021/11/26|
|53|[best-flutter/flutter_amap_location](https://github.com/best-flutter/flutter_amap_location)|高德地图flutter定位组件|264|Dart|2021/11/15|
|54|[wkl007/flutter_trip](https://github.com/wkl007/flutter_trip)|Flutter 仿携程网 App，支持 Android 与 iOS 双平台|252|Dart|2021/07/14|
|55|[lizhuoyuan/flutter_study](https://github.com/lizhuoyuan/flutter_study)|flutter 学习记录|248|Dart|2021/08/12|
|56|[jiang111/flutter_code](https://github.com/jiang111/flutter_code)|b站视频教程： https://space.bilibili.com/480410119/ |245|Dart|2021/10/25|
|57|[flutterchina/nine_grid_view](https://github.com/flutterchina/nine_grid_view)|Flutter NineGridView & DragSortView. Similar to Weibo / WeChat nine grid view controls to display pictures. Flutter仿微信/微博九宫格、拖拽排序，微信群组，钉钉群组，QQ讨论组头像。|224|Dart|2021/05/10|
|58|[DevYao/flutter_wechat_clone](https://github.com/DevYao/flutter_wechat_clone)|flutter 学习 demo|218|Dart|2021/09/24|
|59|[leetomlee123/book](https://github.com/leetomlee123/book)|笔趣阁源基于flutter的小说阅读app|206|Dart|2021/11/29|
|60|[JiangJuHong/FlutterTencentImPlugin](https://github.com/JiangJuHong/FlutterTencentImPlugin)|腾讯即时通讯(IM)，Flutter插件|197|Dart|2021/11/26|
|61|[RxReader/alipay_kit](https://github.com/RxReader/alipay_kit)|flutter版支付宝登录/支付|197|Dart|2021/09/23|
|62|[user1121114685/Wallpaper_Engine](https://github.com/user1121114685/Wallpaper_Engine)|一个便捷的创意工坊下载器|196|Dart|2021/11/09|
|63|[softfatgay/flutter-netease](https://github.com/softfatgay/flutter-netease)|深度还原网易严选webApp，Flutter项目，接口为真实数据。项目已完善|191|Dart|2021/11/25|
|64|[Yuzopro/opengit_flutter](https://github.com/Yuzopro/opengit_flutter)|OpenGit基于Flutter的Github客户端，支持Android和iOS。项目中涉及到BloC、Redux、国际化、多主题以及Github相关信息的查看等。|184|Dart|2021/07/26|
|65|[easemob/im_flutter_sdk](https://github.com/easemob/im_flutter_sdk)|环信im flutter sdk,  example中包含ui代码.|184|Dart|2021/11/26|
|66|[designDo/flutter-checkio](https://github.com/designDo/flutter-checkio)|How time flies.一款开源习惯打卡APP，流畅的动画体验，Bloc实现状态管理，主题(颜色)切换，字体切换，数据库管理等。|179|Dart|2021/10/28|
|67|[Cyenoch/Flutter-Coolapk](https://github.com/Cyenoch/Flutter-Coolapk)|flutter coolapk, 酷安 Flutter版（第三方）酷安, 酷安Windows版, 酷安Linux版|177|Dart|2021/06/02|
|68|[zhaolongs/flutter-ho](https://github.com/zhaolongs/flutter-ho)|flutter 快速开发模版、脚手架|174|Dart|2021/11/17|
|69|[iota9star/mikan_flutter](https://github.com/iota9star/mikan_flutter)|Flutter 驱动的三方 蜜柑计划（https://mikanani.me） APP，:construction: 开发中...|168|Dart|2021/11/07|
|70|[RxReader/tencent_kit](https://github.com/RxReader/tencent_kit)|flutter版QQ登录/分享|163|Dart|2021/09/29|
|71|[ShaunRain/flutter_tindercard](https://github.com/ShaunRain/flutter_tindercard)|A Tinder(探探) Card Widget build with flutter.|161|Dart|2021/08/31|
|72|[DingMouRen/flutter_tiktok](https://github.com/DingMouRen/flutter_tiktok)|Flutter Tiktok 抖音实战  🍰🍓🍖🍟🍕🍔|159|Dart|2021/09/18|
|73|[chenxianqi/kefu_server](https://github.com/chenxianqi/kefu_server)|基于MIMC小米消息云实现的客服系统|148|Dart|2021/10/13|
|74|[gstory0404/flutter_unionad](https://github.com/gstory0404/flutter_unionad)|字节跳动 穿山甲广告SDK Bytedance-UnionAD flutter版本插件|128|Dart|2021/11/17|
|75|[simplezhli/flutter_2d_amap](https://github.com/simplezhli/flutter_2d_amap)|Flutter 高德2D地图插件（支持Android、iOS、Web）|128|Dart|2021/11/29|
|76|[2697a/bujuan](https://github.com/2697a/bujuan)|Flutter三方网易云|127|Dart|2021/06/28|
|77|[ducafecat/getx_quick_start](https://github.com/ducafecat/getx_quick_start)|getx 功能整理: 路由、中间件、组件、状态、多语言、样式|126|Dart|2021/07/24|
|78|[OpenIMSDK/Open-IM-SDK-Flutter](https://github.com/OpenIMSDK/Open-IM-SDK-Flutter)|OpenIM：由前微信技术专家打造的基于 Go 实现的即时通讯（IM）项目，Flutter版本IM SDK 可以轻松替代第三方IM云服务，打造具备聊天、社交功能的app。|123|Dart|2021/12/01|
|79|[GetuiLaboratory/getui-flutter-plugin](https://github.com/GetuiLaboratory/getui-flutter-plugin)|个推官方提供的推送SDK Flutter 插件（支持 Android & iOS）|110|Dart|2021/11/30|
|80|[ikimiler/flutter-vjujiao](https://github.com/ikimiler/flutter-vjujiao)|Flutter构建的跨平台聚合阅读App，起名V聚焦|109|Dart|2021/06/18|
|81|[xiaojia21190/ZY_Player_flutter](https://github.com/xiaojia21190/ZY_Player_flutter)|ZY Player Flutter 资源播放器|106|Dart|2021/10/29|
|82|[Ha2ryZhang/alltv_flutter](https://github.com/Ha2ryZhang/alltv_flutter)|alltv_flutter 一览全网主播|106|Dart|2021/09/23|
|83|[longer96/flutter_pickers](https://github.com/longer96/flutter_pickers)|flutter 选择器库，包括日期及时间选择器（可设置范围）、单项选择器（可用于性别、民族、学历、星座、年龄、身高、体重、温度等）、城市地址选择器（分省级、地级及县级）、多项选择器等…… 欢迎Fork & pr贡献您的代码，大家共同学习|104|Dart|2021/11/22|
|84|[Mayandev/django_morec](https://github.com/Mayandev/django_morec)|🎬一个非常精美的电影推荐应用，使用 Flutter 与 Django 进行构建，可根据用户收藏的电影、演员、标签，定时生成推荐列表以及相应的推荐解释。|103|Dart|2021/06/11|
|85|[pheromone/Flutter_learn_demo](https://github.com/pheromone/Flutter_learn_demo)|Flutter_learn_demo  Flutter学习历程|101|Dart|2021/11/26|
|86|[flutter-dev/asset_generator](https://github.com/flutter-dev/asset_generator)|根据pubspec.yaml中设置的目录模板自动向其中添加文件记录的简单脚本|99|Dart|2021/07/27|
|87|[18824863285/BaseFlutter](https://github.com/18824863285/BaseFlutter)|Provider MVVM的最佳实践，最适合新手学习的flutter开源项目|99|Dart|2021/07/30|
|88|[yangchong211/YCFlutterUtils](https://github.com/yangchong211/YCFlutterUtils)|Flutter Utils 全网最齐全的工具类。包含bus，颜色，日期，文件，json，log，sp，加解密，num，图片，网络，正则，验证，路由，文本，时间，spi，计时器，拓展类，编解码，发射，异常，字节转化，解析等等工具类。|99|Dart|2021/08/13|
|89|[gedoor/YueDuFlutter](https://github.com/gedoor/YueDuFlutter)|阅读Flutter版|96|Dart|2021/11/22|
|90|[nullptrX/pangle_flutter](https://github.com/nullptrX/pangle_flutter)|Flutter版穿山甲SDK，支持Android、iOS。A Flutter plugin that supports Pangle SDK on Android and iOS.|96|Dart|2021/11/19|
|91|[RandyWei/flt_video_player](https://github.com/RandyWei/flt_video_player)|基于腾讯云点播播放器的Flutter插件|94|Dart|2021/11/30|
|92|[tp7309/flutter_sticky_and_expandable_list](https://github.com/tp7309/flutter_sticky_and_expandable_list)|粘性头部与分组列表Sliver实现 Build a grouped list, which support expand/collapse section and sticky headers, support use it with sliver widget.|89|Dart|2021/11/29|
|93|[fluttercandies/flutter_filereader](https://github.com/fluttercandies/flutter_filereader)|Flutter实现的本地文件(pdf word excel 等)查看插件,非在线预览|84|Dart|2021/07/23|
|94|[Nayuta403/fps_monitor](https://github.com/Nayuta403/fps_monitor)|屏幕流畅度检测工具|81|Dart|2021/05/05|
|95|[niezhiyang/flutter_autosize_screen](https://github.com/niezhiyang/flutter_autosize_screen)|A low-cost Flutter screen adaptation solution（一个极低成本的 Flutter 屏幕适配方案）|80|Dart|2021/10/19|
|96|[abcd498936590/Dart-Cms-Flutter](https://github.com/abcd498936590/Dart-Cms-Flutter)|这是一个使用flutter技术开发的Dart-Cms的安卓客户端|80|Dart|2021/10/23|
|97|[RxReader/weibo_kit](https://github.com/RxReader/weibo_kit)|flutter版新浪微博登录/分享|79|Dart|2021/07/31|
|98|[ifgyong/flutter_easyHub](https://github.com/ifgyong/flutter_easyHub)|🔥🔥🔥简单易用的toast动画，支持iOS和android，支持widget添加，纯flutter，现在有近30种动画可供选择。Simple and easy toast animation, supports iOS and android, supports widget addition, pure flutter, now there are nearly 30 kinds of animations to choose from。|75|Dart|2021/09/27|
|99|[xiedong11/flutter_app](https://github.com/xiedong11/flutter_app)|Flutter进阶之旅专栏|74|Dart|2021/09/15|
|100|[iconfont-cli/flutter-iconfont-cli](https://github.com/iconfont-cli/flutter-iconfont-cli)|在flutter中使用iconfont图标，不依赖字体，支持多色彩|71|Dart|2021/05/19|
|101|[BuglyDevTeam/Bugly-Flutter-Plugin](https://github.com/BuglyDevTeam/Bugly-Flutter-Plugin)|Flutter工程可以通过集成此插件实现bugly crash捕获、上报的功能|70|Dart|2021/06/08|
|102|[huage2580/yuedu_hd](https://github.com/huage2580/yuedu_hd)|阅读hd_flutter|68|Dart|2021/11/25|
|103|[wuweijian1997/FlutterCountdownTimer](https://github.com/wuweijian1997/FlutterCountdownTimer)|A flutter countdown timer. [10 days 5:30:46] ⬇⬇⬇⬇   flutter倒计时|67|Dart|2021/07/04|
|104|[sunyongsheng/Allpass](https://github.com/sunyongsheng/Allpass)|Allpass是一款简洁的私密信息管理工具，包括密码管理与卡片信息管理，支持指纹解锁软件、csv导入导出、从Chrome中导入、从剪贴板中导入、文件夹与标签功能、收藏与备注功能、密码生成器、WebDAV同步等功能，采用Flutter构建|67|Dart|2021/09/14|
|105|[fluttercandies/left-scroll-actions](https://github.com/fluttercandies/left-scroll-actions)|Flutter的左滑删除组件|65|Dart|2021/11/26|
|106|[mapleafgo/clash-for-flutter](https://github.com/mapleafgo/clash-for-flutter)|Clash的桌面客户端，支持 windows、linux、macos|65|Dart|2021/08/04|
|107|[longer96/flutter-demo](https://github.com/longer96/flutter-demo)|🔥Flutter 那些花里胡哨的界面🔥，花里胡哨的底部导航，仿抖音、小红书底部菜单，向上展开菜单，半圆菜单，中间凹进去的菜单，动画底部导航|64|Dart|2021/10/14|
|108|[lianyagang/flutter_swiper_null_safety](https://github.com/lianyagang/flutter_swiper_null_safety)|根据大佬https://github.com/best-flutter/flutter_swiper修改到空安全swiper|62|Dart|2021/08/04|
|109|[NewPracticer/Flutter-Dart-DataStructure](https://github.com/NewPracticer/Flutter-Dart-DataStructure)|使用flutter的dart语言重写数据结构与算法。包括线性搜索、选择排序、插入排序、栈，队列，链表、递归、归并排序、快速排序、二分搜索、二分搜索树、集合 和 映射、堆、优先队列、冒泡排序、希尔排序、线段树、Trie字典树、并查集、AVL树、红黑树、哈希表、计数排序、LSD基数排序、MSD排序，桶排序、字符串匹配、图的邻接矩阵、邻接表，深度优先遍历及应用|59|Dart|2021/09/24|
|110|[yangchong211/YCHybridFlutter](https://github.com/yangchong211/YCHybridFlutter)|Android和flutter混合开发案例|57|Dart|2021/08/16|
|111|[DanXi-Dev/DanXi](https://github.com/DanXi-Dev/DanXi)|[Windows / Mac / Android / iOS] Maybe the best all-rounded service app for Fudan University students.  可能是复旦学生最好的第三方校园服务APP。|56|Dart|2021/12/01|
|112|[kangshaojun/dart-book](https://github.com/kangshaojun/dart-book)|Dart语言实战-基于Flutter框架开发-第二版 随书源码|55|Dart|2021/07/18|
|113|[abcd498936590/fijkplayer_skin](https://github.com/abcd498936590/fijkplayer_skin)|fijkplayer的一款基础皮肤，美化播放UI，增加顶部返回菜单、标题，透明渐变皮肤。支持手势快进进退|54|Dart|2021/10/30|
|114|[abcd498936590/flutter_eyepetizer](https://github.com/abcd498936590/flutter_eyepetizer)|使用 Flutter + GetX 仿开眼视频app|52|Dart|2021/11/01|
|115|[xxxDeveloper/flutter-adapter](https://github.com/xxxDeveloper/flutter-adapter)|📱基于flutter的屏幕适配方案 / screen adaptation scheme based on flutter|52|Dart|2021/10/10|
|116|[mpflutter/mpflutter](https://github.com/mpflutter/mpflutter)|MPFlutter 是一个跨平台 Flutter 开发框架，可用于微信小程序以及 Web 应用开发。|51|Dart|2021/12/01|
|117|[AriesHoo/flutter_readhub](https://github.com/AriesHoo/flutter_readhub)|Freadhub-使用Flutter为Readhub非官方客户端;主要囊括以下功能：1、热门话题、科技动态、开发者、区块链四大模块；2、相关聚合资讯快捷查看；3、方便快捷的浅色/深色模式切换；4、丰富的彩虹颜色主题/每日主题切换；5、长按社会化分享预览图效果模式；6、方便快捷的意见反馈入口|50|Dart|2021/09/10|
|118|[jhflovehqy/flutter_bolg_manage](https://github.com/jhflovehqy/flutter_bolg_manage)|Flutter实战项目，采用Getx框架管理，遵循Material design设计风格，适合您实战参考或练手|50|Dart|2021/11/01|
|119|[hahafather007/flutter_weather](https://github.com/hahafather007/flutter_weather)|🔥🔥🔥Flutter最美天气预报，多种自定义view动画，MaterialDesign风格🔥🔥🔥|49|Dart|2021/09/03|
|120|[KagurazakaHanabi/dailypics](https://github.com/KagurazakaHanabi/dailypics)|图鉴日图 - 精选壁纸推荐|49|Dart|2021/07/08|
|121|[finogeeks/finclip-flutter-demo](https://github.com/finogeeks/finclip-flutter-demo)|FinClip Flutter 运行环境，让小程序在 Flutter 应用中无缝运行  / Flutter DEMO for FinClip|46|Dart|2021/11/14|
|122|[yy1300326388/flutter_drink_login_app](https://github.com/yy1300326388/flutter_drink_login_app)|Speed Code 2 - 漂亮的饮料食谱 App|45|Dart|2021/05/08|
|123|[xiao-cao-x/pixiv_func_android](https://github.com/xiao-cao-x/pixiv_func_android)|功能齐全的Pixiv第三方客户端 支持免代理 支持查看动图|45|Dart|2021/11/30|
|124|[UvDream/flutter_netease_cloud_music](https://github.com/UvDream/flutter_netease_cloud_music)|网易云音乐IOS高仿 (Flutter版本)|44|Dart|2021/11/16|
|125|[cliclitv/clicli-dark](https://github.com/cliclitv/clicli-dark)|:zap: CliCli Dark. clicli 纯黑|43|Dart|2021/11/09|
|126|[asjqkkkk/flutter-blog](https://github.com/asjqkkkk/flutter-blog)|💻使用flutter完成的个人web博客.|42|Dart|2021/06/01|
|127|[MobClub/MobPush-For-Flutter](https://github.com/MobClub/MobPush-For-Flutter)|MobPush的Flutter插件|42|Dart|2021/11/18|
|128|[ChessLuo/flutter_study_app](https://github.com/ChessLuo/flutter_study_app)|🔥勤能补拙，我希望能利用业余时间去学习及总结一些有关flutter的知识并运用到项目中去，Come on！！！|41|Dart|2021/08/01|
|129|[qq326646683/interactiveviewer_gallery](https://github.com/qq326646683/interactiveviewer_gallery)|Enhancements to interactiveviewer: picture and video preview, support two-finger gesture zoom, double-click to zoom, switch left and right。图片预览，视频预览，图片视频混合预览|40|Dart|2021/06/19|
|130|[githubityu/flutter_jdshop_mvvm](https://github.com/githubityu/flutter_jdshop_mvvm)|跟着大地老师学习的,模仿京东的一个实战项目，但是根据实际项目做了许多修改和优化|40|Dart|2021/07/27|
|131|[zegoim/zego-flutter-sdk](https://github.com/zegoim/zego-flutter-sdk)|即构科技音视频Flutter SDK|39|Dart|2021/11/30|
|132|[HenryQuan/AnimeOne](https://github.com/HenryQuan/AnimeOne)|anime1的非官方APP|38|Dart|2021/09/08|
|133|[xdd666t/flutter_wan](https://github.com/xdd666t/flutter_wan)|flutter版玩android，页面基本均是fish_redux搭建|38|Dart|2021/09/25|
|134|[BayMikyou/EyeVideo-Flutter](https://github.com/BayMikyou/EyeVideo-Flutter)|这是一款使用Flutter开发的高仿开眼视频APP的短视频APP, 里面使用了Flutter的组件、Dio网络请求的封装、Bloc状态管理以及UI和逻辑分离|37|Dart|2021/05/25|
|135|[tencentyun/TencentIMFlutterDemo](https://github.com/tencentyun/TencentIMFlutterDemo)|腾讯云即时通信IMdemo|37|Dart|2021/11/11|
|136|[SBDavid/flutter_sliver_tracker](https://github.com/SBDavid/flutter_sliver_tracker)|flutter滑动曝光埋点框架，支持SliverList、SliverGrid|36|Dart|2021/06/07|
|137|[alberliu/fim](https://github.com/alberliu/fim)|flutter写的IM移动客户端|36|Dart|2021/10/22|
|138|[Tecode/flutter_widget](https://github.com/Tecode/flutter_widget)|Flutter视频播放，自适应高度轮播图，视频作为背景的登录页面|35|Dart|2021/08/18|
|139|[c1avie/TRTCSimpleDemo](https://github.com/c1avie/TRTCSimpleDemo)|基于腾讯云flutter sdk，提供的Demo|35|Dart|2021/10/18|
|140|[nightmare-space/adb_tool](https://github.com/nightmare-space/adb_tool)|ADB TOOL，提供 adb 管理，android 端 adb 安装，开启远程调试。|34|Dart|2021/11/30|
|141|[NKUST-ITC/NKUST-AP-Flutter](https://github.com/NKUST-ITC/NKUST-AP-Flutter)|高科校務通 made by Flutter|34|Dart|2021/10/04|
|142|[koharubiyori/Moegirl-plus](https://github.com/koharubiyori/Moegirl-plus)|萌娘百科第三方安卓客户端。（已不再维护）|33|Dart|2021/10/24|
|143|[CustedNG/CustedNG](https://github.com/CustedNG/CustedNG)|Custed NG，致力于你的校园生活|31|Dart|2021/11/23|
|144|[wo5813288/wan_giao](https://github.com/wo5813288/wan_giao)|学习flutter，所以系统的做一款应用来实践一下。这款应用也开发了很多内容了，后续还要继续更新功能。开发这个项目主要也是熟悉flutter的树形结构的写法和UI组件，项目中也用到了flutter比较流行的框架。|30|Dart|2021/11/03|
|145|[CxmyDev/screen_autosize](https://github.com/CxmyDev/screen_autosize)|🔥 A low-cost Flutter screen adaptation solution (类似今日头条屏幕适配方案，一个极低成本的 Flutter 屏幕适配方案)。|30|Dart|2021/06/20|
|146|[fangkyi03/flutter-rlstyle](https://github.com/fangkyi03/flutter-rlstyle)|在flutter 里面使用react语法写样式|30|Dart|2021/07/06|
|147|[Termare/termare_app](https://github.com/Termare/termare_app)|使用Flutter开发的多平台终端模拟器，Termare 类似于 Termux ，在安卓端有独立的源，但支持多个平台，还在大量的开发与测试中~|29|Dart|2021/11/18|
|148|[tec8297729/flutter_flexible](https://github.com/tec8297729/flutter_flexible)|flutter脚手架，让你更专注UI层|29|Dart|2021/11/22|
|149|[ma125120/flutter_my_picker](https://github.com/ma125120/flutter_my_picker)|适用于flutter的有关日期和时间的选择器，支持年份(showYearPicker)、月份(showMonthPicker)、日期(showDatePicker)、时间(showTimePicker)、日期时间(showDateTimePicker)等。|28|Dart|2021/10/13|
|150|[ifgyong/flutter-example](https://github.com/ifgyong/flutter-example)|120多种小部件的使用例子，根据 flutter-book的例子,可以直接运行的哦|28|Dart|2021/08/12|

⬆ [回到目录](#内容目录)

<br/>

## TeX

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[lib-pku/libpku](https://github.com/lib-pku/libpku)|贵校课程资料民间整理|24.9k|TeX|2021/11/01|
|2|[soulmachine/leetcode](https://github.com/soulmachine/leetcode)|LeetCode题解，151道题完整版|10.1k|TeX|2021/06/05|
|3|[billryan/resume](https://github.com/billryan/resume)|An elegant \LaTeX\ résumé template. 大陆镜像 https://gods.coding.net/p/resume/git|4.9k|TeX|2021/09/05|
|4|[zhanwen/MathModel](https://github.com/zhanwen/MathModel)|研究生数学建模，本科生数学建模、数学建模竞赛优秀论文，数学建模算法，LaTeX论文模板，算法思维导图，参考书籍，Matlab软件教程，PPT|3.8k|TeX|2021/10/18|
|5|[sjtug/SJTUThesis](https://github.com/sjtug/SJTUThesis)|上海交通大学 XeLaTeX 学位论文及课程论文模板   Shanghai Jiao Tong University XeLaTeX Thesis Template|2.3k|TeX|2021/11/27|
|6|[NiuTrans/MTBook](https://github.com/NiuTrans/MTBook)|《机器翻译：基础与模型》肖桐 朱靖波 著 - Machine Translation: Foundations and Models|2.3k|TeX|2021/11/25|
|7|[jindongwang/transferlearning-tutorial](https://github.com/jindongwang/transferlearning-tutorial)|《迁移学习简明手册》LaTex源码|2.1k|TeX|2021/05/27|
|8|[foxsen/archbase](https://github.com/foxsen/archbase)|教科书《计算机体系结构基础》（胡伟武等，第三版）的开源版本|1.5k|TeX|2021/11/29|
|9|[wuzhouhui/awk](https://github.com/wuzhouhui/awk)|The AWK Programming Language (AWK 程序设计语言, awkbook) 中文翻译, LaTeX 排版|962|TeX|2021/11/21|
|10|[dustincys/hithesis](https://github.com/dustincys/hithesis)|嗨！thesis！哈尔滨工业大学毕业论文LaTeX模板|890|TeX|2021/11/29|
|11|[x-magus/ThesisUESTC](https://github.com/x-magus/ThesisUESTC)|ThesisUESTC-电子科技大学毕业论文模板|678|TeX|2021/09/06|
|12|[BHOSC/BUAAthesis](https://github.com/BHOSC/BUAAthesis)|北航毕设论文LaTeX模板|607|TeX|2021/11/29|
|13|[MeouSker77/Cpp17](https://github.com/MeouSker77/Cpp17)|本书为《C++17 the complete guide》的个人中文翻译，仅供学习和交流使用，侵删|561|TeX|2021/08/15|
|14|[ice1000/resume](https://github.com/ice1000/resume)|:space_invader: My resume / 我的简历|543|TeX|2021/11/08|
|15|[latexstudio/CUMCMThesis](https://github.com/latexstudio/CUMCMThesis)|全国大学生数学建模竞赛LaTeX论文模板 已经适配到 2020 年格式|462|TeX|2021/07/13|
|16|[hushidong/biblatex-gb7714-2015](https://github.com/hushidong/biblatex-gb7714-2015)|A biblatex implementation of the GB/T7714-2015 bibliography style     GB/T 7714-2015 参考文献著录和标注的biblatex样式包|404|TeX|2021/10/30|
|17|[moesoha/debian-media-box](https://github.com/moesoha/debian-media-box)|“Debian 小药盒”，一个用来包装 Debian 安装介质的盒子设计和介绍用的说明书。|378|TeX|2021/08/15|
|18|[whutug/whu-thesis](https://github.com/whutug/whu-thesis)|:memo: 武汉大学毕业论文 LaTeX 模版 2021|340|TeX|2021/06/19|
|19|[sheng-qiang/BUPTBachelorThesis](https://github.com/sheng-qiang/BUPTBachelorThesis)|A LaTeX Template for BUPT Bachelor Thesis (updated in 2018) 北京邮电大学学士学位论文LaTeX模板|336|TeX|2021/05/02|
|20|[SamZhangQingChuan/Editorials](https://github.com/SamZhangQingChuan/Editorials)|算法题解& 教程|275|TeX|2021/07/11|
|21|[CheckBoxStudio/BUAAThesis](https://github.com/CheckBoxStudio/BUAAThesis)|北航研究生学位论文模板（Word+LaTeX）.|240|TeX|2021/10/13|
|22|[njuHan/njuthesis-nju-thesis-template](https://github.com/njuHan/njuthesis-nju-thesis-template)|南京大学学位论文(本科/硕士/博士)，毕业论文LaTeX模板|223|TeX|2021/06/15|
|23|[sjtug/SJTUBeamer](https://github.com/sjtug/SJTUBeamer)|上海交通大学 Beamer 模版   Beamer template for Shanghai Jiao Tong University|185|TeX|2021/12/01|
|24|[nuaatug/nuaathesis](https://github.com/nuaatug/nuaathesis)|LaTeX document class for NUAA, supporting bachelor/master/PH.D thesis in Chinese/English/Japanese. 南航本科、硕士、博士学位论文 LaTeX 模板|177|TeX|2021/11/04|
|25|[OsbertWang/install-latex-guide-zh-cn](https://github.com/OsbertWang/install-latex-guide-zh-cn)|一份简短的关于 LaTeX 安装的介绍|165|TeX|2021/12/01|
|26|[nanmu42/CQUThesis](https://github.com/nanmu42/CQUThesis)|:pencil: 重庆大学毕业论文LaTeX模板---LaTeX Thesis Template for Chongqing University|158|TeX|2021/06/06|
|27|[SYSU-SCC/sysu-thesis](https://github.com/SYSU-SCC/sysu-thesis)|中山大学 LaTeX 论文项目模板|154|TeX|2021/05/16|
|28|[annProg/PanBook](https://github.com/annProg/PanBook)|Pandoc LaTeX，Epub模板，用于生成书籍，幻灯片(beamer)，简历，论文等（cv, thesis, ebook,beamer)|142|TeX|2021/09/15|
|29|[TouchFishPioneer/SEU-Beamer-Slide](https://github.com/TouchFishPioneer/SEU-Beamer-Slide)|东南大学幻灯片模板（豪华版）:clipboard:|132|TeX|2021/10/15|
|30|[Aetf/xjtuthesis](https://github.com/Aetf/xjtuthesis)| 使用 LaTeX 排版学位论文！适用于西安交通大学学生|119|TeX|2021/09/23|
|31|[xiaoweiChen/Learn-LLVM-12](https://github.com/xiaoweiChen/Learn-LLVM-12)|《Learn LLVM 12》的非专业个人翻译|111|TeX|2021/10/05|
|32|[disc0ver-csu/csu-thesis](https://github.com/disc0ver-csu/csu-thesis)|中南大学学术论文LaTex模板。Central South University Thesis LaTeX Template.|102|TeX|2021/06/07|
|33|[nju-lug/NJUThesis](https://github.com/nju-lug/NJUThesis)|南京大学学位论文LaTeX模板|102|TeX|2021/12/01|
|34|[wangrongwei/omnetpp_primer](https://github.com/wangrongwei/omnetpp_primer)|OMNeT++的仿真手册|100|TeX|2021/07/21|
|35|[openbiox/Cookbook-for-R-Chinese](https://github.com/openbiox/Cookbook-for-R-Chinese)| :books:Cookbook for R  中文翻译|97|TeX|2021/08/18|
|36|[sppmg/TW_Thesis_Template](https://github.com/sppmg/TW_Thesis_Template)|The LaTeX Template for TW Thesis 台灣碩博士 LaTeX 論文樣板|87|TeX|2021/10/05|
|37|[note286/xdupgthesis](https://github.com/note286/xdupgthesis)|西安电子科技大学研究生学位论文XeLaTeX模板|84|TeX|2021/12/01|
|38|[yuxtech/ShiJihuai-complex](https://github.com/yuxtech/ShiJihuai-complex)|史济怀复变函数LaTeX重排|76|TeX|2021/11/10|
|39|[Didnelpsun/Math](https://github.com/Didnelpsun/Math)|考研数学，数学一，包括高等数学、线性代数、概率统计|70|TeX|2021/11/20|
|40|[TomHeaven/nudt_thesis](https://github.com/TomHeaven/nudt_thesis)|NUDT硕士博士毕业论文latex模板|70|TeX|2021/06/05|
|41|[kanition/pbrtbook](https://github.com/kanition/pbrtbook)|pbrt 中文整合翻译 基于物理的渲染：从理论到实现 Physically Based Rendering: From Theory To Implementation|68|TeX|2021/11/20|
|42|[shuosc/SHU-Bachelor-Thesis-OSC](https://github.com/shuosc/SHU-Bachelor-Thesis-OSC)|上海大学本科生毕业论文latex模板-开源社区版本。|67|TeX|2021/05/24|
|43|[zhcosin/elementary-math](https://github.com/zhcosin/elementary-math)|初等数学笔记，LaTeX 排版，内容涉及代数、数论、几何、组合等。|67|TeX|2021/08/06|
|44|[megrxu/zju-report-latex-template](https://github.com/megrxu/zju-report-latex-template)|浙江大学实验报告模板|64|TeX|2021/05/28|
|45|[NWPUMetaphysicsOffice/Yet-Another-LaTeX-Template-for-NPU-Thesis](https://github.com/NWPUMetaphysicsOffice/Yet-Another-LaTeX-Template-for-NPU-Thesis)|西北工业大学硕博学位论文模版   Yet Another Thesis Template for Northwestern Polytechnical University|63|TeX|2021/09/08|
|46|[iydon/sustechthesis](https://github.com/iydon/sustechthesis)|南方科技大学本科生毕业论文 LaTeX 模板（Southern University of Science and Technology Thesis Template LaTeX Template）|59|TeX|2021/06/12|
|47|[kevinleeex/scu_thesis_2020](https://github.com/kevinleeex/scu_thesis_2020)| 四川大学研究生学位论文LaTeX模版(复刻官方word模版)Here you go!|59|TeX|2021/11/27|
|48|[qiziqiang/MathematicalModeling](https://github.com/qiziqiang/MathematicalModeling)|关于数学建模的一些资料和我自己的数模论文|58|TeX|2021/05/22|
|49|[LogCreative/SJTUBeamermin](https://github.com/LogCreative/SJTUBeamermin)|上海交通大学 LaTeX Beamer 幻灯片模板 - VI 最小工作集|57|TeX|2021/11/05|
|50|[sikouhjw/gdutthesis](https://github.com/sikouhjw/gdutthesis)|广东工业大学 LaTeX 论文模板|54|TeX|2021/12/01|
|51|[NPUSCG/nputhesis](https://github.com/NPUSCG/nputhesis)|西北工业大学硕士/博士学位论文 LaTeX 模板|53|TeX|2021/05/10|
|52|[yufree/sciguide](https://github.com/yufree/sciguide)|现代科研指北|53|TeX|2021/11/07|
|53|[a171232886/TJUThesis_master_2021](https://github.com/a171232886/TJUThesis_master_2021)|天大博士/硕士学位论文Latex模板，根据2021年版要求修改，可直接在Overleaf上运行|53|TeX|2021/11/17|
|54|[TomHeaven/nudtproposal](https://github.com/TomHeaven/nudtproposal)|NUDT硕士博士研究生开题报告latex模板|49|TeX|2021/09/01|
|55|[wengan-li/ncku-thesis-template-latex](https://github.com/wengan-li/ncku-thesis-template-latex)|NCKU Thesis/Dissertation Template in Latex   台灣國立成功大學碩博士用畢業論文LaTex模板|49|TeX|2021/06/14|
|56|[zoam/xmu-thesis-grd](https://github.com/zoam/xmu-thesis-grd)|厦门大学研究生学位论文 LaTeX 模板|47|TeX|2021/05/28|
|57|[MacroUniverse/PhysWiki-log](https://github.com/MacroUniverse/PhysWiki-log)|小时百科|47|TeX|2021/11/29|
|58|[CNYuYang/My-19-years-old](https://github.com/CNYuYang/My-19-years-old)|记录自己十九岁那年参加电子设计大赛的短文📚|42|TeX|2021/08/08|
|59|[netcan/HFUT_Thesis](https://github.com/netcan/HFUT_Thesis)|合肥工业大学毕业设计（论文）模板|41|TeX|2021/05/24|
|60|[ylsislove/make-a-little-progress-every-day](https://github.com/ylsislove/make-a-little-progress-every-day)|学无止境，督促自己学习。每天进步一点点，水滴石穿-贵在坚持。|38|TeX|2021/11/24|
|61|[Timozer/CUGThesis](https://github.com/Timozer/CUGThesis)|中国地质大学（武汉）研究生学位论文 TeX 模板。目前仍然在开发和完善中，如果有下载并且使用的同学，请时常记得来这里更新模板。|38|TeX|2021/05/17|
|62|[syvshc/2019Fall_FA](https://github.com/syvshc/2019Fall_FA)|2019秋季学期泛函分析笔记QAQ:unicorn:|37|TeX|2021/09/08|
|63|[bxdd/XdOffer](https://github.com/bxdd/XdOffer)|本项目总结了笔者大学实习和暑期夏令营二十余场面试的经验，旨在指导如何从零开始准备自己的简历、实习和夏令营面试笔试。|36|TeX|2021/05/11|
|64|[DansYU/SeuThesiY](https://github.com/DansYU/SeuThesiY)|用于排版东南大学硕博学位论文的LaTeX模板|35|TeX|2021/05/25|
|65|[longbiaochen/corona-virus](https://github.com/longbiaochen/corona-virus)|一个冠状病毒肺炎传染病学研究数据集|34|TeX|2021/09/30|
|66|[jiafeng5513/JLU_Dissertation](https://github.com/jiafeng5513/JLU_Dissertation)|吉林大学学位论文Latex模板|31|TeX|2021/06/05|
|67|[wtsnwei/pdsp](https://github.com/wtsnwei/pdsp)|《Personal Development for Smart People》的中文自译版|30|TeX|2021/08/13|
|68|[imguozr/NJUPThesis-Bachelor](https://github.com/imguozr/NJUPThesis-Bachelor)|LaTeX template for NJUPT Undergraduate Thesis. 南京邮电大学本科毕业论文LaTeX模版。|30|TeX|2021/05/21|
|69|[mosesnow/LZUthesis](https://github.com/mosesnow/LZUthesis)|兰州大学学位论文LyX模板及LaTeX模板|30|TeX|2021/05/28|
|70|[wxflogic/latex](https://github.com/wxflogic/latex)|中文LaTeX作业模板|30|TeX|2021/11/23|
|71|[yuhldr/LZUThesis2020](https://github.com/yuhldr/LZUThesis2020)|兰州大学2021毕业论文LaTex模板|30|TeX|2021/05/19|
|72|[mengchaoheng/SCUT_thesis](https://github.com/mengchaoheng/SCUT_thesis)|华南理工大学硕博士学位论文LaTeX模板（非官方）|29|TeX|2021/09/18|
|73|[xiaoweiChen/CPP-Move-Semantics](https://github.com/xiaoweiChen/CPP-Move-Semantics)|《C++ Move Semantics》的非专业个人翻译|27|TeX|2021/07/25|
|74|[Yixf-Education/course_Statistics_Story](https://github.com/Yixf-Education/course_Statistics_Story)|天津医科大学，本科选修课，《故事中的统计学》课程资料|26|TeX|2021/11/26|
|75|[thu-ml/tianshou-docs-zh_CN](https://github.com/thu-ml/tianshou-docs-zh_CN)|天授中文文档|26|TeX|2021/09/13|
|76|[oops-sdu/sdu_report](https://github.com/oops-sdu/sdu_report)|LaTeX 模板用于山东大学实验报告|25|TeX|2021/05/29|
|77|[CSUcse/CSUthesis](https://github.com/CSUcse/CSUthesis)|中南大学研究生学位论文LaTex模版（博士和硕士）|24|TeX|2021/11/22|
|78|[hailiang-wang/book-of-tao](https://github.com/hailiang-wang/book-of-tao)|《道德经》是中国历史上春秋时期末期老子写作的论述处世哲学的作品|23|TeX|2021/11/15|
|79|[huangyxi/whutmod](https://github.com/huangyxi/whutmod)|武汉理工大学数学建模培训 LaTeX 模板    A LaTeX Template for Mathematical Modeling Training at Wuhan University of Technology  (WHUT)|22|TeX|2021/10/20|
|80|[SUSTech-CRA/sustech-master-thesis](https://github.com/SUSTech-CRA/sustech-master-thesis)|南方科技大学研究生学位论文LaTeX模板|22|TeX|2021/11/29|
|81|[OChicken/SCUT-Bachelor-Thesis-Template](https://github.com/OChicken/SCUT-Bachelor-Thesis-Template)|Latex template for the bachelor graduation thesis of South China University of Technology (SCUT)  华南理工大学 本科毕业论文LaTeX模板|22|TeX|2021/06/20|
|82|[XinzeZhang/HUST-PhD-Thesis-Latex](https://github.com/XinzeZhang/HUST-PhD-Thesis-Latex)|华中科技大学博士毕业论文Latex模板|22|TeX|2021/07/09|
|83|[claviering/latex-scnu](https://github.com/claviering/latex-scnu)|华师毕业论文模板, 华师本科毕业论文模板, 华师论文模板, latex 模板, 毕业论文模板, SCNU, SCNU 论文模板, SCNU 本科论文模板|21|TeX|2021/06/04|
|84|[xiaoweiChen/Data-Paralle-Cpp](https://github.com/xiaoweiChen/Data-Paralle-Cpp)|个人翻译《Data Parallel C++》|21|TeX|2021/07/08|
|85|[Nick-Hopps/gdutart](https://github.com/Nick-Hopps/gdutart)|广东工业大学毕业设计/论文LaTeX模板|21|TeX|2021/11/18|
|86|[zheyuye/resume-chinese](https://github.com/zheyuye/resume-chinese)|中文LaTeX简历模板|20|TeX|2021/05/06|
|87|[hellckt/SHMTUThesis](https://github.com/hellckt/SHMTUThesis)|上海海事大学 LaTeX 学位论文模板|20|TeX|2021/08/25|
|88|[DeepTrial/ECNU-Dissertations-Latex-Template](https://github.com/DeepTrial/ECNU-Dissertations-Latex-Template)|华东师范大学硕（博）士论文LaTex模板|20|TeX|2021/11/15|
|89|[LucaJiang/Multivariate-Statistics-Self-Learning](https://github.com/LucaJiang/Multivariate-Statistics-Self-Learning)|应用多元统计分析（高慧璇版）自学材料|19|TeX|2021/05/08|
|90|[obster-y/NCEPU-thesis](https://github.com/obster-y/NCEPU-thesis)|An unofficial LaTeX template based on NCEPU degree thesis requirement 一个非官方的华北电力大学学士学位论文LaTeX模板|18|TeX|2021/11/20|
|91|[liziwl/sustech-master-reports](https://github.com/liziwl/sustech-master-reports)|南方科技大学硕士开题报告、年度考核报告 （中/英）|18|TeX|2021/11/30|
|92|[DemerzelSun12/hitletter](https://github.com/DemerzelSun12/hitletter)|一个简单的哈工大信纸模板|17|TeX|2021/09/16|
|93|[ElunDai/gdutthesis](https://github.com/ElunDai/gdutthesis)|广东工业大学LaTeX论文模板项目(非官方)|17|TeX|2021/06/16|
|94|[Koyamin/ECNUThesis-Undergraduate](https://github.com/Koyamin/ECNUThesis-Undergraduate)|华东师范大学 (East China Normal University, ECNU) 本科生学士学位论文 LaTeX 模板|17|TeX|2021/05/11|
|95|[ouc-ocean-group/oucthesis](https://github.com/ouc-ocean-group/oucthesis)|中国海洋大学硕士博士学位论文 LaTeX 模板 Latex template for OUC graduate thesis.|17|TeX|2021/11/23|
|96|[huangyxi/WHUT-Bachelor](https://github.com/huangyxi/WHUT-Bachelor)|武汉理工大学本科生毕业设计（论文） LaTeX 模板    LaTeX Template for Bachelor's Degree Thesis at Wuhan University of Technology (WHUT)|16|TeX|2021/10/20|
|97|[ilcpm/pandoc_markdown2docx_instuction](https://github.com/ilcpm/pandoc_markdown2docx_instuction)|借助pandoc转换，用Markdown写Word论文的指导，基于重庆大学毕业论文排版|16|TeX|2021/05/27|
|98|[latexstudio/APMCMThesis](https://github.com/latexstudio/APMCMThesis)|2021 年 APMCM亚太地区大学生数学建模|15|TeX|2021/11/24|
|99|[iotang/linux_tutorials_for_brand_new_oiers](https://github.com/iotang/linux_tutorials_for_brand_new_oiers)|Linux Tutorials for Brand New Oiers   给信息组学弟学妹的 Linux 入门手把手教程|15|TeX|2021/07/18|
|100|[fangcun010/VkGPUDrivenCNGuide](https://github.com/fangcun010/VkGPUDrivenCNGuide)|基于Vulkan的GPU Driven Rendering教程|15|TeX|2021/11/03|

⬆ [回到目录](#内容目录)

<br/>

## Lua

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[loveshell/ngx_lua_waf](https://github.com/loveshell/ngx_lua_waf)|ngx_lua_waf是一个基于lua-nginx-module(openresty)的web应用防火墙|3.4k|Lua|2021/05/15|
|2|[Wscats/articles](https://github.com/Wscats/articles)|🔖My Learning Notes and Memories - 分享我的学习片段和与你的回忆|3.0k|Lua|2021/10/30|
|3|[jerrykuku/luci-app-vssr](https://github.com/jerrykuku/luci-app-vssr)|HelloWorld是一个以用户最佳主观体验为导向的插件，它支持多种主流协议和多种自定义视频分流服务，拥有精美的操作界面，并配上直观的节点信息。|1.5k|Lua|2021/10/08|
|4|[wongdean/rime-settings](https://github.com/wongdean/rime-settings)|接近原生的鼠须管 Rime 配置|937|Lua|2021/09/25|
|5|[Tinywan/lua-nginx-redis](https://github.com/Tinywan/lua-nginx-redis)|:hibiscus: Redis、Lua、Nginx、OpenResty 笔记和资料|837|Lua|2021/10/26|
|6|[jx-sec/jxwaf](https://github.com/jx-sec/jxwaf)|JXWAF(锦衣盾)是一款开源web应用防火墙|827|Lua|2021/10/12|
|7|[qingmail/rime](https://github.com/qingmail/rime)|Rime 鼠须管（Squirrel）朙月拼音｜小鹤双拼｜自然码双拼配置|725|Lua|2021/11/30|
|8|[kiccer/Soldier76](https://github.com/kiccer/Soldier76)|PUBG - 罗技鼠标宏   兴趣使然的项目，完虐收费宏！点个Star支持一下作者！[PUBG - Logitech mouse macro   Support 12 kinds of guns without recoil!]|707|Lua|2021/09/09|
|9|[Kengxxiao/ArknightsGameData](https://github.com/Kengxxiao/ArknightsGameData)|《明日方舟》游戏数据|626|Lua|2021/11/25|
|10|[scomper/Rime](https://github.com/scomper/Rime)|鼠须管配置|382|Lua|2021/08/16|
|11|[cloudwu/stellaris_cn](https://github.com/cloudwu/stellaris_cn)|Stellaris 群星 汉化 Mod|361|Lua|2021/11/23|
|12|[SivanLaai/rime-pure](https://github.com/SivanLaai/rime-pure)|【rime 小狼毫\trime 同文】手机/PC一站式配置【简约皮肤\拼音搜狗词库\原创trime同文 四叶草 九宫格 拼音方案\四叶草拼音\四叶草地球拼音\小鹤双拼\极品五笔\QQ五笔\徐码\郑码】|212|Lua|2021/10/28|
|13|[luastar/luastar](https://github.com/luastar/luastar)|一个基于openresty的http接口和web开发框架|135|Lua|2021/07/14|
|14|[placeless/squirrel_config](https://github.com/placeless/squirrel_config)|Rime 鼠须管小鹤双拼配置|121|Lua|2021/05/25|
|15|[BlindingDark/rime-easy-en](https://github.com/BlindingDark/rime-easy-en)|Rime / Easy English 英文输入法|119|Lua|2021/06/08|
|16|[26F-Studio/Techmino](https://github.com/26F-Studio/Techmino)|Techmino:方块研究所唯一官方仓库(Github)|95|Lua|2021/12/01|
|17|[jerrykuku/luci-app-ttnode](https://github.com/jerrykuku/luci-app-ttnode)|一个运行在openwrt下的甜糖星愿自动采集插件。|94|Lua|2021/10/24|
|18|[Iorest/rime-setting](https://github.com/Iorest/rime-setting)|rime 输入法配置|92|Lua|2021/10/03|
|19|[freeioe/freeioe](https://github.com/freeioe/freeioe)|FreeIOE is a framework for building IOE (Internet Of Everything) edge-computing gateway 开源的边缘计算网关框架.  讨论群: 291292378|72|Lua|2021/11/27|
|20|[linkease/istore](https://github.com/linkease/istore)|一个支持 Openwrt 标准的软件中心实现。支持其它固件开发者集成到自己的固件里面。更方便小白搜索，下载插件。更方便开发者发布插件。|69|Lua|2021/12/01|
|21|[hunzsig-warcraft3/h-lua](https://github.com/hunzsig-warcraft3/h-lua)|使用 H-Lua SDK 完成魔兽地图制造！高效高能！|61|Lua|2021/10/31|
|22|[tech-microworld/ws-cloud-gateway](https://github.com/tech-microworld/ws-cloud-gateway)|基于 openresty + etcd 实现的轻量级网关服务|59|Lua|2021/11/23|
|23|[kiccer/logitech-macro-frame](https://github.com/kiccer/logitech-macro-frame)|罗技宏框架 - 提炼自 kiccer/Soldier76 项目|58|Lua|2021/08/10|
|24|[tkkcc/arknights](https://github.com/tkkcc/arknights)|《明日方舟》速通 |54|Lua|2021/11/30|
|25|[sniper00/BallGame](https://github.com/sniper00/BallGame)|moon game server的一个使用示例，搭建简单的服务器框架|48|Lua|2021/10/21|
|26|[Seekladoom/Seekladoom-ASS-Effect](https://github.com/Seekladoom/Seekladoom-ASS-Effect)|分享一下鄙人自己做的一些特效字幕，如有写得不当的地方还请多多包涵。|48|Lua|2021/09/04|
|27|[bs10081/Rime-Cx330](https://github.com/bs10081/Rime-Cx330)|適配小鶴雙拼、全拼、五筆：支持部分中英混合輸入、Emoji輸入、簡繁轉換、傳承字標準字轉換、UTF-8 GBK編碼轉換、200萬詞庫覆蓋日常絕大多數場景。|47|Lua|2021/11/05|
|28|[cissusnar/cpluginscript](https://github.com/cissusnar/cpluginscript)|快贴插件|45|Lua|2021/11/10|
|29|[oniondelta/Onion_Rime_Files](https://github.com/oniondelta/Onion_Rime_Files)|電腦 Rime 洋蔥方案（注音、雙拼、拼音、形碼、行列30）|44|Lua|2021/11/30|
|30|[iwiniwin/LuaKit](https://github.com/iwiniwin/LuaKit)|Lua核心工具包，包含对面向对象，组件系统（灵活的绑定解绑模式），mvc分模块加载，事件分发系统等常用模式的封装。同时提供打印，内存泄漏检测，性能分析等常用工具类。|43|Lua|2021/09/04|
|31|[oneinstack/ngx_lua_waf](https://github.com/oneinstack/ngx_lua_waf)|ngx_lua_waf是一个基于lua-nginx-module的web应用防火墙|41|Lua|2021/08/31|
|32|[Ace-Who/rime-xuma](https://github.com/Ace-Who/rime-xuma)|徐码／爾雅：三重注解、双重反查、屏蔽词组、全码后置、顶功版本……|41|Lua|2021/07/11|
|33|[zuorn/hammerspoon_config](https://github.com/zuorn/hammerspoon_config)|hammerspoon 配置文件|41|Lua|2021/09/28|
|34|[nshen/learn-neovim-lua](https://github.com/nshen/learn-neovim-lua)|📜 学习 Neovim 全配置， 逃离 VSCode。|40|Lua|2021/11/30|
|35|[SwimmingTiger/WowBigfootClassic](https://github.com/SwimmingTiger/WowBigfootClassic)|魔兽世界经典怀旧服大脚插件修改版|38|Lua|2021/10/08|
|36|[Seekladoom/Aegisub-Karaoke-Effect-481-Templates](https://github.com/Seekladoom/Aegisub-Karaoke-Effect-481-Templates)|转载自https://huyhuu.com/news/1282/Aegisub-Karaoke-Effect|35|Lua|2021/09/04|
|37|[jerrykuku/luci-app-go-aliyundrive-webdav](https://github.com/jerrykuku/luci-app-go-aliyundrive-webdav)|本项目是go-aliyun-webdav 的Luci 控制面板。|32|Lua|2021/11/10|
|38|[Ponpon55837/Squirrel](https://github.com/Ponpon55837/Squirrel)|鼠鬚管洋蔥純注音版簡化安裝與外觀設計|32|Lua|2021/11/18|
|39|[iDvel/rime-settings](https://github.com/iDvel/rime-settings)|自用 Rime 配置|29|Lua|2021/11/21|
|40|[whsir/ngx_lua_waf](https://github.com/whsir/ngx_lua_waf)|ngx_lua_waf改版，增加网段白名单等新功能|27|Lua|2021/07/27|
|41|[yanhuacuo/98wubi](https://github.com/yanhuacuo/98wubi)|具备码元提示功能的98五笔配置文件（for中州韵）|27|Lua|2021/11/03|
|42|[sirpdboy/luci-app-advanced](https://github.com/sirpdboy/luci-app-advanced)|luci-app-advanced 高级设置，包括smartdns，openclash，防火墙，DHCP等。|26|Lua|2021/07/31|
|43|[ASC8384/myRime](https://github.com/ASC8384/myRime)|我的 Rime 配置，适用于朙月拼音／小鹤双拼／小狼毫 ／ibus|26|Lua|2021/11/14|
|44|[amorphobia/openfly](https://github.com/amorphobia/openfly)|词库开源的小鹤音形 Rime 配方|26|Lua|2021/11/19|
|45|[nybdech/skynet-qp-server](https://github.com/nybdech/skynet-qp-server)|一个基于skynet的棋牌游戏服务端，也可以作为其他类型的游戏服务端|22|Lua|2021/09/23|
|46|[loaden/rime](https://github.com/loaden/rime)|中州韵输入法|22|Lua|2021/09/10|
|47|[sffxzzp/Starbound-Chinese](https://github.com/sffxzzp/Starbound-Chinese)|Starbound 汉化 mod|22|Lua|2021/08/05|
|48|[guoyongshi/BFFilter](https://github.com/guoyongshi/BFFilter)|WOW怀旧服组队频道信息过滤|21|Lua|2021/07/02|
|49|[EvanMeek/dotfiles](https://github.com/EvanMeek/dotfiles)|所有的配置文件(持续更新)|21|Lua|2021/11/30|
|50|[k8scat/lua-resty-feishu-auth](https://github.com/k8scat/lua-resty-feishu-auth)|适用于 OpenResty / ngx_lua 的基于飞书组织架构的登录认证|19|Lua|2021/11/24|
|51|[walkingsky/luci-wifidog](https://github.com/walkingsky/luci-wifidog)|wifidog的luci管理界面，基于openwert的可编译package|18|Lua|2021/08/16|
|52|[maojunxyz/flypy-linux](https://github.com/maojunxyz/flypy-linux)|flypy(小鹤双拼）hooked under fcitx-rime、ibus-rime、Yong Input Tool.|17|Lua|2021/10/10|
|53|[njjjay/IOTQQPlugins_selfuse](https://github.com/njjjay/IOTQQPlugins_selfuse)|自用IOTQQ/OPQbot插件合集|17|Lua|2021/08/30|
|54|[shengnoah/blues](https://github.com/shengnoah/blues)|Blues是一个基于Openresty + LUA的WEB演示框架|16|Lua|2021/10/29|
|55|[lilith-avatar/avatar-ava](https://github.com/lilith-avatar/avatar-ava)|:hammer_and_wrench: AvaKit开发框架|16|Lua|2021/11/29|
|56|[njcx/nginx_dump](https://github.com/njcx/nginx_dump)|该工具用于把Openresty(Nginx+Lua) 请求参数和响应 dump出来，用于旁路HTTP流量分析、风控、资产识别、API数据泄露等等|16|Lua|2021/07/08|
|57|[EtherProject/EtherWorkCollection](https://github.com/EtherProject/EtherWorkCollection)|EtherEngine 作品集|16|Lua|2021/09/05|
|58|[qwe7989199/RubyTools](https://github.com/qwe7989199/RubyTools)|日文字幕注音工具/Generate and add ruby in Aegisub|15|Lua|2021/11/22|
|59|[lxfly2000/Acy-Font](https://github.com/lxfly2000/Acy-Font)|自制手写字体。A hand-writing font set.|14|Lua|2021/10/24|
|60|[bayard/acamar](https://github.com/bayard/acamar)|Acamar auto-learning spam blocker addon for WoW / Acamar 自学习垃圾消息屏蔽插件。支持WoW怀旧版。|14|Lua|2021/06/22|
|61|[chenxuuu/lua-online](https://github.com/chenxuuu/lua-online)|在线测试lua代码，无需后端。test lua code online.|14|Lua|2021/08/24|
|62|[HahahaVal/easy_game](https://github.com/HahahaVal/easy_game)|一个基于skynet的简单服务器框架|12|Lua|2021/11/12|
|63|[xiyoo0812/luabt](https://github.com/xiyoo0812/luabt)|基于lua的AI行为树框架|11|Lua|2021/11/16|
|64|[brendonjkding/fgoScript](https://github.com/brendonjkding/fgoScript)|Fgo script based on touchelf. 适用于越狱iOS的Fgo脚本|11|Lua|2021/11/10|
|65|[qwertyuiop6/DST-Server-Build](https://github.com/qwertyuiop6/DST-Server-Build)|:video_game: 饥荒DST-server搭建|10|Lua|2021/05/04|
|66|[chenxuuu/receiver-meow-lua](https://github.com/chenxuuu/receiver-meow-lua)|接待喵lua插件的lua脚本仓库|10|Lua|2021/10/03|
|67|[ariwori/toomanyitemsplus](https://github.com/ariwori/toomanyitemsplus)|饥荒联机版控制台MOD。|10|Lua|2021/11/21|
|68|[ouwen000/lua-touchsprte](https://github.com/ouwen000/lua-touchsprte)|触动精灵代码合集|9|Lua|2021/11/09|
|69|[opq-osc/IOTQQ_Plugins](https://github.com/opq-osc/IOTQQ_Plugins)|IOTQQ机器人LuaPlugins插件仓库|9|Lua|2021/06/09|
|70|[tkzcfc/Kurumi](https://github.com/tkzcfc/Kurumi)|cocos2dx制作的动作游戏~|9|Lua|2021/10/21|

⬆ [回到目录](#内容目录)

<br/>

## Rust

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[rustcc/writing-an-os-in-rust](https://github.com/rustcc/writing-an-os-in-rust)|《使用Rust编写操作系统》|1.3k|Rust|2021/09/15|
|2|[messense/aliyundrive-webdav](https://github.com/messense/aliyundrive-webdav)|阿里云盘 WebDAV 服务|1.1k|Rust|2021/11/30|
|3|[iovxw/rssbot](https://github.com/iovxw/rssbot)|Lightweight Telegram RSS notification bot. 用于消息通知的轻量级 Telegram RSS 机器人|1.1k|Rust|2021/11/29|
|4|[ihciah/clean-dns-bpf](https://github.com/ihciah/clean-dns-bpf)|基于 Rust + eBPF 丢弃 GFW DNS 污染包|907|Rust|2021/10/13|
|5|[gmg137/netease-cloud-music-gtk](https://github.com/gmg137/netease-cloud-music-gtk)|Linux 平台下基于 Rust + GTK 开发的网易云音乐播放器|811|Rust|2021/10/12|
|6|[dontpanic92/OpenPAL3](https://github.com/dontpanic92/OpenPAL3)|仙三开源版 - The efforts to create an open-source implementation of Chinese Paladin 3|412|Rust|2021/11/28|
|7|[zu1k/copy-translator](https://github.com/zu1k/copy-translator)|简单、轻量、好用的划词翻译软件，利用DeepL翻译，无需注册api token|383|Rust|2021/11/08|
|8|[kuchiki-rs/kuchiki](https://github.com/kuchiki-rs/kuchiki)|(朽木) HTML/XML tree manipulation library for Rust|378|Rust|2021/06/15|
|9|[BinChengZhao/delicate](https://github.com/BinChengZhao/delicate)|A lightweight and distributed task scheduling platform written in rust. （一个轻量的分布式的任务调度平台通过rust编写）|377|Rust|2021/11/19|
|10|[arloor/nftables-nat-rust](https://github.com/arloor/nftables-nat-rust)|nftables nat rule generator——nftables nat规则生成器|363|Rust|2021/09/20|
|11|[geph-official/geph4](https://github.com/geph-official/geph4)|Geph (迷霧通) is a modular Internet censorship circumvention system designed specifically to deal with national filtering. |345|Rust|2021/10/24|
|12|[Enter-tainer/cxx2flow](https://github.com/Enter-tainer/cxx2flow)|将 C/C++ 代码转换成流程图 / Turn your C/C++ code into flowchart|304|Rust|2021/11/15|
|13|[wtklbm/rust-library-i18n](https://github.com/wtklbm/rust-library-i18n)|Rust 核心库和标准库的源码级中文翻译，可作为 IDE 工具的智能提示，也可以生成本地 API 文档 (Rust core library and standard library translation. can be used as IntelliSense for IDE tools, and can also generate local API documentation)|295|Rust|2021/11/30|
|14|[inherd/coco](https://github.com/inherd/coco)|An effective DevOps analysis and auto-suggest tool。Coco 是一个研发效能分析工具，如团队发展现状（根据架构复杂度及行数变更）、团队演进、历史分析等。生成可视化报告及对应的改进建议。 |275|Rust|2021/11/18|
|15|[p4gefau1t/trojan-r](https://github.com/p4gefau1t/trojan-r)|轻量高效的 Trojan 代理，使用 Rust 实现|240|Rust|2021/07/10|
|16|[wubx/rust-in-databend](https://github.com/wubx/rust-in-databend)|Rust 培养提高计划 https://github.com/datafuselabs/databend|236|Rust|2021/11/29|
|17|[HUST-OS/tornado-os](https://github.com/HUST-OS/tornado-os)|异步内核就像风一样快！|229|Rust|2021/08/18|
|18|[tyrchen/geektime-rust](https://github.com/tyrchen/geektime-rust)|我的极客时间 Rust 课程的代码仓库，随课程更新|227|Rust|2021/12/01|
|19|[gfreezy/seeker](https://github.com/gfreezy/seeker)|通过使用 tun 来实现透明代理。实现了类似 surge 增强模式与网关模式。|202|Rust|2021/09/19|
|20|[HMBSbige/get_cnip](https://github.com/HMBSbige/get_cnip)|获取国内 IP 和域名，生成路由表和 PAC 文件|194|Rust|2021/12/01|
|21|[ZhangHanDong/inviting-rust](https://github.com/ZhangHanDong/inviting-rust)|Rust实战视频课程代码示例|159|Rust|2021/08/30|
|22|[datum-lang/datum](https://github.com/datum-lang/datum)|A easy maintain(read/write) language for transform from/to other languages. 下一代企业级编程语言。|133|Rust|2021/11/01|
|23|[WeBankBlockchain/WeDPR-Lab-Core](https://github.com/WeBankBlockchain/WeDPR-Lab-Core)|Core libraries of WeDPR instant scenario-focused solutions for privacy-inspired business; WeDPR即时可用场景式隐私保护高效解决方案核心算法组件|126|Rust|2021/10/07|
|24|[zu1k/good-mitm](https://github.com/zu1k/good-mitm)|利用MITM技术实现请求和返回的重写、重定向、阻断等操作|117|Rust|2021/11/30|
|25|[Lisprez/so_stupid_search](https://github.com/Lisprez/so_stupid_search)|It's my honor to drive you fucking fire faster, to have more time with your Family and Sunshine.This tool is for those who often want to search for a string Deeply into a directory in Recursive mode, but not with the great tools: grep, ack, ripgrep .........every thing should be Small, Thin, Fast, L ...|111|Rust|2021/06/16|
|26|[rbatis/abs_admin](https://github.com/rbatis/abs_admin)|an enterprise-class one-stop backend solution,Rust企业级一站式后台解决方案|109|Rust|2021/11/30|
|27|[editso/fuso](https://github.com/editso/fuso)|一款快速, 稳定, 高效, 轻量的内网穿透, 端口转发工具|106|Rust|2021/11/30|
|28|[rust-tw/book-tw](https://github.com/rust-tw/book-tw)|Rust 程式設計語言（正體中文翻譯）|92|Rust|2021/10/01|
|29|[iTXTech/mcl-installer](https://github.com/iTXTech/mcl-installer)|MCL一键安装工具|90|Rust|2021/07/19|
|30|[Nugine/bfjit](https://github.com/Nugine/bfjit)|Brainfuck JIT 虚拟机教程|89|Rust|2021/05/18|
|31|[xuesongbj/Rust-Notes](https://github.com/xuesongbj/Rust-Notes)| 🦀️  Rust学习笔记 📖  |84|Rust|2021/12/01|
|32|[wtklbm/crm](https://github.com/wtklbm/crm)|Cargo registry manager (Cargo 注册表管理器)，用于方便的管理和更换 Rust 国内镜像源 |84|Rust|2021/11/27|
|33|[rust-lang-cn/book-cn](https://github.com/rust-lang-cn/book-cn)|Rust  程序设计语言——Chinese translation of The Rust Programming Language (Book)|80|Rust|2021/11/30|
|34|[ElnuDev/hebi](https://github.com/ElnuDev/hebi)|A highly customizable snake clone made in Rust with the Bevy engine, named after the Japanese word for snake, 蛇.|68|Rust|2021/09/05|
|35|[inherd/unflow](https://github.com/inherd/unflow)|unflow 是一个低代码、无代码设计语言。unFlow is a Design as Code implementation, a DSL for UX & backend modeling. DSL to Sketch file, Sketch to DSL, DSL to code. |56|Rust|2021/06/30|
|36|[lolishinshi/exloli](https://github.com/lolishinshi/exloli)|从 E 站抓取画(ben)廊(zi)并上传 telegraph 并转发至 telegram channel|53|Rust|2021/10/17|
|37|[dxx/datastructure-algorithm](https://github.com/dxx/datastructure-algorithm)|:file_folder: 图解数据结构和算法，使用多种语言实现|51|Rust|2021/10/19|
|38|[FuGangqiang/mdblog.rs](https://github.com/FuGangqiang/mdblog.rs)|static site generator from markdown files(markdown 格式静态博客生成器)|40|Rust|2021/11/18|
|39|[inrust/Rust-Programming-in-Action](https://github.com/inrust/Rust-Programming-in-Action)|《Rust编程：入门、实战与进阶》源码|38|Rust|2021/07/16|
|40|[xxg1413/inviting-rust-note](https://github.com/xxg1413/inviting-rust-note)|Rust实战课程笔记|37|Rust|2021/11/25|
|41|[zkonge/mirua](https://github.com/zkonge/mirua)|又又又一个Mirai一键启动器|35|Rust|2021/06/16|
|42|[nintha/demo-myblog](https://github.com/nintha/demo-myblog)|使用Rust、Actix-web和MongoDB构建简单博客网站|33|Rust|2021/08/23|
|43|[rust-zh/faq](https://github.com/rust-zh/faq)|收集整理在 Rust 众 https://t.me/rust_zh 的讨论中出现的一些常见疑问及其解答|33|Rust|2021/11/21|
|44|[stuartZhang/scaffold-wizard](https://github.com/stuartZhang/scaffold-wizard)|问卷反馈收集, 前端脚手架安装向导, rust, gtk3, win32, dll|32|Rust|2021/09/20|
|45|[nange/blog](https://github.com/nange/blog)|记录一些知识。。|32|Rust|2021/11/21|
|46|[phodal/quake](https://github.com/phodal/quake)|Quake 是面向极客的知识管理元框架。它可以：自由的文本内容管理、建知识网络体系、抓住稍纵即逝的灵感、自由的呈现画布。基于 Git + Markdown 的文档代码化方式，提供无限可能的数据可能性。基于 WebComponent + 插件化，提供自由的呈现画布。|31|Rust|2021/12/01|
|47|[chengluyu/turnip-price](https://github.com/chengluyu/turnip-price)|《动物森友会》大头菜价格算法 Rust & WebAssembly 版 / The Rust & WebAssembly implementation of the algorithm of the turnip price in Animal Crossing: New Horizon.|29|Rust|2021/11/27|
|48|[BUAA-SE-Compiling/natrium](https://github.com/BUAA-SE-Compiling/natrium)|北航 2020 年软院编译原理相关资料与规划   Docs and planning on Compilers class in 2020 for College of Software of BUAA|28|Rust|2021/10/07|
|49|[HUST-OS/tiaoshi-dashi-sbi](https://github.com/HUST-OS/tiaoshi-dashi-sbi)|调试大师：你见过最强的内核调试器|28|Rust|2021/05/07|
|50|[inherd/guarding](https://github.com/inherd/guarding)|Guarding 是一个用于 Java、JavaScript、Rust、Golang 等语言的架构守护工具。借助于易于理解的 DSL，来编写守护规则。Guarding is a guardians for code, architecture, layered. |28|Rust|2021/11/10|

⬆ [回到目录](#内容目录)

<br/>

## MATLAB

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[Kivy-CN/Stanford-CS-229-CN](https://github.com/Kivy-CN/Stanford-CS-229-CN)|A Chinese Translation of Stanford CS229 notes 斯坦福机器学习CS229课程讲义的中文翻译|2.9k|MATLAB|2021/11/24|
|2|[personqianduixue/Math_Model](https://github.com/personqianduixue/Math_Model)|美国大学生数学建模竞赛、全国大学生数学建模竞赛、华为杯研究生数学建模、数学建模美赛论文，数学建模国赛论文、LaTeX模板、国赛LaTeX模板、美赛LaTeX模板、mathorcup历年论文、研究生数学建模历年论文、电工杯、华中赛、APMCM亚太地区数学建模、深圳杯、中青杯、华东杯、数维杯、东三省数学建模、认证杯、数学建模书籍、数学建模算法、国赛评阅要点、数学建模word模板、软件模型算法汇总、MATLAB算法、常用Matlab代码、智能算法、现代的算法、图论算法、优化算法|247|MATLAB|2021/11/29|
|3|[yang69/MIMO-OFDM-Wireless-Communications-with-MATLAB](https://github.com/yang69/MIMO-OFDM-Wireless-Communications-with-MATLAB)|MATLAB Code for MIMO-OFDM Wireless Communications with MATLAB   MIMO-OFDM无线通信技术及MATLAB实现|121|Matlab|2021/05/20|
|4|[Wenzhe-Liu/sound-source-localization-algorithm_DOA_estimation](https://github.com/Wenzhe-Liu/sound-source-localization-algorithm_DOA_estimation)|关于语音信号声源定位DOA估计所用的一些传统算法|100|MATLAB|2021/06/30|
|5|[qwe14789cn/radar_tools](https://github.com/qwe14789cn/radar_tools)|信号处理工具箱|99|MATLAB|2021/08/31|
|6|[yandld/nav_matlab](https://github.com/yandld/nav_matlab)|基于的matlab导航科学计算库|62|MATLAB|2021/11/11|
|7|[NMID-CQUPT/CLF-CQPUT](https://github.com/NMID-CQUPT/CLF-CQPUT)|🛰重庆邮电大学课程攻略 Learning files for courses and training in  Chongqing University of Posts and Telecommunications|58|MATLAB|2021/11/13|
|8|[chengji253/Multiple-fixed-wing-UAVs-flight-simulation-platform](https://github.com/chengji253/Multiple-fixed-wing-UAVs-flight-simulation-platform)|A multiple fixed-wing UAVs flight simulation platform built by matlab and simulink.  一个小型固定翼无人机集群仿真演示平台|52|MATLAB|2021/05/13|
|9|[AomanHao/Matlab-Image-Dehaze-Enhance](https://github.com/AomanHao/Matlab-Image-Dehaze-Enhance)|图像去雾、图像增强、灰度直方图均衡化等|38|MATLAB|2021/10/23|
|10|[yanshui177/RecPlate-lib](https://github.com/yanshui177/RecPlate-lib)|基于BP神经网络的车牌识别系统|37|MATLAB|2021/05/13|
|11|[zuster/EconometricsResources](https://github.com/zuster/EconometricsResources)|经济学相关专业资料集|35|MATLAB|2021/08/02|
|12|[yooongchun/MatlabCourse](https://github.com/yooongchun/MatlabCourse)|数模课程Matlab代码资源仓库|31|MATLAB|2021/06/24|
|13|[JackHCC/Audio-Digital-Processing](https://github.com/JackHCC/Audio-Digital-Processing)|数字信号处理大作业：Matlab实现语音分析：加噪声，频谱分析，滤波器等等（内附报告）【Matlab for speech analysis: add noise, spectrum analysis, filter, etc】|29|MATLAB|2021/06/29|
|14|[ZhangHongBo2019/Constraint_NSGA-II_Algorithms](https://github.com/ZhangHongBo2019/Constraint_NSGA-II_Algorithms)|这是一个带约束条件的非支配排序遗传算法NSGA-II，解决了一个多目标优化问题|22|MATLAB|2021/09/10|
|15|[Wang-Yanping1996/Secured-Constraints-Unit-Commitment-SCUC-model-of-Power-system](https://github.com/Wang-Yanping1996/Secured-Constraints-Unit-Commitment-SCUC-model-of-Power-system)|Secured Constraints Unit Commitment model of Power system, including the model based on AC flow equation and DC flow equation. Only the constraints in pre-contingency state are considered. There is a piecewise function expression for power generation costs. However, I'm sorry that the version is rel ...|22|MATLAB|2021/08/14|
|16|[JiaxuanLiu/BPA-SAR-simulation](https://github.com/JiaxuanLiu/BPA-SAR-simulation)|使用后向投影算法(BPA)完成成像仿真|21|MATLAB|2021/07/08|
|17|[complone/NaSch](https://github.com/complone/NaSch)|三车道交通流元胞自动机matlab实现|19|MATLAB|2021/08/07|
|18|[ToKiNoBug/3dPixelPaintingInMinecraft](https://github.com/ToKiNoBug/3dPixelPaintingInMinecraft)|在minecraft中生成3d的地图画|19|MATLAB|2021/09/24|
|19|[Egoqing/Plate-location-and-recognition-based-on-FasterRCNN-and-CNN](https://github.com/Egoqing/Plate-location-and-recognition-based-on-FasterRCNN-and-CNN)|电子科技大学，信通学院，综合课程设计，车牌定位识别，附带数据集与训练好的网络|18|MATLAB|2021/05/09|
|20|[qxr777/NumericalMethod](https://github.com/qxr777/NumericalMethod)|数值计算方法课程|17|MATLAB|2021/11/15|
|21|[JackHCC/TheAlgorithm](https://github.com/JackHCC/TheAlgorithm)|Matlab实现的一些数学基础算法(Some mathematical basic algorithms implemented by Matlab)|12|MATLAB|2021/06/29|
|22|[ZhuliuAiagle/MyCourse](https://github.com/ZhuliuAiagle/MyCourse)|本科阶段一些课程资料整理|11|MATLAB|2021/08/24|
|23|[JackHCC/Routing-Algorithm](https://github.com/JackHCC/Routing-Algorithm)|MATLAB实现路由算法基本原理（内附报告）[MATLAB realizes the basic principle of routing algorithm - mathematics experiment work (with report)]|10|MATLAB|2021/06/29|
|24|[jeyao/Orbital-Angular-Momentum](https://github.com/jeyao/Orbital-Angular-Momentum)|涡旋电磁波相关仿真|10|MATLAB|2021/06/17|
|25|[sxy0818/ucas-course-2020](https://github.com/sxy0818/ucas-course-2020)|2020-2021学年的国科大雁栖湖集中教学选修课程课件|10|MATLAB|2021/08/28|
|26|[chdilo/BadAppleOSC](https://github.com/chdilo/BadAppleOSC)|在示波器上播放Bad Apple!!|10|MATLAB|2021/09/29|
|27|[Wang-Yanping1996/Mixed-Heat-Gas-Power-System-Scheduling](https://github.com/Wang-Yanping1996/Mixed-Heat-Gas-Power-System-Scheduling)|Maintenance Model of Power System with Matlab and Yalmip, including the maintenance of branches and generators, and a partial mantenance is allowed for branches. The Object Functions are 1.Maximize the minimum reserve rate, 2.Minimize the cost, 3. Minimize the Variance of reserve rate(resulting in a ...|9|MATLAB|2021/08/14|
|28|[intLyc/MTO-Platform](https://github.com/intLyc/MTO-Platform)|Multi-Task Optimization Platform 多任务优化平台|9|MATLAB|2021/11/30|
|29|[ToKiNoBug/MandelbortSetCalculate-Render](https://github.com/ToKiNoBug/MandelbortSetCalculate-Render)|计算和渲染曼德勃罗集的两套程序。|8|MATLAB|2021/06/13|
|30|[Tyler2025/Machine_Learning](https://github.com/Tyler2025/Machine_Learning)|台湾大学李宏毅教授课程作业练习|8|MATLAB|2021/09/02|
|31|[iChunyu/signal-process-demo](https://github.com/iChunyu/signal-process-demo)|数字信号处理学习记录|8|MATLAB|2021/11/29|
|32|[WHUzxp/Supports_for_PST_Paper](https://github.com/WHUzxp/Supports_for_PST_Paper)|论文《考虑实时市场联动的电力零售商鲁棒定价策略》的支撑文件，已投稿《电网技术》杂志。2021年10月23日。|8|MATLAB|2021/10/28|
|33|[cggos/opt_matlab](https://github.com/cggos/opt_matlab)|最优化方法及其MATLAB实现 源代码|7|MATLAB|2021/10/07|
|34|[lsr3406/Power-MVC](https://github.com/lsr3406/Power-MVC)|简单的电力系统计算工具包, 用于求解普通潮流, 交直流潮流, OPF, 故障, 暂态稳定.|7|MATLAB|2021/05/12|
|35|[Wang-Yanping1996/Maintenance-Model-of-Power-System](https://github.com/Wang-Yanping1996/Maintenance-Model-of-Power-System)|Maintenance Model of Power System with Matlab and Yalmip, including the maintenance of  branches and generators, and a partial mantenance is allowed for branches. The Object Functions are 1.Maximize the minimum reserve rate, 2.Minimize the cost, 3. Minimize the Variance of reserve rate(resulting in  ...|6|MATLAB|2021/08/14|
|36|[sailaoda/HUSTEIC_family](https://github.com/sailaoda/HUSTEIC_family)|🔥华中科技大学电信专业 课程资料 作业 代码 实验报告 HUSTEIC 课程分享计划 |6|MATLAB|2021/09/21|
|37|[wangweike123/Smooth-trajectory-splicing-based-on-Bezier-curve](https://github.com/wangweike123/Smooth-trajectory-splicing-based-on-Bezier-curve)|使用贝塞尔曲线，通过合理选取控制点使轨迹在拼接点处曲率连续|5|MATLAB|2021/09/06|
|38|[LingyvKong/PatternRecognition-with-matlab](https://github.com/LingyvKong/PatternRecognition-with-matlab)|使用matlab从0构建模式识别算法|5|MATLAB|2021/06/26|
|39|[xiesp/IntroductionToLinearAlgebra-Chinese-note](https://github.com/xiesp/IntroductionToLinearAlgebra-Chinese-note)|MIT  Gilbert Strang教授的线性导数导论笔记，包含全中文书本内容和视频笔记|5|MATLAB|2021/07/15|
|40|[tlt18/Plate-Location](https://github.com/tlt18/Plate-Location)|数字图像处理 车牌定位和字符分割|5|MATLAB|2021/06/24|

⬆ [回到目录](#内容目录)

<br/>

## Ruby

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[pluosi/app-host](https://github.com/pluosi/app-host)|应用内网发布   iOS OTA (Over-the-Air)   APP publish website like fir.im    适用于企业 iOS & Android 内网发布测试使用，方便管理和分发 APP 包|1.8k|Ruby|2021/11/04|
|2|[MBoxPlus/mbox](https://github.com/MBoxPlus/mbox)|Toolchain for Mobile App Development 移动研发工具链|588|Ruby|2021/11/16|
|3|[janx/ruby-pinyin](https://github.com/janx/ruby-pinyin)|中文汉字转拼音,　支持中英文符号混合词语。Pinyin is a romanization system (phonemic notation) of Chinese characters, this gem helps you to convert Chinese characters into pinyin form.|525|Ruby|2021/08/21|
|4|[kaich/codeobscure](https://github.com/kaich/codeobscure)|code obscure for object-c project.  方便强大的OC工程代码自动混淆工具|522|Ruby|2021/07/02|
|5|[lanrion/weixin_rails_middleware](https://github.com/lanrion/weixin_rails_middleware)|微信集成 ruby weixin_rails_middleware for integration weixin. |413|Ruby|2021/09/28|
|6|[ybdt/poc-hub](https://github.com/ybdt/poc-hub)|漏洞仓库：远程漏洞、本地漏洞|389|Ruby|2021/11/25|
|7|[flink-china/flink-china-doc](https://github.com/flink-china/flink-china-doc)|Flink 官方文档中文翻译项目 :cn:|370|Ruby|2021/07/13|
|8|[bingohuang/progit2-gitbook](https://github.com/bingohuang/progit2-gitbook)|Pro Gi­t 第二版 ­简体中文|289|Ruby|2021/09/06|
|9|[tryzealot/zealot](https://github.com/tryzealot/zealot)|开源自部署移动应用和 macOS 应用分发平台，提供 iOS、Android SDK、fastlane 等丰富组件库   Over The Air Server for deployment of Android, iOS and macOS apps|285|Ruby|2021/11/29|
|10|[githubmaidou/tools](https://github.com/githubmaidou/tools)|Python渗透漏洞工具|232|Ruby|2021/08/16|
|11|[r0eXpeR/fingerprint](https://github.com/r0eXpeR/fingerprint)|各种工具指纹收集分享|169|Ruby|2021/11/03|
|12|[rubytaiwan/ruby-taiwan](https://github.com/rubytaiwan/ruby-taiwan)|ruby-taiwan.org website source code. # master 追 ruby-china , production 才是 ruby-taiwan|141|Ruby|2021/09/28|
|13|[baidu/m-git](https://github.com/baidu/m-git)|MGit 是一款基于 Git 的多仓库管理工具，可以安全的、高效的管理多个 Git 仓库； 适合于在多个仓库中进行关联开发的项目，实现批量的版本管理功能，提高 Git 操作的效率，避免逐个执行 Git 命令带来的误操作风险。|132|Ruby|2021/08/12|
|14|[netease-im/NIM_iOS_SDK](https://github.com/netease-im/NIM_iOS_SDK)|网易云信 iOS SDK 发布仓库。【推荐客户得京东卡，首次推荐成单得3000元京东卡，连续推荐4500元/单，上不封顶。】点击参与https://yunxin.163.com/promotion/recommend|121|Ruby|2021/07/28|
|15|[xdite/mind-hack](https://github.com/xdite/mind-hack)|打造超人思維|104|Ruby|2021/06/21|
|16|[dsh0416/ruby-relearning](https://github.com/dsh0416/ruby-relearning)|《Ruby 再入门》：一本关于重新思考编程入门的教程|97|Ruby|2021/09/28|
|17|[uasi/skk-emoji-jisyo](https://github.com/uasi/skk-emoji-jisyo)|SKK 絵文字辞書|82|Ruby|2021/08/18|
|18|[diguage/byte-buddy-tutorial](https://github.com/diguage/byte-buddy-tutorial)|“Byte Buddy Tutorial” 中文翻译：Byte Buddy 教程。|76|Ruby|2021/11/16|
|19|[kufu/kiji](https://github.com/kufu/kiji)|e-Gov API 連携 gem|73|Ruby|2021/09/28|
|20|[iPermanent/rubyTools](https://github.com/iPermanent/rubyTools)|日常使用ruby脚本|73|Ruby|2021/10/18|
|21|[icyleaf/hpr](https://github.com/icyleaf/hpr)|镜像任意 git 仓库到 gitlab 的同步工具，具有定时更新的功能|66|Ruby|2021/10/13|
|22|[VICTOR-LUO-F/aliyun-sms](https://github.com/VICTOR-LUO-F/aliyun-sms)|a Ruby Gem for using aliyun sms service. 一个应用阿里云短信推送服务的Ruby Gem。|64|Ruby|2021/07/10|
|23|[SaulLawliet/watchdog](https://github.com/SaulLawliet/watchdog)|IF (接口/网页 有变化) THEN (提醒你)|51|Ruby|2021/11/05|
|24|[chengdh/manage-huo-baby](https://github.com/chengdh/manage-huo-baby)|管货宝-功能齐备的零担(专线)物流业务平台|50|Ruby|2021/06/10|
|25|[FunnyWolf/vipermsf](https://github.com/FunnyWolf/vipermsf)|viper 自定义的msf|43|Ruby|2021/11/09|
|26|[liuzhenangel/RBlog](https://github.com/liuzhenangel/RBlog)|个人博客系统v2.0|36|Ruby|2021/10/13|
|27|[lyy289065406/CVE-2021-22192](https://github.com/lyy289065406/CVE-2021-22192)|CVE-2021-22192 靶场： 未授权用户 RCE 漏洞|34|Ruby|2021/05/02|
|28|[thierryxing/dingtalk-bot](https://github.com/thierryxing/dingtalk-bot)|钉钉自定义机器人Ruby SDK|33|Ruby|2021/09/09|
|29|[jinhucheung/milog](https://github.com/jinhucheung/milog)|Milog 是一基于 Ruby on Rails 的个人博客网站|28|Ruby|2021/09/28|
|30|[yuchuanfeng/CFGifEmotionDemo](https://github.com/yuchuanfeng/CFGifEmotionDemo)|gif图文混排、动态图片、动态表情、网络图片图文混排、gif表情|28|Ruby|2021/11/22|
|31|[mycolorway/wework](https://github.com/mycolorway/wework)|Wework is ruby API wrapper for  wechat work「企业微信」.|24|Ruby|2021/11/11|
|32|[cyril/aoandon.rb](https://github.com/cyril/aoandon.rb)|Aoandon (青行燈) is a minimalist network intrusion detection system (NIDS).|24|Ruby|2021/06/30|
|33|[5xRuby/mokumokukai](https://github.com/5xRuby/mokumokukai)|Ruby 默默會 - 工作職缺看版|22|Ruby|2021/07/13|
|34|[jhjguxin/weixin](https://github.com/jhjguxin/weixin)|微信公众平台 开放消息接口|21|Ruby|2021/07/13|
|35|[OneEyedEagle/EAGLE-RGSS3](https://github.com/OneEyedEagle/EAGLE-RGSS3)|老鹰的脚本小巢|18|Ruby|2021/11/29|
|36|[hiroshiyui/youbike](https://github.com/hiroshiyui/youbike)|臺北市公共自行車「YouBike 微笑單車」租賃站資料轉換程式|17|Ruby|2021/09/28|
|37|[diylove/wiki](https://github.com/diylove/wiki)|diy万事万物的程序|16|Ruby|2021/11/29|
|38|[dao42/qywechat-notifier](https://github.com/dao42/qywechat-notifier)|RubyGem: 企业微信 notification exception 插件|16|Ruby|2021/09/28|
|39|[GITHZZ/UnityiOSExporter](https://github.com/GITHZZ/UnityiOSExporter)|用于Unity-iOS导出ipa的打包工具--ipa export tool for unity|15|Ruby|2021/07/10|
|40|[bitoex/bitopro-api-ruby](https://github.com/bitoex/bitopro-api-ruby)|API Wrapper gem for the Bitopro(幣託) crypto currency exchange written in Ruby.|15|Ruby|2021/07/13|

⬆ [回到目录](#内容目录)

<br/>

## Scala

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[geekyouth/SZT-bigdata](https://github.com/geekyouth/SZT-bigdata)|深圳地铁大数据客流分析系统🚇🚄🌟|1.3k|Scala|2021/05/09|
|2|[seveniruby/AppCrawler](https://github.com/seveniruby/AppCrawler)|基于appium的app自动遍历工具|1.0k|Scala|2021/11/17|
|3|[luochana/News_recommend](https://github.com/luochana/News_recommend)|基于Spark的新闻推荐系统，包含爬虫项目、web网站以及spark推荐系统|199|Scala|2021/08/13|
|4|[pierre94/flink-notes](https://github.com/pierre94/flink-notes)|flink学习笔记|187|Scala|2021/05/30|
|5|[CrestOfWave/Spark-2.3.1](https://github.com/CrestOfWave/Spark-2.3.1)|Spark-2.3.1源码解读|180|Scala|2021/08/13|
|6|[ReactivePlatform/Pragmatic-Scala](https://github.com/ReactivePlatform/Pragmatic-Scala)|Pragmatic Scala 中文版——《Scala实用指南》代码清单（包含 SBT 版本（切到sbt分支））|159|Scala|2021/11/30|
|7|[qindongliang/streaming-offset-to-zk](https://github.com/qindongliang/streaming-offset-to-zk)|一个手动管理spark streaming集成kafka时的偏移量到zookeeper中的小项目|137|Scala|2021/08/10|
|8|[GuoNingNing/fire-spark](https://github.com/GuoNingNing/fire-spark)|Spark 脚手架工程，标准化 spark 开发、部署、测试流程。|83|Scala|2021/06/04|
|9|[baolibin/Bigdata](https://github.com/baolibin/Bigdata)|大数据处理相关技术学习之路(持续更新中...)。        Bigdata整理  -->  慢慢滴~ 大数据相关技术包括离线处理，实时处理，OLAP等，如hadoop、spark、flink、hive、hbase、oozie...以及大数据项目，如用户画像、数据仓库等，欢迎感兴趣的小伙伴一起来开发...|49|Scala|2021/11/30|
|10|[kobelzy/SparkML](https://github.com/kobelzy/SparkML)|基于SparkML2.0进行的Kaggle、JData等比赛|40|Scala|2021/06/05|
|11|[ojlm/pea](https://github.com/ojlm/pea)|分布式压测引擎. A distributed stress tool based on gatling|39|Scala|2021/10/06|
|12|[springMoon/flink-rookie](https://github.com/springMoon/flink-rookie)|Flink 菜鸟公众号代码地址|39|Scala|2021/10/08|
|13|[Zjinji/SparkSyllabusBook](https://github.com/Zjinji/SparkSyllabusBook)|《Spark大数据分析源码解析与实例详解》图书配套实例资源|37|Scala|2021/11/25|
|14|[markwhitey/SchoolBigDataAnalysis](https://github.com/markwhitey/SchoolBigDataAnalysis)|此项目是对大学生的一卡通消费数据、图书借阅记录和图书馆门禁数据在spark集群的大数据框架环境之下进行聚类、关联分析，分析出学生的消费水平、生活规律、学习强度等聚类结果，以及将聚类结果进行FPGrowth关联分析得出学生聚类之间存在的关联性，此项目是使用scala语言，利用sparkSQL集合hive进行大数据分析|29|Scala|2021/08/05|
|15|[dedge-space/Reflow](https://github.com/dedge-space/Reflow)|任务[串/并联]组合调度框架。|23|Scala|2021/11/26|
|16|[HenryBao91/Flink-Analysis-of-Electronic-Commerce](https://github.com/HenryBao91/Flink-Analysis-of-Electronic-Commerce)|Flink 电商指标分析系统|20|Scala|2021/06/04|
|17|[AlwenXXD/nscscc2021](https://github.com/AlwenXXD/nscscc2021)|龙芯杯21个人赛作品|17|Scala|2021/09/15|
|18|[godelgnisEJW/real-time-monitoring-system](https://github.com/godelgnisEJW/real-time-monitoring-system)|本项目主要用于搭建一个基于docker的实时监控系统，涉及的大数据相关组件有Filebeat，Kafka，Zookeeper，Flink，后端主要使用了SpringBoot进行开发，数据库使用了Redis做缓存，前端用了Vue框架，通过Ant Design组件库和Echarts进行开发|15|Scala|2021/10/07|
|19|[fengchi66/realtime-dw](https://github.com/fengchi66/realtime-dw)|一个实时数仓项目，从0到1搭建实时数仓|13|Scala|2021/05/27|
|20|[welldo2017/scala-study](https://github.com/welldo2017/scala-study)|scala语言从0到入门教程|13|Scala|2021/07/20|
|21|[rison168/spark-profile-tags](https://github.com/rison168/spark-profile-tags)|基于Spark企业级用户画像项目|13|Scala|2021/08/16|
|22|[DnailZ/WangProver](https://github.com/DnailZ/WangProver)|An Automatic Theorem Prover for L(X) in Wang's Book (数理逻辑 第2版 汪芳庭) in Scala.|12|Scala|2021/11/29|
|23|[liuwei9/spinal_yolo](https://github.com/liuwei9/spinal_yolo)|使用spinal hdl在FPGA上实现YOLO|11|Scala|2021/11/10|
|24|[Skycrab/model-deploy](https://github.com/Skycrab/model-deploy)|Spark PMML 模型离线部署|10|Scala|2021/06/05|
|25|[wangpy1995/Spark-SQL-HBase](https://github.com/wangpy1995/Spark-SQL-HBase)| 利用spark sql在HBase上搭建的sql查询， 支持标准sql查询操作，后续有空闲时间会增加支持插入，删除，建表相关的ddl 语法（rowkey生成策略 部分尚未找到较好的解决方案，hbase查询 table也有待修改[目前暂定为TableMapper]）。 |8|Scala|2021/09/16|
|26|[RHobart/spark-lineage-parent](https://github.com/RHobart/spark-lineage-parent)|跟踪Spark-sql中的字段血缘关系|7|Scala|2021/08/12|
|27|[yilong2001/yl-spark-sql](https://github.com/yilong2001/yl-spark-sql)|一个Spark SQL方言，增强了批处理、机器学习、模型服务等语义；基于统一的SQL语法，提供了一个ETL、机器学习和推荐系统的框架|6|Scala|2021/06/04|
|28|[lou-yj/dbsync](https://github.com/lou-yj/dbsync)|通用数据库实时同步工具|6|Scala|2021/05/13|
|29|[xuanbo/dataflow](https://github.com/xuanbo/dataflow)|基于 Spark 任务流|6|Scala|2021/08/27|
|30|[sandexp/NoteOfSpark](https://github.com/sandexp/NoteOfSpark)|spark源码笔记|6|Scala|2021/10/21|

⬆ [回到目录](#内容目录)

<br/>

## Vim Script

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[wsdjeg/vim-galore-zh_cn](https://github.com/wsdjeg/vim-galore-zh_cn)|Vim 从入门到精通|8.6k|Vim script|2021/08/21|
|2|[wklken/k-vim](https://github.com/wklken/k-vim)|vim配置|4.8k|Vim script|2021/08/16|
|3|[mashirozx/Pixiv-Nginx](https://github.com/mashirozx/Pixiv-Nginx)|P站（Pixiv）的正确打开方式|1.3k|Vim script|2021/10/24|
|4|[dofy/learn-vim](https://github.com/dofy/learn-vim)|Vim 实操教程（Learning Vim）Vim practical tutorial.|1.3k|Vim script|2021/10/01|
|5|[skywind3000/vim-init](https://github.com/skywind3000/vim-init)|轻量级 Vim 配置框架，全中文注释|636|Vim script|2021/05/06|
|6|[jaywcjlove/vim-web](https://github.com/jaywcjlove/vim-web)|◈ 搞得像IDE一样的Vim，安装配置自己的Vim。|559|Vim script|2021/10/03|
|7|[TTWShell/legolas-vim](https://github.com/TTWShell/legolas-vim)|Vim配置，为python、go开发者打造的IDE。|239|Vim script|2021/08/18|
|8|[broqiang/vim-go-ide](https://github.com/broqiang/vim-go-ide)|Vim as the IDE for the go language 将 vim 打造成 go 语言的 ide|164|Vim script|2021/07/11|
|9|[jayli/vim-easycomplete](https://github.com/jayli/vim-easycomplete)|杭州市余杭区最好用的 VIM/NVIM 代码补全插件|130|Vim script|2021/11/17|
|10|[overmind1980/oeasyvim](https://github.com/overmind1980/oeasyvim)|这是oeasy制作的的一套关于vim的教程 可以在 https://www.lanqiao.cn/teacher/courses/2840 做实验 邀请码FJWYIMGB 本教程从0基础开始，到精通vim配置，和高级命令。希望能让vim是你的开发更高效。|108|Vim script|2021/11/02|
|11|[dofy/7th-vim](https://github.com/dofy/7th-vim)|安装简单，轻量、易用、高可配置性。 Lightweight & Customizable Vim configuration options.|92|Vim script|2021/11/16|
|12|[FengShangWuQi/to-vim-tmux-zsh](https://github.com/FengShangWuQi/to-vim-tmux-zsh)|如何让 vim，tmux，zsh 成为我们的神器|83|Vim script|2021/11/26|
|13|[hotoo/vimrc](https://github.com/hotoo/vimrc)|:v: 闲耘™ 的 Vim 配置。|82|Vim script|2021/11/25|
|14|[ZSaberLv0/ZFVimIM](https://github.com/ZSaberLv0/ZFVimIM)|vim输入法 / Vim Input Method by pure vim script, support: user word, dynamic word priority, cloud db files|80|Vim script|2021/11/06|
|15|[vimim/vimim](https://github.com/vimim/vimim)|VimIM —— Vim 中文輸入法|77|Vim script|2021/05/23|
|16|[iokfine/bee-dashboard](https://github.com/iokfine/bee-dashboard)|管理多个bee节点 提现工具|45|Vim script|2021/09/14|
|17|[epii1/vim](https://github.com/epii1/vim)|涛叔的 vim 配置|40|Vim script|2021/11/14|
|18|[chloneda/vim-cheatsheet](https://github.com/chloneda/vim-cheatsheet)|🍁Vim 命令速查表，包含一切你需要知道的东西！|39|Vim script|2021/07/14|
|19|[mrbeardad/SpaceVim](https://github.com/mrbeardad/SpaceVim)|基于SpaceVim的真正开箱即用、无需配置的IDE，你只需要记住快捷键即可。（目前默认支持C/C++、Go、Python、Shell、Markdown、VimL）|36|Vim script|2021/09/26|
|20|[skywind3000/vim-dict](https://github.com/skywind3000/vim-dict)|没办法，被逼的，重新整理一个词典补全的数据库|36|Vim script|2021/08/29|

⬆ [回到目录](#内容目录)

<br/>

## Perl

|#|Repository|Description|Stars|Language|Updated|
|:-|:-|:-|:-|:-|:-|
|1|[bollwarm/SecToolSet](https://github.com/bollwarm/SecToolSet)|The security tool(project) Set from github。github安全项目工具集合 |997|Perl|2021/11/04|
|2|[Oreomeow/checkinpanel](https://github.com/Oreomeow/checkinpanel)|一个主要运行在 𝐞𝐥𝐞𝐜𝐕𝟐𝐏 或 𝐪𝐢𝐧𝐠𝐥𝐨𝐧𝐠 等定时面板，同时支持系统运行环境的签到项目（环境：𝑷𝒚𝒕𝒉𝒐𝒏 3.8+ / 𝑵𝒐𝒅𝒆.𝒋𝒔 10+ / 𝑩𝒂𝒔𝒉 4+ / 𝑶𝒑𝒆𝒏𝑱𝑫𝑲8 / 𝑷𝒆𝒓𝒍5）|485|Perl|2021/11/27|
|3|[wisdomfusion/qqwry.dat](https://github.com/wisdomfusion/qqwry.dat)|纯真IP地址数据库镜像，mirror of qqwry.dat|355|Perl|2021/05/07|
|4|[open-c3/open-c3](https://github.com/open-c3/open-c3)|CICD系统/发布系统/作业平台|138|Perl|2021/11/30|
|5|[leolovenet/qqwry2mmdb](https://github.com/leolovenet/qqwry2mmdb)|为 Wireshark 能使用纯真网络 IP 数据库(QQwry)而提供的格式转换工具|112|Perl|2021/11/01|
|6|[k1d0ne/cobaltstrike_plugin](https://github.com/k1d0ne/cobaltstrike_plugin)|陆续补充一些自己写的cobaltstrike插件|34|Perl|2021/11/05|
|7|[jim-kirisame/jpgramma-cn-pdf-converter](https://github.com/jim-kirisame/jpgramma-cn-pdf-converter)|一个将日语语法指南网页版转成pdf的小工具|29|Perl|2021/10/10|
|8|[shangshanzhizhe/Work_flow_of_population_genetics](https://github.com/shangshanzhizhe/Work_flow_of_population_genetics)|整理常用的群体遗传学分析流程和脚本|27|Perl|2021/11/01|
|9|[g0v/people-in-news](https://github.com/g0v/people-in-news)|公眾人物新聞的追蹤|17|Perl|2021/11/01|
|10|[MYDan/mayi](https://github.com/MYDan/mayi)|蚂蚁蛋运维助手(安装方式: curl -L http://update.mydan.org bash)|12|Perl|2021/10/14|

⬆ [回到目录](#内容目录)